"use strict";
var KTDatatablesDataSourceAjaxServer = function() {

	var initTableUsers = function() {
		
		var table = $('#kt_datatable_users').DataTable({

			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,
			//stateSave: true,

			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[0, 'desc']],
            language: {
                "lengthMenu": "Her sayfada _MENU_ kayıt göster",
                "info": "Sayfa: _PAGE_/_PAGES_ (_MAX_ kayıt filtrelendi)",
                "infoEmpty": "Kayıt Yok.",
                "zeroRecords": "Kayıt Yok.",
                "infoFiltered": "(Toplam _MAX_ kayıttan filtrelendi)",
                "processing": "Yükleniyor...",
                "search": "Arama:",
            },

			ajax: {
				url: HOST_URL + '/getUsers',
				type: 'POST',
				headers: { 'X-CSRF-TOKEN': $('#token').val() },
				data: function(data){
					data.columnsDef = ['id', 'identity', 'first_name', 'email', 'province_district', 'user_detail', 'phone',  'date_joined', 'icons', 'actions'];
					data.searchRole = $('#select2_staff_search').val();
					data.searchUserType = $('#select2_user_type_search').val();
					data.searchTeam = $('#select2_team_search').val();
					data.searchActive = $('#select2_active_search').val();
					data.searchInfoShare = $('#select2_infoshare_search').val();
			    },
			},
			columns: [
				{name: 'id'},
				{name: 'identity'},
				{name: 'first_name'},
				{name: 'email'},
				{name: 'province_district'},
				{name: 'user_detail'},
				{name: 'phone'},
				{name: 'date_joined'},
				{name: 'icons'},
				{name: 'actions', responsivePriority: -1},
			],
			columnDefs: [
				{targets: 0, title: 'ID', visible: true},
				{targets: 1, title: 'TC'},
				{
					
					targets: 2, 
					title: 'Ad Soyad',

					render: function(data, type, full, meta) {
						//console.log(data);
						var sdata = full[8].split("::");

						var fullname = '';
						if(sdata[1] == 1){
							fullname = data;
						}else{
							fullname = '<del>'+data+'</del>';
						}

						return fullname;
					},

				},
				{targets: 3, title: 'Eposta'},
				{targets: 4, title: 'Konum'},
				{
					targets: 5, 
					title: 'Statü', 
					orderable: false,

					render: function(data, type, full, meta) {

						var returnValue = '';
						var sdata = data.split("::");
						//console.log(sdata);
						var user_type = {
							'coordinator': {'state': 'fa-user-tie', 'color': 'text-warning'},
							'referee': {'state': 'fa-child', 'color': 'text-warning'},
							'player': {'state': 'fa-users', 'color': 'text-success'},
							'only_user': {'state': 'fa-user', 'color': ''},
						};

						if(sdata[0] == 'coordinator'){
							returnValue += '<i data-trigger="hover" data-toggle="tooltip" title="Koordinatör" class="fas ' + user_type[sdata[0]].state + ' ' + user_type[sdata[0]].color + '"></i> ';
						}

						if(sdata[1] == 'referee'){
							returnValue += '<i data-trigger="hover" data-toggle="tooltip" title="Hakem" class="fas ' + user_type[sdata[1]].state + ' ' + user_type[sdata[1]].color + '"></i> ';
						}

						if(sdata[2].split(":")[0] == 'player'){
							returnValue += '<i data-trigger="hover" data-toggle="tooltip" title="Oyuncu" class="fas ' + user_type[sdata[2].split(":")[0]].state + ' ' + user_type[sdata[2].split(":")[0]].color + '"></i> '+sdata[2].split(":")[1];
						}

						if(sdata[3] == 'only_user'){
							returnValue += '<i data-trigger="hover" data-toggle="tooltip" title="Kullanıcı" class="fas ' + user_type[sdata[3]].state + ' ' + user_type[sdata[3]].color + '"></i> ';
						}

						return returnValue;
					},
				},
				{targets: 6, title: 'Telefon'},
				{targets: 7, title: 'Katılma Tar.'},
				{
					targets: 8, 
					title: '',
					
					render: function(data, type, full, meta) {
						
						var sdata = data.split("::");
						//console.log(sdata);
						//return;

						var staff_status = {
							'2': {'state': 'fa-user-shield', 'color': 'text-warning'},
							'1': {'state': 'fa-user-shield', 'color': 'text-success'},
							'': {'state': 'fa-user-shield', 'color': ''},
						};

						var active_status = {
							'1': {'state': 'fa-check-circle', 'color': 'text-success'},
							'': {'state': 'fa-times-circle', 'color': 'text-danger'},
						};

						var email_confirmed_status = {
							'1': {'state': 'fa-envelope-open', 'color': 'text-success'},
							'': {'state': 'fa-envelope-open', 'color': 'text-danger'},
						};

						var phone_confirmed_status = {
							'1': {'state': 'fa-phone-square', 'color': 'text-success'},
							'': {'state': 'fa-phone-square', 'color': 'text-danger'},
						};

						var infoshare_status = {
							'1': {'state': 'fa-handshake', 'color': 'text-success'},
							'2': {'state': 'fa-handshake', 'color': ''},
							'': {'state': 'fa-handshake', 'color': 'text-danger'},
						};
						
						if (typeof staff_status[sdata[0]] === 'undefined') {
							//console.log('bbb undefined');
							return data;
						}
						//console.log(phone_confirmed_status);
						//console.log(sdata[3]);

						return '<i data-trigger="hover" data-toggle="tooltip" title="'+((sdata[0] == '2') ? 'Süper User' : ((sdata[0] == '1') ? 'Yönetici' : 'Yönetici Değil'))+'" class="fas ' + staff_status[sdata[0]].state + ' ' + staff_status[sdata[0]].color + ' "></i> \
						<i data-trigger="hover" data-toggle="tooltip" title="'+((sdata[1] == '1') ? 'Aktif' : 'Pasif')+'" class="fas ' + active_status[sdata[1]].state + ' ' + active_status[sdata[1]].color + ' "></i> \
						<i data-trigger="hover" data-toggle="tooltip" title="'+((sdata[2] == '1') ? 'Eposta Onaylı' : 'Eposta Onaylı Değil')+'" class="fas ' + email_confirmed_status[sdata[2]].state + ' ' + email_confirmed_status[sdata[2]].color + ' "></i> \
						<i data-trigger="hover" data-toggle="tooltip" title="'+((sdata[3] == '1') ? 'Telefon Onaylı' : 'Telefon Onaylı Değil')+'" class="fas ' + phone_confirmed_status[sdata[3]].state + ' ' + phone_confirmed_status[sdata[3]].color + ' "></i> \
						<i data-trigger="hover" data-toggle="tooltip" title="'+((sdata[4] == '1') ? 'Bilgi paylaşım izni vermiş' : (sdata[4] == '2') ? 'Bilgi paylaşım izni henüz seçilmemiş' : 'Bilgi paylaşım izni vermemiş')+'" class="fas ' + infoshare_status[sdata[4]].state + ' ' + infoshare_status[sdata[4]].color + ' "></i>';
					},
				},
				{
					targets: -1,
					title: 'İşlem',
					orderable: false,
					render: function(data, type, full, meta) {
						
						var sdata = full[8].split("::");

						var deleteBtn = '';
						if(sdata[1] == 1){
							deleteBtn = '\
							<a href="users/delete/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_delete_user" title="Sil">\
								<i class="fas fa-trash-alt"></i>\
							</a>\
							';
						}

						return '\
						<a href="users/view/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_view_user" title="Görüntüle">\
							<i class="fas fa-eye"></i>\
						</a>\
						<a href="users/edit/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_change_user" title="Düzenle">\
							<i class="fas fa-edit"></i>\
						</a>\
						<a href="users/password/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_password_user" title="Şifre Değiştir">\
							<i class="fas fa-key"></i>\
						</a>'+deleteBtn;
					},
				},
			],
			drawCallback: function (settings) {
		      	$('[data-toggle="tooltip"]').tooltip();
		      	viButUser();
		    }

		});


		$('#kt_search').on('click', function(e) {
			e.preventDefault();
			
			var params = {};
			$('.datatable-input').each(function() {
				//console.log($(this));
				var i = $(this).data('col-index');
				if (params[i]) {
					params[i] += '|' + $(this).val();
				}
				else {
					params[i] = $(this).val();
				}
			});
			$.each(params, function(i, val) {
				// apply search params to datatable
				table.column(i).search(val ? val : '', false, false);
			});

			table.table().draw();
		});

		$('#kt_reset').on('click', function(e) {
			e.preventDefault();
			$('.datatable-input').each(function() {
				if($(this).prop("tagName") == 'INPUT'){
					$(this).val('');
				}else if($(this).prop("tagName") == 'SELECT'){
					$(this).val(null).change();
				}
				
				table.column($(this).data('col-index')).search('', false, false);
			});

			table.table().draw();
		});
	};

	var initTablePlayerList = function() {
		
		var table = $('#kt_datatable_player_list').DataTable({

			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,
			//stateSave: true,

			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[0, 'desc']],
            language: {
                "lengthMenu": "Her sayfada _MENU_ kayıt göster",
                "info": "Sayfa: _PAGE_/_PAGES_ (_MAX_ kayıt filtrelendi)",
                "infoEmpty": "Kayıt Yok.",
                "zeroRecords": "Kayıt Yok.",
                "infoFiltered": "(Toplam _MAX_ kayıttan filtrelendi)",
                "processing": "Yükleniyor...",
                "search": "Arama:",
            },

			ajax: {
				url: HOST_URL + '/getPlayers',
				type: 'POST',
				headers: { 'X-CSRF-TOKEN': $('#token').val() },
				data: function(data){
					data.columnsDef = ['id', 'identity', 'first_name', 'email', 'province_district', 'user_detail', 'phone',  'date_joined', 'icons', 'actions'];
					data.team_id = $('#team_id').val();
					data.searchRole = $('#select2_staff_search').val();
					data.searchUserType = $('#select2_user_type_search').val();
					data.searchTeam = $('#select2_team_search').val();
					data.searchActive = $('#select2_active_search').val();
			    },
			},
			columns: [
				{name: 'id'},
				{name: 'identity'},
				{name: 'first_name'},
				{name: 'email'},
				{name: 'province_district'},
				{name: 'user_detail'},
				{name: 'phone'},
				{name: 'date_joined'},
				{name: 'icons'},
				{name: 'actions', responsivePriority: -1},
			],
			columnDefs: [
				{targets: 0, title: 'Oyuncu ID', visible: true},
				{targets: 1, title: 'TC'},
				{
					targets: 2, 
					title: 'Ad Soyad',
					render: function(data, type, full, meta) {
						//console.log(data);
						var sdata = full[8].split("::");

						var fullname = '';
						if(sdata[1] == 1){
							fullname = data;
						}else{
							fullname = '<del>'+data+'</del>';
						}
						return fullname;
					},
				},
				{targets: 3, title: 'Eposta'},
				{targets: 4, title: 'Konum'},
				{
					targets: 5, 
					title: 'Takım', 
					orderable: false
				},
				{targets: 6, title: 'Telefon'},
				{targets: 7, title: 'Katılma Tar.'},
				{
					targets: 8, 
					title: '',
					
					render: function(data, type, full, meta) {
						
						var sdata = data.split("::");
						//console.log(sdata);
						//return;

						var staff_status = {
							'2': {'state': 'fa-user-shield', 'color': 'text-warning'},
							'1': {'state': 'fa-user-shield', 'color': 'text-success'},
							'': {'state': 'fa-user-shield', 'color': ''},
						};

						var active_status = {
							'1': {'state': 'fa-check-circle', 'color': 'text-success'},
							'': {'state': 'fa-times-circle', 'color': 'text-danger'},
						};

						var email_confirmed_status = {
							'1': {'state': 'fa-envelope-open', 'color': 'text-success'},
							'': {'state': 'fa-envelope-open', 'color': 'text-danger'},
						};

						var phone_confirmed_status = {
							'1': {'state': 'fa-phone-square', 'color': 'text-success'},
							'': {'state': 'fa-phone-square', 'color': 'text-danger'},
						};
						
						if (typeof staff_status[sdata[0]] === 'undefined') {
							//console.log('bbb undefined');
							return data;
						}

						//console.log(phone_confirmed_status);
						//console.log(sdata[3]);

						return '<i data-trigger="hover" data-toggle="tooltip" title="'+((sdata[0] == '2') ? 'Süper User' : ((sdata[0] == '1') ? 'Yönetici' : 'Yönetici Değil'))+'" class="fas ' + staff_status[sdata[0]].state + ' ' + staff_status[sdata[0]].color + ' "></i> \
						<i data-trigger="hover" data-toggle="tooltip" title="'+((sdata[1] == '1') ? 'Aktif' : 'Pasif')+'" class="fas ' + active_status[sdata[1]].state + ' ' + active_status[sdata[1]].color + ' "></i> \
						<i data-trigger="hover" data-toggle="tooltip" title="'+((sdata[2] == '1') ? 'Eposta Onaylı' : 'Eposta Onaylı Değil')+'" class="fas ' + email_confirmed_status[sdata[2]].state + ' ' + email_confirmed_status[sdata[2]].color + ' "></i> \
						<i data-trigger="hover" data-toggle="tooltip" title="'+((sdata[3] == '1') ? 'Telefon Onaylı' : 'Telefon Onaylı Değil')+'" class="fas ' + phone_confirmed_status[sdata[3]].state + ' ' + phone_confirmed_status[sdata[3]].color + ' "></i>';
					},
				},
				{
					targets: -1,
					title: 'İşlem',
					orderable: false,
					render: function(data, type, full, meta) {
						
						var sdata = full[8].split("::");
						return '\
						<a href="/teams/addinsideplayer_save/'+$('#team_id').val()+"/"+full[0]+'" class="btn btn-sm btn-success" title="Oyuncuyu Takıma Ekle">\
							<i class="fas fa-plus"></i> Ekle\
						</a>';
					},
				},
			],
			drawCallback: function (settings) {
		      	$('[data-toggle="tooltip"]').tooltip();
		      	viButUser();
		    }

		});


		$('#kt_search').on('click', function(e) {
			e.preventDefault();
			
			var params = {};
			$('.datatable-input').each(function() {
				//console.log($(this));
				var i = $(this).data('col-index');
				if (params[i]) {
					params[i] += '|' + $(this).val();
				}
				else {
					params[i] = $(this).val();
				}
			});
			$.each(params, function(i, val) {
				// apply search params to datatable
				table.column(i).search(val ? val : '', false, false);
			});

			table.table().draw();
		});

		$('#kt_reset').on('click', function(e) {
			e.preventDefault();
			$('.datatable-input').each(function() {
				if($(this).prop("tagName") == 'INPUT'){
					$(this).val('');
				}else if($(this).prop("tagName") == 'SELECT'){
					$(this).val(null).change();
				}
				
				table.column($(this).data('col-index')).search('', false, false);
			});

			table.table().draw();
		});
	};

	var initTableRoles = function() {
		
		var table = $('#kt_datatable_roles').DataTable({

			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,
			//stateSave: true,

			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[0, 'desc']],
            language: {
                "lengthMenu": "Her sayfada _MENU_ kayıt göster",
                "info": "Sayfa: _PAGE_/_PAGES_ (_MAX_ kayıt filtrelendi)",
                "infoEmpty": "Kayıt Yok.",
                "zeroRecords": "Kayıt Yok.",
                "infoFiltered": "(Toplam _MAX_ kayıttan filtrelendi)",
                "processing": "Yükleniyor...",
                "search": "Arama:",
            },
			ajax: {
				url: HOST_URL + '/getRoles',
				type: 'POST',
				headers: { 'X-CSRF-TOKEN': $('#token').val() },
				data: function(data){
					data.columnsDef = ['id', 'name', 'actions'];
			    },
			},
			columns: [
				{name: 'id'},
				{name: 'name'},
				{name: 'actions', responsivePriority: -1},
			],
			columnDefs: [
				{targets: 0, title: 'ID', visible: false},
				{targets: 1, title: 'Adı'},
				{
					targets: -1,
					title: 'İşlem',
					orderable: false,
					width: '150px', //btn btn-text-primary btn-hover-light-primary font-weight-bold 
					render: function(data, type, full, meta) {
						return '\
						<a href="roles/view/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_view_permission" title="Görüntüle">\
							<i class="fas fa-eye"></i>\
						</a>\
						<a href="roles/edit/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_change_permission" title="Düzenle">\
							<i class="fas fa-edit"></i>\
						</a>\
						<a href="roles/delete/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_delete_permission" title="Sil">\
							<i class="fas fa-trash-alt"></i>\
						</a>\
						<a href="roles/permission/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_change_permission" title="Yetkileri Düzenle">\
							<i class="fas fa-book-medical"></i></i>\
						</a>\
						';
					},
				},
			],
			drawCallback: function (settings) {
				viButRole();
		    }

		});
	};

	var initTableConferences = function() {

		// begin first table
		var table = $('#kt_datatable_conferences').DataTable({

			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,
			//stateSave: true,

			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[0, 'desc']],
            language: {
                "lengthMenu": "Her sayfada _MENU_ kayıt göster",
                "info": "Sayfa: _PAGE_/_PAGES_ (_MAX_ kayıt filtrelendi)",
                "infoEmpty": "Kayıt Yok.",
                "zeroRecords": "Kayıt Yok.",
                "infoFiltered": "(Toplam _MAX_ kayıttan filtrelendi)",
                "processing": "Yükleniyor...",
                "search": "Arama:",
            },

			ajax: {
				url: HOST_URL + '/getConferences',
				type: 'POST',
				headers: { 'X-CSRF-TOKEN': $('#token').val() },
				data: {
					// parameters for custom backend script demo
					// columnsDef: ['id', 'first_name', 'username', 'email', 'actions'],
					columnsDef: ['id', 'province_id', 'name', 'actions'],
				},
			},
			columns: [
				{name: 'id'},
				{name: 'name'},
				{name: 'province_id'},
				{name: 'actions', responsivePriority: -1},
			],
			columnDefs: [

				{targets: 0, title: 'ID', visible: false},
				{targets: 1, title: 'Ad'},
				{targets: 2, title: 'Şehir'},

				{
					targets: -1,
					title: 'İşlem',
					orderable: false,
					width: '150px',
					render: function(data, type, full, meta) {
						return '\
						<a href="conferences/view/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_view_conference" title="Görüntüle">\
							<i class="fas fa-eye"></i>\
						</a>\
						<a href="conferences/edit/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_change_conference" title="Düzenle">\
							<i class="fas fa-edit"></i>\
						</a>\
						<a href="conferences/delete/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_delete_conference" title="Sil">\
							<i class="fas fa-trash-alt"></i>\
						</a>\
						';
					},
				},

			],
			drawCallback: function (settings) {
				viButConference();
		    }
		});

		$('#kt_search').on('click', function(e) {
			e.preventDefault();
			
			var params = {};
			$('.datatable-input').each(function() {
				//console.log($(this));
				var i = $(this).data('col-index');
				if (params[i]) {
					params[i] += '|' + $(this).val();
				}
				else {
					params[i] = $(this).val();
				}
			});
			$.each(params, function(i, val) {
				// apply search params to datatable
				table.column(i).search(val ? val : '', false, false);
			});

			table.table().draw();
		});

		$('#kt_reset').on('click', function(e) {
			e.preventDefault();
			$('.datatable-input').each(function() {
				if($(this).prop("tagName") == 'INPUT'){
					$(this).val('');
				}else if($(this).prop("tagName") == 'SELECT'){
					$(this).val(null).change();
				}
				
				table.column($(this).data('col-index')).search('', false, false);
			});

			table.table().draw();
		});
	};

	var initTableProvinces = function() {

		var table = $('#kt_datatable_provinces');

		// begin first table
		table.DataTable({

			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,
			//stateSave: true,

			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[0, 'desc']],
            language: {
                "lengthMenu": "Her sayfada _MENU_ kayıt göster",
                "info": "Sayfa: _PAGE_/_PAGES_ (_MAX_ kayıt filtrelendi)",
                "infoEmpty": "Kayıt Yok.",
                "zeroRecords": "Kayıt Yok.",
                "infoFiltered": "(Toplam _MAX_ kayıttan filtrelendi)",
                "processing": "Yükleniyor...",
                "search": "Arama:",
            },

			ajax: {
				url: HOST_URL + '/getProvinces',
				type: 'POST',
				headers: { 'X-CSRF-TOKEN': $('#token').val() },
				data: {
					// parameters for custom backend script demo
					// columnsDef: ['id', 'first_name', 'username', 'email', 'actions'],
					columnsDef: ['id', 'name', 'plate_code', 'actions'],
				},
			},
			columns: [
				{name: 'id'},
				{name: 'name'},
				{name: 'plate_code'},
				{name: 'actions', responsivePriority: -1},
			],
			columnDefs: [

				{targets: 0, title: 'ID', visible: false},
				{targets: 1, title: 'Ad'},
				{targets: 2, title: 'Plaka'},

				{
					targets: -1,
					title: 'İşlem',
					orderable: false,
					width: '150px',
					render: function(data, type, full, meta) {
						return '\
						<a href="provinces/view/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_view_province" title="Görüntüle">\
							<i class="fas fa-eye"></i>\
						</a>\
						<a href="provinces/edit/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_change_province" title="Düzenle">\
							<i class="fas fa-edit"></i>\
						</a>\
						<a href="provinces/delete/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_delete_province" title="Sil">\
							<i class="fas fa-trash-alt"></i>\
						</a>\
						';
					},
				},

			],
			drawCallback: function (settings) {
				viButProvince();
		    }
		});
	};

	var initTableDistricts = function() {

		// begin first table
		var table = $('#kt_datatable_districts').DataTable({

			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,
			//stateSave: true,

			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[2, 'asc']],
            language: {
                "lengthMenu": "Her sayfada _MENU_ kayıt göster",
                "info": "Sayfa: _PAGE_/_PAGES_ (_MAX_ kayıt filtrelendi)",
                "infoEmpty": "Kayıt Yok.",
                "zeroRecords": "Kayıt Yok.",
                "infoFiltered": "(Toplam _MAX_ kayıttan filtrelendi)",
                "processing": "Yükleniyor...",
                "search": "Arama:",
            },

			ajax: {
				url: HOST_URL + '/getDistricts',
				type: 'POST',
				headers: { 'X-CSRF-TOKEN': $('#token').val() },
				data: {
					columnsDef: ['id', 'name', 'province_id', 'actions'],
				},
			},
			columns: [
				{name: 'id'},
				{name: 'name'},
				{name: 'province_id'},
				{name: 'actions', responsivePriority: -1},
			],
			columnDefs: [

				{targets: 0, title: 'ID', visible: false},
				{targets: 1, title: 'Ad'},
				{targets: 2, title: 'Şehir'},

				{
					targets: -1,
					title: 'İşlem',
					orderable: false,
					width: '150px',
					render: function(data, type, full, meta) {
						return '\
						<a href="districts/view/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_view_district" title="Görüntüle">\
							<i class="fas fa-eye"></i>\
						</a>\
						<a href="districts/edit/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_change_district" title="Düzenle">\
							<i class="fas fa-edit"></i>\
						</a>\
						<a href="districts/delete/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_delete_district" title="Sil">\
							<i class="fas fa-trash-alt"></i>\
						</a>\
						';
					},
				},

			],
			drawCallback: function (settings) {
				viButDistrict();
		    }
		});

		$('#kt_search').on('click', function(e) {
			e.preventDefault();
			
			var params = {};
			$('.datatable-input').each(function() {
				//console.log($(this));
				var i = $(this).data('col-index');
				if (params[i]) {
					params[i] += '|' + $(this).val();
				}
				else {
					params[i] = $(this).val();
				}
			});
			$.each(params, function(i, val) {
				// apply search params to datatable
				table.column(i).search(val ? val : '', false, false);
			});

			table.table().draw();
		});

		$('#kt_reset').on('click', function(e) {
			e.preventDefault();
			$('.datatable-input').each(function() {
				if($(this).prop("tagName") == 'INPUT'){
					$(this).val('');
				}else if($(this).prop("tagName") == 'SELECT'){
					$(this).val(null).change();
				}
				
				table.column($(this).data('col-index')).search('', false, false);
			});

			table.table().draw();
		});
	};

	var initTableGrounds = function() {

		// begin first table
		var table = $('#kt_datatable_grounds').DataTable({

			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,
			//stateSave: true,

			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[1, 'asc']],
            language: {
                "lengthMenu": "Her sayfada _MENU_ kayıt göster",
                "info": "Sayfa: _PAGE_/_PAGES_ (_MAX_ kayıt filtrelendi)",
                "infoEmpty": "Kayıt Yok.",
                "zeroRecords": "Kayıt Yok.",
                "infoFiltered": "(Toplam _MAX_ kayıttan filtrelendi)",
                "processing": "Yükleniyor...",
                "search": "Arama:",
            },

			ajax: {
				url: HOST_URL + '/getGrounds',
				type: 'POST',
				headers: { 'X-CSRF-TOKEN': $('#token').val() },
				data: {
					columnsDef: ['id', 'name', 'province_district', 'actions'],
				},
			},
			columns: [
				{name: 'id'},
				{name: 'name'},
				{name: 'province_district'},
				{name: 'actions', responsivePriority: -1},
			],
			columnDefs: [

				{targets: 0, title: 'ID', visible: false},
				{targets: 1, title: 'Ad'},
				{targets: 2, title: 'Konum'},

				{
					targets: -1,
					title: 'İşlem',
					orderable: false,
					width: '150px',
					render: function(data, type, full, meta) {
						return '\
						<a href="grounds/view/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_view_ground" title="Görüntüle">\
							<i class="fas fa-eye"></i>\
						</a>\
						<a href="grounds/edit/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_change_ground" title="Düzenle">\
							<i class="fas fa-edit"></i>\
						</a>\
						<a href="grounds/delete/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_delete_ground" title="Sil">\
							<i class="fas fa-trash-alt"></i>\
						</a>\
						<a href="grounds/hour/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_view_groundhour" title="Saatler">\
							<i class="fas fa-clock"></i>\
						</a>\
						';
					},
				},

			],
			drawCallback: function (settings) {
				viButGround();
		    }
		});

		$('#kt_search').on('click', function(e) {
			e.preventDefault();
			
			var params = {};
			$('.datatable-input').each(function() {
				//console.log($(this));
				var i = $(this).data('col-index');
				if (params[i]) {
					params[i] += '|' + $(this).val();
				}
				else {
					params[i] = $(this).val();
				}
			});
			$.each(params, function(i, val) {
				// apply search params to datatable
				table.column(i).search(val ? val : '', false, false);
			});

			table.table().draw();
		});

		$('#kt_reset').on('click', function(e) {
			e.preventDefault();
			$('.datatable-input').each(function() {
				if($(this).prop("tagName") == 'INPUT'){
					$(this).val('');
				}else if($(this).prop("tagName") == 'SELECT'){
					$(this).val(null).change();
				}
				
				table.column($(this).data('col-index')).search('', false, false);
			});

			table.table().draw();
		});
	};

	var initTableGroundProperties = function() {

		// begin first table
		var table = $('#kt_datatable_ground_properties').DataTable({

			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,
			//stateSave: true,

			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[0, 'desc']],
            language: {
                "lengthMenu": "Her sayfada _MENU_ kayıt göster",
                "info": "Sayfa: _PAGE_/_PAGES_ (_MAX_ kayıt filtrelendi)",
                "infoEmpty": "Kayıt Yok.",
                "zeroRecords": "Kayıt Yok.",
                "infoFiltered": "(Toplam _MAX_ kayıttan filtrelendi)",
                "processing": "Yükleniyor...",
                "search": "Arama:",
            },

			ajax: {
				url: HOST_URL + '/getGroundProperties',
				type: 'POST',
				headers: { 'X-CSRF-TOKEN': $('#token').val() },
				data: {
					columnsDef: ['id', 'name', 'actions'],
				},
			},
			columns: [
				{name: 'id'},
				{name: 'name'},
				{name: 'actions', responsivePriority: -1},
			],
			columnDefs: [

				{targets: 0, title: 'ID', visible: false},
				{targets: 1, title: 'Ad'},

				{
					targets: -1,
					title: 'İşlem',
					orderable: false,
					width: '150px',
					render: function(data, type, full, meta) {
						return '\
						<a href="ground_properties/view/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_view_ground btn_view_groundproperty" title="Görüntüle">\
							<i class="fas fa-eye"></i>\
						</a>\
						<a href="ground_properties/edit/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_change_ground btn_change_groundproperty" title="Düzenle">\
							<i class="fas fa-edit"></i>\
						</a>\
						<a href="ground_properties/delete/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_delete_ground btn_delete_groundproperty" title="Sil">\
							<i class="fas fa-trash-alt"></i>\
						</a>\
						';
					},
				},

			],
			drawCallback: function (settings) {
				viButGroundproperty();
		    }
		});

		$('#kt_search').on('click', function(e) {
			e.preventDefault();
			
			var params = {};
			$('.datatable-input').each(function() {
				//console.log($(this));
				var i = $(this).data('col-index');
				if (params[i]) {
					params[i] += '|' + $(this).val();
				}
				else {
					params[i] = $(this).val();
				}
			});
			$.each(params, function(i, val) {
				// apply search params to datatable
				table.column(i).search(val ? val : '', false, false);
			});

			table.table().draw();
		});

		$('#kt_reset').on('click', function(e) {
			e.preventDefault();
			$('.datatable-input').each(function() {
				if($(this).prop("tagName") == 'INPUT'){
					$(this).val('');
				}else if($(this).prop("tagName") == 'SELECT'){
					$(this).val(null).change();
				}
				
				table.column($(this).data('col-index')).search('', false, false);
			});

			table.table().draw();
		});
	};

	var initTableGroundHour = function() {

		var gunler = ['Pazartesi', 'Salı', 'Çarşamba', 'Perşembe', 'Cuma', 'Cumartesi', 'Pazar'];
		var checkGen = function(check_si, check_i, datas){
			//console.log(check_si+' - '+check_i+' - '+datas);

			var _gtsi = "";
			var _gtsis = "";
			var _gti = "";

			if(check_si == 'checked'){
				_gti = 'style="display:none;"';
			}else{
				_gtsis = 'style="display:none;"';
			}
			
			return '\
				<div class="checkbox-inline">\
					<label class="checkbox checkbox-outline checkbox-primary checkbox-lg">\
					<input type="checkbox" '+check_si+' class="ground_hour_cb btn_change_groundhour" data-type="ground-table-subscription-item" data-day="'+datas[0]+'" data-time="'+datas[1]+'" data-d="'+datas[2]+'" >\
					<span></span><div class="subsLabel" '+_gtsis+'>Abone</div></label>\
				</div>\
				<span class="switch switch-sm switch-outline switch-primary" '+_gti+'>\
				    <label>\
					    <input type="checkbox" '+check_i+' class="ground_hour_cb btn_change_groundhour" data-type="ground-table-item" data-day="'+datas[0]+'" data-time="'+datas[1]+'" data-d="'+datas[2]+'"/>\
					    <span></span>\
				    </label>\
				</span>';
		}

		var setGroundHourAjaxData = function(__type, __day, __time, __d, __val){
			//console.log(__type+' - '+__day+' - '+__time+' - '+__d+' - '+__val);
			
			$.ajax({
                method: 'POST',
                url : '/setGroundHour',
                dataType: 'json',
                headers: { 'X-CSRF-TOKEN': $('#token').val() },
				data: {
					week_start_date: $('#select2_week_start_date').val(),
					ground_id: $('#ground_id').val(),
					type: __type,
					day: __day,
					time: __time,
					d: __d,
					val: __val
				},
                success: function(data, textStatus, jqXHR)
                {	
                	$(function() {
	                    if(data.status == "success"){
	                    	toastr.options = {"closeButton": true,"positionClass": "toast-bottom-right"}
	                    	toastr.success(data.text)	
	                    }
	                });
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
             
                }
            });
            
        }

		// begin first table
		var table = $('#kt_datatable_ground_hour').DataTable({
			responsive: false,
			searchDelay: 500,
			processing: true,
			serverSide: true,
			//stateSave: true,
			paging:   false,
			ordering: false,
			info: false,

			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: ["All"],
            pageLength: 9999,
            //order: [[0, 'desc']],
            language: {
                "lengthMenu": "Her sayfada _MENU_ kayıt göster",
                "info": "Sayfa: _PAGE_/_PAGES_ (_MAX_ kayıt filtrelendi)",
                "infoEmpty": "Kayıt Yok.",
                "zeroRecords": "Kayıt Yok.",
                "infoFiltered": "(Toplam _MAX_ kayıttan filtrelendi)",
                "processing": "Yükleniyor...",
                "search": "Arama:",
            },
			ajax: {
				url: HOST_URL + '/getGroundHour',
				type: 'POST',
				headers: { 'X-CSRF-TOKEN': $('#token').val() },
				data: function(data){
					data.columnsDef = ['sort_id', 'time', 'day1', 'day2', 'day3', 'day4', 'day5', 'day6', 'day7'];
					data.ground_id = $('#ground_id').val();
					data.select2_week_start_date = $('#select2_week_start_date').val();
			    }
			},
			columns: [
				{name: 'sort_id'},
				{name: 'time'},
				{name: 'day1'},
				{name: 'day2'},
				{name: 'day3'},
				{name: 'day4'},
				{name: 'day5'},
				{name: 'day6'},
				{name: 'day7'},
			],
			columnDefs: [
				{targets: 0, orderable: false, visible: false},
				{targets: 1, orderable: false, className: "dt-center"},
				{targets: 2, orderable: false, className: "dt-center",
					render: function(data, type, full, meta) {
						//console.log(full);
						var check_si_arr = '';
						var check_i_arr = '';
						var datas = new Array();

						var d = data.split('|');

						if(d[1] == 1){
							check_si_arr = 'checked';
						}

						if(d[2] == 1){
							check_i_arr = 'checked';
						}

						datas[0] = d[0];
						datas[1] = full[1];
						datas[2] = full[2];
						
						if (typeof data === 'undefined') {
							return data;
						}

						return checkGen(check_si_arr, check_i_arr, datas);
					}
				},
				{targets: 3, orderable: false, className: "dt-center",
					render: function(data, type, full, meta) {
						var check_si_arr = '';
						var check_i_arr = '';
						var datas = new Array();

						var d = data.split('|');

						if(d[1] == 1){
							check_si_arr = 'checked';
						}

						if(d[2] == 1){
							check_i_arr = 'checked';
						}

						datas[0] = d[0];
						datas[1] = full[1];
						datas[2] = full[2];
						
						if (typeof data === 'undefined') {
							return data;
						}

						return checkGen(check_si_arr, check_i_arr, datas);
					}
				},
				{targets: 4, orderable: false, className: "dt-center",
					render: function(data, type, full, meta) {
						var check_si_arr = '';
						var check_i_arr = '';
						var datas = new Array();

						var d = data.split('|');

						if(d[1] == 1){
							check_si_arr = 'checked';
						}

						if(d[2] == 1){
							check_i_arr = 'checked';
						}

						datas[0] = d[0];
						datas[1] = full[1];
						datas[2] = full[2];
						
						if (typeof data === 'undefined') {
							return data;
						}

						return checkGen(check_si_arr, check_i_arr, datas);
					}
				},
				{targets: 5, orderable: false, className: "dt-center",
					render: function(data, type, full, meta) {
						var check_si_arr = '';
						var check_i_arr = '';
						var datas = new Array();

						var d = data.split('|');

						if(d[1] == 1){
							check_si_arr = 'checked';
						}

						if(d[2] == 1){
							check_i_arr = 'checked';
						}

						datas[0] = d[0];
						datas[1] = full[1];
						datas[2] = full[2];
						
						if (typeof data === 'undefined') {
							return data;
						}

						return checkGen(check_si_arr, check_i_arr, datas);
					}
				},
				{targets: 6, orderable: false, className: "dt-center",
					render: function(data, type, full, meta) {
						var check_si_arr = '';
						var check_i_arr = '';
						var datas = new Array();

						var d = data.split('|');

						if(d[1] == 1){
							check_si_arr = 'checked';
						}

						if(d[2] == 1){
							check_i_arr = 'checked';
						}

						datas[0] = d[0];
						datas[1] = full[1];
						datas[2] = full[2];
						
						if (typeof data === 'undefined') {
							return data;
						}

						return checkGen(check_si_arr, check_i_arr, datas);
					}
				},
				{targets: 7, orderable: false, className: "dt-center",
					render: function(data, type, full, meta) {
						var check_si_arr = '';
						var check_i_arr = '';
						var datas = new Array();

						var d = data.split('|');

						if(d[1] == 1){
							check_si_arr = 'checked';
						}

						if(d[2] == 1){
							check_i_arr = 'checked';
						}

						datas[0] = d[0];
						datas[1] = full[1];
						datas[2] = full[2];
						
						if (typeof data === 'undefined') {
							return data;
						}

						return checkGen(check_si_arr, check_i_arr, datas);
					}
				},
				{targets: 8, orderable: false, className: "dt-center",
					render: function(data, type, full, meta) {
						var check_si_arr = '';
						var check_i_arr = '';
						var datas = new Array();

						var d = data.split('|');

						if(d[1] == 1){
							check_si_arr = 'checked';
						}

						if(d[2] == 1){
							check_i_arr = 'checked';
						}

						datas[0] = d[0];
						datas[1] = full[1];
						datas[2] = full[2];
						
						if (typeof data === 'undefined') {
							return data;
						}

						return checkGen(check_si_arr, check_i_arr, datas);
					}
				}
			],
			drawCallback: function (settings) {
				viButGroundhour();
		    }
		}).on('xhr.dt', function ( e, settings, json, xhr ) {
	        for (var i = 0; i < json.colName.length; i++) {
	        	$(table.column(i+2).header()).html(json.colName[i]+'</br>'+gunler[i]);
	        }
	    });


		$('#kt_datatable_ground_hour').on( 'change', 'input.ground_hour_cb', function (e) {
			
			e.preventDefault();
			var __type = $( this ).attr('data-type');
			var __day = $( this ).attr('data-day');
			var __time = $( this ).attr('data-time');
			var __d = $( this ).attr('data-d');

			if($(this).attr('data-type') == 'ground-table-subscription-item'){
				if($(this).prop("checked") == true){
					//console.log('check');
					$( "input[data-type='ground-table-subscription-item'][data-day='"+$( this ).attr('data-day')+"'][data-time='"+$( this ).attr('data-time')+"']" ).parent().find('.subsLabel').show();
					$( "input[data-type='ground-table-item'][data-day='"+$( this ).attr('data-day')+"'][data-time='"+$( this ).attr('data-time')+"']" ).prop('checked', false);
					$( "input[data-type='ground-table-item'][data-day='"+$( this ).attr('data-day')+"'][data-time='"+$( this ).attr('data-time')+"']" ).parent().parent().hide();
				}else{
					//console.log('un check');
					$( "input[data-type='ground-table-subscription-item'][data-day='"+$( this ).attr('data-day')+"'][data-time='"+$( this ).attr('data-time')+"']" ).parent().find('.subsLabel').hide();
					$( "input[data-type='ground-table-item'][data-day='"+$( this ).attr('data-day')+"'][data-time='"+$( this ).attr('data-time')+"']" ).parent().parent().show();
				}
			}else if($(this).attr('data-type') == 'ground-table-item'){

			}

			var __val = $( "input[data-type='"+__type+"'][data-day='"+__day+"'][data-time='"+__time+"']" ).prop('checked');
			setGroundHourAjaxData(__type, __day, __time, __d, __val);
			
		});

		$('#kt_search').on('click', function(e) {
			e.preventDefault();
			
			var params = {};
			$('.datatable-input').each(function() {
				//console.log($(this));
				var i = $(this).data('col-index');
				if (params[i]) {
					params[i] += '|' + $(this).val();
				}
				else {
					params[i] = $(this).val();
				}
			});
			$.each(params, function(i, val) {
				// apply search params to datatable
				table.column(i).search(val ? val : '', false, false);
			});

			table.table().draw();
		});

		$('#kt_reset').on('click', function(e) {
			e.preventDefault();
			$('.datatable-input').each(function() {
				if($(this).prop("tagName") == 'INPUT'){
					$(this).val('');
				}else if($(this).prop("tagName") == 'SELECT'){
					$(this).val(null).change();
				}
				
				table.column($(this).data('col-index')).search('', false, false);
			});

			table.table().draw();
		});
	};

	var initTableLeagues = function() {

		var table = $('#kt_datatable_leagues').DataTable({

			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,
			//stateSave: true,

			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[0, 'desc']],
            language: {
                "lengthMenu": "Her sayfada _MENU_ kayıt göster",
                "info": "Sayfa: _PAGE_/_PAGES_ (_MAX_ kayıt filtrelendi)",
                "infoEmpty": "Kayıt Yok.",
                "zeroRecords": "Kayıt Yok.",
                "infoFiltered": "(Toplam _MAX_ kayıttan filtrelendi)",
                "processing": "Yükleniyor...",
                "search": "Arama:",
            },

			ajax: {
				url: HOST_URL + '/getLeagues',
				type: 'POST',
				headers: { 'X-CSRF-TOKEN': $('#token').val() },
				data: {
					columnsDef: ['id', 'name', 'province', 'active', 'actions'],
				},
			},
			columns: [
				{name: 'id'},
				{name: 'name'},
				{name: 'province'},
				{name: 'active'},
				{name: 'actions', responsivePriority: -1},
			],
			columnDefs: [

				{targets: 0, title: 'ID', visible: false},
				{targets: 1, title: 'Ad'},
				{targets: 2, title: 'Şehir', orderable: false},
				{
					targets: 3, 
					title: 'Aktif',
					render: function(data, type, full, meta) {

						var active_status = {
							'true': {'state': 'fa-check-circle', 'color': 'text-success'},
							'false': {'state': 'fa-times-circle', 'color': 'text-danger'},
						};
						
						if (typeof data === 'undefined') {
							//console.log('bbb undefined');
							return data;
						}

						return '<i class="fas ' + active_status[data].state + ' ' + active_status[data].color + ' "></i>';
					},
				},
				{
					targets: -1,
					title: 'İşlem',
					orderable: false,
					width: '150px',
					render: function(data, type, full, meta) {
						return '\
						<a href="leagues/view/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_view_league" title="Görüntüle">\
							<i class="fas fa-eye"></i>\
						</a>\
						<a href="leagues/edit/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_change_league" title="Düzenle">\
							<i class="fas fa-edit"></i>\
						</a>\
						<a href="leagues/delete/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_delete_league" title="Sil">\
							<i class="fas fa-trash-alt"></i>\
						</a>\
						<a href="league_gallery?league_id=' + full[0] + '" class="btn btn-sm btn-clean btn-icon btn_gallery_league" title="Lig Galerileri">\
							<i class="fas fa-photo-video"></i>\
						</a>\
						';
					},
				},

			],
			drawCallback: function (settings) {
				viButLeague();
		    }
		});

		$('#kt_search').on('click', function(e) {
			e.preventDefault();
			
			var params = {};
			$('.datatable-input').each(function() {
				//console.log($(this));
				var i = $(this).data('col-index');
				if (params[i]) {
					params[i] += '|' + $(this).val();
				}
				else {
					params[i] = $(this).val();
				}
			});
			$.each(params, function(i, val) {
				// apply search params to datatable
				table.column(i).search(val ? val : '', false, false);
			});

			table.table().draw();
		});

		$('#kt_reset').on('click', function(e) {
			e.preventDefault();
			$('.datatable-input').each(function() {
				if($(this).prop("tagName") == 'INPUT'){
					$(this).val('');
				}else if($(this).prop("tagName") == 'SELECT'){
					$(this).val(null).change();
				}
				
				table.column($(this).data('col-index')).search('', false, false);
			});

			table.table().draw();
		});
	};

	var initTableLeagueGalleries = function() {


		var queryString = '';
		if($('#league_id').val() != ""){
			queryString = '?league_id='+$('#league_id').val();
		}

		var table = $('#kt_datatable_league_galleries').DataTable({

			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,
			//stateSave: true,

			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[1, 'desc']],
            language: {
                "lengthMenu": "Her sayfada _MENU_ kayıt göster",
                "info": "Sayfa: _PAGE_/_PAGES_ (_MAX_ kayıt filtrelendi)",
                "infoEmpty": "Kayıt Yok.",
                "zeroRecords": "Kayıt Yok.",
                "infoFiltered": "(Toplam _MAX_ kayıttan filtrelendi)",
                "processing": "Yükleniyor...",
                "search": "Arama:",
            },

			ajax: {
				url: HOST_URL + '/getLeagueGalleries',
				type: 'POST',
				headers: { 'X-CSRF-TOKEN': $('#token').val() },
				data: function(data){
					data.columnsDef = ['id', 'name', 'league', 'show_home', 'actions'];
					data.league_id = $('#league_id').val();
			    },
			},
			columns: [
				{name: 'id'},
				{name: 'name'},
				{name: 'league'},
				{name: 'show_home'},
				{name: 'actions', responsivePriority: -1},
			],
			columnDefs: [

				{targets: 0, title: 'ID', orderable: false, visible: false},
				{targets: 1, title: 'Ad'},
				{targets: 2, title: 'Lig', orderable: false},
				{
					targets: 3, 
					title: 'Ana Sayfada Göster',
					render: function(data, type, full, meta) {

						var active_status = {
							'true': {'state': 'fa-check-circle', 'color': 'text-success'},
							'false': {'state': 'fa-times-circle', 'color': 'text-danger'},
						};
						
						if (typeof data === 'undefined') {
							//console.log('bbb undefined');
							return data;
						}

						return '<i class="fas ' + active_status[data].state + ' ' + active_status[data].color + ' "></i>';
					},
				},
				{
					targets: -1,
					title: 'İşlem',
					orderable: false,
					width: '150px',
					render: function(data, type, full, meta) {
						return '\
						<a href="league_gallery/view/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_view_league" title="Görüntüle">\
							<i class="fas fa-eye"></i>\
						</a>\
						<a href="league_gallery/edit/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_change_league" title="Düzenle">\
							<i class="fas fa-edit"></i>\
						</a>\
						<a href="league_gallery/delete/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_delete_league" title="Sil">\
							<i class="fas fa-trash-alt"></i>\
						</a>\
						';
					},
				},

			],
			drawCallback: function (settings) {
				viButLeague();
		    }
		});

		$('#kt_search').on('click', function(e) {
			e.preventDefault();
			
			var params = {};
			$('.datatable-input').each(function() {
				//console.log($(this));
				var i = $(this).data('col-index');
				if (params[i]) {
					params[i] += '|' + $(this).val();
				}
				else {
					params[i] = $(this).val();
				}
			});
			$.each(params, function(i, val) {
				// apply search params to datatable
				table.column(i).search(val ? val : '', false, false);
			});

			table.table().draw();
		});

		$('#kt_reset').on('click', function(e) {
			e.preventDefault();
			$('.datatable-input').each(function() {
				if($(this).prop("tagName") == 'INPUT'){
					$(this).val('');
				}else if($(this).prop("tagName") == 'SELECT'){
					$(this).val(null).change();
				}
				
				table.column($(this).data('col-index')).search('', false, false);
			});

			table.table().draw();
		});
	};

	var initTableLeagueGalleryItems = function() {

		// begin first table
		var table = $('#kt_datatable_league_gallery_items').DataTable({
			responsive: true,
			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[0, 'desc']],
            language: {
                "lengthMenu": "Her sayfada _MENU_ kayıt göster",
                "info": "Sayfa: _PAGE_/_PAGES_ (_MAX_ kayıt filtrelendi)",
                "infoEmpty": "Kayıt Yok.",
                "zeroRecords": "Kayıt Yok.",
                "infoFiltered": "(Toplam _MAX_ kayıttan filtrelendi)",
                "processing": "Yükleniyor...",
                "search": "Arama:",
            },
			columnDefs: [
				//{targets: 0, visible: false},
				{targets: -1, orderable: false},
			],
			
		});

		$('.league_gallery_item_edit_button').on('click', function(e) {
			e.preventDefault();
		    var target = $(this).attr('href');
		    $('html, body').animate({
		    	scrollTop: ($('#league_gallery_itemsForm').offset().top)
		    }, 1000);

			$.ajax({
                method: 'GET',
                url : '/getLeagueGalleryItemF/'+table.row(this.id).data()[0],
                dataType: 'json',
                success: function(data, textStatus, jqXHR)
                {	

                	$("#league_gallery_itemsForm input[name=league_gallery_items_id]").val(data.items['league_gallery_items_id']);

                	$("#league_gallery_itemsForm select[name=embed_source]").val(data.items['embed_source']).trigger('change');
                	$("#league_gallery_itemsForm input[name=embed_code]").val(data.items['embed_code']);
                	$("#league_gallery_itemsForm textarea[name=description]").val(data.items['description']);

                },
                error: function (jqXHR, textStatus, errorThrown)
                {

                }
            });
		});
	};

	var initTableChampions = function() {

		var table = $('#kt_datatable_champions').DataTable({

			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,
			//stateSave: true,

			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[1, 'desc']],
            language: {
                "lengthMenu": "Her sayfada _MENU_ kayıt göster",
                "info": "Sayfa: _PAGE_/_PAGES_ (_MAX_ kayıt filtrelendi)",
                "infoEmpty": "Kayıt Yok.",
                "zeroRecords": "Kayıt Yok.",
                "infoFiltered": "(Toplam _MAX_ kayıttan filtrelendi)",
                "processing": "Yükleniyor...",
                "search": "Arama:",
            },

			ajax: {
				url: HOST_URL + '/getChampions',
				type: 'POST',
				headers: { 'X-CSRF-TOKEN': $('#token').val() },
				data: function(data){
					data.columnsDef = ['id', 'season', 'team', 'year', 'point', 'actions'];
					data.leagueId = $('#select2_league_search').val();
			    }
			},
			columns: [
				{name: 'id'},
				{name: 'season'},
				{name: 'team'},
				{name: 'year'},
				{name: 'point'},
				{name: 'actions', responsivePriority: -1},
			],
			columnDefs: [

				{targets: 0, title: 'ID', visible: false},
				{targets: 1, title: 'Sezon'},
				{targets: 2, title: 'Takım'},
				{targets: 3, title: 'Yıl'},
				{targets: 4, title: 'Puan', orderable: false},
				{
					targets: -1,
					title: 'İşlem',
					orderable: false,
					width: '150px',
					render: function(data, type, full, meta) {
						return '\
						<a href="champions/view/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_view_league" title="Görüntüle">\
							<i class="fas fa-eye"></i>\
						</a>\
						<a href="champions/edit/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_change_league" title="Düzenle">\
							<i class="fas fa-edit"></i>\
						</a>\
						<a href="champions/delete/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_delete_league" title="Sil">\
							<i class="fas fa-trash-alt"></i>\
						</a>\
						';
					},
				},

			],
			drawCallback: function (settings) {
				viButLeague();
		    }
		});

		$('#kt_search').on('click', function(e) {
			e.preventDefault();
			
			var params = {};
			$('.datatable-input').each(function() {
				//console.log($(this));
				var i = $(this).data('col-index');
				if (params[i]) {
					params[i] += '|' + $(this).val();
				}
				else {
					params[i] = $(this).val();
				}
			});
			$.each(params, function(i, val) {
				// apply search params to datatable
				table.column(i).search(val ? val : '', false, false);
			});

			table.table().draw();
		});

		$('#kt_reset').on('click', function(e) {
			e.preventDefault();
			$('.datatable-input').each(function() {
				if($(this).prop("tagName") == 'INPUT'){
					$(this).val('');
				}else if($(this).prop("tagName") == 'SELECT'){
					$(this).val(null).change();
				}
				
				table.column($(this).data('col-index')).search('', false, false);
			});

			table.table().draw();
		});
	};

	var initTableSeasons = function() {

		// begin first table
		var table = $('#kt_datatable_seasons').DataTable({

			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,
			//stateSave: true,

			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[0, 'desc']],
            language: {
                "lengthMenu": "Her sayfada _MENU_ kayıt göster",
                "info": "Sayfa: _PAGE_/_PAGES_ (_MAX_ kayıt filtrelendi)",
                "infoEmpty": "Kayıt Yok.",
                "zeroRecords": "Kayıt Yok.",
                "infoFiltered": "(Toplam _MAX_ kayıttan filtrelendi)",
                "processing": "Yükleniyor...",
                "search": "Arama:",
            },

			ajax: {
				url: HOST_URL + '/getSeasons',
				type: 'POST',
				headers: { 'X-CSRF-TOKEN': $('#token').val() },
				data: {
					columnsDef: ['id', 'league', 'year', 'name', 'polymorphic_ctype_id', 'start_date', 'end_date', 'actions'],
				},
			},
			columns: [
				{name: 'id'},
				{name: 'league'},
				{name: 'year'},
				{name: 'name'},
				{name: 'polymorphic_ctype_id'},
				{name: 'start_date'},
				{name: 'end_date'},
				{name: 'actions', responsivePriority: -1},
			],
			columnDefs: [

				{targets: 0, title: 'ID', visible: false},
				{
					targets: 1,
					title: 'Lig',
					render: function(data, type, full, meta) {
						var returnValue = '';
						var sdata = data.split("::");

						return sdata[0];
					},
				},
				{targets: 2, title: 'Yıl'},
				{targets: 3, title: 'Ad'},
				{targets: 4, title: 'Tip'},
				{targets: 5, title: 'Başlangıç'},
				{targets: 6, title: 'Bitiş'},
				{
					targets: -1,
					title: 'İşlem',
					orderable: false,
					width: '150px',
					render: function(data, type, full, meta) {

						var returnValue = '';
						var sdata = full[1].split("::")[1];

						console.log(sdata);

						var ss = '';
						var etMenuItem = '';

						if(full[4] == 'pointseason'){
							ss = 'p';
							if(sdata == 26 || sdata == 37){
								etMenuItem = '\
								<a href="seasonsettings/edit/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_view_seasonsettings" title="Sezon Ayarları">\
									<i class="fas fa-trophy"></i>\
								</a>\
								';
							}
						}else if(full[4] == 'fixtureseason'){
							ss = 'f';
							etMenuItem = '\
							<a href="fixture_group/?tree_id='+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_view_fixturegroup" title="Gruplar">\
								<i class="fas fa-layer-group"></i>\
							</a>\
							';
							if(sdata == 26 || sdata == 37){
								etMenuItem = etMenuItem+'\
								<a href="seasonsettings/edit/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_view_seasonsettings" title="Sezon Ayarları">\
									<i class="fas fa-trophy"></i>\
								</a>\
								';
							}
						}else if(full[4] == 'eliminationseason'){
							ss = 'e';
							etMenuItem = '\
							<a href="elimination_tree/?tree_id='+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_view_eliminationtree" title="Eleme Ağaçları">\
								<i class="fas fa-sitemap"></i>\
							</a>\
							';
						}

						return '\
						<a href="seasons'+ss+'/view/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_view_season btn_view_season_'+ss+'" title="Görüntüle">\
							<i class="fas fa-eye"></i>\
						</a>\
						<a href="seasons'+ss+'/edit/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_change_season btn_change_season_'+ss+'" title="Düzenle">\
							<i class="fas fa-edit"></i>\
						</a>\
						<a href="seasons'+ss+'/delete/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_delete_season btn_delete_season_'+ss+'" title="Sil">\
							<i class="fas fa-trash-alt"></i>\
						</a>'+etMenuItem;
						
					},
				},
			],
			drawCallback: function (settings) {
				viButSeason();
		    }
		});

		$('#kt_search').on('click', function(e) {
			e.preventDefault();
			
			var params = {};
			$('.datatable-input').each(function() {
				//console.log($(this));
				var i = $(this).data('col-index');
				if (params[i]) {
					params[i] += '|' + $(this).val();
				}
				else {
					params[i] = $(this).val();
				}
			});
			$.each(params, function(i, val) {
				// apply search params to datatable
				table.column(i).search(val ? val : '', false, false);
			});

			table.table().draw();
		});

		$('#kt_reset').on('click', function(e) {
			e.preventDefault();
			$('.datatable-input').each(function() {
				if($(this).prop("tagName") == 'INPUT'){
					$(this).val('');
				}else if($(this).prop("tagName") == 'SELECT'){
					$(this).val(null).change();
				}
				
				table.column($(this).data('col-index')).search('', false, false);
			});

			table.table().draw();
		});
	};

	var initTableSeasonsAddBonus = function() {

		// begin first table
		var table = $('#kt_datatable_seasons_addbonus').DataTable({

			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,
			//stateSave: true,

			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			paging: false,
			info: false,
            order: [[5, 'desc']],
            language: {
                "lengthMenu": "Her sayfada _MENU_ kayıt göster",
                "info": "Sayfa: _PAGE_/_PAGES_ (_MAX_ kayıt filtrelendi)",
                "infoEmpty": "Kayıt Yok.",
                "zeroRecords": "Kayıt Yok.",
                "infoFiltered": "(Toplam _MAX_ kayıttan filtrelendi)",
                "processing": "Yükleniyor...",
                "search": "Arama:",
            },

			ajax: {
				url: HOST_URL + '/getSeasonsAddBonus',
				type: 'POST',
				headers: { 'X-CSRF-TOKEN': $('#token').val() },
				data: {
					columnsDef: ['id', 'league', 'name', 'year', 'start_date', 'end_date', 'sumadded', 'month'],
				},
			},
			columns: [
				{name: 'id'},
				{name: 'league'},
				{name: 'name'},
				{name: 'year'},
				{name: 'start_date'},
				{name: 'end_date'},
				{name: 'sumadded'},
				{name: 'month'}
			],
			columnDefs: [

				{
					targets: 0, 
					title: '',
					width: '10px',
					orderable: false,
					render: function(data, type, full, meta) {
						console.log(full);
						return '<div class="form-group" style="margin: 0.5rem;">\
								    <div class="checkbox-list">\
								        <label class="checkbox">\
								            <input type="checkbox" name="seasonid[]" value="'+data+'">\
								            <span></span>\
								        </label>\
								    </div>\
								</div>';
					},

				},
				{targets: 1, title: 'Lig'},
				{targets: 2, title: 'Ad'},
				{targets: 3, title: 'Yıl'},
				{targets: 4, title: 'Başlangıç'},
				{targets: 5, title: 'Bitiş'},
				{targets: 6, title: 'T. Eklenen'},
				{targets: 7, title: 'Ay'}

			],
		});
	};

	var initTableTeams = function() {

        function setTeamsAjaxData(_selector, _id, _url){
			//console.log($(_selector).val());
			$.ajax({
                method: 'GET',
                url : _url+'/'+_id+'/'+$(_selector).val(),
                dataType: 'json',
                success: function(data, textStatus, jqXHR)
                {	
                	$(function() {
	                    if(data.status == "success"){
	                    	toastr.options = {"closeButton": true,"positionClass": "toast-bottom-right"}
	                    	toastr.success(data.text)	
	                    }
	                });
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
             
                }
            });
        }

		var select2Optioneee = { placeholder: "Seçiniz", language: "tr", width: 150 };
		// begin first table
		var table = $('#kt_datatable_teams').DataTable({
			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,
			//stateSave: true,

			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[0, 'desc']],
            language: {
                "lengthMenu": "Her sayfada _MENU_ kayıt göster",
                "info": "Sayfa: _PAGE_/_PAGES_ (_MAX_ kayıt filtrelendi)",
                "infoEmpty": "Kayıt Yok.",
                "zeroRecords": "Kayıt Yok.",
                "infoFiltered": "(Toplam _MAX_ kayıttan filtrelendi)",
                "processing": "Yükleniyor...",
                "search": "Arama:",
            },
			ajax: {
				url: HOST_URL + '/getTeams',
				type: 'POST',
				headers: { 'X-CSRF-TOKEN': $('#token').val() },
				data: {
					columnsDef: ['id', 'name', 'province_id', 'district_id', 'captain_id', 'phone', 'created_at', 'potential', 'status', 'ms', 'ks', 'actions'],
				},
			},
			columns: [
				{name: 'id'},
				{name: 'name'},
				{name: 'province_id'},
				{name: 'district_id'},
				{name: 'captain_id'},
				{name: 'phone'},
				{name: 'created_at'},
				{name: 'potential', responsivePriority: -1},
				{name: 'status'},
				{name: 'ms'},
				{name: 'ks'},
				{name: 'actions', responsivePriority: -1},
			],
			columnDefs: [
				{targets: 0, title: 'ID', visible: false},
				{targets: 1, title: 'Ad'},
				{targets: 2, title: 'Şehir'},
				{targets: 3, title: 'İlçe'},
				{targets: 4, title: 'Kaptan'},
				{targets: 5, title: 'Telefon'},
				{targets: 6, title: 'Oluşturulma'},
				{
					targets: 7, 
					title: 'Potansiyel - Memnuniyet - Jogo Memnuniyet',
					render: function(data, type, full, meta) {

						var returnValue = '';
						var sdata = data.split("::");

						returnValue += '<select class="form-control select2" id="select2_potential_dt_'+full[0]+'" name="select2_potential_dt_'+full[0]+'"><option></option>';
							$.each(_potential, function(i, val) {
								if(val != undefined){
									returnValue += '<option value="'+i+'"';
									if(i == sdata[0]){
										returnValue += ' selected="selected" ';
									}
									returnValue += '>'+val+'</option>';
								}
							});
		                returnValue += '</select><br />';
		                
						returnValue += '<select class="form-control select2" id="select2_satisfaction_dt_'+full[0]+'" name="select2_satisfaction_dt_'+full[0]+'"><option></option>';
							$.each(_satisfaction, function(i, val) {
								if(val != undefined){
									//console.log(i+' - '+val);
									returnValue += '<option value="'+i+'"';
									if(i == sdata[1]){
										returnValue += ' selected="selected" ';
									}
									returnValue += '>'+val+'</option>';
								}
							});
		                returnValue += '</select><br />';

		                returnValue += '<select class="form-control select2" id="select2_satisfaction_jogo_dt_'+full[0]+'" name="select2_satisfaction_jogo_dt_'+full[0]+'"><option></option>';
							$.each(_satisfaction_jogo, function(i, val) {
								if(val != undefined){
									//console.log(i+' - '+val);
									returnValue += '<option value="'+i+'"';
									if(i == sdata[2]){
										returnValue += ' selected="selected" ';
									}
									returnValue += '>'+val+'</option>';
								}
							});
		                returnValue += '</select>';

						return returnValue;
					},
				},
				{
					targets: 8, 
					title: 'Fesih',
					orderable: false,
					render: function(data, type, full, meta) {

						var active_status = {
							'active': {'state': 'fa-check-circle', 'color': 'text-success', 'text': 'Aktif'},
							'termination': {'state': 'fa-exclamation-circle', 'color': 'text-warning', 'text': 'Fesih İsteği'},
							'passive': {'state': 'fa-times-circle', 'color': 'text-danger', 'text': 'Feshedildi'},
						};
						//status
				            //"active" 
				            //"termination" // Fesih talebi gönderilmiş
				            //"passive" // Feshedilmiş
						if (typeof data === 'undefined') {
							//console.log('bbb undefined');
							return data;
						}

						return '<i data-trigger="hover" data-toggle="tooltip" title="' + active_status[data].text + '" class="fas ' + active_status[data].state + ' ' + active_status[data].color + ' "></i>';
					},
				},
				{targets: 9, title: 'Maç S.', orderable: false},
				{targets: 10, title: 'Kadro S.', orderable: false},
				{
					targets: -1,
					title: 'İşlem',
					orderable: false,
					width: '150px',
					render: function(data, type, full, meta) {
						return '\
						<a href="teams/view/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_view_team" title="Görüntüle">\
							<i class="fas fa-eye"></i>\
						</a>\
						<a href="teams/edit/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_change_team" title="Düzenle">\
							<i class="fas fa-edit"></i>\
						</a>\
						<a href="teams/delete/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_delete_team" title="Sil">\
							<i class="fas fa-trash-alt"></i>\
						</a>\
						';
					},
				},
			],
			drawCallback: function (settings) {
				viButTeam();
		      	$('[data-toggle="tooltip"]').tooltip();
		    }
		}).on('draw', function () {
		    $.each(table.column(0).data(), function(i, val) {
				$('#select2_potential_dt_'+val).select2(select2Optioneee).on('change', function (e) {
	            	//console.log('#select2_potential_dt_'+val);
	            	setTeamsAjaxData('#select2_potential_dt_'+val, val, 'setTeamsP');
	            });

				$('#select2_satisfaction_dt_'+val).select2(select2Optioneee).on('change', function (e) {
	            	//console.log('#select2_satisfaction_dt_'+val);
	            	setTeamsAjaxData('#select2_satisfaction_dt_'+val, val, 'setTeamsS');
	            });

				$('#select2_satisfaction_jogo_dt_'+val).select2(select2Optioneee).on('change', function (e) {
	            	//console.log('#select2_satisfaction_jogo_dt_'+val);
	            	setTeamsAjaxData('#select2_satisfaction_jogo_dt_'+val, val, 'setTeamsSJ');
	            });
			});
		});

		$('#kt_search').on('click', function(e) {
			e.preventDefault();

			$('#potential').val($('#select2_potential_search').val()+'|'+$('#select2_satisfaction_search').val()+'|'+$('#select2_satisfaction_jogo_search').val());

			$('#ms').val($('#msmin').val()+'|'+$('#msmax').val()+'|'+$('#daterange_start_search > input').val()+'|'+$('#daterange_end_search > input').val());
			$('#ks').val($('#ksmin').val()+'|'+$('#ksmax').val());

			var params = {};
			$('.datatable-input').each(function() {
				//console.log($(this));
				var i = $(this).data('col-index');
				if (params[i]) {
					params[i] += '|' + $(this).val();
				}
				else {
					params[i] = $(this).val();
				}
			});
			$.each(params, function(i, val) {
				// apply search params to datatable
				table.column(i).search(val ? val : '', false, false);
			});

			table.table().draw();
		});

		$('#kt_reset').on('click', function(e) {
			e.preventDefault();
			$('.datatable-input').each(function() {
				if($(this).prop("tagName") == 'INPUT'){
					$(this).val('');
				}else if($(this).prop("tagName") == 'SELECT'){
					$(this).val(null).change();
				}
				
				table.column($(this).data('col-index')).search('', false, false);
			});

			table.table().draw();
		});
	};

	var initTableTeams_Seasons = function() {

		// begin first table
		var table = $('#kt_datatable_teams_seasons').DataTable({
			responsive: true,
			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[0, 'desc']],
            language: {
                "lengthMenu": "Her sayfada _MENU_ kayıt göster",
                "info": "Sayfa: _PAGE_/_PAGES_ (_MAX_ kayıt filtrelendi)",
                "infoEmpty": "Kayıt Yok.",
                "zeroRecords": "Kayıt Yok.",
                "infoFiltered": "(Toplam _MAX_ kayıttan filtrelendi)",
                "processing": "Yükleniyor...",
                "search": "Arama:",
            },
			columnDefs: [
				{targets: 0, visible: false},
				{targets: -1, orderable: false},
			],
			
		});
	};

	var initTableTeams_Players = function() {

		// begin first table
		var table = $('#kt_datatable_teams_players').DataTable({
			responsive: true,
			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[0, 'desc']],
            language: {
                "lengthMenu": "Her sayfada _MENU_ kayıt göster",
                "info": "Sayfa: _PAGE_/_PAGES_ (_MAX_ kayıt filtrelendi)",
                "infoEmpty": "Kayıt Yok.",
                "zeroRecords": "Kayıt Yok.",
                "infoFiltered": "(Toplam _MAX_ kayıttan filtrelendi)",
                "processing": "Yükleniyor...",
                "search": "Arama:",
            },
			columnDefs: [
				{targets: 0, visible: false},
				{targets: -1, orderable: false},
			],
			
		});
	};

	var initTableMatches = function() {

		var select2Optioneee = { placeholder: "Seçiniz", language: "tr", width: 150 };
		// begin first table
		var table = $('#kt_datatable_matches').DataTable({
			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,
			//stateSave: true,

			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[0, 'desc']],
            language: {
                "lengthMenu": "Her sayfada _MENU_ kayıt göster",
                "info": "Sayfa: _PAGE_/_PAGES_ (_MAX_ kayıt filtrelendi)",
                "infoEmpty": "Kayıt Yok.",
                "zeroRecords": "Kayıt Yok.",
                "infoFiltered": "(Toplam _MAX_ kayıttan filtrelendi)",
                "processing": "Yükleniyor...",
                "search": "Arama:",
            },
			ajax: {
				url: HOST_URL + '/getMatches',
				type: 'POST',
				headers: { 'X-CSRF-TOKEN': $('#token').val() },
				data: function(data){
					data.columnsDef = ['id', 'season', 'team1_id', 'team2_id', 'date', 'time', 'videodurumu', 'completed', 'actions'];
					data.leagueId = $('#select2_league_search').val();
			    }
			},
			columns: [
				{name: 'id'},
				{name: 'season'},
				{name: 'team1_id'},
				{name: 'team2_id'},
				{name: 'date'},
				{name: 'time'},
				{name: 'videodurumu'},
				{name: 'completed'},
				{name: 'actions', responsivePriority: -1}
			],
			columnDefs: [
				{targets: 0, title: 'ID', visible: false},
				{targets: 1, title: 'Sezon'},
				{targets: 2, title: 'Birinci Takım'},
				{targets: 3, title: 'İkinci Takım'},
				{targets: 4, title: 'Tarih'},
				{targets: 5, title: 'Saat'},
				{
					targets: 6, 
					title: 'Vid. D.',
					orderable: false,
					render: function(data, type, full, meta) {

						var video_status = {
							'recordable': {'state': 'fa-video-slash', 'color': 'text-danger', 'text': 'Kayıt Edilebilir'},
							'recording': {'state': 'fa-video', 'color': 'text-info', 'text': 'Kayıt Ediliyor'},
							'recorded': {'state': 'fa-video', 'color': 'text-success', 'text': 'Kayıt Edildi'},
							'finished': {'state': 'fa-video', 'color': 'text-warning', 'text': 'Tamamlandı'},
						};
						if (typeof data === 'undefined') {
							return data;
						}
						return '<i data-trigger="hover" data-toggle="tooltip" title="' + video_status[data].text + '" class="fas ' + video_status[data].state + ' ' + video_status[data].color + ' "></i>';
					},
				},
				{
					targets: 7, 
					title: '',
					render: function(data, type, full, meta) {

						var active_status = {
							'true': {'state': 'fa-check-circle', 'color': 'text-success', 'text': 'Tamamlandı'},
							'false': {'state': 'fa-times-circle', 'color': 'text-danger', 'text': 'Tamamlanmadı'},
						};
						if (typeof data === 'undefined') {
							return data;
						}
						return '<i data-trigger="hover" data-toggle="tooltip" title="' + active_status[data].text + '" class="fas ' + active_status[data].state + ' ' + active_status[data].color + ' "></i>';
					},
				},
				{
					targets: -1,
					title: 'İşlem',
					orderable: false,
					width: '150px',
					render: function(data, type, full, meta) {
						return '\
						<a href="matches/view/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_view_match" title="Görüntüle">\
							<i class="fas fa-eye"></i>\
						</a>\
						<a href="matches/edit/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_change_match" title="Düzenle">\
							<i class="fas fa-edit"></i>\
						</a>\
						<a href="matches/delete/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_delete_match" title="Sil">\
							<i class="fas fa-trash-alt"></i>\
						</a>\
						';
					},
				}
			],
			drawCallback: function (settings) {
				viButMatch();
		      	$('[data-toggle="tooltip"]').tooltip();
		    }
		});

		$('#kt_search').on('click', function(e) {
			
			e.preventDefault();
            $('#daterange_dates_search').val($('#daterange_start_search > input').val()+'::'+$('#daterange_end_search > input').val());

			var params = {};
			$('.datatable-input').each(function() {
				//console.log($(this));
				var i = $(this).data('col-index');
				if (params[i]) {
					params[i] += '|' + $(this).val();
				}
				else {
					params[i] = $(this).val();
				}
			});
			$.each(params, function(i, val) {
				// apply search params to datatable
				table.column(i).search(val ? val : '', false, false);
			});

			table.table().draw();
			
		});

		$('#kt_reset').on('click', function(e) {
			
			e.preventDefault();
			$('.datatable-input').each(function() {
				if($(this).prop("tagName") == 'INPUT'){
					$(this).val('');
				}else if($(this).prop("tagName") == 'SELECT'){
					$(this).val(null).change();
				}
				
				table.column($(this).data('col-index')).search('', false, false);
			});

			table.table().draw();
			
		});
	};

	var initTableReservations = function() {

		var select2Optioneee = { placeholder: "Seçiniz", language: "tr", width: 150 };
		// begin first table
		var table = $('#kt_datatable_reservations').DataTable({
			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,
			//stateSave: true,

			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[0, 'desc']],
            language: {
                "lengthMenu": "Her sayfada _MENU_ kayıt göster",
                "info": "Sayfa: _PAGE_/_PAGES_ (_MAX_ kayıt filtrelendi)",
                "infoEmpty": "Kayıt Yok.",
                "zeroRecords": "Kayıt Yok.",
                "infoFiltered": "(Toplam _MAX_ kayıttan filtrelendi)",
                "processing": "Yükleniyor...",
                "search": "Arama:",
            },
			ajax: {
				url: HOST_URL + '/getReservations',
				type: 'POST',
				headers: { 'X-CSRF-TOKEN': $('#token').val() },
				data: {
					columnsDef: ['reservation_id', 'province_name', 'ground_name', 'reservation_groundtable_date', 'reservation_groundtable_time', 'team_id', 'reservation_offer_count', 'status', 'actions'],
				},
			},
			columns: [
				{name: 'reservation_id'},
				{name: 'province_name'},
				{name: 'ground_name'},
				{name: 'reservation_groundtable_date'},
				{name: 'reservation_groundtable_time'},
				{name: 'team_id'},
				{name: 'reservation_offer_count'},
				{name: 'status'},
				{name: 'actions', responsivePriority: -1},
			],
			columnDefs: [
				{targets: 0, title: 'ID', visible: false},
				{targets: 1, title: 'Şehir'},
				{targets: 2, title: 'Halı Saha'},
				{targets: 3, title: 'Tarih'},
				{targets: 4, title: 'Saat'},
				{targets: 5, title: 'T.B.Takım'},
				{targets: 6, title: 'Teklif S.'},
				{targets: 7, title: 'Durum'},
				{
					targets: -1,
					title: 'İşlem',
					orderable: false,
					width: '150px',
					render: function(data, type, full, meta) {
						return '\
						<a href="reservations/view/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_view_reservation" title="Görüntüle">\
							<i class="fas fa-eye"></i>\
						</a>\
						<a href="reservations/edit/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_change_reservation" title="Düzenle">\
							<i class="fas fa-edit"></i>\
						</a>\
						<a href="reservations/delete/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_delete_reservation" title="Sil">\
							<i class="fas fa-trash-alt"></i>\
						</a>\
						';
					},
				},
			],
			drawCallback: function (settings) {
				viButReservation();
		      	$('[data-toggle="tooltip"]').tooltip();
		    }
		}).on('draw', function () {
			/*
			    $.each(table.column(0).data(), function(i, val) {
					$('#select2_potential_dt_'+val).select2(select2Optioneee).on('change', function (e) {
		            	//console.log('#select2_potential_dt_'+val);
		            	setTeamsAjaxData('#select2_potential_dt_'+val, val, 'setTeamsP');
		            });

					$('#select2_satisfaction_dt_'+val).select2(select2Optioneee).on('change', function (e) {
		            	//console.log('#select2_satisfaction_dt_'+val);
		            	setTeamsAjaxData('#select2_satisfaction_dt_'+val, val, 'setTeamsS');
		            });
				});
			*/
		});

		$('#kt_search').on('click', function(e) {
			e.preventDefault();

			$('#potential').val($('#select2_potential_search').val()+'|'+$('#select2_satisfaction_search').val()+'|'+$('#select2_satisfaction_jogo_search').val());
			$('#ms').val($('#msmin').val()+'|'+$('#msmax').val());
			$('#ks').val($('#ksmin').val()+'|'+$('#ksmax').val());

			var params = {};
			$('.datatable-input').each(function() {
				//console.log($(this));
				var i = $(this).data('col-index');
				if (params[i]) {
					params[i] += '|' + $(this).val();
				}
				else {
					params[i] = $(this).val();
				}
			});
			$.each(params, function(i, val) {
				// apply search params to datatable
				table.column(i).search(val ? val : '', false, false);
			});

			table.table().draw();
		});

		$('#kt_reset').on('click', function(e) {
			e.preventDefault();
			$('.datatable-input').each(function() {
				if($(this).prop("tagName") == 'INPUT'){
					$(this).val('');
				}else if($(this).prop("tagName") == 'SELECT'){
					$(this).val(null).change();
				}
				
				table.column($(this).data('col-index')).search('', false, false);
			});

			table.table().draw();
		});
	};

	var initTableMatchPlayers = function() {

		// begin first table
		var table = $('#kt_datatable_match_players').DataTable({
			responsive: true,
			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[0, 'desc']],
            language: {
                "lengthMenu": "Her sayfada _MENU_ kayıt göster",
                "info": "Sayfa: _PAGE_/_PAGES_ (_MAX_ kayıt filtrelendi)",
                "infoEmpty": "Kayıt Yok.",
                "zeroRecords": "Kayıt Yok.",
                "infoFiltered": "(Toplam _MAX_ kayıttan filtrelendi)",
                "processing": "Yükleniyor...",
                "search": "Arama:",
            },
			columnDefs: [
				{targets: 0, visible: false},
				{targets: -1, orderable: false},
			],
			
		});

		$('.match_player_edit_button').on('click', function(e) {
			e.preventDefault();
		    var target = $(this).attr('href');
		    $('html, body').animate({
		    	scrollTop: ($('#match_playerForm').offset().top)
		    }, 1000);

			$.ajax({
                method: 'GET',
                url : '/getMatchPlayerF/'+table.row(this.id).data()[0],
                dataType: 'json',
                success: function(data, textStatus, jqXHR)
                {
					$("#match_playerForm input[name=match_player_id]").val(data.items['id']);
					$("#match_playerForm select[name=player_id]").val(data.items['player_id']).trigger('change');
					$("#match_playerForm select[name=player_id]").prop("disabled", true);
					$("#match_playerForm input[name=guest_name]").val(data.items['guest_name']);
					$("#match_playerForm select[name=position_id]").val(data.items['position_id']).trigger('change');
                },
                error: function (jqXHR, textStatus, errorThrown)
                {

                }
            });
		});
	};

	var initTableMatchActions = function() {

		// begin first table
		var table = $('#kt_datatable_match_actions').DataTable({
			responsive: true,
			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[0, 'desc']],
            language: {
                "lengthMenu": "Her sayfada _MENU_ kayıt göster",
                "info": "Sayfa: _PAGE_/_PAGES_ (_MAX_ kayıt filtrelendi)",
                "infoEmpty": "Kayıt Yok.",
                "zeroRecords": "Kayıt Yok.",
                "infoFiltered": "(Toplam _MAX_ kayıttan filtrelendi)",
                "processing": "Yükleniyor...",
                "search": "Arama:",
            },
			columnDefs: [
				//{targets: 0, visible: false},
				{targets: -1, orderable: false},
			],
			
		});

		$('.match_action_edit_button').on('click', function(e) {
			e.preventDefault();
		    var target = $(this).attr('href');
		    $('html, body').animate({
		    	scrollTop: ($('#match_actionForm').offset().top)
		    }, 1000);

			$.ajax({
                method: 'GET',
                url : '/getMatchActionF/'+table.row(this.id).data()[0],
                dataType: 'json',
                success: function(data, textStatus, jqXHR)
                {	

                	$("#match_actionForm input[name=match_action_id]").val(data.items['id']);

                	$("#match_actionForm select[name=embed_source]").val(data.items['embed_source']).trigger('change');
                	$("#match_actionForm input[name=embed_code]").val(data.items['embed_code']);
                	$("#match_actionForm select[name=team_id]").val(data.items['team_id']).trigger('change');
					//$("#match_actionForm select[name=team_id]").prop("disabled", true);
                	$("#match_actionForm select[name=match_player_id]").val(data.items['match_player_id']).trigger('change');
					//$("#match_actionForm select[name=match_player_id]").prop("disabled", true);
					$("#match_actionForm select[name=type]").val(data.items['type']).trigger('change');
					$("#match_actionForm input[name=minute]").val(data.items['minute']);

                },
                error: function (jqXHR, textStatus, errorThrown)
                {

                }
            });
		});
	};

	var initTableMatchPanoramas = function() {

		// begin first table
		var table = $('#kt_datatable_match_panoramas').DataTable({
			responsive: true,
			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[0, 'desc']],
            language: {
                "lengthMenu": "Her sayfada _MENU_ kayıt göster",
                "info": "Sayfa: _PAGE_/_PAGES_ (_MAX_ kayıt filtrelendi)",
                "infoEmpty": "Kayıt Yok.",
                "zeroRecords": "Kayıt Yok.",
                "infoFiltered": "(Toplam _MAX_ kayıttan filtrelendi)",
                "processing": "Yükleniyor...",
                "search": "Arama:",
            },
			columnDefs: [
				//{targets: 0, visible: false},
				{targets: -1, orderable: false},
			],
			
		});

		$('.match_panorama_edit_button').on('click', function(e) {
			e.preventDefault();
		    var target = $(this).attr('href');
		    $('html, body').animate({
		    	scrollTop: ($('#match_panoramaForm').offset().top)
		    }, 1000);

			$.ajax({
                method: 'GET',
                url : '/getMatchPanoramaF/'+table.row(this.id).data()[0],
                dataType: 'json',
                success: function(data, textStatus, jqXHR)
                {	

                	$("#match_panoramaForm input[name=match_panorama_id]").val(data.items['id']);

                	$("#match_panoramaForm select[name=panorama_id]").val(data.items['panorama_id']).trigger('change');
                	$("#match_panoramaForm select[name=match_player_id_mpa]").val(data.items['match_player_id']).trigger('change');

                	$("#match_panoramaForm select[name=embed_source_mpa]").val(data.items['embed_source']).trigger('change');
                	$("#match_panoramaForm input[name=embed_code_mpa]").val(data.items['embed_code']);

                },
                error: function (jqXHR, textStatus, errorThrown)
                {

                }
            });
		});
	};

	var initTableEliminationTree = function() {
		
		var queryString = '';
		if($('#tree_id').val() != ""){
			queryString = '?tree_id='+$('#tree_id').val();
		}

		// begin first table
		var table = $('#kt_datatable_elimination_tree').DataTable({

			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,
			//stateSave: true,

			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[1, 'asc']],
            language: {
                "lengthMenu": "Her sayfada _MENU_ kayıt göster",
                "info": "Sayfa: _PAGE_/_PAGES_ (_MAX_ kayıt filtrelendi)",
                "infoEmpty": "Kayıt Yok.",
                "zeroRecords": "Kayıt Yok.",
                "infoFiltered": "(Toplam _MAX_ kayıttan filtrelendi)",
                "processing": "Yükleniyor...",
                "search": "Arama:",
            },

			ajax: {
				url: HOST_URL + '/getEliminationTrees',
				type: 'POST',
				headers: { 'X-CSRF-TOKEN': $('#token').val() },
				data: function(data){
					data.columnsDef = ['id', 'name', 'season', 'type', 'actions'];
					data.treeId = $('#tree_id').val();
			    },
			},
			columns: [
				{name: 'id'},
				{name: 'name'},
				{name: 'season'},
				{name: 'type'},
				{name: 'actions', responsivePriority: -1},
			],
			columnDefs: [

				{targets: 0, title: 'ID', visible: false},
				{targets: 1, title: 'Ad'},
				{targets: 2, title: 'Sezon'},
				{targets: 3, title: 'Tip'},
				{
					targets: -1,
					title: 'İşlem',
					orderable: false,
					width: '150px',
					render: function(data, type, full, meta) {
						return '\
						<a href="/elimination_tree/view/' + full[0] + queryString + '" class="btn btn-sm btn-clean btn-icon btn_view_eliminationtree" title="Görüntüle">\
							<i class="fas fa-eye"></i>\
						</a>\
						<a href="/elimination_tree/edit/' + full[0] + queryString + '" class="btn btn-sm btn-clean btn-icon btn_change_eliminationtree" title="Düzenle">\
							<i class="fas fa-edit"></i>\
						</a>\
						<a href="/elimination_tree/delete/' + full[0] + queryString + '" class="btn btn-sm btn-clean btn-icon btn_delete_eliminationtree" title="Sil">\
							<i class="fas fa-trash-alt"></i>\
						</a>\
						<a href="/elimination_tree/tree/' + full[0] + queryString + '" class="btn btn-sm btn-clean btn-icon" title="Eleme Ağacı">\
							<i class="fas fa-sitemap"></i>\
						</a>\
						';
					},
				},

			],
			drawCallback: function (settings) {
				viButEliminationtree();
		    }
		});

		$('#kt_search').on('click', function(e) {
			e.preventDefault();
			
			var params = {};
			$('.datatable-input').each(function() {
				//console.log($(this));
				var i = $(this).data('col-index');
				if (params[i]) {
					params[i] += '|' + $(this).val();
				}
				else {
					params[i] = $(this).val();
				}
			});
			$.each(params, function(i, val) {
				// apply search params to datatable
				table.column(i).search(val ? val : '', false, false);
			});

			table.table().draw();
		});

		$('#kt_reset').on('click', function(e) {
			e.preventDefault();
			$('.datatable-input').each(function() {
				if($(this).prop("tagName") == 'INPUT'){
					$(this).val('');
				}else if($(this).prop("tagName") == 'SELECT'){
					$(this).val(null).change();
				}
				
				table.column($(this).data('col-index')).search('', false, false);
			});

			table.table().draw();
		});
	};

	var initTableFixtureGroup = function() {
		
		var queryString = '';
		if($('#tree_id').val() != ""){
			queryString = '?tree_id='+$('#tree_id').val();
		}

		// begin first table
		var table = $('#kt_datatable_fixture_group').DataTable({

			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,

			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[1, 'asc']],
            language: {
                "lengthMenu": "Her sayfada _MENU_ kayıt göster",
                "info": "Sayfa: _PAGE_/_PAGES_ (_MAX_ kayıt filtrelendi)",
                "infoEmpty": "Kayıt Yok.",
                "zeroRecords": "Kayıt Yok.",
                "infoFiltered": "(Toplam _MAX_ kayıttan filtrelendi)",
                "processing": "Yükleniyor...",
                "search": "Arama:",
            },

			ajax: {
				url: HOST_URL + '/getFixtureGroup',
				type: 'POST',
				headers: { 'X-CSRF-TOKEN': $('#token').val() },
				data: function(data){
					data.columnsDef = ['id', 'name', 'season', 'actions'];
					data.treeId = $('#tree_id').val();
			    },
			},
			columns: [
				{name: 'id'},
				{name: 'name'},
				{name: 'season'},
				{name: 'actions', responsivePriority: -1},
			],
			columnDefs: [

				{targets: 0, title: 'ID', visible: false},
				{targets: 1, title: 'Ad'},
				{targets: 2, title: 'Sezon'},
				{
					targets: -1,
					title: 'İşlem',
					orderable: false,
					width: '150px',
					render: function(data, type, full, meta) {
						return '\
						<a href="/fixture_group/view/' + full[0] + queryString + '" class="btn btn-sm btn-clean btn-icon btn_view_fixturegroup" title="Görüntüle">\
							<i class="fas fa-eye"></i>\
						</a>\
						<a href="/fixture_group/edit/' + full[0] + queryString + '" class="btn btn-sm btn-clean btn-icon btn_change_fixturegroup" title="Düzenle">\
							<i class="fas fa-edit"></i>\
						</a>\
						<a href="/fixture_group/delete/' + full[0] + queryString + '" class="btn btn-sm btn-clean btn-icon btn_delete_fixturegroup" title="Sil">\
							<i class="fas fa-trash-alt"></i>\
						</a>\
						';
					},
				},

			],
			drawCallback: function (settings) {
				viButEliminationtree();
		    }
		});

		$('#kt_search').on('click', function(e) {
			e.preventDefault();
			
			var params = {};
			$('.datatable-input').each(function() {
				//console.log($(this));
				var i = $(this).data('col-index');
				if (params[i]) {
					params[i] += '|' + $(this).val();
				}
				else {
					params[i] = $(this).val();
				}
			});
			$.each(params, function(i, val) {
				// apply search params to datatable
				table.column(i).search(val ? val : '', false, false);
			});

			table.table().draw();
		});

		$('#kt_reset').on('click', function(e) {
			e.preventDefault();
			$('.datatable-input').each(function() {
				if($(this).prop("tagName") == 'INPUT'){
					$(this).val('');
				}else if($(this).prop("tagName") == 'SELECT'){
					$(this).val(null).change();
				}
				
				table.column($(this).data('col-index')).search('', false, false);
			});

			table.table().draw();
		});
	};

	var initTablePlayerPositions = function() {

		// begin first table
		var table = $('#kt_datatable_player_positions').DataTable({

			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,
			//stateSave: true,

			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[3, 'asc']],
            language: {
                "lengthMenu": "Her sayfada _MENU_ kayıt göster",
                "info": "Sayfa: _PAGE_/_PAGES_ (_MAX_ kayıt filtrelendi)",
                "infoEmpty": "Kayıt Yok.",
                "zeroRecords": "Kayıt Yok.",
                "infoFiltered": "(Toplam _MAX_ kayıttan filtrelendi)",
                "processing": "Yükleniyor...",
                "search": "Arama:",
            },

			ajax: {
				url: HOST_URL + '/getPlayerPositions',
				type: 'POST',
				headers: { 'X-CSRF-TOKEN': $('#token').val() },
				data: {
					columnsDef: ['id', 'name', 'code', 'actions'],
				},
			},
			columns: [
				{name: 'id'},
				{name: 'name'},
				{name: 'code'},
				{name: 'order'},
				{name: 'actions', responsivePriority: -1},
			],
			columnDefs: [

				{targets: 0, title: 'ID', visible: false},
				{targets: 1, title: 'Ad'},
				{targets: 2, title: 'Kod'},
				{targets: 3, title: 'Sıra'},
				{
					targets: -1,
					title: 'İşlem',
					orderable: false,
					width: '150px',
					render: function(data, type, full, meta) {
						return '\
						<a href="player_positions/view/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_view_position" title="Görüntüle">\
							<i class="fas fa-eye"></i>\
						</a>\
						<a href="player_positions/edit/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_change_position" title="Düzenle">\
							<i class="fas fa-edit"></i>\
						</a>\
						<a href="player_positions/delete/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_delete_position" title="Sil">\
							<i class="fas fa-trash-alt"></i>\
						</a>\
						';
					},
				},

			],
			drawCallback: function (settings) {
				viButPosition();
		    }
		});

		$('#kt_search').on('click', function(e) {
			e.preventDefault();
			
			var params = {};
			$('.datatable-input').each(function() {
				//console.log($(this));
				var i = $(this).data('col-index');
				if (params[i]) {
					params[i] += '|' + $(this).val();
				}
				else {
					params[i] = $(this).val();
				}
			});
			$.each(params, function(i, val) {
				// apply search params to datatable
				table.column(i).search(val ? val : '', false, false);
			});

			table.table().draw();
		});

		$('#kt_reset').on('click', function(e) {
			e.preventDefault();
			$('.datatable-input').each(function() {
				if($(this).prop("tagName") == 'INPUT'){
					$(this).val('');
				}else if($(this).prop("tagName") == 'SELECT'){
					$(this).val(null).change();
				}
				
				table.column($(this).data('col-index')).search('', false, false);
			});

			table.table().draw();
		});
	};

	var initTablePointTypes = function() {

		// begin first table
		var table = $('#kt_datatable_point_types').DataTable({

			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,
			//stateSave: true,

			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[0, 'desc']],
            language: {
                "lengthMenu": "Her sayfada _MENU_ kayıt göster",
                "info": "Sayfa: _PAGE_/_PAGES_ (_MAX_ kayıt filtrelendi)",
                "infoEmpty": "Kayıt Yok.",
                "zeroRecords": "Kayıt Yok.",
                "infoFiltered": "(Toplam _MAX_ kayıttan filtrelendi)",
                "processing": "Yükleniyor...",
                "search": "Arama:",
            },

			ajax: {
				url: HOST_URL + '/getPointTypes',
				type: 'POST',
				headers: { 'X-CSRF-TOKEN': $('#token').val() },
				data: {
					columnsDef: ['id', 'name', 'min_point', 'max_point', 'win_score', 'defeat_score', 'draw_score', 'actions'],
				},
			},
			columns: [
				{name: 'id'},
				{name: 'name'},
				{name: 'min_point'},
				{name: 'max_point'},
				{name: 'win_score'},
				{name: 'defeat_score'},
				{name: 'draw_score'},
				{name: 'actions', responsivePriority: -1},
			],
			columnDefs: [

				{targets: 0, title: 'ID', visible: false},
				{targets: 1, title: 'Ad'},
				{targets: 2, title: 'Min Puan'},
				{targets: 3, title: 'Max Puan'},
				{targets: 4, title: 'Galibiyet Puanı'},
				{targets: 5, title: 'Yenilgi Puanı'},
				{targets: 6, title: 'Beraberlik Puanı'},
				{
					targets: -1,
					title: 'İşlem',
					orderable: false,
					width: '150px',
					render: function(data, type, full, meta) {
						return '\
						<a href="point_types/view/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_view_pointtype" title="Görüntüle">\
							<i class="fas fa-eye"></i>\
						</a>\
						<a href="point_types/edit/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_change_pointtype" title="Düzenle">\
							<i class="fas fa-edit"></i>\
						</a>\
						<a href="point_types/delete/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_delete_pointtype" title="Sil">\
							<i class="fas fa-trash-alt"></i>\
						</a>\
						';
					},
				},

			],
			drawCallback: function (settings) {
				viButPointType();
		    }
		});

		$('#kt_search').on('click', function(e) {
			e.preventDefault();
			
			var params = {};
			$('.datatable-input').each(function() {
				//console.log($(this));
				var i = $(this).data('col-index');
				if (params[i]) {
					params[i] += '|' + $(this).val();
				}
				else {
					params[i] = $(this).val();
				}
			});
			$.each(params, function(i, val) {
				// apply search params to datatable
				table.column(i).search(val ? val : '', false, false);
			});

			table.table().draw();
		});

		$('#kt_reset').on('click', function(e) {
			e.preventDefault();
			$('.datatable-input').each(function() {
				if($(this).prop("tagName") == 'INPUT'){
					$(this).val('');
				}else if($(this).prop("tagName") == 'SELECT'){
					$(this).val(null).change();
				}
				
				table.column($(this).data('col-index')).search('', false, false);
			});

			table.table().draw();
		});
	};

	var initTableTactics = function() {

		// begin first table
		var table = $('#kt_datatable_tactics').DataTable({

			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,
			//stateSave: true,

			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[0, 'desc']],
            language: {
                "lengthMenu": "Her sayfada _MENU_ kayıt göster",
                "info": "Sayfa: _PAGE_/_PAGES_ (_MAX_ kayıt filtrelendi)",
                "infoEmpty": "Kayıt Yok.",
                "zeroRecords": "Kayıt Yok.",
                "infoFiltered": "(Toplam _MAX_ kayıttan filtrelendi)",
                "processing": "Yükleniyor...",
                "search": "Arama:",
            },

			ajax: {
				url: HOST_URL + '/getTactics',
				type: 'POST',
				headers: { 'X-CSRF-TOKEN': $('#token').val() },
				data: {
					columnsDef: ['id', 'name', 'actions'],
				},
			},
			columns: [
				{name: 'id'},
				{name: 'name'},
				{name: 'actions', responsivePriority: -1},
			],
			columnDefs: [

				{targets: 0, title: 'ID', visible: false},
				{targets: 1, title: 'Ad'},
				{
					targets: -1,
					title: 'İşlem',
					orderable: false,
					width: '150px',
					render: function(data, type, full, meta) {
						return '\
						<a href="tactics/view/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_view_tactic" title="Görüntüle">\
							<i class="fas fa-eye"></i>\
						</a>\
						<a href="tactics/edit/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_change_tactic" title="Düzenle">\
							<i class="fas fa-edit"></i>\
						</a>\
						<a href="tactics/delete/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_delete_tactic" title="Sil">\
							<i class="fas fa-trash-alt"></i>\
						</a>\
						';
					},
				},
			],
			drawCallback: function (settings) {
				viButTactic();
		    }
		});

		$('#kt_search').on('click', function(e) {
			e.preventDefault();
			
			var params = {};
			$('.datatable-input').each(function() {
				//console.log($(this));
				var i = $(this).data('col-index');
				if (params[i]) {
					params[i] += '|' + $(this).val();
				}
				else {
					params[i] = $(this).val();
				}
			});
			$.each(params, function(i, val) {
				// apply search params to datatable
				table.column(i).search(val ? val : '', false, false);
			});

			table.table().draw();
		});

		$('#kt_reset').on('click', function(e) {
			e.preventDefault();
			$('.datatable-input').each(function() {
				if($(this).prop("tagName") == 'INPUT'){
					$(this).val('');
				}else if($(this).prop("tagName") == 'SELECT'){
					$(this).val(null).change();
				}
				
				table.column($(this).data('col-index')).search('', false, false);
			});

			table.table().draw();
		});
	};

	var initTableTeamPotentials = function() {

		// begin first table
		var table = $('#kt_datatable_team_potentials').DataTable({

			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,
			//stateSave: true,

			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[0, 'desc']],
            language: {
                "lengthMenu": "Her sayfada _MENU_ kayıt göster",
                "info": "Sayfa: _PAGE_/_PAGES_ (_MAX_ kayıt filtrelendi)",
                "infoEmpty": "Kayıt Yok.",
                "zeroRecords": "Kayıt Yok.",
                "infoFiltered": "(Toplam _MAX_ kayıttan filtrelendi)",
                "processing": "Yükleniyor...",
                "search": "Arama:",
            },

			ajax: {
				url: HOST_URL + '/getTeamPotentials',
				type: 'POST',
				headers: { 'X-CSRF-TOKEN': $('#token').val() },
				data: {
					columnsDef: ['id', 'name', 'actions'],
				},
			},
			columns: [
				{name: 'id'},
				{name: 'name'},
				{name: 'actions', responsivePriority: -1},
			],
			columnDefs: [

				{targets: 0, title: 'ID', visible: false},
				{targets: 1, title: 'Ad'},
				{
					targets: -1,
					title: 'İşlem',
					orderable: false,
					width: '150px',
					render: function(data, type, full, meta) {
						return '\
						<a href="team_potentials/view/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_view_potential" title="Görüntüle">\
							<i class="fas fa-eye"></i>\
						</a>\
						<a href="team_potentials/edit/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_change_potential" title="Düzenle">\
							<i class="fas fa-edit"></i>\
						</a>\
						<a href="team_potentials/delete/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_delete_potential" title="Sil">\
							<i class="fas fa-trash-alt"></i>\
						</a>\
						';
					},
				},

			],
			drawCallback: function (settings) {
				viButPotential();
		    }
		});

		$('#kt_search').on('click', function(e) {
			e.preventDefault();
			
			var params = {};
			$('.datatable-input').each(function() {
				//console.log($(this));
				var i = $(this).data('col-index');
				if (params[i]) {
					params[i] += '|' + $(this).val();
				}
				else {
					params[i] = $(this).val();
				}
			});
			$.each(params, function(i, val) {
				// apply search params to datatable
				table.column(i).search(val ? val : '', false, false);
			});

			table.table().draw();
		});

		$('#kt_reset').on('click', function(e) {
			e.preventDefault();
			$('.datatable-input').each(function() {
				if($(this).prop("tagName") == 'INPUT'){
					$(this).val('');
				}else if($(this).prop("tagName") == 'SELECT'){
					$(this).val(null).change();
				}
				
				table.column($(this).data('col-index')).search('', false, false);
			});

			table.table().draw();
		});
	};

	var initTableTeamSatisfactions = function() {

		// begin first table
		var table = $('#kt_datatable_team_satisfactions').DataTable({

			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,
			//stateSave: true,

			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[0, 'desc']],
            language: {
                "lengthMenu": "Her sayfada _MENU_ kayıt göster",
                "info": "Sayfa: _PAGE_/_PAGES_ (_MAX_ kayıt filtrelendi)",
                "infoEmpty": "Kayıt Yok.",
                "zeroRecords": "Kayıt Yok.",
                "infoFiltered": "(Toplam _MAX_ kayıttan filtrelendi)",
                "processing": "Yükleniyor...",
                "search": "Arama:",
            },

			ajax: {
				url: HOST_URL + '/getTeamSatisfactions',
				type: 'POST',
				headers: { 'X-CSRF-TOKEN': $('#token').val() },
				data: {
					columnsDef: ['id', 'name', 'actions'],
				},
			},
			columns: [
				{name: 'id'},
				{name: 'name'},
				{name: 'actions', responsivePriority: -1},
			],
			columnDefs: [

				{targets: 0, title: 'ID', visible: false},
				{targets: 1, title: 'Ad'},
				{
					targets: -1,
					title: 'İşlem',
					orderable: false,
					width: '150px',
					render: function(data, type, full, meta) {
						return '\
						<a href="team_satisfactions/view/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_view_satisfaction" title="Görüntüle">\
							<i class="fas fa-eye"></i>\
						</a>\
						<a href="team_satisfactions/edit/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_change_satisfaction" title="Düzenle">\
							<i class="fas fa-edit"></i>\
						</a>\
						<a href="team_satisfactions/delete/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_delete_satisfaction" title="Sil">\
							<i class="fas fa-trash-alt"></i>\
						</a>\
						';
					},
				},
			],
			drawCallback: function (settings) {
				viButSatisfaction();
		    }
		});

		$('#kt_search').on('click', function(e) {
			e.preventDefault();
			
			var params = {};
			$('.datatable-input').each(function() {
				//console.log($(this));
				var i = $(this).data('col-index');
				if (params[i]) {
					params[i] += '|' + $(this).val();
				}
				else {
					params[i] = $(this).val();
				}
			});
			$.each(params, function(i, val) {
				// apply search params to datatable
				table.column(i).search(val ? val : '', false, false);
			});

			table.table().draw();
		});

		$('#kt_reset').on('click', function(e) {
			e.preventDefault();
			$('.datatable-input').each(function() {
				if($(this).prop("tagName") == 'INPUT'){
					$(this).val('');
				}else if($(this).prop("tagName") == 'SELECT'){
					$(this).val(null).change();
				}
				
				table.column($(this).data('col-index')).search('', false, false);
			});

			table.table().draw();
		});
	};

	var initTableTeamSatisfactionsJogo = function() {

		// begin first table
		var table = $('#kt_datatable_team_satisfactions_jogo').DataTable({

			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,
			//stateSave: true,

			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[0, 'desc']],
            language: {
                "lengthMenu": "Her sayfada _MENU_ kayıt göster",
                "info": "Sayfa: _PAGE_/_PAGES_ (_MAX_ kayıt filtrelendi)",
                "infoEmpty": "Kayıt Yok.",
                "zeroRecords": "Kayıt Yok.",
                "infoFiltered": "(Toplam _MAX_ kayıttan filtrelendi)",
                "processing": "Yükleniyor...",
                "search": "Arama:",
            },

			ajax: {
				url: HOST_URL + '/getTeamSatisfactionsJogo',
				type: 'POST',
				headers: { 'X-CSRF-TOKEN': $('#token').val() },
				data: {
					columnsDef: ['id', 'name', 'actions'],
				},
			},
			columns: [
				{name: 'id'},
				{name: 'name'},
				{name: 'actions', responsivePriority: -1},
			],
			columnDefs: [

				{targets: 0, title: 'ID', visible: false},
				{targets: 1, title: 'Ad'},
				{
					targets: -1,
					title: 'İşlem',
					orderable: false,
					width: '150px',
					render: function(data, type, full, meta) {
						return '\
						<a href="team_satisfactions_jogo/view/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_view_satisfaction_jogo" title="Görüntüle">\
							<i class="fas fa-eye"></i>\
						</a>\
						<a href="team_satisfactions_jogo/edit/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_change_satisfaction_jogo" title="Düzenle">\
							<i class="fas fa-edit"></i>\
						</a>\
						<a href="team_satisfactions_jogo/delete/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_delete_satisfaction_jogo" title="Sil">\
							<i class="fas fa-trash-alt"></i>\
						</a>\
						';
					},
				},
			],
			drawCallback: function (settings) {
				viButSatisfactionJogo();
		    }
		});

		$('#kt_search').on('click', function(e) {
			e.preventDefault();
			
			var params = {};
			$('.datatable-input').each(function() {
				//console.log($(this));
				var i = $(this).data('col-index');
				if (params[i]) {
					params[i] += '|' + $(this).val();
				}
				else {
					params[i] = $(this).val();
				}
			});
			$.each(params, function(i, val) {
				// apply search params to datatable
				table.column(i).search(val ? val : '', false, false);
			});

			table.table().draw();
		});

		$('#kt_reset').on('click', function(e) {
			e.preventDefault();
			$('.datatable-input').each(function() {
				if($(this).prop("tagName") == 'INPUT'){
					$(this).val('');
				}else if($(this).prop("tagName") == 'SELECT'){
					$(this).val(null).change();
				}
				
				table.column($(this).data('col-index')).search('', false, false);
			});

			table.table().draw();
		});
	};

	var initTableBanners = function() {

		// begin first table
		var table = $('#kt_datatable_banners').DataTable({

			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,
			//stateSave: true,

			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[0, 'desc']],
            language: {
                "lengthMenu": "Her sayfada _MENU_ kayıt göster",
                "info": "Sayfa: _PAGE_/_PAGES_ (_MAX_ kayıt filtrelendi)",
                "infoEmpty": "Kayıt Yok.",
                "zeroRecords": "Kayıt Yok.",
                "infoFiltered": "(Toplam _MAX_ kayıttan filtrelendi)",
                "processing": "Yükleniyor...",
                "search": "Arama:",
            },

			ajax: {
				url: HOST_URL + '/getBanners',
				type: 'POST',
				headers: { 'X-CSRF-TOKEN': $('#token').val() },
				data: {
					columnsDef: ['id', 'name', 'actions'],
				},
			},
			columns: [
				{name: 'id'},
				{name: 'name'},
				{name: 'actions', responsivePriority: -1},
			],
			columnDefs: [

				{targets: 0, title: 'ID', visible: false},
				{targets: 1, title: 'Ad'},
				{
					targets: -1,
					title: 'İşlem',
					orderable: false,
					width: '150px',
					render: function(data, type, full, meta) {
						return '\
						<a href="banners/view/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_view_banners" title="Görüntüle">\
							<i class="fas fa-eye"></i>\
						</a>\
						<a href="banners/edit/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_change_banners" title="Düzenle">\
							<i class="fas fa-edit"></i>\
						</a>\
						';
					},
				},
			]
			/*,
			drawCallback: function (settings) {
				viButSatisfactionJogo();
		    }
		    */
		});

		$('#kt_search').on('click', function(e) {
			e.preventDefault();
			
			var params = {};
			$('.datatable-input').each(function() {
				//console.log($(this));
				var i = $(this).data('col-index');
				if (params[i]) {
					params[i] += '|' + $(this).val();
				}
				else {
					params[i] = $(this).val();
				}
			});
			$.each(params, function(i, val) {
				// apply search params to datatable
				table.column(i).search(val ? val : '', false, false);
			});

			table.table().draw();
		});

		$('#kt_reset').on('click', function(e) {
			e.preventDefault();
			$('.datatable-input').each(function() {
				if($(this).prop("tagName") == 'INPUT'){
					$(this).val('');
				}else if($(this).prop("tagName") == 'SELECT'){
					$(this).val(null).change();
				}
				
				table.column($(this).data('col-index')).search('', false, false);
			});

			table.table().draw();
		});
	};

	var initTablePanoramaTypes = function() {

		// begin first table
		var table = $('#kt_datatable_panorama_types').DataTable({

			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,

			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[0, 'desc']],
            			
			language: {
                "lengthMenu": "Her sayfada _MENU_ kayıt göster",
                "info": "Sayfa: _PAGE_/_PAGES_ (_MAX_ kayıt filtrelendi)",
                "infoEmpty": "Kayıt Yok.",
                "zeroRecords": "Kayıt Yok.",
                "infoFiltered": "(Toplam _MAX_ kayıttan filtrelendi)",
                "processing": "Yükleniyor...",
                "search": "Arama:",
            },

			ajax: {
				url: HOST_URL + '/getPanoramaTypes',
				type: 'POST',
				headers: { 'X-CSRF-TOKEN': $('#token').val() },
				data: {
					columnsDef: ['id', 'name', 'to_who', 'period', 'season_id', 'actions'],
				},
			},
			columns: [
				{name: 'id'},
				{name: 'name'},
				{name: 'to_who'},
				{name: 'period'},
				{name: 'season_id'},
				{name: 'actions', responsivePriority: -1},
			],
			columnDefs: [

				{targets: 0, title: 'ID', visible: false},
				{targets: 1, title: 'Ad'},
				{targets: 2, title: 'Kime'},
				{targets: 3, title: 'Period'},
				{targets: 4, title: 'Sezon'},
				{
					targets: -1,
					title: 'İşlem',
					orderable: false,
					width: '150px',
					render: function(data, type, full, meta) {
						return '\
						<a href="panorama_types/view/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_view_tactic" title="Görüntüle">\
							<i class="fas fa-eye"></i>\
						</a>\
						<a href="panorama_types/edit/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_change_tactic" title="Düzenle">\
							<i class="fas fa-edit"></i>\
						</a>\
						<a href="panorama_types/delete/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_delete_tactic" title="Sil">\
							<i class="fas fa-trash-alt"></i>\
						</a>\
						';
					},
				},
			],
			drawCallback: function (settings) {
				viButTactic();
		    }
		});

		$('#kt_search').on('click', function(e) {
			e.preventDefault();
			
			var params = {};
			$('.datatable-input').each(function() {
				//console.log($(this));
				var i = $(this).data('col-index');
				if (params[i]) {
					params[i] += '|' + $(this).val();
				}
				else {
					params[i] = $(this).val();
				}
			});
			$.each(params, function(i, val) {
				// apply search params to datatable
				table.column(i).search(val ? val : '', false, false);
			});

			table.table().draw();
		});

		$('#kt_reset').on('click', function(e) {
			e.preventDefault();
			$('.datatable-input').each(function() {
				if($(this).prop("tagName") == 'INPUT'){
					$(this).val('');
				}else if($(this).prop("tagName") == 'SELECT'){
					$(this).val(null).change();
				}
				
				table.column($(this).data('col-index')).search('', false, false);
			});

			table.table().draw();
		});
	};

	var initTableMonthlyPanormas = function() {

		// begin first table
		var table = $('#kt_datatable_monthly_panoramas').DataTable({

			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,

			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[0, 'desc']],
            language: {
                "lengthMenu": "Her sayfada _MENU_ kayıt göster",
                "info": "Sayfa: _PAGE_/_PAGES_ (_MAX_ kayıt filtrelendi)",
                "infoEmpty": "Kayıt Yok.",
                "zeroRecords": "Kayıt Yok.",
                "infoFiltered": "(Toplam _MAX_ kayıttan filtrelendi)",
                "processing": "Yükleniyor...",
                "search": "Arama:",
            },

			ajax: {
				url: HOST_URL + '/getPanoramas',
				type: 'POST',
				headers: { 'X-CSRF-TOKEN': $('#token').val() },
				data: {
					columnsDef: ['id', 'season', 'to_who', 'type', 'period', 'team-player', 'actions'],
				},
			},
			columns: [
				{name: 'id'},
				{name: 'season'},
				{name: 'to_who'},
				{name: 'type'},
				{name: 'period'},
				{name: 'team-player'},
				{name: 'actions', responsivePriority: -1},
			],
			columnDefs: [

				{targets: 0, title: 'ID', visible: false},
				{targets: 1, title: 'Sezon'},
				{targets: 2, title: 'Kime'},
				{targets: 3, title: 'Tip'},
				{targets: 4, title: 'Period'},
				{targets: 5, title: 'Takım/Oyuncu'},
				{
					targets: -1,
					title: 'İşlemler',
					orderable: false,
					width: '150px',
					render: function(data, type, full, meta) {

						console.log(full);

						return '\
						<a href="monthly_panorama'+'_'+full[2]+'/view/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_view_tactic" title="Görüntüle">\
							<i class="fas fa-eye"></i>\
						</a>\
						<a href="monthly_panorama'+'_'+full[2]+'/edit/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_change_tactic" title="Düzenle">\
							<i class="fas fa-edit"></i>\
						</a>\
						<a href="monthly_panorama'+'_'+full[2]+'/delete/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_delete_tactic" title="Sil">\
							<i class="fas fa-trash-alt"></i>\
						</a>\
						';
					},
				},
			],
			drawCallback: function (settings) {
				viButTactic();
		    }
		});

		$('#kt_search').on('click', function(e) {
			e.preventDefault();
			
			var params = {};
			$('.datatable-input').each(function() {
				//console.log($(this));
				var i = $(this).data('col-index');
				if (params[i]) {
					params[i] += '|' + $(this).val();
				}
				else {
					params[i] = $(this).val();
				}
			});
			$.each(params, function(i, val) {
				// apply search params to datatable
				table.column(i).search(val ? val : '', false, false);
			});

			table.table().draw();
		});

		$('#kt_reset').on('click', function(e) {
			e.preventDefault();
			$('.datatable-input').each(function() {
				if($(this).prop("tagName") == 'INPUT'){
					$(this).val('');
				}else if($(this).prop("tagName") == 'SELECT'){
					$(this).val(null).change();
				}
				
				table.column($(this).data('col-index')).search('', false, false);
			});

			table.table().draw();
		});
	};

	var initTablePenaltyTypes = function() {

		// begin first table
		var table = $('#kt_datatable_penalty_types').DataTable({

			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,

			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[0, 'desc']],
            			
			language: {
                "lengthMenu": "Her sayfada _MENU_ kayıt göster",
                "info": "Sayfa: _PAGE_/_PAGES_ (_MAX_ kayıt filtrelendi)",
                "infoEmpty": "Kayıt Yok.",
                "zeroRecords": "Kayıt Yok.",
                "infoFiltered": "(Toplam _MAX_ kayıttan filtrelendi)",
                "processing": "Yükleniyor...",
                "search": "Arama:",
            },

			ajax: {
				url: HOST_URL + '/getPenaltyTypes',
				type: 'POST',
				headers: { 'X-CSRF-TOKEN': $('#token').val() },
				data: {
					columnsDef: ['id', 'name', 'to_who', 'actions'],
				},
			},
			columns: [
				{name: 'id'},
				{name: 'name'},
				{name: 'to_who'},
				{name: 'actions', responsivePriority: -1},
			],
			columnDefs: [

				{targets: 0, title: 'ID', visible: false},
				{targets: 1, title: 'Ad'},
				{targets: 2, title: 'Kime'},
				{
					targets: -1,
					title: 'İşlem',
					orderable: false,
					width: '150px',
					render: function(data, type, full, meta) {
						return '\
						<a href="penalty_types/view/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_view_tactic" title="Görüntüle">\
							<i class="fas fa-eye"></i>\
						</a>\
						<a href="penalty_types/edit/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_change_tactic" title="Düzenle">\
							<i class="fas fa-edit"></i>\
						</a>\
						<a href="penalty_types/delete/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_delete_tactic" title="Sil">\
							<i class="fas fa-trash-alt"></i>\
						</a>\
						';
					},
				},
			],
			drawCallback: function (settings) {
				viButTactic();
		    }
		});

		$('#kt_search').on('click', function(e) {
			e.preventDefault();
			
			var params = {};
			$('.datatable-input').each(function() {
				//console.log($(this));
				var i = $(this).data('col-index');
				if (params[i]) {
					params[i] += '|' + $(this).val();
				}
				else {
					params[i] = $(this).val();
				}
			});
			$.each(params, function(i, val) {
				// apply search params to datatable
				table.column(i).search(val ? val : '', false, false);
			});

			table.table().draw();
		});

		$('#kt_reset').on('click', function(e) {
			e.preventDefault();
			$('.datatable-input').each(function() {
				if($(this).prop("tagName") == 'INPUT'){
					$(this).val('');
				}else if($(this).prop("tagName") == 'SELECT'){
					$(this).val(null).change();
				}
				
				table.column($(this).data('col-index')).search('', false, false);
			});

			table.table().draw();
		});
	};

	var initTablePenalties = function() {

		// begin first table
		var table = $('#kt_datatable_penalties').DataTable({

			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,

			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[0, 'desc']],
            language: {
                "lengthMenu": "Her sayfada _MENU_ kayıt göster",
                "info": "Sayfa: _PAGE_/_PAGES_ (_MAX_ kayıt filtrelendi)",
                "infoEmpty": "Kayıt Yok.",
                "zeroRecords": "Kayıt Yok.",
                "infoFiltered": "(Toplam _MAX_ kayıttan filtrelendi)",
                "processing": "Yükleniyor...",
                "search": "Arama:",
            },

			ajax: {
				url: HOST_URL + '/getPenalties',
				type: 'POST',
				headers: { 'X-CSRF-TOKEN': $('#token').val() },
				data: {
					columnsDef: ['id', 'season', 'match', 'team', 'player', 'polymorphic_ctype_id', 'type', 'actions'],
				},
			},
			columns: [
				{name: 'id'},
				{name: 'season'},
				{name: 'match'},
				{name: 'team'},
				{name: 'player'},
				{name: 'polymorphic_ctype_id'},
				{name: 'type'},
				{name: 'actions', responsivePriority: -1},
			],
			columnDefs: [

				{targets: 0, title: 'ID', visible: false},
				{targets: 1, title: 'Sezon'},
				{targets: 2, title: 'Maç'},
				{targets: 3, title: 'Takım'},
				{targets: 4, title: 'Oyuncu'},
				{targets: 5, title: 'Ceza Türü'},
				{targets: 6, visible: false},
				{
					targets: -1,
					title: 'İşlemler',
					orderable: false,
					width: '150px',
					render: function(data, type, full, meta) {
						return '\
						<a href="'+full[6]+'_penalties/view/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_view_tactic" title="Görüntüle">\
							<i class="fas fa-eye"></i>\
						</a>\
						<a href="'+full[6]+'_penalties/edit/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_change_tactic" title="Düzenle">\
							<i class="fas fa-edit"></i>\
						</a>\
						<a href="'+full[6]+'_penalties/delete/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_delete_tactic" title="Sil">\
							<i class="fas fa-trash-alt"></i>\
						</a>\
						';
					},
				},
			],
			drawCallback: function (settings) {
				viButTactic();
		    }
		});

		$('#kt_search').on('click', function(e) {
			e.preventDefault();
			
			var params = {};
			$('.datatable-input').each(function() {
				//console.log($(this));
				var i = $(this).data('col-index');
				if (params[i]) {
					params[i] += '|' + $(this).val();
				}
				else {
					params[i] = $(this).val();
				}
			});
			$.each(params, function(i, val) {
				// apply search params to datatable
				table.column(i).search(val ? val : '', false, false);
			});

			table.table().draw();
		});

		$('#kt_reset').on('click', function(e) {
			e.preventDefault();
			$('.datatable-input').each(function() {
				if($(this).prop("tagName") == 'INPUT'){
					$(this).val('');
				}else if($(this).prop("tagName") == 'SELECT'){
					$(this).val(null).change();
				}
				
				table.column($(this).data('col-index')).search('', false, false);
			});

			table.table().draw();
		});
	};

	var initTableNews = function() {

		// begin first table
		var table = $('#kt_datatable_news').DataTable({

			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,

			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[0, 'desc']],
            			
			language: {
                "lengthMenu": "Her sayfada _MENU_ kayıt göster",
                "info": "Sayfa: _PAGE_/_PAGES_ (_MAX_ kayıt filtrelendi)",
                "infoEmpty": "Kayıt Yok.",
                "zeroRecords": "Kayıt Yok.",
                "infoFiltered": "(Toplam _MAX_ kayıttan filtrelendi)",
                "processing": "Yükleniyor...",
                "search": "Arama:",
            },

			ajax: {
				url: HOST_URL + '/getNews',
				type: 'POST',
				headers: { 'X-CSRF-TOKEN': $('#token').val() },
				data: {
					columnsDef: ['id', 'name', 'order', 'actions'],
				},
			},
			columns: [
				{name: 'id'},
				{name: 'name'},
				{name: 'actions', responsivePriority: -1},
			],
			columnDefs: [

				{targets: 0, title: 'ID', visible: false},
				{targets: 1, title: 'Ad'},
				{
					targets: -1,
					title: 'İşlem',
					orderable: false,
					width: '150px',
					render: function(data, type, full, meta) {
						return '\
						<a href="news/view/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_view_tactic" title="Görüntüle">\
							<i class="fas fa-eye"></i>\
						</a>\
						<a href="news/edit/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_change_tactic" title="Düzenle">\
							<i class="fas fa-edit"></i>\
						</a>\
						<a href="news/delete/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_delete_tactic" title="Sil">\
							<i class="fas fa-trash-alt"></i>\
						</a>\
						';
					},
				},
			],
			drawCallback: function (settings) {
				viButTactic();
		    }
		});

		$('#kt_search').on('click', function(e) {
			e.preventDefault();
			
			var params = {};
			$('.datatable-input').each(function() {
				//console.log($(this));
				var i = $(this).data('col-index');
				if (params[i]) {
					params[i] += '|' + $(this).val();
				}
				else {
					params[i] = $(this).val();
				}
			});
			$.each(params, function(i, val) {
				// apply search params to datatable
				table.column(i).search(val ? val : '', false, false);
			});

			table.table().draw();
		});

		$('#kt_reset').on('click', function(e) {
			e.preventDefault();
			$('.datatable-input').each(function() {
				if($(this).prop("tagName") == 'INPUT'){
					$(this).val('');
				}else if($(this).prop("tagName") == 'SELECT'){
					$(this).val(null).change();
				}
				
				table.column($(this).data('col-index')).search('', false, false);
			});

			table.table().draw();
		});
	};

	var initTableNewsTypes = function() {

		// begin first table
		var table = $('#kt_datatable_news_types').DataTable({

			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,

			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[2, 'desc']],
            			
			language: {
                "lengthMenu": "Her sayfada _MENU_ kayıt göster",
                "info": "Sayfa: _PAGE_/_PAGES_ (_MAX_ kayıt filtrelendi)",
                "infoEmpty": "Kayıt Yok.",
                "zeroRecords": "Kayıt Yok.",
                "infoFiltered": "(Toplam _MAX_ kayıttan filtrelendi)",
                "processing": "Yükleniyor...",
                "search": "Arama:",
            },

			ajax: {
				url: HOST_URL + '/getNewsTypes',
				type: 'POST',
				headers: { 'X-CSRF-TOKEN': $('#token').val() },
				data: {
					columnsDef: ['id', 'name', 'order', 'actions'],
				},
			},
			columns: [
				{name: 'id'},
				{name: 'name'},
				{name: 'order'},
				{name: 'actions', responsivePriority: -1},
			],
			columnDefs: [

				{targets: 0, title: 'ID', visible: false},
				{targets: 1, title: 'Ad'},
				{targets: 2, title: 'Sıra'},
				{
					targets: -1,
					title: 'İşlem',
					orderable: false,
					width: '150px',
					render: function(data, type, full, meta) {
						return '\
						<a href="news_types/view/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_view_tactic" title="Görüntüle">\
							<i class="fas fa-eye"></i>\
						</a>\
						<a href="news_types/edit/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_change_tactic" title="Düzenle">\
							<i class="fas fa-edit"></i>\
						</a>\
						<a href="news_types/delete/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_delete_tactic" title="Sil">\
							<i class="fas fa-trash-alt"></i>\
						</a>\
						';
					},
				},
			],
			drawCallback: function (settings) {
				viButTactic();
		    }
		});

		$('#kt_search').on('click', function(e) {
			e.preventDefault();
			
			var params = {};
			$('.datatable-input').each(function() {
				//console.log($(this));
				var i = $(this).data('col-index');
				if (params[i]) {
					params[i] += '|' + $(this).val();
				}
				else {
					params[i] = $(this).val();
				}
			});
			$.each(params, function(i, val) {
				// apply search params to datatable
				table.column(i).search(val ? val : '', false, false);
			});

			table.table().draw();
		});

		$('#kt_reset').on('click', function(e) {
			e.preventDefault();
			$('.datatable-input').each(function() {
				if($(this).prop("tagName") == 'INPUT'){
					$(this).val('');
				}else if($(this).prop("tagName") == 'SELECT'){
					$(this).val(null).change();
				}
				
				table.column($(this).data('col-index')).search('', false, false);
			});

			table.table().draw();
		});
	};

	var initTableAgenda = function() {

		// begin first table
		var table = $('#kt_datatable_agenda').DataTable({

			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,

			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[0, 'desc']],
            			
			language: {
                "lengthMenu": "Her sayfada _MENU_ kayıt göster",
                "info": "Sayfa: _PAGE_/_PAGES_ (_MAX_ kayıt filtrelendi)",
                "infoEmpty": "Kayıt Yok.",
                "zeroRecords": "Kayıt Yok.",
                "infoFiltered": "(Toplam _MAX_ kayıttan filtrelendi)",
                "processing": "Yükleniyor...",
                "search": "Arama:",
            },

			ajax: {
				url: HOST_URL + '/getAgenda',
				type: 'POST',
				headers: { 'X-CSRF-TOKEN': $('#token').val() },
				data: {
					columnsDef: ['id', 'name', 'link', 'order', 'actions'],
				},
			},
			columns: [
				{name: 'id'},
				{name: 'name'},
				{name: 'link'},
				{name: 'actions', responsivePriority: -1},
			],
			columnDefs: [

				{targets: 0, title: 'ID', visible: false},
				{targets: 1, title: 'Ad'},
				{targets: 2, title: 'Link'},
				{
					targets: -1,
					title: 'İşlem',
					orderable: false,
					width: '150px',
					render: function(data, type, full, meta) {
						return '\
						<a href="agenda/view/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_view_tactic" title="Görüntüle">\
							<i class="fas fa-eye"></i>\
						</a>\
						<a href="agenda/edit/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_change_tactic" title="Düzenle">\
							<i class="fas fa-edit"></i>\
						</a>\
						<a href="agenda/delete/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_delete_tactic" title="Sil">\
							<i class="fas fa-trash-alt"></i>\
						</a>\
						';
					},
				},
			],
			drawCallback: function (settings) {
				viButTactic();
		    }
		});

		$('#kt_search').on('click', function(e) {
			e.preventDefault();
			
			var params = {};
			$('.datatable-input').each(function() {
				//console.log($(this));
				var i = $(this).data('col-index');
				if (params[i]) {
					params[i] += '|' + $(this).val();
				}
				else {
					params[i] = $(this).val();
				}
			});
			$.each(params, function(i, val) {
				// apply search params to datatable
				table.column(i).search(val ? val : '', false, false);
			});

			table.table().draw();
		});

		$('#kt_reset').on('click', function(e) {
			e.preventDefault();
			$('.datatable-input').each(function() {
				if($(this).prop("tagName") == 'INPUT'){
					$(this).val('');
				}else if($(this).prop("tagName") == 'SELECT'){
					$(this).val(null).change();
				}
				
				table.column($(this).data('col-index')).search('', false, false);
			});

			table.table().draw();
		});
	};

	var initTableTags = function() {

		// begin first table
		var table = $('#kt_datatable_tags').DataTable({

			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,

			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[0, 'desc']],
            			
			language: {
                "lengthMenu": "Her sayfada _MENU_ kayıt göster",
                "info": "Sayfa: _PAGE_/_PAGES_ (_MAX_ kayıt filtrelendi)",
                "infoEmpty": "Kayıt Yok.",
                "zeroRecords": "Kayıt Yok.",
                "infoFiltered": "(Toplam _MAX_ kayıttan filtrelendi)",
                "processing": "Yükleniyor...",
                "search": "Arama:",
            },

			ajax: {
				url: HOST_URL + '/getTags',
				type: 'POST',
				headers: { 'X-CSRF-TOKEN': $('#token').val() },
				data: {
					columnsDef: ['id', 'name', 'actions'],
				},
			},
			columns: [
				{name: 'id'},
				{name: 'name'},
				{name: 'actions', responsivePriority: -1},
			],
			columnDefs: [

				{targets: 0, title: 'ID', visible: false},
				{targets: 1, title: 'Ad'},
				{targets: 2, title: 'Sıra'},
				{
					targets: -1,
					title: 'İşlem',
					orderable: false,
					width: '150px',
					render: function(data, type, full, meta) {
						return '\
						<a href="tags/view/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_view_tactic" title="Görüntüle">\
							<i class="fas fa-eye"></i>\
						</a>\
						<a href="tags/edit/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_change_tactic" title="Düzenle">\
							<i class="fas fa-edit"></i>\
						</a>\
						<a href="tags/delete/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_delete_tactic" title="Sil">\
							<i class="fas fa-trash-alt"></i>\
						</a>\
						';
					},
				},
			],
			drawCallback: function (settings) {
				viButTactic();
		    }
		});

		$('#kt_search').on('click', function(e) {
			e.preventDefault();
			
			var params = {};
			$('.datatable-input').each(function() {
				//console.log($(this));
				var i = $(this).data('col-index');
				if (params[i]) {
					params[i] += '|' + $(this).val();
				}
				else {
					params[i] = $(this).val();
				}
			});
			$.each(params, function(i, val) {
				// apply search params to datatable
				table.column(i).search(val ? val : '', false, false);
			});

			table.table().draw();
		});

		$('#kt_reset').on('click', function(e) {
			e.preventDefault();
			$('.datatable-input').each(function() {
				if($(this).prop("tagName") == 'INPUT'){
					$(this).val('');
				}else if($(this).prop("tagName") == 'SELECT'){
					$(this).val(null).change();
				}
				
				table.column($(this).data('col-index')).search('', false, false);
			});

			table.table().draw();
		});
	};

	var initTableRules = function() {

		// begin first table
		var table = $('#kt_datatable_rules').DataTable({

			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,
			//stateSave: true,

			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[2, 'asc']],
            language: {
                "lengthMenu": "Her sayfada _MENU_ kayıt göster",
                "info": "Sayfa: _PAGE_/_PAGES_ (_MAX_ kayıt filtrelendi)",
                "infoEmpty": "Kayıt Yok.",
                "zeroRecords": "Kayıt Yok.",
                "infoFiltered": "(Toplam _MAX_ kayıttan filtrelendi)",
                "processing": "Yükleniyor...",
                "search": "Arama:",
            },

			ajax: {
				url: HOST_URL + '/getRules',
				type: 'POST',
				headers: { 'X-CSRF-TOKEN': $('#token').val() },
				data: {
					columnsDef: ['id', 'name', 'order', 'actions'],
				},
			},
			columns: [
				{name: 'id'},
				{name: 'name'},
				{name: 'order'},
				{name: 'actions', responsivePriority: -1},
			],
			columnDefs: [

				{targets: 0, title: 'ID', visible: false},
				{targets: 1, title: 'Ad'},
				{targets: 2, title: 'Sıra'},
				{
					targets: -1,
					title: 'İşlem',
					orderable: false,
					width: '150px',
					render: function(data, type, full, meta) {
						return '\
						<a href="rules/view/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_view_pointtype" title="Görüntüle">\
							<i class="fas fa-eye"></i>\
						</a>\
						<a href="rules/edit/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_change_pointtype" title="Düzenle">\
							<i class="fas fa-edit"></i>\
						</a>\
						';
						/*
						<a href="rules/delete/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_delete_pointtype" title="Sil">\
							<i class="fas fa-trash-alt"></i>\
						</a>\
						';
						*/
					},
				},

			],
			drawCallback: function (settings) {
				viButPointType();
		    }
		});
		/*
		$('#kt_search').on('click', function(e) {
			e.preventDefault();
			
			var params = {};
			$('.datatable-input').each(function() {
				//console.log($(this));
				var i = $(this).data('col-index');
				if (params[i]) {
					params[i] += '|' + $(this).val();
				}
				else {
					params[i] = $(this).val();
				}
			});
			$.each(params, function(i, val) {
				// apply search params to datatable
				table.column(i).search(val ? val : '', false, false);
			});

			table.table().draw();
		});

		$('#kt_reset').on('click', function(e) {
			e.preventDefault();
			$('.datatable-input').each(function() {
				if($(this).prop("tagName") == 'INPUT'){
					$(this).val('');
				}else if($(this).prop("tagName") == 'SELECT'){
					$(this).val(null).change();
				}
				
				table.column($(this).data('col-index')).search('', false, false);
			});

			table.table().draw();
		});
		*/
	};

	return {

		//main function to initiate the module
		init: function() {
			initTableUsers();
			initTablePlayerList();
			initTableRoles();
			initTableConferences();
			initTableProvinces();
			initTableDistricts();
			initTableGrounds();
			initTableGroundProperties();
			initTableGroundHour();
			initTableLeagues();
			initTableLeagueGalleries();
			initTableLeagueGalleryItems();
			initTableChampions();
			initTableSeasons();
			initTableSeasonsAddBonus();
			initTableTeams();
			initTableTeams_Seasons();
			initTableTeams_Players();
			initTableMatches();
			initTableMatchPlayers();
			initTableMatchActions();
			initTableMatchPanoramas();
			initTableReservations();
			initTableEliminationTree();
			initTableFixtureGroup();
			initTablePlayerPositions();
			initTablePointTypes();
			initTableTactics();
			initTableTeamPotentials();
			initTableTeamSatisfactions();
			initTableTeamSatisfactionsJogo();
			initTableBanners();
			initTablePanoramaTypes();
			initTableMonthlyPanormas();
			initTablePenaltyTypes();
			initTablePenalties();
			initTableNews();
			initTableNewsTypes();
			initTableAgenda();
			initTableTags();
			initTableRules();
		},

	};

}();



jQuery(document).ready(function() {
	KTDatatablesDataSourceAjaxServer.init();
});
