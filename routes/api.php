<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['cors', 'json.response']], function () {

	Route::get('sosyalligtrigger', 'AjaxApiController@sosyalligtrigger');

	Route::post('auth/token', 'Auth\ApiAuthController@login');
	Route::post('new_password_reset', 'PasswordResetController@changeMailCreate');
	Route::post('password_reset', 'PasswordResetController@create');
	Route::post('password_reset/validate_token', 'PasswordResetController@find');
	Route::post('password_reset/confirm/', 'PasswordResetController@reset');
	Route::post('tckconfirm/', 'AjaxApiController@TCKConfirm');

	//Route::post('password_reset', 'AjaxApiController@passwordReset');
	//Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
	//Route::post('register','Auth\ApiAuthController@register');

	Route::get('provinces', 'AjaxApiController@provinces');
	Route::get('provinces/{id}/districts', 'AjaxApiController@districts');
	Route::get('ground-properties', 'AjaxApiController@groProperties');
	Route::get('grounds', 'AjaxApiController@grounds');
	Route::get('grounds/{slug}', 'AjaxApiController@groundDetails');
	
	Route::get('positions', 'AjaxApiController@positions');

	Route::get('news/', 'AjaxApiController@news');
	Route::get('news/tv', 'AjaxApiController@newsTv');
	Route::get('news/{id}', 'AjaxApiController@newsDetail');

	Route::get('blog', 'AjaxApiController@blog');
	Route::get('blog/{slug}', 'AjaxApiController@blogDetail');
	Route::get('search', 'AjaxApiController@search');

	Route::post('user-create', 'AjaxApiController@userCreate');
	Route::post('send_new_sms', 'AjaxApiController@sendNewSms');
	Route::post('me/phone_confirm', 'AjaxApiController@phoneConfirm');
	Route::post('hesap_kurtarma', 'AjaxApiController@hesapKurtarma');
	Route::post('hesap_dogrula', 'AjaxApiController@hesapDogrula');



	Route::get('leagues', 'AjaxApiController@leaguesAll');
	Route::get('leagues/{id}', 'AjaxApiController@leagues');
	Route::get('leagues/{id}/seasons', 'AjaxApiController@leaSeasons');
	Route::get('leagues/{id}/seasons/{sid}/calendar', 'AjaxApiController@leaSeaCalendar');

	Route::get('leagues/{id}/seasons/{sid}/conferences', 'AjaxApiController@leaSeaConferences');
	Route::get('leagues/{id}/seasons/{sid}/conference_points', 'AjaxApiController@leaSeaConferencePoints');

	Route::get('leagues/{id}/seasons/{sid}/groups', 'AjaxApiController@leaSeaGroups');
	Route::get('leagues/{id}/seasons/{sid}/group_points/', 'AjaxApiController@leaSeaGroupPoints');

	Route::get('leagues/{id}/seasons/{sid}/temsilci_matches', 'AjaxApiController@leaSeaTemsilciMatches');
	Route::get('leagues/{id}/seasons/{sid}/recent_matches', 'AjaxApiController@leaSeaRecentMatches');
	Route::get('leagues/{id}/seasons/{sid}/recent_groupmatches', 'AjaxApiController@leaSeaRecentGroupMatches');
	Route::get('leagues/{id}/seasons/{sid}/only_groupmatches/{gid}', 'AjaxApiController@leaSeaRecentGroupMatches2');
	Route::get('leagues/{id}/seasons/{sid}/matches', 'AjaxApiController@leaSeaMatches');

	Route::get('leagues/{id}/seasons/{sid}/live_matches', 'AjaxApiController@leaSeaLiveMatches');
	Route::get('leagues/{id}/seasons/{sid}/most_valuable_players', 'AjaxApiController@leaSeaMostValuablePlayers');
	Route::get('leagues/{id}/seasons/{sid}/temsilci_valuable_players', 'AjaxApiController@leaSeaTemsilciValuablePlayers');

	Route::get('leagues/{id}/seasons/{sid}/stats_player', 'AjaxApiController@leaSeaStatsPlayer');
	Route::get('leagues/{id}/seasons/{sid}/stats_player_ps', 'AjaxApiController@leaSeaStatsPlayerPS');
	Route::get('leagues/{id}/seasons/{sid}/stats_player_card', 'AjaxApiController@leaSeaStatsPlayerCard');
	Route::get('leagues/{id}/seasons/{sid}/stats_team', 'AjaxApiController@leaSeaStatsTeam');
	
	Route::get('leagues/{id}/seasons/{sid}/penalties', 'AjaxApiController@leaSeaPenalties');

	Route::get('leagues/{id}/seasons/{sid}/tournament_summary', 'AjaxApiController@leaSeaTournamentSummary');

	Route::get('leagues/{id}/seasons/{sid}/team_points', 'AjaxApiController@leaSeaTeamPoints');
	Route::get('leagues/{id}/seasons/{sid}/gallery', 'AjaxApiController@leaSeaGallery');
	Route::get('leagues/{id}/seasons/{sid}/marking_tags', 'AjaxApiController@leaSeaMarkingTags');

	Route::get('leagues/{id}/seasons/{sid}/gallery_before', 'AjaxApiController@leaSeaGalleryBefore');
	Route::get('leagues/{id}/seasons/{sid}/gallery_now', 'AjaxApiController@leaSeaGalleryNow');
	Route::get('leagues/{id}/seasons/{sid}/gallery_after', 'AjaxApiController@leaSeaGalleryAfter');

	Route::get('leagues/{id}/seasons/{sid}/getdata_info', 'AjaxApiController@leaSeaGetDataInfo');
	Route::get('leagues/{id}/seasons/{sid}/getdata_before', 'AjaxApiController@leaSeaGetDataBefore');
	Route::get('leagues/{id}/seasons/{sid}/getdata_after', 'AjaxApiController@leaSeaGetDataAfter');

	Route::get('leagues/{id}/seasons/{sid}/panoramas', 'AjaxApiController@monthlyPanorama');
	Route::get('leagues/{id}/seasons/{sid}/panoramadates', 'AjaxApiController@monthlyPanoramaDates');


	Route::get('leagues/{id}/galleries', 'AjaxApiController@leaGalleries');
	Route::get('leagues/{id}/home_galleries', 'AjaxApiController@leaHomeGalleries');
	Route::get('leagues/{id}/championships', 'AjaxApiController@leaChampionships');

	Route::get('leagues/{id}/transfers', 'AjaxApiController@leaTransfers');

	Route::get('leagues/{id}/transfer_market', 'AjaxApiController@leaTransferMarket');



	/****************************/
	/*********** MAÇ ************/

	Route::get('matches/{id}', 'AjaxApiController@matches');
	Route::get('matches/{id}/video', 'AjaxApiController@matchVideo');
	Route::get('matches/{id}/actions', 'AjaxApiController@matchAction');
	Route::get('matches/{id}/players', 'AjaxApiController@matchPlayer');
	Route::get('matches/{id}/photos', 'AjaxApiController@matchPhoto');
	Route::get('matches/{id}/panoramas', 'AjaxApiController@matchPanorama');
	Route::get('matches/{id}/press_conference', 'AjaxApiController@matchPressConference');

	/*********** MAÇ ************/
	/****************************/

	Route::get('banner/{str}', 'AjaxApiController@getBanner');
	Route::get('rules/{type}', 'AjaxApiController@getRules');


});

Route::group(['middleware' => 'auth:api'], function() {

	/**** API ****/
	Route::group(['prefix' => '/app'], function () {
		Route::get('/coordinator-matches/{p?}', 'AjaxAppApiController@getAppCoordinatorMatches');
		Route::get('/coordinator-matches/{team_id}/team_players', 'AjaxAppApiController@getAppTeamPlayers');

		Route::get('/coordinator-matches/{match_id}/players', 'AjaxAppApiController@getAppPlayer');
		Route::post('/coordinator-matches/{match_id}/players', 'AjaxAppApiController@setAppPlayer');
		
		Route::post('/coordinator-matches/{match_id}/actions', 'AjaxAppApiController@setAppAction');
		Route::put('/coordinator-matches/{match_id}/video', 'AjaxAppApiController@setAppVideo');

		Route::get('/coordinator-matches/{match_id}/panorama', 'AjaxAppApiController@getAppPanorama');
		Route::post('/coordinator-matches/{match_id}/panorama', 'AjaxAppApiController@setAppPanorama');

		Route::get('/coordinator-matches/{match_id}/press', 'AjaxAppApiController@getAppPress');
		Route::post('/coordinator-matches/{match_id}/press', 'AjaxAppApiController@setAppPress');

		Route::get('/coordinator-matches/{match_id}/photo', 'AjaxAppApiController@getAppPhoto');
		Route::post('/coordinator-matches/{match_id}/photo', 'AjaxAppApiController@setAppPhoto');


		Route::get('/coordinator-matches/{player_id}/player_photo', 'AjaxAppApiController@plaPhoto');
		Route::post('/coordinator-matches/{player_id}/player_photo', 'AjaxAppApiController@plaPhotoSave');


		Route::get('/panorama-types', 'AjaxAppApiController@getAppPanoramaTypes');

		Route::get('/video-creator/player/{type}/{offset}/{limit}', 'AjaxAppApiController@getAppPlayerStats');
		Route::post('/video-creator/player/addvideo', 'AjaxAppApiController@postAppAddVideoPlayer');

		Route::put('/coordinator-matches/{match_id}/matchcompleted', 'AjaxAppApiController@setAppMatchCompleted');


		Route::get('/coordinator-matches/{match_id}/tmplayers/{team_id?}', 'AjaxAppApiController@getAppTMPlayer');
		Route::post('/coordinator-matches/{match_id}/tmplayers/{team_id?}', 'AjaxAppApiController@setAppTMPlayer');

		Route::get('/coordinator-matches/{match_id}/getmatchstatus', 'AjaxAppApiController@getAppMatchStatus');
		Route::post('/coordinator-matches/{match_id}/setmatchstatus', 'AjaxAppApiController@setAppMatchStatus');

		Route::get('/coordinator-matches/{match_id}/getmatchaction', 'AjaxAppApiController@getAppMatchAction');
		Route::post('/coordinator-matches/{match_id}/setmatchaction', 'AjaxAppApiController@setAppMatchAction');
		Route::delete('/coordinator-matches/{match_id}/deletematchaction/{action_id}', 'AjaxAppApiController@deleteAppMatchAction');

		Route::post('/coordinator-matches/{match_id}/setliveembedurl', 'AjaxAppApiController@setAppMatchLiveVideo');
		Route::post('/coordinator-matches/{match_id}/setsummaryembedurl', 'AjaxAppApiController@setAppMatchSummaryVideo');

		Route::get('/coordinator-matches/getallpositions', 'AjaxAppApiController@getAppAllPositions');




	});
	/**** API ****/

	//Route::post('logout', 'Auth\ApiAuthController@logout');
	Route::get('me', 'AjaxApiController@me');
	Route::put('me', 'AjaxApiController@meUpdate');

	Route::post('me/change_password', 'Auth\ApiAuthController@changePassword');

	Route::post('sendinfoshare', 'AjaxApiController@sendInfoShare');

	Route::get('provinces/{id}/league', 'AjaxApiController@proLeague');
	Route::get('agenda', 'AjaxApiController@agenda');
	Route::get('tactics', 'AjaxApiController@tactics');

	Route::get('elimination_trees', 'AjaxApiController@eliminationTrees');
	Route::get('elimination_trees/{etid}', 'AjaxApiController@eliminationTreeId');

	Route::post('register-player', 'AjaxApiController@registerPlayer');


	/****************************/
	/*********** LİG ************/
	
	Route::get('captain-transfers', 'AjaxApiController@captainTransfer');
	Route::post('captain-transfers', 'AjaxApiController@captainTransferPost');

	Route::delete('captain-transfers/{id}', 'AjaxApiController@captainTransferDelete');

	Route::get('player-transfers', 'AjaxApiController@playerTransfer');
	Route::post('player-transfers', 'AjaxApiController@playerTransferPost');

	Route::post('player-transfers/{id}/accept/', 'AjaxApiController@playerTransferAccept');
	Route::post('player-transfers/{id}/deny/', 'AjaxApiController@playerTransferDeny');

	Route::get('captain-team', 'AjaxApiController@captainTeam');
	Route::get('captain-team/transfer_status', 'AjaxApiController@transferStatus');

	Route::post('captain-team/request_termination', 'AjaxApiController@requestTermination');
	Route::post('captain-team/squad_lock', 'AjaxApiController@squadLock');
	Route::post('captain-team/logo_update', 'AjaxApiController@captainTeamLogoUpdate');
	
	/*********** LİG ************/
	/****************************/

	/****************************/
	/********** TAKIM ***********/

	Route::post('teams', 'AjaxApiController@teamCreate');

	Route::get('teams/{id}', 'AjaxApiController@teams');
	Route::get('teams/{id}/seasons', 'AjaxApiController@teaSeasons');
	Route::get('teams/{id}/featured_players', 'AjaxApiController@teaFeaPlayers');
	Route::get('teams/{id}/matches', 'AjaxApiController@teaMatches');
	Route::get('teams/{id}/match_summaries', 'AjaxApiController@teaMatchSummaries');
	Route::get('teams/{id}/season_performance', 'AjaxApiController@teaSeasonPerformance');
	Route::get('teams/{id}/last_match_squad', 'AjaxApiController@teaLastMatchSquad');

	Route::get('teams/{id}/recent_matches', 'AjaxApiController@teaRecentMatches');

	Route::get('teams/{id}/players', 'AjaxApiController@teaPlayers');
	Route::get('teams/{id}/penalties', 'AjaxApiController@teaPenalties');

	Route::get('teams/{id}/press_conferences', 'AjaxApiController@teaPressConferences');
	Route::get('teams/{id}/championships', 'AjaxApiController@teaChampionships');

	Route::post('teams/{id}/assign_captain', 'AjaxApiController@teaAssignCaptain');
	Route::delete('teams/{id}/release_player', 'AjaxApiController@teaReleasePlayer');

	Route::get('teams/{id}/social', 'AjaxApiController@teaSocial');
	Route::put('teams/{id}', 'AjaxApiController@teaSocialUpdate');


	/********** TAKIM ***********/
	/****************************/

	/****************************/
	/********* OYUNCU ***********/
	
	Route::get('players', 'AjaxApiController@playerSearch');

	Route::get('players/{id}', 'AjaxApiController@players');
	Route::get('players/{id}/seasons', 'AjaxApiController@plaSeason');
	Route::get('players/{id}/last_ratings', 'AjaxApiController@plaLastRatings');
	Route::get('players/{id}/goals', 'AjaxApiController@plaGoals');
	Route::get('players/{id}/competition', 'AjaxApiController@plaCompetition');
	Route::get('players/{id}/performance_history', 'AjaxApiController@plaPerformanceHistory');
	Route::get('players/{id}/rating', 'AjaxApiController@plaRating');
	Route::get('players/{id}/matches', 'AjaxApiController@plaMatches');
	Route::get('players/{id}/press_conferences', 'AjaxApiController@plaPressConferences');
	Route::get('players/{id}/playervideo', 'AjaxApiController@plaPlayervideo');
	Route::post('players/{id}/photo', 'AjaxApiController@plaPhotoSave');

	/********* OYUNCU ***********/
	/****************************/


	Route::get('ground-tables', 'AjaxApiController@groTables');

	Route::post('reservations', 'AjaxApiController@reservations');
	Route::post('reservation-offers', 'AjaxApiController@reservationOffers');

	Route::delete('reservations/{rid}', 'AjaxApiController@reservationsDelete');
	Route::delete('reservation-offers/{oid}', 'AjaxApiController@reservationOffersDelete');

	



});

Route::group(['middleware' => 'api'], function() {

});