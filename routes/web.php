<?php

/*
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
$permission = Permission::create(['name' => 'editaaaa']);
die;
*/

// phpinfo(); die;
Route::redirect('/', '/dashboard', 301);
Route::get('/dashboard', 'HomeController@index');


Route::get('/info', 'AutomationController@index');
Route::get('/datenow', 'AutomationController@dateNow');
Route::get('/addpermission', 'AutomationController@addPermission');

Route::get('/ai/player/{pid}', 'AutomationController@aiPlayer');

Route::get('video-creator/player/{type}/{offset}/{limit}', 'AjaxAppApiController@getAppPlayerStats');


Route::get('/cropimage/{folder}/{image_url}', 'AjaxAppApiController@cropImages');

Auth::routes();

Route::get('/register', 'HomeController@dashboard');
Route::post('/password/email', 'HomeController@dashboard');
Route::get('/password/reset', 'HomeController@dashboard');

Route::get('/dashboard', 'HomeController@dashboard')->name('dashboard');

Route::get('/date/{param}', 'AutomationController@checkDate');
Route::get('/dateModel/{param}', 'AutomationController@checkModelDate');

//Route::get('/playerdegerhesapla/{player_id}', 'AutomationController@playerDegerHesapla');
Route::get('/playerdegerguncelle/{player_id}', 'AutomationController@playerDegerGuncelle');
Route::get('/degeruygula/{offset}/{limit}', 'AutomationController@degerUygula');

Route::get('/sezonpuanhesapla/{sezon_id}', 'AutomationController@sezonPuanHesapla');
Route::get('/sezonpuanhesapla/{sezon_id}/{mode}', 'AutomationController@sezonPuanHesapla');

Route::get('/sezonistatistikhesapla/{sezon_id}/{offset}/{limit}', 'AutomationController@sezonIstatistikHesapla');
Route::get('/sezonistatistikhesapla/{sezon_id}/{offset}/{limit}/{mode}', 'AutomationController@sezonIstatistikHesapla');

Route::get('/sezonlist', 'AutomationController@sezonList');



Route::get('/playerphotocheck/{offset}/{limit}', 'AutomationController@playerPhotocheck');
Route::get('/teamphotocheck/{offset}/{limit}', 'AutomationController@teamPhotocheck');
Route::get('/matchvideocheck/{offset}/{limit}', 'AutomationController@matchVideocheck');
Route::get('/matchvideocheck2/{offset}/{limit}', 'AutomationController@matchVideocheck2');
Route::get('/matchpanoramavideocheck/{offset}/{limit}', 'AutomationController@matchPanoramaVideocheck');
Route::get('/matchpressconvideocheck/{offset}/{limit}', 'AutomationController@matchPressConVideocheck');

Route::get('/matchactionvideocheck/{offset}/{limit}', 'AutomationController@matchActionVideocheck');


Route::get('/takimlariac', 'AutomationController@takimlariAc');

Route::get('/cron/rbkirmizi', 'AutomationController@rbkirmizi');
Route::get('/cron/smstest', 'AutomationController@smsTest');

Route::get('/videourldegistir/{offset}/{limit}', 'AutomationController@videoUrlDegistir');
Route::get('/smstest', 'AutomationController@smsTest');

//RoleProvince
Route::group(['prefix' => '/users'], function () {
	Route::get('/', 'UserController@index')->name('users');

	Route::get('/view/{id}', 'UserController@crud');
	Route::get('/add', 'UserController@crud');
	Route::get('/edit/{id}', 'UserController@crud');
	Route::get('/delete/{id}', 'UserController@crud');
	
	Route::post('/save', 'UserController@save');

	Route::get('/password/{id}', 'UserController@password');
	Route::post('/save_password', 'UserController@save_password');

	Route::get('/logo_delete/{id}', 'UserController@userPhotoDelete');
	Route::post('/logo_save', 'AjaxController@userPhotoSave');
});


Route::group(['prefix' => '/addoutsideplayer'], function () {
	Route::get('/add/{team_id}', 'UserController@addoutsideplayer_crud');
	Route::post('/save', 'UserController@addoutsideplayer_save');
});

//RoleProvince
Route::group(['prefix' => '/roles'], function () {
	Route::get('/', 'RoleController@index')->name('roles');

	Route::get('/view/{id}', 'RoleController@crud');
	Route::get('/add', 'RoleController@crud');
	Route::get('/edit/{id}', 'RoleController@crud');
	Route::get('/delete/{id}', 'RoleController@crud');
	
	Route::post('/save', 'RoleController@save');

	Route::get('/permission/{id}', 'RoleController@permission');
	Route::post('/permission_save', 'RoleController@permission_save');
});

//RoleProvince
Route::group(['prefix' => '/conferences'], function () {
	Route::get('/', 'ConferenceController@index')->name('conferences');

	Route::get('/view/{id}', 'ConferenceController@crud');
	Route::get('/add', 'ConferenceController@crud');
	Route::get('/edit/{id}', 'ConferenceController@crud');
	Route::get('/delete/{id}', 'ConferenceController@crud');
	
	Route::post('/save', 'ConferenceController@save');
});

//RoleProvince
Route::group(['prefix' => '/provinces'], function () {
	Route::get('/', 'ProvinceController@index')->name('provinces');

	Route::get('/view/{id}', 'ProvinceController@crud');
	Route::get('/add', 'ProvinceController@crud');
	Route::get('/edit/{id}', 'ProvinceController@crud');
	Route::get('/delete/{id}', 'ProvinceController@crud');
	
	Route::post('/save', 'ProvinceController@save');
});

//RoleProvince
Route::group(['prefix' => '/districts'], function () {
	Route::get('/', 'DistrictController@index')->name('districts');

	Route::get('/view/{id}', 'DistrictController@crud');
	Route::get('/add', 'DistrictController@crud');
	Route::get('/edit/{id}', 'DistrictController@crud');
	Route::get('/delete/{id}', 'DistrictController@crud');
	
	Route::post('/save', 'DistrictController@save');
});

//RoleProvince
Route::group(['prefix' => '/grounds'], function () {
	Route::get('/', 'GroundController@index')->name('grounds');

	Route::get('/view/{id}', 'GroundController@crud');
	Route::get('/add', 'GroundController@crud');
	Route::get('/edit/{id}', 'GroundController@crud');
	Route::get('/delete/{id}', 'GroundController@crud');
	Route::get('/hour/{id}', 'GroundController@ground_hour');
	
	Route::post('/save', 'GroundController@save');
});

Route::group(['prefix' => '/ground_properties'], function () {
	Route::get('/', 'GroundController@ground_properties')->name('ground_properties');

	Route::get('/view/{id}', 'GroundController@ground_properties_crud');
	Route::get('/add', 'GroundController@ground_properties_crud');
	Route::get('/edit/{id}', 'GroundController@ground_properties_crud');
	Route::get('/delete/{id}', 'GroundController@ground_properties_crud');
	
	Route::post('/save', 'GroundController@ground_properties_save');
});

Route::group(['prefix' => '/ground_image'], function () {
	Route::post('/save', 'AjaxController@groundImageSave');
	Route::get('/delete/{id}/{giid}', 'GroundController@groundImageDelete');
});

//RoleProvince
Route::group(['prefix' => '/leagues'], function () {
	Route::get('/', 'LeagueController@index')->name('leagues');

	Route::get('/view/{id}', 'LeagueController@crud');
	Route::get('/add', 'LeagueController@crud');
	Route::get('/edit/{id}', 'LeagueController@crud');
	Route::get('/delete/{id}', 'LeagueController@crud');
	
	Route::post('/save', 'LeagueController@save');
});

//RoleProvince
Route::group(['prefix' => '/league_gallery'], function () {
	Route::get('/', 'LeagueController@league_gallery')->name('league_gallery');

	Route::get('/view/{id}', 'LeagueController@crud_league_gallery');
	Route::get('/add', 'LeagueController@crud_league_gallery');
	Route::get('/edit/{id}', 'LeagueController@crud_league_gallery');
	Route::get('/delete/{id}', 'LeagueController@crud_league_gallery');
	
	Route::post('/save', 'LeagueController@save_league_gallery');
});

//RoleProvince
Route::group(['prefix' => '/league_gallery_items'], function () {
	Route::post('/save', 'LeagueController@save_league_gallery_items');
	Route::get('/delete/{id}/{lgiid}', 'LeagueController@delete_league_gallery_items');
});

//RoleProvince
Route::group(['prefix' => '/champions'], function () {
	Route::get('/', 'ChampionController@index')->name('champions');

	Route::get('/view/{id}', 'ChampionController@crud');
	Route::get('/add', 'ChampionController@crud');
	Route::get('/edit/{id}', 'ChampionController@crud');
	Route::get('/delete/{id}', 'ChampionController@crud');
	
	Route::post('/save', 'ChampionController@save');
});

Route::group(['prefix' => '/seasons'], function () {
	Route::get('/', 'SeasonController@index')->name('seasons');

	Route::get('/addbonus', 'SeasonController@addBonus');
	Route::post('/addbonus/save', 'SeasonController@addBonusSave');
});

Route::group(['prefix' => '/seasonsettings'], function () {
	Route::get('/edit/{id}', 'SeasonController@crudSeasonSettings');
	
	Route::post('/save', 'SeasonController@saveSeasonSettings');
});

Route::group(['prefix' => '/seasonsp'], function () {
	Route::get('/view/{id}', 'SeasonController@crudp');
	Route::get('/add', 'SeasonController@crudp');
	Route::get('/edit/{id}', 'SeasonController@crudp');
	Route::get('/delete/{id}', 'SeasonController@crudp');
	
	Route::post('/save', 'SeasonController@savep');
});

Route::group(['prefix' => '/seasonsf'], function () {
	Route::get('/view/{id}', 'SeasonController@crudf');
	Route::get('/add', 'SeasonController@crudf');
	Route::get('/edit/{id}', 'SeasonController@crudf');
	Route::get('/delete/{id}', 'SeasonController@crudf');
	
	Route::post('/save', 'SeasonController@savef');
});

Route::group(['prefix' => '/fixture_group'], function () {
	Route::get('/', 'SharedController@fixture_group')->name('fixture_group');

	Route::get('/view/{id}', 'SharedController@fixture_group_crud');
	Route::get('/add', 'SharedController@fixture_group_crud');
	Route::get('/edit/{id}', 'SharedController@fixture_group_crud');
	Route::get('/delete/{id}', 'SharedController@fixture_group_crud');
	
	Route::post('/save', 'SharedController@fixture_group_save');
	Route::get('/tree/{id}', 'SharedController@fixture_group_tree_crud');
	Route::post('/tree_save', 'SharedController@fixture_group_item_save');
});

Route::group(['prefix' => '/seasonse'], function () {
	Route::get('/view/{id}', 'SeasonController@crude');
	Route::get('/add', 'SeasonController@crude');
	Route::get('/edit/{id}', 'SeasonController@crude');
	Route::get('/delete/{id}', 'SeasonController@crude');
	
	Route::post('/save', 'SeasonController@savee');
});

Route::group(['prefix' => '/elimination_tree'], function () {
	Route::get('/', 'SharedController@elimination_tree')->name('elimination_tree');

	Route::get('/view/{id}', 'SharedController@elimination_tree_crud');
	Route::get('/add', 'SharedController@elimination_tree_crud');
	Route::get('/edit/{id}', 'SharedController@elimination_tree_crud');
	Route::get('/delete/{id}', 'SharedController@elimination_tree_crud');
	
	Route::post('/save', 'SharedController@elimination_tree_save');
	Route::get('/tree/{id}', 'SharedController@elimination_tree_tree_crud');
	Route::post('/tree_save', 'SharedController@elimination_tree_item_save');
});

Route::group(['prefix' => '/season_image'], function () {
	Route::post('/save', 'AjaxController@seasonImageSave');
	Route::post('/desc_save', 'SeasonController@seasonImageDescSave');
	Route::get('/delete/{sid}/{siid}', 'SeasonController@seasonImageDelete');
});


Route::group(['prefix' => '/teams'], function () {
	Route::get('/', 'TeamController@index')->name('teams');

	Route::get('/view/{id}', 'TeamController@crud');
	Route::get('/add', 'TeamController@crud');
	Route::get('/edit/{id}', 'TeamController@crud');
	Route::get('/delete/{id}', 'TeamController@crud');
	
	Route::post('/save', 'TeamController@save');

	Route::get('/setCaptain/{id}/{pid}', 'TeamController@setCaptain');
	Route::get('/releasePlayer/{id}/{pid}', 'TeamController@releasePlayer');
	Route::get('/setPlayOff/{id}/{tsid}', 'TeamController@setPlayOff');
	Route::get('/setSquadLocked/{id}/{tsid}', 'TeamController@setSquadLocked');

	Route::post('/request_termination', 'TeamController@requestTermination');

	Route::get('/addinsideplayer/{id}', 'TeamController@addinsideplayer');
	Route::get('/addinsideplayer_save/{id}/{pid}', 'TeamController@addinsideplayer_save');

	Route::post('/addpoint', 'TeamController@addPoint');
	Route::post('/addtransfer', 'TeamController@addTransfer');

	Route::get('/logo_delete/{id}', 'TeamController@teamLogoDelete');
	Route::get('/cover_delete/{id}', 'TeamController@teamCoverDelete');

	Route::post('/logo_save', 'AjaxController@teamLogoSave');
	Route::post('/cover_save', 'AjaxController@teamCoverSave');

});

Route::group(['prefix' => '/matches'], function () {
	Route::get('/', 'MatchController@index')->name('matches');

	Route::get('/view/{id}', 'MatchController@crud');
	Route::get('/add', 'MatchController@crud');
	Route::get('/edit/{id}', 'MatchController@crud');
	Route::get('/delete/{id}', 'MatchController@crud');

	Route::post('/save', 'MatchController@save');
});

Route::group(['prefix' => '/match_video'], function () {
	Route::post('/save', 'MatchController@match_video_save');
});

Route::group(['prefix' => '/match_player'], function () {
	Route::post('/save', 'MatchController@match_player_save');
	Route::get('/delete/{id}/{mpid}', 'MatchController@match_player_delete');
});

Route::group(['prefix' => '/match_action'], function () {
	Route::post('/save', 'MatchController@match_action_save');
	Route::get('/delete/{id}/{maid}', 'MatchController@match_action_delete');
});

Route::group(['prefix' => '/match_panorama'], function () {
	Route::post('/save', 'MatchController@match_panorama_save');
	Route::get('/delete/{id}/{mpaid}', 'MatchController@match_panorama_delete');
});

Route::group(['prefix' => '/match_image'], function () {
	Route::post('/save', 'AjaxController@matchImageSave');
	Route::post('/desc_save', 'MatchController@matchImageDescSave');
	Route::get('/delete/{mid}/{miid}', 'MatchController@matchImageDelete');
});

Route::group(['prefix' => '/match_press'], function () {
	Route::post('/save', 'MatchController@match_press_save');
});

Route::group(['prefix' => '/reservations'], function () {
	Route::get('/', 'ReservationController@index')->name('reservations');

	Route::get('/view/{id}', 'ReservationController@crud');
	Route::get('/add', 'ReservationController@crud');
	Route::get('/edit/{id}', 'ReservationController@crud');
	Route::get('/delete/{id}', 'ReservationController@crud');

	Route::post('/save', 'ReservationController@save');
});

Route::post('/reservation_offer_convert_match/save', 'ReservationController@reservation_offer_convert_match');

Route::group(['prefix' => '/player_positions'], function () {
	Route::get('/', 'SharedController@player_position')->name('player_positions');

	Route::get('/view/{id}', 'SharedController@player_position_crud');
	Route::get('/add', 'SharedController@player_position_crud');
	Route::get('/edit/{id}', 'SharedController@player_position_crud');
	Route::get('/delete/{id}', 'SharedController@player_position_crud');
	
	Route::post('/save', 'SharedController@player_position_save');
});

Route::group(['prefix' => '/point_types'], function () {
	Route::get('/', 'SharedController@point_type')->name('point_types');

	Route::get('/view/{id}', 'SharedController@point_type_crud');
	Route::get('/add', 'SharedController@point_type_crud');
	Route::get('/edit/{id}', 'SharedController@point_type_crud');
	Route::get('/delete/{id}', 'SharedController@point_type_crud');
	
	Route::post('/save', 'SharedController@point_type_save');
});

Route::group(['prefix' => '/tactics'], function () {
	Route::get('/', 'SharedController@tactic')->name('tactics');

	Route::get('/view/{id}', 'SharedController@tactic_crud');
	Route::get('/add', 'SharedController@tactic_crud');
	Route::get('/edit/{id}', 'SharedController@tactic_crud');
	Route::get('/delete/{id}', 'SharedController@tactic_crud');
	
	Route::post('/save', 'SharedController@tactic_save');
});

Route::group(['prefix' => '/team_potentials'], function () {
	Route::get('/', 'SharedController@team_potential')->name('team_potentials');

	Route::get('/view/{id}', 'SharedController@team_potential_crud');
	Route::get('/add', 'SharedController@team_potential_crud');
	Route::get('/edit/{id}', 'SharedController@team_potential_crud');
	Route::get('/delete/{id}', 'SharedController@team_potential_crud');
	
	Route::post('/save', 'SharedController@team_potential_save');
});

Route::group(['prefix' => '/team_satisfactions'], function () {
	Route::get('/', 'SharedController@team_satisfaction')->name('team_satisfactions');

	Route::get('/view/{id}', 'SharedController@team_satisfaction_crud');
	Route::get('/add', 'SharedController@team_satisfaction_crud');
	Route::get('/edit/{id}', 'SharedController@team_satisfaction_crud');
	Route::get('/delete/{id}', 'SharedController@team_satisfaction_crud');
	
	Route::post('/save', 'SharedController@team_satisfaction_save');
});

Route::group(['prefix' => '/team_satisfactions_jogo'], function () {
	Route::get('/', 'SharedController@team_satisfaction_jogo')->name('team_satisfaction_jogos_jogo');

	Route::get('/view/{id}', 'SharedController@team_satisfaction_jogo_crud');
	Route::get('/add', 'SharedController@team_satisfaction_jogo_crud');
	Route::get('/edit/{id}', 'SharedController@team_satisfaction_jogo_crud');
	Route::get('/delete/{id}', 'SharedController@team_satisfaction_jogo_crud');
	
	Route::post('/save', 'SharedController@team_satisfaction_jogo_save');
});

Route::group(['prefix' => '/banners'], function () {
	Route::get('/', 'BannerController@banners')->name('banners');

	Route::get('/view/{id}', 'BannerController@banner_crud');
	Route::get('/add', 'BannerController@banner_crud');
	Route::get('/edit/{id}', 'BannerController@banner_crud');
	//Route::get('/delete/{id}', 'BannerController@banner_crud');
	
	Route::post('/save', 'BannerController@banner_save');

	Route::get('/image_delete/{id}', 'BannerController@bannerPhotoDelete');
	Route::post('/image_save', 'AjaxController@bannerPhotoSave');

});


Route::group(['prefix' => '/penalties'], function () {
	Route::get('/', 'PenaltyController@index');
});


Route::group(['prefix' => '/player_penalties'], function () {
	Route::get('/view/{id}', 'PenaltyController@player_penalties_crud');
	Route::get('/add', 'PenaltyController@player_penalties_crud');
	Route::get('/edit/{id}', 'PenaltyController@player_penalties_crud');
	Route::get('/delete/{id}', 'PenaltyController@player_penalties_crud');

	Route::post('/save', 'PenaltyController@player_penalties_save');
});

Route::group(['prefix' => '/team_penalties'], function () {
	Route::get('/view/{id}', 'PenaltyController@team_penalties_crud');
	Route::get('/add', 'PenaltyController@team_penalties_crud');
	Route::get('/edit/{id}', 'PenaltyController@team_penalties_crud');
	Route::get('/delete/{id}', 'PenaltyController@team_penalties_crud');

	Route::post('/save', 'PenaltyController@team_penalties_save');
});



Route::group(['prefix' => '/penalty_types'], function () {
	Route::get('/', 'PenaltyController@penalty_types')->name('penalty_types');

	Route::get('/view/{id}', 'PenaltyController@penalty_type_crud');
	Route::get('/add', 'PenaltyController@penalty_type_crud');
	Route::get('/edit/{id}', 'PenaltyController@penalty_type_crud');
	Route::get('/delete/{id}', 'PenaltyController@penalty_type_crud');
	
	Route::post('/save', 'PenaltyController@penalty_type_save');
});







Route::group(['prefix' => '/news'], function () {
	Route::get('/', 'NewsController@index')->name('news');

	Route::get('/view/{id}', 'NewsController@news_crud');
	Route::get('/add', 'NewsController@news_crud');
	Route::get('/edit/{id}', 'NewsController@news_crud');
	Route::get('/delete/{id}', 'NewsController@news_crud');
	
	Route::post('/save', 'NewsController@news_save');
});

Route::group(['prefix' => '/new_image'], function () {
	Route::post('/save', 'AjaxController@newImageSave');

	Route::get('/image_delete/{id}', 'NewsController@newImageDelete');
	//Route::get('/logo_delete/{id}', 'UserController@userPhotoDelete');
});

/*
Route::group(['prefix' => '/match_image'], function () {
	Route::post('/save', 'AjaxController@matchImageSave');
	Route::post('/desc_save', 'MatchController@matchImageDescSave');
	Route::get('/delete/{mid}/{miid}', 'MatchController@matchImageDelete');
});
*/



Route::group(['prefix' => '/news_types'], function () {
	Route::get('/', 'NewsController@news_types')->name('news_types');

	Route::get('/view/{id}', 'NewsController@news_type_crud');
	Route::get('/add', 'NewsController@news_type_crud');
	Route::get('/edit/{id}', 'NewsController@news_type_crud');
	Route::get('/delete/{id}', 'NewsController@news_type_crud');
	
	Route::post('/save', 'NewsController@news_type_save');
});


Route::group(['prefix' => '/tags'], function () {
	Route::get('/', 'NewsController@tags')->name('tags');

	Route::get('/view/{id}', 'NewsController@tags_crud');
	Route::get('/add', 'NewsController@tags_crud');
	Route::get('/edit/{id}', 'NewsController@tags_crud');
	Route::get('/delete/{id}', 'NewsController@tags_crud');
	
	Route::post('/save', 'NewsController@tags_save');
});


Route::group(['prefix' => '/rules'], function () {
	Route::get('/', 'RulesController@index')->name('rules');

	Route::get('/view/{id}', 'RulesController@rules_crud');
	Route::get('/add', 'RulesController@rules_crud');
	Route::get('/edit/{id}', 'RulesController@rules_crud');
	Route::get('/delete/{id}', 'RulesController@rules_crud');
	
	Route::post('/save', 'RulesController@rules_save');
});


//RoleProvince
Route::group(['prefix' => '/agenda'], function () {
	Route::get('/', 'AgendaController@index')->name('agenda');

	Route::get('/view/{id}', 'AgendaController@crud');
	Route::get('/add', 'AgendaController@crud');
	Route::get('/edit/{id}', 'AgendaController@crud');
	Route::get('/delete/{id}', 'AgendaController@crud');
	
	Route::post('/photo_save', 'AjaxController@agendaPhotoSave');
	Route::get('/photo_delete/{id}', 'AgendaController@agendaPhotoDelete');
	
	Route::post('/save', 'AgendaController@save');
});

//RoleProvince
Route::group(['prefix' => '/panorama_types'], function () {
	Route::get('/', 'PanoramaController@panorama_types')->name('panorama_types');

	Route::get('/view/{id}', 'PanoramaController@panorama_type_crud');
	Route::get('/add', 'PanoramaController@panorama_type_crud');
	Route::get('/edit/{id}', 'PanoramaController@panorama_type_crud');
	Route::get('/delete/{id}', 'PanoramaController@panorama_type_crud');
	
	Route::post('/save', 'PanoramaController@panorama_type_save');

	Route::get('/logo_delete/{id}', 'PanoramaController@panorama_type_LogoDelete');
	Route::post('/logo_save', 'AjaxController@panorama_type_LogoSave');
});


//RoleProvince
Route::group(['prefix' => '/monthly_panorama'], function () {
	Route::get('/', 'PanoramaController@monthly_panorama');
});

Route::group(['prefix' => '/monthly_panorama_player'], function () {
	Route::get('/view/{id}', 'PanoramaController@monthly_panorama_player_crud');
	Route::get('/add', 'PanoramaController@monthly_panorama_player_crud');
	Route::get('/edit/{id}', 'PanoramaController@monthly_panorama_player_crud');
	Route::get('/delete/{id}', 'PanoramaController@monthly_panorama_player_crud');
	
	Route::post('/save', 'PanoramaController@monthly_panorama_player_save');
});

Route::group(['prefix' => '/monthly_panorama_team'], function () {
	Route::get('/view/{id}', 'PanoramaController@monthly_panorama_team_crud');
	Route::get('/add', 'PanoramaController@monthly_panorama_team_crud');
	Route::get('/edit/{id}', 'PanoramaController@monthly_panorama_team_crud');
	Route::get('/delete/{id}', 'PanoramaController@monthly_panorama_team_crud');
	
	Route::post('/save', 'PanoramaController@monthly_panorama_team_save');
});


// AJAX ROUTES
Route::post('/getUsers', 'AjaxController@getUsersAjax'); //RoleProvince
Route::post('/getPlayers', 'AjaxController@getPlayersAjax');
Route::post('/getRoles', 'AjaxController@getRolesAjax');

Route::post('/getConferences', 'AjaxController@getConferencesAjax'); //RoleProvince
Route::post('/getProvinces', 'AjaxController@getProvincesAjax'); //RoleProvince
Route::post('/getDistricts', 'AjaxController@getDistrictsAjax'); //RoleProvince

Route::post('/getGrounds', 'AjaxController@getGroundsAjax'); //RoleProvince
Route::post('/getGroundProperties', 'AjaxController@getGroundPropertiesAjax');
Route::post('/getGroundHour', 'AjaxController@getGroundHourAjax');

Route::post('/getLeagues', 'AjaxController@getLeaguesAjax'); //RoleProvince
Route::post('/getLeagueGalleries', 'AjaxController@getLeagueGalleriesAjax'); //RoleProvince
Route::post('/getChampions', 'AjaxController@getChampionsAjax'); //RoleProvince
Route::post('/getSeasons', 'AjaxController@getSeasonsAjax'); //RoleProvince
Route::post('/getSeasonsAddBonus', 'AjaxController@getSeasonsAddBonusAjax');

Route::post('/getTeams', 'AjaxController@getTeamsAjax'); //RoleProvince
Route::post('/getMatches', 'AjaxController@getMatchesAjax'); //RoleProvince

Route::post('/getReservations', 'AjaxController@getReservationsAjax'); //RoleProvince

Route::post('/getEliminationTrees', 'AjaxController@getEliminationTreesAjax');
Route::post('/getFixtureGroup', 'AjaxController@getFixtureGroupAjax');

Route::post('/getPlayerPositions', 'AjaxController@getPlayerPositionsAjax');
Route::post('/getPointTypes', 'AjaxController@getPointTypesAjax');
Route::post('/getTactics', 'AjaxController@getTacticsAjax');
Route::post('/getTeamPotentials', 'AjaxController@getTeamPotentialsAjax');
Route::post('/getTeamSatisfactions', 'AjaxController@getTeamSatisfactionsAjax');
Route::post('/getTeamSatisfactionsJogo', 'AjaxController@getTeamSatisfactionsJogoAjax');
Route::post('/getBanners', 'AjaxController@getBannersAjax');

Route::post('/getPenaltyTypes', 'AjaxController@getPenaltyTypesAjax');
Route::post('/getPenalties', 'AjaxController@getPenaltiesAjax');

Route::post('/getPanoramaTypes', 'AjaxController@getPanoramaTypesAjax');
Route::post('/getPanoramas', 'AjaxController@getPanoramasAjax');

Route::get('/getPanoramaTeamsAllS/{sid}/{str}', 'AjaxController@getPanoramaTeamsAllSAjax');

Route::post('/getNews', 'AjaxController@getNewsAjax');
Route::post('/getNewsTypes', 'AjaxController@getNewsTypesAjax');
Route::post('/getTags', 'AjaxController@getTagsAjax');

Route::post('/getAgenda', 'AjaxController@getAgendaAjax');


Route::post('/getRules', 'AjaxController@getRulesAjax');

// Select2 - AJAX 
Route::get('/getDistrictsP', 'AjaxController@getDistrictsPAjax');
Route::get('/getDistrictsF/{pid}', 'AjaxController@getDistrictsFAjax');

Route::get('/getUsersF/{str}', 'AjaxController@getUsersFAjax');
Route::get('/getUsersC/{str}', 'AjaxController@getUsersCAjax');
Route::get('/getUsersRe/{sid}/{str}', 'AjaxController@getUsersReAjax');
Route::get('/getUsersRe/{sid}', 'AjaxController@getUsersReAjax');
Route::get('/getUsersCo/{sid}/{str}', 'AjaxController@getUsersCoAjax');
Route::get('/getUsersCo/{sid}', 'AjaxController@getUsersCoAjax');

Route::get('/getSeasonsF/{str}', 'AjaxController@getSeasonsFAjax'); //RoleProvince
Route::get('/getSeasonsF', 'AjaxController@getSeasonsFAjax'); //RoleProvince

Route::get('/getSeasonsFAll/{str}', 'AjaxController@getSeasonsFAllAjax'); //RoleProvince
Route::get('/getSeasonsFAll', 'AjaxController@getSeasonsFAllAjax'); //RoleProvince

Route::get('/getSeasonsL/{lid}', 'AjaxController@getSeasonsLAjax');

Route::get('/getSeasonsLAll/{lid}', 'AjaxController@getSeasonsLAllAjax');

Route::get('/getSeasonsPanormaAll/{pid}', 'AjaxController@getSeasonsPanormaAllAjax');


Route::get('/getSeasonsE/{lid}', 'AjaxController@getSeasonsEAjax');
Route::get('/getSeasonsE2/{lid}', 'AjaxController@getSeasonsE2Ajax');

Route::get('/getTeamsF/{sid}/{str}', 'AjaxController@getTeamsFAjax');
Route::get('/getTeamsF/{sid}', 'AjaxController@getTeamsFAjax');

Route::get('/getTeamsR/{sid}/{str}', 'AjaxController@getTeamsRAjax');
Route::get('/getTeamsR/{sid}', 'AjaxController@getTeamsRAjax');

Route::get('/getTeamsS/{sid}/{str}', 'AjaxController@getTeamsSAjax');
Route::get('/getTeamsS/{sid}', 'AjaxController@getTeamsSAjax');

Route::get('/getTeamsAllS/{str}', 'AjaxController@getTeamsAllSAjax');

Route::get('/getGroundsF/{sid}/{str}', 'AjaxController@getGroundsFAjax');
Route::get('/getGroundsRe/{sid}/{str}', 'AjaxController@getGroundsReAjax');
Route::get('/getGroundsRe/{sid}', 'AjaxController@getGroundsReAjax');

Route::get('/getGroundsP/{str}', 'AjaxController@getGroundsPAjax'); //RoleProvince
Route::get('/getGroundHourF', 'AjaxController@getGroundHourFAjax');
Route::get('/getGroundHourFE', 'AjaxController@getGroundHourFEAjax');

Route::get('/getSeasonMatches/{sid}', 'AjaxController@getSeasonMatchesAjax');
Route::get('/getSeasonMatches/{sid}/{str}', 'AjaxController@getSeasonMatchesAjax');

Route::get('/getPlayerPenaltyTypesS', 'AjaxController@getPlayerPenaltyTypesSAjax');
Route::get('/getTeamPenaltyTypesS', 'AjaxController@getTeamPenaltyTypesSAjax');

Route::get('/getTeamPlayer/{tid}', 'AjaxController@getTeamPlayerAjax');
Route::get('/getTeamPlayer/{tid}/{str}', 'AjaxController@getTeamPlayerAjax');


Route::get('/getMatchPlayerTeam/{mid}/{tid}', 'AjaxController@getMatchPlayerTeamAjax');


Route::get('/getMatchPlayer/{mid}', 'AjaxController@getMatchPlayerAjax');
Route::get('/getMatchPlayer/{mid}/{str}', 'AjaxController@getMatchPlayerAjax');

Route::get('/getMatchTeam/{mid}', 'AjaxController@getMatchTeamAjax');
Route::get('/getMatchTeam/{mid}/{str}', 'AjaxController@getMatchTeamAjax');


Route::get('/getMatchPlayerF/{mpid}', 'AjaxController@getMatchPlayerFAjax');
Route::get('/getMatchActionF/{maid}', 'AjaxController@getMatchActionFAjax');
Route::get('/getMatchPanoramaF/{mpaid}', 'AjaxController@getMatchPanoramaFAjax');

Route::get('/setTeamsP/{id}/{val}', 'AjaxController@setTeamsPAjax');
Route::get('/setTeamsS/{id}/{val}', 'AjaxController@setTeamsSAjax');
Route::get('/setTeamsSJ/{id}/{val}', 'AjaxController@setTeamsSJAjax');

Route::post('/setGroundHour', 'AjaxController@setGroundHourAjax');
Route::post('/setReservationOffers', 'AjaxController@setReservationOffersAjax');


Route::get('/getLeagueGalleryItemF/{lgiid}', 'AjaxController@getLeagueGalleryItemFAjax');