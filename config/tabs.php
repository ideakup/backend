<?php

return [

    'users' => [
        'tab_01' => [
            'title' => 'Genel',
            'icon' => 'fas fa-user',
            'form' => 'users',
        ],
        'tab_02' => [
            'title' => 'Profil Fotosu',
            'icon' => 'fas fa-image',
            'content' => 'user_photo',
        ],
    ],
    'teams' => [
        'tab_01' => [
            'title' => 'Genel',
            'icon' => 'fas fa-users',
            'form' => 'teams',
        ],
        'tab_02' => [
            'title' => 'Sezonlar',
            'icon' => 'fas fa-box',
            'content' => 'seasons',
            'relationship' => 'team_teamseason',
        ],
        'tab_03' => [
            'title' => 'Kadro',
            'icon' => 'fas fa-user-friends',
            'content' => 'players',
            'relationship' => 'player',
            //'form' => 'team_custom_details',
        ],
        'tab_04' => [
            'title' => 'Fesih Durumu',
            'icon' => 'fas fa-times-circle text-danger',
            'content' => 'team_status',
        ],
        'tab_05' => [
            'title' => 'Takım Görselleri',
            'icon' => 'fas fa-image',
            'content' => 'team_photo',
        ],
    ],
    'matches' => [
        'tab_01' => [
            'title' => 'Genel',
            'icon' => 'fas fa-futbol',
            'form' => 'matches',
        ],
        'tab_02' => [
            'title' => 'Video',
            'icon' => 'fas fa-play-circle',
            'form' => 'match_video',
            'relationship' => 'match_video',
        ],
        'tab_03' => [
            'title' => 'Oyuncular',
            'icon' => 'fas fa-user',
            'content' => 'match_player',
            'form' => 'match_player',
            'relationship' => 'match_player',
        ],
        'tab_04' => [
            'title' => 'Maç Aksiyonları',
            'icon' => 'fas fa-angle-double-right',
            'content' => 'match_action',
            'form' => 'match_action',
            'relationship' => 'match_action',
        ],
        'tab_05' => [
            'title' => 'Panoramalar',
            'icon' => 'fas fa-map',
            'form' => 'match_panorama',
            'content' => 'match_panorama',
            'relationship' => 'match_panorama',
        ],
        'tab_06' => [
            'title' => 'Basın Toplantısı',
            'icon' => 'fas fa-microphone-alt',
            'form' => 'match_press',
            'relationship' => 'match_press',
        ],
        'tab_07' => [
            'title' => 'Fotoğraflar',
            'icon' => 'fas fa-image',
            'content' => 'match_image',
        ],
    ],
    'reservations' => [
        'tab_01' => [
            'title' => 'Genel',
            'icon' => 'fas fa-futbol',
            'form' => 'reservations',
        ],
        'tab_02' => [
            'title' => 'Teklifler',
            'icon' => 'fas fa-play-circle',
            'content' => 'reservation_offer',
            'form' => 'reservation_offer_convert_match'
        ],
    ],
    'seasonsettings' => [
        'tab_01' => [
            'title' => 'Genel',
            'icon' => 'fas fa-trophy',
            'form' => 'seasonsettings',
        ],
        'tab_02' => [
            'title' => 'Tur. Öncesi',
            'icon' => 'fas fa-angle-double-left',
            'content' => 'before_tournament_image',
        ],
        'tab_03' => [
            'title' => 'Tur. Sırası',
            'icon' => 'fas fa-angle-double-up',
            'content' => 'now_tournament_image',
        ],
        'tab_04' => [
            'title' => 'Tur. Sonrası',
            'icon' => 'fas fa-angle-double-right',
            'content' => 'after_tournament_image',
        ],
        'tab_05' => [
            'title' => 'Tur. Hakkında',
            'icon' => 'fas fa-trophy',
            'content' => 'info_tournament_image',
        ],
    ],
    'banners' => [
        'tab_01' => [
            'title' => 'Genel',
            'icon' => 'fas fa-flag',
            'form' => 'banners',
            
        ],
        'tab_02' => [
            'title' => 'Görsel',
            'icon' => 'fas fa-image',
            'content' => 'banner_photo',
        ],
    ],
    'news' => [
        'tab_01' => [
            'title' => 'Genel',
            'icon' => 'fas fa-newspaper',
            'form' => 'news',
            
        ],
        'tab_02' => [
            'title' => 'Foto',
            'icon' => 'fas fa-image',
            'content' => 'new_photogallery',
        ],
    ],
    'agenda' => [
        'tab_01' => [
            'title' => 'Genel',
            'icon' => 'fas fa-photo-video',
            'form' => 'agenda',
            
        ],
        'tab_02' => [
            'title' => 'Foto',
            'icon' => 'fas fa-image',
            'content' => 'agenda_photo',
        ],
    ],
    'grounds' => [
        'tab_01' => [
            'title' => 'Genel',
            'icon' => 'fas fa-border-all',
            'form' => 'grounds',
            
        ],
        'tab_02' => [
            'title' => 'Foto',
            'icon' => 'fas fa-image',
            'content' => 'ground_photogallery',
        ],
    ],
    'panorama_types' => [
        'tab_01' => [
            'title' => 'Genel',
            'icon' => 'fas fa-border-all',
            'form' => 'panorama_types',
            
        ],
        'tab_02' => [
            'title' => 'Foto',
            'icon' => 'fas fa-image',
            'content' => 'panorama_types_photo',
        ],
    ],
    'league_gallery' => [
        'tab_01' => [
            'title' => 'Genel',
            'icon' => 'fas fa-futbol',
            'form' => 'league_gallery',
        ],
        'tab_02' => [
            'title' => 'Galeri Elemanları',
            'icon' => 'fas fa-angle-double-right',
            'content' => 'league_gallery_items',
            'form' => 'league_gallery_items',
            'relationship' => 'league_gallery_items',
        ],
    ],

];