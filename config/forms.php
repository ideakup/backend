<?php

return [
    'conferences' => [
        'name' => [
            'type' => 'text',
            'validation' => 'required|max:100',
            'title' => 'Ad',
            'placeholder' => 'Ad',
            'description' => '',
        ],

        'province_id' => [
            'type' => 'select2',
            'validation' => 'required',

            'dataSource' => 'App\Province',
            
            'title' => 'Şehir',
            'description' => '',
        ],
        
        'district_id' => [
            'type' => 'select2',
            'validation' => 'required',
            'multiple' => 'multiple',

            'relationship' => 'conferences_district', //model function
            'relation_col' => 'district_id',
            'dataSource' => 'App\District',

            'ajax' => true,
            'ajax_url' => '/getDistrictsF',
            'trigger' => 'province_id',

            'title' => 'İlçeler',
            'description' => '',
        ],
    ],

    'provinces' => [
        'name' => [
            'type' => 'text',
            'validation' => 'required|max:100',

            'slug' => true,
            'title' => 'Ad',
            'placeholder' => 'Ad',
            'description' => '',
        ],
        'plate_code' => [
            'type' => 'number',
            'step' => '1',
            'validation' => 'required|numeric|min:0|max:9999',
            'title' => 'Şehir Plaka Kodu',
            'placeholder' => 'Şehir Plaka Kodu',
            'description' => '',
        ],
    ],

    'districts' => [
        'name' => [
            'type' => 'text',
            'validation' => 'required|max:100',

            'slug' => true,
            'title' => 'Ad',
            'placeholder' => 'Ad',
            'description' => '',
        ],
        /*
        'slug' => [
            'type' => 'text',
            'validation' => 'required|max:50',
            'title' => 'Slug',
            'placeholder' => 'Slug',
            'description' => '',
        ],
        */
        'province_id' => [
            'type' => 'select2',
            'validation' => 'required',

            'dataSource' => 'App\Province',

            'title' => 'Şehir',
            'description' => '',
        ],
    ],

    'leagues' => [
        'name' => [
        	'type' => 'text',
        	'validation' => 'required|max:100',
        	'title' => 'Ad',
        	'placeholder' => 'Ad',
        	'description' => '',
	    ],
	    'facebook' => [
        	'type' => 'text',
        	'validation' => 'nullable|max:32',
        	'title' => 'Facebook Kullanıcı Adı',
        	'placeholder' => 'Facebook Kullanıcı Adı',
        	'description' => '',
	    ],
	    'twitter' => [
        	'type' => 'text',
        	'validation' => 'nullable|max:32',
        	'title' => 'Twitter Kullanıcı Adı',
        	'placeholder' => 'Twitter Kullanıcı Adı',
        	'description' => '',
	    ],
	    'instagram' => [
        	'type' => 'text',
        	'validation' => 'nullable|max:32',
        	'title' => 'Instagram Kullanıcı Adı',
        	'placeholder' => 'Instagram Kullanıcı Adı',
        	'description' => '',
	    ],
	    'active' => [
	    	'type' => 'checkbox',
        	'title' => 'Aktif',
        	'description' => '',
        	'value' => 'active', 
            'checkvalue' => true, 
            'uncheckvalue' => false,
	    ],
	    'province' => [
	    	'type' => 'select2',
	    	'multiple' => 'multiple',

            'relationship' => 'league_province', //model function
            'relation_col' => 'province_id',
        	'dataSource' => 'App\Province',
        	
            'title' => 'Şehirler',
        	'description' => '',
	    ],
    ],

    'league_gallery' => [
        'league_id' => [
            'type' => 'hidden',
            'validation' => 'required',
            'exception' => 'league_gallery_league_id',
            'title' => 'Lig',
            'description' => '',
        ],
        'name' => [
            'type' => 'text',
            'validation' => 'required|max:100',
            'title' => 'Ad',
            'placeholder' => 'Ad',
            'description' => '',
        ],
        'order' => [
            'type' => 'number',
            'min' => '0',
            'max' => '9999',
            'step' => '1',
            'validation' => 'required|numeric|min:0|max:9999',
            'title' => 'Sıra',
            'placeholder' => 'Sıra',
            'description' => '',
        ],
        'show_home' => [
            'type' => 'checkbox',
            'title' => 'Ana Sayfada Göster',
            'description' => '',
            'value' => 'active', 
            'checkvalue' => true, 
            'uncheckvalue' => false,
        ],
    ],

    'league_gallery_items' => [
        'header_oe' => [
            'type' => 'h3',
            'title' => 'Galeri Elemanı Ekle / Düzenle',
        ],
        'league_id' => [
            'type' => 'hidden',
            'validation' => 'required',
            'exception' => 'league_gallery_league_id',
            'title' => 'Lig',
            'description' => '',
        ],
        'league_gallery_items_id' => [
            'type' => 'hidden',
            'validation' => 'nullable',
            'title' => '',
            'placeholder' => '',
            'description' => '',
        ],
        'embed_source' => [
            'add_only' => true,
            'type' => 'select2',
            'validation' => 'required',
            'dataSource' => 'json_data',
            'title' => 'Video Kaynağı',
            'description' => '',
            'json_data' => [
                0 => ['id' => 'app', 'name' => 'App'],
                1 => ['id' => 'youtube', 'name' => 'YouTube'],
                2 => ['id' => 'vimeo', 'name' => 'Vimeo'],
                3 => ['id' => 'izlesene', 'name' => 'Izlesene'],
            ]
        ],
        'embed_code' => [
            'add_only' => true,
            'type' => 'text',
            'validation' => 'required|max:255',

            'title' => 'Video Embed Code',
            'placeholder' => 'Video Embed Code',
            'description' => '',
        ],
        'description' => [
            'type' => 'textarea',
            'validation' => 'nullable|max:100',
            'title' => 'Açıklama',
            'placeholder' => 'Açıklama',
            'description' => '',
        ],
    ],

    'champions' => [
        'season_id' => [
            'type' => 'select2',
            'validation' => 'required',

            'relationship' => 'active_season',
            //'relation_col' => 'season_id',
            'dataSource' => 'App\Season',
            'seperator' => 'active_season',

            'ajax' => true,
            'ajax_url' => '/getSeasonsFAll',
            'search' => true,
            'hoop' => true,

            'title' => 'Sezon',
            'description' => '',
        ],
        'team_id' => [
            'type' => 'select2',
            'validation' => 'required',

            'relationship' => 'team',
            'relation_col' => 'season_id',
            'dataSource' => 'App\TeamTeamSeason',
            'seperator' => 'match_team',

            'ajax' => true,
            'ajax_url' => '/getTeamsR',
            'search' => true,
            //'trigger' => 'season_id',

            'title' => 'Birinci Takım',
            'description' => '',
        ],
        'year' => [
            'type' => 'number',
            'validation' => 'required|numeric|min:2005|max:2100',
            'step' => '1',

            'title' => 'Yıl',
            'placeholder' => 'Yıl',
            'description' => '',
        ],
        'point' => [
            'type' => 'number',
            'validation' => 'nullable|numeric|min:0|max:9999',
            'step' => '1',

            'title' => 'Puan',
            'placeholder' => 'Puan',
            'description' => '',
        ],
    ],

    'seasonsettings' => [

        'season_id' => [
            'type' => 'hidden',
            'validation' => 'nullable',
            'model_col' => 'id',
            'title' => '',
            'placeholder' => '',
            'description' => '',
        ],
        'header_desc1' => [
            'type' => 'p',
            'title' => 'Sezona Maçlar eklenene kadar Turnuva Öncesi aktiftir. Sezon Şampiyonu eklendikten sonra Turnuva Sonrası aktif olur.',
            'color' => 'text-danger',
        ],
        'header_to' => [
            'type' => 'h3',
            'title' => 'Turnuva Öncesi Bilgileri',
        ],
        'temsilci_team_id' => [
            'type' => 'select2',
            'validation' => 'nullable',

            'relationship' => 'team',
            //'dataSource' => 'App\TeamTeamSeason',
            'seperator' => 'temsilciteam',

            'ajax' => true,
            'ajax_url' => '/getTeamsAllS',
            'search' => true,
            'hoop' => true,
            //'trigger' => 'season_id',

            'title' => 'Avr. Finallerinde Temsil Eden Takım',
            'description' => '',
        ],
        'teams' => [
            'type' => 'select2',
            'validation' => 'nullable',
            'multiple' => 'multiple',

            'relationship' => 'team',
            //'dataSource' => 'App\TeamTeamSeason',
            'seperator' => 'katilimciteams',

            'ajax' => true,
            'ajax_url' => '/getTeamsAllS',
            'search' => true,
            'hoop' => true,
            //'trigger' => 'season_id',

            'title' => 'Takımlar',
            'description' => '',
        ],
        'before_teaser_text' => [
            'type' => 'html',
            'validation' => 'nullable',
            'title' => 'Video Yanı Metin',
            'placeholder' => 'Video Yanı Metin',
            'description' => '',
            'custom' => true,
        ],
        'before_teaser' => [
            'type' => 'text',
            'validation' => 'nullable|max:150',
            'title' => 'TRF Teaser Embed',
            'placeholder' => 'TRF Teaser Embed',
            'description' => '',
            'custom' => true,
        ],
        'before_playlist' => [
            'type' => 'text',
            'validation' => 'nullable|max:150',
            'title' => 'Son TRF Yaşananlar Playlist',
            'placeholder' => 'Son TRF Yaşananlar Playlist',
            'description' => '',
            'custom' => true,
        ],
        'total_match' => [
            'type' => 'number',
            'validation' => 'nullable|max:10000|min:0',
            'title' => 'Turnuva Maç Sayısı',
            'placeholder' => 'Turnuva Maç Sayısı',
            'description' => '',
            'custom' => true,
        ],



        'to_fkbt' => [
            'type' => 'text',
            'validation' => 'nullable',
            'title' => 'Finallere Katılmayı Başaran Takım',
            'placeholder' => 'Finallere Katılmayı Başaran Takım',
            'description' => '',
            'custom' => true,
        ],
        'to_tt' => [
            'type' => 'text',
            'validation' => 'nullable',
            'title' => 'Toplam Takım',
            'placeholder' => 'Toplam Takım',
            'description' => '',
            'custom' => true,
        ],
        
        

        'to_fkbo' => [
            'type' => 'text',
            'validation' => 'nullable',
            'title' => 'Finallere Katılmayı Başaran Oyuncu',
            'placeholder' => 'Finallere Katılmayı Başaran Oyuncu',
            'description' => '',
            'custom' => true,
        ],
        'to_o' => [
            'type' => 'text',
            'validation' => 'nullable',
            'title' => 'Toplam Oyuncu',
            'placeholder' => 'Toplam Oyuncu',
            'description' => '',
            'custom' => true,
        ],

        
        'to_cye' => [
            'type' => 'text',
            'validation' => 'nullable',
            'title' => 'Canlı Yayın Erişimi',
            'placeholder' => 'Canlı Yayın Erişimi',
            'description' => '',
            'custom' => true,
        ],

        

        'info_text' => [
            'type' => 'html',
            'validation' => 'nullable',
            'title' => 'Hakkında Metni',
            'placeholder' => 'Hakkında Metni',
            'description' => '',
            'custom' => true,
        ],


        'header_tnow' => [
            'type' => 'h3',
            'title' => 'Turnuva Sırası Bilgileri',
        ],
        'now_teaser' => [
            'type' => 'text',
            'validation' => 'nullable|max:150',
            'title' => 'Günün Klibi',
            'placeholder' => 'Günün Klibi',
            'description' => '',
            'custom' => true,
        ],




        'header_ts' => [
            'type' => 'h3',
            'title' => 'Turnuva Sonrası Bilgileri',
        ],
        'after_teaser_text' => [
            'type' => 'html',
            'validation' => 'nullable',
            'title' => 'Video Yanı Metin',
            'placeholder' => 'Video Yanı Metin',
            'description' => '',
            'custom' => true,
        ],
        'after_teaser' => [
            'type' => 'text',
            'validation' => 'nullable|max:150',
            'title' => 'TRF Teaser Embed',
            'placeholder' => 'TRF Teaser Embed',
            'description' => '',
            'custom' => true,
        ],
        'team_id' => [
            'type' => 'select2',
            'validation' => 'nullable',

            'url_input' => 'season_id',
            'seperator' => 'seasonsettings',

            'ajax' => true,
            'ajax_url' => '/getTeamsF',
            'search' => true,
            'hoop' => true,
            //'trigger' => 'season_id',

            'title' => 'Sezon Şampiyonu',
            'description' => '',
        ],
        'team2_id' => [
            'type' => 'select2',
            'validation' => 'nullable',

            'url_input' => 'season_id',
            'seperator' => 'season_team2',

            'ajax' => true,
            'ajax_url' => '/getTeamsF',
            'search' => true,
            'hoop' => true,
            //'trigger' => 'season_id',

            'title' => 'Sezon İkincisi',
            'description' => '',
        ],
        'team3_id' => [
            'type' => 'select2',
            'validation' => 'nullable',

            'url_input' => 'season_id',
            'seperator' => 'season_team3',

            'ajax' => true,
            'ajax_url' => '/getTeamsF',
            'search' => true,
            'hoop' => true,
            //'trigger' => 'season_id',

            'title' => 'Sezon Üçüncüsü',
            'description' => '',
        ],
        'team4_id' => [
            'type' => 'select2',
            'validation' => 'nullable',

            'url_input' => 'season_id',
            'seperator' => 'season_team4',

            'ajax' => true,
            'ajax_url' => '/getTeamsF',
            'search' => true,
            'hoop' => true,
            //'trigger' => 'season_id',

            'title' => 'Sezon Üçüncüsü',
            'description' => '',
        ],

        'header_tpb' => [
            'type' => 'h3',
            'title' => 'Panorama Bilgileri',
        ],
        'player_mvp' => [
            'type' => 'select2',
            'validation' => 'nullable',

            'relationship' => 'player_mvp',
            'seperator' => 'player_mvp',
            'dataSource' => 'App\Player',

            'ajax' => true,
            'ajax_url' => '/getUsersC',
            'search' => true,

            'title' => 'MVP',
            'description' => '',
        ],
        'player_kog' => [
            'type' => 'select2',
            'validation' => 'nullable',

            'relationship' => 'player_kog',
            'seperator' => 'player_kog',
            'dataSource' => 'App\Player',

            'ajax' => true,
            'ajax_url' => '/getUsersC',
            'search' => true,

            'title' => 'Gol Kralı',
            'description' => '',
        ],
        'player_beststriker' => [
            'type' => 'select2',
            'validation' => 'nullable',

            'relationship' => 'player_beststriker',
            'seperator' => 'player_beststriker',
            'dataSource' => 'App\Player',

            'ajax' => true,
            'ajax_url' => '/getUsersC',
            'search' => true,

            'title' => 'En İyi Forvet',
            'description' => '',
        ],
        'player_bestmiddle' => [
            'type' => 'select2',
            'validation' => 'nullable',

            'relationship' => 'player_bestmiddle',
            'seperator' => 'player_bestmiddle',
            'dataSource' => 'App\Player',

            'ajax' => true,
            'ajax_url' => '/getUsersC',
            'search' => true,

            'title' => 'En İyi Ortasaha',
            'description' => '',
        ],
        'player_bestdefance' => [
            'type' => 'select2',
            'validation' => 'nullable',

            'relationship' => 'player_bestdefance',
            'seperator' => 'player_bestdefance',
            'dataSource' => 'App\Player',

            'ajax' => true,
            'ajax_url' => '/getUsersC',
            'search' => true,

            'title' => 'En İyi Defans',
            'description' => '',
        ],
        'player_bestgoalkeeper' => [
            'type' => 'select2',
            'validation' => 'nullable',

            'relationship' => 'player_bestgoalkeeper',
            'seperator' => 'player_bestgoalkeeper',
            'dataSource' => 'App\Player',

            'ajax' => true,
            'ajax_url' => '/getUsersC',
            'search' => true,

            'title' => 'En İyi Kaleci',
            'description' => '',
        ],
    ],

    'seasonsp' => [

        'polymorphic_ctype_id' => [
            'type' => 'hidden',
            'defaultValue' => '34',
            'validation' => 'nullable',
            'title' => '',
            'placeholder' => '',
            'description' => '',
        ],
        'closed' => [
            'type' => 'hidden',
            'defaultValue' => 'false',
            'validation' => 'nullable',
            'title' => '',
            'placeholder' => '',
            'description' => '',
        ],
        'name' => [
            'type' => 'text',
            'validation' => 'required|max:100',
            'title' => 'Ad',
            'placeholder' => 'Ad',
            'description' => '',
        ],
        //rwr
        'league_id' => [
            'type' => 'select2',
            'validation' => 'required',
            'dataSource' => 'App\League',
            'title' => 'Lig',
            'description' => '',
        ],
        'year' => [
            'type' => 'number',
            'validation' => 'required|numeric|min:2005|max:2100',
            'step' => '1',

            'title' => 'Yıl',
            'placeholder' => 'Yıl',
            'description' => '',
        ],
        'start_date' => [
            'type' => 'datepicker',
            'validation' => 'required|date|date_format:d.m.Y',

            'title' => 'Başlangıç Tarihi',
            'placeholder' => 'Başlangıç Tarihi',
            'description' => '',
        ],
        'end_date' => [
            'type' => 'datepicker',
            'validation' => 'required|date|date_format:d.m.Y',

            'title' => 'Bitiş Tarihi',
            'placeholder' => 'Bitiş Tarihi',
            'description' => '',
        ],
        'min_team_point' => [
            'type' => 'number',
            'validation' => 'required|numeric|min:0|max:9999',
            'step' => '1',

            'relationship' => 'league_pointseason',
            'relation_col' => 'min_team_point',

            'title' => 'Minimum Takım Puanı',
            'placeholder' => 'Minimum Takım Puanı',
            'description' => '',
        ],
        'active' => [
            'type' => 'checkbox',
            'title' => 'Aktif',
            'description' => '',
            'value' => 'active', 
            'checkvalue' => true, 
            'uncheckvalue' => false,
        ],

        'header_t' => [
            'type' => 'h3',
            'title' => 'Transfer',
        ],
        'transfer_start_date' => [
            'type' => 'datepicker',
            'validation' => 'date|date_format:d.m.Y',
            'title' => 'Transfer Başlangıç Tarihi',
            'placeholder' => 'Transfer Başlangıç Tarihi',
            'description' => '',
        ],
        'transfer_end_date' => [
            'type' => 'datepicker',
            'validation' => 'date|date_format:d.m.Y',
            'title' => 'Transfer Bitiş Tarihi',
            'placeholder' => 'Transfer Bitiş Tarihi',
            'description' => '',
        ],
        'allowed_transfer_count' => [
            'type' => 'number',
            'validation' => 'required|numeric|min:0|max:9999',
            'step' => '1',

            'title' => 'İzin Verilen Transfer Sayısı',
            'placeholder' => 'İzin Verilen Transfer Sayısı',
            'description' => '',
        ],
        'locking_match_count' => [
            'type' => 'number',
            'validation' => 'required|numeric|min:0|max:9999',
            'step' => '1',

            'title' => 'Kadro Kilit Maç Sayısı',
            'placeholder' => 'Kadro Kilit Maç Sayısı',
            'description' => '',
        ],

        'header_b' => [
            'type' => 'h3',
            'title' => 'Bonus',
        ],
        'has_bonus' => [
            'type' => 'checkbox',

            'relationship' => 'league_pointseason',
            'relation_col' => 'has_bonus',

            'title' => 'Aylık Maç Bonusu',
            'description' => '',
            'value' => 'has_bonus', 
            'checkvalue' => true, 
            'uncheckvalue' => false,
        ],
        'bonus_match_count' => [
            'type' => 'number',
            'validation' => 'nullable|numeric|min:0|max:9999',
            'step' => '1',

            'relationship' => 'league_pointseason',
            'relation_col' => 'bonus_match_count',

            'title' => 'Aylık Min. Maç Sayısı',
            'placeholder' => 'Aylık Min. Maç Sayısı',
            'description' => '',
        ],
        'bonus_point' => [
            'type' => 'number',
            'validation' => 'nullable|numeric|min:0|max:9999',
            'step' => '1',

            'relationship' => 'league_pointseason',
            'relation_col' => 'bonus_point',

            'title' => 'Bonus Puanı',
            'placeholder' => 'Bonus Puanı',
            'description' => '',
        ],
    ],

    'seasonsf' => [

        'polymorphic_ctype_id' => [
            'type' => 'hidden',
            'defaultValue' => '33',
            'validation' => 'nullable',
            'title' => '',
            'placeholder' => '',
            'description' => '',
        ],
        'closed' => [
            'type' => 'hidden',
            'defaultValue' => 'false',
            'validation' => 'nullable',
            'title' => '',
            'placeholder' => '',
            'description' => '',
        ],
        'name' => [
            'type' => 'text',
            'validation' => 'required|max:100',
            'title' => 'Ad',
            'placeholder' => 'Ad',
            'description' => '',
        ],
        //rwr
        'league_id' => [
            'type' => 'select2',
            'validation' => 'required',
            'dataSource' => 'App\League',
            'title' => 'Lig',
            'description' => '',
        ],
        'year' => [
            'type' => 'number',
            'validation' => 'required|numeric|min:2005|max:2100',
            'step' => '1',

            'title' => 'Yıl',
            'placeholder' => 'Yıl',
            'description' => '',
        ],
        'start_date' => [
            'type' => 'datepicker',
            'validation' => 'required|date|date_format:d.m.Y',

            'title' => 'Başlangıç Tarihi',
            'placeholder' => 'Başlangıç Tarihi',
            'description' => '',
        ],
        'end_date' => [
            'type' => 'datepicker',
            'validation' => 'required|date|date_format:d.m.Y',

            'title' => 'Bitiş Tarihi',
            'placeholder' => 'Bitiş Tarihi',
            'description' => '',
        ],
        'active' => [
            'type' => 'checkbox',
            'title' => 'Aktif',
            'description' => '',
            'value' => 'active', 
            'checkvalue' => true, 
            'uncheckvalue' => false,
        ],

        'header_t' => [
            'type' => 'h3',
            'title' => 'Transfer',
        ],
        'transfer_start_date' => [
            'type' => 'datepicker',
            'validation' => 'date|date_format:d.m.Y',
            'title' => 'Transfer Başlangıç Tarihi',
            'placeholder' => 'Transfer Başlangıç Tarihi',
            'description' => '',
        ],
        'transfer_end_date' => [
            'type' => 'datepicker',
            'validation' => 'date|date_format:d.m.Y',
            'title' => 'Transfer Bitiş Tarihi',
            'placeholder' => 'Transfer Bitiş Tarihi',
            'description' => '',
        ],
        'allowed_transfer_count' => [
            'type' => 'number',
            'validation' => 'required|numeric|min:0|max:9999',
            'step' => '1',

            'title' => 'İzin Verilen Transfer Sayısı',
            'placeholder' => 'İzin Verilen Transfer Sayısı',
            'description' => '',
        ],
        'locking_match_count' => [
            'type' => 'number',
            'validation' => 'required|numeric|min:0|max:9999',
            'step' => '1',

            'title' => 'Kadro Kilit Maç Sayısı',
            'placeholder' => 'Kadro Kilit Maç Sayısı',
            'description' => '',
        ],
    ],

    'seasonse' => [

        'polymorphic_ctype_id' => [
            'type' => 'hidden',
            'defaultValue' => '32',
            'validation' => 'nullable',
            'title' => '',
            'placeholder' => '',
            'description' => '',
        ],
        'closed' => [
            'type' => 'hidden',
            'defaultValue' => 'false',
            'validation' => 'nullable',
            'title' => '',
            'placeholder' => '',
            'description' => '',
        ],
        'name' => [
            'type' => 'text',
            'validation' => 'required|max:100',
            'title' => 'Ad',
            'placeholder' => 'Ad',
            'description' => '',
        ],
        //rwr
        'league_id' => [
            'type' => 'select2',
            'validation' => 'required',
            'dataSource' => 'App\League',
            'title' => 'Lig',
            'description' => '',
        ],
        'season_id' => [
            'type' => 'select2',
            'validation' => 'required',

            //'relationship' => 'league_eliminationseason', //model function
            'relation_col' => 'league_id',
            'dataSource' => 'App\Season',
            'seperator' => 'base_season',

            'ajax' => true,
            'ajax_url' => '/getSeasonsE',
            'search' => true,
            'hoop' => true,

            'title' => 'Ana Sezon',
            'description' => '',
        ],
        'year' => [
            'type' => 'number',
            'validation' => 'required|numeric|min:2005|max:2100',
            'step' => '1',

            'title' => 'Yıl',
            'placeholder' => 'Yıl',
            'description' => '',
        ],
        'start_date' => [
            'type' => 'datepicker',
            'validation' => 'required|date|date_format:d.m.Y',

            'title' => 'Başlangıç Tarihi',
            'placeholder' => 'Başlangıç Tarihi',
            'description' => '',
        ],
        'end_date' => [
            'type' => 'datepicker',
            'validation' => 'required|date|date_format:d.m.Y',

            'title' => 'Bitiş Tarihi',
            'placeholder' => 'Bitiş Tarihi',
            'description' => '',
        ],
        'active' => [
            'type' => 'checkbox',
            'title' => 'Aktif',
            'description' => '',
            'value' => 'active', 
            'checkvalue' => true, 
            'uncheckvalue' => false,
        ],
    ],

    'elimination_tree' => [

        'name' => [
            'type' => 'text',
            'validation' => 'required|max:100',
            'title' => 'Ad',
            'placeholder' => 'Ad',
            'description' => '',
        ],
        //rwr
        'league_id' => [
            'type' => 'select2',
            'validation' => 'required',
            'dataSource' => 'App\League',
            'seperator' => 'elimination_tree_league_id',

            'title' => 'Lig',
            'description' => '',
        ],
        'season_id' => [
            'type' => 'select2',
            'validation' => 'required',

            //'relationship' => 'league_eliminationseason', //model function
            'relation_col' => 'league_id',
            'dataSource' => 'App\Season',
            'seperator' => 'elimination_tree_season_id',

            'ajax' => true,
            'ajax_url' => '/getSeasonsE2',
            'search' => true,
            'hoop' => true,

            'title' => 'Sezon',
            'description' => '',
        ],
        'type' => [
            'type' => 'select2',
            'validation' => 'required',
            'dataSource' => 'json_data',
            'title' => 'Ağaç Tipi',
            'description' => '',
            'json_data' => [
                0 => ['id' => 'conference', 'name' => 'Konferans Finali'],
                1 => ['id' => 'final', 'name' => 'İl Finali'],
                2 => ['id' => 'trfinal', 'name' => 'Türkiye Finalleri']
            ]
        ],
        'tree_build' => [
            'type' => 'select2',
            'validation' => 'required',
            'dataSource' => 'json_data',
            'title' => 'Ağaç Yapısı',
            'description' => 'Ağaç tipi "Türkiye Finalleri" olarak seçildiğinde, "1 Ön Eleme - Son 16, Quarter, Semi, Final" özelliği Son 16 olarak ayarlanır! ',
            'json_data' => [
                0 => ['id' => 1, 'name' => 'Final'],
                1 => ['id' => 2, 'name' => 'Yarı Final, Final'],
                2 => ['id' => 3, 'name' => 'Çeyrek Final, Yarı Final, Final'],
                3 => ['id' => 4, 'name' => '1 Ön Eleme - Son 16, Quarter, Semi, Final'],
                4 => ['id' => 5, 'name' => '2 Ön Eleme, Quarter, Semi, Final'],
                5 => ['id' => 6, 'name' => '3 Ön Eleme, Quarter, Semi, Final'],
                6 => ['id' => 7, 'name' => '4 Ön Eleme, Quarter, Semi, Final']
            ]
        ],
        'teams' => [
            'type' => 'select2',
            'validation' => 'nullable',
            'multiple' => 'multiple',

            'relationship' => 'team',
            'relation_col' => 'season_id',
            //'dataSource' => 'App\TeamTeamSeason',
            'seperator' => 'elimination_tree_teams',

            'ajax' => true,
            'ajax_url' => '/getTeamsR',
            'search' => true,
            'hoop' => true,
            //'trigger' => 'season_id',

            'title' => 'Takımlar',
            'description' => '',
        ],
    ],

    'fixture_group' => [

        'name' => [
            'type' => 'text',
            'validation' => 'required|max:100',
            'title' => 'Ad',
            'placeholder' => 'Ad',
            'description' => '',
        ],
        //rwr
        'league_id' => [
            'type' => 'select2',
            'validation' => 'required',
            'dataSource' => 'App\League',
            'seperator' => 'fixture_group_league_id',

            'title' => 'Lig',
            'description' => '',
        ],
        'season_id' => [
            'type' => 'select2',
            'validation' => 'required',

            //'relationship' => 'league_eliminationseason', //model function
            'relation_col' => 'league_id',
            'dataSource' => 'App\Season',
            'seperator' => 'fixture_group_season_id',

            'ajax' => true,
            'ajax_url' => '/getSeasonsE2',
            'search' => true,
            'hoop' => true,

            'title' => 'Sezon',
            'description' => '',
        ],
        'teams' => [
            'type' => 'select2',
            'validation' => 'nullable',
            'multiple' => 'multiple',

            'relationship' => 'team',
            'relation_col' => 'season_id',
            //'dataSource' => 'App\TeamTeamSeason',
            'seperator' => 'fixture_group_teams',

            'ajax' => true,
            'ajax_url' => '/getTeamsR',
            'search' => true,
            'hoop' => true,
            //'trigger' => 'season_id',

            'title' => 'Takımlar',
            'description' => '',
        ],
        'active' => [
            'type' => 'checkbox',
            'title' => 'Aktif',
            'description' => '',
            'value' => 'active', 
            'checkvalue' => true, 
            'uncheckvalue' => false,
        ],
    ],

    'teams' => [

        'name' => [
            'type' => 'text',
            'validation' => 'required|max:100',
            'title' => 'Ad',
            'placeholder' => 'Ad',
            'description' => '',
        ],

        'tactic_id' => [
            'type' => 'select2',
            'validation' => 'required',

            'dataSource' => 'App\Tactic',
            
            'title' => 'Taktik',
            'description' => '',
        ],

        'province_id' => [
            'type' => 'select2',
            'validation' => 'required',

            'dataSource' => 'App\Province',
            
            'title' => 'Şehir',
            'description' => '',
        ],
        
        'district_id' => [
            'type' => 'select2',
            'validation' => 'required',
            //'multiple' => 'multiple',

            //'relationship' => 'conferences_district', //model function
            //'relation_col' => 'district_id',
            'dataSource' => 'App\District',

            'ajax' => true,
            'ajax_url' => '/getDistrictsF',
            'trigger' => 'province_id',

            'title' => 'İlçeler',
            'description' => '',
        ],

        'captain_id' => [
            'type' => 'select2',
            'validation' => 'nullable',

            'relationship' => 'user',
            //'relation_col' => 'district_id',
            'dataSource' => 'App\Player',

            'ajax' => true,
            'ajax_url' => '/getUsersF',
            'search' => true,

            'title' => 'Kaptan',
            'description' => '',
        ],

        'header_ta' => [
            'type' => 'h3',
            'title' => 'Takım Anketi',
        ],
        'potential_id' => [
            'type' => 'select2',
            'validation' => 'nullable',

            'relationship' => 'team_survey',
            'relation_col' => 'potential_id',
            'dataSource' => 'App\TeamPotential',
            
            'title' => 'Potansiyel',
            'description' => '',
        ],        
        'satisfaction_id' => [
            'type' => 'select2',
            'validation' => 'nullable',

            'relationship' => 'team_survey',
            'relation_col' => 'satisfaction_id',
            'dataSource' => 'App\TeamSatisfaction',
            
            'title' => 'Memnuniyet',
            'description' => '',
        ],        
        'satisfaction_jogo_id' => [
            'type' => 'select2',
            'validation' => 'nullable',

            'relationship' => 'team_survey',
            'relation_col' => 'satisfaction_jogo_id',
            'dataSource' => 'App\TeamSatisfactionJogo',
            
            'title' => 'Jogo Memnuniyet',
            'description' => '',
        ],
        //status
            //"active" 
            //"termination" // Fesih talebi gönderilmiş
            //"passive" // Feshedilmiş

        'header_d' => [
            'type' => 'h3',
            'title' => 'Diğer',
        ],
        'facebook' => [
            'type' => 'text',
            'validation' => 'nullable|max:32',

            'title' => 'Facebook Kullanıcı Adı',
            'placeholder' => 'Facebook Kullanıcı Adı',
            'description' => '',
        ],
        'twitter' => [
            'type' => 'text',
            'validation' => 'nullable|max:32',

            'title' => 'Twitter Kullanıcı Adı',
            'placeholder' => 'Twitter Kullanıcı Adı',
            'description' => '',
        ],
        'instagram' => [
            'type' => 'text',
            'validation' => 'nullable|max:32',

            'title' => 'Instagram Kullanıcı Adı',
            'placeholder' => 'Instagram Kullanıcı Adı',
            'description' => '',
        ],
    ],

    /*
    'team_custom_details' => [
        
        'custom_transfer_start_date' => [
            'type' => 'text',
            'validation' => 'nullable',

            'custom' => true,

            'title' => 'Transfer Başlangıç Tarihi',
            'placeholder' => 'Transfer Başlangıç Tarihi',
            'description' => '',
        ],

        'custom_transfer_end_date' => [
            'type' => 'text',
            'validation' => 'nullable',

            'custom' => true,

            'title' => 'Transfer Bitiş Tarihi',
            'placeholder' => 'Transfer Bitiş Tarihi',
            'description' => '',
        ],
        
        'custom_remaining_transfer_count' => [
            'type' => 'number',
            'validation' => 'required|numeric|min:0|max:9999',
            'step' => '1',

            'custom' => true,

            'title' => 'Kalan Transfer Sayısı',
            'placeholder' => 'Kalan Transfer Sayısı',
            'description' => '',
        ],

    ],
    */
    
    'matches' => [

        'season_id' => [
            'type' => 'select2',
            'validation' => 'required',

            'relationship' => 'active_season',
            //'relation_col' => 'season_id',
            'dataSource' => 'App\Season',
            'seperator' => 'active_season',

            'ajax' => true,
            'ajax_url' => '/getSeasonsF',
            'search' => true,
            'hoop' => true,

            'title' => 'Sezon',
            'description' => '',
        ],
        'team1_id' => [
            'type' => 'select2',
            'validation' => 'required',

            'relationship' => 'team',
            'relation_col' => 'season_id',
            'dataSource' => 'App\TeamTeamSeason',
            'seperator' => 'match_team',

            'ajax' => true,
            'ajax_url' => '/getTeamsR',
            'search' => true,
            //'trigger' => 'season_id',

            'title' => 'Birinci Takım',
            'description' => '',
        ],
        'team2_id' => [
            'type' => 'select2',
            'validation' => 'required',

            'relationship' => 'team',
            'relation_col' => 'season_id',
            'dataSource' => 'App\TeamTeamSeason',
            'seperator' => 'match_team',

            'ajax' => true,
            'ajax_url' => '/getTeamsR',
            'search' => true,
            //'trigger' => 'season_id',

            'title' => 'İkinci Takım',
            'description' => '',
        ],
        'ground_id' => [
            'type' => 'select2',
            'validation' => 'required',

            //'relationship' => 'province',
            'relation_col' => 'season_id',
            //'dataSource' => 'App\Ground',
            'seperator' => 'match_ground',

            'ajax' => true,
            'ajax_url' => '/getGroundsRe',
            'search' => true,
            'hoop' => true,

            'title' => 'Halı Saha',
            'description' => '',
        ],
        'date' => [
            'type' => 'datepicker',
            'validation' => 'required|date|date_format:d.m.Y',

            'title' => 'Maç Tarihi',
            'placeholder' => 'Maç Tarihi',
            'description' => '',
        ],
        'time_e' => [ 
            'type' => 'select2',
            'validation' => 'required',

            'relation_col' => 'date',
            'seperator' => 'match_time',

            'ajax' => true,
            'ajax_url' => '/getGroundHourFE',
            'search' => true,
            'hoop' => true,

            'title' => 'Maç Saati',
            'description' => '',
        ],
        'referee_id' => [
            'type' => 'select2',
            'validation' => 'nullable',

            //'relationship' => 'referee',
            'relation_col' => 'season_id',
            //'dataSource' => 'App\RefereeLeague',
            'seperator' => 'match_referee',

            'ajax' => true,
            'ajax_url' => '/getUsersRe',
            'search' => true,
            'hoop' => true,

            'title' => 'Hakem',
            'description' => '',
        ],
        'coordinator_id' => [
            'type' => 'select2',
            'validation' => 'nullable',

            //'relationship' => 'referee',
            'relation_col' => 'season_id',
            //'dataSource' => 'App\RefereeLeague',
            'seperator' => 'match_coordinator',

            'ajax' => true,
            'ajax_url' => '/getUsersCo',
            'search' => true,
            'hoop' => true,

            'title' => 'Koordinatör',
            'description' => '',
        ],
        'completed_status' => [
            'type' => 'select2',
            'addvisible' => 'no',
            'validation' => 'nullable',
            'dataSource' => 'json_data',
            'title' => 'Hükmen Galibiyet',
            'description' => '',
            'json_data' => [
                1 => ['id' => 1, 'name' => 'Birinci Takım Hükmen Kazandı'],
                2 => ['id' => 2, 'name' => 'İkinci Takım Hükmen Kazandı'],
            ]
        ],
        'completed' => [
            'type' => 'switch',
            'addvisible' => 'no',
            'validation' => 'nullable',

            'data' => [
                'on' => ['name' => 'Tamamlandı'],
                'off' => ['name' => 'Devam Ediyor'],
            ],

            'title' => 'Maç Durumu',
            'description' => '', //'Maç aksiyonları ile ilgili güncellemeler yapmak için maçın DEVAM EDİYOR modunda olması gerekmektedir!',
        ]

        /*
        'live_embed_codes' => [
            'type' => 'text',
            'validation' => 'nullable|max:255',

            'title' => 'aaa',
            'placeholder' => 'aaa',
            'description' => '',
        ],
        */
    ],

    'match_video' => [
        
        'status' => [
            'type' => 'select2',
            'validation' => 'required',
            'dataSource' => 'json_data',
            'title' => 'Durum',
            'description' => '',
            'json_data' => [
                0 => ['id' => 'recordable', 'name' => 'Kayıt Edilebilir'],
                1 => ['id' => 'recording', 'name' => 'Kayıt Ediliyor'],
                2 => ['id' => 'recorded', 'name' => 'Kayıt Edildi'],
                3 => ['id' => 'finished', 'name' => 'Tamamlandı'],
            ]
        ],
        'live_source' => [
            'type' => 'select2',
            'validation' => 'required',
            'dataSource' => 'json_data',
            'title' => 'Canlı Video Kaynağı',
            'description' => '',
            'json_data' => [
                0 => ['id' => 'app', 'name' => 'App'],
                1 => ['id' => 'youtube', 'name' => 'YouTube'],
                2 => ['id' => 'vimeo', 'name' => 'Vimeo'],
                3 => ['id' => 'izlesene', 'name' => 'Izlesene'],
            ]
        ],
        'live_embed_code' => [
            'type' => 'text',
            'validation' => 'nullable|max:255',

            'title' => 'Canlı Video Embed Code',
            'placeholder' => 'Canlı Video Embed Code',
            'description' => '',
        ],
        'full_source' => [
            'type' => 'select2',
            'validation' => 'required',
            'dataSource' => 'json_data',
            'title' => 'Tam Maç Video Kaynağı',
            'description' => '',
            'json_data' => [
                0 => ['id' => 'app', 'name' => 'App'],
                1 => ['id' => 'youtube', 'name' => 'YouTube'],
                2 => ['id' => 'vimeo', 'name' => 'Vimeo'],
                3 => ['id' => 'izlesene', 'name' => 'Izlesene'],
            ]
        ],
        'full_embed_code' => [
            'type' => 'text',
            'validation' => 'nullable|max:255',

            'title' => 'Tam Maç Embed Code',
            'placeholder' => 'Tam Maç Embed Code',
            'description' => '',
        ],
        'summary_source' => [
            'type' => 'select2',
            'validation' => 'required',
            'dataSource' => 'json_data',
            'title' => 'Maç Özeti Video Kaynağı',
            'description' => '',
            'json_data' => [
                0 => ['id' => 'app', 'name' => 'App'],
                1 => ['id' => 'youtube', 'name' => 'YouTube'],
                2 => ['id' => 'vimeo', 'name' => 'Vimeo'],
                3 => ['id' => 'izlesene', 'name' => 'Izlesene'],
            ]
        ],
        'summary_embed_code' => [
            'type' => 'text',
            'validation' => 'nullable|max:255',

            'title' => 'Maç Özeti Embed Code',
            'placeholder' => 'Maç Özeti Embed Code',
            'description' => '',
        ],
    ],

    'match_player' => [
        
        'header_oe' => [
            'type' => 'h3',
            'title' => 'Oyuncu Ekle / Düzenle',
        ],
        'match_id' => [
            'type' => 'hidden',
            'model_col' => 'id',
            'validation' => 'nullable',
            'title' => '',
            'placeholder' => '',
            'description' => '',
        ],
        'match_player_id' => [
            'type' => 'hidden',
            'validation' => 'nullable',
            'title' => '',
            'placeholder' => '',
            'description' => '',
        ],
        'player_id' => [
            'add_only' => true,
            'type' => 'select2',
            'validation' => 'nullable',
            
            'seperator' => 'match_team_player',

            'title' => 'Oyuncu',
            'description' => '',
        ],
        'guest_name' => [
            'add_only' => true,
            'type' => 'text',
            'validation' => 'nullable|max:255',

            'title' => 'Misafir Adı',
            'placeholder' => 'Misafir Adı',
            'description' => '',
        ],
        'position_id' => [
            'add_only' => true,
            'type' => 'select2',
            'validation' => 'required',

            //'relationship' => 'player',
            //'relation_col' => 'position_id',
            'dataSource' => 'App\PlayerPosition',

            'title' => 'Posizyon',
            'description' => '',
        ],
        'rating' => [
            'add_only' => true,
            'type' => 'hidden',
            'validation' => 'required|numeric|min:0|max:9999',
            'step' => '1',

            'defaultValue' => 6,

            'title' => 'Rating',
            'placeholder' => 'Rating',
            'description' => '',
        ],
        /*
        'saving' => [
            'add_only' => true,
            'type' => 'number',
            'validation' => 'nullable|numeric|min:0|max:9999',
            'step' => '1',

            'title' => 'Kurtarış',
            'placeholder' => 'Kurtarış',
            'description' => '',
        ],
        */
    ],

    'match_action' => [

        'header_oe' => [
            'type' => 'h3',
            'title' => 'Aksiyon Ekle / Düzenle',
        ],
        'match_id' => [
            'type' => 'hidden',
            'model_col' => 'id',
            'validation' => 'required',
            'title' => '',
            'placeholder' => '',
            'description' => '',
        ],
        'match_action_id' => [
            'type' => 'hidden',
            'validation' => 'nullable',
            'title' => '',
            'placeholder' => '',
            'description' => '',
        ],
        'embed_source' => [
            'add_only' => true,
            'type' => 'select2',
            'validation' => 'nullable',
            'dataSource' => 'json_data',
            'title' => 'Video Kaynağı',
            'description' => '',
            'json_data' => [
                0 => ['id' => 'app', 'name' => 'App'],
                1 => ['id' => 'youtube', 'name' => 'YouTube'],
                2 => ['id' => 'vimeo', 'name' => 'Vimeo'],
                3 => ['id' => 'izlesene', 'name' => 'Izlesene'],
            ]
        ],
        'embed_code' => [
            'add_only' => true,
            'type' => 'text',
            'validation' => 'nullable|max:255',

            'title' => 'Video Embed Code',
            'placeholder' => 'Video Embed Code',
            'description' => '',
        ],
        'match_player_id' => [
            'add_only' => true,
            'type' => 'select2',
            'validation' => 'nullable',
            
            'seperator' => 'match_team_player_all',

            'title' => 'Oyuncu',
            'description' => '',
        ],
        'type' => [
            'add_only' => true,
            'type' => 'select2',
            'validation' => 'required',

            'seperator' => 'match_action_type',

            'dataSource' => 'json_data',
            'title' => 'Aksiyon Türü',
            'description' => '',
            'json_data' => [
                0 => ['id' => 'goal', 'name' => 'Gol'],
                1 => ['id' => 'yellow_card', 'name' => 'Sarı Kart'],
                2 => ['id' => 'red_card', 'name' => 'Kırmızı Kart'],
                3 => ['id' => 'critical', 'name' => 'Kritik Pozisyon'],
                3 => ['id' => 'saving', 'name' => 'Kurtarış'],
            ]
        ],
        'minute' => [
            'add_only' => true,
            'type' => 'text',
            'validation' => 'required',
            'description' => '23.45',

            'title' => 'Dakika',
            'placeholder' => 'Dakika',
            'description' => '',
        ],
    ],

    'match_panorama' => [

        'header_mpa' => [
            'type' => 'h3',
            'title' => 'Panorama Ekle / Düzenle',
        ],
        'match_id' => [
            'type' => 'hidden',
            'model_col' => 'id',
            'validation' => 'required',
            'title' => '',
            'placeholder' => '',
            'description' => '',
        ],
        'match_panorama_id' => [
            'type' => 'hidden',
            'validation' => 'nullable',
            'title' => '',
            'placeholder' => '',
            'description' => '',
        ],
        'panorama_id' => [
            'type' => 'select2',
            'validation' => 'required',

            'seperator' => 'match_panorama_type',
            'dataSource' => 'App\PanoramaType',
            
            'title' => 'Panorama',
            'description' => '',
        ],
        'match_player_id_mpa' => [
            'renamecol' => 'match_player_id',
            'add_only' => true,
            'type' => 'select2',
            'validation' => 'nullable',
            
            'seperator' => 'match_team_player_all',

            'title' => 'Oyuncu',
            'description' => '',
        ],
        'embed_source_mpa' => [
            'renamecol' => 'match_player_id',
            'add_only' => true,
            'type' => 'select2',
            'validation' => 'nullable',
            'dataSource' => 'json_data',
            'title' => 'Video Kaynağı',
            'description' => '',
            'json_data' => [
                0 => ['id' => 'app', 'name' => 'App'],
                1 => ['id' => 'youtube', 'name' => 'YouTube'],
                2 => ['id' => 'vimeo', 'name' => 'Vimeo'],
                3 => ['id' => 'izlesene', 'name' => 'Izlesene'],
            ]
        ],
        'embed_code_mpa' => [
            'renamecol' => 'embed_code',
            'add_only' => true,
            'type' => 'text',
            'validation' => 'nullable|max:255',

            'title' => 'Video Embed Code',
            'placeholder' => 'Video Embed Code',
            'description' => '',
        ],

        

        /*
        'embed_source' => [
            'add_only' => true,
            'type' => 'select2',
            'validation' => 'nullable',
            'dataSource' => 'json_data',
            'title' => 'Video Kaynağı',
            'description' => '',
            'json_data' => [
                0 => ['id' => 'app', 'name' => 'App'],
                1 => ['id' => 'youtube', 'name' => 'YouTube'],
                2 => ['id' => 'vimeo', 'name' => 'Vimeo'],
                3 => ['id' => 'izlesene', 'name' => 'Izlesene'],
            ]
        ],
        'embed_code' => [
            'add_only' => true,
            'type' => 'text',
            'validation' => 'nullable|max:255',

            'title' => 'Video Embed Code',
            'placeholder' => 'Video Embed Code',
            'description' => '',
        ],
        'match_player_id' => [
            'add_only' => true,
            'type' => 'select2',
            'validation' => 'nullable',
            
            'seperator' => 'match_team_player_all',

            'title' => 'Oyuncu',
            'description' => '',
        ],
        'type' => [
            'add_only' => true,
            'type' => 'select2',
            'validation' => 'required',

            'seperator' => 'match_action_type',

            'dataSource' => 'json_data',
            'title' => 'Aksiyon Türü',
            'description' => '',
            'json_data' => [
                0 => ['id' => 'goal', 'name' => 'Gol'],
                1 => ['id' => 'yellow_card', 'name' => 'Sarı Kart'],
                2 => ['id' => 'red_card', 'name' => 'Kırmızı Kart'],
                3 => ['id' => 'critical', 'name' => 'Kritik Pozisyon'],
            ]
        ],
        'minute' => [
            'add_only' => true,
            'type' => 'number',
            'validation' => 'required|numeric|min:0|max:9999',
            'step' => '1',

            'title' => 'Dakika',
            'placeholder' => 'Dakika',
            'description' => '',
        ],
        */
    ],

    'match_press' => [
        
        'embed_source_mp' => [
            'renamecol' => 'embed_source',
            'type' => 'select2',
            'validation' => 'required',
            'dataSource' => 'json_data',


            'title' => 'Video Kaynağı',
            'description' => '',
            'json_data' => [
                0 => ['id' => 'app', 'name' => 'App'],
                1 => ['id' => 'youtube', 'name' => 'YouTube'],
                2 => ['id' => 'vimeo', 'name' => 'Vimeo'],
                3 => ['id' => 'izlesene', 'name' => 'Izlesene'],
            ]
        ],
        'embed_code' => [
            'type' => 'text',
            'validation' => 'nullable|max:255',

            'title' => 'Video Embed Code',
            'placeholder' => 'Video Embed Code',
            'description' => '',
        ],
        'team_id_mp' => [
            'renamecol' => 'team_id',
            'add_only' => true,
            'type' => 'select2',
            'validation' => 'nullable',

            'relationship' => 'team', //model function
            'relation_col' => 'team_id',
            
            'seperator' => 'match_teams',

            'title' => 'Takım',
            'description' => '',
        ],
        'players_id_mp' => [
            'renamecol' => 'player_id',
            'multiple' => 'multiple',
            'add_only' => true,
            'type' => 'select2',
            'validation' => 'nullable',

            'relationship' => 'team', //model function
            'relation_col' => 'player_id',
            'dataSource' => 'App\MatchPlayer',

            'ajax' => true,
            'ajax_url' => '/getMatchPlayerTeam',
            'trigger' => 'team_id_mp',
            
            'seperator' => 'match_team_player_press',

            'title' => 'Oyuncu',
            'description' => '',
        ],
    ],

    'users' => [
        'first_name' => [
            'type' => 'text',
            'validation' => 'required|max:30',

            'title' => 'Adı',
            'placeholder' => 'Adı',
            'description' => '',
        ],
        'last_name' => [
            'type' => 'text',
            'validation' => 'required|max:150',

            'title' => 'Soyadı',
            'placeholder' => 'Soyadı',
            'description' => '',
        ],
        'email' => [
            'type' => 'email',
            'validation' => 'required|email|max:254',

            'title' => 'Eposta Adresi',
            'placeholder' => 'Eposta Adresi',
            'description' => '',
        ],
        'phone' => [
            'type' => 'text',
            'validation' => 'required|max:20',

            'title' => 'Telefon Numarası',
            'placeholder' => 'Telefon Numarası',
            'description' => '',
        ],
        'phone_code' => [
            'type' => 'view',
            'validation' => 'nullable',

            'title' => 'Doğrulama Kodu',
            'placeholder' => 'Doğrulama Kodu',
            'description' => '',
        ],
        /*
        'password' => [
            'type' => 'password',
            'validation' => 'required|max:30',

            'title' => 'Parola',
            'placeholder' => 'Parola',
            'description' => 'Geliştirici Tarafından Düzenlenecek...',
        ],
        */
        'header_y' => [
            'type' => 'h3',
            'title' => 'Yönetici Yetkisi',
        ],
        'role' => [
            'type' => 'select2',
            'validation' => 'nullable',
            //'multiple' => 'multiple',

            //'relationship' => 'profile', //model function
            //'relation_col' => 'province_id',
            'dataSource' => 'Spatie\Permission\Models\Role',
            'seperator' => 'user_role_id',
            
            'title' => 'Yetki',
            'description' => '',
        ],
        'role_province' => [
            'type' => 'select2',
            'validation' => 'nullable',
            'multiple' => 'multiple',

            'relationship' => 'roleprovince', //model function
            'relation_col' => 'province_id',
            'dataSource' => 'App\Province',
            
            'title' => 'Yetkili Olduğu Şehirler',
            'description' => '',
        ],

        'header_i' => [
            'type' => 'h3',
            'title' => 'Profil',
        ],
        'province' => [
            'type' => 'select2',
            'validation' => 'nullable',
            //'multiple' => 'multiple',

            'relationship' => 'profile', //model function
            'relation_col' => 'province_id',
            'dataSource' => 'App\Province',
            
            'title' => 'Şehirler',
            'description' => '',
        ],
        'district_id' => [
            'type' => 'select2',
            'validation' => 'nullable',
            //'multiple' => 'multiple',

            'relationship' => 'profile', //model function
            'relation_col' => 'district_id',
            'dataSource' => 'App\District',

            'ajax' => true,
            'ajax_url' => '/getDistrictsF',
            'trigger' => 'province',

            'title' => 'İlçeler',
            'description' => '',
        ],
        'identity_number' => [
            'type' => 'text',
            'validation' => 'nullable|max:11',

            'relationship' => 'profile',
            'relation_col' => 'identity_number',

            'title' => 'TC Kimlik No',
            'placeholder' => 'TC Kimlik No',
            'description' => '',
        ],
        'birth_date' => [
            'type' => 'datepicker',
            'validation' => 'required|date|date_format:d.m.Y',

            'relationship' => 'profile',
            'relation_col' => 'birth_date',

            'title' => 'Doğum Tarihi',
            'placeholder' => 'Doğum Tarihi',
            'description' => '',
        ],

        'header_o' => [
            'type' => 'h3',
            'relationship' => 'player',
            'title' => 'Oyuncu',
        ],

        'facebook' => [
            'type' => 'text',
            'validation' => 'nullable|max:32',

            'relationship' => 'player',
            'relation_col' => 'facebook',

            'title' => 'Facebook Kullanıcı Adı',
            'placeholder' => 'Facebook Kullanıcı Adı',
            'description' => '',
        ],
        'twitter' => [
            'type' => 'text',
            'validation' => 'nullable|max:32',

            'relationship' => 'player',
            'relation_col' => 'twitter',

            'title' => 'Twitter Kullanıcı Adı',
            'placeholder' => 'Twitter Kullanıcı Adı',
            'description' => '',
        ],
        'instagram' => [
            'type' => 'text',
            'validation' => 'nullable|max:32',

            'relationship' => 'player',
            'relation_col' => 'instagram',

            'title' => 'Instagram Kullanıcı Adı',
            'placeholder' => 'Instagram Kullanıcı Adı',
            'description' => '',
        ],
        'position_id' => [
            'type' => 'select2',
            'validation' => 'nullable',

            'relationship' => 'player',
            'relation_col' => 'position_id',
            'dataSource' => 'App\PlayerPosition',

            'title' => 'Posizyon 1',
            'description' => '',
        ],
        'position2_id' => [
            'type' => 'select2',
            'validation' => 'nullable',

            'relationship' => 'player',
            'relation_col' => 'position2_id',
            'dataSource' => 'App\PlayerPosition',

            'title' => 'Posizyon 2',
            'description' => '',
        ],
        'position3_id' => [
            'type' => 'select2',
            'validation' => 'nullable',
            
            'relationship' => 'player',
            'relation_col' => 'position3_id',
            'dataSource' => 'App\PlayerPosition',

            'title' => 'Posizyon 3',
            'description' => '',
        ],
        'number' => [
            'type' => 'number',
            'validation' => 'nullable|numeric|min:0|max:9999',
            'step' => '1',

            'relationship' => 'player',
            'relation_col' => 'number',

            'title' => 'Forma Numarası',
            'placeholder' => 'Forma Numarası',
            'description' => '',
        ],
        'foot' => [
            'type' => 'radio',
            'validation' => 'nullable',

            'relationship' => 'player',
            'relation_col' => 'foot',

            'title' => 'Güçlü Ayak',
            'description' => '',
            'values' => [
                'none' => ['title' => 'Belirsiz', 'value' => ''],
                'right' => ['title' => 'Sağ Ayak', 'value' => 'right'],
                'left' => ['title' => 'Sol Ayak', 'value' => 'left'],
                'both' => ['title' => 'İkisi', 'value' => 'both'],
            ],
        ],
        'weight' => [
            'type' => 'number',
            'validation' => 'nullable|numeric|min:0|max:9999',
            'step' => '1',

            'relationship' => 'player',
            'relation_col' => 'weight',
            
            'title' => 'Kilo',
            'placeholder' => 'Kilo',
            'description' => '',
        ],
        'height' => [
            'type' => 'number',
            'validation' => 'nullable|numeric|min:0|max:9999',
            'step' => '1',

            'relationship' => 'player',
            'relation_col' => 'height',

            'title' => 'Boy',
            'placeholder' => 'Boy',
            'description' => '',
        ],
    ],

    'addoutsideplayer' => [
        'first_name' => [
            'type' => 'text',
            'validation' => 'required|max:30',

            'title' => 'Adı',
            'placeholder' => 'Adı',
            'description' => '',
        ],
        'last_name' => [
            'type' => 'text',
            'validation' => 'required|max:150',

            'title' => 'Soyadı',
            'placeholder' => 'Soyadı',
            'description' => '',
        ],
        'email' => [
            'type' => 'email',
            'validation' => 'required|email|max:254',

            'title' => 'Eposta Adresi',
            'placeholder' => 'Eposta Adresi',
            'description' => '',
        ],
        'phone' => [
            'type' => 'text',
            'validation' => 'required|max:20',

            'title' => 'Telefon Numarası',
            'placeholder' => 'Telefon Numarası',
            'description' => '',
        ],
        /*
        'password' => [
            'type' => 'password',
            'validation' => 'required|max:30',

            'title' => 'Parola',
            'placeholder' => 'Parola',
            'description' => 'Geliştirici Tarafından Düzenlenecek...',
        ],
        */

        'header_i' => [
            'type' => 'h3',
            'title' => 'Profil',
        ],
        'province' => [
            'type' => 'select2',
            'validation' => 'required',
            //'multiple' => 'multiple',

            'relationship' => 'profile', //model function
            'relation_col' => 'province_id',
            'dataSource' => 'App\Province',
            
            'title' => 'Şehirler',
            'description' => '',
        ],
        'district_id' => [
            'type' => 'select2',
            'validation' => 'required',
            //'multiple' => 'multiple',

            'relationship' => 'profile', //model function
            'relation_col' => 'district_id',
            'dataSource' => 'App\District',

            'ajax' => true,
            'ajax_url' => '/getDistrictsF',
            'trigger' => 'province',

            'title' => 'İlçeler',
            'description' => '',
        ],
        'identity_number' => [
            'type' => 'text',
            'validation' => 'nullable|max:11',

            'relationship' => 'profile',
            'relation_col' => 'identity_number',

            'title' => 'TC Kimlik No',
            'placeholder' => 'TC Kimlik No',
            'description' => '',
        ],
        'birth_date' => [
            'type' => 'datepicker',
            'validation' => 'required|date|date_format:d.m.Y',

            'relationship' => 'profile',
            'relation_col' => 'birth_date',

            'title' => 'Doğum Tarihi',
            'placeholder' => 'Doğum Tarihi',
            'description' => '',
        ],

        'header_o' => [
            'type' => 'h3',
            'relationship' => 'player',
            'title' => 'Oyuncu',
        ],

        'facebook' => [
            'type' => 'text',
            'validation' => 'nullable|max:32',

            'relationship' => 'player',
            'relation_col' => 'facebook',

            'title' => 'Facebook Kullanıcı Adı',
            'placeholder' => 'Facebook Kullanıcı Adı',
            'description' => '',
        ],
        'twitter' => [
            'type' => 'text',
            'validation' => 'nullable|max:32',

            'relationship' => 'player',
            'relation_col' => 'twitter',

            'title' => 'Twitter Kullanıcı Adı',
            'placeholder' => 'Twitter Kullanıcı Adı',
            'description' => '',
        ],
        'instagram' => [
            'type' => 'text',
            'validation' => 'nullable|max:32',

            'relationship' => 'player',
            'relation_col' => 'instagram',

            'title' => 'Instagram Kullanıcı Adı',
            'placeholder' => 'Instagram Kullanıcı Adı',
            'description' => '',
        ],
        'position_id' => [
            'type' => 'select2',
            'validation' => 'required',

            'relationship' => 'player',
            'relation_col' => 'position_id',
            'dataSource' => 'App\PlayerPosition',

            'title' => 'Posizyon 1',
            'description' => '',
        ],
        'position2_id' => [
            'type' => 'select2',
            'validation' => 'required',

            'relationship' => 'player',
            'relation_col' => 'position2_id',
            'dataSource' => 'App\PlayerPosition',

            'title' => 'Posizyon 2',
            'description' => '',
        ],
        'position3_id' => [
            'type' => 'select2',
            'validation' => 'required',
            
            'relationship' => 'player',
            'relation_col' => 'position3_id',
            'dataSource' => 'App\PlayerPosition',

            'title' => 'Posizyon 3',
            'description' => '',
        ],
        'number' => [
            'type' => 'number',
            'validation' => 'required|numeric|min:0|max:9999',
            'step' => '1',

            'relationship' => 'player',
            'relation_col' => 'number',

            'title' => 'Forma Numarası',
            'placeholder' => 'Forma Numarası',
            'description' => '',
        ],
        'foot' => [
            'type' => 'radio',
            'validation' => 'required',

            'relationship' => 'player',
            'relation_col' => 'foot',

            'title' => 'Güçlü Ayak',
            'description' => '',
            'values' => [
                'none' => ['title' => 'Belirsiz', 'value' => 'none'],
                'right' => ['title' => 'Sağ Ayak', 'value' => 'right'],
                'left' => ['title' => 'Sol Ayak', 'value' => 'left'],
                'both' => ['title' => 'İkisi', 'value' => 'both'],
            ],
        ],
        'weight' => [
            'type' => 'number',
            'validation' => 'required|numeric|min:0|max:9999',
            'step' => '1',

            'relationship' => 'player',
            'relation_col' => 'weight',
            
            'title' => 'Kilo',
            'placeholder' => 'Kilo',
            'description' => '',
        ],
        'height' => [
            'type' => 'number',
            'validation' => 'required|numeric|min:0|max:9999',
            'step' => '1',

            'relationship' => 'player',
            'relation_col' => 'height',

            'title' => 'Boy',
            'placeholder' => 'Boy',
            'description' => '',
        ],
    ],

    'roles' => [
        'name' => [
            'type' => 'text',
            'validation' => 'required|max:50',

            'title' => 'Adı',
            'placeholder' => 'Adı',
            'description' => '',
        ],
    ],

    'grounds' => [
        // PROBLEM
        'name' => [
            'type' => 'text',
            'validation' => 'required|max:50',

            'slug' => true,
            'title' => 'Ad',
            'placeholder' => 'Ad',
            'description' => '',
        ],
        'capacity' => [
            'type' => 'number',
            'validation' => 'nullable|numeric|min:0|max:9999',
            'step' => '1',

            'title' => 'Kapasite',
            'placeholder' => 'Kapasite',
            'description' => '',
        ],
        'width' => [
            'type' => 'number',
            'validation' => 'nullable|numeric|min:0|max:9999',
            'step' => '1',

            'title' => 'Genişlik',
            'placeholder' => 'Genişlik',
            'description' => '',
        ],
        'length__s' => [
            'renamecol' => 'length',
            'type' => 'number',
            'validation' => 'nullable|numeric|min:0|max:9999',
            'step' => '1',

            'title' => 'Uzunluk',
            'placeholder' => 'Uzunluk',
            'description' => '',
        ],
        'description' => [
            'type' => 'textarea',
            'validation' => 'nullable',
            'title' => 'Açıklama',
            'placeholder' => 'Açıklama',
            'description' => '',
        ],
        'header_i' => [
            'type' => 'h3',
            'title' => 'İletişim Bilgileri',
        ],
        'phone' => [
            'type' => 'text',
            'validation' => 'nullable|max:20',
            'title' => 'Telefon Numarası 1',
            'placeholder' => 'Telefon Numarası 1',
            'description' => '',
        ],
        'phone2' => [
            'type' => 'text',
            'validation' => 'nullable|max:20',
            'title' => 'Telefon Numarası 2',
            'placeholder' => 'Telefon Numarası 2',
            'description' => '',
        ],
        'email' => [
            'type' => 'email',
            'validation' => 'nullable|email|max:254',
            'title' => 'Eposta Adresi',
            'placeholder' => 'Eposta Adresi',
            'description' => '',
        ],
        'website' => [
            'type' => 'text',
            'validation' => 'nullable|max:200',
            'title' => 'Website',
            'placeholder' => 'Website',
            'description' => '',
        ],
        'header_k' => [
            'type' => 'h3',
            'title' => 'Konum',
        ],
        'province_id' => [
            'type' => 'select2',
            'validation' => 'required',

            'dataSource' => 'App\Province',
            
            'title' => 'Şehir',
            'description' => '',
        ],
        'district_id' => [
            'type' => 'select2',
            'validation' => 'nullable',
            //'multiple' => 'multiple',
            //'relationship' => 'conferences_district', //model function
            //'relation_col' => 'district_id',
            'dataSource' => 'App\District',

            'ajax' => true,
            'ajax_url' => '/getDistrictsF',
            'trigger' => 'province_id',

            'title' => 'İlçeler',
            'description' => '',
        ],
        'address' => [
            'type' => 'text',
            'validation' => 'required|max:250',

            'slug' => true,
            'title' => 'Adres',
            'placeholder' => 'Adres',
            'description' => '',
        ],
        'latitude' => [
            'type' => 'number',
            'step' => '0.000001',
            'validation' => 'nullable|numeric|min:0|max:9999',
            'title' => 'Enlem',
            'placeholder' => 'Enlem',
            'description' => '',
        ],
        'longitude' => [
            'type' => 'number',
            'step' => '0.000001',
            'validation' => 'nullable|numeric|min:0|max:9999',
            'title' => 'Boylam',
            'placeholder' => 'Boylam',
            'description' => '',
        ],
        'ground_property' => [
            'type' => 'select2',
            'validation' => 'nullable',
            'multiple' => 'multiple',

            'relationship' => 'grounds_properties', //model function
            'relation_col' => 'property_id',
            'dataSource' => 'App\GroundProperty',
            
            'title' => 'Halı Saha Özellikleri',
            'description' => '',
        ],
    ],

    'ground_properties' => [
        'name' => [
            'type' => 'text',
            'validation' => 'required|max:100',
            'title' => 'Ad',
            'placeholder' => 'Ad',
            'description' => '',
        ],
    ],

    'reservations' => [
        //rwr
        'league_id' => [
            'type' => 'select2',
            'validation' => 'required',

            'dataSource' => 'App\League',
            'seperator' => 'reservation_league_id',

            'title' => 'Lig',
            'description' => '',
        ],
        'season_id' => [
            'type' => 'select2',
            'validation' => 'required',

            'relation_col' => 'league_id',
            'seperator' => 'reservation_season_id',

            'ajax' => true,
            'ajax_url' => '/getSeasonsL',
            'search' => true,
            'hoop' => true,

            'title' => 'Sezon',
            'description' => '',
        ],



        'ground_id' => [
            'type' => 'select2',
            'validation' => 'required',

            'relation_col' => 'season_id',
            'seperator' => 'reservation_ground_id',

            'ajax' => true,
            'ajax_url' => '/getGroundsRe',
            'search' => true,
            'hoop' => true,

            'title' => 'Halı Saha',
            'description' => '',
        ],

        'team_id' => [
            'type' => 'select2',
            'validation' => 'required',

            'relationship' => 'team',
            'relation_col' => 'season_id',
            'dataSource' => 'App\TeamTeamSeason',
            'seperator' => 'reservation_team_id',

            'ajax' => true,
            'ajax_url' => '/getTeamsR',
            'search' => true,
            //'trigger' => 'season_id',

            'title' => 'Takım',
            'description' => '',
        ],



        'date' => [
            'type' => 'datepicker',
            'validation' => 'required|date|date_format:d.m.Y',
            'seperator' => 'reservation_date',

            'title' => 'Maç Tarihi',
            'placeholder' => 'Maç Tarihi',
            'description' => '',
        ],
        'time' => [
            'type' => 'select2',
            'validation' => 'required',

            'relation_col' => 'date',
            'seperator' => 'reservation_time',

            'ajax' => true,
            'ajax_url' => '/getGroundHourF',
            'search' => true,
            'hoop' => true,

            'title' => 'Maç Saati',
            'description' => '',
        ],
    ],

    'player_positions' => [
        'name' => [
            'type' => 'text',
            'validation' => 'required|max:100',
            'title' => 'Ad',
            'placeholder' => 'Ad',
            'description' => '',
        ],
        'code' => [
            'type' => 'text',
            'validation' => 'required|max:3',
            'title' => 'Kod',
            'placeholder' => 'Kod',
            'description' => '',
        ],
        'order' => [
            'type' => 'number',
            'min' => '0',
            'max' => '9999',
            'step' => '1',
            'validation' => 'required|numeric|min:0|max:9999',
            'title' => 'Sıra',
            'placeholder' => 'Sıra',
            'description' => '',
        ],
    ],

    'point_types' => [
        'name' => [
            'type' => 'text',
            'validation' => 'required|max:100',
            'title' => 'Ad',
            'placeholder' => 'Ad',
            'description' => '',
        ],
        'active' => [
            'type' => 'checkbox',
            'title' => 'Aktif',
            'description' => '',
            'value' => 'active', 
            'checkvalue' => true, 
            'uncheckvalue' => false,
        ],
        'description' => [
            'type' => 'text',
            'validation' => 'required|max:150',
            'title' => 'Açıklama',
            'placeholder' => 'Açıklama',
            'description' => '',
        ],
        'min_point' => [
            'type' => 'number',
            'min' => '0',
            'max' => '9999',
            'step' => '1',
            'validation' => 'required|numeric|min:0|max:9999|min:0',
            'title' => 'Min Puan',
            'placeholder' => 'Min Puan',
            'description' => '',
        ],
        'max_point' => [
            'type' => 'number',
            'min' => '0',
            'max' => '9999',
            'step' => '1',
            'validation' => 'required|numeric|min:0|max:9999|min:0',
            'title' => 'Max Puan',
            'placeholder' => 'Max Puan',
            'description' => '',
        ],
        'win_score' => [
            'type' => 'number',
            'min' => '0',
            'max' => '9999',
            'step' => '1',
            'validation' => 'required|numeric|min:0|max:9999|min:0',
            'title' => 'Galibiyet Puanı',
            'placeholder' => 'Galibiyet Puanı',
            'description' => '',
        ],
        'defeat_score' => [
            'type' => 'number',
            'min' => '0',
            'max' => '9999',
            'step' => '1',
            'validation' => 'required|numeric|min:0|max:9999|min:0',
            'title' => 'Yenilgi Puanı',
            'placeholder' => 'Yenilgi Puanı',
            'description' => '',
        ],
        'draw_score' => [
            'type' => 'number',
            'min' => '0',
            'max' => '9999',
            'step' => '1',
            'validation' => 'required|numeric|min:0|max:9999|min:0',
            'title' => 'Beraberlik Puanı',
            'placeholder' => 'Beraberlik Puanı',
            'description' => '',
        ],
        'color' => [
            'type' => 'text',
            'validation' => 'required|max:6',
            'title' => 'Renk',
            'placeholder' => 'Renk',
            'description' => '',
        ],
    ],

    'tactics' => [
        'name' => [
            'type' => 'text',
            'validation' => 'required|max:100',
            'title' => 'Ad',
            'placeholder' => 'Ad',
            'description' => '',
        ],
    ],

    'team_potentials' => [
        'name' => [
            'type' => 'text',
            'validation' => 'required|max:100',
            'title' => 'Ad',
            'placeholder' => 'Ad',
            'description' => '',
        ],
    ],

    'team_satisfactions' => [
        'name' => [
            'type' => 'text',
            'validation' => 'required|max:100',
            'title' => 'Ad',
            'placeholder' => 'Ad',
            'description' => '',
        ],
    ],

    'team_satisfactions_jogo' => [
        'name' => [
            'type' => 'text',
            'validation' => 'required|max:100',
            'title' => 'Ad',
            'placeholder' => 'Ad',
            'description' => '',
        ],
    ],

    'reservation_offer_convert_match' => [
        'header_cm' => [
            'type' => 'h3',
            'title' => 'Maça Dönüştür',
        ],
        'reservation_offer_id' => [
            'type' => 'hidden',
            'exception' => 'reservation_offer_id',
            'validation' => 'nullable',
            'title' => '',
            'placeholder' => '',
            'description' => '',
        ],
        'referee_id' => [
            'type' => 'select2',
            'validation' => 'required',

            //'relationship' => 'referee',
            'relation_col' => 'season_id',
            //'dataSource' => 'App\RefereeLeague',
            'seperator' => 'match_referee',

            'ajax' => true,
            'ajax_url' => '/getUsersRe',
            'search' => true,
            'hoop' => true,

            'title' => 'Hakem',
            'description' => '',
        ],
        'coordinator_id' => [
            'type' => 'select2',
            'validation' => 'required',

            //'relationship' => 'referee',
            'relation_col' => 'season_id',
            //'dataSource' => 'App\RefereeLeague',
            'seperator' => 'match_coordinator',

            'ajax' => true,
            'ajax_url' => '/getUsersCo',
            'search' => true,
            'hoop' => true,

            'title' => 'Koordinatör',
            'description' => '',
        ],
    ],

    'banners' => [
        'name' => [
            'type' => 'text',
            'validation' => 'required|max:100',
            'title' => 'Ad',
            'placeholder' => 'Ad',
            'description' => '',
        ],
        'url' => [
            'type' => 'text',
            'validation' => 'nullable|max:255',

            'title' => 'Banner URL',
            'placeholder' => 'Banner URL',
            'description' => '',
        ],
    ],


    'player_penalties' => [
        'polymorphic_ctype_id' => [
            'type' => 'hidden',
            'defaultValue' => '44',
            'validation' => 'nullable',
            'title' => '',
            'placeholder' => '',
            'description' => '',
        ],
        'league_id' => [
            'type' => 'select2',
            'validation' => 'required',
            'dataSource' => 'App\League',
            'title' => 'Lig',
            'description' => '',
        ],
        'season_id' => [
            'type' => 'select2',
            'validation' => 'required',

            //'relationship' => 'league_eliminationseason', //model function
            'relation_col' => 'league_id',
            'dataSource' => 'App\Season',
            'seperator' => 'player_penalties_season_id',

            'ajax' => true,
            'ajax_url' => '/getSeasonsL',
            'search' => true,
            'hoop' => true,

            'title' => 'Sezon',
            'description' => '',
        ],
        'match_id' => [
            'type' => 'select2',
            'validation' => 'required',
            //'multiple' => 'multiple',

            //'relationship' => 'team',
            'relation_col' => 'season_id',
            //'dataSource' => 'App\TeamTeamSeason',
            'seperator' => 'player_penalties_match_id',

            'ajax' => true,
            'ajax_url' => '/getSeasonMatches',
            'search' => true,
            'hoop' => true,
            //'trigger' => 'season_id',

            'title' => 'Maç',
            'description' => '',
        ],
        'description' => [
            'type' => 'textarea',
            'validation' => 'nullable',
            'title' => 'Açıklama',
            'placeholder' => 'Açıklama',
            'description' => '',
        ],
        'penalties_type_id' => [
            'type' => 'select2',
            'validation' => 'nullable',
            //'multiple' => 'multiple',

            //'relationship' => 'team',
            //'relation_col' => 'season_id',
            //'dataSource' => 'App\TeamTeamSeason',
            'seperator' => 'ppenalties_type_id',

            'ajax' => true,
            'ajax_url' => '/getPlayerPenaltyTypesS',
            'search' => true,
            'hoop' => true,
            //'trigger' => 'season_id',

            'title' => 'Ceza Türü',
            'description' => '',
        ],
        'match_player_id' => [
            'type' => 'select2',
            'validation' => 'required',

            //'relationship' => 'team',
            'relation_col' => 'match_id',
            //'dataSource' => 'App\TeamTeamSeason',
            'seperator' => 'player_penalties_match_player_id',

            'ajax' => true,
            'ajax_url' => '/getMatchPlayer',
            'search' => true,
            'hoop' => true,
            //'trigger' => 'season_id',

            'title' => 'Oyuncu',
            'description' => '',
        ],
    ],

    'team_penalties' => [
        'polymorphic_ctype_id' => [
            'type' => 'hidden',
            'defaultValue' => '47',
            'validation' => 'nullable',
            'title' => '',
            'placeholder' => '',
            'description' => '',
        ],
        'league_id' => [
            'type' => 'select2',
            'validation' => 'required',
            'dataSource' => 'App\League',
            'title' => 'Lig',
            'description' => '',
        ],
        'season_id' => [
            'type' => 'select2',
            'validation' => 'required',

            //'relationship' => 'league_eliminationseason', //model function
            'relation_col' => 'league_id',
            'dataSource' => 'App\Season',
            'seperator' => 'team_penalties_season_id',

            'ajax' => true,
            'ajax_url' => '/getSeasonsL',
            'search' => true,
            'hoop' => true,

            'title' => 'Sezon',
            'description' => '',
        ],
        'match_id' => [
            'type' => 'select2',
            'validation' => 'required',
            //'multiple' => 'multiple',

            //'relationship' => 'team',
            'relation_col' => 'season_id',
            //'dataSource' => 'App\TeamTeamSeason',
            'seperator' => 'team_penalties_match_id',

            'ajax' => true,
            'ajax_url' => '/getSeasonMatches',
            'search' => true,
            'hoop' => true,
            //'trigger' => 'season_id',

            'title' => 'Maç',
            'description' => '',
        ],
        'description' => [
            'type' => 'textarea',
            'validation' => 'nullable',
            'title' => 'Açıklama',
            'placeholder' => 'Açıklama',
            'description' => '',
        ],
        'penalties_type_id' => [
            'type' => 'select2',
            'validation' => 'nullable',
            //'multiple' => 'multiple',

            //'relationship' => 'team',
            //'relation_col' => 'season_id',
            //'dataSource' => 'App\TeamTeamSeason',
            'seperator' => 'tpenalties_type_id',

            'ajax' => true,
            'ajax_url' => '/getTeamPenaltyTypesS',
            'search' => true,
            'hoop' => true,
            //'trigger' => 'season_id',

            'title' => 'Ceza Türü',
            'description' => '',
        ],
        'match_team_id' => [
            'type' => 'select2',
            'validation' => 'required',

            //'relationship' => 'team',
            'relation_col' => 'match_id',
            //'dataSource' => 'App\TeamTeamSeason',
            'seperator' => 'team_penalties_match_team_id',

            'ajax' => true,
            'ajax_url' => '/getMatchTeam',
            'search' => true,
            'hoop' => true,
            //'trigger' => 'season_id',

            'title' => 'Takım',
            'description' => '',
        ],
    ],

    'penalty_types' => [
        'name' => [
            'type' => 'text',
            'validation' => 'required|max:100',
            'title' => 'Ad',
            'placeholder' => 'Ad',
            'description' => '',
        ],
        'to_who' => [
            'type' => 'select2',
            'validation' => 'required',
            'dataSource' => 'json_data',
            'title' => 'Kime',
            'description' => '',
            'json_data' => [
                1 => ['id' => 'player', 'name' => 'Oyuncu'],
                2 => ['id' => 'team', 'name' => 'Takım'],
            ]
        ],
    ],

    'monthly_panorama_player' => [

        'header' => [
            'type' => 'h3',
            'title' => 'Aylık Panorama Ekle / Düzenle',
        ],
        'monthly_panorama_id' => [
            'type' => 'hidden',
            'model_col' => 'id',
            'validation' => 'nullable',
            'title' => '',
            'placeholder' => '',
            'description' => '',
        ],
        'panorama_id' => [
            'type' => 'select2',
            'validation' => 'required',

            'seperator' => 'monthly_panorama_player_types',
            'dataSource' => 'App\PanoramaType',
            
            'title' => 'Panorama',
            'description' => '',
        ],
        'season_id' => [
            'type' => 'select2',
            'validation' => 'required',

            //'relationship' => 'team',
            'dataSource' => 'App\Season',
            'relation_col' => 'panorama_id',
            'seperator' => 'monthly_panorama_player_season',

            'ajax' => true,
            'ajax_url' => '/getSeasonsPanormaAll',
            'search' => true,
            'hoop' => true,
            //'trigger' => 'season_id',

            'title' => 'Sezon',
            'description' => '',
        ],
        'team_id' => [
            'type' => 'select2',
            'validation' => 'required',

            'relation_col' => 'season_id',
            'dataSource' => 'App\Team',
            'seperator' => 'monthly_panorama_player_team',

            'ajax' => true,
            'ajax_url' => '/getPanoramaTeamsAllS',
            'search' => true,
            'hoop' => true,

            'title' => 'Takımlar',
            'description' => '',
        ],

        'player_id' => [
            'type' => 'select2',
            'validation' => 'required',

            //'relationship' => 'team',
            'relation_col' => 'team_id',
            'dataSource' => 'App\Player',
            'seperator' => 'monthly_panorama_player_player',

            'ajax' => true,
            'ajax_url' => '/getTeamPlayer',
            'search' => true,
            'hoop' => true,
            //'trigger' => 'season_id',

            'title' => 'Oyuncu',
            'description' => '',
        ],
        'date' => [
            'type' => 'datepicker',
            'validation' => 'required|date|date_format:d.m.Y',

            'title' => 'Tarih',
            'placeholder' => 'Tarih',
            'description' => '',
        ],
    ],

    'monthly_panorama_team' => [

        'header' => [
            'type' => 'h3',
            'title' => 'Aylık Panorama Ekle / Düzenle',
        ],
        'monthly_panorama_id' => [
            'type' => 'hidden',
            'model_col' => 'id',
            'validation' => 'nullable',
            'title' => '',
            'placeholder' => '',
            'description' => '',
        ],
        'panorama_id' => [
            'type' => 'select2',
            'validation' => 'required',

            'seperator' => 'monthly_panorama_team_types',
            'dataSource' => 'App\PanoramaType',
            
            'title' => 'Panorama',
            'description' => '',
        ],
        'season_id' => [
            'type' => 'select2',
            'validation' => 'required',

            //'relationship' => 'team',
            'dataSource' => 'App\Season',
            'relation_col' => 'panorama_id',
            'seperator' => 'monthly_panorama_player_season',

            'ajax' => true,
            'ajax_url' => '/getSeasonsPanormaAll',
            'search' => true,
            'hoop' => true,
            //'trigger' => 'season_id',

            'title' => 'Sezon',
            'description' => '',
        ],
        'team_id' => [
            'type' => 'select2',
            'validation' => 'required',

            'relation_col' => 'season_id',
            'dataSource' => 'App\Team',
            'seperator' => 'monthly_panorama_player_team',

            'ajax' => true,
            'ajax_url' => '/getPanoramaTeamsAllS',
            'search' => true,
            'hoop' => true,

            'title' => 'Takımlar',
            'description' => '',
        ],
        'date' => [
            'type' => 'datepicker',
            'validation' => 'required|date|date_format:d.m.Y',

            'title' => 'Tarih',
            'placeholder' => 'Tarih',
            'description' => '',
        ],
    ],

    'player_penalties' => [
        'polymorphic_ctype_id' => [
            'type' => 'hidden',
            'defaultValue' => '44',
            'validation' => 'nullable',
            'title' => '',
            'placeholder' => '',
            'description' => '',
        ],
        'league_id' => [
            'type' => 'select2',
            'validation' => 'required',
            'dataSource' => 'App\League',
            'title' => 'Lig',
            'description' => '',
        ],
        'season_id' => [
            'type' => 'select2',
            'validation' => 'required',

            //'relationship' => 'league_eliminationseason', //model function
            'relation_col' => 'league_id',
            'dataSource' => 'App\Season',
            'seperator' => 'player_penalties_season_id',

            'ajax' => true,
            'ajax_url' => '/getSeasonsL',
            'search' => true,
            'hoop' => true,

            'title' => 'Sezon',
            'description' => '',
        ],
        'match_id' => [
            'type' => 'select2',
            'validation' => 'required',
            //'multiple' => 'multiple',

            //'relationship' => 'team',
            'relation_col' => 'season_id',
            //'dataSource' => 'App\TeamTeamSeason',
            'seperator' => 'player_penalties_match_id',

            'ajax' => true,
            'ajax_url' => '/getSeasonMatches',
            'search' => true,
            'hoop' => true,
            //'trigger' => 'season_id',

            'title' => 'Maç',
            'description' => '',
        ],
        'description' => [
            'type' => 'textarea',
            'validation' => 'nullable',
            'title' => 'Açıklama',
            'placeholder' => 'Açıklama',
            'description' => '',
        ],
        'penalties_type_id' => [
            'type' => 'select2',
            'validation' => 'nullable',
            //'multiple' => 'multiple',

            //'relationship' => 'team',
            //'relation_col' => 'season_id',
            //'dataSource' => 'App\TeamTeamSeason',
            'seperator' => 'ppenalties_type_id',

            'ajax' => true,
            'ajax_url' => '/getPlayerPenaltyTypesS',
            'search' => true,
            'hoop' => true,
            //'trigger' => 'season_id',

            'title' => 'Ceza Türü',
            'description' => '',
        ],
        'match_player_id' => [
            'type' => 'select2',
            'validation' => 'required',

            //'relationship' => 'team',
            'relation_col' => 'match_id',
            //'dataSource' => 'App\TeamTeamSeason',
            'seperator' => 'player_penalties_match_player_id',

            'ajax' => true,
            'ajax_url' => '/getMatchPlayer',
            'search' => true,
            'hoop' => true,
            //'trigger' => 'season_id',

            'title' => 'Oyuncu',
            'description' => '',
        ],
    ],

    'team_penalties' => [
        'polymorphic_ctype_id' => [
            'type' => 'hidden',
            'defaultValue' => '47',
            'validation' => 'nullable',
            'title' => '',
            'placeholder' => '',
            'description' => '',
        ],
        'league_id' => [
            'type' => 'select2',
            'validation' => 'required',
            'dataSource' => 'App\League',
            'title' => 'Lig',
            'description' => '',
        ],
        'season_id' => [
            'type' => 'select2',
            'validation' => 'required',

            //'relationship' => 'league_eliminationseason', //model function
            'relation_col' => 'league_id',
            'dataSource' => 'App\Season',
            'seperator' => 'team_penalties_season_id',

            'ajax' => true,
            'ajax_url' => '/getSeasonsL',
            'search' => true,
            'hoop' => true,

            'title' => 'Sezon',
            'description' => '',
        ],
        'match_id' => [
            'type' => 'select2',
            'validation' => 'required',
            //'multiple' => 'multiple',

            //'relationship' => 'team',
            'relation_col' => 'season_id',
            //'dataSource' => 'App\TeamTeamSeason',
            'seperator' => 'team_penalties_match_id',

            'ajax' => true,
            'ajax_url' => '/getSeasonMatches',
            'search' => true,
            'hoop' => true,
            //'trigger' => 'season_id',

            'title' => 'Maç',
            'description' => '',
        ],
        'description' => [
            'type' => 'textarea',
            'validation' => 'nullable',
            'title' => 'Açıklama',
            'placeholder' => 'Açıklama',
            'description' => '',
        ],
        'penalties_type_id' => [
            'type' => 'select2',
            'validation' => 'nullable',
            //'multiple' => 'multiple',

            //'relationship' => 'team',
            //'relation_col' => 'season_id',
            //'dataSource' => 'App\TeamTeamSeason',
            'seperator' => 'tpenalties_type_id',

            'ajax' => true,
            'ajax_url' => '/getTeamPenaltyTypesS',
            'search' => true,
            'hoop' => true,
            //'trigger' => 'season_id',

            'title' => 'Ceza Türü',
            'description' => '',
        ],
        'match_team_id' => [
            'type' => 'select2',
            'validation' => 'required',

            //'relationship' => 'team',
            'relation_col' => 'match_id',
            //'dataSource' => 'App\TeamTeamSeason',
            'seperator' => 'team_penalties_match_team_id',

            'ajax' => true,
            'ajax_url' => '/getMatchTeam',
            'search' => true,
            'hoop' => true,
            //'trigger' => 'season_id',

            'title' => 'Takım',
            'description' => '',
        ],
    ],

    'panorama_types' => [
        'name' => [
            'type' => 'text',
            'validation' => 'required|max:100',
            'title' => 'Ad',
            'placeholder' => 'Ad',
            'description' => '',
        ],
        'league_id' => [
            'type' => 'select2',
            'validation' => 'nullable',
            'dataSource' => 'App\League',
            'title' => 'Lig',
            'description' => '',
        ],
        'season_id' => [
            'type' => 'select2',
            'validation' => 'nullable',

            //'relationship' => 'league_eliminationseason', //model function
            'relation_col' => 'league_id',
            'dataSource' => 'App\Season',

            'seperator' => 'panorama_types_season',

            'ajax' => true,
            'ajax_url' => '/getSeasonsLAll',
            'search' => true,
            'hoop' => true,

            'title' => 'Sezon',
            'description' => '',
        ],
        'to_who' => [
            'type' => 'select2',
            'validation' => 'required',
            'dataSource' => 'json_data',
            'title' => 'Kime',
            'description' => '',
            'json_data' => [
                1 => ['id' => 'player', 'name' => 'Oyuncu'],
                2 => ['id' => 'team', 'name' => 'Takım']
            ]
        ],
        'period' => [
            'type' => 'select2',
            'validation' => 'required',
            'dataSource' => 'json_data',
            'title' => 'Periyot',
            'description' => '',
            'json_data' => [
                1 => ['id' => 'match', 'name' => 'Maç'],
                2 => ['id' => 'month', 'name' => 'Aylık'],
                //3 => ['id' => 'season', 'name' => 'Sezon'],
            ]
        ],
        'position_id' => [
            'type' => 'select2',
            'validation' => 'nullable',

            //'relationship' => 'player',
            //'relation_col' => 'position_id',
            'dataSource' => 'App\PlayerPosition',

            'title' => 'Posizyon',
            'description' => '',
        ],
    ],

    'news' => [
        'league_id' => [
            'type' => 'select2',
            'validation' => 'required',
            'multiple' => 'multiple',

            'relationship' => 'news_leagues', //model function
            'relation_col' => 'league_id',
            'dataSource' => 'App\League',
            
            'title' => 'Lig',
            'description' => '',
        ],
        'name' => [
            'type' => 'text',
            'validation' => 'required|max:100',
            //'slug' => true,
            'title' => 'Ad',
            'placeholder' => 'Ad',
            'description' => '',
        ],
        'slug' => [
            'type' => 'text',
            'validation' => 'required|max:100',
            'title' => 'Slug',
            'placeholder' => 'Slug',
            'description' => '',
        ],
        'subtitle' => [
            'type' => 'textarea',
            'validation' => 'required',
            'title' => 'Alt Başlık',
            'placeholder' => 'Alt Başlık',
            'description' => '',
        ],
        'date' => [
            'type' => 'datetimepicker',
            'validation' => 'required',

            'title' => 'Haber Tarihi',
            'placeholder' => 'Haber Tarihi',
            'description' => '',
        ],
        'type_id' => [
            'type' => 'select2',
            'validation' => 'required',

            'dataSource' => 'App\NewsTypes',
            
            'title' => 'Haber Türü',
            'description' => '',
        ],
        'content' => [
            'type' => 'html',
            'validation' => 'nullable',
            'title' => 'İçerik',
            'placeholder' => 'İçerik',
            'description' => '',
            'custom' => true,
        ],
        'headline' => [
            'type' => 'checkbox',
            'title' => 'Manşet',
            'description' => '',
            'value' => 'headline', 
            'checkvalue' => true, 
            'uncheckvalue' => false,
        ],
        'tag_id' => [
            'type' => 'select2',
            'validation' => 'nullable',
            'multiple' => 'multiple',

            'relationship' => 'news_tags', //model function
            'relation_col' => 'tag_id',
            'dataSource' => 'App\Tag',
            
            'title' => 'Etiketler',
            'description' => '',
        ],
        'active' => [
            'type' => 'checkbox',
            'title' => 'Aktif',
            'description' => '',
            'value' => 'active', 
            'checkvalue' => true, 
            'uncheckvalue' => false,
        ],
    ],

    'news_types' => [
        'name' => [
            'type' => 'text',
            'validation' => 'required|max:100',
            'title' => 'Ad',
            'placeholder' => 'Ad',
            'description' => '',
        ],
        'order' => [
            'type' => 'number',
            'step' => '1',
            'validation' => 'required|numeric|min:0|max:9999',
            'title' => 'Sıra',
            'placeholder' => 'Sıra',
            'description' => '',
        ],
        'active' => [
            'type' => 'checkbox',
            'title' => 'Aktif',
            'description' => '',
            'value' => 'active', 
            'checkvalue' => true, 
            'uncheckvalue' => false,
        ],
    ],

    'agenda' => [
        'name' => [
            'type' => 'text',
            'validation' => 'required|max:100',
            //'slug' => true,
            'title' => 'Ad',
            'placeholder' => 'Ad',
            'description' => '',
        ],
        'slug' => [
            'type' => 'text',
            'validation' => 'required|max:50',
            'title' => 'Slug',
            'placeholder' => 'Slug',
            'description' => '',
        ],
        'link' => [
            'type' => 'text',
            'validation' => 'required|max:250',
            'title' => 'Link',
            'placeholder' => 'Link',
            'description' => '',
        ],
    ],


    'tags' => [
        'name' => [
            'type' => 'text',
            'validation' => 'required|max:100',
            'title' => 'Ad',
            'placeholder' => 'Ad',
            'description' => '',
        ],
    ],

    'rules' => [
        'name' => [
            'type' => 'text',
            'validation' => 'required|max:100',
            'title' => 'Ad',
            'placeholder' => 'Ad',
            'description' => '',
        ],
        'rule_text' => [
            'type' => 'html',
            'validation' => 'nullable',
            'title' => 'Kurallar',
            'description' => '',
            'custom' => true,
        ],
        'order' => [
            'type' => 'number',
            'min' => '0',
            'max' => '9999',
            'step' => '1',
            'validation' => 'required|numeric|min:0|max:9999',
            'title' => 'Sıra',
            'placeholder' => 'Sıra',
            'description' => '',
        ],
    ],


];