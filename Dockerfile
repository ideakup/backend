FROM php:7.3.28-fpm-alpine3.13

RUN apk add --no-cache openssl bash nodejs npm postgresql-dev libxml2-dev
RUN docker-php-ext-install bcmath pdo pdo_pgsql soap

RUN apk add --no-cache freetype libpng libjpeg-turbo freetype-dev libpng-dev jpeg-dev libjpeg libjpeg-turbo-dev 
RUN docker-php-ext-configure gd --with-freetype-dir=/usr/lib/ --with-png-dir=/usr/lib/ --with-jpeg-dir=/usr/lib/ --with-gd
RUN docker-php-ext-install gd

WORKDIR /var/www

RUN rm -rf /var/www/html
RUN ln -s public html
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

COPY . /var/www

RUN chmod -R 777 /var/www/storage

EXPOSE 9000
ENTRYPOINT [ "php-fpm" ]