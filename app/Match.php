<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Match extends Model
{
    protected $table = 'matches';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;

    //public.matches.matches_coordinator_id_427875e1_fk_coordinators_id
    public function coordinator()
    {
        return $this->hasOne('App\Coordinator', 'id', 'coordinator_id');
    }

	//public.matches.matches_ground_id_91bba733_fk_grounds_id
    public function ground()
    {
        return $this->hasOne('App\Ground', 'id', 'ground_id');
    }

	//public.matches.matches_referee_id_4baa2316_fk_referees_id
    public function referee()
    {
        return $this->hasOne('App\Referee', 'id', 'referee_id');
    }

	//public.matches.matches_season_id_1c71f317_fk_seasons_id
	public function season()
    {
        return $this->belongsTo('App\Season', 'season_id', 'id');
    }

	//public.matches.matches_team1_id_7d18cbe0_fk_teams_id
	public function team1()
    {
        return $this->hasOne('App\Team', 'id', 'team1_id');
    }

	//public.matches.matches_team2_id_a6be4f57_fk_teams_id
    public function team2()
    {
        return $this->hasOne('App\Team', 'id', 'team2_id');
    }

	//public.eliminationtree_items.eliminationtree_items_match_id_059193c8_fk_matches_id
    public function eliminationtree_items()
    {
        return $this->hasOne('App\EliminationTreeItem', 'match_id', 'id');
    }
    


	//public.match_actions.match_actions_match_id_eb025b14_fk_matches_id
    public function match_action()
    {
        return $this->hasMany('App\MatchAction', 'match_id', 'id');
    }

    public function match_action_order_minute()
    {
        return $this->hasMany('App\MatchAction', 'match_id', 'id')->orderBy('minute', 'asc');
    }

	//public.match_image.match_image_match_id_f77ca2f0_fk_matches_id
    public function match_image()
    {
        return $this->hasMany('App\MatchImage', 'match_id', 'id');
    }

	//public.match_panorama.match_panorama_match_id_c7e8b252_fk_matches_id
    public function match_panorama()
    {
        return $this->hasMany('App\MatchPanorama', 'match_id', 'id');
    }

	//public.match_player.match_player_match_id_36a2729e_fk_matches_id
    public function match_player()
    {
        return $this->hasMany('App\MatchPlayer', 'match_id', 'id');
    }

    public function match_player_is_not_guest()
    {
        return $this->hasMany('App\MatchPlayer', 'match_id', 'id')->where('player_id', '!=', null);
    }

    public function match_player_is_guest()
    {
        return $this->hasMany('App\MatchPlayer', 'match_id', 'id')->where('player_id', null);
    }

	//public.match_video.match_video_match_id_47d9c0bb_fk_matches_id
    public function match_video()
    {
        return $this->hasOne('App\MatchVideo', 'match_id', 'id');
    }

	//public.penalties.panorama_penal_match_id_3a1b4f65_fk_matches_id

	//public.press_conferences.press_conferences_match_id_4ea38742_fk_matches_id
    public function match_press()
    {
        return $this->hasOne('App\PressConferences', 'match_id', 'id');
    }

	//public.reservations.reservations_match_id_ae01fb70_fk_matches_id
    public function reservation()
    {
        return $this->belongsTo('App\Reservation', 'id', 'match_id');
    }

    /*
    //***************
    //Get Date Format
    public function getCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    //***************
    //Set Date Format
    public function setCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function setUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }
    */
    
}