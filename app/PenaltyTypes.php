<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class PenaltyTypes extends Model
{
    protected $table = 'penal_types';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;


	//public.penal_types.panorama_penaltype_league_id_57e6ec85_fk_leagues_id

	
	//public.penal_types.panorama_penaltype_season_id_789b312b_fk_seasons_id

	
	//public.player_penalties.panorama_penalplayer_penal_type_id_67eedef6_fk_panorama_

	
	//public.team_penalties.panorama_penalteam_penal_type_id_db08f36d_fk_panorama_

    
    /*
    //***************
    //Get Date Format
    public function getCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    //***************
    //Set Date Format
    public function setCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function setUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }
    */
    
}