<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class News extends Model
{
    protected $table = 'news';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;

	//public.news.news_created_by_id_aa492904_fk_users_id
	public function user()
    {
        return $this->hasOne('App\User', 'id', 'created_by_id');
    }

	//public.news.news_photo_gallery_id_4f15cd03_fk_photo_gal
	public function photo_gallery()
    {
        return $this->hasOne('App\PhotoGallery', 'gallery_ptr_id', 'photo_gallery_id');
    }

	//public.news.news_type_id_2af31cfb_fk_news_types_id
    public function news_type()
    {
        return $this->hasOne('App\NewsTypes', 'id', 'type_id');
    }

	//public.news.news_video_gallery_id_8cdad97b_fk_video_gal
	public function video_gallery()
    {
        return $this->hasOne('App\VideoGallery', 'gallery_ptr_id', 'video_gallery_id');
    }


	//public.news_leagues.news_leagues_news_id_9fc102a9_fk_news_id
	public function news_leagues()
    {
        return $this->hasMany('App\NewsLeagues', 'news_id', 'id');
    }

	//public.news_tags.news_tags_news_id_06acc240_fk_news_id
    public function news_tags()
    {
        return $this->hasMany('App\NewsTags', 'news_id', 'id');
    }
    
    /*
    //***************
    //Get Date Format
    public function getCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    //***************
    //Set Date Format
    public function setCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function setUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }
    */
    
}