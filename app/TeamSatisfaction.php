<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class TeamSatisfaction extends Model
{
    protected $table = 'team_satisfactions';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;

    public function team_survey()
    {
        return $this->belongsTo('App\TeamSurvey', 'id', 'satisfaction_id');
    }

    
    /*
    //***************
    //Get Date Format
    public function getCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    //***************
    //Set Date Format
    public function setCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function setUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }
    */
    
}