<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class ReservationOffer extends Model
{
    protected $table = 'reservation_offer';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;

    //	public.reservation_offer.reservation_offer_player_id_b6d05d0b_fk_players_id	auto
    public function player()
    {
        return $this->hasOne('App\Player', 'id', 'players_id');
    }

	//	public.reservation_offer.reservation_offer_reservation_id_3e60b189_fk_reservations_id	auto
    public function reservation()
    {
        return $this->hasOne('App\Reservation', 'id', 'reservation_id');
    }

	//	public.reservation_offer.reservation_offer_team_id_f25fa80e_fk_teams_id	auto
    public function team()
    {
        return $this->hasOne('App\Team', 'id', 'team_id');
    }

    
    /*
    //***************
    //Get Date Format
    public function getCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    //***************
    //Set Date Format
    public function setCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function setUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }
    */
    
}