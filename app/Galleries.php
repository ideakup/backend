<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Galleries extends Model
{
    protected $table = 'galleries';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;

	//public.galleries.galleries_created_by_id_bb21a958_fk_users_id

	//public.galleries.galleries_polymorphic_ctype_id_7aca4da0_fk_django_co

	//public.galleries_leagues.galleries_leagues_gallery_id_f0a9d9b1_fk_galleries_id
    public function galleries_leagues()
    {
        return $this->hasMany('App\GalleriesLeagues', 'gallery_id', 'id');
    }
	
	//public.photo_galleries.photo_galleries_gallery_ptr_id_d5f0a74c_fk_galleries_id

	//public.video_galleries.video_galleries_gallery_ptr_id_3e5932a9_fk_galleries_id

    
    /*
    //***************
    //Get Date Format
    public function getCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    //***************
    //Set Date Format
    public function setCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function setUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }
    */
    
}