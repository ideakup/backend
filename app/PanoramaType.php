<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class PanoramaType extends Model
{
    protected $table = 'panorama_types';
    protected $dateFormat = 'Y-m-d H:i:sO';


    //public.penalties.panorama_penal_league_id_3ecffddd_fk_leagues_id
    public function league()
    {
        return $this->hasOne('App\League', 'id', 'league_id');
    }

    //public.penalties.panorama_penal_season_id_471b75c6_fk_seasons_id
    public function season()
    {
        return $this->hasOne('App\Season', 'id', 'season_id');
    }

    //public.player_penalties.panorama_penalplayer_penal_ptr_id_4902942a_fk_panorama_penal_id
    public function panorama_types_prize()
    {
        return $this->hasOne('App\PanoramaTypePrize', 'panoramatype_ptr_id', 'id');
    }


    
    /*
    //***************
    //Get Date Format
    public function getCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    //***************
    //Set Date Format
    public function setCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function setUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }
    */
    
}
