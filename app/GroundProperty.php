<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class GroundProperty extends Model
{
    protected $table = 'ground_properties';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;
    
    public function grounds_properties()
    {
        return $this->belongsTo('App\GroundsProperty', 'id', 'property_id');
    }

    
    /*
    //***************
    //Get Date Format
    public function getCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    //***************
    //Set Date Format
    public function setCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function setUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }
    */
    
}