<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Reservation extends Model
{
    protected $table = 'reservations';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;

    //	public.reservations.reservations_captain_id_9a3342a2_fk_players_id	auto
    public function captain()
    {
        return $this->hasOne('App\User', 'id', 'captain_id');
    }

	//	public.reservations.reservations_ground_table_id_3c3e5865_fk_reservati	auto
    public function reservation_groundtable()
    {
        return $this->hasOne('App\ReservationGroundtable', 'id', 'ground_table_id');
    }

	//	public.reservations.reservations_match_id_ae01fb70_fk_matches_id	auto
    public function match()
    {
        return $this->hasOne('App\Match', 'id', 'match_id');
    }

	//	public.reservations.reservations_teamseason_id_5f8d4381_fk_team_teamseason_id	auto
	public function team_teamseason()
    {
        return $this->hasOne('App\TeamTeamSeason', 'id', 'teamseason_id');
    }
    
	//	public.reservation_offer.reservation_offer_reservation_id_3e60b189_fk_reservations_id	normal
	public function reservation_offer()
    {
        return $this->hasMany('App\ReservationOffer', 'reservation_id', 'id');
        //return $this->belongsTo('App\ReservationOffer', 'id', 'reservation_id');
    }

    
    /*
    //***************
    //Get Date Format
    public function getCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    //***************
    //Set Date Format
    public function setCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function setUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }
    */
    
}