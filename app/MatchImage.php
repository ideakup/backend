<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class MatchImage extends Model
{
    protected $table = 'match_image';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;
  
	//public.match_image.match_image_match_id_f77ca2f0_fk_matches_id
	public function match()
    {
        return $this->belongsTo('App\Match', 'match_id', 'id');
    }

    /*
    //***************
    //Get Date Format
    public function getCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    //***************
    //Set Date Format
    public function setCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function setUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }
    */
    
    
}