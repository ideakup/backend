<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Province extends Model
{
    protected $table = 'provinces';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;

    public function conference()
    {
        return $this->hasMany('App\Conference', 'province_id', 'id');
    }

    public function district()
    {
        return $this->hasMany('App\District', 'province_id', 'id')->orderBy('slug');
    }

    public function ground()
    {
        return $this->hasMany('App\Ground', 'province_id', 'id');
    }

    public function league_province()
    {
        return $this->hasMany('App\LeaguesProvince', 'province_id', 'id');
    }

    public function nostalgia()
    {
        return $this->hasMany('App\Nostalgia', 'province_id', 'id');
    }
	
	public function profile()
    {
        return $this->hasMany('App\Profile', 'province_id', 'id');
    }

    public function team()
    {
        return $this->hasMany('App\Team', 'province_id', 'id');
    }

    
    /*
    //***************
    //Get Date Format
    public function getCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    //***************
    //Set Date Format
    public function setCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function setUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }
    */
    
}