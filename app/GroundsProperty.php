<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class GroundsProperty extends Model
{
    protected $table = 'grounds_properties';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;

	public function ground()
    {
        return $this->belongsTo('App\Ground', 'ground_id', 'id');
    }

	public function ground_property()
    {
        return $this->hasOne('App\GroundProperty', 'id', 'property_id');
    }

}