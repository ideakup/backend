<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class LeagueGalleries extends Model
{
    protected $table = 'league_galleries';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;

    //public.league_galleries.league_galleries_league_id_6147a5e2_fk_leagues_id
    public function league()
    {
        return $this->belongsTo('App\League', 'league_id', 'id');
    }

    //public.league_gallery_items.league_gallery_items_gallery_id_0ecc2863_fk_league_galleries_id
    public function league_gallery_items()
    {
        return $this->hasMany('App\LeagueGalleryItems', 'gallery_id', 'id')->orderBy('id', 'desc');
    }

    public function items()
    {
        return $this->hasMany('App\LeagueGalleryItems', 'gallery_id', 'id')->select('id','description', 'embed_source', 'embed_code')->limit(10)->orderBy('id', 'desc');
    }

    
    /*
    //***************
    //Get Date Format
    public function getCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    //***************
    //Set Date Format
    public function setCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function setUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }
    */
    
}