<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Ground extends Model
{
    protected $table = 'grounds';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;

    public function district()
    {
        return $this->hasOne('App\District', 'id', 'district_id');
    }

    public function province()
    {
        return $this->hasOne('App\Province', 'id', 'province_id');
    }

    public function ground_images()
    {
        return $this->hasMany('App\GroundImage', 'ground_id', 'id');
    }

    public function grounds_properties()
    {
        return $this->hasMany('App\GroundsProperty', 'ground_id', 'id');
    }

	//public.matches.matches_ground_id_91bba733_fk_grounds_id
    public function match()
    {
        return $this->hasOne('App\Match', 'id', 'ground_id');
    }

	//public.reservation_groundsubscription.reservation_groundsubscription_ground_id_1b098520_fk_grounds_id
    public function reservation_groundsubscription()
    {
        return $this->hasMany('App\ReservationGroundsubscription', 'ground_id', 'id');
    }

	//public.reservation_groundtable.reservation_groundtable_ground_id_ca812d2d_fk_grounds_id
    public function reservation_groundtable()
    {
        return $this->hasMany('App\ReservationGroundtable', 'ground_id', 'id')->select('*', 'time as hour');
    }

    
    /*
    //***************
    //Get Date Format
    public function getCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    //***************
    //Set Date Format
    public function setCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function setUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }
    */
    
}