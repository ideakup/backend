<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class PlayerPenalty extends Model
{
    protected $table = 'player_penalties';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;
    protected $primaryKey = 'penal_ptr_id';
    
	//public.player_penalties.panorama_penalplayer_penal_ptr_id_4902942a_fk_panorama_penal_id
    public function penalty()
    {
        return $this->hasOne('App\Penalty', 'id', 'penal_ptr_id');
    }

	//public.player_penalties.panorama_penalplayer_penal_type_id_67eedef6_fk_panorama_
	public function penalty_type()
    {
        return $this->hasOne('App\PenaltyTypes', 'id', 'penal_type_id');
    }


	//public.player_penalties.player_penalties_player_id_66d726bc_fk_players_id
    public function player()
    {
        return $this->hasOne('App\Player', 'id', 'player_id');
    }

	//public.player_penalties.player_penalties_team_id_ce9ba0a4_fk_teams_id
    public function team()
    {
        return $this->hasOne('App\Team', 'id', 'team_id');
    }

}