<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class ReservationGroundtime extends Model
{
    protected $table = 'reservation_groundtime';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;

}