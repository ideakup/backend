<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class TeamTeamPlayer extends Model
{
    protected $table = 'team_teamplayer';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;


    public function player()
    {
        return $this->belongsTo('App\Player', 'player_id', 'id');
    }


    public function team()
    {
        return $this->belongsTo('App\Team', 'team_id', 'id');
    }

    
    /*
    //***************
    //Get Date Format
    public function getCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    //***************
    //Set Date Format
    public function setCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function setUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }
    */
    
}