<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class TeamTeamSeasonAddPointLog extends Model
{
    protected $table = 'team_teamseason_add_point_log';
    protected $dateFormat = 'Y-m-d H:i:sO';
    //public $timestamps = false;

    //public.team_teamseason_add_point_log.team_teamseason_add__teamseason_id_ee7fb7f6_fk_team_team
    public function teamseason()
    {
        return $this->belongsTo('App\TeamTeamSeason', 'id', 'teamseason_id');
    }

	//public.team_teamseason_add_point_log.team_teamseason_add_point_log_user_id_10826485_fk_users_id
    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    
    /*
    //***************
    //Get Date Format
    public function getCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    //***************
    //Set Date Format
    public function setCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function setUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }
    */
    
}