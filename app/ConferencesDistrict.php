<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class ConferencesDistrict extends Model
{
    protected $table = 'conferences_districts';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;

    public function conference()
    {
        return $this->hasMany('App\Conference', 'id', 'conference_id');
    }

    public function district()
    {
        return $this->hasMany('App\District', 'id', 'district_id');
    }
    
}