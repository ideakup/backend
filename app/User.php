<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

use Carbon\Carbon;

class User extends Authenticatable
{

    use HasRoles, HasApiTokens, Notifiable;
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;


    protected $fillable = [
        'first_name', 'last_name', 'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];


    //public.authtoken_token.authtoken_token_user_id_35299eff_fk_users_id


    public function coordinator()
    {
        return $this->belongsTo('App\Coordinator', 'id', 'user_id');
    }


    //public.django_admin_log.django_admin_log_user_id_c564eba6_fk_users_id


    //public.django_rest_passwordreset_resetpasswordtoken.django_rest_password_user_id_e8015b11_fk_users_id


    //public.galleries.galleries_created_by_id_bb21a958_fk_users_id


    //public.news.news_created_by_id_aa492904_fk_users_id
    public function news()
    {
        return $this->belongsTo('App\News', 'id', 'created_by_id');
    }

    public function player()
    {
        return $this->belongsTo('App\Player', 'id', 'user_id');
    }

    public function profile()
    {
        return $this->belongsTo('App\Profile', 'id', 'user_id');
    }

    public function referee()
    {
        return $this->belongsTo('App\Referee', 'id', 'user_id');
    }


    //public.team_teamseason_add_point_log.team_teamseason_add_point_log_user_id_10826485_fk_users_id


    //public.users_groups.users_groups_user_id_f500bee5_fk_users_id


    //public.users_leagues.users_leagues_user_id_6171aa81_fk_users_id


    //public.users_user_permissions.users_user_permissions_user_id_92473840_fk_users_id


    public function roleprovince()
    {
        return $this->hasMany('App\UserRoleHasProvince', 'user_id', 'id');
    }

    
    /*
    //***************
    //Get Date Format
    public function getCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    //***************
    //Set Date Format
    public function setCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function setUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }
    */
    
}