<?php
 
namespace App\Classes;

use Auth;
use Carbon\Carbon;

class SmsClass {

    public function checkHour()
    {
        $sendDate = '';
        if(Carbon::now()->greaterThanOrEqualTo(Carbon::now()->startOfDay()->addHours(9))){
            // Hemen Gönder
            $sendDate = '';
        }else{
            // Sabah 10:00'da Gönder
            $sendDate = Carbon::now()->startOfDay()->addHours(10)->format('dmYHi');   
        }
        return $sendDate;
    }

    public function smsTest()
    {
        $phone_no = '5552617703';
        $xml='<SingleTextSMS>
            <UserName>5334328802-3408</UserName>
            <PassWord>Dc@0123</PassWord>
            <Action>12</Action>
            <Mesgbody>test sms</Mesgbody>
            <Numbers>'.$phone_no.'</Numbers>
            <Originator>Rakipbul</Originator>
            <SDate></SDate>
        </SingleTextSMS>';
        $gelen = $this->XMLPOST('http://g.iletimx.com',$xml);
        return $gelen;
    }
	
    public function smsSend($phone_no, $phone_code)
    {
        //$phone_no = '5552617703';
        $xml='<SingleTextSMS>
            <UserName>5334328802-3408</UserName>
            <PassWord>Dc@0123</PassWord>
            <Action>12</Action>
            <Mesgbody>Telefon dogrulama kodu: '.$phone_code.'</Mesgbody>
            <Numbers>'.$phone_no.'</Numbers>
            <Originator>Rakipbul</Originator>
            <SDate></SDate>
        </SingleTextSMS>';
        $gelen = $this->XMLPOST('http://g.iletimx.com',$xml);
        return $gelen;
    }

    //CALL CENTER A GONDER
    public function smsSendTerminationCallCenter($phone_no, $team)
    {
        //$phone_no = '5552617703';
        $takimAdi = $team->name;
        $kaptanAdi = $team->captain->user->first_name.' '.$team->captain->user->last_name;
        $kaptanTel = $team->captain->user->phone;
        $smsText = $takimAdi.' takimi fesih talebinde bulundu. Kaptan: '.$kaptanAdi.' Tel: 0'.$kaptanTel.'. Kendisiyle iletisime gecip sebebini ogrenebilirsin.';
        
        $xml='<SingleTextSMS>
            <UserName>5334328802-3408</UserName>
            <PassWord>Dc@0123</PassWord>
            <Action>12</Action>
            <Mesgbody>'.$smsText.'</Mesgbody>
            <Numbers>'.$phone_no.'</Numbers>
            <Originator>Rakipbul</Originator>
            <SDate>'.$this->checkHour().'</SDate>
        </SingleTextSMS>';
        
        $gelen = $this->XMLPOST('http://g.iletimx.com',$xml);
        return $gelen;
    }

    //CALL CENTER A GONDER
    public function smsSendNewTeamCallCenter($phone_no, $team)
    {
        //$phone_no = '5552617703';
        $takimAdi = $team->name;
        $takimIlce = $team->district->name;
        $kaptanAdi = $team->captain->user->first_name.' '.$team->captain->user->last_name;
        $kaptanTel = $team->captain->user->phone;

        $smsText = 'Sehrinde '.$takimAdi.' takimi kuruldu! Kaptan: '.$kaptanAdi.' - Ilce: '.$takimIlce.' - Tel: 0'.$kaptanTel.'. Haydi simdi ara ve onlari ailemize kat ;)';

        $xml='<SingleTextSMS>
            <UserName>5334328802-3408</UserName>
            <PassWord>Dc@0123</PassWord>
            <Action>12</Action>
            <Mesgbody>'.$smsText.'</Mesgbody>
            <Numbers>'.$phone_no.'</Numbers>
            <Originator>Rakipbul</Originator>
            <SDate>'.$this->checkHour().'</SDate>
        </SingleTextSMS>';
        $gelen = $this->XMLPOST('http://g.iletimx.com',$xml);
        return $gelen;
    }

    //TAKIMI KURAN KAPTANA GONDER
    public function smsSendNewTeamCaptain($phone_no)
    {
        //$phone_no = '5552617703';
        $smsText = "Tebrikler takiminizi kurdunuz! Ilk macinizi almak icin https://bit.ly/rakipbul_ adresinden mac rezervasyonu yapabilir veya 05321111172'yi arayabilirsiniz.";

        $xml='<SingleTextSMS>
            <UserName>5334328802-3408</UserName>
            <PassWord>Dc@0123</PassWord>
            <Action>12</Action>
            <Mesgbody>'.$smsText.'</Mesgbody>
            <Numbers>'.$phone_no.'</Numbers>
            <Originator>Rakipbul</Originator>
            <SDate>'.$this->checkHour().'</SDate>
        </SingleTextSMS>';
        $gelen = $this->XMLPOST('http://g.iletimx.com',$xml);
        return $gelen;
    }

    //TRANSFER TEKLIFI ALAN OYUNCUYA GONDER
    public function smsSendTransferPlayer($phone_no)
    {
        //$phone_no = '5552617703';
        $smsText = "Transfer Teklifi Aldiniz! Aldiginiz transfer teklifini www.rakipbul.com adresine giris yapip profilinize tıklayarak goruntuleyebilirsiniz. ";
        $xml='<SingleTextSMS>
            <UserName>5334328802-3408</UserName>
            <PassWord>Dc@0123</PassWord>
            <Action>12</Action>
            <Mesgbody>'.$smsText.'</Mesgbody>
            <Numbers>'.$phone_no.'</Numbers>
            <Originator>Rakipbul</Originator>
            <SDate>'.$this->checkHour().'</SDate>
        </SingleTextSMS>';
        $gelen = $this->XMLPOST('http://g.iletimx.com',$xml);
        return $gelen;
    }

    //Mac Tamamlandiya Alininca Takim Kadrosundaki Tum Oyunculara Gonder
    public function smsSendMatchCompletedTrigger($match)
    {
        $phone_arr = array();
        foreach ($match->match_player_is_not_guest as $mplayer) {
            if($mplayer->player->user->infoshare || is_null($mplayer->player->user->infoshare)){
                $phone_arr[] = $mplayer->player->user->phone;
            }
        }
        $this->smsSendMatchCompleted(implode(",", $phone_arr), $match->id);
    }
    public function smsSendMatchCompleted($phone_no, $macId)
    {
        //$phone_no = '5552617703';
        $smsText = "Maciniz Yayinda! Son oynadiginiz macin detaylarina, pozisyon ve gol videolarına bu linkten ulasabilirsiniz. https://www.rakipbul.com/mac/".$macId;
        $xml='<SingleTextSMS>
            <UserName>5334328802-3408</UserName>
            <PassWord>Dc@0123</PassWord>
            <Action>12</Action>
            <Mesgbody>'.$smsText.'</Mesgbody>
            <Numbers>'.$phone_no.'</Numbers>
            <Originator>Rakipbul</Originator>
            <SDate>'.$this->checkHour().'</SDate>
        </SingleTextSMS>';
        $gelen = $this->XMLPOST('http://g.iletimx.com',$xml);
        return $gelen;
    }

    //Mac Olusturulunca Takim Kaptanina Gonder
    public function smsSendNewMatch($match)
    {
        $macId = $match->id;
        $macTarihi = $match->date;
        $macSaati = $match->time;

        $team1 = $match->team1;
        $team1captain = $match->team1->captain;

        $team2 = $match->team2;
        $team2captain = $match->team2->captain;

        if(!empty($team1captain) && ($team1captain->user->infoshare || is_null($team1captain->user->infoshare))){
            $smsText =  Carbon::parse($macTarihi)->format('d.m.Y')." saat ".Carbon::parse($macSaati)->format('H:i')." macinda rakibiniz ".$team2->name."! 2 takimin mac oncesi analizi: https://www.rakipbul.com/mac/".$macId."/oncesi";
            $phone_no = $team1captain->user->phone;
            //$phone_no = '5552617703';
            $xml='<SingleTextSMS>
                <UserName>5334328802-3408</UserName>
                <PassWord>Dc@0123</PassWord>
                <Action>12</Action>
                <Mesgbody>'.$smsText.'</Mesgbody>
                <Numbers>'.$phone_no.'</Numbers>
                <Originator>Rakipbul</Originator>
                <SDate>'.$this->checkHour().'</SDate>
            </SingleTextSMS>';
            $gelen = $this->XMLPOST('http://g.iletimx.com',$xml);
        }

        if(!empty($team2captain) && ($team2captain->infoshare || is_null($team2captain->infoshare))){
            $smsText = Carbon::parse($macTarihi)->format('d.m.Y')." saat ".Carbon::parse($macSaati)->format('H:i')." macinda rakibiniz ".$team1->name."! 2 takimin mac oncesi analizi: https://www.rakipbul.com/mac/".$macId."/oncesi";
            $phone_no = $team2captain->user->phone;
            //$phone_no = '5552617703';
            $xml='<SingleTextSMS>
                <UserName>5334328802-3408</UserName>
                <PassWord>Dc@0123</PassWord>
                <Action>12</Action>
                <Mesgbody>'.$smsText.'</Mesgbody>
                <Numbers>'.$phone_no.'</Numbers>
                <Originator>Rakipbul</Originator>
                <SDate>'.$this->checkHour().'</SDate>
            </SingleTextSMS>';
            $gelen = $this->XMLPOST('http://g.iletimx.com',$xml);
        }
        
        return $gelen;
    }

    //20 puan üstüne çıkmış takım kaptanına gönder
    public function smsSendRbkirmizi($phone_no)
    {

        //$phone_no = '5552617703';

        $smsText = 'Değerli kaptanımız, Rakipbul Liginde kırmızı takım kategorisine yükseldiniz. RBKIRMIZI koduyla www.jogo.com.tr adresinden yapacağınız NIKE ve JOGO forma alışverişinizde %15 indirim hakkı kazandınız.';

        $xml='<SingleTextSMS>
            <UserName>5334328802-3408</UserName>
            <PassWord>Dc@0123</PassWord>
            <Action>12</Action>
            <Mesgbody>'.$smsText.'</Mesgbody>
            <Numbers>'.$phone_no.'</Numbers>
            <Originator>Rakipbul</Originator>
            <SDate>'.$this->checkHour().'</SDate>
        </SingleTextSMS>';
        $gelen = $this->XMLPOST('http://g.iletimx.com',$xml);
        return $gelen;

    }

    public function XMLPOST($PostAddress,$xmlData){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$PostAddress);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,2);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml"));
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlData);
        $result = curl_exec($ch);
        return $result;
    }

}