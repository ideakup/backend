<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class League extends Model
{
    protected $table = 'leagues';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;

    public function league_province()
    {
        return $this->hasMany('App\LeaguesProvince', 'league_id', 'id');
    }


	//public.coordinators_leagues.coordinators_leagues_league_id_cba7f14a_fk_leagues_id
	

	//public.galleries_leagues.galleries_leagues_league_id_ab8949c2_fk_leagues_id
	public function galleries_leagues()
    {
        return $this->hasMany('App\GalleriesLeagues', 'league_id', 'id');
    }

	//public.league_galleries.league_galleries_league_id_6147a5e2_fk_leagues_id
	public function league_galleries()
    {
        return $this->hasMany('App\LeagueGalleries', 'league_id', 'id');
    }

	//public.leagues_provinces.leagues_provinces_league_id_7f804d45_fk_leagues_id
	

	//public.news_leagues.news_leagues_league_id_e34d6430_fk_leagues_id
	

	//public.nostalgia.nostalgia_league_id_5cf187f4_fk_leagues_id
	

	//public.panorama_types.panorama_types_league_id_4a82df6f_fk_leagues_id
	

	//public.penal_types.panorama_penaltype_league_id_57e6ec85_fk_leagues_id
	

	//public.penalties.panorama_penal_league_id_3ecffddd_fk_leagues_id
	

	//public.referees_leagues.referees_leagues_league_id_5d11b197_fk_leagues_id
	

	//public.seasons.seasons_league_id_b9480c20_fk_leagues_id
	

	public function season()
    {
        return $this->hasMany('App\Season', 'league_id', 'id')->orderBy('end_date' ,'desc');
    }

	public function seasonApi()
    {
        return $this->hasMany('App\Season', 'league_id', 'id')->where('polymorphic_ctype_id','!=','32')->where('active', true)->orderBy('end_date' ,'desc');
    }

    public function seasonApiPoint()
    {
        return $this->hasMany('App\Season', 'league_id', 'id')->where('polymorphic_ctype_id','=','34')->where('active', true)->orderBy('end_date' ,'desc');
    }
	

	//public.teams.teams_league_id_1c30af07_fk_leagues_id
	

	//public.users_leagues.users_leagues_league_id_2663b0bd_fk_leagues_id

    
    /*
    //***************
    //Get Date Format
    public function getCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    //***************
    //Set Date Format
    public function setCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function setUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }
    */
    
}