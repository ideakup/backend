<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class EliminationTreesTeam extends Model
{
    protected $table = 'elimination_trees_teams';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;

    //public.elimination_trees_teams.eliminaton_trees_tea_eliminationtree_id_9fb0f03f_fk_eliminato
    public function eliminaton_tree()
    {
        return $this->hasOne('App\EliminationTree', 'id', 'eliminationtree_id');
    }

	//public.elimination_trees_teams.eliminaton_trees_teams_team_id_9feed9e9_fk_teams_id
    public function team()
    {
        return $this->hasOne('App\Team', 'id', 'team_id');
    }
	
}