<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class DjangoContentType extends Model
{
    protected $table = 'django_content_type';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;

    //public.auth_permission.auth_permission_content_type_id_2f476e4b_fk_django_co

    
	//public.django_admin_log.django_admin_log_content_type_id_c4bce8eb_fk_django_co

	
	//public.galleries.galleries_polymorphic_ctype_id_7aca4da0_fk_django_co

	
	//public.panorama_types.panorama_types_polymorphic_ctype_id_b9331343_fk_django_co

	
	//public.penalties.panorama_penal_polymorphic_ctype_id_af1a93cb_fk_django_co

	
	//public.seasons.seasons_polymorphic_ctype_id_f388581b_fk_django_content_type_id

}