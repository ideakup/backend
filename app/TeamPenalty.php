<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class TeamPenalty extends Model
{
    protected $table = 'team_penalties';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;
    protected $primaryKey = 'penal_ptr_id';

	//public.team_penalties.panorama_penalteam_penal_ptr_id_be54120c_fk_panorama_penal_id
    public function penalty()
    {
        return $this->hasOne('App\Penalty', 'id', 'penal_ptr_id');
    }

	//public.team_penalties.panorama_penalteam_penal_type_id_db08f36d_fk_panorama_
	public function penalty_type()
    {
        return $this->hasOne('App\PenaltyTypes', 'id', 'penal_type_id');
    }

	//public.team_penalties.panorama_penalteam_team_id_0d09311c_fk_teams_id
    public function team()
    {
        return $this->hasOne('App\Team', 'id', 'team_id');
    }
	
}