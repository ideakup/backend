<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Transfer extends Model
{
    protected $table = 'transfers';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;

    //public.transfers.transfers_requested_player_id_5c5741cf_fk_players_id
    public function requested_player()
    {
        return $this->hasOne('App\Player', 'id', 'requested_player_id');
    }

	//public.transfers.transfers_requested_team_id_b0535e7d_fk_teams_id
    public function requested_team()
    {
        return $this->hasOne('App\Team', 'id', 'requested_team_id');
    }

	//public.transfers.transfers_requesting_captain_id_78bb8e63_fk_players_id
    public function requesting_captain()
    {
        return $this->hasOne('App\Player', 'id', 'requesting_captain_id');
    }

	//public.transfers.transfers_requesting_team_id_c012054f_fk_teams_id
    public function requesting_team()
    {
        return $this->hasOne('App\Team', 'id', 'requesting_team_id');
    }
	
	//public.transfers_seasons.transfers_seasons_transfer_id_8dc1c694_fk_transfers_id
    public function transfer_season()
    {
        return $this->belongsTo('App\TransferSeason', 'id', 'transfer_id');
    }

    
    /*
    //***************
    //Get Date Format
    public function getCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    //***************
    //Set Date Format
    public function setCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function setUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }
    */
    
}