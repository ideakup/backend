<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class MatchPlayer extends Model
{
    protected $table = 'match_player';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;

    // public.match_player.match_player_match_id_36a2729e_fk_matches_id
    public function match()
    {
        return $this->belongsTo('App\Match', 'match_id', 'id');
    }

	// public.match_player.match_player_player_id_76c08d05_fk_players_id
	public function player()
    {
        return $this->hasOne('App\Player', 'id', 'player_id');
    }

	// public.match_player.match_player_position_id_32cdbc82_fk_player_positions_id
	public function player_position()
    {
        return $this->hasOne('App\PlayerPosition', 'id', 'position_id');
    }

	// public.match_player.match_player_team_id_d4c17188_fk_teams_id
    public function team()
    {
        return $this->hasOne('App\Team', 'id', 'team_id');
    }

	// public.match_actions.match_actions_match_player_id_1535af0a_fk_match_player_id
    public function match_action()
    {
        return $this->hasMany('App\MatchAction', 'match_player_id', 'id');
    }

    public function match_action_goal()
    {
        return $this->hasMany('App\MatchAction', 'match_player_id', 'id')->where('type', 'goal');
    }

    public function match_action_red_card()
    {
        return $this->hasMany('App\MatchAction', 'match_player_id', 'id')->where('type', 'red_card');
    }

    public function match_action_yellow_card()
    {
        return $this->hasMany('App\MatchAction', 'match_player_id', 'id')->where('type', 'yellow_card');
    }

    public function match_player_panorama()
    {
        return $this->hasMany('App\MatchPanorama', 'match_player_id', 'id');
    }

    public function press_conferences_player()
    {
        return $this->hasOne('App\PressConferencesPlayers', 'matchplayer_id', 'id');
    }

    /*
    //***************
    //Get Date Format
    public function getCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    //***************
    //Set Date Format
    public function setCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function setUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }
    */
    
}