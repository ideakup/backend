<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Season extends Model
{
    protected $table = 'seasons';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;

    public function league()
    {
        return $this->belongsTo('App\League', 'league_id', 'id');
    }

    public function content_type()
    {
        return $this->hasOne('App\DjangoContentType', 'id', 'polymorphic_ctype_id');
    }

    public function championships()
    {
        return $this->hasOne('App\Championships', 'season_id', 'id');
    }

    public function league_elimination_baseseason()
    {
        return $this->hasOne('App\LeagueEliminationSeason', 'base_season_id', 'id');
    }

    public function league_eliminationseason()
    {
        return $this->hasOne('App\LeagueEliminationSeason', 'season_ptr_id', 'id');
    }
    public function league_eliminationseasonApi()
    {
        return $this->hasOne('App\LeagueEliminationSeason', 'base_season_id', 'id');
    }

    public function league_fixtureseason()
    {
        return $this->hasOne('App\LeagueFixtureSeason', 'season_ptr_id', 'id');
    }

	//public.league_photo_gallery.league_photo_gallery_season_id_35b24c86_fk_seasons_id

    public function league_pointseason()
    {
        return $this->hasOne('App\LeaguePointSeason', 'season_ptr_id', 'id');
    }

    public function match()
    {
        return $this->hasMany('App\Match', 'season_id', 'id');
    }

    public function matchOrdered()
    {
        return $this->hasMany('App\Match', 'season_id', 'id')->where('completed', true)->orderBy('date')->orderBy('time')->orderBy('id');
    }

    public function season_total_match()
    {
        return $this->hasOne('App\SeasonBeforeAfter', 'season_id', 'id')->where('status', 'before')->where('type', 'total_match');
    }

    public function season_to_tt()
    {
        return $this->hasOne('App\SeasonBeforeAfter', 'season_id', 'id')->where('status', 'before')->where('type', 'to_tt');
    }
    public function season_to_fkbt()
    {
        return $this->hasOne('App\SeasonBeforeAfter', 'season_id', 'id')->where('status', 'before')->where('type', 'to_fkbt');
    }
    public function season_to_o()
    {
        return $this->hasOne('App\SeasonBeforeAfter', 'season_id', 'id')->where('status', 'before')->where('type', 'to_o');
    }
    public function season_to_fkbo()
    {
        return $this->hasOne('App\SeasonBeforeAfter', 'season_id', 'id')->where('status', 'before')->where('type', 'to_fkbo');
    }
    public function season_to_cye()
    {
        return $this->hasOne('App\SeasonBeforeAfter', 'season_id', 'id')->where('status', 'before')->where('type', 'to_cye');
    }

    public function season_image_before()
    {
        return $this->hasMany('App\SeasonBeforeAfter', 'season_id', 'id')->where('status', 'before')->where('type', 'image');
    }

    public function season_image_now()
    {
        return $this->hasMany('App\SeasonBeforeAfter', 'season_id', 'id')->where('status', 'now')->where('type', 'image');
    }

    public function season_image_before_slide()
    {
        return $this->hasOne('App\SeasonBeforeAfter', 'season_id', 'id')->where('status', 'before_slide')->where('type', 'image');
    }

    public function season_image_after()
    {
        return $this->hasMany('App\SeasonBeforeAfter', 'season_id', 'id')->where('status', 'after')->where('type', 'image');
    }

    public function season_image_after_slide()
    {
        return $this->hasOne('App\SeasonBeforeAfter', 'season_id', 'id')->where('status', 'after_slide')->where('type', 'image');
    }

    public function season_temsilci_team()
    {
        return $this->hasOne('App\SeasonBeforeAfter', 'season_id', 'id')->where('status', 'before')->where('type', 'temsilci_team_id');
    }

    public function season_katilimci_teams()
    {
        return $this->hasOne('App\SeasonBeforeAfter', 'season_id', 'id')->where('status', 'before')->where('type', 'teams');
    }

    public function season_before_teaser_text()
    {
        return $this->hasOne('App\SeasonBeforeAfter', 'season_id', 'id')->where('status', 'before')->where('type', 'teaser_text');
    }

    public function season_before_teaser()
    {
        return $this->hasOne('App\SeasonBeforeAfter', 'season_id', 'id')->where('status', 'before')->where('type', 'teaser');
    }

    public function season_before_teaser_img()
    {
        return $this->hasOne('App\SeasonBeforeAfter', 'season_id', 'id')->where('status', 'before_teaser_img')->where('type', 'image');
    }

    public function season_now_teaser()
    {
        return $this->hasOne('App\SeasonBeforeAfter', 'season_id', 'id')->where('status', 'now')->where('type', 'teaser');
    }


    
    public function season_before_playlist()
    {
        return $this->hasOne('App\SeasonBeforeAfter', 'season_id', 'id')->where('status', 'before')->where('type', 'playlist');
    }



    public function season_player_mvp()
    {
        return $this->hasOne('App\SeasonBeforeAfter', 'season_id', 'id')->where('status', 'after')->where('type', 'player_mvp');
    }

    public function season_player_kog()
    {
        return $this->hasOne('App\SeasonBeforeAfter', 'season_id', 'id')->where('status', 'after')->where('type', 'player_kog');
    }

    public function season_player_beststriker()
    {
        return $this->hasOne('App\SeasonBeforeAfter', 'season_id', 'id')->where('status', 'after')->where('type', 'player_beststriker');
    }

    public function season_player_bestmiddle()
    {
        return $this->hasOne('App\SeasonBeforeAfter', 'season_id', 'id')->where('status', 'after')->where('type', 'player_bestmiddle');
    }

    public function season_player_bestdefance()
    {
        return $this->hasOne('App\SeasonBeforeAfter', 'season_id', 'id')->where('status', 'after')->where('type', 'player_bestdefance');
    }

    public function season_player_bestgoalkeeper()
    {
        return $this->hasOne('App\SeasonBeforeAfter', 'season_id', 'id')->where('status', 'after')->where('type', 'player_bestgoalkeeper');
    }



    public function season_info_text()
    {
        return $this->hasOne('App\SeasonBeforeAfter', 'season_id', 'id')->where('status', 'info')->where('type', 'info_text');
    }

    public function season_info_slide_photo()
    {
        return $this->hasOne('App\SeasonBeforeAfter', 'season_id', 'id')->where('status', 'info')->where('type', 'info_slide_photo');
    }

    public function season_info_img1()
    {
        return $this->hasOne('App\SeasonBeforeAfter', 'season_id', 'id')->where('status', 'info')->where('type', 'info_img1');
    }

    public function season_info_img2()
    {
        return $this->hasOne('App\SeasonBeforeAfter', 'season_id', 'id')->where('status', 'info')->where('type', 'info_img2');
    }




    public function season_after_teaser_text()
    {
        return $this->hasOne('App\SeasonBeforeAfter', 'season_id', 'id')->where('status', 'after')->where('type', 'teaser_text');
    }

    public function season_after_teaser()
    {
        return $this->hasOne('App\SeasonBeforeAfter', 'season_id', 'id')->where('status', 'after')->where('type', 'teaser');
    }

    public function season_after_teaser_img()
    {
        return $this->hasOne('App\SeasonBeforeAfter', 'season_id', 'id')->where('status', 'after_teaser_img')->where('type', 'image');
    }


    public function season_team2()
    {
        return $this->hasOne('App\SeasonBeforeAfter', 'season_id', 'id')->where('status', 'after')->where('type', 'team2_id');
    }
    public function season_team3()
    {
        return $this->hasOne('App\SeasonBeforeAfter', 'season_id', 'id')->where('status', 'after')->where('type', 'team3_id');
    }
    public function season_team4()
    {
        return $this->hasOne('App\SeasonBeforeAfter', 'season_id', 'id')->where('status', 'after')->where('type', 'team4_id');
    }

	//public.nostalgia.nostalgia_season_id_85e7c0c1_fk_seasons_id


	//public.panorama_types.panorama_types_season_id_ec51f1a7_fk_seasons_id


	//public.penal_types.panorama_penaltype_season_id_789b312b_fk_seasons_id


	//public.penalties.panorama_penal_season_id_471b75c6_fk_seasons_id
    public function penalties()
    {
        return $this->hasMany('App\Penalty', 'season_id', 'id');
    }

	//public.player_values.player_values_season_id_44f1a140_fk_seasons_id
    public function player_value()
    {
        return $this->hasMany('App\PlayerValue', 'season_id', 'id');
    }

	//public.team_playerteamhistory.team_playerteamhistory_season_id_ebf7dfff_fk_seasons_id
    public function team_playerteamhistory()
    {
        return $this->hasMany('App\TeamPlayerTeamHistory', 'season_id', 'id');
    }



   //public.team_playerteamhistory.team_playerteamhistory_season_id_ebf7dfff_fk_seasons_id
    public function team_playerteamhistory_goal()
    {
        return $this->hasMany('App\TeamPlayerTeamHistory', 'season_id', 'id')->orderBy('goal', 'desc');
    }
   //public.team_playerteamhistory.team_playerteamhistory_season_id_ebf7dfff_fk_seasons_id
    public function team_playerteamhistory_yellow_card()
    {
        return $this->hasMany('App\TeamPlayerTeamHistory', 'season_id', 'id')->orderBy('yellow_card', 'desc');
    }
   //public.team_playerteamhistory.team_playerteamhistory_season_id_ebf7dfff_fk_seasons_id
    public function team_playerteamhistory_red_card()
    {
        return $this->hasMany('App\TeamPlayerTeamHistory', 'season_id', 'id')->orderBy('red_card', 'desc');
    }
   //public.team_playerteamhistory.team_playerteamhistory_season_id_ebf7dfff_fk_seasons_id
    public function team_playerteamhistory_save()
    {
        return $this->hasMany('App\TeamPlayerTeamHistory', 'season_id', 'id')->orderBy('gk_save', 'desc');
    }




	//public.team_teamseason.team_teamseason_season_id_8269f079_fk_seasons_id
    public function team_teamseason()
    {
        return $this->hasMany('App\TeamTeamSeason', 'season_id', 'id');
    }

    public function team_teamseasonApi()
    {
        return $this->hasMany('App\TeamTeamSeason', 'season_id', 'id')->limit(10);
    }

	//public.transfers_seasons.transfers_seasons_season_id_b905c643_fk_seasons_id

    
    /*
    //***************
    //Get Date Format
    public function getCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    //***************
    //Set Date Format
    public function setCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function setUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }
    */
    
}