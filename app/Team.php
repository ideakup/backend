<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Team extends Model
{
    protected $table = 'teams';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;
	
    public function captain()
    {
        return $this->hasOne('App\Player', 'id', 'captain_id');
    }

	public function district()
    {
        return $this->hasOne('App\District', 'id', 'district_id');
    }

    public function province()
    {
        return $this->hasOne('App\Province', 'id', 'province_id');
    }
	
	public function league()
    {
        return $this->hasOne('App\League', 'id', 'league_id');
    }
	
    public function tactic()
    {
        return $this->belongsTo('App\Tactic', 'tactic_id', 'id');
    }

	public function championships()
    {
        return $this->hasMany('App\Championships', 'team_id', 'id');
    }

	//public.elimination_trees_teams.eliminaton_trees_teams_team_id_9feed9e9_fk_teams_id
	

	//public.eliminationtree_items.eliminationtree_items_team1_id_4a24d842_fk_teams_id
	

	//public.eliminationtree_items.eliminationtree_items_team2_id_44b0732c_fk_teams_id
	

	//public.groups_teams.groups_teams_team_id_26c0cb56_fk_teams_id
	

	//public.match_actions.match_actions_team_id_ce06aeb8_fk_teams_id
	

	//public.match_player.match_player_team_id_d4c17188_fk_teams_id
	

	//public.matches.matches_team1_id_7d18cbe0_fk_teams_id
	public function match1()
    {
        return $this->hasMany('App\Match', 'team1_id', 'id');
    }

	//public.matches.matches_team2_id_a6be4f57_fk_teams_id
    public function match2()
    {
        return $this->hasMany('App\Match', 'team2_id', 'id');
    }

	//public.nostalgia.nostalgia_team_id_7f293e86_fk_teams_id
	

	//public.player_penalties.player_penalties_team_id_ce9ba0a4_fk_teams_id
	public function player_penalties()
    {
        return $this->hasMany('App\PlayerPenalty', 'team_id', 'id');
    }

	//public.players.players_team_id_8b821f35_fk_teams_id
	public function player()
    {
        return $this->hasMany('App\Player', 'team_id', 'id')->orderBy('created_at', 'asc');
    }

	//public.press_conferences.press_conferences_team_id_4055566d_fk_teams_id
	public function team_press()
    {
        return $this->hasMany('App\PressConferences', 'team_id', 'id');
    }

	//public.reservation_offer.reservation_offer_team_id_f25fa80e_fk_teams_id
	

	//public.team_penalties.panorama_penalteam_team_id_0d09311c_fk_teams_id
	public function team_penalties()
    {
        return $this->hasMany('App\TeamPenalty', 'team_id', 'id');
    }

	//public.team_playerteamhistory.team_playerteamhistory_team_id_278c78c5_fk_teams_id
	public function team_playerteamhistory()
    {
        return $this->hasMany('App\TeamPlayerTeamHistory', 'team_id', 'id');
    }

	//public.team_surveys.team_surveys_team_id_f009c2ff_fk_teams_id
	public function team_survey()
    {
        return $this->hasOne('App\TeamSurvey', 'team_id', 'id');
    }

	//public.team_teamplayer.team_teamplayer_team_id_a2885497_fk_teams_id
	public function team_teamplayer()
    {
        return $this->hasMany('App\TeamTeamPlayer', 'team_id', 'id')->whereNull('released_date');
    }

	//public.team_teamseason.team_teamseason_team_id_aaafe647_fk_teams_id
	public function team_teamseason()
    {
        return $this->hasMany('App\TeamTeamSeason', 'team_id', 'id')->orderBy('id', 'desc');
    }

	//public.transfers.transfers_requested_team_id_b0535e7d_fk_teams_id
	

	//public.transfers.transfers_requesting_team_id_c012054f_fk_teams_id

    /*
    //***************
    //Get Date Format
    public function getCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    //***************
    //Set Date Format
    public function setCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function setUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }
    */
    
    
}