<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class PressConferences extends Model
{
    protected $table = 'press_conferences';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;

    //public.press_conferences.press_conferences_match_id_4ea38742_fk_matches_id  auto
    public function match()
    {
        return $this->hasOne('App\Match', 'id', 'match_id');
    }

    //public.press_conferences.press_conferences_team_id_4055566d_fk_teams_id auto
    public function team()
    {
        return $this->hasOne('App\Team', 'id', 'team_id');
    }
    

    //public.press_conferences_players.press_conferences_pl_pressconference_id_01354912_fk_press_con  normal
    public function press_conferences_players()
    {
        return $this->hasMany('App\PressConferencesPlayers', 'pressconference_id', 'id');
    }

    
    /*
    //***************
    //Get Date Format
    public function getCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    //***************
    //Set Date Format
    public function setCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function setUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }
    */
    
}