<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class PanoramaTypePrize extends Model
{
    protected $table = 'panorama_types_prize';
    protected $primaryKey = 'panoramatype_ptr_id';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;


	// public.panorama_types.panorama_types_league_id_4a82df6f_fk_leagues_id	auto
	

	// public.panorama_types.panorama_types_polymorphic_ctype_id_b9331343_fk_django_co	auto
	

	// public.panorama_types.panorama_types_season_id_ec51f1a7_fk_seasons_id




	// public.panorama_types_penal.panorama_types_penal_panoramatype_ptr_id_773b05bc_fk_panorama_	normal


	// public.panorama_types_prize.panorama_types_prize_panoramatype_ptr_id_e3c7e3c0_fk_panorama_	normal


    
    /*
    //***************
    //Get Date Format
    public function getCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    //***************
    //Set Date Format
    public function setCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function setUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }
    */
    
}
