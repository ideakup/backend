<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class EliminationTree extends Model
{
    protected $table = 'elimination_trees';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;

    //public.elimination_trees.eliminaton_trees_season_id_99ffb239_fk_league_el
    public function league_eliminationseason()
    {
        return $this->hasOne('App\LeagueEliminationSeason', 'season_ptr_id', 'season_id');
    }

	//public.elimination_trees_teams.eliminaton_trees_tea_eliminationtree_id_9fb0f03f_fk_eliminato
    public function eliminaton_tree_teams()
    {
        return $this->hasMany('App\EliminationTreesTeam', 'eliminationtree_id', 'id');
    }

	//public.eliminationtree_items.eliminationtree_items_tree_id_f13db7cf_fk_eliminaton_trees_id
    public function items()
    {
        return $this->hasMany('App\EliminationTreeItem', 'tree_id', 'id')->orderBy('level', 'desc');
    }

    public function itemstr()
    {
        return $this->hasMany('App\EliminationTreeItem', 'tree_id', 'id')->orderBy('level_title', 'asc')->orderBy('level', 'desc')->orderBy('line_group', 'asc');
    }

    public function items_distinct()
    {
        return $this->hasMany('App\EliminationTreeItem', 'tree_id', 'id')->select('level')->distinct();
    }

    
    /*
    //***************
    //Get Date Format
    public function getCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    //***************
    //Set Date Format
    public function setCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function setUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }
    */
    
}