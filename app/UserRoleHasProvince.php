<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class UserRoleHasProvince extends Model
{
    protected $table = 'userrole_has_province';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;

}