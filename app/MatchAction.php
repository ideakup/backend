<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class MatchAction extends Model
{
    protected $table = 'match_actions';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;

	//public.match_actions.match_actions_match_id_eb025b14_fk_matches_id
	public function match()
    {
        return $this->belongsTo('App\Match', 'match_id', 'id');
    }

	//public.match_actions.match_actions_match_player_id_1535af0a_fk_match_player_id
	public function match_player()
    {
        return $this->hasOne('App\MatchPlayer', 'id', 'match_player_id');
    }

	//public.match_actions.match_actions_team_id_ce06aeb8_fk_teams_id
	public function match_team()
    {
        return $this->hasOne('App\Team', 'id', 'team_id');
    }

    /*
    //***************
    //Get Date Format
    public function getCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    //***************
    //Set Date Format
    public function setCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function setUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }
    */
    
}