<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Nostalgia extends Model
{
    protected $table = 'nostalgia';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;

    // public.nostalgia.nostalgia_conference_id_0280bfcb_fk_conferences_id

    
	// public.nostalgia.nostalgia_league_id_5cf187f4_fk_leagues_id

	
	// public.nostalgia.nostalgia_province_id_a5096539_fk_provinces_id

	
	// public.nostalgia.nostalgia_season_id_85e7c0c1_fk_seasons_id

	
	// public.nostalgia.nostalgia_team_id_7f293e86_fk_teams_id


    
    /*
    //***************
    //Get Date Format
    public function getCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    //***************
    //Set Date Format
    public function setCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function setUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }
    */
    
}
