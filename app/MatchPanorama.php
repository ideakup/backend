<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class MatchPanorama extends Model
{
    protected $table = 'match_panorama';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;

	//public.match_panorama.match_panorama_match_id_c7e8b252_fk_matches_id	auto
	public function match()
    {
        return $this->belongsTo('App\Match', 'match_id', 'id');
    }

	//public.match_panorama.match_panorama_match_player_id_76dc9445_fk_match_player_id	auto
	public function match_player_panorama()
    {
        return $this->belongsTo('App\MatchPlayer', 'match_player_id', 'id');
    }

	//public.match_panorama.match_panorama_panorama_id_4fb26656_fk_panorama_	auto
	public function panorama_type()
    {
        return $this->hasOne('App\PanoramaType', 'id', 'panorama_id');
    }

    
    /*
    //***************
    //Get Date Format
    public function getCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    //***************
    //Set Date Format
    public function setCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function setUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }
    */
    
    
}