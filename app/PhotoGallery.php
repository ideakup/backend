<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class PhotoGallery extends Model
{
    protected $table = 'photo_galleries';
    protected $dateFormat = 'Y-m-d H:i:sO';
    protected $primaryKey = 'gallery_ptr_id';
    public $timestamps = false;

	//public.photo_galleries.photo_galleries_gallery_ptr_id_d5f0a74c_fk_galleries_id
	public function gallery()
    {
        return $this->hasOne('App\Galleries', 'id', 'gallery_ptr_id');
    }

	//public.league_photo_gallery.league_photo_gallery_gallery_id_a2900efe_fk_photo_gal


	//public.news.news_photo_gallery_id_4f15cd03_fk_photo_gal
	public function news()
    {
        return $this->belongsTo('App\News', 'gallery_ptr_id', 'photo_gallery_id');
    }

	//public.photo_gallery_items.photo_gallery_items_gallery_id_39023daf_fk_photo_gal
	public function items()
    {
        return $this->hasMany('App\PhotoGalleryItem', 'gallery_id', 'gallery_ptr_id');
    }

}