<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class RefereeLeague extends Model
{
    protected $table = 'referees_leagues';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;

}