<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Validator;

use Carbon\Carbon;
use App\User;

use App\Ground;
use App\GroundProperty;
use App\GroundsProperty;
use App\GroundImage;

class GroundController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard');
    }

    public function crud($id = null)
    {
        $ground = NULL;
        if(!is_null($id)){
            $ground = Ground::find($id);
        }
        return view('dashboard', array('model_data' => $ground));
    }
  
    public function save(Request $request)
    {

        $text = "";
        //dd($request->input());
        if ($request->crud == 'delete') {

            $model = Ground::find($request->id);
            foreach ($model->grounds_properties as $value) {
                $value->delete();
            }
            $model->delete();
            $text = 'Başarıyla Silindi...';

        }else{

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $model = new Ground();
            }else if($request->crud == 'edit'){
                $model = Ground::find($request->id);
            }

            $rules = array();
            $formConfig = config('forms.'.$request->segment);

            if(!is_null($formConfig)){
                foreach ($formConfig as $key => $value){
                    if(!empty(data_get($value, 'validation'))){
                        $rules[$key] = data_get($value, 'validation');
                    }
                }
            }
            
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
               return \Redirect::back()->withErrors($validator)->withInput();
            }
            
            $model->name = $request->name;
            $model->slug = str_slug($request->name, '-');
            $model->capacity = $request->capacity;
            $model->width = $request->width;
            $model->length = $request->length__s;
            $model->description = $request->description;
            $model->phone = $request->phone;
            $model->phone2 = $request->phone2;
            $model->email = $request->email;
            $model->website = $request->website;
            $model->province_id = $request->province_id;
            $model->district_id = $request->district_id;
            $model->address = $request->address;
            $model->latitude = $request->latitude;
            $model->longitude = $request->longitude;
            $model->save();
            
            /* EDIT */
            if($request->crud == 'edit'){
                foreach ($model->grounds_properties as $value_gp) {
                    $value_gp->delete();
                }
            }

            if(!empty($request->ground_property)){
                foreach ($request->ground_property as $value_gp) {
                    $gp = new GroundsProperty();
                    $gp->ground_id = $model->id;
                    $gp->property_id = $value_gp;
                    $gp->save();
                }
            }

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $text = 'Başarıyla Eklendi...';
            }else if($request->crud == 'edit'){
                $text = 'Başarıyla Güncellendi...';
            }
        }

        return redirect($request->segment)->with('message', array('text' => $text, 'status' => 'success'));
    }
    

    public function ground_properties()
    {
        return view('dashboard');
    }

    public function ground_properties_crud($id = null)
    {
        $groundProperty = NULL;
        if(!is_null($id)){
            $groundProperty = GroundProperty::find($id);
        }
        return view('dashboard', array('model_data' => $groundProperty));
    }

    public function ground_properties_save(Request $request)
    {

        $text = "";
        //dd($request->input());
        if ($request->crud == 'delete') {

            $model = GroundProperty::find($request->id);
            if(empty($model->grounds_properties)){
                $model->delete();
                $text = 'Başarıyla Silindi...';
            }else{
                $text = 'Bu özellik en az bir halı sahaya atandığı için silinemedi!';
                return \Redirect::back()->withInput()->with('message', array('text' => $text, 'status' => 'error'));
            }

        }else{

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $model = new GroundProperty();
            }else if($request->crud == 'edit'){
                $model = GroundProperty::find($request->id);
            }

            $rules = array();
            $formConfig = config('forms.'.$request->segment);
            if(!is_null($formConfig)){
                foreach ($formConfig as $key => $value){
                    if(!empty(data_get($value, 'validation'))){
                        $rules[$key] = data_get($value, 'validation');
                    }
                }
            }
            
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
               return \Redirect::back()->withErrors($validator)->withInput();
            }

            $model->name = $request->name;
            $model->save();

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $text = 'Başarıyla Eklendi...';
            }else if($request->crud == 'edit'){
                $text = 'Başarıyla Güncellendi...';
            }
        }

        return redirect($request->segment)->with('message', array('text' => $text, 'status' => 'success'));
    }

    public function ground_hour($id)
    {
        $ground = NULL;
        if(!is_null($id)){
            $ground = Ground::find($id);
        }
        return view('dashboard', array('model_data' => $ground));
    }

    public function groundImageDelete($id, $giid)
    {

        $model = GroundImage::find($giid);
        if(!empty($model)){
            $model->delete();
            $text = 'Başarıyla Silindi...';
            
            return redirect('grounds/edit/'.$id.'?t=tab_02')->with('message', array('text' => $text, 'status' => 'success'));
        }
    }

}
