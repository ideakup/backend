<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Validator;

use Carbon\Carbon;
use App\User;

use App\Season;


use App\News;
use App\NewsTypes;
use App\Tag;

use App\NewsLeagues;
use App\NewsTags;


class NewsController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        return view('dashboard');
    }

    public function news_crud($id = null) {
        $news = NULL;
        if(!is_null($id)){
            $news = News::find($id);
        }
        return view('dashboard', array('model_data' => $news));
    }

    public function news_save(Request $request) {

        //dd($request->input());
        $text = "";

        if ($request->crud == 'delete') {
            /*
            $model = Penalty::find($request->id);
            
            if($model->polymorphic_ctype_id == 44){
                $player_penalty = PlayerPenalty::find($model->id);
                $player_penalty->delete();
            }else if($model->polymorphic_ctype_id == 47){
                $team_penalty = TeamPenalty::find($model->id);
                $team_penalty->delete();
            }

            $model->delete();
            */
            $text = 'Başarıyla Silindi...';
        }else{

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $model = new News();
            }else if($request->crud == 'edit'){
                $model = News::find($request->id);
            }
            
            $rules = array();
            $formConfig = config('forms.'.$request->segment);

            if(!is_null($formConfig)){
                foreach ($formConfig as $key => $value){
                    if(!empty(data_get($value, 'validation'))){
                        $rules[$key] = data_get($value, 'validation');
                    }
                }
            }
            
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                //dd($validator->errors());
                return \Redirect::back()->withErrors($validator)->withInput();
            }



            $model->name = $request->name;
            $model->slug = str_slug($request->slug);
            $model->subtitle = $request->subtitle;
            $model->type_id = $request->type_id;
            $model->date = Carbon::parse($request->date)->format('Y-m-d H:i:s');
            $model->content = $request->content;
            $model->headline = (!empty($request->headline)) ? true : false;
            $model->active = (!empty($request->active)) ? true : false;
            $model->order = 0;
            $model->save();




            /* EDIT */
            if($request->crud == 'edit'){
                foreach ($model->news_leagues as $value_nl) {
                    $value_nl->delete();
                }
            }

            if(!empty($request->league_id)){
                foreach ($request->league_id as $value_nl) {
                    $nl = new NewsLeagues();
                    $nl->news_id = $model->id;
                    $nl->league_id = $value_nl;
                    $nl->save();
                }
            }

            /* EDIT */
            if($request->crud == 'edit'){
                foreach ($model->news_tags as $value_nt) {
                    $value_nt->delete();
                }
            }

            if(!empty($request->tag_id)){
                foreach ($request->tag_id as $value_nt) {
                    $nl = new NewsTags();
                    $nl->news_id = $model->id;
                    $nl->tag_id = $value_nt;
                    $nl->save();
                }
            }

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $text = 'Başarıyla Eklendi...';
            }else if($request->crud == 'edit'){
                $text = 'Başarıyla Güncellendi...';
            }
        }

        return redirect('news')->with('message', array('text' => $text, 'status' => 'success'));
    }

    public function newImageDelete($id)
    {
        $new = News::find($id);

        if(!empty($new->photo_gallery->items)){
            $new->photo_gallery->items->first()->delete();
             $text = 'Başarıyla Silindi...';
            
            return redirect('news/edit/'.$id.'?t=tab_02')->with('message', array('text' => $text, 'status' => 'success'));
        }

    }


    public function news_types() {
        return view('dashboard');
    }

    public function news_type_crud($id = null) {
        $news_types = NULL;
        if(!is_null($id)){
            $news_types = NewsTypes::find($id);
        }
        return view('dashboard', array('model_data' => $news_types));
    }

    public function news_type_save(Request $request) {
        //dd($request->input());
        $text = "";
        if ($request->crud == 'delete') {
            
            $model = NewsTypes::find($request->id);

            $cnt = 0;
            if($model->to_who == 'player'){
                $cnt = PlayerPenalty::where('penal_type_id', $model->id)->count();
            }else if($model->to_who == 'team'){
                $cnt = TeamPenalty::where('penal_type_id', $model->id)->count();
            }

            if($cnt == 0){
                $model->delete();
                $text = 'Başarıyla Silindi...';
            }else{
                $text = 'Bu cezanın uygulandığı en az bir kayıp bulunduğu için silinemedi!';
                return \Redirect::back()->withInput()->with('message', array('text' => $text, 'status' => 'error'));
            }
            

        }else{

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $model = new NewsTypes();
            }else if($request->crud == 'edit'){
                $model = NewsTypes::find($request->id);
            }

            $rules = array();
            $formConfig = config('forms.'.$request->segment);

            if(!is_null($formConfig)){
                foreach ($formConfig as $key => $value){
                    if(!empty(data_get($value, 'validation'))){
                        $rules[$key] = data_get($value, 'validation');
                    }
                }
            }
            
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
               return \Redirect::back()->withErrors($validator)->withInput();
            }

            $model->name = $request->name;
            $model->order = $request->order;
            $model->active = (!empty($request->active)) ? true : false;
            $model->save();

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $text = 'Başarıyla Eklendi...';
            }else if($request->crud == 'edit'){
                $text = 'Başarıyla Güncellendi...';
            }
        }

        return redirect($request->segment)->with('message', array('text' => $text, 'status' => 'success'));
    }



    public function tags() {
        return view('dashboard');
    }

    public function tags_crud($id = null) {
        $news_types = NULL;
        if(!is_null($id)){
            $news_types = Tag::find($id);
        }
        return view('dashboard', array('model_data' => $news_types));
    }

    public function tags_save(Request $request) {
        //dd($request->input());
        $text = "";
        if ($request->crud == 'delete') {
            
            $model = Tag::find($request->id);
            $cnt = $model->news_tags->count();

            if($cnt == 0){
                $model->delete();
                $text = 'Başarıyla Silindi...';
            }else{
                $text = 'Bu etiketin kullanıldığı en az bir kayıt bulunduğu için silinemedi!';
                return \Redirect::back()->withInput()->with('message', array('text' => $text, 'status' => 'error'));
            }
            

        }else{

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $model = new Tag();
            }else if($request->crud == 'edit'){
                $model = Tag::find($request->id);
            }

            $rules = array();
            $formConfig = config('forms.'.$request->segment);

            if(!is_null($formConfig)){
                foreach ($formConfig as $key => $value){
                    if(!empty(data_get($value, 'validation'))){
                        $rules[$key] = data_get($value, 'validation');
                    }
                }
            }
            
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
               return \Redirect::back()->withErrors($validator)->withInput();
            }

            $model->name = $request->name;
            $model->save();

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $text = 'Başarıyla Eklendi...';
            }else if($request->crud == 'edit'){
                $text = 'Başarıyla Güncellendi...';
            }
        }

        return redirect($request->segment)->with('message', array('text' => $text, 'status' => 'success'));
    }




}
