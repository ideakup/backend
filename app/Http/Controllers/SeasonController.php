<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Validator;

use Carbon\Carbon;
use App\User;

use App\Season;
use App\SeasonBeforeAfter;
use App\LeaguePointSeason;
use App\LeagueFixtureSeason;
use App\LeagueEliminationSeason;

use App\TeamTeamSeasonAddPointLog;

use App\Championships;
use App\Team;

class SeasonController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
    	return view('dashboard');
    }

    public function addBonus() {
        return view('dashboard');
    }

    public function addBonusSave(Request $request) {

        $seasonIds = $request->input('seasonid');
        $recordDate = Carbon::now()->subMonth();

        if(!empty($seasonIds)){

            foreach ($seasonIds as $seasonId) {
                
                $season = Season::find($seasonId);

                //dump($season);
                //dump($recordDate->startOfMonth()->format('Y-m-d').' - '.$recordDate->endOfMonth()->format('Y-m-d'));

                if(!empty($season->league_pointseason)){

                    $bonus_match_count = null;
                    $bonus_point = null;

                    if($season->league_pointseason->has_bonus){
                        $bonus_match_count = $season->league_pointseason->bonus_match_count;
                        $bonus_point = $season->league_pointseason->bonus_point;

                        //dump('Gerçek - bonus_match_count: '.$bonus_match_count);
                        //dump('Gerçek - bonus_point: '.$bonus_point);
                    }
                    
                    //Test için manuple satırı...
                    //$bonus_match_count = 1;
                    //$bonus_point = 10;
                    //dump('Temp - bonus_match_count: '.$bonus_match_count);
                    //dump('Temp - bonus_point: '.$bonus_point);
                    
                    foreach ($season->team_teamseason->where('match_total', '>=', $bonus_match_count) as $teamseason) {
                        $team = $teamseason->team;
                        //dump('Takım: '.$team->name);
                        //dump($teamseason);

                        $matches1 = collect($team->match1)->where('completed', true)->where('date', '>=', $recordDate->startOfMonth()->format('Y-m-d'))->where('date', '<=', $recordDate->endOfMonth()->format('Y-m-d'));
                        $matches2 = collect($team->match2)->where('completed', true)->where('date', '>=', $recordDate->startOfMonth()->format('Y-m-d'))->where('date', '<=', $recordDate->endOfMonth()->format('Y-m-d'));
                        $matches = $matches1->merge($matches2)->sortBy('completed_at');

                        if($matches->count() >= $bonus_match_count){
                            //dump('Maç Sayısı: '.$matches->count());
                            //dump('Puan Ekle: '.$bonus_point);

                            //dump(json_decode($teamseason->teamseason_add_point_log));
                            $dahaOnceEklenmis = false;
                            foreach ($teamseason->teamseason_add_point_log as $log) {

                                if($log->point_type == 'Monthly Bonus' && json_decode($log->meta)->year == $recordDate->format('Y') && json_decode($log->meta)->month == $recordDate->format('n')){
                                    //dump('daha önce eklenmiş');
                                    $dahaOnceEklenmis = true;
                                }

                            }

                            
                            if(!$dahaOnceEklenmis){
                                //dump('ekleeee');

                                $newPoint = $teamseason->point + $bonus_point;

                                $teamseason->point = $newPoint;
                                $teamseason->save();

                                $pointLog = new TeamTeamSeasonAddPointLog();
                                $pointLog->point_add = $bonus_point;
                                $pointLog->point_new = $newPoint;
                                $pointLog->teamseason_id = $teamseason->id;
                                $pointLog->user_id = Auth::user()->id;
                                $pointLog->meta = json_encode(array('year' => $recordDate->format('Y'), 'month' => $recordDate->format('n')));
                                $pointLog->point_type = 'Monthly Bonus';
                                $pointLog->save();
                            }

                        }

                    }

                }

            }

            $text = 'Bonus Puanlar Başarıyla Eklendi...';
            return redirect('seasons/addbonus')->with('message', array('text' => $text, 'status' => 'success'));

        }else{

            $text = 'Bir sezon seçmelisiniz...';
            return redirect('seasons/addbonus')->with('message', array('text' => $text, 'status' => 'warning'));

        }
    }

    public function crudp($id = null) {
        $season = NULL;
        if(!is_null($id)){
            $season = Season::find($id);
        }
        return view('dashboard', array('model_data' => $season));
    }

    public function savep(Request $request) {

        $text = "";
        
        if ($request->crud == 'delete') {

            $model = Season::find($request->id);
            if(!empty($model->league_pointseason)){
                $model->league_pointseason->delete();
            }
            if(!empty($model->player_value)){
                $model->player_value->delete();
            }
            $model->delete();
            $text = 'Başarıyla Silindi...';

        }else{

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $model = new Season();
            }else if($request->crud == 'edit'){
                $model = Season::find($request->id);
            }

            $rules = array();
            $formConfig = config('forms.'.$request->segment);
            
            if(!is_null($formConfig)){
                foreach ($formConfig as $key => $value){
                    if(!empty(data_get($value, 'validation'))){
                        $rules[$key] = data_get($value, 'validation');
                    }
                }
            }
            
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                return \Redirect::back()->withErrors($validator)->withInput();
            }
            
            $model->polymorphic_ctype_id = $request->polymorphic_ctype_id;
            $model->closed = ($request->closed == 'false') ? false : true;
            $model->name = $request->name;
            $model->league_id = $request->league_id;
            $model->year = $request->year;
            $model->start_date = Carbon::parse($request->start_date);
            $model->end_date = Carbon::parse($request->end_date);
            $model->transfer_start_date = Carbon::parse($request->transfer_start_date);
            $model->transfer_end_date = Carbon::parse($request->transfer_end_date);
            $model->allowed_transfer_count = $request->allowed_transfer_count;
            $model->locking_match_count = $request->locking_match_count;
            $model->active = (!empty($request->active)) ? true : false;
            $model->save();

            if ($request->crud == 'add') {

                $subModel = new LeaguePointSeason();
                $subModel->season_ptr_id = $model->id;

            }else if($request->crud == 'edit'){

                $subModel = $model->league_pointseason;
                if(empty($subModel)){
                    $subModel = new LeaguePointSeason();
                    $subModel->season_ptr_id = $model->id;
                }

            }

            $subModel->min_team_point = $request->min_team_point;
            $subModel->has_bonus = (!empty($request->has_bonus)) ? true : false;
            $subModel->bonus_match_count = $request->bonus_match_count;
            $subModel->bonus_point = $request->bonus_point;
            $subModel->save();
            
            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $text = 'Başarıyla Eklendi...';
            }else if($request->crud == 'edit'){
                $text = 'Başarıyla Güncellendi...';
            }
        }

        return redirect('seasons')->with('message', array('text' => $text, 'status' => 'success'));
    }


    public function crudf($id = null) {
        $season = NULL;
        if(!is_null($id)){
            $season = Season::find($id);
        }
        return view('dashboard', array('model_data' => $season));
    }

    public function savef(Request $request) {

        $text = "";
        //dd($request->input());
        if ($request->crud == 'delete') {

            $model = Season::find($request->id);

            if(!empty($model->team_teamseason)){
                foreach ($model->team_teamseason as $teamseason) {
                    $teamseason->delete();
                }
            }

            if(!empty($model->league_fixtureseason)){
                foreach ($model->league_fixtureseason->groups as $group) {
                    foreach ($group->group_teams as $group_team) {
                        $group_team->delete();
                    }
                    $group->delete();
                }
                $model->league_fixtureseason->delete();
            }
            $model->delete();
            $text = 'Başarıyla Silindi...';

        }else{

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $model = new Season();
            }else if($request->crud == 'edit'){
                $model = Season::find($request->id);
            }

            $rules = array();
            $formConfig = config('forms.'.$request->segment);
            if(!is_null($formConfig)){
                foreach ($formConfig as $key => $value){
                    if(!empty(data_get($value, 'validation'))){
                        $rules[$key] = data_get($value, 'validation');
                    }
                }
            }
            
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
               return \Redirect::back()->withErrors($validator)->withInput();
            }

            $model->polymorphic_ctype_id = $request->polymorphic_ctype_id;
            $model->closed = ($request->closed == 'false') ? false : true;
            $model->name = $request->name;
            $model->league_id = $request->league_id;
            $model->year = $request->year;
            $model->start_date = Carbon::parse($request->start_date);
            $model->end_date = Carbon::parse($request->end_date);
            $model->transfer_start_date = Carbon::parse($request->transfer_start_date);
            $model->transfer_end_date = Carbon::parse($request->transfer_end_date);
            $model->allowed_transfer_count = $request->allowed_transfer_count;
            $model->locking_match_count = $request->locking_match_count;
            $model->active = (!empty($request->active)) ? true : false;
            $model->save();

            if ($request->crud == 'add') {
                $subModel = new LeagueFixtureSeason();
                $subModel->season_ptr_id = $model->id;
            }else if($request->crud == 'edit'){
                $subModel = $model->league_fixtureseason;
            }

            $subModel->min_team_point = 0;
            $subModel->save();
            
            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $text = 'Başarıyla Eklendi...';
            }else if($request->crud == 'edit'){
                $text = 'Başarıyla Güncellendi...';
            }
        }

        return redirect('seasons')->with('message', array('text' => $text, 'status' => 'success'));
    }


    public function crude($id = null) {
        $season = NULL;
        if(!is_null($id)){
            $season = Season::find($id);
        }
        return view('dashboard', array('model_data' => $season));
    }

    public function savee(Request $request) {

        $text = "";
        // dd($request->input());
        if ($request->crud == 'delete') {

            $model = Season::find($request->id);
            //$model->league_fixtureseason->delete();
            $model->delete();
            $text = 'Başarıyla Silindi...';

        }else{

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $model = new Season();
            }else if($request->crud == 'edit'){
                $model = Season::find($request->id);
            }

            $rules = array();
            $formConfig = config('forms.'.$request->segment);
            if(!is_null($formConfig)){
                foreach ($formConfig as $key => $value){
                    if(!empty(data_get($value, 'validation'))){
                        $rules[$key] = data_get($value, 'validation');
                    }
                }
            }
            
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
               return \Redirect::back()->withErrors($validator)->withInput();
            }



            if ($request->crud == 'add') {

                $contModel = LeagueEliminationSeason::where('base_season_id', $request->season_id)->count();
                if($contModel != 0){
                    return \Redirect::back()->with('message', array('text' => 'Seçilen Ana Sezon Bir Elemeli Sezona Sahiptir...', 'status' => 'error'))->withInput();
                }

            }else if($request->crud == 'edit'){

                $contModel = LeagueEliminationSeason::where('season_ptr_id', "!=", $request->id)->where('base_season_id', $request->season_id)->count();
                if($contModel != 0){
                    return \Redirect::back()->with('message', array('text' => 'Seçilen Ana Sezon Bir Elemeli Sezona Sahiptir...', 'status' => 'error'))->withInput();
                }
            }
            


            $model->polymorphic_ctype_id = $request->polymorphic_ctype_id;
            $model->closed = ($request->closed == 'false') ? false : true;
            $model->name = $request->name;
            $model->league_id = $request->league_id;
            $model->year = $request->year;
            $model->start_date = Carbon::parse($request->start_date);
            $model->end_date = Carbon::parse($request->end_date);
            $model->transfer_start_date = Carbon::parse($request->transfer_start_date);
            $model->transfer_end_date = Carbon::parse($request->transfer_end_date);
            $model->allowed_transfer_count = (!empty($request->allowed_transfer_count)) ? $request->allowed_transfer_count : 0;
            $model->locking_match_count = (!empty($request->locking_match_count)) ? $request->locking_match_count : 0;;
            $model->active = (!empty($request->active)) ? true : false;
            $model->save();

            if ($request->crud == 'add') {
                $subModel = new LeagueEliminationSeason();
                $subModel->season_ptr_id = $model->id;
                $subModel->base_season_id = $request->season_id;
            }else if($request->crud == 'edit'){
                $subModel = $model->league_eliminationseason;
                $subModel->season_ptr_id = $model->id;
                $subModel->base_season_id = $request->season_id;
            }

            $subModel->save();
            
            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $text = 'Başarıyla Eklendi...';
            }else if($request->crud == 'edit'){
                $text = 'Başarıyla Güncellendi...';
            }
        }

        return redirect('seasons')->with('message', array('text' => $text, 'status' => 'success'));
    }

    public function crudSeasonSettings($id = null) {
        $season = NULL;
        if(!is_null($id)){
            $season = Season::find($id);
        }
        return view('dashboard', array('model_data' => $season));
    }

    public function saveSeasonSettings(Request $request) {
        
        //dd($request->input());

        $season = Season::find($request->input('season_id'));
        $team = Team::find($request->input('team_id'));

        $championships = Championships::where('season_id', $season->id)->first();

        $rules = array();
        $formConfig = config('forms.'.$request->segment);
        if(!is_null($formConfig)){
            foreach ($formConfig as $key => $value){
                if(!empty(data_get($value, 'validation'))){
                    $rules[$key] = data_get($value, 'validation');
                }
            }
        }
        
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
           return \Redirect::back()->withErrors($validator)->withInput();
        }


        $total_match = SeasonBeforeAfter::where('type', 'total_match')->where('season_id', $request->input('season_id'))->where('status', 'before')->first();
        if(empty($total_match)){
            $total_match = new SeasonBeforeAfter();
        }
        $total_match->season_id = $request->input('season_id');
        $total_match->type = 'total_match';
        $total_match->status = 'before';
        $total_match->data = (!empty($request->input('total_match'))) ? $request->input('total_match') : '';
        $total_match->save();









        $temsilci_team_id = SeasonBeforeAfter::where('type', 'temsilci_team_id')->where('season_id', $request->input('season_id'))->where('status', 'before')->first();
        if(empty($temsilci_team_id)){
            $temsilci_team_id = new SeasonBeforeAfter();
        }
        $temsilci_team_id->season_id = $request->input('season_id');
        $temsilci_team_id->type = 'temsilci_team_id';
        $temsilci_team_id->status = 'before';
        $temsilci_team_id->data = (!empty($request->input('temsilci_team_id'))) ? $request->input('temsilci_team_id') : '';
        $temsilci_team_id->save();

        $to_tt = SeasonBeforeAfter::where('type', 'to_tt')->where('season_id', $request->input('season_id'))->where('status', 'before')->first();
        if(empty($to_tt)){
            $to_tt = new SeasonBeforeAfter();
        }
        $to_tt->season_id = $request->input('season_id');
        $to_tt->type = 'to_tt';
        $to_tt->status = 'before';
        $to_tt->data = (!empty($request->input('to_tt'))) ? $request->input('to_tt') : '';
        $to_tt->save();

        $to_fkbt = SeasonBeforeAfter::where('type', 'to_fkbt')->where('season_id', $request->input('season_id'))->where('status', 'before')->first();
        if(empty($to_fkbt)){
            $to_fkbt = new SeasonBeforeAfter();
        }
        $to_fkbt->season_id = $request->input('season_id');
        $to_fkbt->type = 'to_fkbt';
        $to_fkbt->status = 'before';
        $to_fkbt->data = (!empty($request->input('to_fkbt'))) ? $request->input('to_fkbt') : '';
        $to_fkbt->save();

        $to_o = SeasonBeforeAfter::where('type', 'to_o')->where('season_id', $request->input('season_id'))->where('status', 'before')->first();
        if(empty($to_o)){
            $to_o = new SeasonBeforeAfter();
        }
        $to_o->season_id = $request->input('season_id');
        $to_o->type = 'to_o';
        $to_o->status = 'before';
        $to_o->data = (!empty($request->input('to_o'))) ? $request->input('to_o') : '';
        $to_o->save();

        $to_fkbo = SeasonBeforeAfter::where('type', 'to_fkbo')->where('season_id', $request->input('season_id'))->where('status', 'before')->first();
        if(empty($to_fkbo)){
            $to_fkbo = new SeasonBeforeAfter();
        }
        $to_fkbo->season_id = $request->input('season_id');
        $to_fkbo->type = 'to_fkbo';
        $to_fkbo->status = 'before';
        $to_fkbo->data = (!empty($request->input('to_fkbo'))) ? $request->input('to_fkbo') : '';
        $to_fkbo->save();

        $to_cye = SeasonBeforeAfter::where('type', 'to_cye')->where('season_id', $request->input('season_id'))->where('status', 'before')->first();
        if(empty($to_cye)){
            $to_cye = new SeasonBeforeAfter();
        }
        $to_cye->season_id = $request->input('season_id');
        $to_cye->type = 'to_cye';
        $to_cye->status = 'before';
        $to_cye->data = (!empty($request->input('to_cye'))) ? $request->input('to_cye') : '';
        $to_cye->save();




        $info_text = SeasonBeforeAfter::where('type', 'info_text')->where('season_id', $request->input('season_id'))->where('status', 'info')->first();
        if(empty($info_text)){
            $info_text = new SeasonBeforeAfter();
        }
        $info_text->season_id = $request->input('season_id');
        $info_text->type = 'info_text';
        $info_text->status = 'info';
        $info_text->data = (!empty($request->input('info_text'))) ? $request->input('info_text') : '';
        $info_text->save();



        
        $before_teaser_text = SeasonBeforeAfter::where('type', 'teaser_text')->where('season_id', $request->input('season_id'))->where('status', 'before')->first();
        if(empty($before_teaser_text)){
            $before_teaser_text = new SeasonBeforeAfter();
        }
        $before_teaser_text->season_id = $request->input('season_id');
        $before_teaser_text->type = 'teaser_text';
        $before_teaser_text->status = 'before';
        $before_teaser_text->data = (!empty($request->input('before_teaser_text'))) ? $request->input('before_teaser_text') : '';
        $before_teaser_text->save();

        
        $before_teaser = SeasonBeforeAfter::where('type', 'teaser')->where('season_id', $request->input('season_id'))->where('status', 'before')->first();
        if(empty($before_teaser)){
            $before_teaser = new SeasonBeforeAfter();
        }
        $before_teaser->season_id = $request->input('season_id');
        $before_teaser->type = 'teaser';
        $before_teaser->status = 'before';
        $before_teaser->data = (!empty($request->input('before_teaser'))) ? $request->input('before_teaser') : '';
        $before_teaser->save();

        
        $now_teaser = SeasonBeforeAfter::where('type', 'teaser')->where('season_id', $request->input('season_id'))->where('status', 'now')->first();
        if(empty($now_teaser)){
            $now_teaser = new SeasonBeforeAfter();
        }
        $now_teaser->season_id = $request->input('season_id');
        $now_teaser->type = 'teaser';
        $now_teaser->status = 'now';
        $now_teaser->data = (!empty($request->input('now_teaser'))) ? $request->input('now_teaser') : '';
        $now_teaser->save();
        

        $before_playlist = SeasonBeforeAfter::where('type', 'playlist')->where('season_id', $request->input('season_id'))->where('status', 'before')->first();
        if(empty($before_playlist)){
            $before_playlist = new SeasonBeforeAfter();
        }
        $before_playlist->season_id = $request->input('season_id');
        $before_playlist->type = 'playlist';
        $before_playlist->status = 'before';
        $before_playlist->data = (!empty($request->input('before_playlist'))) ? $request->input('before_playlist') : '';
        $before_playlist->save();
    
       
        $teams = SeasonBeforeAfter::where('type', 'teams')->where('season_id', $request->input('season_id'))->where('status', 'before')->first();
        if(empty($teams)){
            $teams = new SeasonBeforeAfter();
        }
        $teams->season_id = $request->input('season_id');
        $teams->type = 'teams';
        $teams->status = 'before';
        $teams->data = (!empty($request->input('teams'))) ? json_encode($request->input('teams')) : '';
        $teams->save();
        

        if (empty($championships)) {
            $championships = new Championships();
        }

        if(!empty($team)) {
            $championships->season_id = $season->id;
            $championships->year = $season->year;
            $championships->team_id = $team->id;
            $championships->point = 0;
            $championships->save();
        } else {
            $championships->delete();
        }

        $player_mvp = SeasonBeforeAfter::where('type', 'player_mvp')->where('season_id', $request->input('season_id'))->where('status', 'after')->first();
        if(empty($player_mvp)){
            $player_mvp = new SeasonBeforeAfter();
        }
        $player_mvp->season_id = $request->input('season_id');
        $player_mvp->type = 'player_mvp';
        $player_mvp->status = 'after';
        $player_mvp->data = (!empty($request->input('player_mvp'))) ? $request->input('player_mvp') : '';
        $player_mvp->save();


        $player_kog = SeasonBeforeAfter::where('type', 'player_kog')->where('season_id', $request->input('season_id'))->where('status', 'after')->first();
        if(empty($player_kog)){
            $player_kog = new SeasonBeforeAfter();
        }
        $player_kog->season_id = $request->input('season_id');
        $player_kog->type = 'player_kog';
        $player_kog->status = 'after';
        $player_kog->data = (!empty($request->input('player_kog'))) ? $request->input('player_kog') : '';
        $player_kog->save();


        $player_beststriker = SeasonBeforeAfter::where('type', 'player_beststriker')->where('season_id', $request->input('season_id'))->where('status', 'after')->first();
        if(empty($player_beststriker)){
            $player_beststriker = new SeasonBeforeAfter();
        }
        $player_beststriker->season_id = $request->input('season_id');
        $player_beststriker->type = 'player_beststriker';
        $player_beststriker->status = 'after';
        $player_beststriker->data = (!empty($request->input('player_beststriker'))) ? $request->input('player_beststriker') : '';
        $player_beststriker->save();


        $player_bestmiddle = SeasonBeforeAfter::where('type', 'player_bestmiddle')->where('season_id', $request->input('season_id'))->where('status', 'after')->first();
        if(empty($player_bestmiddle)){
            $player_bestmiddle = new SeasonBeforeAfter();
        }
        $player_bestmiddle->season_id = $request->input('season_id');
        $player_bestmiddle->type = 'player_bestmiddle';
        $player_bestmiddle->status = 'after';
        $player_bestmiddle->data = (!empty($request->input('player_bestmiddle'))) ? $request->input('player_bestmiddle') : '';
        $player_bestmiddle->save();


        $player_bestdefance = SeasonBeforeAfter::where('type', 'player_bestdefance')->where('season_id', $request->input('season_id'))->where('status', 'after')->first();
        if(empty($player_bestdefance)){
            $player_bestdefance = new SeasonBeforeAfter();
        }
        $player_bestdefance->season_id = $request->input('season_id');
        $player_bestdefance->type = 'player_bestdefance';
        $player_bestdefance->status = 'after';
        $player_bestdefance->data = (!empty($request->input('player_bestdefance'))) ? $request->input('player_bestdefance') : '';
        $player_bestdefance->save();


        $player_bestgoalkeeper = SeasonBeforeAfter::where('type', 'player_bestgoalkeeper')->where('season_id', $request->input('season_id'))->where('status', 'after')->first();
        if(empty($player_bestgoalkeeper)){
            $player_bestgoalkeeper = new SeasonBeforeAfter();
        }
        $player_bestgoalkeeper->season_id = $request->input('season_id');
        $player_bestgoalkeeper->type = 'player_bestgoalkeeper';
        $player_bestgoalkeeper->status = 'after';
        $player_bestgoalkeeper->data = (!empty($request->input('player_bestgoalkeeper'))) ? $request->input('player_bestgoalkeeper') : '';
        $player_bestgoalkeeper->save();

        /********************************************************************************/

        $after_teaser_text = SeasonBeforeAfter::where('type', 'teaser_text')->where('season_id', $request->input('season_id'))->where('status', 'after')->first();
        if(empty($after_teaser_text)){
            $after_teaser_text = new SeasonBeforeAfter();
        }
        $after_teaser_text->season_id = $request->input('season_id');
        $after_teaser_text->type = 'teaser_text';
        $after_teaser_text->status = 'after';
        $after_teaser_text->data = (!empty($request->input('after_teaser_text'))) ? $request->input('after_teaser_text') : '';
        $after_teaser_text->save();


        $after_teaser = SeasonBeforeAfter::where('type', 'teaser')->where('season_id', $request->input('season_id'))->where('status', 'after')->first();
        if(empty($after_teaser)){
            $after_teaser = new SeasonBeforeAfter();
        }
        $after_teaser->season_id = $request->input('season_id');
        $after_teaser->type = 'teaser';
        $after_teaser->status = 'after';
        $after_teaser->data = (!empty($request->input('after_teaser'))) ? $request->input('after_teaser') : '';
        $after_teaser->save();


        $season_team2 = SeasonBeforeAfter::where('type', 'team2_id')->where('season_id', $request->input('season_id'))->where('status', 'after')->first();
        if(empty($season_team2)){
            $season_team2 = new SeasonBeforeAfter();
        }
        $season_team2->season_id = $request->input('season_id');
        $season_team2->type = 'team2_id';
        $season_team2->status = 'after';
        $season_team2->data = (!empty($request->input('team2_id'))) ? $request->input('team2_id') : '';
        $season_team2->save();

        $season_team3 = SeasonBeforeAfter::where('type', 'team3_id')->where('season_id', $request->input('season_id'))->where('status', 'after')->first();
        if(empty($season_team3)){
            $season_team3 = new SeasonBeforeAfter();
        }
        $season_team3->season_id = $request->input('season_id');
        $season_team3->type = 'team3_id';
        $season_team3->status = 'after';
        $season_team3->data = (!empty($request->input('team3_id'))) ? $request->input('team3_id') : '';
        $season_team3->save();

        $season_team4 = SeasonBeforeAfter::where('type', 'team4_id')->where('season_id', $request->input('season_id'))->where('status', 'after')->first();
        if(empty($season_team4)){
            $season_team4 = new SeasonBeforeAfter();
        }
        $season_team4->season_id = $request->input('season_id');
        $season_team4->type = 'team4_id';
        $season_team4->status = 'after';
        $season_team4->data = (!empty($request->input('team4_id'))) ? $request->input('team4_id') : '';
        $season_team4->save();


        $text = 'Başarıyla Güncellendi...';
        return redirect('seasons')->with('message', array('text' => $text, 'status' => 'success'));

    }

    public function seasonImageDescSave(Request $request) {

        $status = $request->input('season_status');
        if(!empty($request->input('season_'.$status.'_image'))){
            foreach ($request->input('season_'.$status.'_image') as $key => $value) {
                $season_image = SeasonBeforeAfter::find($key);
                $season_image->description = $value;
                $season_image->save();
            }
        }

        if($status == 'after'){
            $status = 'tab_03';
        }elseif($status == 'before'){
            $status = 'tab_02';
        }else{
            $status = 'tab_01';
        }
        
        $text = 'Başarıyla Kaydedildi...';
        return redirect('seasonsettings/edit/'.$request->input('season_id').'?t='.$status)->with('message', array('text' => $text, 'status' => 'success'));
    }

    public function seasonImageDelete($id, $siid) {

        $model = SeasonBeforeAfter::find($siid);
        $status = $model->status;
        if(!empty($model)){
         
            $model->delete();

            if($status == 'after'){
                $status = 'tab_03';
            }elseif($status == 'before'){
                $status = 'tab_02';
            }else{
                $status = 'tab_01';
            }

            $text = 'Başarıyla Silindi...';
            return redirect('seasonsettings/edit/'.$id.'?t='.$status)->with('message', array('text' => $text, 'status' => 'success'));
        }
    }
    
}





