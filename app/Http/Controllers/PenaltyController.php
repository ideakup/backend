<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Validator;

use Carbon\Carbon;
use App\User;

use App\Season;
use App\TeamTeamSeason;

use App\Match;
use App\MatchVideo;
use App\MatchPlayer;
use App\MatchAction;
use App\MatchPanorama;
use App\MatchImage;

use App\PointType;

use App\PressConferences;
use App\PressConferencesPlayers;

use App\Team;
use App\Player;
use App\TeamPlayerTeamHistory;

use App\Penalty;
use App\PlayerPenalty;
use App\TeamPenalty;
use App\PenaltyTypes;

class PenaltyController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        return view('dashboard');
    }

    public function player_penalties_crud($id = null) {
        $penalty = NULL;
        if(!is_null($id)){
            $penalty = Penalty::find($id);
        }
        return view('dashboard', array('model_data' => $penalty));
    }

    public function player_penalties_save(Request $request) {

        //dd($request->input());
        $text = "";

        if ($request->crud == 'delete') {

            $model = Penalty::find($request->id);
            
            if($model->polymorphic_ctype_id == 44){
                $player_penalty = PlayerPenalty::find($model->id);
                $player_penalty->delete();
            }else if($model->polymorphic_ctype_id == 47){
                $team_penalty = TeamPenalty::find($model->id);
                $team_penalty->delete();
            }

            $model->delete();
            $text = 'Başarıyla Silindi...';
        }else{

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $model = new Penalty();
            }else if($request->crud == 'edit'){
                $model = Penalty::find($request->id);
            }
            
            $rules = array();
            $formConfig = config('forms.'.$request->segment);

            if(!is_null($formConfig)){
                foreach ($formConfig as $key => $value){
                    if(!empty(data_get($value, 'validation'))){
                        $rules[$key] = data_get($value, 'validation');
                    }
                }
            }
            
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
               return \Redirect::back()->withErrors($validator)->withInput();
            }

            $model->polymorphic_ctype_id = $request->polymorphic_ctype_id;
            $model->league_id = $request->league_id;
            $model->season_id = $request->season_id;
            $model->match_id = $request->match_id;
            $model->description = $request->description;
            $model->save();

            $matchplayer = MatchPlayer::find($request->match_player_id);
            
            if ($request->crud == 'add') {
                $player_penalty = new PlayerPenalty();
            }else if($request->crud == 'edit'){
                $player_penalty = PlayerPenalty::find($model->id);
            }

            $player_penalty->penal_ptr_id = $model->id;
            $player_penalty->penal_type_id = $request->penalties_type_id;

            if(!empty($matchplayer->player_id)){
                $player_penalty->player_id = $matchplayer->player_id;
                $player_penalty->team_id = $matchplayer->team_id;
            }else{
                $player_penalty->guest_name = $matchplayer->guest_name;
            }

            $player_penalty->save();

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $text = 'Başarıyla Eklendi...';
            }else if($request->crud == 'edit'){
                $text = 'Başarıyla Güncellendi...';
            }
        }

        return redirect('penalties')->with('message', array('text' => $text, 'status' => 'success'));
    }

    public function team_penalties_crud($id = null) {
        $penalty = NULL;
        if(!is_null($id)){
            $penalty = Penalty::find($id);
        }
        return view('dashboard', array('model_data' => $penalty));
    }

    public function team_penalties_save(Request $request) {

        //dd($request->input());
        $text = "";

        if ($request->crud == 'delete') {

            $model = Penalty::find($request->id);
            
            if($model->polymorphic_ctype_id == 44){
                $player_penalty = PlayerPenalty::find($model->id);
                $player_penalty->delete();
            }else if($model->polymorphic_ctype_id == 47){
                $team_penalty = TeamPenalty::find($model->id);
                $team_penalty->delete();
            }

            $model->delete();
            $text = 'Başarıyla Silindi...';
        }else{


            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $model = new Penalty();
            }else if($request->crud == 'edit'){
                $model = Penalty::find($request->id);
            }
            
            $rules = array();
            $formConfig = config('forms.'.$request->segment);

            if(!is_null($formConfig)){
                foreach ($formConfig as $key => $value){
                    if(!empty(data_get($value, 'validation'))){
                        $rules[$key] = data_get($value, 'validation');
                    }
                }
            }
            
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
               return \Redirect::back()->withErrors($validator)->withInput();
            }

            $model->polymorphic_ctype_id = $request->polymorphic_ctype_id;
            $model->league_id = $request->league_id;
            $model->season_id = $request->season_id;
            $model->match_id = $request->match_id;
            $model->description = $request->description;
            $model->save();

            
            if ($request->crud == 'add') {
                $team_penalty = new TeamPenalty();
            }else if($request->crud == 'edit'){
                $team_penalty = TeamPenalty::find($model->id);
            }

            $team_penalty->penal_ptr_id = $model->id;
            $team_penalty->penal_type_id = $request->penalties_type_id;
            $team_penalty->team_id = $request->match_team_id;

            $team_penalty->save();

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $text = 'Başarıyla Eklendi...';
            }else if($request->crud == 'edit'){
                $text = 'Başarıyla Güncellendi...';
            }
        }

        return redirect('penalties')->with('message', array('text' => $text, 'status' => 'success'));
    }





    public function penalty_types() {
        return view('dashboard');
    }

    public function penalty_type_crud($id = null) {
        $penalty_types = NULL;
        if(!is_null($id)){
            $penalty_types = PenaltyTypes::find($id);
        }
        return view('dashboard', array('model_data' => $penalty_types));
    }

    public function penalty_type_save(Request $request)
    {
        //dd($request->input());
        $text = "";
        if ($request->crud == 'delete') {

            $model = PenaltyTypes::find($request->id);

            $cnt = 0;
            if($model->to_who == 'player'){
                $cnt = PlayerPenalty::where('penal_type_id', $model->id)->count();
            }else if($model->to_who == 'team'){
                $cnt = TeamPenalty::where('penal_type_id', $model->id)->count();
            }

            if($cnt == 0){
                $model->delete();
                $text = 'Başarıyla Silindi...';
            }else{
                $text = 'Bu cezanın uygulandığı en az bir kayıp bulunduğu için silinemedi!';
                return \Redirect::back()->withInput()->with('message', array('text' => $text, 'status' => 'error'));
            }

        }else{

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $model = new PenaltyTypes();
            }else if($request->crud == 'edit'){
                $model = PenaltyTypes::find($request->id);
            }

            $rules = array();
            $formConfig = config('forms.'.$request->segment);

            if(!is_null($formConfig)){
                foreach ($formConfig as $key => $value){
                    if(!empty(data_get($value, 'validation'))){
                        $rules[$key] = data_get($value, 'validation');
                    }
                }
            }
            
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
               return \Redirect::back()->withErrors($validator)->withInput();
            }

            $model->name = $request->name;
            $model->to_who = $request->to_who;
            $model->active = true;
            $model->save();

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $text = 'Başarıyla Eklendi...';
            }else if($request->crud == 'edit'){
                $text = 'Başarıyla Güncellendi...';
            }
        }

        return redirect($request->segment)->with('message', array('text' => $text, 'status' => 'success'));
    }

}
