<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Carbon\Carbon;

use App\Classes\SmsClass;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

use App\Match;
use App\Team;
use App\MatchVideo;
use App\MatchPlayer;
use App\MatchAction;
use App\MatchPanorama;
use App\MatchImage;
use App\User;
use App\Player;
use App\Season;
use App\TeamTeamSeason;
use App\PlayerPosition;

use App\PanoramaType;
use App\PressConferences;
use App\PressConferencesPlayers;

class AjaxAppApiController extends Controller
{

    /*  
        GET => getVideoCreatorPlayer
        
        {type} = 'goal', 'saving', 'nogoalnosaving'
        {offset} = integer => page * {limit}
        {limit} = integer => max:100


        POST => postVideoCreatorPlayerAddVideo
    */

    public function getAppPlayerStats($type, $offset, $limit)
    {
        
        $queryStartDate = '2021-01-01';

        $_limit = $limit;
        if ($limit > 100) {
            $_limit = 100;
        }

        $data = array();
        $data['type'] = $type;
        $data['offset'] = $offset;
        $data['limit'] = $_limit;
        $data['result'] = array();

        if($type == 'goal'){

            $user_ids = User::select(
                'users.id as user_id'
                /*
                'users.first_name',
                'users.last_name',
                'users.is_active',
                'users.phone',
                'users.phone_confirmed',

                'players.id as player_id',
                'players.number',

                'match_player.id as match_player_id',
                'match_player.rating',
                'match_player.saving',
                'match_player.match_id',

                'matches.id as match_id',
                'matches.date',

                'match_actions.id as match_actions_id',
                'match_actions.type',
                */
            )->
            leftJoin('players', 'players.user_id', '=', 'users.id')->

            leftJoin('match_player', 'match_player.player_id', '=', 'players.id')->
            leftJoin('matches', 'matches.id', '=', 'match_player.match_id')->
            leftJoin('match_actions', 'match_actions.match_player_id', '=', 'match_player.id')->

            whereNotNULL('players.id')->where('matches.date', '>=', $queryStartDate)->
            whereNotNULL('match_actions.id')->where('match_actions.type', 'goal')->
            offset($offset)->limit($_limit)->distinct()->orderBy('users.id', 'desc')->get()->pluck('user_id');

            $users = User::select(
                '*',
                DB::raw("CONCAT(users.first_name, ' ', users.last_name) as fullname")
            )->
            whereIn('users.id', $user_ids)->orderBy('users.id', 'desc')->get();

            foreach ($users as $user) {
                $userArr = array();
                $userArr['user_id'] = $user->id;
                $userArr['fullname'] = $user->fullname;
                //$userArr['player'] = $user->player->toArray();
                $userArr['match_count'] = 0;
                $userArr['goal_count'] = 0;

                $temp_rating = 0;
                
                $match_players = $user->player->match_player;
                foreach ($match_players as $match_player) {

                    if($match_player->match->date >= $queryStartDate){
                        $userArr['match_count']++;
                        $userArr['goal_count'] += $match_player->match_action_goal->count();
                        $temp_rating += $match_player->rating;
                        /*
                            $match_playersArr = array();
                            $match_playersArr = $match_player->toArray();
                            $match_playersArr['match_date'] = $match_player->match->date;
                            $match_playersArr['goal'] = $match_player->match_action_goal->count();
                            $userArr['match_players'][] = $match_playersArr;
                        */
                    }
                }
                $userArr['rating'] = number_format($temp_rating / $userArr['match_count'], 1, '.', ',');
                $data['result'][] = $userArr;
            }

        }elseif($type == 'saving'){

            $user_ids = User::select(
                'users.id as user_id'
                /*
                'users.first_name',
                'users.last_name',
                'users.is_active',
                'users.phone',
                'users.phone_confirmed',

                'players.id as player_id',
                'players.number',

                'match_player.id as match_player_id',
                'match_player.rating',
                'match_player.saving',
                'match_player.match_id',

                'matches.id as match_id',
                'matches.date',

                'match_actions.id as match_actions_id',
                'match_actions.type',
                */
                
            )->
            leftJoin('players', 'players.user_id', '=', 'users.id')->

            leftJoin('match_player', 'match_player.player_id', '=', 'players.id')->
            leftJoin('matches', 'matches.id', '=', 'match_player.match_id')->
            leftJoin('match_actions', 'match_actions.match_player_id', '=', 'match_player.id')->

            whereNotNULL('players.id')->where('matches.date', '>=', $queryStartDate)->
            whereNULL('match_actions.id')->
            offset($offset)->limit($_limit)->distinct()->orderBy('users.id', 'desc')->get()->pluck('user_id');

            $users = User::select(
                '*',
                DB::raw("CONCAT(users.first_name, ' ', users.last_name) as fullname")
            )->
            whereIn('users.id', $user_ids)->orderBy('users.id', 'desc')->get();

            foreach ($users as $user) {
                $userArr = array();
                $userArr['user_id'] = $user->id;
                $userArr['fullname'] = $user->fullname;
                //$userArr['player'] = $user->player->toArray();
                $userArr['match_count'] = 0;
                $userArr['saving_count'] = 0;

                $goal_count = 0;
                $temp_rating = 0;
                
                $match_players = $user->player->match_player;
                foreach ($match_players as $match_player) {

                    if($match_player->match->date >= $queryStartDate){
                        $userArr['match_count']++;
                        $goal_count += $match_player->match_action_goal->count();

                        if(!empty($match_player->saving)){
                            $userArr['saving_count'] += $match_player->saving;
                        }

                        $temp_rating += $match_player->rating;
                        /*
                            $match_playersArr = array();
                            $match_playersArr = $match_player->toArray();
                            $match_playersArr['match_date'] = $match_player->match->date;
                            $match_playersArr['goal'] = $match_player->match_action_goal->count();
                            $userArr['match_players'][] = $match_playersArr;
                        */
                    }
                }
                $userArr['rating'] = number_format($temp_rating / $userArr['match_count'], 1, '.', ',');

                if($goal_count == 0 && $userArr['saving_count'] > 0){
                    $data['result'][] = $userArr;
                }
            }

        }elseif($type == 'nogoalnosaving'){

            $user_ids = User::select(
                'users.id as user_id'
                /*
                'users.first_name',
                'users.last_name',
                'users.is_active',
                'users.phone',
                'users.phone_confirmed',

                'players.id as player_id',
                'players.number',

                'match_player.id as match_player_id',
                'match_player.rating',
                'match_player.saving',
                'match_player.match_id',

                'matches.id as match_id',
                'matches.date'
                */
            )->
            leftJoin('players', 'players.user_id', '=', 'users.id')->

            leftJoin('match_player', 'match_player.player_id', '=', 'players.id')->
            leftJoin('matches', 'matches.id', '=', 'match_player.match_id')->

            whereNotNULL('players.id')->where('matches.date', '>=', $queryStartDate)->
            offset($offset)->limit($_limit)->distinct()->orderBy('users.id', 'desc')->get()->pluck('user_id');


            $users = User::select(
                '*',
                DB::raw("CONCAT(users.first_name, ' ', users.last_name) as fullname")
            )->
            whereIn('users.id', $user_ids)->orderBy('users.id', 'desc')->get();

            foreach ($users as $user) {
                $userArr = array();
                $userArr['user_id'] = $user->id;
                $userArr['fullname'] = $user->fullname;
                //$userArr['player'] = $user->player->toArray();
                $userArr['match_count'] = 0;

                $userArr['goal_count'] = 0;
                $userArr['saving_count'] = 0;
                $temp_rating = 0;
                
                $match_players = $user->player->match_player;
                foreach ($match_players as $match_player) {

                    if($match_player->match->date >= $queryStartDate){
                        $userArr['match_count']++;
                        $userArr['goal_count'] += $match_player->match_action_goal->count();

                        if(!empty($match_player->saving)){
                            $userArr['saving_count'] += $match_player->saving;
                        }

                        $temp_rating += $match_player->rating;
                        /*
                            $match_playersArr = array();
                            $match_playersArr = $match_player->toArray();
                            $match_playersArr['match_date'] = $match_player->match->date;
                            $match_playersArr['goal'] = $match_player->match_action_goal->count();
                            $userArr['match_players'][] = $match_playersArr;
                        */
                    }
                }
                $userArr['rating'] = number_format($temp_rating / $userArr['match_count'], 1, '.', ',');

                if($userArr['goal_count'] == 0 && $userArr['saving_count'] == 0){
                    $data['result'][] = $userArr;
                }
            }

        }

        return response()->json($data, 200);
    }

    public function postAppAddVideoPlayer(Request $request)
    {

        if(!empty($request->input('user_id')) && !empty($request->input('player_video_embed_code'))){
            $user = User::find($request->input('user_id'));
            $player = $user->player;
            $player->embed_code = $request->input('player_video_embed_code');
            $player->save();

            return response()->json("OK", 200);
        }else{
            return response()->json("ERROR", 200);
        }
    }

    public function getAppCoordinatorMatches(Request $request, $p = null)
    {
        $data = array();

        if(!empty($request->input('page'))){
            $pageNumber = $request->input('page');
        }else{
            $pageNumber = 1;
        }

        $pageLimit = 12;
        $roleProvince = collect(Auth::user()->roleprovince)->pluck('province_id');
        
        $collects = Match::select(
            'matches.id',
            'matches.team1_id',
            'matches.team2_id',
            'matches.season_id as season',
            'matches.completed',
            'matches.team1_goal',
            'matches.team2_goal',
            'matches.team1_point',
            'matches.team2_point',
            'matches.date',
            'matches.time',
            'match_video.status as video_status'
        )->
        leftJoin('match_video', 'match_video.match_id', '=', 'matches.id')->
        leftJoin('seasons', 'seasons.id', '=', 'matches.season_id')->
        leftJoin('leagues', 'leagues.id', '=', 'seasons.league_id')->
        leftJoin('leagues_provinces', 'leagues.id', '=', 'leagues_provinces.league_id')->
        leftJoin('coordinators', 'coordinators.id', '=', 'matches.coordinator_id')->
        where('matches.completed', false);

        if(Auth::user()->email != 'ahmetbguller@gmail.com'){
            $collects = $collects->where('coordinators.user_id', Auth::user()->id);
        }
        
        if($roleProvince->count() > 0){
            $collects = $collects->whereIn('leagues_provinces.province_id', $roleProvince);
        }

        if(!empty($p) && $p == 'completed'){
            $collects = $collects->whereIn('match_video.status', array('recorded', 'finished'));
        }elseif(!empty($p) && $p == 'future'){
            $collects = $collects->whereIn('match_video.status', array('recordable', 'recording'));
        }


        $data['count'] = $collects->count();

        if($data['count'] - ($pageNumber * $pageLimit) < 0){
            $data['next'] = null;
        }else{
            $data['next'] = url('/api/app/coordinator-matches').'/'. ((!empty($p)) ? $p.'/' : '') .'?page='.($pageNumber+1);
        }

        if($pageNumber <= 1){
            $data['previous'] = null;
        }else{
            $data['previous'] = url('/api/app/coordinator-matches').'/'. ((!empty($p)) ? $p.'/' : '') .'?page='.($pageNumber-1);
        }

        $collects = $collects->orderBy('date', 'DESC')->offset($pageLimit*($pageNumber-1))->limit($pageLimit)->get();

        foreach ($collects as $collect) {
            $result = $collect->toArray();

            $result['team1']['id'] = $collect->team1->id;
            $result['team1']['name'] = $collect->team1->name;
            if (!empty($collect->team1->image)) {
                $result['team1']['image_url'] = env('MEDIA_END').$collect->team1->image;
            }else{
                $result['team1']['image_url'] = env('MEDIA_END').'default-team.png';
            }

            $result['team2']['id'] = $collect->team2->id;
            $result['team2']['name'] = $collect->team2->name;
            if (!empty($collect->team2->image)) {
                $result['team2']['image_url'] = env('MEDIA_END').$collect->team2->image;
            }else{
                $result['team2']['image_url'] = env('MEDIA_END').'default-team.png';
            }

            $data['results'][] = $result;
        }

        return response()->json($data, 200);
    }

    public function getAppTeamPlayers($team_id, Request $request)
    {
        $data = array();

        $team = Team::find($team_id);
        $collects = $team->player;

        foreach ($collects as $collect) {

            $result['id'] = $collect->id;
            if(!empty($collect->players_position)){
                $result['position']['id'] = $collect->players_position->id;
                $result['position']['name'] = $collect->players_position->name;
                $result['position']['code'] = $collect->players_position->code;
            }

            $result['user']['first_name'] = $collect->user->first_name;
            $result['user']['last_name'] = $collect->user->last_name;
            $result['user']['fullname'] = $collect->user->first_name.' '.$collect->user->last_name;
            if(!empty($collect->user->image)){
                $result['user']['image_url'] = env('MEDIA_END').$collect->user->image;
            }else{
                $result['user']['image_url'] = env('MEDIA_END').'default-player.png';
            }

            $result['team']['id'] = $collect->team->id;
            $result['team']['name'] = $collect->team->name;
            if (!empty($collect->team->image)) {
                $result['team']['image_url'] = env('MEDIA_END').$collect->team->image;
            }else{
                $result['team']['image_url'] = env('MEDIA_END').'default-team.png';
            }

            $data[] = $result;
            
        }
        
        return response()->json($data, 200);
    }

    public function getAppPlayer($match_id, Request $request)
    {
        $data = array();

        $match = Match::find($match_id);
        $match_players = $match->match_player;

        $data['match_id'] = $match_id;
        $data['match_players'] = array();

        foreach ($match_players as $match_player) {

            $model = array();

            if (!empty($match_player->player_id)) {
                $model['player_id'] = $match_player->player_id;
            } elseif (!empty($match_player->guest_name)) {
                $model['guest_name'] = $match_player->guest_name;
            }

            $model['match_id'] = $match_player->match_id;
            $model['team_id'] = $match_player->team_id;
            $model['position_id'] = $match_player->position_id;
            $model['rating'] = $match_player->rating;
            if(!empty($match_player->saving)){
                $model['saving'] = $match_player->saving;
            }

            $model['id'] = $match_player->id;

            $data['match_players'][] = $model;
            
        }

        return response()->json($data, 200);
    }

    public function setAppPlayer($match_id, Request $request)
    {
        $data = array();

        $match_players = collect($request->input());

        //$data['match_players'] = $match_players;
        $data['match_id'] = $match_id;
        $data['match_players'] = array();

        foreach ($match_players as $match_player) {

            if (!empty($match_player['player_id'])) {

                $model = MatchPlayer::where('match_id', $match_id)->where('player_id', $match_player['player_id'])->first();
                if (is_null($model)) {
                    $model = new MatchPlayer();
                    $model->player_id = $match_player['player_id'];
                }

            } elseif (!empty($match_player['guest_name'])) {

                $model = new MatchPlayer();
                $model->guest_name = $match_player['guest_name'];

            }

            $model->match_id = intval($match_id);
            $model->team_id = $match_player['team'];
            $model->position_id = $match_player['position'];
            $model->rating = $match_player['rating'];
            if(!empty($match_player['saving'])){
                $model->saving = $match_player['saving'];
            }

            $model->save();

            if (!empty($match_player['guest_name'])) {
                if(!empty($match_player['guest_id'])){
                    $model->guest_id = $match_player['guest_id'];
                }else{
                    $model->guest_id = $model->id;
                }
            }
            $data['match_players'][] = $model;
            
        }

        $data['success'] = true;

        return response()->json($data, 200);
    }

    public function setAppAction($match_id, Request $request)
    {
        $data = array();

        $match_actions = collect($request->input('goals'));

        $data['match_id'] = $match_id;
        $data['match_actions'] = array();

        foreach ($match_actions as $match_action) {

            /*
                $model = MatchPlayer::where('match_id', $match_id)->where('match_player_id', $match_action['match_player'])->first();
                if (is_null($model)) {
                    $model = new MatchPlayer();
                    $model->player_id = $match_player['player_id'];
                }
            */

            $model = new MatchAction();

            if(!empty($match_action['match_player'])){
                $model->match_player_id = $match_action['match_player'];
            }

            $model->match_id = intval($match_id);
            $model->team_id = $match_action['team'];

            $model->type = $match_action['type'];
            $model->minute = $match_action['minute'];
            $model->embed_source = 'app';
            $model->embed_code = $match_action['embed_code'];

            $model->save();

            $data['match_actions'][] = $model;
            
        }
        
        $data['success'] = true;

        return response()->json($data, 200);
    }

    public function setAppVideo($match_id, Request $request)
    {
        $data = array();

        $match_actions = collect($request->input());

        $data['match_id'] = $match_id;

        $model = MatchVideo::where('match_id', $match_id)->first();
        $model->status = $match_actions['status'];
        
        if(!empty($match_actions['live_source'])){
            $model->live_source = $match_actions['live_source'];
            $model->live_embed_code = $match_actions['live_embed_code'];
        }
        
        if (!empty($match_actions['full_source'])) {
            $model->full_source = $match_actions['full_source'];
            $model->full_embed_code = $match_actions['full_embed_code'];
        }

        if (!empty($match_actions['summary_source'])) {
            $model->summary_source = $match_actions['summary_source'];
            $model->summary_embed_code = $match_actions['summary_embed_code'];
        }
        
        $model->save();

        $data['match_video'] = $match_actions;

        $data['success'] = true;

        return response()->json($data, 200);
    }

    public function getAppPanoramaTypes(Request $request)
    {
        $data = PanoramaType::select('panorama_types.id','panorama_types.name','panorama_types.action_conn')->
        leftJoin('panorama_types_prize', 'panorama_types_prize.panoramatype_ptr_id', '=', 'panorama_types.id')->
        where('panorama_types_prize.period', 'match')->where('active', true)->orderBy('id', 'asc')->get();

        return response()->json($data, 200);
    }

    public function getAppPanorama($match_id, Request $request)
    {
        $data = MatchPanorama::select('id','match_id','match_player_id','panorama_id','embed_code','embed_source','action_id')->where('match_id', $match_id)->get();
        return response()->json($data, 200);
    }

    public function setAppPanorama($match_id, Request $request)
    {
        $data = array();

        foreach ($request->input() as $panoramaData) {

            if(empty($panoramaData['id'])){
                /* ADD */
                $model = new MatchPanorama();
                $model->match_id = $panoramaData['match_id'];
                $mpCount = MatchPanorama::where('match_id', $panoramaData['match_id'])->where('panorama_id', $panoramaData['panorama_id'])->count(); 
            }else{
                /* EDIT */
                $model = MatchPanorama::find($panoramaData['id']);
                $mpCount = MatchPanorama::where('match_id', $panoramaData['match_id'])->where('panorama_id', $panoramaData['panorama_id'])->where('id', '!=', $model->id)->count();
            }

            if($mpCount > 0){
                $panoramaData['process_status'] = 'error:You can add only one of the same panorama type.';
            }else{
                $panoramaData['process_status'] = 'success';
            }

            if($mpCount == 0){

                $model->match_player_id = $panoramaData['match_player_id'];
                $model->panorama_id = $panoramaData['panorama_id'];
                
                if(!empty($panoramaData['action_id'])){

                    $model_action = MatchAction::find($panoramaData['action_id']);
                    $model->action_id = $panoramaData['action_id'];
                    $model->embed_source = $model_action->embed_source;
                    $model->embed_code = $model_action->embed_code;

                }else{

                    $model->embed_source = $panoramaData['embed_source'];
                    $model->embed_code = $panoramaData['embed_code'];

                }

                $model->save();

                if($model->match->completed && !is_null(MatchPlayer::find($panoramaData['match_player_id'])->player_id)){
                    $match_season = $model->match->season;
                    $league_provinces = $model->match->season->league->league_province;


                    $panoramaPoint = array(1 => array(8000, 'Maçın Gümüş Oyuncusu'), 2 => array(5000, 'Maçın Golü'), 3 => array(15000, 'Maçın Altın Oyuncusu'), 4 => array(5000, 'Maçın Dinamik Oyuncusu'), 5 => array(5000, 'Maçın Defansı'), 6 => array(5000, 'Maçın Orta Sahası'), 7 => array(5000, 'Maçın Forveti'), 8 => array(5000, 'Maçın Kalecisi'));

                    $seasonCross = array(4, 3.5, 2.5, 1.5);
                    $seasonCrossIndex = 0;

                    $seasonTRCross = array(10, 3);
                    $seasonTRCrossIndex = 0;

                    $seasonList = array();
                    $seasonTRList = array();
                    
                    foreach ($league_provinces as $league_province) {

                        if($league_province->league->league_province->count() == 1){
                            
                            $seasons = $league_province->league->seasonApi;
                            foreach ($seasons as $season) {

                                if($season->polymorphic_ctype_id == 32){ $seasonType = 'elimination'; }else if($season->polymorphic_ctype_id == 33){ $seasonType = 'fixture'; }else if($season->polymorphic_ctype_id == 34){ $seasonType = 'point'; }
                                $seasonList[] = array($seasonCross[$seasonCrossIndex], $season->id, $seasonType, ($season->year.' - '.$season->name), $season->year);

                                if(!empty($season->league_eliminationseasonApi)){
                                    if($season->league_eliminationseasonApi->season_ptr->polymorphic_ctype_id == 32){ $seasonRType = 'elimination'; }else if($season->league_eliminationseasonApi->season_ptr->polymorphic_ctype_id == 33){ $seasonRType = 'fixture'; }else if($season->league_eliminationseasonApi->season_ptr->polymorphic_ctype_id == 34){ $seasonRType = 'point'; }

                                    $seasonList[] = array($seasonCross[$seasonCrossIndex], $season->league_eliminationseasonApi->season_ptr->id, $seasonRType, ($season->league_eliminationseasonApi->season_ptr->year.' - '.$season->league_eliminationseasonApi->season_ptr->name), $season->league_eliminationseasonApi->season_ptr->year );
                                }

                                if(count($seasonCross)-1 > $seasonCrossIndex){
                                    $seasonCrossIndex++;
                                }
                            }
                            
                            $all_seasons = $league_province->league->season->where('active', true);
                            foreach ($all_seasons as $season) {

                                if($season->polymorphic_ctype_id == 32){ $seasonEType = 'elimination'; }else if($season->polymorphic_ctype_id == 33){ $seasonEType = 'fixture'; }else if($season->polymorphic_ctype_id == 34){ $seasonEType = 'point'; }
                                $is_valid = array_search($season->id, array_column($seasonList, 1));
                                
                                if($is_valid === false){
                                    $seasonList[] = array(end($seasonCross), $season->id, $seasonEType, ($season->year.' - '.$season->name), $season->year);
                                }
                            }
                            

                        }else if($league_province->league->league_province->count() > 1){

                            $seasonsTR = $league_province->league->seasonApi;
                            foreach ($seasonsTR as $seasonTR) {

                                if($seasonTR->polymorphic_ctype_id == 32){ $seasonType = 'elimination'; }else if($seasonTR->polymorphic_ctype_id == 33){ $seasonType = 'fixture'; }else if($seasonTR->polymorphic_ctype_id == 34){ $seasonType = 'point'; }
                                $seasonTRList[] = array($seasonTRCross[$seasonTRCrossIndex], $seasonTR->id, $seasonType, ($seasonTR->year.' - '.$seasonTR->name), $seasonTR->year);

                                if(!empty($seasonTR->league_eliminationseasonApi)){
                                    if($seasonTR->league_eliminationseasonApi->season_ptr->polymorphic_ctype_id == 32){ $seasonRType = 'elimination'; }else if($seasonTR->league_eliminationseasonApi->season_ptr->polymorphic_ctype_id == 33){ $seasonRType = 'fixture'; }else if($seasonTR->league_eliminationseasonApi->season_ptr->polymorphic_ctype_id == 34){ $seasonRType = 'point'; }

                                    $seasonTRList[] = array($seasonTRCross[$seasonTRCrossIndex], $seasonTR->league_eliminationseasonApi->season_ptr->id, $seasonRType, ($seasonTR->league_eliminationseasonApi->season_ptr->year.' - '.$seasonTR->league_eliminationseasonApi->season_ptr->name), $seasonTR->league_eliminationseasonApi->season_ptr->year );
                                }

                                if(count($seasonTRCross)-1 > $seasonTRCrossIndex){
                                    $seasonTRCrossIndex++;
                                }
                            }


                            $all_seasonsTR = $league_province->league->season->where('active', true);
                            foreach ($all_seasonsTR as $seasonTR) {

                                if($seasonTR->polymorphic_ctype_id == 32){ $seasonEType = 'elimination'; }else if($seasonTR->polymorphic_ctype_id == 33){ $seasonEType = 'fixture'; }else if($seasonTR->polymorphic_ctype_id == 34){ $seasonEType = 'point'; }
                                $is_valid = array_search($seasonTR->id, array_column($seasonTRList, 1));
                                
                                if($is_valid === false){
                                    $seasonTRList[] = array(end($seasonTRCross), $seasonTR->id, $seasonEType, ($seasonTR->year.' - '.$seasonTR->name), $seasonTR->year);
                                }
                            }

                        }else{
                            // AÇÇ dump('sıkıntı olmasın !!!');                
                            // AÇÇ dump('???? '.$league_province->league->name);
                        }
                    }

                    if(count($seasonList) > 0){

                        $match_player = MatchPlayer::find($model->match_player_id);
                        if(!empty($match_season)){

                            $is_valid = array_search($match_season->id, array_column($seasonList, 1));
                            
                            $panoramaTotalPoint = 0;
                            foreach ($match_player->match_player_panorama as $panorama) {
                                $panoramaTotalPoint += $panoramaPoint[$panorama->panorama_id][0];
                            }

                            $matchPoint = $seasonList[$is_valid][0] * (
                                $panoramaTotalPoint
                            );

                            $player_val = Player::find($match_player->player_id);
                            $player_val->value += $matchPoint;
                            $player_val->save();
                        }

                    }

                    if(count($seasonTRList) > 0){

                        $match_player = MatchPlayer::find($model->match_player_id);
                        if(!empty($match_season)){
                            
                            //$matchTotalPointArray = array();
                            $is_valid = array_search($match_season->id, array_column($seasonTRList, 1));
                            
                            $playerHistoryTRPoint = 0;
                            foreach ($match_player->match_player_panorama as $panorama) {
                                $playerHistoryTRPoint += $panoramaPoint[$panorama->panorama_id][0];
                            }
                            
                            $matchPoint = $seasonTRList[$is_valid][0] * (
                                $playerHistoryTRPoint
                            );

                            //$matchTotalPointArray = array('match_id' => $match_player->match_id, 'point' => $matchPoint, 'match_player' => $match_player->id, 'player' => $match_player->player->id);
                            
                            $player_val = Player::find($match_player->player_id);
                            $player_val->value += $matchPoint;
                            $player_val->save();
                        }
                        
                    }
                }
            }

            $data[] = $panoramaData;

        }

        return response()->json($data, 200);
    }

    public function getAppPress($match_id, Request $request)
    {
        $data = PressConferences::select('id','match_id','team_id','embed_code','embed_source')->where('match_id', $match_id)->first();
        if(!empty($data->press_conferences_players)){
            $data->press_conferences_players;
        }
        return response()->json($data, 200);
    }

    public function setAppPress($match_id, Request $request)
    {

        $model = PressConferences::where('match_id', $match_id)->first();
        /* ADD */
        if (is_null($model)) {
            $model = new PressConferences();
            $model->match_id = $match_id;
        }
        $model->embed_source = $request->embed_source;
        $model->embed_code = $request->embed_code;
        $model->team_id = $request->team_id;
        $model->save();

         /* EDIT */
        foreach ($model->press_conferences_players as $value_pcp) {
            $value_pcp->delete();
        }

        if(!empty($request->match_player_ids)){
            foreach ($request->match_player_ids as $value_pl) {
                $pcp = new PressConferencesPlayers();
                $pcp->pressconference_id = $model->id;
                $pcp->matchplayer_id = $value_pl;
                $pcp->save();
            }
        }

        $data = PressConferences::select('id','match_id','team_id','embed_code','embed_source')->where('match_id', $match_id)->first();
        if(!empty($data->press_conferences_players)){
            $data->press_conferences_players;
        }

        return response()->json($data, 200);
    }

    public function getAppPhoto($match_id, Request $request)
    {
        $data = MatchImage::select('id','match_id','image','description')->where('match_id', $match_id)->get();
        foreach ($data as $image) {
            $image->image = env('MEDIA_END').$image->image;
        }
        return response()->json($data, 200);
    }

    public function setAppPhoto($match_id, Request $request)
    {

        $data = array();

        $conf = array('width' => 900, 'height' => 600, 'aspectRatio' => 0, 'widen' => true, 'heighten' => true, 'crop' => true);
        $photo = $request->file('file');
        
        $fileName = str_slug(str_before($photo->getClientOriginalName(), '.'), '_').'_'.str_random(5).'.'.$photo->getClientOriginalExtension();
        $imageObj = Image::make($photo);

        if ($conf['widen']) {
            $imageObj->widen($conf['width'], function ($constraint) {
                $constraint->upsize();
            });
        }

        $ext = $photo->getClientOriginalExtension();
        if($ext == "png"){
            $imageObj->save(env('MEDIA_PATH').'match-images/'.$fileName, 93);
        }else{
            $imageObj->save(env('MEDIA_PATH').'match-images/'.$fileName, 93, 'jpg');
        }

        $matchimage = new MatchImage();
        $matchimage->image = 'match-images/'.$fileName;
        $matchimage->match_id = $match_id;
        $matchimage->save();
        
        $data['location'] = $matchimage->image;
        $data['id'] = $matchimage->id;
        $data['match_id'] = $matchimage->match_id;
        $data['status'] = 'success';
        
        return response()->json($data, 200);
    }

    public function setAppMatchCompleted($match_id, Request $request)
    {

        $data = array();

        $text = "";
        $match_completed_status = 'completed:true'; // tamamlandıya değiştir.
        $smsSendMatchCompletedControl = true;
        $smsSendNewMatchControl = false;

        $model = Match::find($match_id);

        if($model->completed == true){

            $data['match_id'] = $model->id;
            $data['status'] = 'error';
            return response()->json($data, 200);

        }else{

            $model->completed_at = Carbon::now();
            $model->completed = true;
            $model->save();


            $season = Season::find($model->season_id);
            $min_point = 0;
            
            if($season->polymorphic_ctype_id == 33){
                $min_point = $season->league_fixtureseason->min_team_point;
            }else if($season->polymorphic_ctype_id == 34){
                $min_point = $season->league_pointseason->min_team_point;
            }

            
            $teamSeason1 = TeamTeamSeason::where('season_id', $model->season_id)->where('team_id', $model->team1_id)->first();
            if(empty($teamSeason1)){

                $teamSeason = new TeamTeamSeason();

                if($season->polymorphic_ctype_id == 32){ /*elimination*/
                    $teamSeason->squad_locked = true;
                }else{
                    $teamSeason->squad_locked = false;
                }
                
                $teamSeason->point = $min_point;
                $teamSeason->season_id = $model->season_id;
                $teamSeason->team_id = $model->team1_id;

                $teamSeason->remaining_transfer_count = $season->allowed_transfer_count;
                $teamSeason->playoff_member = true;
                $teamSeason->drawn = 0;
                $teamSeason->goal_against = 0;
                $teamSeason->goal_for = 0;
                $teamSeason->lost = 0;
                $teamSeason->match_total = 0;
                $teamSeason->won = 0;
                $teamSeason->gk_save = 0;
                $teamSeason->red_card = 0;
                $teamSeason->yellow_card = 0;

                $teamSeason->save();
            }

            $teamSeason2 = TeamTeamSeason::where('season_id', $model->season_id)->where('team_id', $model->team2_id)->first();
            if(empty($teamSeason2)){

                $teamSeason = new TeamTeamSeason();

                if($season->polymorphic_ctype_id == 32){ /*elimination*/
                    $teamSeason->squad_locked = true;
                }else{
                    $teamSeason->squad_locked = false;
                }
                
                $teamSeason->point = $min_point;
                $teamSeason->season_id = $model->season_id;
                $teamSeason->team_id = $model->team2_id;

                $teamSeason->remaining_transfer_count = $season->allowed_transfer_count;
                $teamSeason->playoff_member = true;
                $teamSeason->drawn = 0;
                $teamSeason->goal_against = 0;
                $teamSeason->goal_for = 0;
                $teamSeason->lost = 0;
                $teamSeason->match_total = 0;
                $teamSeason->won = 0;
                $teamSeason->gk_save = 0;
                $teamSeason->red_card = 0;
                $teamSeason->yellow_card = 0;

                $teamSeason->save();
            }

            //Tamamlanmış maçlar için teamseason kayıtlarında güncellemeler yapar
            app(\App\Http\Controllers\MatchController::class)->teamseasons_statistics_update($model, $match_completed_status);
            app(\App\Http\Controllers\MatchController::class)->players_history_update($model, $match_completed_status);
            app(\App\Http\Controllers\MatchController::class)->players_deger_update($model, $match_completed_status);
            
            //$this->teamseasons_statistics_update($model, $match_completed_status);
            //$this->players_history_update($model, $match_completed_status);
            //$this->players_deger_update($model, $match_completed_status);
            
            if($smsSendMatchCompletedControl && $model->sendsms != 1){
                //SMS GÖNDER; //Mac Tamamlandiya Alininca Takim Kadrosundaki Tum Oyunculara Gonder
                $smsHelper = new SmsClass();
                $smsHelper->smsSendMatchCompletedTrigger($model);
                $model->sendsms = 1;
                $model->save();
            }

            $data['match_id'] = $model->id;
            $data['status'] = 'success';
            return response()->json($data, 200);
        }
    }

    /* getMatchPlayerWithTeamPlayerDetails (New)
        ** Her iki takım oyuncularını döndürür
        ** match_player_id null ise maça eklenmemiş takım oyuncusudur
        ** match_player_id dolu ise maç oyuncusudur.
        ** match_player > guest_name dolu ise misafir maç oyuncusudur.
        ** match_player > guest_name null ise takım da yeralan maç oyuncusudur.

        ** return postMatchPlayerWithTeamPlayerDetails ile aynıdır.
    */
    public function getAppTMPlayer($match_id, $team_id = null, Request $request)
    {
        $data = array();

        $match = Match::find($match_id);
        if(!empty($match)){
            
            if(!empty($match->team1_id)){
                if(!empty($match->team1)){
                    if(empty($team_id) || (!empty($team_id) && $match->team1_id == $team_id)){
                        foreach ($match->team1->player as $tplayer) {
                            $tplayer_temp = null;
                            
                            $tplayer_temp['team_id'] = $tplayer->team_id;
                            $tplayer_temp['player_id'] = $tplayer->id;
                            $tplayer_temp['position_id'] = $tplayer->position_id;
                            
                            $match_player = $match->match_player_is_not_guest->where('player_id', $tplayer->id)->first();
                            
                            if (!empty($match_player)){

                                $tplayer_temp['match_player_id'] = $match_player->id;
                                $tplayer_temp['match_player']['player_id'] = $match_player->player_id;
                                $tplayer_temp['match_player']['guest_name'] = null;
                                $tplayer_temp['match_player']['match_id'] = $match_player->match_id;
                                $tplayer_temp['match_player']['team_id'] = $match_player->team_id;
                                $tplayer_temp['match_player']['position_id'] = $match_player->position_id;
                                $tplayer_temp['match_player']['rating'] = $match_player->rating;

                                if(!empty($match_player->saving)){
                                    $tplayer_temp['match_player']['saving'] = $match_player->saving;
                                }

                                $tplayer_temp['match_player']['id'] = $match_player->id;
                            }else{
                                $tplayer_temp['match_player_id'] = null;
                            }

                            $tplayer_temp['user']['first_name'] = $tplayer->user->first_name;
                            $tplayer_temp['user']['last_name'] = $tplayer->user->last_name;
                            $tplayer_temp['user']['fullname'] = $tplayer->user->first_name.' '.$tplayer->user->last_name;
                            if(!empty($tplayer->user->image)){
                                $tplayer_temp['user']['image_url'] = env('APP_MEDIA_PATH').$tplayer->user->image;
                            }else{
                                $tplayer_temp['user']['image_url'] = env('APP_MEDIA_PATH').'default-player.png';
                            }

                            $tplayer_temp['team']['id'] = $tplayer->team->id;
                            $tplayer_temp['team']['name'] = $tplayer->team->name;
                            if (!empty($tplayer->team->image)) {
                                $tplayer_temp['team']['image_url'] = env('APP_MEDIA_PATH').$tplayer->team->image;
                            }else{
                                $tplayer_temp['team']['image_url'] = env('APP_MEDIA_PATH').'default-team.png';
                            }

                            $data[] = $tplayer_temp;
                        }

                        foreach ($match->match_player_is_guest->where('team_id', $match->team1_id) as $tguest) {
                            $tguest_temp = null;
                            
                            $tguest_temp['team_id'] = $tguest->team_id;
                            $tguest_temp['player_id'] = null;

                            $tguest_temp['match_player_id'] = $tguest->id;
                            $tguest_temp['match_player']['player_id'] = null;
                            $tguest_temp['match_player']['guest_name'] = $tguest->guest_name;
                            $tguest_temp['match_player']['match_id'] = $tguest->match_id;
                            $tguest_temp['match_player']['team_id'] = $tguest->team_id;
                            $tguest_temp['match_player']['position_id'] = $tguest->position_id;
                            $tguest_temp['match_player']['rating'] = $tguest->rating;
                            if(!empty($tguest->saving)){
                                $tguest_temp['match_player']['saving'] = $tguest->saving;
                            }

                            $tguest_temp['match_player']['id'] = $tguest->id;

                            $tguest_temp['team']['id'] = $tguest->team->id;
                            $tguest_temp['team']['name'] = $tguest->team->name;
                            if (!empty($tguest->team->image)) {
                                $tguest_temp['team']['image_url'] = env('APP_MEDIA_PATH').$tguest->team->image;
                            }else{
                                $tguest_temp['team']['image_url'] = env('APP_MEDIA_PATH').'default-team.png';
                            }

                            $data[] = $tguest_temp;
                        }
                    }
                }
            }

            if(!empty($match->team2_id)){
                if(!empty($match->team2)){
                    if(empty($team_id) || (!empty($team_id) && $match->team2_id == $team_id)){
                        foreach ($match->team2->player as $tplayer) {
                            $tplayer_temp = null;
                            
                            $tplayer_temp['team_id'] = $tplayer->team_id;
                            $tplayer_temp['player_id'] = $tplayer->id;
                            $tplayer_temp['position_id'] = $tplayer->position_id;
                            
                            $match_player = $match->match_player_is_not_guest->where('player_id', $tplayer->id)->first();
                            
                            if (!empty($match_player)){

                                $tplayer_temp['match_player_id'] = $match_player->id;
                                $tplayer_temp['match_player']['player_id'] = $match_player->player_id;
                                $tplayer_temp['match_player']['guest_name'] = null;
                                $tplayer_temp['match_player']['match_id'] = $match_player->match_id;
                                $tplayer_temp['match_player']['team_id'] = $match_player->team_id;
                                $tplayer_temp['match_player']['position_id'] = $match_player->position_id;
                                $tplayer_temp['match_player']['rating'] = $match_player->rating;

                                if(!empty($match_player->saving)){
                                    $tplayer_temp['match_player']['saving'] = $match_player->saving;
                                }

                                $tplayer_temp['match_player']['id'] = $match_player->id;
                            }else{
                                $tplayer_temp['match_player_id'] = null;
                            }

                            $tplayer_temp['user']['first_name'] = $tplayer->user->first_name;
                            $tplayer_temp['user']['last_name'] = $tplayer->user->last_name;
                            $tplayer_temp['user']['fullname'] = $tplayer->user->first_name.' '.$tplayer->user->last_name;
                            if(!empty($tplayer->user->image)){
                                $tplayer_temp['user']['image_url'] = env('APP_MEDIA_PATH').$tplayer->user->image;
                            }else{
                                $tplayer_temp['user']['image_url'] = env('APP_MEDIA_PATH').'default-player.png';
                            }

                            $tplayer_temp['team']['id'] = $tplayer->team->id;
                            $tplayer_temp['team']['name'] = $tplayer->team->name;
                            if (!empty($tplayer->team->image)) {
                                $tplayer_temp['team']['image_url'] = env('APP_MEDIA_PATH').$tplayer->team->image;
                            }else{
                                $tplayer_temp['team']['image_url'] = env('APP_MEDIA_PATH').'default-team.png';
                            }

                            $data[] = $tplayer_temp;
                        }

                        foreach ($match->match_player_is_guest->where('team_id', $match->team2_id) as $tguest) {
                            $tguest_temp = null;
                            
                            $tguest_temp['team_id'] = $tguest->team_id;
                            $tguest_temp['player_id'] = null;

                            $tguest_temp['match_player_id'] = $tguest->id;
                            $tplayer_temp['match_player']['player_id'] = null;
                            $tguest_temp['match_player']['guest_name'] = $tguest->guest_name;
                            $tguest_temp['match_player']['match_id'] = $tguest->match_id;
                            $tguest_temp['match_player']['team_id'] = $tguest->team_id;
                            $tguest_temp['match_player']['position_id'] = $tguest->position_id;
                            $tguest_temp['match_player']['rating'] = $tguest->rating;
                            if(!empty($tguest->saving)){
                                $tguest_temp['match_player']['saving'] = $tguest->saving;
                            }

                            $tguest_temp['match_player']['id'] = $tguest->id;

                            $tguest_temp['team']['id'] = $tguest->team->id;
                            $tguest_temp['team']['name'] = $tguest->team->name;
                            if (!empty($tguest->team->image)) {
                                $tguest_temp['team']['image_url'] = env('APP_MEDIA_PATH').$tguest->team->image;
                            }else{
                                $tguest_temp['team']['image_url'] = env('APP_MEDIA_PATH').'default-team.png';
                            }

                            $data[] = $tguest_temp;
                        }
                    }
                }
            }
        }

        return response()->json($data, 200);
    }

    /* postMatchPlayerWithTeamPlayerDetails (New)
        ** match_player_id null yada boş ise maça yeni oyuncu ekler
        ** match_player_id getMatchPlayerWithTeamPlayerDetails den gelen ile doldurulmuş ise oyuncuyu günceller
        ** daha önce eklenmiş match_player_id gönderilmezse oyuncuyu maçtan siler.

        ** return getMatchPlayerWithTeamPlayerDetails ile aynıdır.
    */
    public function setAppTMPlayer($match_id, $team_id = null, Request $request)
    {
        $data = array();

        $match = Match::find($match_id);
        $r_match_players = collect($request->input());
        if(!empty($team_id)){
            $r_match_players = $r_match_players->where('team_id', $team_id);
        }
        $temp_r_match_players_id = array();
        
        foreach ($r_match_players as $match_player) {

            if(empty($match_player['match_player_id'])){
                //dump('ekle');
                if (!empty($match_player['player_id'])) {
                    $model = new MatchPlayer();
                    $model->player_id = $match_player['player_id'];
                } elseif (!empty($match_player['guest_name'])) {
                    $model = new MatchPlayer();
                    $model->guest_name = $match_player['guest_name'];
                }
            }else{
                //dump('duzenle');
                $model = MatchPlayer::where('id', $match_player['match_player_id'])->first();
            }

            $model->match_id = intval($match_id);
            $model->team_id = $match_player['team_id'];
            $model->position_id = $match_player['position'];
            $model->rating = $match_player['rating'];
            if(!empty($match_player['saving'])){
                $model->saving = $match_player['saving'];
            }

            $model->save();
            $temp_r_match_players_id[] = $model->id;
        }


        //dump($temp_r_match_players_id);

        $filtered = array_where($temp_r_match_players_id, function ($value, $key) {
            return !empty($value);
        });
        
        //dump($filtered);
        //dump($match->match_player->whereNotIn('id', $filtered));

        $mfmp = $match->match_player->whereNotIn('id', $filtered);
        if(!empty($team_id)){
            $mfmp = $mfmp->where('team_id', $team_id);
        }

        foreach ($mfmp as $match_player) {
            $match_player->delete();
            //dump('sil');
        }


        if(!empty($match)){
            
            if(!empty($match->team1_id)){
                if(!empty($match->team1)){
                    if(empty($team_id) || (!empty($team_id) && $match->team1_id == $team_id)){
                        foreach ($match->team1->player as $tplayer) {
                            $tplayer_temp = null;
                            
                            $tplayer_temp['team_id'] = $tplayer->team_id;
                            $tplayer_temp['player_id'] = $tplayer->id;
                            
                            $match_player = $match->match_player_is_not_guest->where('player_id', $tplayer->id)->first();
                            
                            if (!empty($match_player)){

                                $tplayer_temp['match_player_id'] = $match_player->id;
                                $tplayer_temp['match_player']['player_id'] = $match_player->player_id;
                                $tplayer_temp['match_player']['guest_name'] = null;
                                $tplayer_temp['match_player']['match_id'] = $match_player->match_id;
                                $tplayer_temp['match_player']['team_id'] = $match_player->team_id;
                                $tplayer_temp['match_player']['position_id'] = $match_player->position_id;
                                $tplayer_temp['match_player']['rating'] = $match_player->rating;

                                if(!empty($match_player->saving)){
                                    $tplayer_temp['match_player']['saving'] = $match_player->saving;
                                }

                                $tplayer_temp['match_player']['id'] = $match_player->id;
                            }else{
                                $tplayer_temp['match_player_id'] = null;
                            }

                            $tplayer_temp['user']['first_name'] = $tplayer->user->first_name;
                            $tplayer_temp['user']['last_name'] = $tplayer->user->last_name;
                            $tplayer_temp['user']['fullname'] = $tplayer->user->first_name.' '.$tplayer->user->last_name;
                            if(!empty($tplayer->user->image)){
                                $tplayer_temp['user']['image_url'] = env('APP_MEDIA_PATH').$tplayer->user->image;
                            }else{
                                $tplayer_temp['user']['image_url'] = env('APP_MEDIA_PATH').'default-player.png';
                            }

                            $tplayer_temp['team']['id'] = $tplayer->team->id;
                            $tplayer_temp['team']['name'] = $tplayer->team->name;
                            if (!empty($tplayer->team->image)) {
                                $tplayer_temp['team']['image_url'] = env('APP_MEDIA_PATH').$tplayer->team->image;
                            }else{
                                $tplayer_temp['team']['image_url'] = env('APP_MEDIA_PATH').'default-team.png';
                            }

                            $data[] = $tplayer_temp;
                        }

                        foreach ($match->match_player_is_guest->where('team_id', $match->team1_id) as $tguest) {
                            $tguest_temp = null;
                            
                            $tguest_temp['team_id'] = $tguest->team_id;
                            $tguest_temp['player_id'] = null;

                            $tguest_temp['match_player_id'] = $tguest->id;
                            $tplayer_temp['match_player']['player_id'] = null;
                            $tguest_temp['match_player']['guest_name'] = $tguest->guest_name;
                            $tguest_temp['match_player']['match_id'] = $tguest->match_id;
                            $tguest_temp['match_player']['team_id'] = $tguest->team_id;
                            $tguest_temp['match_player']['position_id'] = $tguest->position_id;
                            $tguest_temp['match_player']['rating'] = $tguest->rating;
                            if(!empty($tguest->saving)){
                                $tguest_temp['match_player']['saving'] = $tguest->saving;
                            }

                            $tguest_temp['match_player']['id'] = $tguest->id;

                            $tguest_temp['team']['id'] = $tguest->team->id;
                            $tguest_temp['team']['name'] = $tguest->team->name;
                            if (!empty($tguest->team->image)) {
                                $tguest_temp['team']['image_url'] = env('APP_MEDIA_PATH').$tguest->team->image;
                            }else{
                                $tguest_temp['team']['image_url'] = env('APP_MEDIA_PATH').'default-team.png';
                            }

                            $data[] = $tguest_temp;
                        }
                    }
                }
            }

            if(!empty($match->team2_id)){
                if(!empty($match->team2)){
                    if(empty($team_id) || (!empty($team_id) && $match->team2_id == $team_id)){
                        foreach ($match->team2->player as $tplayer) {
                            $tplayer_temp = null;
                            
                            $tplayer_temp['team_id'] = $tplayer->team_id;
                            $tplayer_temp['player_id'] = $tplayer->id;
                            
                            $match_player = $match->match_player_is_not_guest->where('player_id', $tplayer->id)->first();
                            
                            if (!empty($match_player)){

                                $tplayer_temp['match_player_id'] = $match_player->id;
                                $tplayer_temp['match_player']['player_id'] = $match_player->player_id;
                                $tplayer_temp['match_player']['guest_name'] = null;
                                $tplayer_temp['match_player']['match_id'] = $match_player->match_id;
                                $tplayer_temp['match_player']['team_id'] = $match_player->team_id;
                                $tplayer_temp['match_player']['position_id'] = $match_player->position_id;
                                $tplayer_temp['match_player']['rating'] = $match_player->rating;

                                if(!empty($match_player->saving)){
                                    $tplayer_temp['match_player']['saving'] = $match_player->saving;
                                }

                                $tplayer_temp['match_player']['id'] = $match_player->id;
                            }else{
                                $tplayer_temp['match_player_id'] = null;
                            }

                            $tplayer_temp['user']['first_name'] = $tplayer->user->first_name;
                            $tplayer_temp['user']['last_name'] = $tplayer->user->last_name;
                            $tplayer_temp['user']['fullname'] = $tplayer->user->first_name.' '.$tplayer->user->last_name;
                            if(!empty($tplayer->user->image)){
                                $tplayer_temp['user']['image_url'] = env('APP_MEDIA_PATH').$tplayer->user->image;
                            }else{
                                $tplayer_temp['user']['image_url'] = env('APP_MEDIA_PATH').'default-player.png';
                            }

                            $tplayer_temp['team']['id'] = $tplayer->team->id;
                            $tplayer_temp['team']['name'] = $tplayer->team->name;
                            if (!empty($tplayer->team->image)) {
                                $tplayer_temp['team']['image_url'] = env('APP_MEDIA_PATH').$tplayer->team->image;
                            }else{
                                $tplayer_temp['team']['image_url'] = env('APP_MEDIA_PATH').'default-team.png';
                            }

                            $data[] = $tplayer_temp;
                        }

                        foreach ($match->match_player_is_guest->where('team_id', $match->team2_id) as $tguest) {
                            $tguest_temp = null;
                            
                            $tguest_temp['team_id'] = $tguest->team_id;
                            $tguest_temp['player_id'] = null;

                            $tguest_temp['match_player_id'] = $tguest->id;
                            $tplayer_temp['match_player']['player_id'] = null;
                            $tguest_temp['match_player']['guest_name'] = $tguest->guest_name;
                            $tguest_temp['match_player']['match_id'] = $tguest->match_id;
                            $tguest_temp['match_player']['team_id'] = $tguest->team_id;
                            $tguest_temp['match_player']['position_id'] = $tguest->position_id;
                            $tguest_temp['match_player']['rating'] = $tguest->rating;
                            if(!empty($tguest->saving)){
                                $tguest_temp['match_player']['saving'] = $tguest->saving;
                            }

                            $tguest_temp['match_player']['id'] = $tguest->id;

                            $tguest_temp['team']['id'] = $tguest->team->id;
                            $tguest_temp['team']['name'] = $tguest->team->name;
                            if (!empty($tguest->team->image)) {
                                $tguest_temp['team']['image_url'] = env('APP_MEDIA_PATH').$tguest->team->image;
                            }else{
                                $tguest_temp['team']['image_url'] = env('APP_MEDIA_PATH').'default-team.png';
                            }

                            $data[] = $tguest_temp;
                        }
                    }
                }
            }
        }

        return response()->json($data, 200);
    }

    /* getMatchStatus (New)
        ** Maça özel tanımlanmıştır. 
        ** match_status smallint. default value = 0;
        ** hangi sayı hangi duruma denk geliyor anlamında bir tanımlama yapmadım. Kurguya göre istediğin gibi kullanabilirsin.
    */
    public function getAppMatchStatus($match_id, Request $request)
    {
        
        $data = array();
        $match = Match::find($match_id);

        $data['match_status'] = (int)$match->app_status;

        return response()->json($data, 200);
    }

    /* setMatchStatus (New)
        ** match_status smallint. default value = 0;
        ** return getMatchStatus ile aynıdır.
    */
    public function setAppMatchStatus($match_id, Request $request)
    {
        $data = array();
        
        $match = Match::find($match_id);
        $match_status = (int)$request->input('match_status');
        $match->app_status = $match_status;
        $match->save();

        $model = MatchVideo::where('match_id', $match_id)->first();
        if((int)$request->input('match_status') == 1 || (int)$request->input('match_status') == 2 || (int)$request->input('match_status') == 3){
            $model->status = 'recording';
        }else if((int)$request->input('match_status') == 4){
            $model->status = 'recorded';
        }else if((int)$request->input('match_status') == 5){
            $model->status = 'finished';
        }
        $model->save();

        $data['match_status'] = (int)$match->app_status;

        if((int)$request->input('match_status') == 5){
            $this->setAppMatchCompleted($match_id, $request);
        }

        return response()->json($match_status, 200);
    }

    /* getAllPlayerPositions (New)
        ** Sisteme kaydedilmiş tüm oyuncu pozisyonlarını döndürür.
    */
    public function getAppAllPositions()
    {
        $data = array();

        $positions = PlayerPosition::orderBy('order')->get();

        foreach ($positions as $position) {
            $temp_pos = array();

            $temp_pos['id'] = $position->id;
            $temp_pos['name'] = $position->name;
            $temp_pos['code'] = $position->code;

            $data[] = $temp_pos;
        }
        return response()->json($data, 200);
    }


    /* getMatchAction (New)
        ** Maç aksiyonlarını döndürür.
    */
    public function getAppMatchAction($match_id, Request $request)
    {
        $data = array();
        $match = Match::find($match_id);
        $match_actions = $match->match_action_order_minute;

        foreach ($match_actions as $match_action) {
            $temp_act = array();
            //$temp_act = $match_action;

            $temp_act['id'] = $match_action->id;
            $temp_act['type'] = $match_action->type;
            $temp_act['minute'] = $match_action->minute;

            $temp_act['match_id'] = $match_action->match_id;
            $temp_act['match']['id'] = $match_action->match->id;
            $temp_act['match']['date'] = $match_action->match->date;
            $temp_act['match']['time'] = $match_action->match->time;
            $temp_act['match']['ground_id'] = $match_action->match->ground_id;
            $temp_act['match']['referee_id'] = $match_action->match->referee_id;
            $temp_act['match']['season_id'] = $match_action->match->season_id;
            $temp_act['match']['team1_id'] = $match_action->match->team1_id;
            $temp_act['match']['team2_id'] = $match_action->match->team2_id;

            $temp_act['team_id'] = $match_action->team_id;
            $temp_act['team']['id'] = $match_action->match_team->id;
            $temp_act['team']['name'] = $match_action->match_team->name;
            if (!empty($match_action->match_team->image)) {
                $temp_act['team']['image'] = env('APP_MEDIA_PATH').$match_action->match_team->image;
            }else{
                $temp_act['team']['image'] = env('APP_MEDIA_PATH').'default-team.png';
            }

            $temp_act['match_player_id'] = $match_action->match_player_id;

            if(!empty($match_action->match_player)){
                $temp_act['match_player']['player_id'] = $match_action->match_player->player_id;
                $temp_act['match_player']['guest_name'] = $match_action->match_player->guest_name;
                $temp_act['match_player']['match_id'] = $match_action->match_player->match_id;
                $temp_act['match_player']['team_id'] = $match_action->match_player->team_id;
                $temp_act['match_player']['position_id'] = $match_action->match_player->position_id;
                $temp_act['match_player']['rating'] = $match_action->match_player->rating;
                $temp_act['match_player']['id'] = $match_action->match_player->id;

                if(!empty($match_action->match_player->player)){
                    $temp_act['user']['first_name'] = $match_action->match_player->player->user->first_name;
                    $temp_act['user']['last_name'] = $match_action->match_player->player->user->last_name;
                    $temp_act['user']['fullname'] = $match_action->match_player->player->user->first_name.' '.$match_action->match_player->player->user->last_name;
                    if(!empty($match_action->match_player->player->user->image)){
                        $temp_act['user']['image_url'] = env('APP_MEDIA_PATH').$match_action->match_player->player->user->image;
                    }else{
                        $temp_act['user']['image_url'] = env('APP_MEDIA_PATH').'default-player.png';
                    }
                }
            }

            $temp_act['embed_source'] = $match_action->embed_source;
            $temp_act['embed_code'] = $match_action->embed_code;
            $temp_act['seconds'] = $match_action->seconds;
            $temp_act['action_time'] = $match_action->action_time;


            $data[] = $temp_act;
        }
        

        return response()->json($data, 200);
    }

    /* setMatchAction (New)
        ** Maç aksiyonları ekler.
        ** type, minute, team_id zorunlu alanlardır.
        ** match_player_id, embed_code opsiyoneldir.
        ** id null veya boş ise yeni aksiyon ekler. 
        ** id dolu ise mevcut aksiyonu günceller.
        ** aksiyon silme işlemi yapmaz.
        ** return getAppMatchAction ile aynıdır.
    */
    public function setAppMatchAction($match_id, Request $request)
    {
        $data = array();
        $match = Match::find($match_id);
        $match_actions = collect($request->input());
        $match_actions_id = array();

        foreach ($match_actions as $match_action) {

            if(!empty($match_action['id'])){
                $model = MatchAction::find($match_action['id']);
            }else{
                $model = new MatchAction();
            }

            $model->match_id = intval($match_id);

            if(!empty($match_action['id'])){
                if(!empty($match_action['team_id'])){
                    $model->team_id = $match_action['team_id'];
                }
                if(!empty($match_action['type'])){
                    $model->type = $match_action['type'];
                }
                if(!empty($match_action['minute'])){
                    $model->minute = $match_action['minute'];
                }
            }else{
                $model->team_id = $match_action['team_id'];
                $model->type = $match_action['type'];
                $model->minute = $match_action['minute'];
            }
            
            if(!empty($match_action['match_player_id'])){
                $model->match_player_id = $match_action['match_player_id'];
            }

            if(!empty($match_action['seconds'])){
                $model->seconds = $match_action['seconds'];
            }
            if(!empty($match_action['action_time'])){
                $model->action_time = Carbon::parse(str_replace('/', '.', $match_action['action_time']))->format('Y-m-d H:i:s.uO');
            }
            
            if(!empty($match_action['embed_code'])){
                $model->embed_source = $match_action['embed_source'];
                $model->embed_code = $match_action['embed_code'];
            }

            $model->save();
            $match_actions_id[] = $model->id;
        }

        $match_actions = $match->match_action_order_minute->whereIn('id', $match_actions_id);
        foreach ($match_actions as $match_action) {
            $temp_act = array();
            //$temp_act = $match_action;

            $temp_act['id'] = $match_action->id;
            $temp_act['type'] = $match_action->type;
            $temp_act['minute'] = $match_action->minute;

            $temp_act['match_id'] = $match_action->match_id;
            $temp_act['match']['id'] = $match_action->match->id;
            $temp_act['match']['date'] = $match_action->match->date;
            $temp_act['match']['time'] = $match_action->match->time;
            $temp_act['match']['ground_id'] = $match_action->match->ground_id;
            $temp_act['match']['referee_id'] = $match_action->match->referee_id;
            $temp_act['match']['season_id'] = $match_action->match->season_id;
            $temp_act['match']['team1_id'] = $match_action->match->team1_id;
            $temp_act['match']['team2_id'] = $match_action->match->team2_id;

            $temp_act['team_id'] = $match_action->team_id;
            $temp_act['team']['id'] = $match_action->match_team->id;
            $temp_act['team']['name'] = $match_action->match_team->name;
            if (!empty($match_action->match_team->image)) {
                $temp_act['team']['image'] = env('APP_MEDIA_PATH').$match_action->match_team->image;
            }else{
                $temp_act['team']['image'] = env('APP_MEDIA_PATH').'default-team.png';
            }

            $temp_act['match_player_id'] = $match_action->match_player_id;

            if(!empty($match_action->match_player)){
                $temp_act['match_player']['player_id'] = $match_action->match_player->player_id;
                $temp_act['match_player']['guest_name'] = $match_action->match_player->guest_name;
                $temp_act['match_player']['match_id'] = $match_action->match_player->match_id;
                $temp_act['match_player']['team_id'] = $match_action->match_player->team_id;
                $temp_act['match_player']['position_id'] = $match_action->match_player->position_id;
                $temp_act['match_player']['rating'] = $match_action->match_player->rating;
                $temp_act['match_player']['id'] = $match_action->match_player->id;

                if(!empty($match_action->match_player->player)){
                    $temp_act['user']['first_name'] = $match_action->match_player->player->user->first_name;
                    $temp_act['user']['last_name'] = $match_action->match_player->player->user->last_name;
                    $temp_act['user']['fullname'] = $match_action->match_player->player->user->first_name.' '.$match_action->match_player->player->user->last_name;
                    if(!empty($match_action->match_player->player->user->image)){
                        $temp_act['user']['image_url'] = env('APP_MEDIA_PATH').$match_action->match_player->player->user->image;
                    }else{
                        $temp_act['user']['image_url'] = env('APP_MEDIA_PATH').'default-player.png';
                    }
                }
            }

            $temp_act['embed_source'] = $match_action->embed_source;
            $temp_act['embed_code'] = $match_action->embed_code;
            $temp_act['seconds'] = $match_action->seconds;
            $temp_act['action_time'] = $match_action->action_time;


            $data[] = $temp_act;
        }

        return response()->json($data, 200);
    }

    /* deleteMatchAction (New)
        ** Maç aksiyonunu siler.
        ** return getAppMatchAction ile aynıdır.
    */
    public function deleteAppMatchAction($match_id, $action_id, Request $request)
    {
        $data = array();

        $match = Match::find($match_id);
        $model = MatchAction::where('id', $action_id)->where('match_id', $match_id);
        $model->delete();


        $match_actions = $match->match_action_order_minute;
        foreach ($match_actions as $match_action) {
            $temp_act = array();
            //$temp_act = $match_action;

            $temp_act['id'] = $match_action->id;
            $temp_act['type'] = $match_action->type;
            $temp_act['minute'] = $match_action->minute;

            $temp_act['match_id'] = $match_action->match_id;
            $temp_act['match']['id'] = $match_action->match->id;
            $temp_act['match']['date'] = $match_action->match->date;
            $temp_act['match']['time'] = $match_action->match->time;
            $temp_act['match']['ground_id'] = $match_action->match->ground_id;
            $temp_act['match']['referee_id'] = $match_action->match->referee_id;
            $temp_act['match']['season_id'] = $match_action->match->season_id;
            $temp_act['match']['team1_id'] = $match_action->match->team1_id;
            $temp_act['match']['team2_id'] = $match_action->match->team2_id;

            $temp_act['team_id'] = $match_action->team_id;
            $temp_act['team']['id'] = $match_action->match_team->id;
            $temp_act['team']['name'] = $match_action->match_team->name;
            if (!empty($match_action->match_team->image)) {
                $temp_act['team']['image'] = env('APP_MEDIA_PATH').$match_action->match_team->image;
            }else{
                $temp_act['team']['image'] = env('APP_MEDIA_PATH').'default-team.png';
            }

            $temp_act['match_player_id'] = $match_action->match_player_id;

            if(!empty($match_action->match_player)){
                $temp_act['match_player']['player_id'] = $match_action->match_player->player_id;
                $temp_act['match_player']['guest_name'] = $match_action->match_player->guest_name;
                $temp_act['match_player']['match_id'] = $match_action->match_player->match_id;
                $temp_act['match_player']['team_id'] = $match_action->match_player->team_id;
                $temp_act['match_player']['position_id'] = $match_action->match_player->position_id;
                $temp_act['match_player']['rating'] = $match_action->match_player->rating;
                $temp_act['match_player']['id'] = $match_action->match_player->id;

                if(!empty($match_action->match_player->player)){
                    $temp_act['user']['first_name'] = $match_action->match_player->player->user->first_name;
                    $temp_act['user']['last_name'] = $match_action->match_player->player->user->last_name;
                    $temp_act['user']['fullname'] = $match_action->match_player->player->user->first_name.' '.$match_action->match_player->player->user->last_name;
                    if(!empty($match_action->match_player->player->user->image)){
                        $temp_act['user']['image_url'] = env('APP_MEDIA_PATH').$match_action->match_player->player->user->image;
                    }else{
                        $temp_act['user']['image_url'] = env('APP_MEDIA_PATH').'default-player.png';
                    }
                }
            }

            $temp_act['embed_source'] = $match_action->embed_source;
            $temp_act['embed_code'] = $match_action->embed_code;


            $data[] = $temp_act;
        }

        return response()->json($data, 200);
    }



    public function setAppMatchLiveVideo($match_id, Request $request)
    {
        $data = array();

        $match_videos = collect($request->input());

        $model = MatchVideo::where('match_id', $match_id)->first();
        
        if(!empty($match_videos['live_embed_code'])){
            $model->live_source = $match_videos['live_source'];
            $model->live_embed_code = $match_videos['live_embed_code'];
            
            $model->full_source = $match_videos['live_source'];
            $model->full_embed_code = $match_videos['live_embed_code'];
        }
        
        $model->save();

        $data = $match_videos;

        return response()->json($data, 200);
    }

    public function setAppMatchSummaryVideo($match_id, Request $request)
    {
        $data = array();

        $match_videos = collect($request->input());

        $model = MatchVideo::where('match_id', $match_id)->first();

        if (!empty($match_videos['summary_embed_code'])) {
            $model->summary_source = $match_videos['summary_source'];
            $model->summary_embed_code = $match_videos['summary_embed_code'];
        }
        
        $model->save();

        $data = $match_videos;

        return response()->json($data, 200);
    }


    function cropImages($folder, $image_url, Request $request){

        if(!empty($request->d)){
            $img_d = (int)$request->d;
        }else{
            $img_d = 50;
        }

        $conf = array('width' => $img_d, 'height' => $img_d, 'aspectRatio' => 0, 'widen' => true, 'heighten' => true, 'crop' => true);
        //http://localhost:8000/cropimage/team-logos/img_20220217_173738_814_1UKl2.jpg

        if(file_exists(env('MEDIA_PATH').$folder.'/'.$image_url)){
            $imageObj = Image::make(env('MEDIA_PATH').$folder.'/'.$image_url);    
        }else{

            if($folder == 'team-logos'){
                $imageObj = Image::make(env('MEDIA_PATH').'default-team.png');
            }else{
                $imageObj = Image::make(env('MEDIA_PATH').'default-player.png');
            }
            
        }

        
                

        if($imageObj->width() >= $imageObj->height()) {
            if ($conf['heighten']) {
                $imageObj->heighten($conf['height'], function ($constraint) {
                    $constraint->upsize();
                });
            }

            $crop_x = ($imageObj->width() - $img_d) / 2;
            $crop_y = 0;

        } else {
            if ($conf['widen']) {
                $imageObj->widen($conf['width'], function ($constraint) {
                    $constraint->upsize();
                });
            }

            $crop_x = 0;
            $crop_y = ($imageObj->height() - $img_d) / 2;
        }

        return $imageObj->crop($img_d, $img_d, (int)$crop_x, (int)$crop_y)->encode('png')->response('png');
    }

    public function plaPhoto($id)
    {
        $data = array();

        $player = Player::find($id);
        $user = $player->user;

        $data['location'] = env('MEDIA_END').$user->image;
        $data['player_id'] = $id;
        $data['status'] = 'success';
        
        return response()->json($data, 200);
    }

    public function plaPhotoSave($id, Request $request)
    {
        $data = array();

        $conf = array('width' => 200, 'height' => 200, 'aspectRatio' => 0, 'widen' => true, 'heighten' => true, 'crop' => true);
        $photo = $request->file('file');
        
        $fileName = str_slug(str_before($photo->getClientOriginalName(), '.'), '_').'_'.str_random(5).'.'.$photo->getClientOriginalExtension();
        $imageObj = Image::make($photo);

        if ($conf['widen']) {
            $imageObj->widen($conf['width'], function ($constraint) {
                $constraint->upsize();
            });
        }

        $ext = $photo->getClientOriginalExtension();
        if($ext == "png"){
            $imageObj->save(env('MEDIA_PATH').'user-images/'.$fileName, 93);
        }else{
            $imageObj->save(env('MEDIA_PATH').'user-images/'.$fileName, 93, 'jpg');
        }

        $player = Player::find($id);

        $user = $player->user;
        $user->image = 'user-images/'.$fileName;
        $user->save();

        $data['location'] = env('MEDIA_END').$user->image;
        $data['player_id'] = $id;
        $data['status'] = 'success';
        
        return response()->json($data, 200);
    }

    



}
