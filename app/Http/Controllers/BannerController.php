<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Validator;

use Carbon\Carbon;
use App\User;

use App\Season;

use App\PlayerPosition;
use App\PointType;
use App\Tactic;
use App\TeamPotential;
use App\TeamSatisfaction;
use App\TeamSatisfactionJogo;

use App\EliminationTree;
use App\EliminationTreesTeam;
use App\EliminationTreeItem;

use App\Group;
use App\GroupTeam;

use App\Match;
use App\MatchVideo;
use App\TeamTeamSeason;
use App\Banner;

class BannerController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function banners()
    {
        $text = "Bu sayfaya ulaşmak için yetkiniz bulunmuyor!";
        if(request()->segment(1) == 'banners' && !Auth::user()->hasPermissionTo('add_banner')){
            return redirect('/dashboard')->with('message', array('text' => $text, 'status' => 'error'));
        }
        return view('dashboard');
    }

    public function banner_crud($id = null)
    {
        $text = "Bu sayfaya ulaşmak için yetkiniz bulunmuyor!";
        if((request()->segment(2) == 'edit' && !Auth::user()->hasPermissionTo('add_banner'))){
            return redirect('/banners')->with('message', array('text' => $text, 'status' => 'error'));
        }
        $banner = NULL;
        if(!is_null($id)){
            $banner = Banner::find($id);
        }
        return view('dashboard', array('model_data' => $banner));
    }

    public function banner_save(Request $request)
    {
        //dd($request->input());
        $text = "";
        if ($request->crud == 'delete') {
        }else{

            /* ADD - EDIT */
            if($request->crud == 'edit'){
                $model = Banner::find($request->id);
            }

            $rules = array();
            $formConfig = config('forms.'.$request->segment);

            if(!is_null($formConfig)){
                foreach ($formConfig as $key => $value){
                    if(!empty(data_get($value, 'validation'))){
                        $rules[$key] = data_get($value, 'validation');
                    }
                }
            }
            
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
               return \Redirect::back()->withErrors($validator)->withInput();
            }

            $model->name = $request->name;
            $model->url = $request->url;
            $model->save();

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $text = 'Başarıyla Eklendi...';
            }else if($request->crud == 'edit'){
                $text = 'Başarıyla Güncellendi...';
            }
        }

        return redirect($request->segment)->with('message', array('text' => $text, 'status' => 'success'));
    }

    public function bannerPhotoDelete($id) {
        $banner = Banner::find($id);
        $banner->image = null;
        $banner->save();

        $text = 'Banner Görseli Başarıyla Silindi...';
        return redirect('banners/edit/'.$id.'?t=tab_02')->with('message', array('text' => $text, 'status' => 'success'));
    }



}
