<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Validator;

use Carbon\Carbon;
//use App\Role;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

	public function index()
    {
        $text = "Bu sayfaya ulaşmak için yetkiniz bulunmuyor!";
        if(request()->segment(1) == 'roles' && !Auth::user()->hasPermissionTo('view_permission')){
            return redirect('/dashboard')->with('message', array('text' => $text, 'status' => 'error'));
        }
    	return view('dashboard');
    }

    public function crud($id = null)
    {
        $text = "Bu sayfaya ulaşmak için yetkiniz bulunmuyor!";
        if(
            (request()->segment(2) == 'edit' && !Auth::user()->hasPermissionTo('change_permission')) ||
            (request()->segment(2) == 'permission' && !Auth::user()->hasPermissionTo('change_permission')) ||
            (request()->segment(2) == 'delete' && !Auth::user()->hasPermissionTo('delete_permission'))
        ){
            return redirect('/roles')->with('message', array('text' => $text, 'status' => 'error'));
        }

        $role = NULL;
        if(!is_null($id)){
            $role = Role::find($id);
        }
        return view('dashboard', array('model_data' => $role));
    }

    public function save(Request $request)
    {
        //dump($request->input());
        $text = "";
        if ($request->crud == 'delete') {

            $model = Role::find($request->id);

            if($model->users->count()){
                $text = 'Bu yetki grubuna sahip kullanıcılar olduğu için silinemedi.';
                return redirect($request->segment)->with('message', array('text' => $text, 'status' => 'error'));    
            }

            if($model->permissions->count()){
                $text = 'Bu yetki grubuna atanmış yetkiler olduğu için silinemedi.';
                return redirect($request->segment)->with('message', array('text' => $text, 'status' => 'error'));    
            }

            $model->delete();
         
            $text = 'Başarıyla Silindi...';
            return redirect($request->segment)->with('message', array('text' => $text, 'status' => 'success'));

        }else{

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $model = new Role();
            }else if($request->crud == 'edit'){
                $model = Role::find($request->id);
            }

            $rules = array();
            $formConfig = config('forms.'.$request->segment);

            if(!is_null($formConfig)){
                foreach ($formConfig as $key => $value){
                    if(!empty(data_get($value, 'validation'))){
                        $rules[$key] = data_get($value, 'validation');
                    }
                }
            }
            
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
               return \Redirect::back()->withErrors($validator)->withInput();
            }

            $model->name = str_slug($request->name, '-');
            $model->guard_name = 'web';
            $model->description = $request->name;
            $model->save();
           
            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $text = 'Başarıyla Eklendi...';
            }else if($request->crud == 'edit'){
                $text = 'Başarıyla Güncellendi...';
            }
        }

        return redirect($request->segment)->with('message', array('text' => $text, 'status' => 'success'));
    }


    public function permission($id = null)
    {
        $role = NULL;
        if(!is_null($id)){
            $role = Role::find($id);
        }
        return view('dashboard', array('model_data' => $role));
    }

    public function permission_save(Request $request)
    {
        
        //$permission = Permission::create(['name' => 'add_banner']);
        
        $role = Role::find($request->input('id'));
        $mevcutPerm2 = $role->permissions->pluck('name')->toArray();
        $yeniPerm2 = $request->input('permissions');

        if(empty($yeniPerm2)){
            $yeniPerm2 = array();
        }

        //dump($role);
        //dump($mevcutPerm2);
        //dd($yeniPerm2);

        foreach ($mevcutPerm2 as $mevcutPerm) {
            if(!in_array($mevcutPerm, $yeniPerm2)){
                //dump($mevcutPerm.' Silinecek.');
                if($role->hasPermissionTo($mevcutPerm)){
                    $role->revokePermissionTo($mevcutPerm);
                }
            }
        }

        foreach ($yeniPerm2 as $yeniPerm) {
            if(!in_array($yeniPerm, $mevcutPerm2)){
                //dump($yeniPerm.' Eklenecek.');
                $role->givePermissionTo($yeniPerm);
            }
        }

        $text = 'Başarıyla Güncellendi...';
        return redirect('roles/permission/'.$request->input('id'))->with('message', array('text' => $text, 'status' => 'success'));
        
    }
    






}