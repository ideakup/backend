<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Validator;

use Carbon\Carbon;
use App\User;

use App\Player;
use App\Team;
use App\Season;
use App\Match;

use App\MatchVideo;
use App\TeamTeamSeason;

use App\Reservation;
use App\ReservationGroundsubscription;
use App\ReservationGroundtable;
use App\ReservationGroundtime;
use App\ReservationOffer;

class ReservationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard');
    }

    public function crud($id = null)
    {
        $reservation = NULL;
        if(!is_null($id)){
            $reservation = Reservation::find($id);
        }
        return view('dashboard', array('model_data' => $reservation));
    }

    public function save(Request $request)
    {
        //dd($request->input());
        $text = "";
        if ($request->crud == 'delete') {

            $model = Reservation::find($request->id);

            foreach ($model->reservation_offer as $reservation_offer) {
                $reservation_offer->delete();
            }

            $model->delete();
            
            $text = 'Başarıyla Silindi...';

        }else{

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $model = new Reservation();
            }else if($request->crud == 'edit'){
                $model = Reservation::find($request->id);
            }

            $rules = array();
            $formConfig = config('forms.'.$request->segment);

            if(!is_null($formConfig)){
                foreach ($formConfig as $key => $value){
                    if(!empty(data_get($value, 'validation'))){
                        $rules[$key] = data_get($value, 'validation');
                    }
                }
            }
            
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
               return \Redirect::back()->withErrors($validator)->withInput();
            }

            $season = Season::find($request->season_id);
            $min_point = 0;
            
            if($season->polymorphic_ctype_id == 33){
                $min_point = $season->league_fixtureseason->min_team_point;
            }else if($season->polymorphic_ctype_id == 34){
                $min_point = $season->league_pointseason->min_team_point;
            }

            $teamSeason = TeamTeamSeason::where('season_id', $request->season_id)->where('team_id', $request->team_id)->first();
            if(empty($teamSeason)){

                $teamSeason = new TeamTeamSeason();

                $teamSeason->squad_locked = false;
                $teamSeason->point = $min_point;
                //$teamSeason->squad_locked_date = ''; 
                $teamSeason->season_id = $request->season_id;
                //$teamSeason->squad_locked_player_id = ''; 
                $teamSeason->team_id = $request->team_id;

                $teamSeason->remaining_transfer_count = $season->allowed_transfer_count;
                $teamSeason->playoff_member = true;
                $teamSeason->drawn = 0;
                $teamSeason->goal_against = 0;
                $teamSeason->goal_for = 0;
                $teamSeason->lost = 0;
                $teamSeason->match_total = 0;
                $teamSeason->won = 0;
                $teamSeason->gk_save = 0;
                $teamSeason->red_card = 0;
                $teamSeason->yellow_card = 0;
                $teamSeason->save();

            }


            if(count(explode(':', $request->time)) == 1){
                $model->ground_table_id = $request->time;
            }else{

                $table = ReservationGroundtable::where('ground_id', $request->input('ground_id'))->
                where('date', Carbon::parse($request->input('date'))->format('Y-m-d'))->
                where('time', $request->input('time'))->first();
                
                if(empty($table)){
                    $table = new ReservationGroundtable();
                    $table->ground_id = $request->input('ground_id');
                    $table->date = Carbon::parse($request->input('date'))->format('Y-m-d');
                    $table->time = $request->input('time');
                    $table->status = 'empty';
                    $table->active = true;
                    $table->save();
                }else{
                    $table->active = true;
                    $table->save();
                }

                $model->ground_table_id = $table->id;

            }

            $model->captain_id = $teamSeason->team->captain_id;
            $model->teamseason_id = $teamSeason->id;
            $model->status = 'open';
            $model->save();

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $text = 'Başarıyla Eklendi...';
            }else if($request->crud == 'edit'){
                $text = 'Başarıyla Güncellendi...';
            }
        }

        return redirect($request->segment)->with('message', array('text' => $text, 'status' => 'success'));
    }


    public function reservation_offer_convert_match(Request $request)
    {
        
        //dd($request->input());
        $text = "";

        $reservation = Reservation::find($request->id);
        if(!empty($request->reservation_offer_id)){
            $reservationOfferTeamID = ReservationOffer::find($request->reservation_offer_id)->team_id;
        }

        $rules = array();
        $formConfig = config('forms.reservation_offer_convert_match');

        if(!is_null($formConfig)){
            foreach ($formConfig as $key => $value){
                if(!empty(data_get($value, 'validation'))){
                    $rules[$key] = data_get($value, 'validation');
                }
            }
        }
        
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
           return \Redirect::back()->withErrors($validator)->withInput();
        }


        $team1_id = $reservation->team_teamseason->team_id;
        $teamSeason1 = TeamTeamSeason::where('season_id', $reservation->team_teamseason->season->id)->where('team_id', $team1_id)->first();
        $match1Count = Match::where('season_id', $reservation->team_teamseason->season->id)->
        where(function ($query) use ($team1_id) {
            $query->where('team1_id', $team1_id)
                  ->orWhere('team2_id', $team1_id);
        })->count();
        
        if(!empty($teamSeason1)){
            if($match1Count >= 5 && !$teamSeason1->squad_locked){
                $text = $teamSeason1->team->name.' takımı 5 maç yapmış. Önce takımı kilitlemelisiniz.';
                return redirect($request->segment)->with('message', array('text' => $text, 'status' => 'error'));
            }
        }


        if(!empty($reservationOfferTeamID)){
            $team2_id = $reservationOfferTeamID;
            $teamSeason2 = TeamTeamSeason::where('season_id', $reservation->team_teamseason->season->id)->where('team_id', $team2_id)->first();
            $match2Count = Match::where('season_id', $reservation->team_teamseason->season->id)->
            where(function ($query) use ($team2_id) {
                $query->where('team1_id', $team2_id)
                      ->orWhere('team2_id', $team2_id);
            })->count();
            
            if(!empty($teamSeason2)){
                if($match2Count >= 5 && !$teamSeason2->squad_locked){
                    $text = $teamSeason2->team->name.' takımı 5 maç yapmış. Önce takımı kilitlemelisiniz.';
                    return redirect($request->segment)->with('message', array('text' => $text, 'status' => 'error'));
                }
            }
        }


        $match = new Match();

        $match->season_id = $reservation->team_teamseason->season->id;
        $match->team1_id = $reservation->team_teamseason->team_id;
        $match->team2_id = (!empty($reservationOfferTeamID)) ? $reservationOfferTeamID : null;
        $match->ground_id = $reservation->reservation_groundtable->ground_id;
        $match->date = Carbon::parse($reservation->reservation_groundtable->date)->format('Y-m-d');
        $match->time = $reservation->reservation_groundtable->time;
        $match->referee_id = $request->referee_id;
        $match->coordinator_id = $request->coordinator_id;

        $match->team1_goal = 0;
        $match->team2_goal = 0;
        $match->team1_point = 0;
        $match->team2_point = 0;
        $match->completed = false;

        $match->save();


        $match_video = MatchVideo::where('match_id', $match->id)->first();
        if (is_null($match_video)) {
            $match_video = new MatchVideo();
            $match_video->match_id = $match->id;
        }

        $match_video->status = 'recordable';
        $match_video->live_source = 'app';
        $match_video->live_embed_code = null;
        $match_video->full_source = 'app';
        $match_video->full_embed_code = null;
        $match_video->summary_source = 'app';
        $match_video->summary_embed_code = null;
        $match_video->save();
        

        $reservation->match_id = $match->id;
        $reservation->status = 'confirmed';
        $reservation->save();

        $season = Season::find($match->season_id);
        $min_point = 0;
        

        if($season->polymorphic_ctype_id == 33){
            $min_point = $season->league_fixtureseason->min_team_point;
        }else if($season->polymorphic_ctype_id == 34){
            $min_point = $season->league_pointseason->min_team_point;
        }

        $teamSeason1 = TeamTeamSeason::where('season_id', $match->season_id)->where('team_id', $match->team1_id)->first();
        if(empty($teamSeason1)){

            $teamSeason = new TeamTeamSeason();

            if($season->polymorphic_ctype_id == 32){ /*elimination*/
                $teamSeason->squad_locked = true;
            }else{
                $teamSeason->squad_locked = false;
            }
                
            $teamSeason->point = $min_point;
            //$teamSeason->squad_locked_date = ''; 
            $teamSeason->season_id = $match->season_id;
            //$teamSeason->squad_locked_player_id = ''; 
            $teamSeason->team_id = $match->team1_id;

            $teamSeason->remaining_transfer_count = $season->allowed_transfer_count;
            $teamSeason->playoff_member = true;
            $teamSeason->drawn = 0;
            $teamSeason->goal_against = 0;
            $teamSeason->goal_for = 0;
            $teamSeason->lost = 0;
            $teamSeason->match_total = 0;
            $teamSeason->won = 0;
            $teamSeason->gk_save = 0;
            $teamSeason->red_card = 0;
            $teamSeason->yellow_card = 0;

            $teamSeason->save();

        }

        if(!empty($match->team2_id)){
            $teamSeason2 = TeamTeamSeason::where('season_id', $match->season_id)->where('team_id', $match->team2_id)->first();
            if(empty($teamSeason2)){

                $teamSeason = new TeamTeamSeason();

                if($season->polymorphic_ctype_id == 32){ /*elimination*/
                    $teamSeason->squad_locked = true;
                }else{
                    $teamSeason->squad_locked = false;
                }
                
                $teamSeason->point = $min_point;
                //$teamSeason->squad_locked_date = ''; 
                $teamSeason->season_id = $match->season_id;
                //$teamSeason->squad_locked_player_id = '';
                $teamSeason->team_id = $match->team2_id;

                $teamSeason->remaining_transfer_count = $season->allowed_transfer_count;
                $teamSeason->playoff_member = true;
                $teamSeason->drawn = 0;
                $teamSeason->goal_against = 0;
                $teamSeason->goal_for = 0;
                $teamSeason->lost = 0;
                $teamSeason->match_total = 0;
                $teamSeason->won = 0;
                $teamSeason->gk_save = 0;
                $teamSeason->red_card = 0;
                $teamSeason->yellow_card = 0;

                $teamSeason->save();

            }
        }

        $text = 'Maç Başarıyla Eklendi...';

        return redirect('matches/edit/'.$match->id)->with('message', array('text' => $text, 'status' => 'success'));
    }

    
}
