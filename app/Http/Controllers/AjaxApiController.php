<?php

namespace App\Http\Controllers;

use App\Classes\SmsClass;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

use Carbon\Carbon;

use App\User;
use App\Player;
use App\Profile;
use App\Province;
use App\Team;
use App\TeamTeamPlayer;
use App\League;
use App\Season;
use App\SeasonBeforeAfter;
use App\Match;
use App\MatchAction;
use App\MatchVideo;
use App\Conference;
use App\TeamTeamSeason;
use App\PointType;
use App\TeamPlayerTeamHistory;
use App\LeagueGalleries;
use App\Agenda;
use App\PlayerPosition;
use App\PressConferences;
use App\PressConferencesPlayers;
use App\Ground;
use App\GroundProperty;
use App\Reservation;
use App\ReservationOffer;
use App\ReservationGroundtable;
use App\ReservationGroundtime;
use App\EliminationTree;
use App\News;
use App\Championships;
use App\Transfer;
use App\TransferSeason;
use App\Penalty;
use App\PlayerPenalty;
use App\Tactic;
use App\TeamSurvey;
use App\Group;
use App\UserRoleHasProvince;
use App\Banner;
use App\Rules;
use App\MounthlyPanorama;

class AjaxApiController extends Controller
{

    public function django_password_create(string $password) 
    {
        $algorithm = "pbkdf2_sha256";
        $iterations = 150000;

        $salt = base64_encode(openssl_random_pseudo_bytes(9));
        //$salt = base64_encode($salt);

        $hash = hash_pbkdf2("SHA256", $password, $salt, $iterations, 0, true);    
        $toDBStr = $algorithm ."$". $iterations ."$". $salt ."$". base64_encode($hash);

        return $toDBStr;
    }
    
    public function me()
    {
        $data = array();

        $user = User::select(
            'id',
            'email',
            'first_name',
            'last_name',
            DB::raw("CONCAT(first_name, ' ', last_name) as fullname"),
            'email',
            'phone',
            'email_confirmed',
            'phone_confirmed',
            'infoshare',
            'image',
            'sosyalligcode'

        )->find(Auth::user()->id);

        $data['user'] = $user->toArray();
        $data['user']['is_player'] = (empty($user->player)) ? false : true;

        $data['user']['is_captain'] = false;
        if(!empty($user->player->team)){
            if($user->player->team->captain_id == $user->player->id){
                $data['user']['is_captain'] = true;
            }
        }

        if(!empty($user->image)){
            $data['user']['image_url'] = env('MEDIA_END').$user->image;
        }else{
            $data['user']['image_url'] = env('MEDIA_END').'default-player.png';
        }

        $data['user']['profile'] = $user->profile->toArray();
        $data['user']['profile']['province'] = (!empty($user->profile->province)) ? $user->profile->province->toArray() : null;
        $data['user']['profile']['district'] = (!empty($user->profile->district)) ? $user->profile->district->toArray() : null;

        if(!empty($user->player)){
            $data['user']['player'] = $user->player->toArray();
            $data['user']['player']['fullname'] = $user->fullname;

            $data['user']['player']['position'] = (!empty($user->player->players_position)) ? $user->player->players_position->toArray() : null;
            $data['user']['player']['position2'] = (!empty($user->player->players_position2)) ? $user->player->players_position2->toArray() : null;
            $data['user']['player']['position3'] = (!empty($user->player->players_position3)) ? $user->player->players_position3->toArray() : null;

            $data['user']['player']['value'] = (int)$user->player->value;
            $data['user']['player']['old_system_value'] = (int)$user->player->old_system_value;
        }

        if (!empty($user->player->team)) {
            $data['user']['player']['team'] = $user->player->team->toArray();            

            if (!empty($user->player->team->image)) {
                $data['user']['player']['team']['image_url'] = env('MEDIA_END').$user->player->team->image;
            }else{
                $data['user']['player']['team']['image_url'] = env('MEDIA_END').'default-team.png';
            }

            $dLeague = $user->player->team->league;
            if(!empty($dLeague->seasonApi)){
                $dSeason = $dLeague->seasonApi->first();
            
                $data['user']['player']['team']['league'] = $dLeague->toArray();
                $data['user']['player']['team']['league']['province'] = $dLeague->league_province->first()->province->toArray();

                $data['user']['player']['team']['league']['season'] = $dSeason->toArray();
                $data['user']['player']['team']['league']['season']['fullname'] = $dSeason->year.' '.$dLeague->name.' '.$dSeason->name;

                if($dSeason->polymorphic_ctype_id == 32){
                    $data['user']['player']['team']['league']['season']['type'] = 'elimination';
                }else if($dSeason->polymorphic_ctype_id == 33){
                    $data['user']['player']['team']['league']['season']['type'] = 'fixture';
                }else if($dSeason->polymorphic_ctype_id == 34){
                    $data['user']['player']['team']['league']['season']['type'] = 'point';
                }
            }
        }

        return response()->json($data, 200);
    }

    public function meUpdate(Request $request)
    {
        $data = array();
        
        $user = Auth::user();

        if(!empty($request->input('first_name'))){
            //$user->first_name = $request->input('first_name');
            //$user->last_name = $request->input('last_name');
            $user->email = $request->input('email');
            $user->phone = $request->input('phone');
            $user->sosyalligcode = $request->input('sosyalligcode');
        }
        $user->save();

        if(!empty($user->player)){
            $player = $user->player;
            $player->instagram = $request->input('player')['instagram'];
            $player->facebook = $request->input('player')['facebook'];
            $player->twitter = $request->input('player')['twitter'];
            $player->weight = $request->input('player')['weight'];
            $player->height = $request->input('player')['height'];
            $player->position_id = $request->input('player')['position_id'];
            $player->position2_id = $request->input('player')['position2_id'];
            $player->position3_id = $request->input('player')['position3_id'];
            $player->number = $request->input('player')['number'];
            $player->foot = $request->input('player')['foot'];
            $player->save();
        }else{
            if(!empty($request->input('player'))){
                $player = new Player();
                $player->user_id = $user->id;
                $player->value = 100000;
                $player->old_system_value = 0;

                $player->instagram = $request->input('player')['instagram'];
                $player->facebook = $request->input('player')['facebook'];
                $player->twitter = $request->input('player')['twitter'];
                $player->weight = $request->input('player')['weight'];
                $player->height = $request->input('player')['height'];
                $player->position_id = $request->input('player')['position_id'];
                $player->position2_id = $request->input('player')['position2_id'];
                $player->position3_id = $request->input('player')['position3_id'];
                $player->number = $request->input('player')['number'];
                $player->foot = $request->input('player')['foot'];
                $player->save();
            }
        }

        if(!empty($user->profile)){
            $profile = $user->profile;

            if(!empty($request->input('profile')['province_id'])){
                $profile->province_id = $request->input('profile')['province_id'];
            }
            if(!empty($request->input('profile')['district_id'])){
                $profile->district_id = $request->input('profile')['district_id'];
            }

            if(!empty($request->input('profile')['identity_number'])){
                $profile->identity_number = $request->input('profile')['identity_number'];
            }
            if(!empty($request->input('profile')['birth_date'])){
                $profile->birth_date = $request->input('profile')['birth_date'];
            }

            $profile->save();
        }

        $user->fullname = $user->first_name.' '.$user->last_name;

        if(!empty($user->image)){
            $user->image_url = env('MEDIA_END').$user->image;
        }else{
            $user->image_url = env('MEDIA_END').'default-player.png';
        }

        $user->is_player = (empty($user->player)) ? false : true;

        $user->is_captain = false;
        if(!empty($user->player->team)){
            if($user->player->team->captain_id == $user->player->id){
                $user->is_captain = true;
            }
        }

        $profile->province;
        $profile->district;

        if(!empty($user->player)){
            $player->fullname = $user->first_name.' '.$user->last_name;

            if(!empty($user->image)){
                $player->image_url = env('MEDIA_END').$user->image;
            }else{
                $player->image_url = env('MEDIA_END').'default-player.png';
            }

            $player->position = $player->players_position;
            $player->position2 = $player->players_position2;
            $player->position3 = $player->players_position3;

            if(!empty($player->team)){
                if (!empty($player->team->image)) {
                    $player->team->image_url = env('MEDIA_END').$player->team->image;
                }else{
                    $player->team->image_url = env('MEDIA_END').'default-team.png';
                }

                $player->team->league;
                $player->team->league->season = null;
                $player->team->league->province = $player->team->league->league_province->first()->province;
            }
        }
        
        return response()->json($user, 200);
    }

    public function sendInfoShare(Request $request)
    {
        $data = array();
        
        $user = Auth::user();

        if(!empty($request->input('infoshare'))){
            $user->infoshare = $request->input('infoshare');
        }
        $user->save();
        
        return response()->json(array('message' => ["TAMAM"]), 200);
    }

    public function phoneConfirm(Request $request)
    {

        $user = User::where('phone', $request->input('number'))->where('phone_code', $request->input('code'))->first();
        if(empty($user)){
            return response()->json(array('message' => ["Girdiğiniz aktivasyon kodu hatalı. Cep telefonunuza gelen aktivasyon kodunu kontrol edip yeniden deneyiniz."]), 400);
        }
        $user->is_active = true;
        $user->phone_confirmed = true;
        $user->phone_code = null;
        $user->save();

        return response()->json(array('message' => ["TAMAM"]), 200);
    }

    public function TCKConfirm(Request $request)
    {
        
        $data['tcno'] = $request->input('profile')['identity_number'];
        $data['isim'] = mb_strtoupper(str_replace('i', 'İ', $request->input('first_name')), 'UTF-8');
        $data['soyisim'] = mb_strtoupper(str_replace('i', 'İ', $request->input('last_name')), 'UTF-8');
        //$data['isim'] = mb_strtoupper($request->input('first_name'), 'UTF-8');
        //$data['soyisim'] = mb_strtoupper($request->input('last_name'), 'UTF-8');
        $data['dogumyili'] = explode('-', $request->input('profile')['birth_date'])[0];

        $post_data = '<?xml version="1.0" encoding="utf-8"?>
        <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
            <soap:Body>
                <TCKimlikNoDogrula xmlns="http://tckimlik.nvi.gov.tr/WS">
                    <TCKimlikNo>'.$data['tcno'].'</TCKimlikNo>
                    <Ad>'.$data['isim'].'</Ad>
                    <Soyad>'.$data['soyisim'].'</Soyad>
                    <DogumYili>'.$data['dogumyili'].'</DogumYili>
                </TCKimlikNoDogrula>
            </soap:Body>
        </soap:Envelope>';

        $ch = curl_init();

        // CURL options
        $options = array(
            CURLOPT_URL             => 'https://tckimlik.nvi.gov.tr/Service/KPSPublic.asmx',
            CURLOPT_POST            => true,
            CURLOPT_POSTFIELDS      => $post_data,
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_SSL_VERIFYPEER  => false,
            CURLOPT_HEADER          => false,
            CURLOPT_HTTPHEADER      => array(
                'POST /Service/KPSPublic.asmx HTTP/1.1',
                'Host: tckimlik.nvi.gov.tr',
                'Content-Type: text/xml; charset=utf-8',
                'SOAPAction: "http://tckimlik.nvi.gov.tr/WS/TCKimlikNoDogrula"',
                'Content-Length: '.strlen($post_data)
            ),
        );
        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        curl_close($ch);

        if(strip_tags($response) === 'true'){
            $confirm = true;
        }else{
            $confirm = false;
        }
        //return $response;
        return response()->json(array('tckconfirm' => $confirm), 200);
    }

    public function hesapKurtarma(Request $request)
    {

        $search = $request->input('search');
        $search = str_replace(" ", "", $search);
        $search = str_replace("  ", "", $search);
        $search = str_replace("+", "", $search);

        $user = User::select('users.email', 'users.username', 'users.phone', 'profiles.identity_number')->
        leftJoin('profiles', 'users.id', '=', 'profiles.user_id')->
        where('users.email', $search)->
        orWhere('users.username', $search)->
        orWhere('users.phone', $search)->
        orWhere('profiles.identity_number', $search)->first();

        if(!empty($user)){
            $emailArr = explode('@', $user->email);
            $emailFirst = substr($emailArr[0], 0, 1);
            $emailLast = substr($emailArr[0], -1, 1);
            $emailChrCount = strlen($emailArr[0]);
            $emailSecret = '';
            for ($i=0; $i < $emailChrCount-2; $i++) { 
                $emailSecret .= '*';
            }
            $userLimit['email'] = $emailFirst.$emailSecret.$emailLast.'@'.$emailArr[1];
            $userLimit['sendEmail'] = $user->email;


            $phoneS = $user->phone;
            if(strlen($user->phone) == 10){
                $phoneS = '0'.$user->phone;
            }elseif(strlen($user->phone) == 11){
                $phoneS = $user->phone;
            }
            $phoneSecret = substr($phoneS, 0, 1).' (*'.substr($phoneS, 2, 1).'*) *** ** *'.substr($phoneS, -1, 1);
            $userLimit['phone'] = $phoneSecret;
            //$userLimit['phone2'] = $user->phone;

            /*
            $identityS = $user->identity_number;
            $identityFirst = substr($identityS, 0, 1);
            $identityEnd = substr($identityS, -1, 1);
            $identityChrCount = strlen($identityS);
            $identitySecret = '';
            for ($i=0; $i < $identityChrCount-2; $i++) { 
                $identitySecret .= '*';
            }
            $userLimit['identity_number'] = $identityFirst.$identitySecret.$identityEnd;
            $userLimit['identity_number2'] = $user->identity_number;
            */
        }

        if(!empty($user)){
            return response()->json($userLimit, 200);
        }else{
            return response()->json(array('message' => ["Bu bilgilerle kayıtlı bir üye bulunamadı..."]), 400);
        }
    }

    public function hesapDogrula(Request $request)
    {

        $birth_date = $request->input('birth_date');
        $identity_number = $request->input('identity_number');

        $user = User::select('users.email', 'users.username', 'users.phone', 'profiles.identity_number')->
        leftJoin('profiles', 'users.id', '=', 'profiles.user_id')->
        where('profiles.birth_date', $birth_date)->
        where('profiles.identity_number', $identity_number)->first();

        if(!empty($user)){
            return response()->json($request->input(), 200);
        }else{
            return response()->json(array('message' => ["Bu bilgilerle kayıtlı bir üye bulunamadı..."]), 400);
        }
    }

    public function userCreate(Request $request)
    {
        $isPlayer = false;
        $emailCount = User::where('username', $request->input('email'))->orWhere('email', $request->input('email'))->count();
        $phoneCount = User::where('phone', $request->input('phone'))->count();

        if(!empty($request->input('profile')['identity_number'])){
            $isPlayer = true;
            $identityNumberCount = Profile::where('identity_number', $request->input('profile')['identity_number'])->count();
        }

        $returnMessage = array();

        if($emailCount > 0){
            $returnMessage[] = array('email' => ["Bu eposta adresi alanına sahip kullanıcı zaten mevcut."]);
        }

        if($phoneCount > 0){
            $returnMessage[] = array('phone' => ["Bu telefon alanına sahip kullanıcı zaten mevcut."]);
        }
        
        if($isPlayer){
            if($identityNumberCount > 0){
                $returnMessage[] = array('identity_number' => ["Bu TC Kimlik alanına sahip kullanıcı zaten mevcut."]);
            }
        }

        if (!empty($returnMessage)) {
            return response()->json($returnMessage, 400);
        }

        $user = new User();

        $user->username = $request->input('email');
        $user->email = $request->input('email');

        $user->first_name = $request->input('first_name');
        $user->last_name = $request->input('last_name');
        $user->is_staff = false;
        $user->is_superuser = false;

        $user->is_active = false; // SMS ONAYLANDIĞINDA TRUE OLACAK;
        $user->infoshare = $request->input('infoshare');

        //$user->last_login = Carbon::now();
        $user->date_joined = Carbon::now();
        $user->created_at = Carbon::now();
        $user->updated_at = Carbon::now();
        $user->image = '';
        $user->phone = $request->input('phone');
        $user->email_confirmed = false; // E MAIL ONAYLANDIĞINDA TRUE OLACAK;
        $user->phone_confirmed = false; // SMS ONAYLANDIĞINDA TRUE OLACAK;

        $user->password = $this->django_password_create($request->input('password'));
        //$user->privacy = $request->input('privacy');
        $user->phone_code = mt_rand(100000, 999000); // SMS ONAYLANDIĞINDA NULL OLACAK;
        $user->save();



        $profile = new Profile();

        $profile->user_id = $user->id;
        $profile->province_id = $request->input('profile')['province_id'];
        $profile->district_id = $request->input('profile')['district_id'];

        if($isPlayer){
            $profile->identity_number = $request->input('profile')['identity_number'];
            $profile->birth_date = $request->input('profile')['birth_date'];
        }
        
        $profile->created_at = Carbon::now();
        $profile->updated_at = Carbon::now();
        $profile->save();


        if($isPlayer){
            $player = new Player();

            $player->user_id = $user->id;
            $player->facebook = $request->input('player')['facebook'];
            $player->twitter = $request->input('player')['twitter'];
            $player->instagram = $request->input('player')['instagram'];
            $player->position_id = $request->input('player')['position_id'];
            $player->position2_id = $request->input('player')['position2_id'];
            $player->position3_id = $request->input('player')['position3_id'];
            $player->weight = $request->input('player')['weight'];
            $player->height = $request->input('player')['height'];
            $player->foot = $request->input('player')['foot'];
            $player->number = $request->input('player')['number'];

            $player->value = 100000;
            $player->old_system_value = 0;

            $player->created_at = Carbon::now();
            $player->updated_at = Carbon::now();
            $player->save();
        }

        //SMS GÖNDER;
        $smsHelper = new SmsClass();
        $smsHelper->smsSend($user->phone, $user->phone_code);

        return response()->json($user, 200);
    }

    public function registerPlayer(Request $request)
    {
        $isPlayer = false;
        $emailCount = User::where('username', $request->input('email'))->orWhere('email', $request->input('email'))->count();
        $phoneCount = User::where('phone', $request->input('phone'))->count();

        if(!empty($request->input('profile')['identity_number'])){
            $isPlayer = true;
            $identityNumberCount = Profile::where('identity_number', $request->input('profile')['identity_number'])->count();
        }

        $returnMessage = array();

        if($emailCount > 0){
            $returnMessage[] = array('email' => ["Bu eposta adresi alanına sahip kullanıcı zaten mevcut."]);
        }

        if($phoneCount > 0){
            $returnMessage[] = array('phone' => ["Bu telefon alanına sahip kullanıcı zaten mevcut."]);
        }
        
        if($isPlayer){
            if($identityNumberCount > 0){
                $returnMessage[] = array('identity_number' => ["Bu TC Kimlik alanına sahip kullanıcı zaten mevcut."]);
            }
        }

        if (!empty($returnMessage)) {
            return response()->json($returnMessage, 400);
        }

        
        $team = Auth::user()->player->team;

        /***** Aktif Sezon gg*****/
        $activeSeason = null;
        $activeSeasons = array();
        $existSeasons = array();
        $t_seasons = Auth::user()->player->team->league->seasonApi;

        foreach ($t_seasons as $t_season) {
            if($t_season->active == true){
                if($t_season->polymorphic_ctype_id != 32){
                    if($t_season->start_date <= Carbon::now()->format('Y-m-d') && $t_season->end_date >= Carbon::now()->format('Y-m-d')){
                        $activeSeasons[] = $t_season;
                    }
                    $existSeasons[] = $t_season;
                }
            }
        }

        if(count($activeSeasons) > 1){
            foreach ($activeSeasons as $season) {
                if($season->polymorphic_ctype_id == 33){

                    $fs_group_teams_id = array();
                    foreach ($season->league_fixtureseason->groups as $group) {
                        foreach ($group->group_teams as $group_team) {
                            $fs_group_teams_id[] = $group_team->team_id;
                        }
                    }
                    if(array_search($team->id, $fs_group_teams_id)){
                        $activeSeason = $season;
                    }

                }
            }
        }else{
            if(count($existSeasons)){
                $activeSeason = $existSeasons[0];
            }
        }

        if(empty($activeSeason)){
            $activeSeason = $activeSeasons[0];
        }elseif(!empty($activeSeason)){

            if(!empty($team->team_teamseason->where('season_id', $activeSeason->id)->first())){
                $remaining_transfer_count = $team->team_teamseason->where('season_id', $activeSeason->id)->first()->remaining_transfer_count;
                $squad_locked = $team->team_teamseason->where('season_id', $activeSeason->id)->first()->squad_locked;
            }else{
                $remaining_transfer_count = $activeSeason->allowed_transfer_count;
                $squad_locked = false;
            }
        }
        /***** Aktif Sezon gg*****/

        if(!empty($activeSeason)){
            $teamSeason = TeamTeamSeason::where('season_id', $activeSeason->id)->where('team_id', Auth::user()->player->team->id)->first();
            if(empty($teamSeason)){
                $teamSeason = new TeamTeamSeason();
                $teamSeason->squad_locked = false;
                $teamSeason->point = $activeSeason->league_pointseason->min_team_point;
                $teamSeason->season_id = $activeSeason->id;
                $teamSeason->team_id = $team->id;
                $teamSeason->remaining_transfer_count = $activeSeason->allowed_transfer_count;
                $teamSeason->playoff_member = true;
                $teamSeason->drawn = 0;
                $teamSeason->goal_against = 0;
                $teamSeason->goal_for = 0;
                $teamSeason->lost = 0;
                $teamSeason->match_total = 0;
                $teamSeason->won = 0;
                $teamSeason->gk_save = 0;
                $teamSeason->red_card = 0;
                $teamSeason->yellow_card = 0;
                $teamSeason->save();
            }

            if($teamSeason->remaining_transfer_count == 0 && $teamSeason->squad_locked){
                $returnMessage[] = array('message' => ["Takımın transfer hakkı bulunmuyor"]);
                return response()->json($returnMessage, 400);
            }

            $matchCount = Match::where('season_id', $activeSeason->id)->where('completed', true)->
                where(function ($query) use ($team) {
                    $query->where('team1_id', $team->id)
                        ->orWhere('team2_id', $team->id);
                })->count();


            if($activeSeason->locking_match_count <= $matchCount && !$teamSeason->squad_locked){
                $returnMessage[] = array('message' => ["Transfer Teklifi gönderilmedi. Önce takım kadrosunu kilitlemelisiniz."]);
                return response()->json($returnMessage, 400);
            }
        }

        $user = new User();

        $user->username = $request->input('email');
        $user->email = $request->input('email');

        $user->first_name = $request->input('first_name');
        $user->last_name = $request->input('last_name');
        $user->is_staff = false;
        $user->is_superuser = false;

        $user->is_active = false; // SMS ONAYLANDIĞINDA TRUE OLACAK;

        //$user->last_login = Carbon::now();
        $user->date_joined = Carbon::now();
        $user->created_at = Carbon::now();
        $user->updated_at = Carbon::now();
        $user->image = '';
        $user->phone = $request->input('phone');
        $user->email_confirmed = false; // E MAIL ONAYLANDIĞINDA TRUE OLACAK;
        $user->phone_confirmed = false; // SMS ONAYLANDIĞINDA TRUE OLACAK;

        $user->password = $this->django_password_create($request->input('password'));
        //$user->privacy = $request->input('privacy');
        //$user->phone_code = mt_rand(100000, 999000); // SMS ONAYLANDIĞINDA NULL OLACAK;
        $user->save();


        $profile = new Profile();

        $profile->user_id = $user->id;
        $profile->province_id = $request->input('profile')['province_id'];
        $profile->district_id = $request->input('profile')['district_id'];

        if($isPlayer){
            $profile->identity_number = $request->input('profile')['identity_number'];
            $profile->birth_date = $request->input('profile')['birth_date'];
        }
        
        $profile->created_at = Carbon::now();
        $profile->updated_at = Carbon::now();
        $profile->save();


        if($isPlayer){
            $player = new Player();

            $player->user_id = $user->id;
            $player->team_id = Auth::user()->player->team->id;
            $player->position_id = $request->input('player')['position_id'];
            $player->position2_id = $request->input('player')['position2_id'];
            $player->position3_id = $request->input('player')['position3_id'];
            $player->weight = $request->input('player')['weight'];
            $player->height = $request->input('player')['height'];
            $player->foot = $request->input('player')['foot'];
            $player->number = $request->input('player')['number'];

            $player->value = 100000;
            $player->old_system_value = 0;

            $player->created_at = Carbon::now();
            $player->updated_at = Carbon::now();
            $player->save();
        }


        $teamTeamPlayerNew = new TeamTeamPlayer();
        $teamTeamPlayerNew->player_id = $player->id;
        $teamTeamPlayerNew->team_id = $player->team_id;
        $teamTeamPlayerNew->joined_date = Carbon::now()->format('Y-m-d');
        $teamTeamPlayerNew->save();

        //SMS GÖNDER;
        //$smsHelper = new SmsClass();
        //$smsHelper->smsSend($user->phone, $user->phone_code);

        return response()->json($user, 200);
        /**/
    }

    public function sendNewSms(Request $request)
    {
        $user = User::where('phone', $request->number)->first();
        $user->phone_code = mt_rand(100000, 999000); // SMS ONAYLANDIĞINDA NULL OLACAK;
        $user->save();
        //SMS GÖNDER;
        $smsHelper = new SmsClass();
        $smsHelper->smsSend($user->phone, $user->phone_code);

        return response()->json($request->number, 200);
    }

    public function positions()
    {
        $data = array();

        $positions = PlayerPosition::all();

        return response()->json($positions, 200);
    }

    public function grounds(Request $request)
    {
        $data = array();

        $grounds = Ground::select('grounds.*');
        
        $page = 1;
        $pageCount = 12;

        if(!empty($request->input('province'))){
            $grounds = $grounds->where('province_id', $request->input('province'));
        }
        
        if(!empty($request->input('district'))){
            $grounds = $grounds->where('district_id', $request->input('district'));
        }
        
        if(!empty($request->input('term'))){
            $grounds->where('name', 'ilike', '%'.$request->input('term').'%');
        }
        
        if(!empty($request->input('properties'))){
            $grounds = $grounds->join('grounds_properties', function ($join) use ($request) {
                $join->on('grounds.id', '=', 'grounds_properties.ground_id')
                     ->whereIn('grounds_properties.property_id', $request->input('properties'));
            });
        }

        $data['count'] = $grounds->count();
        $data['next'] = null;
        $data['previous'] = null;

        if(!empty($request->input('page'))){
            $page = $request->input('page');
            $offset = ($page-1)*$pageCount;
            $grounds = $grounds->offset($offset);
        }
        
        $grounds = $grounds->limit($pageCount)->orderby('id', 'desc')->get();
        $data['results'] = array();
        foreach ($grounds as $ground) {
            $groArr = array();

            $groArr = $ground->toArray();
            
            $groArr['province'] = $ground->province;
            $groArr['district'] = $ground->district;
            $groArr['grounds_properties'] = $ground->grounds_properties;

            $ggimage = $ground->ground_images->first();
            if (!empty($ggimage)) {
                $groArr['image'] = 'https://www.rakipbul.com/'.$ggimage->image;
            }else{
                $groArr['image'] = array();
            }
            
            $data['results'][] = $groArr;
        }

        return response()->json($data, 200);
    }

    public function groundsall(Request $request)
    {
        $grounds = Ground::orderby('id', 'desc')->get();
        $data = array();
        foreach ($grounds as $ground) {
            $groArr = array();

            $groArr = $ground->toArray();
            
            $groArr['province'] = $ground->province;
            $groArr['district'] = $ground->district;
            $groArr['grounds_properties'] = $ground->grounds_properties;

            $ggimage = $ground->ground_images->first();
            if (!empty($ggimage)) {
                $groArr['image'] = $ggimage->image;
            }else{
                $groArr['image'] = array();
            }
            
            $data[] = $groArr;
        }

        return response()->json($data, 200);
    }

    public function groundDetails($slug, Request $request)
    {
        $data = array();

        $ground = Ground::where('slug', $slug)->first();

        if(!empty($ground)){
            $data = $ground->toArray();
            $data['province'] = $ground->province->toArray();
            $data['district'] = $ground->district->toArray();

            $ggprops = $ground->grounds_properties;

            foreach ($ggprops as $ggprop) {
                $gp = $ggprop->ground_property->toArray();

                $data['properties'][] = $gp;
            }

            $ggimages = $ground->ground_images;

            $data['images'] = array();
            foreach ($ggimages as $ggimage) {
                $data['images'][]['image'] = 'https://www.rakipbul.com/'.$ggimage->image;
            }
        }

        return response()->json($data, 200);
    }

    public function groProperties()
    {
        $data = array();

        $groProperties = GroundProperty::orderBy('name')->get();

        return response()->json($groProperties, 200);
    }

    public function news(Request $request)
    {
        $data = array();
        $unlegalType = array(63, 61, 68, 59, 64, 62, 67, 60, 66, 65, 69);

        $_page = $request->input('page');
        $_league = $request->input('league');

        $_page_next_str = '';
        $_page_prev_str = '';
        $_league_str = '';

        $news = News::select('news.*', 'news_leagues.league_id')->join('news_leagues', 'news_leagues.news_id', '=', 'news.id')->whereNotIn('news.type_id', $unlegalType);
        
        if(!empty($_league)){
            $news = $news->where('news_leagues.league_id', $_league);
            $_league_str = 'league='.$_league;
        }
        
        $data['count'] = $news->count();


        $page = 1;
        $pageCount = 12;

        if(!empty($_page)){
            $page = $_page;
            $offset = ($page-1)*$pageCount;
            $news = $news->offset($offset);

            $_page_next_str = 'page='.($page+1);
            $_page_prev_str = 'page='.($page-1);
        }else{
            $_page_next_str = 'page=2';
        }


        $data['next'] = env('APP_URL').'api/news/?'.$_page_next_str.'&'.$_league_str;
        if($page > 1){
            $data['previous'] = env('APP_URL').'api/news/?'.$_page_prev_str.'&'.$_league_str;
        }else{
            $data['previous'] = null;
        }

        
        $news = $news->limit($pageCount)->orderby('news.created_at', 'desc')->get();

        $data['results'] = array();
        foreach ($news as $new) {
            $newArr = array();

            $newArr = $new->toArray();
            $newArr['type'] = $new->news_type->toArray();

            if(!empty($new->photo_gallery_id) && !empty($new->photo_gallery->items->first()->image)){

                $newArr['image'] = env('MEDIA_END').$new->photo_gallery->items->first()->image;
            }

            if(!empty($new->video_gallery_id)){

                if (!empty($new->video_gallery->items->first()->embed_code)) {
                    $e_code = $new->video_gallery->items->first()->embed_code;
                    $e_source = $new->video_gallery->items->first()->embed_source;

                    if ($e_source == 'app'){
                        $newArr['embed_url'] = $e_code;
                        $newArr['image'] = url('default.jpg');
                    }else if ($e_source == 'vimeo'){
                        $newArr['embed_url'] = 'https://player.vimeo.com/video/'.$e_code;
                        $newArr['image'] = url('default.jpg');
                    }else if ($e_source == 'youtube'){
                        $newArr['embed_url'] = 'https://www.youtube.com/embed/'.$e_code;
                        $newArr['image'] = 'https://i.ytimg.com/vi/'.$e_code.'/hqdefault.jpg';
                    }
                }

            }
            
            $data['results'][] = $newArr;
        }

        return response()->json($data, 200);
    }

    public function newsDetail($id, Request $request)
    {
        $data = array();
        $legalType = array(69);

        $_search = $request->input('search');
        $_type = $request->input('type');
        $_page = $request->input('page');

        $_search_str = '';
        $_type_str = '';
        $_page_next_str = '';
        $_page_prev_str = '';

        if(!empty($_type)){
            $legalType = array($_type);
            $_type_str = 'type='.$_type;
        }

        $new = News::select('*')->where('id', $id);//->whereNotNull('photo_gallery_id');
        
        if(!empty($_search)){
            $new = $new->where('name', 'ilike', '%'.$_search.'%');
            $_search_str = 'search='.$_search;
        }

        $new = $new->orderby('created_at', 'desc')->first();

        $newArr = array();

        $newArr['id'] = $new->id;
        $newArr['name'] = $new->name;
        $newArr['slug'] = $new->slug;
        $newArr['subtitle'] = $new->subtitle;
        $newArr['date'] = $new->date;
        $newArr['image'] = $new->image;
        $newArr['content'] = $new->content;

        if(!empty($new->photo_gallery_id)){
            $newArr['image'] = env('MEDIA_END').$new->photo_gallery->items->first()->image;
        }

        if(!empty($new->video_gallery_id)){

            $e_code = $new->video_gallery->items->first()->embed_code;
            $e_source = $new->video_gallery->items->first()->embed_source;

            if ($e_source == 'app'){
                $newArr['embed_url'] = $e_code;
                $newArr['image'] = url('default.jpg');
            }else if ($e_source == 'vimeo'){
                $newArr['embed_url'] = 'https://player.vimeo.com/video/'.$e_code;
                $newArr['image'] = url('default.jpg');
            }else if ($e_source == 'youtube'){
                $newArr['embed_url'] = 'https://www.youtube.com/embed/'.$e_code;
                $newArr['image'] = 'https://i.ytimg.com/vi/'.$e_code.'/mqdefault.jpg';
            }

        }
        
        $data = $newArr;
        
        return response()->json($data, 200);
    }

    public function newsTv(Request $request)
    {
        $data = array();
        $legalType = array(63, 61, 68, 59, 64, 62, 67, 60, 66, 65);

        $_search = $request->input('search');
        $_type = $request->input('type');
        $_page = $request->input('page');

        $_search_str = '';
        $_type_str = '';
        $_page_next_str = '';
        $_page_prev_str = '';

        if(!empty($_type)){
            $legalType = array($_type);
            $_type_str = 'type='.$_type;
        }

        $news = News::select('*')->whereIn('type_id', $legalType);//->whereNotNull('photo_gallery_id');
        
        $page = 1;
        $pageCount = 12;
        
        if(!empty($_search)){
            $news = $news->where('name', 'ilike', '%'.$_search.'%');
            $_search_str = 'search='.$_search;
        }
        
        $data['count'] = $news->count();
        
        if(!empty($_page)){
            $page = $_page;
            $offset = ($page-1)*$pageCount;
            $news = $news->offset($offset);

            $_page_next_str = 'page='.($page+1);
            $_page_prev_str = 'page='.($page-1);
        }else{
            $_page_next_str = 'page=2';
        }


        $data['next'] = env('APP_URL').'api/news/tv/?'.$_page_next_str.'&'.$_search_str.'&'.$_type_str;
        if($page > 1){
            $data['previous'] = env('APP_URL').'api/news/tv/?'.$_page_prev_str.'&'.$_search_str.'&'.$_type_str;
        }else{
            $data['previous'] = null;
        }

        
        $news = $news->limit($pageCount)->orderby('created_at', 'desc')->get();

        $data['results'] = array();
        foreach ($news as $new) {
            $newArr = array();

            $newArr = $new->toArray();
            $newArr['type'] = $new->news_type->toArray();

            if(!empty($new->photo_gallery_id)){
                $newArr['image'] = env('MEDIA_END').$new->photo_gallery->items->first()->image;
            }

            if(!empty($new->video_gallery_id)){

                $e_code = $new->video_gallery->items->first()->embed_code;
                $e_source = $new->video_gallery->items->first()->embed_source;

                

                if ($e_source == 'app'){
                    $newArr['embed_url'] = $e_code;
                    $newArr['image'] = url('default.jpg');
                }else if ($e_source == 'vimeo'){
                    $newArr['embed_url'] = 'https://player.vimeo.com/video/'.$e_code;
                    $newArr['image'] = url('default.jpg');
                }else if ($e_source == 'youtube'){
                    $newArr['embed_url'] = 'https://www.youtube.com/embed/'.$e_code;
                    $newArr['image'] = 'https://i.ytimg.com/vi/'.$e_code.'/mqdefault.jpg';
                }

            }
            
            $data['results'][] = $newArr;
        }

        return response()->json($data, 200);
    }

    public function blog(Request $request)
    {
        $data = array();
        $legalType = array(69);

        $_search = $request->input('search');
        $_type = $request->input('type');
        $_page = $request->input('page');

        $_search_str = '';
        $_type_str = '';
        $_page_next_str = '';
        $_page_prev_str = '';

        if(!empty($_type)){
            $legalType = array($_type);
            $_type_str = 'type='.$_type;
        }

        $news = News::select('*')->whereIn('type_id', $legalType);//->whereNotNull('photo_gallery_id');
        
        if(!empty($_search)){
            $news = $news->where('name', 'ilike', '%'.$_search.'%');
            $_search_str = 'search='.$_search;
        }

        $news = $news->orderby('created_at', 'desc')->get();

        foreach ($news as $new) {
            $newArr = array();

            $newArr['id'] = $new->id;
            $newArr['name'] = $new->name;
            $newArr['slug'] = $new->slug;
            $newArr['subtitle'] = $new->subtitle;
            $newArr['date'] = $new->date;
            $newArr['image'] = $new->image;

            if(!empty($new->photo_gallery_id)){
                $newArr['image'] = env('MEDIA_END').$new->photo_gallery->items->first()->image;
            }

            if(!empty($new->video_gallery_id)){

                $e_code = $new->video_gallery->items->first()->embed_code;
                $e_source = $new->video_gallery->items->first()->embed_source;

                

                if ($e_source == 'app'){
                    $newArr['embed_url'] = $e_code;
                    $newArr['image'] = url('default.jpg');
                }else if ($e_source == 'vimeo'){
                    $newArr['embed_url'] = 'https://player.vimeo.com/video/'.$e_code;
                    $newArr['image'] = url('default.jpg');
                }else if ($e_source == 'youtube'){
                    $newArr['embed_url'] = 'https://www.youtube.com/embed/'.$e_code;
                    $newArr['image'] = 'https://i.ytimg.com/vi/'.$e_code.'/mqdefault.jpg';
                }

            }
            
            $data[] = $newArr;
        }

        return response()->json($data, 200);
    }

    public function blogDetail($slug, Request $request)
    {
        $data = array();
        $legalType = array(69);

        $_search = $request->input('search');
        $_type = $request->input('type');
        $_page = $request->input('page');

        $_search_str = '';
        $_type_str = '';
        $_page_next_str = '';
        $_page_prev_str = '';

        if(!empty($_type)){
            $legalType = array($_type);
            $_type_str = 'type='.$_type;
        }

        $new = News::select('*')->where('slug', $slug);//->whereNotNull('photo_gallery_id');
        
        if(!empty($_search)){
            $new = $new->where('name', 'ilike', '%'.$_search.'%');
            $_search_str = 'search='.$_search;
        }

        $new = $new->orderby('created_at', 'desc')->first();

        $newArr = array();

        $newArr['id'] = $new->id;
        $newArr['name'] = $new->name;
        $newArr['slug'] = $new->slug;
        $newArr['subtitle'] = $new->subtitle;
        $newArr['date'] = $new->date;
        $newArr['image'] = $new->image;
        $newArr['content'] = $new->content;

        $newArr['prev'] = null;
        $newArr['next'] = null;
        $newArr['author'] = $new->user;

        array_forget($newArr, 'author.username');
        array_forget($newArr, 'author.email');
        array_forget($newArr, 'author.email_confirmed');
        array_forget($newArr, 'author.infoshare');
        array_forget($newArr, 'author.is_staff');
        array_forget($newArr, 'author.is_superuser');
        array_forget($newArr, 'author.last_login');
        array_forget($newArr, 'author.phone');
        array_forget($newArr, 'author.phone_confirmed');
        array_forget($newArr, 'author.phone_code');
        
        $newArr['author']['age'] = (!empty($new->user->profile->birth_date)) ? Carbon::parse($new->user->profile->birth_date)->age : null;
        $newArr['author']['profile'] = $new->user->profile->toArray();

        array_forget($newArr, 'author.profile.identity_number');

        // $newArr['author']['profile']['province'] = (!empty($new->user->profile->province)) ? $new->user->profile->province->toArray() : null;
        // $newArr['author']['profile']['district'] = (!empty($new->user->profile->district)) ? $new->user->profile->district->toArray() : null;

        if(!empty($new->photo_gallery_id)){
            $newArr['image'] = env('MEDIA_END').$new->photo_gallery->items->first()->image;
        }

        if(!empty($new->video_gallery_id)){

            $e_code = $new->video_gallery->items->first()->embed_code;
            $e_source = $new->video_gallery->items->first()->embed_source;

            if ($e_source == 'app'){
                $newArr['embed_url'] = $e_code;
                $newArr['image'] = url('default.jpg');
            }else if ($e_source == 'vimeo'){
                $newArr['embed_url'] = 'https://player.vimeo.com/video/'.$e_code;
                $newArr['image'] = url('default.jpg');
            }else if ($e_source == 'youtube'){
                $newArr['embed_url'] = 'https://www.youtube.com/embed/'.$e_code;
                $newArr['image'] = 'https://i.ytimg.com/vi/'.$e_code.'/mqdefault.jpg';
            }

        }
        
        $data = $newArr;
        

        return response()->json($data, 200);
    }

    public function search(Request $request)
    {
        $data = array();

        $_term = str_replace('+', ' ', $request->input('term'));

        /* user search */
        $users = User::select(
            'users.id',
            DB::raw("CONCAT(users.first_name, ' ', users.last_name) as fullname"),
            'users.image',
            'players.id as player_id',
            'players.number',
            'players.foot',
            'players.weight',
            'players.height',
            'players.facebook',
            'players.twitter',
            'players.instagram',
            'players.value'
            
        )->leftJoin('players', 'users.id', '=', 'players.user_id')->whereNotNull('players.id');

        $users = $users->where(function ($query) use ($_term) {
            $query->where(DB::raw("REPLACE(CONCAT(users.first_name, ' ', users.last_name),'  ',' ')"), 'ilike', '%'.$_term.'%');
        });

        $users = $users->limit(6)->get();

        $data['player'] = array();
        foreach ($users as $user) {
            $userArr = $user->toArray();

            array_forget($userArr, 'username');
            array_forget($userArr, 'email');
            array_forget($userArr, 'email_confirmed');
            array_forget($userArr, 'infoshare');
            array_forget($userArr, 'is_staff');
            array_forget($userArr, 'is_superuser');
            array_forget($userArr, 'last_login');
            array_forget($userArr, 'phone');
            array_forget($userArr, 'phone_confirmed');
            array_forget($userArr, 'phone_code');


            if(!empty($user->image)){
                $userArr['image_url'] = env('MEDIA_END').$user->image;
            }else{
                $userArr['image_url'] = env('MEDIA_END').'default-player.png';
            }

            $userArr['position'] = (!empty($user->player->players_position)) ? $user->player->players_position->toArray() : null;
            $userArr['position2'] = (!empty($user->player->players_position2)) ? $user->player->players_position2->toArray() : null;
            $userArr['position3'] = (!empty($user->player->players_position3)) ? $user->player->players_position3->toArray() : null;

            if(!empty($user->player->team)){
                $userArr['team'] = $user->player->team->toArray();
                
                if (!empty($user->player->team->image)) {
                    $userArr['team']['image_url'] = env('MEDIA_END').$user->player->team->image;
                }else{
                    $userArr['team']['image_url'] = env('MEDIA_END').'default-team.png';
                }
                
                if(!empty($user->player->team->league)){
                    $userArr['team']['league'] = $user->player->team->league->toArray();
                    $userArr['team']['league']['province'] = $user->player->team->league->league_province->first()->province->toArray();
                    $userArr['team']['league']['season'] = null;
                }

            }else{
                $userArr['team'] = null;
            }

            $data['player'][] = $userArr;
        }
        /* user search */

        /* team search */
        $teams = Team::where('name', 'ilike', '%'.$_term.'%')->limit(6)->get();

        $data['team'] = array();
        foreach ($teams as $team) {

            $teamArr = $team->toArray();
            if (!empty($team->image)) {
                $teamArr['image_url'] = env('MEDIA_END').$team->image;
            }else{
                $teamArr['image_url'] = env('MEDIA_END').'default-team.png';
            }
            
            if(!empty($team->league)){
                $teamArr['league'] = $team->league->toArray();
                $teamArr['league']['province'] = $team->league->league_province->first()->province->toArray();
                $teamArr['league']['season'] = null;
            }

            $data['team'][] = $teamArr;
        }
        /* team search */

        return response()->json($data, 200);
    }

    public function provinces()
    {
        $data = array();

        $province = Province::orderBy('slug')->get();

        return response()->json($province, 200);
    }

    public function districts($id)
    {
        $data = array();

        $district = Province::find($id)->district;

        return response()->json($district, 200);
    }

    public function proLeague($id)
    {
        $data = array();

        if (count(Province::find($id)->league_province)) {
            $province_league = Province::find($id)->league_province->first()->league;
        }else{
            $province_league = League::where('active', true)->orderBy('id')->first();
        }

        return response()->json($province_league, 200);
    }

    public function leaguesAll()
    {
        $data = array();
        $leagues = League::select('leagues.id', 'leagues.name', 'leagues.facebook', 'leagues.twitter', 'leagues.instagram')->where('active', true)->
        where('provinces.plate_code', '!=', 99)->
        leftJoin('leagues_provinces', 'leagues_provinces.league_id', '=', 'leagues.id')->
        leftJoin('provinces', 'provinces.id', '=', 'leagues_provinces.province_id')->
        orderBy('provinces.plate_code')->get();


        foreach ($leagues as $league) {
            $lData = array();

            $lData = $league->toArray();

            //$lData['province'] = $league->league_province;
        

            if($league->seasonApi->count() == 0){
                $lData['season'] = null;
            }else{
                $lData['season'] = $league->seasonApi->first()->toArray();
                $lData['season']['fullname'] = $league->seasonApi->first()->year.' '.$league->name.' '.$league->seasonApi->first()->name;
                if($league->seasonApi->first()->polymorphic_ctype_id == 32){
                    $lData['season']['type'] = 'elimination';
                }else if($league->seasonApi->first()->polymorphic_ctype_id == 33){
                    $lData['season']['type'] = 'fixture';
                }else if($league->seasonApi->first()->polymorphic_ctype_id == 34){
                    $lData['season']['type'] = 'point';
                }
            }
            
            //$lData['season'] = $league->seasonApi->first()->toArray();
            if($league->league_province->count() == 1){
                $lData['province'] = $league->league_province->first()->province->toArray();
                $data[] = $lData;
            }else{
                $lData['province'] = null;
            }
            
        }

        return response()->json($data, 200);
    }

    public function leagues($id)
    {
        $data = array();
        $league = League::select('id', 'name', 'facebook', 'twitter', 'instagram')->find($id);

        $data = $league->toArray();
        if(!empty($league->league_province) && $league->league_province->count() > 0){
            $data['province'] = $league->league_province->first()->province->toArray();
        }
        if(!empty($league->seasonApi->first())){
            $data['season'] = $league->seasonApi->first()->toArray();

            if($league->seasonApi->first()->polymorphic_ctype_id == 32){
                $data['season']['type'] = 'elimination';
            }else if($league->seasonApi->first()->polymorphic_ctype_id == 33){
                $data['season']['type'] = 'fixture';
            }else if($league->seasonApi->first()->polymorphic_ctype_id == 34){
                $data['season']['type'] = 'point';
            }

        }else{
            $data['season'] = null;
        }

        return response()->json($data, 200);
    }

    public function leaSeasons($id)
    {
        $data = array();

        $dLeague = League::find($id);
        $dSeasons = $dLeague->seasonApi;

        foreach ($dSeasons as $dSeason) {

            $dSeason['fullname'] = $dSeason->year.' '.$dLeague->name.' '.$dSeason->name;
            if($dSeason->polymorphic_ctype_id == 32){
                $dSeason['type'] = 'elimination';
            }else if($dSeason->polymorphic_ctype_id == 33){
                $dSeason['type'] = 'fixture';
            }else if($dSeason->polymorphic_ctype_id == 34){
                $dSeason['type'] = 'point';
            }

            $data[] = $dSeason->toArray();
        }

        return response()->json($data, 200);
    }

    public function leaSeaCalendar($id, $sid)
    {
        $data = array();
        $season = Season::select('id', 'start_date', 'end_date', 'transfer_start_date', 'transfer_end_date', 'year')->find($sid);

        if(!empty($season->league_eliminationseasonApi)){
            $data['elimination_end_date'] = $season->league_eliminationseasonApi->base_seasonApi->end_date;
            $data['elimination_start_date'] = $season->league_eliminationseasonApi->base_seasonApi->start_date;
        }
        $data['season_end_date'] = $season->end_date;
        $data['season_start_date'] = $season->start_date;
        $data['transfer_end_date'] = $season->transfer_end_date;
        $data['transfer_start_date'] = $season->transfer_start_date;
        $data['year'] = $season->year;

        if($season->match->count() == 0){
            $data['step'] = 'oncesi';
        }elseif(!empty($season->championships)){
            $data['step'] = 'sonrasi';
        }else{
            $data['step'] = '';
        }

        $data['menu'] = "active";

        return response()->json($data, 200);
    }

    public function leaSeaConferences($id, $sid)
    {
        $data = array();
        $league = League::select('id', 'name', 'facebook', 'twitter', 'instagram')->find($id);
        $conference = null;
        if($league->league_province->count() > 0) {
            $conference = Conference::where('province_id', $league->league_province->first()->province_id)->orderBy('name', 'asc')->get();
        }
        //$data = $conference->toArray();
        return response()->json($conference, 200);
    }

    public function leaSeaConferencePoints($id, $sid, Request $request)
    {

        $data = array();
        if(!empty($request->input('id'))){

            //DB::enableQueryLog();
            $conference = Conference::find($request->input('id'));
            $conferences_district = $conference->conferences_district;
            
            $districts = array();
            foreach ($conferences_district as $value) {
                $districts[] = $value->district_id;
            }
            $season = Season::find($sid);
            $teamseason = TeamTeamSeason::select('*', 'team_teamseason.season_id as season', 'team_teamseason.squad_locked_player_id as squad_locked_player')->
            leftJoin('teams', 'teams.id', '=', 'team_teamseason.team_id')->
            whereIn('teams.district_id', $districts)->
            where('team_teamseason.season_id', $sid)->
            where('teams.status', '!=', 'passive');
            /*
            if(!empty($request->input('limit'))){
                $teamseason = $teamseason->limit($request->input('limit'));
            }
            */
            $teamseason = $teamseason->orderBy('playoff_member', 'desc');
            $teamseason = $teamseason->orderBy('point', 'desc')->get();
            //return response()->json(DB::getQueryLog(), 200);
            
            foreach ($teamseason as $ts) {

                $teamid = $ts->team_id;
                $teamArr = $ts->toArray();

                $teamArr['last_five_match_result'] = array();
                $l5ms = Match::where('season_id', $sid)->where('completed', true)->
                    where(function ($query) use ($teamid) {
                        $query->where('team1_id', $teamid)
                            ->orWhere('team2_id', $teamid);
                    })->orderBy('date', 'desc')->limit(5)->get();

                foreach ($l5ms as $l5m) {
                    if($l5m->team1_goal == $l5m->team2_goal){
                        $teamArr['last_five_match_result'][] = 0;
                    }else if($l5m->team1_goal > $l5m->team2_goal){
                        $teamArr['last_five_match_result'][] = 1;
                    }else if($l5m->team1_goal < $l5m->team2_goal){
                        $teamArr['last_five_match_result'][] = 2;
                    }
                }

                $teamArr['team'] = $ts->team->toArray();

                if (!empty($ts->team->image)) {
                    $teamArr['team']['image_url'] = env('MEDIA_END').$ts->team->image;
                }else{
                    $teamArr['team']['image_url'] = env('MEDIA_END').'default-team.png';
                }

                $teamArr['team']['league'] = $ts->team->league->toArray();
                $teamArr['team']['league']['province'] = $ts->team->league->league_province->first()->province->toArray();
                $teamArr['team']['league']['season'] = $ts->team->league->seasonApi->first()->toArray();

                if($ts->team->league->seasonApi->first()->polymorphic_ctype_id == 32){
                    $teamArr['team']['league']['season']['type'] = 'elimination';
                }else if($ts->team->league->seasonApi->first()->polymorphic_ctype_id == 33){
                    $teamArr['team']['league']['season']['type'] = 'fixture';
                }else if($ts->team->league->seasonApi->first()->polymorphic_ctype_id == 34){
                    $teamArr['team']['league']['season']['type'] = 'point';
                }

                $teamArr['team']['league']['season']['fullname'] = $ts->team->league->seasonApi->first()->year.' '.$ts->team->league->name.' '.$ts->team->league->seasonApi->first()->name;

                $teamArr['season_performance']['yellow_card'] = $ts->yellow_card;
                $teamArr['season_performance']['red_card'] = $ts->red_card;
                $teamArr['season_performance']['gk_save'] = $ts->gk_save;

                /*
                $r = 0.0;
                foreach ($ts->team->team_playerteamhistory as $value) {
                    $r += $value->rating;
                }
                $teamArr['season_performance']['total_rating'] = ($ts->team->team_playerteamhistory->count() > 0) ? number_format((float)($r / $ts->team->team_playerteamhistory->count()), 1, '.', '') : 0;
                */

                $teamArr['point_type'] = PointType::where('min_point', '<=', $ts->point)->where('max_point', '>=', $ts->point)->first();

                if($teamArr['match_total'] > 0){
                    $data[] = $teamArr;
                    if(count($data) == $request->input('limit')){
                        break;
                    }
                }
            }
        }

        return response()->json($data, 200);
    }

    public function leaSeaGroups($id, $sid)
    {
        $data = array();
        //$league = League::select('id', 'name', 'facebook', 'twitter', 'instagram')->find($id);
        $groups = Group::where('season_id', $sid)->orderBy('name', 'asc')->get();
        //$data = $conference->toArray();
        return response()->json($groups, 200);
    }

    public function leaSeaGroupPoints($id, $sid, Request $request)
    {

        $data = array();
        if(!empty($request->input('id'))){

            //DB::enableQueryLog();
            $group = Group::find($request->input('id'));

            $season = Season::find($sid);

            $teamseason = TeamTeamSeason::select('*', 'team_teamseason.season_id as season', 'team_teamseason.squad_locked_player_id as squad_locked_player')->
            leftJoin('teams', 'teams.id', '=', 'team_teamseason.team_id')->
            whereIn('team_teamseason.team_id', $group->group_teams->pluck('team_id')->all())->
            where('team_teamseason.season_id', $sid)->
            where('teams.status', '!=', 'passive');
            if(!empty($request->input('limit'))){
                $teamseason = $teamseason->limit($request->input('limit'));
            }
            $teamseason = $teamseason->orderBy('playoff_member', 'desc');
            $teamseason = $teamseason->orderBy('point', 'desc')->get();
            //return response()->json(DB::getQueryLog(), 200);
            
            foreach ($teamseason as $ts) {

                $teamid = $ts->team_id;
                $teamArr = $ts->toArray();

                $teamArr['last_five_match_result'] = array();
                $l5ms = Match::where('season_id', $sid)->where('completed', true)->
                    where(function ($query) use ($teamid) {
                        $query->where('team1_id', $teamid)
                            ->orWhere('team2_id', $teamid);
                    })->orderBy('date', 'desc')->limit(5)->get();

                foreach ($l5ms as $l5m) {
                    if($l5m->team1_goal == $l5m->team2_goal){
                        $teamArr['last_five_match_result'][] = 0;
                    }else if($l5m->team1_goal > $l5m->team2_goal){
                        $teamArr['last_five_match_result'][] = 1;
                    }else if($l5m->team1_goal < $l5m->team2_goal){
                        $teamArr['last_five_match_result'][] = 2;
                    }
                }

                $teamArr['team'] = $ts->team->toArray();

                if (!empty($ts->team->image)) {
                    $teamArr['team']['image_url'] = env('MEDIA_END').$ts->team->image;
                }else{
                    $teamArr['team']['image_url'] = env('MEDIA_END').'default-team.png';
                }

                $teamArr['team']['league'] = $ts->team->league->toArray();
                $teamArr['team']['league']['province'] = $ts->team->league->league_province->first()->province->toArray();
                $teamArr['team']['league']['season'] = $ts->team->league->seasonApi->first()->toArray();

                if($ts->team->league->seasonApi->first()->polymorphic_ctype_id == 32){
                    $teamArr['team']['league']['season']['type'] = 'elimination';
                }else if($ts->team->league->seasonApi->first()->polymorphic_ctype_id == 33){
                    $teamArr['team']['league']['season']['type'] = 'fixture';
                }else if($ts->team->league->seasonApi->first()->polymorphic_ctype_id == 34){
                    $teamArr['team']['league']['season']['type'] = 'point';
                }

                $teamArr['team']['league']['season']['fullname'] = $ts->team->league->seasonApi->first()->year.' '.$ts->team->league->name.' '.$ts->team->league->seasonApi->first()->name;

                $teamArr['season_performance']['yellow_card'] = $ts->yellow_card;
                $teamArr['season_performance']['red_card'] = $ts->red_card;
                $teamArr['season_performance']['gk_save'] = $ts->gk_save;

                /*
                $r = 0.0;
                foreach ($ts->team->team_playerteamhistory as $value) {
                    $r += $value->rating;
                }
                $teamArr['season_performance']['total_rating'] = ($ts->team->team_playerteamhistory->count() > 0) ? number_format((float)($r / $ts->team->team_playerteamhistory->count()), 1, '.', '') : 0;
                */

                $teamArr['point_type'] = PointType::where('min_point', '<=', $ts->point)->where('max_point', '>=', $ts->point)->first();

                if($season->league->id == '26' || $teamArr['match_total'] > 0){
                    $data[] = $teamArr;
                }
            }
        }

        return response()->json($data, 200);
    }

    public function leaSeaTemsilciMatches($id, $sid, Request $request)
    {

        $data = array();
        $_ids = array();

        $season = Season::find($sid);
        $_ids[] = $sid;
        if(!empty($season->league_elimination_baseseason)){
            $_ids[] = $season->league_elimination_baseseason->season_ptr_id;
        }

           
        //return response()->json($_ids, 200);
        //die;
     

        $matches = Match::select('*', 'season_id as season')->
        whereIn('season_id', $_ids)->where('completed', true);

        if(!empty($season->season_temsilci_team)){
            if(!empty($season->season_temsilci_team->data)){
                $matches = $matches->where(function ($query) use ($season) {
                    $query->where('team1_id', $season->season_temsilci_team->data)->orWhere('team2_id', $season->season_temsilci_team->data);
                });
            }
        }

        $matches = $matches->orderBy('date', 'desc')->orderBy('time', 'desc');
        //->where('date' <= Carbon::now())
        if(!empty($request->input('limit'))){
            $matches = $matches->limit($request->input('limit'));
        }

        $matches = $matches->get();

        foreach ($matches as $match) {

            $matchArr = $match->toArray();
            $matchArr['video_status'] = (!empty($match->match_video)) ? $match->match_video->status : 'recordable';

            $matchArr['team1'] = $match->team1->toArray();

            if (!empty($match->team1->image)) {
                $matchArr['team1']['image_url'] = env('MEDIA_END').$match->team1->image;
            }else{
                $matchArr['team1']['image_url'] = env('MEDIA_END').'default-team.png';
            }

            $matchArr['team1']['league'] = $season->league->toArray();
            //$matchArr['team1']['league']['province'] = $season->league->league_province->first()->province->toArray();
            $matchArr['team1']['league']['season'] = $season->league->seasonApi->first()->toArray();

            if($season->league->seasonApi->first()->polymorphic_ctype_id == 32){
                $matchArr['team1']['league']['season']['type'] = 'elimination';
            }else if($season->league->seasonApi->first()->polymorphic_ctype_id == 33){
                $matchArr['team1']['league']['season']['type'] = 'fixture';
            }else if($season->league->seasonApi->first()->polymorphic_ctype_id == 34){
                $matchArr['team1']['league']['season']['type'] = 'point';
            }
            $matchArr['team1']['league']['season']['fullname'] = $season->league->seasonApi->first()->year.' '.$season->league->name.' '.$season->league->seasonApi->first()->name;


            $matchArr['team2'] = $match->team2->toArray();

            if (!empty($match->team2->image)) {
                $matchArr['team2']['image_url'] = env('MEDIA_END').$match->team2->image;
            }else{
                $matchArr['team2']['image_url'] = env('MEDIA_END').'default-team.png';
            }

            $matchArr['team2']['league'] = $season->league->toArray();
            //$matchArr['team2']['league']['province'] = $season->league->league_province->first()->province->toArray();
            $matchArr['team2']['league']['season'] = $season->league->seasonApi->first()->toArray();

            if($season->league->seasonApi->first()->polymorphic_ctype_id == 32){
                $matchArr['team2']['league']['season']['type'] = 'elimination';
            }else if($season->league->seasonApi->first()->polymorphic_ctype_id == 33){
                $matchArr['team2']['league']['season']['type'] = 'fixture';
            }else if($season->league->seasonApi->first()->polymorphic_ctype_id == 34){
                $matchArr['team2']['league']['season']['type'] = 'point';
            }
            $matchArr['team2']['league']['season']['fullname'] = $season->league->seasonApi->first()->year.' '.$season->league->name.' '.$season->league->seasonApi->first()->name;

            $data[] = $matchArr;

        }


        return response()->json($data, 200);
    }


    public function leaSeaRecentMatches($id, $sid, Request $request)
    {

        $data = array();
        $_ids = array();

        $season = Season::find($sid);
        $_ids[] = $sid;
        if(!empty($season->league_elimination_baseseason)){
            $_ids[] = $season->league_elimination_baseseason->season_ptr_id;
        }

        //return response()->json($_ids, 200);
        //die;

        $matches = Match::select('*', 'season_id as season')->
        whereIn('season_id', $_ids)->where('completed', true)->orderBy('date', 'desc')->orderBy('time', 'desc');
        //->where('date' <= Carbon::now())
        if(!empty($request->input('limit'))){
            $matches = $matches->limit($request->input('limit'));
        }

        $matches = $matches->get();

        foreach ($matches as $match) {

            $matchArr = $match->toArray();
            $matchArr['video_status'] = (!empty($match->match_video)) ? $match->match_video->status : 'recordable';

            $matchArr['team1'] = $match->team1->toArray();

            if (!empty($match->team1->image)) {
                $matchArr['team1']['image_url'] = env('MEDIA_END').$match->team1->image;
            }else{
                $matchArr['team1']['image_url'] = env('MEDIA_END').'default-team.png';
            }

            $matchArr['team1']['league'] = $season->league->toArray();
            //$matchArr['team1']['league']['province'] = $season->league->league_province->first()->province->toArray();
            $matchArr['team1']['league']['season'] = $season->league->seasonApi->first()->toArray();

            if($season->league->seasonApi->first()->polymorphic_ctype_id == 32){
                $matchArr['team1']['league']['season']['type'] = 'elimination';
            }else if($season->league->seasonApi->first()->polymorphic_ctype_id == 33){
                $matchArr['team1']['league']['season']['type'] = 'fixture';
            }else if($season->league->seasonApi->first()->polymorphic_ctype_id == 34){
                $matchArr['team1']['league']['season']['type'] = 'point';
            }
            $matchArr['team1']['league']['season']['fullname'] = $season->league->seasonApi->first()->year.' '.$season->league->name.' '.$season->league->seasonApi->first()->name;


            $matchArr['team2'] = $match->team2->toArray();

            if (!empty($match->team2->image)) {
                $matchArr['team2']['image_url'] = env('MEDIA_END').$match->team2->image;
            }else{
                $matchArr['team2']['image_url'] = env('MEDIA_END').'default-team.png';
            }

            $matchArr['team2']['league'] = $season->league->toArray();
            //$matchArr['team2']['league']['province'] = $season->league->league_province->first()->province->toArray();
            $matchArr['team2']['league']['season'] = $season->league->seasonApi->first()->toArray();

            if($season->league->seasonApi->first()->polymorphic_ctype_id == 32){
                $matchArr['team2']['league']['season']['type'] = 'elimination';
            }else if($season->league->seasonApi->first()->polymorphic_ctype_id == 33){
                $matchArr['team2']['league']['season']['type'] = 'fixture';
            }else if($season->league->seasonApi->first()->polymorphic_ctype_id == 34){
                $matchArr['team2']['league']['season']['type'] = 'point';
            }
            $matchArr['team2']['league']['season']['fullname'] = $season->league->seasonApi->first()->year.' '.$season->league->name.' '.$season->league->seasonApi->first()->name;

            $data[] = $matchArr;

        }


        return response()->json($data, 200);
    }

    public function leaSeaRecentGroupMatches($id, $sid, Request $request)
    {

        $data = array();
        $_ids = array();

        $season = Season::find($sid);
        $_ids[] = $sid;
        if(!empty($season->league_elimination_baseseason)){
            $_ids[] = $season->league_elimination_baseseason->season_ptr_id;
        }

        //return response()->json($_ids, 200);
        //die;

        $matches = Match::select('*', 'season_id as season')->
        whereIn('season_id', $_ids)->where('completed', true)->orderBy('date', 'desc')->orderBy('time', 'desc');
        //->where('date' <= Carbon::now())
        if(!empty($request->input('limit'))){
            $matches = $matches->limit($request->input('limit'));
        }

        $matches = $matches->get();

        foreach ($matches as $match) {

            $matchArr = $match->toArray();
            $matchArr['video_status'] = (!empty($match->match_video)) ? $match->match_video->status : 'recordable';

            $matchArr['team1'] = $match->team1->toArray();

            if (!empty($match->team1->image)) {
                $matchArr['team1']['image_url'] = env('MEDIA_END').$match->team1->image;
            }else{
                $matchArr['team1']['image_url'] = env('MEDIA_END').'default-team.png';
            }

            $matchArr['team1']['league'] = $season->league->toArray();
            //$matchArr['team1']['league']['province'] = $season->league->league_province->first()->province->toArray();
            $matchArr['team1']['league']['season'] = $season->league->seasonApi->first()->toArray();

            if($season->league->seasonApi->first()->polymorphic_ctype_id == 32){
                $matchArr['team1']['league']['season']['type'] = 'elimination';
            }else if($season->league->seasonApi->first()->polymorphic_ctype_id == 33){
                $matchArr['team1']['league']['season']['type'] = 'fixture';
            }else if($season->league->seasonApi->first()->polymorphic_ctype_id == 34){
                $matchArr['team1']['league']['season']['type'] = 'point';
            }
            $matchArr['team1']['league']['season']['fullname'] = $season->league->seasonApi->first()->year.' '.$season->league->name.' '.$season->league->seasonApi->first()->name;


            $matchArr['team2'] = $match->team2->toArray();

            if (!empty($match->team2->image)) {
                $matchArr['team2']['image_url'] = env('MEDIA_END').$match->team2->image;
            }else{
                $matchArr['team2']['image_url'] = env('MEDIA_END').'default-team.png';
            }

            $matchArr['team2']['league'] = $season->league->toArray();
            //$matchArr['team2']['league']['province'] = $season->league->league_province->first()->province->toArray();
            $matchArr['team2']['league']['season'] = $season->league->seasonApi->first()->toArray();

            if($season->league->seasonApi->first()->polymorphic_ctype_id == 32){
                $matchArr['team2']['league']['season']['type'] = 'elimination';
            }else if($season->league->seasonApi->first()->polymorphic_ctype_id == 33){
                $matchArr['team2']['league']['season']['type'] = 'fixture';
            }else if($season->league->seasonApi->first()->polymorphic_ctype_id == 34){
                $matchArr['team2']['league']['season']['type'] = 'point';
            }
            $matchArr['team2']['league']['season']['fullname'] = $season->league->seasonApi->first()->year.' '.$season->league->name.' '.$season->league->seasonApi->first()->name;

            $data[] = $matchArr;

        }


        return response()->json($data, 200);
    }

    public function leaSeaRecentGroupMatches2($id, $sid, $gid, Request $request)
    {

        $data = array();

        $season = Season::find($sid);

        $group = Group::find($gid);
        $team_idss = array();

        foreach ($group->group_teams as $gteams) {
            $team_idss[] = $gteams['team_id'];
        }

        $matches = Match::where('season_id', $sid)->where('completed', true)->
                where(function ($query) use ($team_idss) {
                    $query->whereIn('team1_id', $team_idss)
                        ->orWhereIn('team2_id', $team_idss);
                })->orderBy('date', 'desc')->orderBy('time', 'desc')->get();

        /*
        return response()->json($team_idss, 200);
        die;
        */

        /*
        $matches = Match::select('*', 'season_id as season')->
        whereIn('season_id', $sid)->where('completed', true)->orderBy('date', 'desc')->orderBy('time', 'desc');
        //->where('date' <= Carbon::now())
        if(!empty($request->input('limit'))){
            $matches = $matches->limit($request->input('limit'));
        }
        $matches = $matches->get();
        */

        foreach ($matches as $match) {

            $matchArr = $match->toArray();
            $matchArr['video_status'] = (!empty($match->match_video)) ? $match->match_video->status : 'recordable';

            $matchArr['team1'] = $match->team1->toArray();

            if (!empty($match->team1->image)) {
                $matchArr['team1']['image_url'] = env('MEDIA_END').$match->team1->image;
            }else{
                $matchArr['team1']['image_url'] = env('MEDIA_END').'default-team.png';
            }

            $matchArr['team1']['league'] = $season->league->toArray();
            //$matchArr['team1']['league']['province'] = $season->league->league_province->first()->province->toArray();
            $matchArr['team1']['league']['season'] = $season->league->seasonApi->first()->toArray();

            if($season->league->seasonApi->first()->polymorphic_ctype_id == 32){
                $matchArr['team1']['league']['season']['type'] = 'elimination';
            }else if($season->league->seasonApi->first()->polymorphic_ctype_id == 33){
                $matchArr['team1']['league']['season']['type'] = 'fixture';
            }else if($season->league->seasonApi->first()->polymorphic_ctype_id == 34){
                $matchArr['team1']['league']['season']['type'] = 'point';
            }
            $matchArr['team1']['league']['season']['fullname'] = $season->league->seasonApi->first()->year.' '.$season->league->name.' '.$season->league->seasonApi->first()->name;


            $matchArr['team2'] = $match->team2->toArray();

            if (!empty($match->team2->image)) {
                $matchArr['team2']['image_url'] = env('MEDIA_END').$match->team2->image;
            }else{
                $matchArr['team2']['image_url'] = env('MEDIA_END').'default-team.png';
            }

            $matchArr['team2']['league'] = $season->league->toArray();
            //$matchArr['team2']['league']['province'] = $season->league->league_province->first()->province->toArray();
            $matchArr['team2']['league']['season'] = $season->league->seasonApi->first()->toArray();

            if($season->league->seasonApi->first()->polymorphic_ctype_id == 32){
                $matchArr['team2']['league']['season']['type'] = 'elimination';
            }else if($season->league->seasonApi->first()->polymorphic_ctype_id == 33){
                $matchArr['team2']['league']['season']['type'] = 'fixture';
            }else if($season->league->seasonApi->first()->polymorphic_ctype_id == 34){
                $matchArr['team2']['league']['season']['type'] = 'point';
            }
            $matchArr['team2']['league']['season']['fullname'] = $season->league->seasonApi->first()->year.' '.$season->league->name.' '.$season->league->seasonApi->first()->name;

            $data[] = $matchArr;

        }


        return response()->json($data, 200);
    }

    public function leaSeaMatches($id, $sid, Request $request)
    {

        $data = array();
        $_ids = array();

        $season = Season::find($sid);
        $_ids[] = $sid;
        if(!empty($season->league_elimination_baseseason)){
            $_ids[] = $season->league_elimination_baseseason->season_ptr_id;
        }

        $matches = Match::select('*', 'season_id as season')->whereIn('season_id', $_ids)->where('date', Carbon::parse($request->input('date'))->format('Y-m-d'))->orderBy('time', 'desc');
        $matches = $matches->get();
        
        foreach ($matches as $match) {

            $matchArr = $match->toArray();
            $matchArr['video_status'] = (!empty($match->match_video)) ? $match->match_video->status : 'recordable';

            $matchArr['team1'] = $match->team1->toArray();

            if (!empty($match->team1->image)) {
                $matchArr['team1']['image_url'] = env('MEDIA_END').$match->team1->image;
            }else{
                $matchArr['team1']['image_url'] = env('MEDIA_END').'default-team.png';
            }

            $matchArr['team1']['league'] = $season->league->toArray();
            //$matchArr['team1']['league']['province'] = $season->league->league_province->first()->province->toArray();
            $matchArr['team1']['league']['season'] = $season->league->seasonApi->first()->toArray();

            if($season->league->seasonApi->first()->polymorphic_ctype_id == 32){
                $matchArr['team1']['league']['season']['type'] = 'elimination';
            }else if($season->league->seasonApi->first()->polymorphic_ctype_id == 33){
                $matchArr['team1']['league']['season']['type'] = 'fixture';
            }else if($season->league->seasonApi->first()->polymorphic_ctype_id == 34){
                $matchArr['team1']['league']['season']['type'] = 'point';
            }
            $matchArr['team1']['league']['season']['fullname'] = $season->league->seasonApi->first()->year.' '.$season->league->name.' '.$season->league->seasonApi->first()->name;

            $matchArr['team2'] = $match->team2->toArray();
            
            if (!empty($match->team2->image)) {
                $matchArr['team2']['image_url'] = env('MEDIA_END').$match->team2->image;
            }else{
                $matchArr['team2']['image_url'] = env('MEDIA_END').'default-team.png';
            }

            $matchArr['team2']['league'] = $season->league->toArray();
            //$matchArr['team2']['league']['province'] = $season->league->league_province->first()->province->toArray();
            $matchArr['team2']['league']['season'] = $season->league->seasonApi->first()->toArray();

            if($season->league->seasonApi->first()->polymorphic_ctype_id == 32){
                $matchArr['team2']['league']['season']['type'] = 'elimination';
            }else if($season->league->seasonApi->first()->polymorphic_ctype_id == 33){
                $matchArr['team2']['league']['season']['type'] = 'fixture';
            }else if($season->league->seasonApi->first()->polymorphic_ctype_id == 34){
                $matchArr['team2']['league']['season']['type'] = 'point';
            }
            $matchArr['team2']['league']['season']['fullname'] = $season->league->seasonApi->first()->year.' '.$season->league->name.' '.$season->league->seasonApi->first()->name;

            if(!empty($match->ground)){
                $matchArr['ground'] = $match->ground->toArray();
            }else{
                $matchArr['ground'] = null;
            }

            $data[] = $matchArr;

        }

        return response()->json($data, 200);
    }
    
    public function matches($id)
    {

        $data = array();

        $match = Match::select('*', 'season_id as season')->where('id', $id)->first();
        
        if(!empty($match)){
            $season = Season::find($match->season_id);

            $matchArr = $match->toArray();
            $matchArr['video_status'] = (!empty($match->match_video)) ? $match->match_video->status : 'recordable';

            if(empty($match->team1)){
                $matchArr['team1']['id'] = 0;
                $matchArr['team1']['name'] = 'Takım';
                $matchArr['team1']['image_url'] = env('MEDIA_END').'default-team.png';
                //$matchArr['team1']['league'] = $match->season->league->toArray();
            }else{
                $matchArr['team1'] = $match->team1->toArray();

                if (!empty($match->team1->image)) {
                    $matchArr['team1']['image_url'] = env('MEDIA_END').$match->team1->image;
                }else{
                    $matchArr['team1']['image_url'] = env('MEDIA_END').'default-team.png';
                }

                $matchArr['team1']['league'] = $season->league->toArray();
                //$matchArr['team1']['league']['province'] = $season->league->league_province->first()->province->toArray();
                $matchArr['team1']['league']['season'] = $season->league->seasonApi->first()->toArray();

                if($season->league->seasonApi->first()->polymorphic_ctype_id == 32){
                    $matchArr['team1']['league']['season']['type'] = 'elimination';
                }else if($season->league->seasonApi->first()->polymorphic_ctype_id == 33){
                    $matchArr['team1']['league']['season']['type'] = 'fixture';
                }else if($season->league->seasonApi->first()->polymorphic_ctype_id == 34){
                    $matchArr['team1']['league']['season']['type'] = 'point';
                }
                $matchArr['team1']['league']['season']['fullname'] = $season->league->seasonApi->first()->year.' '.$season->league->name.' '.$season->league->seasonApi->first()->name;
            }

            
            if(empty($match->team2)){
                $matchArr['team2']['id'] = 0;
                $matchArr['team2']['name'] = 'Takım';
                $matchArr['team2']['image_url'] = env('MEDIA_END').'default-team.png';
                //$matchArr['team2']['league'] = $match->season->league->toArray();
            }else{

                $matchArr['team2'] = $match->team2->toArray();

                if (!empty($match->team2->image)) {
                    $matchArr['team2']['image_url'] = env('MEDIA_END').$match->team2->image;
                }else{
                    $matchArr['team2']['image_url'] = env('MEDIA_END').'default-team.png';
                }

                $matchArr['team2']['league'] = $season->league->toArray();
                //$matchArr['team2']['league']['province'] = $season->league->league_province->first()->province->toArray();
                $matchArr['team2']['league']['season'] = $season->league->seasonApi->first()->toArray();

                if($season->league->seasonApi->first()->polymorphic_ctype_id == 32){
                    $matchArr['team2']['league']['season']['type'] = 'elimination';
                }else if($season->league->seasonApi->first()->polymorphic_ctype_id == 33){
                    $matchArr['team2']['league']['season']['type'] = 'fixture';
                }else if($season->league->seasonApi->first()->polymorphic_ctype_id == 34){
                    $matchArr['team2']['league']['season']['type'] = 'point';
                }
                $matchArr['team2']['league']['season']['fullname'] = $season->league->seasonApi->first()->year.' '.$season->league->name.' '.$season->league->seasonApi->first()->name;
            }

            $matchArr['league']['id'] = $season->league->id;
            $matchArr['league']['name'] = $season->league->name;

            if(!empty($match->coordinator)){
                $matchArr['coordinator'] = $match->coordinator->toArray();
                $matchArr['coordinator']['fullname'] = $match->coordinator->user->first_name.' '.$match->coordinator->user->last_name;
                
                if(!empty($match->coordinator->user->image)){
                    $matchArr['coordinator']['image_url'] = env('MEDIA_END').$match->coordinator->user->image;
                }else{
                    $matchArr['coordinator']['image_url'] = env('MEDIA_END').'default-player.png';
                }
            }else{
                $matchArr['coordinator'] = null;
            }

            if(!empty($match->referee)){
                $matchArr['referee'] = $match->referee->toArray();
                $matchArr['referee']['fullname'] = $match->referee->user->first_name.' '.$match->referee->user->last_name;
                
                if(!empty($match->referee->user->image)){
                    $matchArr['referee']['image_url'] = env('MEDIA_END').$match->referee->user->image;
                }else{
                    $matchArr['referee']['image_url'] = env('MEDIA_END').'default-player.png';
                }
            }else{
                $matchArr['referee'] = null;
            }

            if(!empty($match->ground)){
                $matchArr['ground'] = $match->ground->toArray();
            }else{
                $matchArr['ground'] = null;
            }

            $data = $matchArr;
        }

        return response()->json($data, 200);
    }

    public function matchVideo($id)
    {

        $data = array();
        
        $match = Match::select('*', 'season_id as season')->where('id', $id)->first();
        $season = Season::find($match->season_id);

        $matchArr = $match->toArray();
        $matchArr['video_status'] = (!empty($match->match_video)) ? $match->match_video->status : 'recordable';


        if(empty($match->team1)){
            $matchArr['team1']['id'] = 0;
            $matchArr['team1']['name'] = 'Takım';
            $matchArr['team1']['image_url'] = env('MEDIA_END').'default-team.png';
            //$matchArr['team1']['league'] = $match->season->league->toArray();
        }else{

            $matchArr['team1'] = $match->team1->toArray();

            if (!empty($match->team1->image)) {
                $matchArr['team1']['image_url'] = env('MEDIA_END').$match->team1->image;
            }else{
                $matchArr['team1']['image_url'] = env('MEDIA_END').'default-team.png';
            }

            $matchArr['team1']['league'] = $season->league->toArray();
            //$matchArr['team1']['league']['province'] = $season->league->league_province->first()->province->toArray();
            $matchArr['team1']['league']['season'] = $season->league->seasonApi->first()->toArray();

            if($season->league->seasonApi->first()->polymorphic_ctype_id == 32){
                $matchArr['team1']['league']['season']['type'] = 'elimination';
            }else if($season->league->seasonApi->first()->polymorphic_ctype_id == 33){
                $matchArr['team1']['league']['season']['type'] = 'fixture';
            }else if($season->league->seasonApi->first()->polymorphic_ctype_id == 34){
                $matchArr['team1']['league']['season']['type'] = 'point';
            }
            $matchArr['team1']['league']['season']['fullname'] = $season->league->seasonApi->first()->year.' '.$season->league->name.' '.$season->league->seasonApi->first()->name;
        }

        if(empty($match->team2)){
            $matchArr['team2']['id'] = 0;
            $matchArr['team2']['name'] = 'Takım';
            $matchArr['team2']['image_url'] = env('MEDIA_END').'default-team.png';
            //$matchArr['team2']['league'] = $match->season->league->toArray();
        }else{

            $matchArr['team2'] = $match->team2->toArray();

            if (!empty($match->team2->image)) {
                $matchArr['team2']['image_url'] = env('MEDIA_END').$match->team2->image;
            }else{
                $matchArr['team2']['image_url'] = env('MEDIA_END').'default-team.png';
            }

            $matchArr['team2']['league'] = $season->league->toArray();
            //$matchArr['team2']['league']['province'] = $season->league->league_province->first()->province->toArray();
            $matchArr['team2']['league']['season'] = $season->league->seasonApi->first()->toArray();

            if($season->league->seasonApi->first()->polymorphic_ctype_id == 32){
                $matchArr['team2']['league']['season']['type'] = 'elimination';
            }else if($season->league->seasonApi->first()->polymorphic_ctype_id == 33){
                $matchArr['team2']['league']['season']['type'] = 'fixture';
            }else if($season->league->seasonApi->first()->polymorphic_ctype_id == 34){
                $matchArr['team2']['league']['season']['type'] = 'point';
            }
            $matchArr['team2']['league']['season']['fullname'] = $season->league->seasonApi->first()->year.' '.$season->league->name.' '.$season->league->seasonApi->first()->name;
        }

        if(!empty($match->match_video)){

            $summ['full_embed_url'] = null;
            $summ['full_embed_code'] = $match->match_video->full_embed_code;
            if ($match->match_video->full_source == 'app'){
                $summ['full_embed_url'] = $match->match_video->full_embed_code;
            }else if ($match->match_video->full_source == 'vimeo'){
                $summ['full_embed_url'] = 'https://player.vimeo.com/video/'.$match->match_video->full_embed_code;
            }else if ($match->match_video->full_source == 'youtube'){
                $summ['full_embed_url'] = 'https://www.youtube.com/embed/'.$match->match_video->full_embed_code;
            }
            $summ['full_source'] = $match->match_video->full_source;


            $summ['live_embed_url'] = null;
            $summ['live_embed_code'] = $match->match_video->live_embed_code;
            if ($match->match_video->live_source == 'app'){
                $summ['live_embed_url'] = $match->match_video->live_embed_code;
            }else if ($match->match_video->live_source == 'vimeo'){
                $summ['live_embed_url'] = 'https://player.vimeo.com/video/'.$match->match_video->live_embed_code;
            }else if ($match->match_video->live_source == 'youtube'){
                $summ['live_embed_url'] = 'https://www.youtube.com/embed/'.$match->match_video->live_embed_code;
            }
            $summ['live_source'] = $match->match_video->live_source;


            $summ['summary_embed_url'] = null;
            $summ['summary_embed_code'] = $match->match_video->summary_embed_code;
            if ($match->match_video->summary_source == 'app'){
                $summ['summary_embed_url'] = $match->match_video->summary_embed_code;
                $summ['summary_image_url'] = url('default.jpg');
            }else if ($match->match_video->summary_source == 'vimeo'){
                $summ['summary_embed_url'] = 'https://player.vimeo.com/video/'.$match->match_video->summary_embed_code;
                $summ['summary_image_url'] = url('default.jpg');
            }else if ($match->match_video->summary_source == 'youtube'){
                $summ['summary_embed_url'] = 'https://www.youtube.com/embed/'.$match->match_video->summary_embed_code;
                $summ['summary_image_url'] = 'https://i3.ytimg.com/vi/'.$match->match_video->summary_embed_code.'/hqdefault.jpg';
            }
            $summ['summary_source'] = $match->match_video->summary_source;

            $summ['status'] = $match->match_video->status;

        }
        
        $summ['match'] = $matchArr;
        $data = $summ;
        
        return response()->json($data, 200);
    }

    public function matchAction($id)
    {

        $data = array();
        
        $match = Match::select('*', 'season_id as season')->where('id', $id)->first();
        $season = Season::find($match->season_id);

        foreach ($match->match_action_order_minute as $action) {
            
            $actionArr = $action->toArray();

            $actionArr['embed_url'] = null;
            $actionArr['embed_code'] = $action->embed_code;
            if ($action->embed_source == 'app'){
                $actionArr['embed_url'] = $action->embed_code;
            }else if ($action->embed_source == 'vimeo'){
                $actionArr['embed_url'] = 'https://player.vimeo.com/video/'.$action->embed_code;
            }else if ($action->embed_source == 'youtube'){
                $actionArr['embed_url'] = 'https://www.youtube.com/embed/'.$action->embed_code;
            }

            $actionArr['team'] = $action->match_team->toArray();

            if (!empty($action->match_team->image)) {
                $actionArr['team']['image_url'] = env('MEDIA_END').$action->match_team->image;
            }else{
                $actionArr['team']['image_url'] = env('MEDIA_END').'default-team.png';
            }

            $actionArr['team']['league'] = $season->league->toArray();
            $actionArr['team']['league']['province'] = $season->league->league_province->first()->province->toArray();
            $actionArr['team']['league']['season'] = null;

            if(!empty($action->match_player)){

                $actionArr['match_player'] = $action->match_player->toArray();

                if(empty($action->match_player->guest_name)){
                    
                    $actionArr['match_player']['fullname'] = $action->match_player->player->user->first_name.' '.$action->match_player->player->user->last_name;
                    $actionArr['match_player']['is_guest'] = false;
                    $actionArr['match_player']['player'] = $action->match_player->player->id;
                    $actionArr['match_player']['value'] = (int)$action->match_player->player->value;
                    $actionArr['match_player']['old_system_value'] = (int)$action->match_player->player->old_system_value;
                    if (!empty($action->match_player->player->user->image)) {
                        $actionArr['match_player']['image_url'] =env('MEDIA_END').$action->match_player->player->user->image;
                    }else{
                        $actionArr['match_player']['image_url'] = env('MEDIA_END').'default-player.png';
                    }

                }else{

                    $actionArr['match_player']['fullname'] = $action->match_player->guest_name;
                    $actionArr['match_player']['is_guest'] = true;
                    $actionArr['match_player']['player'] = null;
                    $actionArr['match_player']['value'] = null;
                    $actionArr['match_player']['image_url'] =env('MEDIA_END').'default-player.png';

                }

                $actionArr['match_player']['position'] = $action->match_player->player_position->toArray();

                $actionArr['match_player']['goal'] = $action->match_player->match_action_goal->count();
                $actionArr['match_player']['red_card'] = $action->match_player->match_action_red_card->count();
                $actionArr['match_player']['yellow_card'] = $action->match_player->match_action_yellow_card->count();

                $actionArr['match_player']['team'] = $action->match_team->toArray();

                if (!empty($action->match_team->image)) {
                    $actionArr['match_player']['team']['image_url'] = env('MEDIA_END').$action->match_team->image;
                }else{
                    $actionArr['match_player']['team']['image_url'] = env('MEDIA_END').'default-team.png';
                }
                $actionArr['match_player']['team']['league'] = $season->league->toArray();
                $actionArr['match_player']['team']['league']['province'] = $season->league->league_province->first()->province->toArray();
                $actionArr['match_player']['team']['league']['season'] = null;


                $actionArr['match_player']['match'] = $match->toArray();
                $actionArr['match_player']['match']['video_status'] = (!empty($match->match_video)) ? $match->match_video->status : 'recordable';

                $actionArr['match_player']['match']['team1'] = $match->team1->toArray();

                if (!empty($match->team1->image)) {
                    $actionArr['match_player']['match']['team1']['image_url'] = env('MEDIA_END').$match->team1->image;
                }else{
                    $actionArr['match_player']['match']['team1']['image_url'] = env('MEDIA_END').'default-team.png';
                }

                $actionArr['match_player']['match']['team1']['league'] = $season->league->toArray();
                //$actionArr['match_player']['match']['team1']['league']['province'] = $season->league->league_province->first()->province->toArray();
                $actionArr['match_player']['match']['team1']['league']['season'] = $season->league->seasonApi->first()->toArray();

                if($season->league->seasonApi->first()->polymorphic_ctype_id == 32){
                    $actionArr['match_player']['match']['team1']['league']['season']['type'] = 'elimination';
                }else if($season->league->seasonApi->first()->polymorphic_ctype_id == 33){
                    $actionArr['match_player']['match']['team1']['league']['season']['type'] = 'fixture';
                }else if($season->league->seasonApi->first()->polymorphic_ctype_id == 34){
                    $actionArr['match_player']['match']['team1']['league']['season']['type'] = 'point';
                }
                $actionArr['match_player']['match']['team1']['league']['season']['fullname'] = $season->league->seasonApi->first()->year.' '.$season->league->name.' '.$season->league->seasonApi->first()->name;


                $actionArr['match_player']['match']['team2'] = $match->team2->toArray();

                if (!empty($match->team2->image)) {
                    $actionArr['match_player']['match']['team2']['image_url'] = env('MEDIA_END').$match->team2->image;
                }else{
                    $actionArr['match_player']['match']['team2']['image_url'] = env('MEDIA_END').'default-team.png';
                }

                $actionArr['match_player']['match']['team2']['league'] = $season->league->toArray();
                //$actionArr['match_player']['match']['team2']['league']['province'] = $season->league->league_province->first()->province->toArray();
                $actionArr['match_player']['match']['team2']['league']['season'] = $season->league->seasonApi->first()->toArray();

                if($season->league->seasonApi->first()->polymorphic_ctype_id == 32){
                    $actionArr['match_player']['match']['team2']['league']['season']['type'] = 'elimination';
                }else if($season->league->seasonApi->first()->polymorphic_ctype_id == 33){
                    $actionArr['match_player']['match']['team2']['league']['season']['type'] = 'fixture';
                }else if($season->league->seasonApi->first()->polymorphic_ctype_id == 34){
                    $actionArr['match_player']['match']['team2']['league']['season']['type'] = 'point';
                }
                $actionArr['match_player']['match']['team2']['league']['season']['fullname'] = $season->league->seasonApi->first()->year.' '.$season->league->name.' '.$season->league->seasonApi->first()->name;

                $actionArr['match_player']['match']['match_action'] = null;


                $actionArr['fullname'] = $actionArr['match_player']['fullname'];
                $actionArr['image_url'] = $actionArr['match_player']['image_url'];

            }else{
                
                $actionArr['match_player']['image_url'] = env('MEDIA_END').'default-player.png';
                $actionArr['image_url'] = $actionArr['match_player']['image_url'];
            }
            
            if(!empty($actionArr['seconds'])){
                $value = $actionArr['minute'];
                $actionArr['minute'] = Carbon::createFromTime(00, 00, 00)->addSeconds($value)->format('i:s');
            }

            $data[] = $actionArr;
        }

        return response()->json($data, 200);
    }

    public function matchPlayer($id, Request $request)
    {

        $data = array();
        
        $match = Match::select('*', 'season_id as season')->where('id', $id)->first();
        $season = Season::find($match->season_id);

        foreach ($match->match_player->where('team_id', $request->input('teamId')) as $player) {
            
            $playerArr = $player->toArray();

            $playerArr['team'] = $player->team->toArray();

            if (!empty($player->team->image)) {
                $playerArr['team']['image_url'] = env('MEDIA_END').$player->team->image;
            }else{
                $playerArr['team']['image_url'] = env('MEDIA_END').'default-team.png';
            }
            $playerArr['team']['league'] = $season->league->toArray();
            $playerArr['team']['league']['province'] = $season->league->league_province->first()->province->toArray();
            $playerArr['team']['league']['season'] = null;

            $playerArr = $player->toArray();

            if(empty($player->guest_name)){
                
                $playerArr['fullname'] = $player->player->user->first_name.' '.$player->player->user->last_name;
                $playerArr['is_guest'] = false;
                $playerArr['player'] = $player->player->id;
                $playerArr['value'] = (int)$player->player->value;
                $playerArr['old_system_value'] = (int)$player->player->old_system_value;
                
                if (!empty($player->player->user->image)) {
                    $playerArr['image_url'] =env('MEDIA_END').$player->player->user->image;
                }else{
                    $playerArr['image_url'] = env('MEDIA_END').'default-player.png';
                }

            }else{

                $playerArr['fullname'] = $player->guest_name;
                $playerArr['is_guest'] = true;
                $playerArr['player'] = null;
                $playerArr['value'] = null;
                $playerArr['image_url'] =env('MEDIA_END').'default-player.png';

            }

            $playerArr['position'] = $player->player_position->toArray();

            $playerArr['goal'] = $player->match_action_goal->count();
            $playerArr['red_card'] = $player->match_action_red_card->count();
            $playerArr['yellow_card'] = $player->match_action_yellow_card->count();
            if($player->position_id == 1){
                $playerArr['saving'] = $player->match->match_action->where('type', 'saving')->where('team_id', $player->team_id)->count();
            }else{
                $playerArr['saving'] = 0;
            }

            $playerArr['team'] = $player->team->toArray();

            
            if (!empty($player->team->image)) {
                $playerArr['team']['image_url'] = env('MEDIA_END').$player->team->image;
            }else{
                $playerArr['team']['image_url'] = env('MEDIA_END').'default-team.png';
            }

            $playerArr['team']['league'] = $season->league->toArray();
            $playerArr['team']['league']['province'] = $season->league->league_province->first()->province->toArray();
            $playerArr['team']['league']['season'] = null;


            $playerArr['match'] = $match->toArray();
            $playerArr['match']['video_status'] = (!empty($match->match_video)) ? $match->match_video->status : 'recordable';

            $playerArr['match']['team1'] = $match->team1->toArray();
            
            if (!empty($match->team1->image)) {
                $playerArr['match']['team1']['image_url'] = env('MEDIA_END').$match->team1->image;
            }else{
                $playerArr['match']['team1']['image_url'] = env('MEDIA_END').'default-team.png';
            }

            $playerArr['match']['team1']['league'] = $season->league->toArray();
            //$playerArr['match']['team1']['league']['province'] = $season->league->league_province->first()->province->toArray();
            $playerArr['match']['team1']['league']['season'] = $season->league->seasonApi->first()->toArray();

            if($season->league->seasonApi->first()->polymorphic_ctype_id == 32){
                $playerArr['match']['team1']['league']['season']['type'] = 'elimination';
            }else if($season->league->seasonApi->first()->polymorphic_ctype_id == 33){
                $playerArr['match']['team1']['league']['season']['type'] = 'fixture';
            }else if($season->league->seasonApi->first()->polymorphic_ctype_id == 34){
                $playerArr['match']['team1']['league']['season']['type'] = 'point';
            }
            $playerArr['match']['team1']['league']['season']['fullname'] = $season->league->seasonApi->first()->year.' '.$season->league->name.' '.$season->league->seasonApi->first()->name;


            $playerArr['match']['team2'] = $match->team2->toArray();
            
            if (!empty($match->team2->image)) {
                $playerArr['match']['team2']['image_url'] = env('MEDIA_END').$match->team2->image;
            }else{
                $playerArr['match']['team2']['image_url'] = env('MEDIA_END').'default-team.png';
            }
            $playerArr['match']['team2']['league'] = $season->league->toArray();
            //$playerArr['match']['team2']['league']['province'] = $season->league->league_province->first()->province->toArray();
            $playerArr['match']['team2']['league']['season'] = $season->league->seasonApi->first()->toArray();

            if($season->league->seasonApi->first()->polymorphic_ctype_id == 32){
                $playerArr['match']['team2']['league']['season']['type'] = 'elimination';
            }else if($season->league->seasonApi->first()->polymorphic_ctype_id == 33){
                $playerArr['match']['team2']['league']['season']['type'] = 'fixture';
            }else if($season->league->seasonApi->first()->polymorphic_ctype_id == 34){
                $playerArr['match']['team2']['league']['season']['type'] = 'point';
            }
            $playerArr['match']['team2']['league']['season']['fullname'] = $season->league->seasonApi->first()->year.' '.$season->league->name.' '.$season->league->seasonApi->first()->name;

            $playerArr['fullname'] = $playerArr['fullname'];
            $playerArr['image_url'] = $playerArr['image_url'];

            $playerArr['match']['match_player'] = null;
            $playerArr['match']['match_video'] = null;
            $playerArr['team']['league']['league_province'] = null;
            $playerArr['team']['league']['season_api'] = null;
            
            $data[] = $playerArr;
        }

        return response()->json($data, 200);
    }
    
    public function matchPhoto($id)
    {

        $data = array();
        
        $match = Match::select('*', 'season_id as season')->where('id', $id)->first();
        $season = Season::find($match->season_id);

        foreach ($match->match_image as $image) {
            $imageArr = $image->toArray();

            $imageArr['image'] = env('MEDIA_END').$image->image;

            $data[] = $imageArr;
        }

        return response()->json($data, 200);
    }
    
    public function matchPanorama($id)
    {

        $data = array();
        
        $match = Match::select('*', 'season_id as season')->where('id', $id)->first();
        $season = Season::find($match->season_id);

        foreach ($match->match_panorama as $panoramas) {
            $panoramaArr = $panoramas->toArray();
            $panoramaArr['panorama'] = $panoramas->panorama_type;

            $panoramaArr['match_player'] = $panoramas->match_player_panorama->toArray();

            if(empty($panoramas->match_player_panorama->guest_name)){
                
                $panoramaArr['match_player']['fullname'] = $panoramas->match_player_panorama->player->user->first_name.' '.$panoramas->match_player_panorama->player->user->last_name;
                $panoramaArr['match_player']['is_guest'] = false;
                $panoramaArr['match_player']['player'] = $panoramas->match_player_panorama->player->id;
                $panoramaArr['match_player']['value'] = $panoramas->match_player_panorama->player->value;
                
                if(!empty($panoramas->match_player_panorama->player->user->image)){
                    $panoramaArr['match_player']['image_url'] = env('MEDIA_END').$panoramas->match_player_panorama->player->user->image;
                }else{
                    $panoramaArr['match_player']['image_url'] = env('MEDIA_END').'default-player.png';
                }

            }else{

                $panoramaArr['match_player']['fullname'] = $panoramas->match_player_panorama->guest_name;
                $panoramaArr['match_player']['is_guest'] = true;
                $panoramaArr['match_player']['player'] = null;
                $panoramaArr['match_player']['value'] = null;
                $panoramaArr['match_player']['image_url'] =env('MEDIA_END').'default-player.png';

            }

            $panoramaArr['match_player']['position'] = $panoramas->match_player_panorama->player_position->toArray();

            $panoramaArr['match_player']['goal'] = $panoramas->match_player_panorama->match_action_goal->count();
            $panoramaArr['match_player']['red_card'] = $panoramas->match_player_panorama->match_action_red_card->count();
            $panoramaArr['match_player']['yellow_card'] = $panoramas->match_player_panorama->match_action_yellow_card->count();

            if($panoramas->match_player_panorama->position_id == 1){
                $panoramaArr['match_player']['saving'] = $panoramas->match_player_panorama->match->match_action->where('type', 'saving')->where('team_id', $panoramas->match_player_panorama->team_id)->count();
            }else{
                $panoramaArr['match_player']['saving'] = 0;
            }

            $panoramaArr['match_player']['team'] = $panoramas->match_player_panorama->team->toArray();
            
            if (!empty($panoramas->match_player_panorama->team->image)) {
                $panoramaArr['match_player']['team']['image_url'] = env('MEDIA_END').$panoramas->match_player_panorama->team->image;
            }else{
                $panoramaArr['match_player']['team']['image_url'] = env('MEDIA_END').'default-team.png';
            }

            $panoramaArr['match_player']['team']['league'] = $panoramas->match_player_panorama->team->league->toArray();
            $panoramaArr['match_player']['team']['league']['province'] = $panoramas->match_player_panorama->team->league->league_province->first()->province->toArray();
            $panoramaArr['match_player']['team']['league']['season'] = null;

            $panoramaArr['embed_url'] = $panoramas->embed_code;

            $data[] = $panoramaArr;
        }


        return response()->json($data, 200);
    }
    
    public function matchPressConference($id)
    {

        $data = array();
        
        $match = Match::select('*', 'season_id as season')->where('id', $id)->first();
        $season = Season::find($match->season_id);

        if(!empty($match->match_press)){

            $data = $match->match_press->toArray();

            $data['embed_url'] = null;
            $data['embed_code'] = $match->match_press->embed_code;
            if ($match->match_press->embed_source == 'app'){
                $data['embed_url'] = $match->match_press->embed_code;
            }else if ($match->match_press->embed_source == 'vimeo'){
                $data['embed_url'] = 'https://player.vimeo.com/video/'.$match->match_press->embed_code;
            }else if ($match->match_press->embed_source == 'youtube'){
                $data['embed_url'] = 'https://www.youtube.com/embed/'.$match->match_press->embed_code;
            }

            if ($match->match_press->embed_source == 'app'){
                $data['embed_image_url'] = url('default.jpg');
            }else if ($match->match_press->embed_source == 'vimeo'){
                $data['embed_image_url'] = url('default.jpg');
            }else if ($match->match_press->embed_source == 'youtube'){
                $data['embed_image_url'] = 'https://i3.ytimg.com/vi/'.$match->match_press->embed_code.'/hqdefault.jpg';
            }

        

            $matchArr = $match->toArray();
            $matchArr['video_status'] = (!empty($match->match_video)) ? $match->match_video->status : 'recordable';

            $matchArr['team1'] = $match->team1->toArray();
            
            if (!empty($match->team1->image)) {
                $matchArr['team1']['image_url'] = env('MEDIA_END').$match->team1->image;
            }else{
                $matchArr['team1']['image_url'] = env('MEDIA_END').'default-team.png';
            }

            $matchArr['team1']['league'] = $season->league->toArray();
            //$matchArr['team1']['league']['province'] = $season->league->league_province->first()->province->toArray();
            $matchArr['team1']['league']['season'] = $season->league->seasonApi->first()->toArray();

            if($season->league->seasonApi->first()->polymorphic_ctype_id == 32){
                $matchArr['team1']['league']['season']['type'] = 'elimination';
            }else if($season->league->seasonApi->first()->polymorphic_ctype_id == 33){
                $matchArr['team1']['league']['season']['type'] = 'fixture';
            }else if($season->league->seasonApi->first()->polymorphic_ctype_id == 34){
                $matchArr['team1']['league']['season']['type'] = 'point';
            }
            $matchArr['team1']['league']['season']['fullname'] = $season->league->seasonApi->first()->year.' '.$season->league->name.' '.$season->league->seasonApi->first()->name;


            $matchArr['team2'] = $match->team2->toArray();

            if (!empty($match->team2->image)) {
                $matchArr['team2']['image_url'] = env('MEDIA_END').$match->team2->image;
            }else{
                $matchArr['team2']['image_url'] = env('MEDIA_END').'default-team.png';
            }

            $matchArr['team2']['league'] = $season->league->toArray();
            //$matchArr['team2']['league']['province'] = $season->league->league_province->first()->province->toArray();
            $matchArr['team2']['league']['season'] = $season->league->seasonApi->first()->toArray();

            if($season->league->seasonApi->first()->polymorphic_ctype_id == 32){
                $matchArr['team2']['league']['season']['type'] = 'elimination';
            }else if($season->league->seasonApi->first()->polymorphic_ctype_id == 33){
                $matchArr['team2']['league']['season']['type'] = 'fixture';
            }else if($season->league->seasonApi->first()->polymorphic_ctype_id == 34){
                $matchArr['team2']['league']['season']['type'] = 'point';
            }
            $matchArr['team2']['league']['season']['fullname'] = $season->league->seasonApi->first()->year.' '.$season->league->name.' '.$season->league->seasonApi->first()->name;

            $matchArr['league']['id'] = $season->league->id;
            $matchArr['league']['name'] = $season->league->name;

            if(!empty($match->coordinator)){
                $matchArr['coordinator'] = $match->coordinator->toArray();
                $matchArr['coordinator']['fullname'] = $match->coordinator->user->first_name.' '.$match->coordinator->user->last_name;

                if (!empty($match->coordinator->user->image)) {
                    $matchArr['coordinator']['image_url'] = env('MEDIA_END').$match->coordinator->user->image;
                }else{
                    $matchArr['coordinator']['image_url'] = env('MEDIA_END').'default-player.png';
                }
            }else{
                $matchArr['coordinator'] = null;
            }

            if(!empty($match->referee)){
                $matchArr['referee'] = $match->referee->toArray();
                $matchArr['referee']['fullname'] = $match->referee->user->first_name.' '.$match->referee->user->last_name;

                if (!empty($match->referee->user->image)) {
                    $matchArr['referee']['image_url'] = env('MEDIA_END').$match->referee->user->image;
                }else{
                    $matchArr['referee']['image_url'] = env('MEDIA_END').'default-player.png';
                }
            }else{
                $matchArr['referee'] = null;
            }

            if(!empty($match->ground)){
                $matchArr['ground'] = $match->ground->toArray();
            }else{
                $matchArr['ground'] = null;
            }

            $data['match'] = $matchArr;

            //$data['team'] = null;
            $data['players'] = array();
        }else{
            $data = false;
        }

        return response()->json($data, 200);
    }

    public function leaGalleries($id)
    {
        $data = array();
        
        return response()->json($data, 200);
    }

    public function leaHomeGalleries($id)
    {
        $data = array();
        
        //where('league_id', $id)->;
        $leagueGalleries = LeagueGalleries::where('league_id', $id)->where('show_home', true)->select('id','name')->get();

        foreach ($leagueGalleries as $leagueGallery) {

            //$leagueGalleryItems = $leagueGallery->league_gallery_items;
            $leagueGallery->items;
            
            foreach ($leagueGallery->items as $leagueGalleryItem) {

                if ($leagueGalleryItem->embed_source == 'app'){
                    $leagueGalleryItem->embed_url = $leagueGalleryItem->embed_code;
                }else if ($leagueGalleryItem->embed_source == 'vimeo'){
                    $leagueGalleryItem->embed_url = 'https://player.vimeo.com/video/'.$leagueGalleryItem->embed_code;
                }else if ($leagueGalleryItem->embed_source == 'youtube'){
                    $leagueGalleryItem->embed_url = 'https://www.youtube.com/embed/'.$leagueGalleryItem->embed_code;
                }

            }
            
        }

        return response()->json($leagueGalleries, 200);
    }

    public function leaChampionships($id, Request $request)
    {

        $data = array();
        
        $seasons = Season::select('id')->where('league_id', $id)->orderBy('end_date','desc')->get();
        $seasonIds = array();

        foreach ($seasons as $season) {
            $seasonIds[] = $season->id;
        }

        $championships = Championships::whereIn('season_id', $seasonIds);

        if(!empty($request->input('limit'))){
            $championships = $championships->limit($request->input('limit'));
            //$championships = $championships->limit(1);
        }

        $championships = $championships->orderBy('year','desc')->get();

        foreach ($championships as $championship) {
            
            $arr = array();
            $arr = $championship->toArray();
            
            $arr['season'] = $championship->season->toArray();
            $arr['season']['fullname'] = $championship->season->year.' '.$championship->season->league->name.' '.$championship->season->name;

            if($championship->season->polymorphic_ctype_id == 32){
                $arr['season']['type'] = 'elimination';
            }else if($championship->season->polymorphic_ctype_id == 33){
                $arr['season']['type'] = 'fixture';
            }else if($championship->season->polymorphic_ctype_id == 34){
                $arr['season']['type'] = 'point';
            }
            
            $arr['team'] = $championship->team->toArray();

            if (!empty($championship->team->image)) {
                $arr['team']['image_url'] = env('MEDIA_END').$championship->team->image;
            }else{
                $arr['team']['image_url'] = env('MEDIA_END').'default-team.png';
            }
            
            $data[] = $arr;
        }

        return response()->json($data, 200);
    }

    public function leaTransfers($id, Request $request)
    {

        $data = array();

        $_season_id = $request->input('season_id');
        $_page = $request->input('page');

        $_season_id_str = '';
        $_page_next_str = '';
        $_page_prev_str = '';

        $transfers = Transfer::select('transfers.*', 'transfers_seasons.season_id')->where('status', 'accept')->leftJoin('transfers_seasons', 'transfers.id', '=', 'transfers_seasons.transfer_id')->leftJoin('players', 'players.id', '=', 'transfers.requested_player_id')->leftJoin('users', 'users.id', '=', 'players.user_id')->where('users.is_active', true);

        if(!empty($_season_id)){
            $transfers = $transfers->where('transfers_seasons.season_id', $_season_id);
            $_season_id_str = 'season_id='.$_season_id;
        }

        $data['count'] = $transfers->count();

        $page = 1;
        $pageCount = 12;

        if(!empty($_page)){
            $page = $_page;
            $offset = ($page-1)*$pageCount;
            $transfers = $transfers->offset($offset);

            $_page_next_str = 'page='.($page+1);
            $_page_prev_str = 'page='.($page-1);
        }else{
            $_page_next_str = 'page=2';
        }

        $data['next'] = env('APP_URL').'api/leagues/'.$id.'/transfers/?'.$_page_next_str.'&'.$_season_id_str;
        if($page > 1){
            $data['previous'] = env('APP_URL').'api/leagues/'.$id.'/transfers/?'.$_page_prev_str.'&'.$_season_id_str;
        }else{
            $data['previous'] = null;
        }

        

        $transfers = $transfers->limit($pageCount)->orderby('transfers.replied_at', 'desc')->get();
        $data['results'] = array();

        foreach ($transfers as $transfer) {

            $tranArr = $transfer->toArray();

            if(!empty($transfer->requesting_team)){
                $tranArr['requesting_team'] = $transfer->requesting_team->toArray();
                if(!empty($transfer->requesting_team->image)){
                    $tranArr['requesting_team']['image_url'] = env('MEDIA_END').$transfer->requesting_team->image;
                }else{
                    $tranArr['requesting_team']['image_url'] = env('MEDIA_END').'default-team.png';
                }
                if(!empty($transfer->transfer_season->season->league)){
                    $tranArr['requesting_team']['league'] = $transfer->transfer_season->season->league->toArray();
                    $tranArr['requesting_team']['league']['province'] = $transfer->transfer_season->season->league->league_province->first()->province->toArray();
                    $tranArr['requesting_team']['league']['season'] = null;
                }else{
                    $tranArr['requesting_team']['league'] = null;
                }
            }else{
                $tranArr['requesting_team'] = null;
            }

            if(!empty($transfer->requested_team)){
                $tranArr['requested_team'] = $transfer->requested_team->toArray();
                if(!empty($transfer->requested_team->image)){
                    $tranArr['requested_team']['image_url'] = env('MEDIA_END').$transfer->requested_team->image;
                }else{
                    $tranArr['requested_team']['image_url'] = env('MEDIA_END').'default-team.png';
                }
                if(!empty($transfer->transfer_season->season->league)){
                    $tranArr['requested_team']['league'] = $transfer->transfer_season->season->league->toArray();
                    $tranArr['requested_team']['league']['province'] = $transfer->transfer_season->season->league->league_province->first()->province->toArray();
                    $tranArr['requested_team']['league']['season'] = null;
                }else{
                    $tranArr['requested_team']['league'] = null;
                }
            }else{
                $tranArr['requested_team'] = null;
            }




            $seasonTemp = $transfer->transfer_season->season;

            $seasonTempArr = $seasonTemp->toArray();
            $seasonTempArr['fullname'] = $seasonTemp->league->seasonApi->first()->year.' '.$seasonTemp->league->name.' '.$seasonTemp->league->seasonApi->first()->name;

            if($seasonTemp->polymorphic_ctype_id == 32){
                $seasonTempArr['type'] = 'elimination';
            }else if($seasonTemp->polymorphic_ctype_id == 33){
                $seasonTempArr['type'] = 'fixture';
            }else if($seasonTemp->polymorphic_ctype_id == 34){
                $seasonTempArr['type'] = 'point';
            }

            $tranArr['seasons'][] = $seasonTempArr;



            $tranArr['requested_player'] = $transfer->requested_player->toArray();
            $tranArr['requested_player']['position'] = (!empty($transfer->requested_player->players_position)) ? $transfer->requested_player->players_position->toArray() : null;
            $tranArr['requested_player']['position2'] = (!empty($transfer->requested_player->players_position2)) ? $transfer->requested_player->players_position2->toArray() : null;
            $tranArr['requested_player']['position3'] = (!empty($transfer->requested_player->players_position3)) ? $transfer->requested_player->players_position3->toArray() : null;
            $tranArr['requested_player']['fullname'] = $transfer->requested_player->user->first_name.' '.$transfer->requested_player->user->last_name;
            if(!empty($user->image)){
                $tranArr['requested_player']['image_url'] = env('MEDIA_END').$transfer->requested_player->user->image;
            }else{
                $tranArr['requested_player']['image_url'] = env('MEDIA_END').'default-player.png';
            }

            $tranArr['requested_player']['value'] = (int)$transfer->requested_player->value;
            $tranArr['requested_player']['old_system_value'] = (int)$transfer->requested_player->old_system_value;

            if(!empty($transfer->requested_player->team)){
                $tranArr['requested_player']['team'] = $transfer->requested_player->team->toArray();

                if(!empty($transfer->requested_player->team->image)){
                    $tranArr['requested_player']['team']['image_url'] = env('MEDIA_END').$transfer->requested_player->team->image;
                }else{
                    $tranArr['requested_player']['team']['image_url'] = env('MEDIA_END').'default-team.png';
                }

                $tranArr['requested_player']['team']['league'] = $transfer->requested_player->team->league->toArray();
            }else{
                $tranArr['requested_player']['team'] = null;
            }


            $data['results'][] = $tranArr;
        }


        return response()->json($data, 200); 
    }

    public function leaTransferMarket($id, Request $request)
    {
        
        $league = League::select('*')->find($id);
        $league_province = $league->league_province;
        
        $provinceIds = array();

        foreach ($league_province as $province) {
            $provinceIds[] = $province->province_id;
        }


        $data = array();

        $_page = $request->input('page');
        $_name = str_replace('+', ' ', $request->input('name'));
        $_age = $request->input('age');
        $_district = $request->input('district');
        $_position = $request->input('position');
        $_has_team = $request->input('has_team');
        

        $_page_next_str = '';
        $_page_prev_str = '';

        $_name_str = '';
        $_age_str = '';
        $_district_str = '';
        $_position_str = '';
        $_has_team_str = '';

    
        $users = User::select(
            'users.id',
            'users.first_name',
            'users.last_name',
            DB::raw("CONCAT(users.first_name, ' ', users.last_name) as fullname"),
            'users.image',
            'users.created_at',

            'players.id as player_id',
            'players.number',
            'players.foot',
            'players.weight',
            'players.height',
            'players.facebook',
            'players.twitter',
            'players.instagram',
            'players.value',
            'players.old_system_value',
            'players.team_id',

            'profiles.province_id'
            
        )->leftJoin('players', 'users.id', '=', 'players.user_id')->leftJoin('profiles', 'users.id', '=', 'profiles.user_id')->whereNotNull('players.id')->whereIn('profiles.province_id', $provinceIds)->where('users.is_active', true);


        if(!is_null($_has_team)){
            $_has_team_str = '&has_team='.$_has_team;

            if($_has_team == '0'){
                $users = $users->whereNull('players.team_id');
            }else{
                $users = $users->whereNotNull('players.team_id');
            }
        }
        
        if(!empty($_position)){
            $_position_str = '&position='.$_position;

            $users = $users->where(function ($query) use ($_position) {
                $query->where('players.position_id', $_position)->orWhere('players.position2_id', $_position)->orWhere('players.position3_id', $_position);
            });
        }

        if(!empty($_district)){
            $_district_str = '&district='.$_district;
            $users = $users->where('profiles.district_id', $_district);
        }

        if(!empty($_age)){
            $_age_str = '&age='.$_age;

            $_ageBetween = explode('-', $_age);

            if(!empty($_ageBetween[0])){
                $_ageBetween[0] = Carbon::now()->subYears($_ageBetween[0])->format('Y-m-d');
                $users = $users->where('profiles.birth_date', '<=', $_ageBetween[0]);
            }

            if(!empty($_ageBetween[1])){
                $_ageBetween[1] = Carbon::now()->subYears($_ageBetween[1]+1)->format('Y-m-d');
                $users = $users->where('profiles.birth_date', '>=', $_ageBetween[1]);
            }
            

        }
        
        if(!empty($_name)){
            $users = $users->where(function ($query) use ($_name) {
                $query->where(DB::raw("REPLACE(CONCAT(users.first_name, ' ', users.last_name),'  ',' ')"), 'ilike', '%'.$_name.'%');
                $_name_str = '&name='.$_name;
            });
        }


        $data['count'] = $users->count();
        
        $page = 1;
        $pageCount = 12;

        if(!empty($_page)){
            $page = $_page;
            $offset = ($page-1)*$pageCount;
            $users = $users->offset($offset);

            $_page_next_str = 'page='.($page+1);
            $_page_prev_str = 'page='.($page-1);
        }else{
            $_page_next_str = 'page=2';
        }
                                                    //page=1      name=an%C4%B1l     age=16-25     district=1     position=1     has_team=1
        $data['next'] = env('APP_URL').'api/leagues/1/transfer_market/?'.$_page_next_str.$_name_str.$_age_str.$_district_str.$_position_str.$_has_team_str;
        if($page > 1){
            $data['previous'] = env('APP_URL').'api/leagues/1/transfer_market/?'.$_page_prev_str.$_name_str.$_age_str.$_district_str.$_position_str.$_has_team_str;
        }else{
            $data['previous'] = null;
        }

        $users = $users->limit($pageCount)->orderBy('value', 'desc')->get();

        $data['results'] = array();
        foreach ($users as $user) {
            $userArr = $user->player->toArray();

            $userArr['user'] = $user->toArray();

            array_forget($userArr, 'user.username');
            array_forget($userArr, 'user.email');
            array_forget($userArr, 'user.email_confirmed');
            array_forget($userArr, 'user.infoshare');
            array_forget($userArr, 'user.is_staff');
            array_forget($userArr, 'user.is_superuser');
            array_forget($userArr, 'user.last_login');
            array_forget($userArr, 'user.phone');
            array_forget($userArr, 'user.phone_confirmed');
            array_forget($userArr, 'user.phone_code');

            $userArr['user']['age'] = (!empty($user->profile->birth_date)) ? Carbon::parse($user->profile->birth_date)->age : null;

            if(!empty($user->image)){
                $userArr['user']['image_url'] = env('MEDIA_END').$user->image;
            }else{
                $userArr['user']['image_url'] = env('MEDIA_END').'default-player.png';
            }

            $userArr['user']['profile'] = $user->profile->toArray();

            array_forget($userArr, 'user.profile.identity_number');

            $userArr['user']['profile']['province'] = (!empty($user->profile->province)) ? $user->profile->province->toArray() : null;
            $userArr['user']['profile']['district'] = (!empty($user->profile->district)) ? $user->profile->district->toArray() : null;

            $userArr['position'] = (!empty($user->player->players_position)) ? $user->player->players_position->toArray() : null;
            $userArr['position2'] = (!empty($user->player->players_position2)) ? $user->player->players_position2->toArray() : null;
            $userArr['position3'] = (!empty($user->player->players_position3)) ? $user->player->players_position3->toArray() : null;

            $userArr['value'] = (int)$user->player->value;
            $userArr['old_system_value'] = (int)$user->player->old_system_value;

            if(!empty($user->player->team)){
                $userArr['team'] = $user->player->team->toArray();
                
                if (!empty($user->player->team->image)) {
                    $userArr['team']['image_url'] = env('MEDIA_END').$user->player->team->image;
                }else{
                    $userArr['team']['image_url'] = env('MEDIA_END').'default-team.png';
                }
                
                if(!empty($user->player->team->league)){
                    $userArr['team']['league'] = $user->player->team->league->toArray();
                    $userArr['team']['league']['province'] = $user->player->team->league->league_province->first()->province->toArray();
                    $userArr['team']['league']['season'] = null;
                }

            }else{
                $userArr['team'] = null;
            }

            $data['results'][] = $userArr;
        }


        return response()->json($data, 200);
    }

    public function captainTransfer(Request $request)
    {
        $data = array();

        $captain = Auth::user()->player;
        $team = Auth::user()->player->team;

        /***** Aktif Sezon gg*****/
        $activeSeason = null;
        $activeSeasons = array();
        $existSeasons = array();
        $t_seasons = Auth::user()->player->team->league->seasonApi;

        foreach ($t_seasons as $t_season) {
            if($t_season->active == true){
                if($t_season->polymorphic_ctype_id != 32){
                    if($t_season->start_date <= Carbon::now()->format('Y-m-d') && $t_season->end_date >= Carbon::now()->format('Y-m-d')){
                        $activeSeasons[] = $t_season;
                    }
                    $existSeasons[] = $t_season;
                }
            }
        }

        if(count($activeSeasons) > 1){
            foreach ($activeSeasons as $season) {
                if($season->polymorphic_ctype_id == 33){

                    $fs_group_teams_id = array();
                    foreach ($season->league_fixtureseason->groups as $group) {
                        foreach ($group->group_teams as $group_team) {
                            $fs_group_teams_id[] = $group_team->team_id;
                        }
                    }
                    if(array_search($team->id, $fs_group_teams_id)){
                        $activeSeason = $season;
                    }

                }
            }
        }else{
            if(count($existSeasons)){
                $activeSeason = $existSeasons[0];
            }
        }

        if(empty($activeSeason)){
            $activeSeason = $activeSeasons[0];
        }elseif(!empty($activeSeason)){

            if(!empty($team->team_teamseason->where('season_id', $activeSeason->id)->first())){
                $remaining_transfer_count = $team->team_teamseason->where('season_id', $activeSeason->id)->first()->remaining_transfer_count;
                $squad_locked = $team->team_teamseason->where('season_id', $activeSeason->id)->first()->squad_locked;
            }else{
                $remaining_transfer_count = $activeSeason->allowed_transfer_count;
                $squad_locked = false;
            }
        }
        /***** Aktif Sezon gg*****/

        if(!empty($activeSeason)){

            $transfers = Transfer::where('requesting_team_id', $captain->team_id)->get();

            foreach ($transfers as $transfer) {
                $transferArr = array();
                
                $transferArr['id'] = $transfer->id;

                if(!empty($transfer->requesting_captain)){
                    $transferArr['requesting_captain_id'] = $transfer->requesting_captain->id;
                    $transferArr['requesting_captain'] = $transfer->requesting_captain;
                }else{
                    $transferArr['requesting_captain_id'] = null;
                    $transferArr['requesting_captain'] = null;
                }

                if(!empty($transfer->requesting_team)){
                    $transferArr['requesting_team_id'] = $transfer->requesting_team->id;
                    $transferArr['requesting_team'] = $transfer->requesting_team;
                }else{
                    $transferArr['requesting_team_id'] = $transfer->requesting_team->id;
                    $transferArr['requesting_team'] = $transfer->requesting_team;
                }

                $transferArr['requested_player_id'] = $transfer->requested_player->id;
                $transferArr['requested_player'] = $transfer->requested_player;

                if (!empty($transfer->requested_player->user->image)) {
                    $transferArr['requested_player']['image_url'] = env('MEDIA_END').$transfer->requested_player->user->image;
                }else{
                    $transferArr['requested_player']['image_url'] = env('MEDIA_END').'default-player.png';
                }

                $transferArr['requested_player']['fullname'] = $transfer->requested_player->user->first_name.' '.$transfer->requested_player->user->last_name;
                $transferArr['requested_player']['position'] = $transfer->requested_player->players_position;

                $transferArr['requested_team'] = $transfer->requested_team;
                $transferArr['seasons'] = $transfer->transfer_season;
                $transferArr['status'] = $transfer->status;
                $transferArr['replied_at'] = $transfer->updated_at;
                $transferArr['created_at'] = $transfer->created_at;

                $data[] = $transferArr;
            }
        }
        
        return response()->json($data, 200);
    }

    public function captainTransferPost(Request $request)
    {
        $data = array();

        $captain = Auth::user()->player;
        $team = Auth::user()->player->team;
        $requested_player = Player::find($request->input('requested_player_id'));

        /***** Aktif Sezon gg*****/
        $activeSeason = null;
        $activeSeasons = array();
        $existSeasons = array();
        $t_seasons = Auth::user()->player->team->league->seasonApi;

        foreach ($t_seasons as $t_season) {
            if($t_season->active == true){
                if($t_season->polymorphic_ctype_id != 32){
                    if($t_season->start_date <= Carbon::now()->format('Y-m-d') && $t_season->end_date >= Carbon::now()->format('Y-m-d')){
                        $activeSeasons[] = $t_season;
                    }
                    $existSeasons[] = $t_season;
                }
            }
        }

        if(count($activeSeasons) > 1){
            foreach ($activeSeasons as $season) {
                if($season->polymorphic_ctype_id == 33){

                    $fs_group_teams_id = array();
                    foreach ($season->league_fixtureseason->groups as $group) {
                        foreach ($group->group_teams as $group_team) {
                            $fs_group_teams_id[] = $group_team->team_id;
                        }
                    }
                    if(array_search($team->id, $fs_group_teams_id)){
                        $activeSeason = $season;
                    }

                }
            }
        }else{
            if(count($existSeasons)){
                $activeSeason = $existSeasons[0];
            }
        }

        if(empty($activeSeason)){
            $activeSeason = $activeSeasons[0];
        }elseif(!empty($activeSeason)){

            if(!empty($team->team_teamseason->where('season_id', $activeSeason->id)->first())){
                $remaining_transfer_count = $team->team_teamseason->where('season_id', $activeSeason->id)->first()->remaining_transfer_count;
                $squad_locked = $team->team_teamseason->where('season_id', $activeSeason->id)->first()->squad_locked;
            }else{
                $remaining_transfer_count = $activeSeason->allowed_transfer_count;
                $squad_locked = false;
            }
        }
        /***** Aktif Sezon gg*****/

        if(!empty($activeSeason)){
            
            $teamSeason = TeamTeamSeason::where('season_id', $activeSeason->id)->where('team_id', $team->id)->first();
            
            if(empty($teamSeason)){
                $teamSeason = new TeamTeamSeason();
                $teamSeason->squad_locked = false;

                if($activeSeason->polymorphic_ctype_id == 33){
                    $teamSeason->point = $activeSeason->league_fixtureseason->min_team_point;
                }else if($activeSeason->polymorphic_ctype_id == 34){
                    $teamSeason->point = $activeSeason->league_pointseason->min_team_point;
                }

                $teamSeason->season_id = $activeSeason->id;
                $teamSeason->team_id = $team->id;
                $teamSeason->remaining_transfer_count = $activeSeason->allowed_transfer_count;
                $teamSeason->playoff_member = true;
                $teamSeason->drawn = 0;
                $teamSeason->goal_against = 0;
                $teamSeason->goal_for = 0;
                $teamSeason->lost = 0;
                $teamSeason->match_total = 0;
                $teamSeason->won = 0;
                $teamSeason->gk_save = 0;
                $teamSeason->red_card = 0;
                $teamSeason->yellow_card = 0;
                $teamSeason->save();
            }

            if($teamSeason->remaining_transfer_count == 0 && $teamSeason->squad_locked){
                $text = 'Takımın transfer hakkı bulunmuyor.';
                return response()->json(array($text), 400);
            }


            $matchCount = Match::where('season_id', $activeSeason->id)->where('completed', true)->
                where(function ($query) use ($team) {
                    $query->where('team1_id', $team->id)
                        ->orWhere('team2_id', $team->id);
                })->count();

            /*
            if($activeSeason->locking_match_count <= $matchCount && !$teamSeason->squad_locked){
                $text = 'Transfer Teklifi gönderilmedi. Önce takım kadrosunu kilitlemelisiniz.';
                return response()->json(array($text), 400);
            }
            */
        }        

        $transfer = new Transfer();
        $transfer->requesting_captain_id = $captain->id;
        $transfer->requesting_team_id = $captain->team_id;

        $transfer->requested_player_id = $requested_player->id;
        if (!empty($requested_player->team_id)) {
            $transfer->requested_team_id = $requested_player->team_id;
        }

        $transfer->status = "pending";
        $transfer->created_at = Carbon::now()->format('Y-m-d H:i:s.uO');
        $transfer->updated_at = Carbon::now()->format('Y-m-d H:i:s.uO');
        $transfer->save();

        if(!empty($activeSeason)){
            $transferSeason = new TransferSeason();
            $transferSeason->transfer_id = $transfer->id;
            $transferSeason->season_id = $activeSeason->id;
            $transferSeason->save();
        }

        $transfer['requesting_captain'] = $transfer->requesting_captain;
        $transfer['requesting_team'] = $transfer->requesting_team;
        $transfer['requested_player'] = $transfer->requested_player;

        if (!empty($transfer->requested_player->user->image)) {
            $transfer['requested_player']['image_url'] = env('MEDIA_END').$transfer->requested_player->user->image;
        }else{
            $transfer['requested_player']['image_url'] = env('MEDIA_END').'default-player.png';
        }

        $transfer['requested_player']['fullname'] = $transfer->requested_player->user->first_name.' '.$transfer->requested_player->user->last_name;
        $transfer['requested_player']['position'] = $transfer->requested_player->players_position;

        $transfer['requested_team'] = $transfer->requested_team;
        $transfer['seasons'] = $transfer->transfer_season;
        $transfer['status'] = $transfer->status;
        $transfer['replied_at'] = $transfer->updated_at;
        $transfer['created_at'] = $transfer->created_at;

        $transfer['message'] = 'OK';

        if(is_null($transfer->requested_player->user->infoshare) || $transfer->requested_player->user->infoshare){
            //SMS GÖNDER; //CALL CENTER A GONDER
            $phone_no = $transfer->requested_player->user->phone;
            $smsHelper = new SmsClass();
            $smsHelper->smsSendTransferPlayer($phone_no);
        }

        return response()->json($transfer, 200);
    }

    public function captainTransferDelete($id, Request $request)
    {
        
        $transfer = Transfer::find($id);
        if(!empty($transfer->transfer_season)){
            $transfer->transfer_season->delete();
        }
        $transfer->delete();

        return response()->json($transfer->id, 200);
    }

    public function playerTransfer(Request $request)
    {
        $data = array();

        $player = Auth::user()->player;

        foreach ($player->transfers_requested as $pte) {
            $pteArr = array();
            $pteArr['id'] = $pte->id;
            $pteArr['requesting_captain_id'] = $pte->requesting_captain->id;
            $pteArr['requesting_captain'] = $pte->requesting_captain;
            $pteArr['requesting_team'] = $pte->requesting_team;
            if (!empty($pte->requesting_team->image)) {
                $pteArr['requesting_team']['image_url'] = env('MEDIA_END').$pte->requesting_team->image;
            }else{
                $pteArr['requesting_team']['image_url'] = env('MEDIA_END').'default-team.png';
            }
            $pteArr['requested_player_id'] = $pte->requested_player->id;
            $pteArr['requested_player'] = $pte->requested_player;
            $pteArr['requested_team'] = $pte->requested_team;
            $pteArr['seasons'] = $pte->transfer_season;
            $pteArr['status'] = $pte->status;
            $pteArr['replied_at'] = $pte->updated_at;

            $data['results'][] = $pteArr;
        }

        $data['count'] =  0;
        $data['next'] =  null;
        $data['previous'] =  null;

        return response()->json($data, 200);
    }

    public function playerTransferPost(Request $request)
    {
        $data = array();

        $requested_player = User::find($request->input('requested_player_id'));
        $captain = Auth::user();

        $transfer = new Transfer();
        $transfer->requesting_captain_id = $captain->id;
        $transfer->requesting_team_id = $captain->player->team_id;

        $transfer->requested_player_id = $requested_player->player->id;
        if (!empty($requested_player->player->team_id)) {
            $transfer->requested_team_id = $requested_player->player->team_id;
        }

        $transfer->status = "pending";
        $transfer->save();
        
        return response()->json($transfer, 200);
    }

    public function playerTransferAccept($id)
    {

        $transfer = Transfer::find($id);
        $requesting_team = $transfer->requesting_team;
        /*
            # Transfer talep eden takımın transfer hakkı var mı
            requesting_team = transfer.requesting_team
            requesting_team_has_transfer_rights, message = has_transfer_rights(requesting_team)
        */

        //if(!empty($transfer->requested_team)){
        //    return response()->json(array('message' => 'Takımı yok '), 400);
        //}

        $error_message = null;
        $success_message = 'OK';

        // Takım en fazla 12 oyuncuya sahip olabilir
        if($requesting_team->player->count() >= 12){
            $error_message = 'Takım maksimum oyuncu sayısına ulaşmıştır.';
        }

        // Takımın ligi yoksa veya ligin hiç sezonu yoksa kullanıcı lig-sezon açılış talebi göndermeli
        if(empty($requesting_team->league)){
            $error_message = 'Bu şehirde aktif bir lig bulunmuyor.';
        }

        /***** Aktif Sezon gg*****/
        $activeSeason = null;
        $activeSeasons = array();
        $existSeasons = array();
        $t_seasons = $requesting_team->league->seasonApi;

        foreach ($t_seasons as $t_season) {
            if($t_season->active == true){
                if($t_season->polymorphic_ctype_id != 32){
                    if($t_season->start_date <= Carbon::now()->format('Y-m-d') && $t_season->end_date >= Carbon::now()->format('Y-m-d')){
                        $activeSeasons[] = $t_season;
                    }
                    $existSeasons[] = $t_season;
                }
            }
        }

        if(count($activeSeasons) > 1){
            foreach ($activeSeasons as $season) {
                if($season->polymorphic_ctype_id == 33){

                    $fs_group_teams_id = array();
                    foreach ($season->league_fixtureseason->groups as $group) {
                        foreach ($group->group_teams as $group_team) {
                            $fs_group_teams_id[] = $group_team->team_id;
                        }
                    }
                    if(array_search($requesting_team->id, $fs_group_teams_id)){
                        $activeSeason = $season;
                    }

                }
            }
        }else{
            if(count($existSeasons)){
                $activeSeason = $existSeasons[0];
            }
        }

        if(empty($activeSeason)){
            $activeSeason = $activeSeasons[0];
        }
        /*
        if(!empty($activeSeason)){

            if(!empty($team->team_teamseason->where('season_id', $activeSeason->id)->first())){
                $remaining_transfer_count = $team->team_teamseason->where('season_id', $activeSeason->id)->first()->remaining_transfer_count;
                $squad_locked = $team->team_teamseason->where('season_id', $activeSeason->id)->first()->squad_locked;
            }else{
                $remaining_transfer_count = $activeSeason->allowed_transfer_count;
                $squad_locked = false;
            }
        }
        */
        /***** Aktif Sezon gg*****/

        if(empty($activeSeason)){
            $error_message = 'Bu şehirde aktif bir lig bulunmuyor.';
        }

        /*
            #######################################
            ## TAKIMIN SEZON GEÇMİŞİ HİÇ YOK İSE ##

            # Takımın mevcut sezonlarını kontrol et
            team_seasons = TeamSeason.objects.filter(team=team)

            # Takımın sezonu yoksa transfer hakkı yoktur
            if not team_seasons.exists():
                return False, default_error_message

            ############################
            # TAKIMIN AÇIK SEZONU YOK İSE

            # Eğer daha önce sezonu olup açık olan sezon yoksa bu aralıkta transfere izin var
            team_opened_seasons = team_seasons.opened_seasons()
            if not team_opened_seasons:
                return True, success_message

            #############################################
            # TAKIMIN AÇIK SEZONU VAR - (1 VE DAHA FALZA)

            # Playoff adayı olmayan takımlar için transfer sezonu hep açık
            # if not team.is_playoff_member:
            #     return True, success_message
        */

        // Sezon transfer döneminde değilse izin verme
        if(!empty($activeSeason)){
            if($activeSeason->transfer_start_date && $activeSeason->transfer_end_date && !($activeSeason->transfer_start_date <= Carbon::now() && Carbon::now() <= $activeSeason->transfer_end_date)){
                $error_message = 'Takımın transfer hakkı bulunmuyor.';
            }
        }

        $requesting_team_season = $requesting_team->team_teamseason->where('season_id', $activeSeason->id)->first();
        if(empty($requesting_team_season)){
        
            $requesting_team_season = new TeamTeamSeason();
            $requesting_team_season->squad_locked = false;

            if($activeSeason->polymorphic_ctype_id == 33){
                $requesting_team_season->point = $activeSeason->league_fixtureseason->min_team_point;
            }else if($activeSeason->polymorphic_ctype_id == 34){
                $requesting_team_season->point = $activeSeason->league_pointseason->min_team_point;
            }

            $requesting_team_season->season_id = $activeSeason->id;
            $requesting_team_season->team_id = $requesting_team->id;
            $requesting_team_season->remaining_transfer_count = $activeSeason->allowed_transfer_count;
            $requesting_team_season->playoff_member = true;
            $requesting_team_season->drawn = 0;
            $requesting_team_season->goal_against = 0;
            $requesting_team_season->goal_for = 0;
            $requesting_team_season->lost = 0;
            $requesting_team_season->match_total = 0;
            $requesting_team_season->won = 0;
            $requesting_team_season->gk_save = 0;
            $requesting_team_season->red_card = 0;
            $requesting_team_season->yellow_card = 0;
            $requesting_team_season->save();
        }

        if($requesting_team_season->squad_locked && !$requesting_team_season->remaining_transfer_count){
            $error_message = 'Takımın transfer hakkı bulunmuyor.';
        }

        // Transfer talep eden takımın transfer hakkı yoksa isteği iptal et
        if(!is_null($error_message)){
            $transfer->status = 'out_of_date';
            $transfer->save();
            return response()->json(array('message' => $error_message), 400);
        }

        // Transfer olan oyuncu kaptan ise, önce kaptanlığı devretmesi uygun olur
        // Transfer olan oyuncunun takımı olmayabilir
        if(!empty($transfer->requested_team) && $transfer->requested_team->captain_id == $transfer->requested_player_id){
            return response()->json(array('message' => 'Transferi kabul etmeden önce başka bir oyuncuyu kaptan olarak atamalısınız.'), 400);
        }

        /***********/

        if(!empty($activeSeason)){

            $teamSeason = $requesting_team->team_teamseason->where('season_id', $activeSeason->id)->first();
            
            if(!empty($teamSeason)){
                if($teamSeason->squad_locked){
                    $teamSeason->remaining_transfer_count = $teamSeason->remaining_transfer_count - 1;
                    $teamSeason->save();
                }
            }else{
                $teamSeason = new TeamTeamSeason();
                $teamSeason->squad_locked = false;

                if($activeSeason->polymorphic_ctype_id == 33){
                    $teamSeason->point = $activeSeason->league_fixtureseason->min_team_point;
                }else if($activeSeason->polymorphic_ctype_id == 34){
                    $teamSeason->point = $activeSeason->league_pointseason->min_team_point;
                }

                $teamSeason->season_id = $activeSeason->id;
                $teamSeason->team_id = $requesting_team->id;
                $teamSeason->remaining_transfer_count = $activeSeason->allowed_transfer_count;
                $teamSeason->playoff_member = true;
                $teamSeason->drawn = 0;
                $teamSeason->goal_against = 0;
                $teamSeason->goal_for = 0;
                $teamSeason->lost = 0;
                $teamSeason->match_total = 0;
                $teamSeason->won = 0;
                $teamSeason->gk_save = 0;
                $teamSeason->red_card = 0;
                $teamSeason->yellow_card = 0;
                $teamSeason->save();
            }
        }


        /***********/

        $transfer->status = 'accept';
        $transfer->replied_at = Carbon::now();
        $transfer->save();

        $transfer->requested_player->team_id = $requesting_team->id;
        $transfer->requested_player->save();

        if(!empty($transfer->requested_player->team)){
            $teamTeamPlayer = TeamTeamPlayer::where('player_id', $transfer->requested_player->id)->where('player_id', $transfer->requested_player->team->id)->whereNull('released_date')->first();
            if(!empty($teamTeamPlayer)){
                $teamTeamPlayer->released_date = Carbon::now()->format('Y-m-d');
                $teamTeamPlayer->save();
            }
        }
        
        $teamTeamPlayerNew = new TeamTeamPlayer();
        $teamTeamPlayerNew->player_id = $transfer->requested_player->id;
        $teamTeamPlayerNew->team_id = $transfer->requested_player->team->id;
        $teamTeamPlayerNew->joined_date = Carbon::now()->format('Y-m-d');
        $teamTeamPlayerNew->save();

        return response()->json(array('message' => 'OK'), 200);
    }

    public function playerTransferDeny($id)
    {

        $transfer = Transfer::find($id);
        $transfer->status = 'deny';
        $transfer->replied_at = Carbon::now();
        $transfer->save();

        return response()->json(array('message' => 'OK'), 200);
    }

    public function captainTeam(Request $request)
    {
        $data = array();
        $team = Auth::user()->player->team;

        /***** Aktif Sezon gg*****/
        $activeSeason = null;
        $activeSeasons = array();
        $existSeasons = array();
        $t_seasons = Auth::user()->player->team->league->seasonApi;

        foreach ($t_seasons as $t_season) {
            if($t_season->active == true){
                if($t_season->polymorphic_ctype_id != 32){
                    if($t_season->start_date <= Carbon::now()->format('Y-m-d') && $t_season->end_date >= Carbon::now()->format('Y-m-d')){
                        $activeSeasons[] = $t_season;
                    }
                    $existSeasons[] = $t_season;
                }
            }
        }

        if(count($activeSeasons) > 1){
            foreach ($activeSeasons as $season) {
                if($season->polymorphic_ctype_id == 33){

                    $fs_group_teams_id = array();
                    foreach ($season->league_fixtureseason->groups as $group) {
                        foreach ($group->group_teams as $group_team) {
                            $fs_group_teams_id[] = $group_team->team_id;
                        }
                    }
                    if(array_search($team->id, $fs_group_teams_id)){
                        $activeSeason = $season;
                    }

                }
            }
        }else{
            if(count($existSeasons)){
                $activeSeason = $existSeasons[0];
            }
        }

        if(empty($activeSeason)){
            $activeSeason = $activeSeasons[0];
        }elseif(!empty($activeSeason)){

            if(!empty($team->team_teamseason->where('season_id', $activeSeason->id)->first())){
                $remaining_transfer_count = $team->team_teamseason->where('season_id', $activeSeason->id)->first()->remaining_transfer_count;
                $squad_locked = $team->team_teamseason->where('season_id', $activeSeason->id)->first()->squad_locked;
            }else{
                $remaining_transfer_count = $activeSeason->allowed_transfer_count;
                $squad_locked = false;
            }
        }
        /***** Aktif Sezon gg*****/

        $data = $team->toArray();
        
        if (!empty($team->image)) {
            $data['image_url'] = env('MEDIA_END').$team->image;
        }else{
            $data['image_url'] = env('MEDIA_END').'default-team.png';
        }

        if (!empty($team->cover_image)) {
            $data['cover_image'] = env('MEDIA_END').$team->cover_image;
        }

        $data['province'] = $team->province->id;
        $data['district'] = $team->district->id;
        $data['captain'] = $team->captain_id;


        $data['league'] = $team->league->toArray();
        $data['league']['province'] = $team->league->league_province->first()->province->toArray();
        $data['league']['season'] = null;

        $data['value'] = $team->player->sum('value'); // PLAYER DEĞERLERİNİ TOPLA

        if(!empty($activeSeason)){
            $teamActiveSeason = TeamTeamSeason::where('season_id', $activeSeason->id)->where('team_id', $team->id)->first();
            if(!empty($teamActiveSeason)){
                $data['squad_locked'] = $teamActiveSeason->squad_locked;
            }
        }

        return response()->json($data, 200);
    }

    public function transferStatus(Request $request)
    {
        $data = array();

        /***** Aktif Sezon gg*****/
        $activeSeason = null;
        $activeSeasons = array();
        $existSeasons = array();
        $t_seasons = Auth::user()->player->team->league->seasonApi;

        foreach ($t_seasons as $t_season) {
            if($t_season->active == true){
                if($t_season->polymorphic_ctype_id != 32){
                    if($t_season->start_date <= Carbon::now()->format('Y-m-d') && $t_season->end_date >= Carbon::now()->format('Y-m-d')){
                        $activeSeasons[] = $t_season;
                    }
                    $existSeasons[] = $t_season;
                }
            }
        }

        if(count($activeSeasons) > 1){
            foreach ($activeSeasons as $season) {
                if($season->polymorphic_ctype_id == 33){

                    $fs_group_teams_id = array();
                    foreach ($season->league_fixtureseason->groups as $group) {
                        foreach ($group->group_teams as $group_team) {
                            $fs_group_teams_id[] = $group_team->team_id;
                        }
                    }
                    if(array_search(Auth::user()->player->team->id, $fs_group_teams_id)){
                        $activeSeason = $season;
                    }

                }
            }
        }else{
            if(count($existSeasons)){
                $activeSeason = $existSeasons[0];
            }
        }
        /*
        if(!empty($activeSeason)){

            if(!empty($team->team_teamseason->where('season_id', $activeSeason->id)->first())){
                $remaining_transfer_count = $team->team_teamseason->where('season_id', $activeSeason->id)->first()->remaining_transfer_count;
                $squad_locked = $team->team_teamseason->where('season_id', $activeSeason->id)->first()->squad_locked;
            }else{
                $remaining_transfer_count = $activeSeason->allowed_transfer_count;
                $squad_locked = false;
            }
        }
        */
        /***** Aktif Sezon gg*****/

        if(empty($activeSeason)){
            $activeSeason = $activeSeasons[0];
        }elseif(!empty($activeSeason)){

            $data['total_transfer_count'] = $activeSeason->allowed_transfer_count;

            if(!empty(Auth::user()->player->team->team_teamseason->where('season_id', $activeSeason->id)->first())){
                $data['remaining_count'] = Auth::user()->player->team->team_teamseason->where('season_id', $activeSeason->id)->first()->remaining_transfer_count;
            }else{
                $data['remaining_count'] = $activeSeason->allowed_transfer_count;
            }
            
            $data['start_date'] = $activeSeason->transfer_start_date;
            $data['end_date'] = $activeSeason->transfer_end_date;
        }
        
        return response()->json($data, 200);
    }

    public function requestTermination(Request $request)
    {
        $team = Auth::user()->player->team;
        $phone_arr = array();

        if(!empty($request->input('data')['cancel']) && $request->input('data')['cancel']){
            $team->status = 'active';
        }else{
            $team->status = 'termination';

            //SMS GÖNDER; //CALL CENTER A GONDER
            $userRHP = UserRoleHasProvince::where('province_id', $team->province->id)->get();
            foreach ($userRHP as $rhp) {
                $usr = User::find($rhp->user_id);
                if($usr->hasRole('call-center')){
                    $phone_arr[] = User::find($rhp->user_id)->phone;
                }
            }

            $smsHelper = new SmsClass();
            $smsHelper->smsSendTerminationCallCenter(implode(",", $phone_arr), $team);
        }
        $team->save();

        return response()->json(array('success' => true), 200);
    }

    public function squadLock(Request $request)
    {

        $team = Auth::user()->player->team;
        /***** Aktif Sezon gg*****/
        $activeSeason = null;
        $activeSeasons = array();
        $existSeasons = array();
        $t_seasons = Auth::user()->player->team->league->seasonApi;

        foreach ($t_seasons as $t_season) {
            if($t_season->active == true){
                if($t_season->polymorphic_ctype_id != 32){
                    if($t_season->start_date <= Carbon::now()->format('Y-m-d') && $t_season->end_date >= Carbon::now()->format('Y-m-d')){
                        $activeSeasons[] = $t_season;
                    }
                    $existSeasons[] = $t_season;
                }
            }
        }

        if(count($activeSeasons) > 1){
            foreach ($activeSeasons as $season) {
                if($season->polymorphic_ctype_id == 33){

                    $fs_group_teams_id = array();
                    foreach ($season->league_fixtureseason->groups as $group) {
                        foreach ($group->group_teams as $group_team) {
                            $fs_group_teams_id[] = $group_team->team_id;
                        }
                    }
                    if(array_search($team->id, $fs_group_teams_id)){
                        $activeSeason = $season;
                    }

                }
            }
        }else{
            if(count($existSeasons)){
                $activeSeason = $existSeasons[0];
            }
        }

        if(empty($activeSeason)){
            $activeSeason = $activeSeasons[0];
        }elseif(!empty($activeSeason)){

            if(!empty($team->team_teamseason->where('season_id', $activeSeason->id)->first())){
                $remaining_transfer_count = $team->team_teamseason->where('season_id', $activeSeason->id)->first()->remaining_transfer_count;
                $squad_locked = $team->team_teamseason->where('season_id', $activeSeason->id)->first()->squad_locked;
            }else{
                $remaining_transfer_count = $activeSeason->allowed_transfer_count;
                $squad_locked = false;
            }
        }
        /***** Aktif Sezon gg*****/

        if(!empty($activeSeason)){
            $teamActiveSeason = TeamTeamSeason::where('season_id', $activeSeason->id)->where('team_id', $team->id)->first();
            
            $teamActiveSeason->squad_locked = !$teamActiveSeason->squad_locked;
            $teamActiveSeason->squad_locked_date = Carbon::now();
            $teamActiveSeason->squad_locked_player_id = Auth::user()->player->id;

            $teamActiveSeason->save();
            return response()->json(array('success' => true), 200);
        }

        return response()->json(array('success' => false), 200);
    }

    public function captainTeamLogoUpdate(Request $request) 
    {


        $conf = array('width' => 200, 'height' => 200, 'aspectRatio' => 0, 'widen' => true, 'heighten' => true, 'crop' => true);
        $photo = $request->file('file');

        $fileName = str_slug(str_before($photo->getClientOriginalName(), '.'), '_').'_'.str_random(5).'.'.$photo->getClientOriginalExtension();
        $imageObj = Image::make($photo);

        if ($conf['widen']) {
            $imageObj->widen($conf['width'], function ($constraint) {
                $constraint->upsize();
            });
        }

        $ext = $photo->getClientOriginalExtension();
        if($ext == "png"){
            $imageObj->save(env('MEDIA_PATH').'team-logos/'.$fileName, 93);
        }else{
            $imageObj->save(env('MEDIA_PATH').'team-logos/'.$fileName, 93, 'jpg');
        }

        $team = Auth::user()->player->team;
        $team->image = 'team-logos/'.$fileName;
        $team->save();

        //echo json_encode(array('location' => $team->image, 'id' => $team->id, 'status' => 'success'));
        return response()->json(array('success' => 'OK'), 200);
    }
    
    public function leaSeaLiveMatches($id, $sid)
    {
        $data = array();
        $_ids = array();

        $season = Season::find($sid);
        $_ids[] = $sid;
        if(!empty($season->league_elimination_baseseason)){
            $_ids[] = $season->league_elimination_baseseason->season_ptr_id;
        }

        $matches = Match::select('*', 'season_id as season')->
        whereIn('season_id', $_ids)->where('date', '>=', Carbon::now())->where('completed', false)->orderBy('date', 'asc')->orderBy('time', 'asc');
        $matches = $matches->limit(16);
        $matches = $matches->get();

        foreach ($matches as $match) {

            $matchArr = $match->toArray();
            $matchArr['video_status'] = (!empty($match->match_video)) ? $match->match_video->status : 'recordable';

            $matchArr['team1'] = $match->team1->toArray();
            
            if (!empty($match->team1->image)) {
                $matchArr['team1']['image_url'] = env('MEDIA_END').$match->team1->image;
            }else{
                $matchArr['team1']['image_url'] = env('MEDIA_END').'default-team.png';
            }

            $matchArr['team1']['league'] = $season->league->toArray();
            //$matchArr['team1']['league']['province'] = $season->league->league_province->first()->province->toArray();
            $matchArr['team1']['league']['season'] = $season->league->seasonApi->first()->toArray();

            if($season->league->seasonApi->first()->polymorphic_ctype_id == 32){
                $matchArr['team1']['league']['season']['type'] = 'elimination';
            }else if($season->league->seasonApi->first()->polymorphic_ctype_id == 33){
                $matchArr['team1']['league']['season']['type'] = 'fixture';
            }else if($season->league->seasonApi->first()->polymorphic_ctype_id == 34){
                $matchArr['team1']['league']['season']['type'] = 'point';
            }
            $matchArr['team1']['league']['season']['fullname'] = $season->league->seasonApi->first()->year.' '.$season->league->name.' '.$season->league->seasonApi->first()->name;


            $matchArr['team2'] = $match->team2->toArray();
            
            if (!empty($match->team2->image)) {
                $matchArr['team2']['image_url'] = env('MEDIA_END').$match->team2->image;
            }else{
                $matchArr['team2']['image_url'] = env('MEDIA_END').'default-team.png';
            }

            $matchArr['team2']['league'] = $season->league->toArray();
            //$matchArr['team2']['league']['province'] = $season->league->league_province->first()->province->toArray();
            $matchArr['team2']['league']['season'] = $season->league->seasonApi->first()->toArray();

            if($season->league->seasonApi->first()->polymorphic_ctype_id == 32){
                $matchArr['team2']['league']['season']['type'] = 'elimination';
            }else if($season->league->seasonApi->first()->polymorphic_ctype_id == 33){
                $matchArr['team2']['league']['season']['type'] = 'fixture';
            }else if($season->league->seasonApi->first()->polymorphic_ctype_id == 34){
                $matchArr['team2']['league']['season']['type'] = 'point';
            }
            $matchArr['team2']['league']['season']['fullname'] = $season->league->seasonApi->first()->year.' '.$season->league->name.' '.$season->league->seasonApi->first()->name;

            $data[] = $matchArr;

        }


        return response()->json($data, 200);
    }

    public function leaSeaMostValuablePlayers($id, $sid, Request $request)
    {

        $data = array();

        $players = Player::select(/* DB::raw('(players.value + players.old_system_value) as total_value'), */ 'players.value', 'players.old_system_value', 'players.id as player_id', 'matches.season_id')->distinct('player_id')->
        leftJoin('match_player', 'match_player.player_id', '=', 'players.id')->
        leftJoin('matches', 'matches.id', '=', 'match_player.match_id')->
        where('matches.season_id', $sid)->
        orderBy('players.value', 'desc')->limit(5)->get();

        foreach ($players as $player) {

            $_player = Player::find($player->player_id);

            $pvalue = $player->toArray();
            $pvalue['value'] = (int)$player->value;
            $pvalue['old_system_value'] = (int)$player->old_system_value;

            $pvalue['player'] = $_player->toArray();
            $pvalue['player']['position'] = (!empty($_player->players_position)) ? $_player->players_position->toArray() : null;
            $pvalue['player']['position2'] = (!empty($_player->players_position2)) ? $_player->players_position2->toArray() : null;
            $pvalue['player']['position3'] = (!empty($_player->players_position3)) ? $_player->players_position3->toArray() : null;
            
            $pvalue['player']['value'] = (int)$_player->value;
            $pvalue['player']['old_system_value'] = (int)$_player->old_system_value;

            $pvalue['player']['fullname'] = $_player->user->first_name.' '.$_player->user->last_name;
            if (!empty($_player->user->image)) {
                $pvalue['player']['image_url'] = env('MEDIA_END').$_player->user->image;
            }else{
                $pvalue['player']['image_url'] = env('MEDIA_END').'default-player.png';
            }

            $team = $_player->team;

            if(!empty($team)){
                $pvalue['player']['team'] = $team;
                
                if (!empty($team->image)) {
                    $pvalue['player']['team']['image_url'] = env('MEDIA_END').$team->image;
                }else{
                    $pvalue['player']['team']['image_url'] = env('MEDIA_END').'default-team.png';
                }
                $pvalue['player']['team']['league'] = $team->league->toArray();
            }else{
                $pvalue['player']['team'] = null;
            }

            $data[] = $pvalue;
        }
        return response()->json($data, 200);
    }

    public function leaSeaTemsilciValuablePlayers($id, $sid, Request $request)
    {
        //*******//
        $data = array();

        $season = Season::find($sid);
        if(!empty($season->season_temsilci_team)){
            if(!empty($season->season_temsilci_team->data)){

                $players = Player::select(/* DB::raw('(players.value + players.old_system_value) as total_value'), */ 'players.value', 'players.old_system_value', 'players.id as player_id', 'matches.season_id')->distinct('player_id')->
                leftJoin('match_player', 'match_player.player_id', '=', 'players.id')->
                leftJoin('matches', 'matches.id', '=', 'match_player.match_id')->
                where('matches.season_id', $sid)->
                where('players.team_id', $$season->season_temsilci_team->data)->
                orderBy('players.value', 'desc')->limit(5)->get();

                foreach ($players as $player) {

                    $_player = Player::find($player->player_id);

                    $pvalue = $player->toArray();
                    $pvalue['value'] = (int)$player->value;
                    $pvalue['old_system_value'] = (int)$player->old_system_value;

                    $pvalue['player'] = $_player->toArray();
                    $pvalue['player']['position'] = (!empty($_player->players_position)) ? $_player->players_position->toArray() : null;
                    $pvalue['player']['position2'] = (!empty($_player->players_position2)) ? $_player->players_position2->toArray() : null;
                    $pvalue['player']['position3'] = (!empty($_player->players_position3)) ? $_player->players_position3->toArray() : null;
                    
                    $pvalue['player']['value'] = (int)$_player->value;
                    $pvalue['player']['old_system_value'] = (int)$_player->old_system_value;

                    $pvalue['player']['fullname'] = $_player->user->first_name.' '.$_player->user->last_name;
                    if (!empty($_player->user->image)) {
                        $pvalue['player']['image_url'] = env('MEDIA_END').$_player->user->image;
                    }else{
                        $pvalue['player']['image_url'] = env('MEDIA_END').'default-player.png';
                    }

                    $team = $_player->team;

                    if(!empty($team)){
                        $pvalue['player']['team'] = $team;
                        
                        if (!empty($team->image)) {
                            $pvalue['player']['team']['image_url'] = env('MEDIA_END').$team->image;
                        }else{
                            $pvalue['player']['team']['image_url'] = env('MEDIA_END').'default-team.png';
                        }
                        $pvalue['player']['team']['league'] = $team->league->toArray();
                    }else{
                        $pvalue['player']['team'] = null;
                    }

                    $data[] = $pvalue;
                }
                return response()->json($data, 200);


            }
        }

        return response()->json($data, 200);
    }

    public function leaSeaStatsPlayer($id, $sid, Request $request)
    {
        $data = array();
        $_ids = array();

        if(!empty($request->input('before')) && $request->input('before') == 'true'){
            $season = Season::find($sid);
        }else{
            $season = Season::find($sid);
        }


        $_ids[] = $sid;
        if(!empty($season->league_elimination_baseseason)){
            $_ids[] = $season->league_elimination_baseseason->season_ptr_id;
        }


        $field = null;
        if(!empty($request->input('field'))){
            $field = $request->input('field');
            if($field == 'save'){
                $field = 'gk_save';
            }

        }

        $limit = 5;
        if(!empty($request->input('limit'))){
            $limit = $request->input('limit');
        }

        $tpths = TeamPlayerTeamHistory::selectRaw('player_id, sum(match) as match, sum(goal) as goal, sum(yellow_card) as yellow_card, sum(red_card) as red_card, sum(gk_save) as gk_save')->whereIn('season_id', $_ids)->groupBy('player_id')->orderBy($field, 'desc')->limit($limit)->get();
        //return response()->json(DB::getQueryLog(), 200);
        foreach ($tpths as $tpth) {
            $tpthArr = $tpth->toArray();
            //$tpthArr['player'] = $tpth->player->toArray();
            $tpthArr['player']['id'] = $tpth->player_id;
            $tpthArr['player']['fullname'] = $tpth->player->user->first_name.' '.$tpth->player->user->last_name;

            if(!empty($tpth->player->user->image)){
                $tpthArr['player']['image_url'] = env('MEDIA_END').$tpth->player->user->image;
            }else{
                $tpthArr['player']['image_url'] = env('MEDIA_END').'default-player.png';
            }

            if(!empty($tpth->player->team)){
                $tpthArr['player']['team']['name'] = $tpth->player->team->name;
            }else{
                $tpthArr['player']['team']['name'] = '';
            }
             
            $data[] = $tpthArr;
        }

        return response()->json($data, 200);
    }

    public function leaSeaStatsPlayerPS($id, $sid, Request $request)
    {
        $data = array();
        $_ids = array();


        $season = Season::find($sid);
 
        $league = $season->league;

        $birOncekiSeasonControl = false;
        $birOncekiSeason = null;
        foreach ($league->seasonApi as $s) {
            if($birOncekiSeasonControl){
                $birOncekiSeason = $s;
                break;
            }
            if($s->id == $season->id){
                $birOncekiSeasonControl = true;
            }
        }

        $_ids[] = $birOncekiSeason->id;
        if(!empty($birOncekiSeason->league_elimination_baseseason)){
            $_ids[] = $birOncekiSeason->league_elimination_baseseason->season_ptr_id;
        }

        $field = null;
        if(!empty($request->input('field'))){
            $field = $request->input('field');
            if($field == 'save'){
                $field = 'gk_save';
            }

        }

        $limit = 6;
        if(!empty($request->input('limit'))){
            $limit = $request->input('limit');
        }

        $tpths = TeamPlayerTeamHistory::selectRaw('player_id, sum(match) as match, sum(goal) as goal, sum(yellow_card) as yellow_card, sum(red_card) as red_card, sum(gk_save) as gk_save')->whereIn('season_id', $_ids)->groupBy('player_id')->orderBy($field, 'desc')->limit($limit)->get();
        //return response()->json(DB::getQueryLog(), 200);
        foreach ($tpths as $tpth) {
            $tpthArr = $tpth->toArray();
            //$tpthArr['player'] = $tpth->player->toArray();
            $tpthArr['player']['id'] = $tpth->player_id;
            $tpthArr['player']['fullname'] = $tpth->player->user->first_name.' '.$tpth->player->user->last_name;

            if(!empty($tpth->player->user->image)){
                $tpthArr['player']['image_url'] = env('MEDIA_END').$tpth->player->user->image;
            }else{
                $tpthArr['player']['image_url'] = env('MEDIA_END').'default-player.png';
            }

            if(!empty($tpth->player->team)){
                $tpthArr['player']['team']['name'] = $tpth->player->team->name;
            }else{
                $tpthArr['player']['team']['name'] = '';
            }
             
            $data[] = $tpthArr;
        }

        return response()->json($data, 200);
    }

    public function leaSeaTournamentSummary($id, $sid, Request $request)
    {
        $data = array();
        $_ids = array();

        $total_match = SeasonBeforeAfter::where('type', 'total_match')->where('season_id', $sid)->where('status', 'before')->first();

        if(!empty($total_match)){
            $data['total_match'] = $total_match->data;
        }

        $to_tt = SeasonBeforeAfter::where('type', 'to_tt')->where('season_id', $sid)->where('status', 'before')->first();

        if(!empty($to_tt)){
            $data['to_tt'] = $to_tt->data;
        }


        $to_fkbt = SeasonBeforeAfter::where('type', 'to_fkbt')->where('season_id', $sid)->where('status', 'before')->first();

        if(!empty($to_fkbt)){
            $data['to_fkbt'] = $to_fkbt->data;
        }


        $to_o = SeasonBeforeAfter::where('type', 'to_o')->where('season_id', $sid)->where('status', 'before')->first();

        if(!empty($to_o)){
            $data['to_o'] = $to_o->data;
        }


        $to_fkbo = SeasonBeforeAfter::where('type', 'to_fkbo')->where('season_id', $sid)->where('status', 'before')->first();

        if(!empty($to_fkbo)){
            $data['to_fkbo'] = $to_fkbo->data;
        }


        $to_cye = SeasonBeforeAfter::where('type', 'to_cye')->where('season_id', $sid)->where('status', 'before')->first();

        if(!empty($to_cye)){
            $data['to_cye'] = $to_cye->data;
        }

        $season = Season::find($sid);
        $_ids[] = $sid;
        if(!empty($season->league_elimination_baseseason)){
            $_ids[] = $season->league_elimination_baseseason->season_ptr_id;
        }

        $matches = Match::whereIn('season_id', $_ids)->where('completed', true)->get();
        $data['match_count'] = $matches->count();

        $matches_actions_goal = MatchAction::whereIn('match_id', $matches->pluck('id'))->where('type', 'goal')->count();
        $data['total_goal'] = $matches_actions_goal;

        $matches_actions_yellow_card = MatchAction::whereIn('match_id', $matches->pluck('id'))->where('type', 'yellow_card')->count();
        $matches_actions_red_card = MatchAction::whereIn('match_id', $matches->pluck('id'))->where('type', 'red_card')->count();
        $data['total_card'] = $matches_actions_yellow_card + $matches_actions_red_card;

        return response()->json($data, 200);
    }

    public function leaSeaStatsPlayerCard($id, $sid, Request $request)
    {
        $data = array();
        $_ids = array();

        $season = Season::find($sid);
        $_ids[] = $sid;
        if(!empty($season->league_elimination_baseseason)){
            $_ids[] = $season->league_elimination_baseseason->season_ptr_id;
        }

        //field=goal
        //field=goal&limit=10
        //field=save&limit=10
        //field=red_card&limit=10
        //field=yellow_card&limit=10
        //field=total_card&limit=10

        $field = null;
        if(!empty($request->input('field'))){
            $field = $request->input('field');
            if($field == 'save'){
                $field = 'gk_save';
            }

        }

        $limit = 5;
        if(!empty($request->input('limit'))){
            $limit = $request->input('limit');
        }

        $tpths = TeamPlayerTeamHistory::selectRaw('player_id, sum(red_card + yellow_card) as total_card, sum(match) as match, sum(goal) as goal, sum(yellow_card) as yellow_card, sum(red_card) as red_card, sum(gk_save) as gk_save')->whereIn('season_id', $_ids)->groupBy('player_id')->orderBy('total_card', 'desc')->limit($limit)->get();
        //$tpths = TeamPlayerTeamHistory::select('*',DB::raw('(red_card + yellow_card) as total_card'))->where('season_id', $season->id)->orderBy($field, 'desc')->limit($limit)->get();
        foreach ($tpths as $tpth) {
            $tpthArr = $tpth->toArray();
            //$tpthArr['player'] = $tpth->player->toArray();
            $tpthArr['player']['id'] = $tpth->player_id;
            $tpthArr['player']['fullname'] = $tpth->player->user->first_name.' '.$tpth->player->user->last_name;

            if(!empty($tpth->player->user->image)){
                $tpthArr['player']['image_url'] = env('MEDIA_END').$tpth->player->user->image;
            }else{
                $tpthArr['player']['image_url'] = env('MEDIA_END').'default-player.png';
            }

            if(!empty($tpth->player->team)){
                $tpthArr['player']['team']['name'] = $tpth->player->team->name;
            }else{
                $tpthArr['player']['team']['name'] = '';
            }
             
            $data[] = $tpthArr;
        }

        return response()->json($data, 200);
    }

    public function leaSeaStatsTeam($id, $sid, Request $request)
    {
        $data = array();
        $season = Season::find($sid);

        $field = null;
        if(!empty($request->input('field'))){
            $field = $request->input('field');
            if($field == 'goal'){
                $field = 'goal_for';
            }
            if($field == 'save'){
                $field = 'gk_save';
            }
        }

        $limit = 5;
        if(!empty($request->input('limit'))){
            $limit = $request->input('limit');
        }

        $tpths = TeamTeamSeason::where('season_id', $season->id)->orderBy($field, 'desc')->limit($limit)->get();
        foreach ($tpths as $tpth) {
            $tpthArr = $tpth->toArray();
            
            $tpthArr['team'] = $tpth->team->toArray();
            
            if(!empty($tpth->team->image)){
                $tpthArr['team']['image_url'] = env('MEDIA_END').$tpth->team->image;
            }else{
                $tpthArr['team']['image_url'] = env('MEDIA_END').'default-player.png';
            }

            $tpthArr['team']['league'] = $tpth->season->league->toArray();
            $tpthArr['team']['league']['province'] = $tpth->season->league->league_province->first()->province->toArray();
            
            $tpthArr['season'] = $tpth->season->toArray();
            $tpthArr['season']['fullname'] = $tpth->season->year.' '.$tpth->season->league->name.' '.$tpth->season->name;

            if($tpth->season->polymorphic_ctype_id == 32){
                $tpthArr['season']['type'] = 'elimination';
            }else if($tpth->season->polymorphic_ctype_id == 33){
                $tpthArr['season']['type'] = 'fixture';
            }else if($tpth->season->polymorphic_ctype_id == 34){
                $tpthArr['season']['type'] = 'point';
            }

            $data[] = $tpthArr;
        }

        return response()->json($data, 200);
    }

    public function leaSeaPenalties($id, $sid, Request $request)
    {
    
        $data = array();
        $data['player'] = array();
        $data['team'] = array();
        $_ids = array();

        $season = Season::find($sid);
        $_ids[] = $sid;
        if(!empty($season->league_elimination_baseseason)){
            $_ids[] = $season->league_elimination_baseseason->season_ptr_id;
        }

        $penalties = Penalty::select('penalties.*')->whereIn('penalties.season_id', $_ids)->
        leftJoin('matches', 'matches.id', '=', 'penalties.match_id')->orderBy('matches.date', 'desc')->get();

        foreach ($penalties as $penalty) {

            $pArr = $penalty->toArray();

            $pArr['match']['id'] = $penalty->match->id;
            $pArr['match']['datetime'] = $penalty->match->date.'T'.$penalty->match->time;


            $pArr['match']['team1']['id'] = $penalty->match->team1->id;
            $pArr['match']['team1']['name'] = $penalty->match->team1->name;
            if(!empty($penalty->match->team1->image)){
                $pArr['match']['team1']['image_url'] = env('MEDIA_END').$penalty->match->team1->image;
            }else{
                $pArr['match']['team1']['image_url'] = env('MEDIA_END').'default-team.png';
            }

            $pArr['match']['team2']['id'] = $penalty->match->team2->id;
            $pArr['match']['team2']['name'] = $penalty->match->team2->name;
            if(!empty($penalty->match->team2->image)){
                $pArr['match']['team2']['image_url'] = env('MEDIA_END').$penalty->match->team2->image;
            }else{
                $pArr['match']['team2']['image_url'] = env('MEDIA_END').'default-team.png';
            }

            $pArr['match']['season']['id'] = $penalty->season->id;
            $pArr['match']['season']['name'] = $penalty->season->name;
            $pArr['match']['season']['fullname'] = $penalty->season->year.' '.$penalty->season->league->name.' '.$penalty->season->name;
            $pArr['match']['season']['year'] = $penalty->season->year;

            if($penalty->season->polymorphic_ctype_id == 32){
                $pArr['match']['season']['type'] = 'elimination';
            }else if($penalty->season->polymorphic_ctype_id == 33){
                $pArr['match']['season']['type'] = 'fixture';
            }else if($penalty->season->polymorphic_ctype_id == 34){
                $pArr['match']['season']['type'] = 'point';
            }

            $pArr['match']['season']['start_date'] = $penalty->season->start_date;
            $pArr['match']['season']['end_date'] = $penalty->season->end_date;

            //$pArr['ctype'] = $penalty->ctype->toArray();
            if($penalty->ctype->model == 'penalplayer'){

                $pArr['penal_type'] = $penalty->player_penalty->penalty_type->toArray();
                //$pArr['ctype']['player_penalty'] = $penalty->player_penalty->toArray();

                if(!empty($penalty->player_penalty->player)){
                    $pArr['player'] = $penalty->player_penalty->player->toArray();

                    $pArr['player']['fullname'] = $penalty->player_penalty->player->user->first_name.' '.$penalty->player_penalty->player->user->last_name;
                    if(!empty($penalty->player_penalty->player->user->image)){
                        $pArr['player']['image_url'] = env('MEDIA_END').$penalty->player_penalty->player->user->image;
                    }else{
                        $pArr['player']['image_url'] = env('MEDIA_END').'default-player.png';
                    }

                    $pArr['player']['position'] = (!empty($penalty->player_penalty->player->players_position)) ? $penalty->player_penalty->player->players_position->toArray() : null;
                    $pArr['player']['position2'] = (!empty($penalty->player_penalty->player->players_position2)) ? $penalty->player_penalty->player->players_position2->toArray() : null;
                    $pArr['player']['position3'] = (!empty($penalty->player_penalty->player->players_position3)) ? $penalty->player_penalty->player->players_position3->toArray() : null;

                    $pArr['player']['value'] = (int)$penalty->player_penalty->player->value;
                    $pArr['player']['old_system_value'] = (int)$penalty->player_penalty->player->old_system_value;

                    if(!empty($penalty->player_penalty->team)){
                        $pArr['player']['team'] = $penalty->player_penalty->team->toArray();
                        
                        if (!empty($penalty->player_penalty->team->image)) {
                            $pArr['player']['team']['image_url'] = env('MEDIA_END').$penalty->player_penalty->team->image;
                        }else{
                            $pArr['player']['team']['image_url'] = env('MEDIA_END').'default-team.png';
                        }

                        $pArr['player']['team']['league'] = $penalty->player_penalty->team->league->toArray();
                        $pArr['player']['team']['league']['province'] = $penalty->player_penalty->team->league->league_province->first()->province->toArray();
                        $pArr['player']['team']['league']['season'] = null;
                    }else{
                        $pArr['player']['team'] = null;
                    }

                    $pArr['image'] = $pArr['player']['image_url'];
                    $pArr['guest_name'] = '';
                }else{
                    $pArr['player'] = null;
                    $pArr['guest_name'] = $penalty->player_penalty->guest_name;
                    $pArr['image'] = env('MEDIA_END').'default-player.png';
                }

                if(!empty($pArr['player']['fullname'])){
                    $pArr['who'] = $pArr['player']['fullname'];
                }else{
                    $pArr['who'] = $pArr['guest_name'];
                }
                $pArr['team'] = $pArr['player']['team'];
                
                $data['player'][] = $pArr;
                
            }elseif($penalty->ctype->model == 'penalteam'){

                $pArr['penal_type'] = $penalty->team_penalty->penalty_type->toArray();

                if(!empty($penalty->team_penalty->team)){
                    $pArr['team'] = $penalty->team_penalty->team->toArray();

                    if(!empty($penalty->team_penalty->team->image)){
                        $pArr['team']['image_url'] = env('MEDIA_END').$penalty->team_penalty->team->image;
                    }else{
                        $pArr['team']['image_url'] = env('MEDIA_END').'default-team.png';
                    }

                    $pArr['team']['league'] = $penalty->team_penalty->team->league->toArray();
                    $pArr['team']['league']['province'] = $penalty->team_penalty->team->league->league_province->first()->province->toArray();
                    $pArr['team']['league']['season'] = null;
                }else{
                    $pArr['team'] = null;
                }
            
                $data['team'][] = $pArr;
            }

        }

        return response()->json($data, 200);
    }

    public function leaSeaTeamPoints($id, $sid, Request $request)
    {
        $data = array();
        return response()->json($data, 200);
    }

    public function leaSeaGallery($id, $sid, Request $request)
    {
        $data = array();
        return response()->json($data, 200);
    }

    public function leaSeaMarkingTags($id, $sid, Request $request)
    {
        $data = array();
        return response()->json($data, 200);
    }

    public function monthlyPanorama($id, $sid, Request $request)
    {

        $data = array();

        $cmc = Carbon::parse($request->input('id')); 
        $panoramas = MounthlyPanorama::where('season_id', $sid)->whereMonth('date', $cmc->month)->whereYear('date', $cmc->year)->orderBy('date', 'DESC')->get();
        $season = Season::find($sid);

        foreach ($panoramas as $panorama) {
            $panoramaArr = $panorama->toArray();
            $panoramaArr['panorama'] = $panorama->panorama_type;

            if(!empty($panoramaArr['panorama']->image)){
                $panoramaArr['panorama']['image_url'] = env('MEDIA_END').$panoramaArr['panorama']->image;
            }
            
            if($panoramaArr['panorama']->to_who == 'player') {

                $panoramaArr['player'] = $panorama->player;
                $panoramaArr['player']['fullname'] = $panorama->player->user->first_name.' '.$panorama->player->user->last_name;

                if(!empty($panorama->player->user->image)){
                    $panoramaArr['player']['image_url'] = env('MEDIA_END').$panorama->player->user->image;
                }else{
                    $panoramaArr['player']['image_url'] = env('MEDIA_END').'default-player.png';
                }
            }

            $panoramaArr['team'] = $panorama->team;
            if (!empty($panorama->team->image)) {
                $panoramaArr['team']['image_url'] = env('MEDIA_END').$panorama->team->image;
            }else{
                $panoramaArr['team']['image_url'] = env('MEDIA_END').'default-team.png';
            }

            $data['panorama'][] = $panoramaArr;
        }

        return response()->json($data, 200);

    }

    public function monthlyPanoramaDates($id, $sid, Request $request)
    {

        $monthNameTR = array('Ocak', 'Şubat', 'Mart', 'Nisan', 'Mayıs', 'Haziran', 'Temmuz', 'Ağustos', 'Eylül', 'Ekim', 'Kasım', 'Aralık');

        $data = array();

        $panoramaDates = MounthlyPanorama::select('date')->where('season_id', $sid)->orderBy('date', 'DESC')->get();
        $season = Season::find($sid);

        $monthFake = array();
        foreach ($panoramaDates as $panoramaDate) {
            $crb = Carbon::parse($panoramaDate['date'])->startOfMonth();
            $monthFake[] = array('id' => $crb->format('Y-m-d'), 'name' => $monthNameTR[$crb->month - 1].' '.$crb->year);
        }
        $monthFake = collect($monthFake)->unique('id')->values()->toArray();
        $data = $monthFake;

        return response()->json($data, 200);

    }

    public function agenda()
    {
        $data = array();

        $agenda = Agenda::orderBy('id', 'desc')->get();

        foreach ($agenda as $ag) {
            $ag->image = env('MEDIA_END').$ag->image;

            if(str_contains($ag->link, 'www.rakipbul.com')){
                $ag->target = '_self';
            }else{
                $ag->target = '_blank';
            }
            
        }

        $data = $agenda->toArray();

        return response()->json($data, 200);
    }

    public function tactics()
    {
        $data = array();

        $tactics = Tactic::select('id', 'name')->orderBy('id', 'desc')->get();

        return response()->json($tactics, 200);
    }

    public function eliminationTrees(Request $request)
    {

        $data = array();

        $seasonId = null;
        if(!empty($request->input('season'))){
            $seasonId = $request->input('season');
        }

        $type = null;
        if(!empty($request->input('type'))){
            $type = $request->input('type');
        }

        $season = Season::find($seasonId);

        if(empty($type)){
            $eliminationTrees = $season->league_elimination_baseseason->elimination_tree;
        }else{
            if($type == 'conference'){
                $eliminationTrees = $season->league_elimination_baseseason->elimination_tree_conference;
            }elseif($type == 'final'){
                $eliminationTrees = $season->league_elimination_baseseason->elimination_tree_final;
            }elseif($type == 'trfinal'){
                $eliminationTrees = $season->league_elimination_baseseason->elimination_tree_trfinal;
            }
        }        

        foreach ($eliminationTrees as $eliminationTree) {
            $eliminationTree->season = $eliminationTree->season_id;
            $data[] = $eliminationTree->toArray();
        }
        return response()->json($data, 200);
    }

    public function eliminationTreeId($etid, Request $request)
    {

        $data = array();
        $treeItems = array();
        //$data['results'] = array();
        //$results = array();

        $eliminationTree = EliminationTree::find($etid);

        $eliminationTree->season = $eliminationTree->season_id;

        $data = $eliminationTree->toArray();
        $data['season'] = $eliminationTree->season_id;

        if($eliminationTree->type == 'trfinal'){
            $etItems = $eliminationTree->itemstr;
        }else{
            $etItems = $eliminationTree->items;
        }

        foreach ($etItems as $item) {

            $eliminationTreeItem = $item->toArray();

            $eliminationTreeItem['match'] = null;
            if (!empty($item->match)) {
                $eliminationTreeItem['match'] = $item->match->toArray();
                $eliminationTreeItem['match']['team1'] = $item->match->team1_id;
                $eliminationTreeItem['match']['team2'] = $item->match->team2_id;
                $eliminationTreeItem['match']['season'] = $item->match->season_id;
            }
            
            $eliminationTreeItem['team1'] = null;
            if (!empty($item->team1)) {
                $eliminationTreeItem['team1'] = $item->team1->toArray();
                
                if (!empty($item->team1->image)) {
                    $eliminationTreeItem['team1']['image_url'] = env('MEDIA_END').$item->team1->image;
                }else{
                    $eliminationTreeItem['team1']['image_url'] = env('MEDIA_END').'default-team.png';
                }
            }

            $eliminationTreeItem['team2'] = null;
            if (!empty($item->team2)) {
                $eliminationTreeItem['team2'] = $item->team2->toArray();
                
                if (!empty($item->team2->image)) {
                    $eliminationTreeItem['team2']['image_url'] = env('MEDIA_END').$item->team2->image;
                }else{
                    $eliminationTreeItem['team2']['image_url'] = env('MEDIA_END').'default-team.png';
                }
            }

            if ($eliminationTreeItem['level_title'] == 'Üçüncülük Maçı'){
                if (!empty($eliminationTreeItem['team1']) && !empty($eliminationTreeItem['team2'])){
                    $treeItems[] = $eliminationTreeItem;
                }
            }else{
                $treeItems[] = $eliminationTreeItem;
            }
        }

        if($eliminationTree->type == 'trfinal'){
            $data['items'] = collect($treeItems)->sortBy('level')->values()->all();
        }else{
            $data['items'] = collect($treeItems)->sortBy('side')->sortBy('level')->values()->all();
        }
        

        return response()->json($data, 200);
    }

    /****************************/
    /********** TAKIM ***********/

    public function teamCreate(Request $request)
    {
        $data = array();

        $team = new Team();
        $team->status = 'active';
        $team->name = $request->name;
        $team->tactic_id = $request->tactic;
        $team->province_id = Auth::user()->profile->province_id;
        $team->district_id = Auth::user()->profile->district_id;

        if(count(Auth::user()->profile->province->league_province->whereNotIn('league_id', [26])) > 0){
            $team->league_id = Auth::user()->profile->province->league_province->whereNotIn('league_id', [26])->first()->league_id;
        }        

        $team->captain_id = Auth::user()->player->id;
        $team->save();

        $team_survey = new TeamSurvey();
        $team_survey->team_id = $team->id;
        $team_survey->potential_id = 5;
        $team_survey->satisfaction_id = 13;
        $team_survey->save();

        $player = Auth::user()->player;
        $player->team_id = $team->id;
        $player->save();

        //SMS GÖNDER; //CALL CENTER A GONDER
        $phone_arr = array();
        $userRHP = UserRoleHasProvince::where('province_id', $team->province->id)->get();
        foreach ($userRHP as $rhp) {
            $usr = User::find($rhp->user_id);
            if($usr->hasRole('call-center')){
                $phone_arr[] = User::find($rhp->user_id)->phone;
            }
        }
        $smsHelper = new SmsClass();
        $smsHelper->smsSendNewTeamCallCenter(implode(",", $phone_arr), $team);

        //SMS GÖNDER; //TAKIMI KURAN KAPTANA GONDER
        $phone_no = Auth::user()->phone;
        $smsHelper = new SmsClass();
        $smsHelper->smsSendNewTeamCaptain($phone_no, $team);

        return response()->json($team, 200);
    }
    
    public function teams($id)
    {
        $data = array();

        $team = Team::find($id);

        $data = $team->toArray();
        
        if (!empty($team->image)) {
            $data['image_url'] = env('MEDIA_END').$team->image;
        }else{
            $data['image_url'] = env('MEDIA_END').'default-team.png';
        }

        if (!empty($team->cover_image)) {
            $data['cover_image'] = env('MEDIA_END').$team->cover_image;
        }

        $data['province'] = $team->province->id;
        $data['district'] = $team->district->id;
        $data['captain'] = $team->captain_id;


        $data['league'] = $team->league->toArray();
        $data['league']['province'] = $team->league->league_province->first()->province->toArray();
        $data['league']['season'] = null;

        $data['value'] = $team->player->sum('value'); // PLAYER DEĞERLERİNİ TOPLA
        $data['squad_locked'] = false;
        
        return response()->json($data, 200);
    }

    public function teaSeasons($id)
    {
        $data = array();
        
        $team = Team::find($id);

        $teamseasons = $team->team_teamseason;

        foreach ($teamseasons as $teamseason) {

            $season = $teamseason->season->toArray();
            $season['fullname'] = $teamseason->season->year.' '.$teamseason->season->league->name.' '.$teamseason->season->name;

            if($teamseason->season->polymorphic_ctype_id == 32){
                $season['type'] = 'elimination';
            }else if($teamseason->season->polymorphic_ctype_id == 33){
                $season['type'] = 'fixture';
            }else if($teamseason->season->polymorphic_ctype_id == 34){
                $season['type'] = 'point';
            }

            $data[] = $season;
        }
        
        return response()->json($data, 200);
    }

    public function teaFeaPlayers($id)
    {
        $data = array();
        $team = Team::find($id); 

        $data['captain'] = array();
        if(!empty($team->captain)){
            $data['captain']['player'] = $team->captain->toArray();
            $data['captain']['player']['fullname'] = $team->captain->user->first_name.' '.$team->captain->user->last_name;

            if (!empty($team->captain->user->image)) {
                $data['captain']['player']['image'] = env('MEDIA_END').$team->captain->user->image;
            }else{
                $data['captain']['player']['image'] = env('MEDIA_END').'default-player.png';
            }

            $data['captain']['player']['position'] = $team->captain->players_position->toArray();
        }

        $data['goal']['player']['position']['name'] = "";
        $data['goal']['player']['position']['code'] = "";
        $data['goal']['player']['number'] = "";
        $data['goal']['player']['image'] = "";
        $data['goal']['value'] = "";

        $data['rating']['player']['position']['name'] = "";
        $data['rating']['player']['position']['code'] = "";
        $data['rating']['player']['number'] = "";
        $data['rating']['player']['image'] = "";
        $data['rating']['value'] = "";

        // DEWAM

        /*
        foreach ($team->team_teamseason as $teamseason) {
            //$data['season'][] = $teamseason->season;
            $seasonArr[] = $teamseason->season;
        }

        $activeSeasonArr = collect($seasonArr)->where('end_date', '>=', Carbon::parse('2020-01-01')->format('Y-m-d'))->all();

        $players = $team->player;

        $high_goal = array('player' => null, 'value' => 0);
        $high_rated = array('player' => null, 'value' => 0);

        foreach ($players as $player) {
            //$data['player'][] = $player->team_playerteamhistory;
        }


        /*
            foreach ($players as $player) {
                $player_goal = $player.active_total_goal
                $player_rating = $player.active_total_rate
            }




            for player in players:
                player_goal = player.active_total_goal
                player_rating = player.active_total_rate

                if player_goal > high_goal['value']:
                    high_goal['player'] = player
                    high_goal['value'] = player_goal

                if player_rating > high_rated['value']:
                    high_rated['player'] = player
                    high_rated['value'] = player_rating

            return {
                'goal': high_goal,
                'rating': high_rated
            }
        */




        /*
        $teamseasons = $team->team_teamseason;

        foreach ($teamseasons as $teamseason) {

            $season = $teamseason->season->toArray();
            $season['fullname'] = $teamseason->season->year.' '.$teamseason->season->league->name.' '.$teamseason->season->name;

            if($teamseason->season->polymorphic_ctype_id == 32){
                $season['type'] = 'elimination';
            }else if($teamseason->season->polymorphic_ctype_id == 33){
                $season['type'] = 'fixture';
            }else if($teamseason->season->polymorphic_ctype_id == 34){
                $season['type'] = 'point';
            }

            $data[] = $season;
        }
        */
        
        return response()->json($data, 200);
    }

    public function teaMatches($id, Request $request)
    {
        $data = array();
        $data['results'] = array();
        $results = array();

        $completed = null;
        if(!empty($request->input('completed'))){
            $completed = $request->input('completed');
        }

        $team = Team::find($id);

        $page = 0;
        $pageCount = 12;
        $pageStart = 0;

        if(!empty($request->input('page'))){
            $page = $request->input('page');
            $pageStart = ($page - 1) * $pageCount;
        }

        $matches1 = $team->match1->where('completed', true);
        $matches2 = $team->match2->where('completed', true);
        $matches = $matches1->merge($matches2)->sortByDesc('date');


        
        foreach ($matches as $match) {

            $matchArr = $match->toArray();
            $matchArr['video_status'] = (!empty($match->match_video)) ? $match->match_video->status : 'recordable';

            if(empty($match->team1)){
                $matchArr['team1']['id'] = 0;
                $matchArr['team1']['name'] = 'Takım';
                $matchArr['team1']['image_url'] = env('MEDIA_END').'default-team.png';
                //$matchArr['team1']['league'] = $match->season->league->toArray();
            }else{
                $matchArr['team1'] = $match->team1->toArray();
                if (!empty($match->team1->image)) {
                    $matchArr['team1']['image_url'] = env('MEDIA_END').$match->team1->image;
                }else{
                    $matchArr['team1']['image_url'] = env('MEDIA_END').'default-team.png';
                }
                if(!empty($match->season)){
                    //$matchArr['team1']['league'] = $match->season->league->toArray();
                    //$matchArr['team1']['league']['province'] = $match->season->league->league_province->first()->province->toArray();
                    //$matchArr['team1']['league']['season'] = $match->season->toArray();
                }
            }


            if(empty($match->team2)){
                $matchArr['team2']['id'] = 0;
                $matchArr['team2']['name'] = 'Takım';
                $matchArr['team2']['image_url'] = env('MEDIA_END').'default-team.png';
                //$matchArr['team2']['league'] = $match->season->league->toArray();
            }else{
                $matchArr['team2'] = $match->team2->toArray();
                if (!empty($match->team2->image)) {
                    $matchArr['team2']['image_url'] = env('MEDIA_END').$match->team2->image;
                }else{
                    $matchArr['team2']['image_url'] = env('MEDIA_END').'default-team.png';
                }
                if(!empty($match->season)){
                    //$matchArr['team2']['league'] = $match->season->league->toArray();
                    //$matchArr['team2']['league']['province'] = $match->season->league->league_province->first()->province->toArray();
                    //$matchArr['team2']['league']['season'] = $match->season->toArray();
                }
            }

            if(!empty($match->coordinator)){
                $matchArr['coordinator'] = $match->coordinator->toArray();
                $matchArr['coordinator']['fullname'] = $match->coordinator->user->first_name.' '.$match->coordinator->user->last_name;
                
                if(!empty($match->coordinator->user->image)){
                    $matchArr['coordinator']['image_url'] = env('MEDIA_END').$match->coordinator->user->image;
                }else{
                    $matchArr['coordinator']['image_url'] = env('MEDIA_END').'default-player.png';
                }
            }else{
                $matchArr['coordinator'] = null;
            }

            if(!empty($match->referee)){
                $matchArr['referee'] = $match->referee->toArray();
                $matchArr['referee']['fullname'] = $match->referee->user->first_name.' '.$match->referee->user->last_name;
                
                if(!empty($match->referee->user->image)){
                    $matchArr['referee']['image_url'] = env('MEDIA_END').$match->referee->user->image;
                }else{
                    $matchArr['referee']['image_url'] = env('MEDIA_END').'default-player.png';
                }
            }else{
                $matchArr['referee'] = null;
            }

            if(!empty($match->ground)){
                $matchArr['ground'] = $match->ground->toArray();
            }else{
                $matchArr['ground'] = null;
            }
            
            if(!empty($match->season)){
                $matchArr['league']['id'] = $match->season->league->id;
                $matchArr['league']['name'] = $match->season->league->name;
            }

            $results[] = $matchArr;
            
        }
        
        $data['count'] = count($results);

        $data['next'] = null;
        if(!empty($page) && (count($results) / $pageCount) > $page){
            $data['next'] = env('MEDIA_END').'players/-id-/matches/?page='.($page+1);
        }

        $data['previous'] = null;
        if(!empty($page) && $page > 1){
            $data['previous'] = env('MEDIA_END').'players/-id-/matches/?page='.($page-1);
        }
        
        $data['results'] = array_values(collect($results)->slice($pageStart, $pageCount)->toArray());
    
        return response()->json($data, 200);
    }

    public function teaMatchSummaries($id, Request $request)
    {
        $data = array();
        $data['results'] = array();
        $results = array();

        $completed = null;
        if(empty($request->input('completed'))){
            $completed = $request->input('completed');
        }

        $team = Team::find($id);

        $page = 0;
        $pageCount = 12;
        $pageStart = 0;

        if(!empty($request->input('page'))){
            $page = $request->input('page');
            $pageStart = ($page - 1) * $pageCount;
        }

        $matches1 = collect($team->match1)->where('completed', true);
        $matches2 = collect($team->match2)->where('completed', true);
        $matches = $matches1->merge($matches2)->sortByDesc('date');
        
        foreach ($matches as $match) {

            if(!empty($match->match_video) && !empty($match->match_video->summary_embed_code)){

                $summ['full_embed_code'] = $match->match_video->full_embed_code;
                if ($match->match_video->full_source == 'app'){
                    $summ['full_embed_url'] = $match->match_video->full_embed_code;
                }else if ($match->match_video->full_source == 'vimeo'){
                    $summ['full_embed_url'] = 'https://player.vimeo.com/video/'.$match->match_video->full_embed_code;
                }else if ($match->match_video->full_source == 'youtube'){
                    $summ['full_embed_url'] = 'https://www.youtube.com/embed/'.$match->match_video->full_embed_code;
                }
                $summ['full_source'] = $match->match_video->full_source;



                $summ['live_embed_code'] = $match->match_video->live_embed_code;
                if ($match->match_video->live_source == 'app'){
                    $summ['live_embed_url'] = $match->match_video->live_embed_code;
                }else if ($match->match_video->live_source == 'vimeo'){
                    $summ['live_embed_url'] = 'https://player.vimeo.com/video/'.$match->match_video->live_embed_code;
                }else if ($match->match_video->live_source == 'youtube'){
                    $summ['live_embed_url'] = 'https://www.youtube.com/embed/'.$match->match_video->live_embed_code;
                }
                $summ['live_source'] = $match->match_video->live_source;



                $summ['summary_embed_code'] = $match->match_video->summary_embed_code;
                if ($match->match_video->summary_source == 'app'){
                    $summ['summary_embed_url'] = $match->match_video->summary_embed_code;
                    $summ['summary_image_url'] = url('default.jpg');
                }else if ($match->match_video->summary_source == 'vimeo'){
                    $summ['summary_embed_url'] = 'https://player.vimeo.com/video/'.$match->match_video->summary_embed_code;
                    $summ['summary_image_url'] = url('default.jpg');
                }else if ($match->match_video->summary_source == 'youtube'){
                    $summ['summary_embed_url'] = 'https://www.youtube.com/embed/'.$match->match_video->summary_embed_code;
                    $summ['summary_image_url'] = 'https://i3.ytimg.com/vi/'.$match->match_video->summary_embed_code.'/hqdefault.jpg';
                }
                $summ['summary_source'] = $match->match_video->summary_source;

                $summ['status'] = $match->match_video->status;

                $matchArr = $match->toArray();
                $matchArr['video_status'] = (!empty($match->match_video)) ? $match->match_video->status : 'recordable';

                if(empty($match->team1)){
                    $matchArr['team1']['id'] = 0;
                    $matchArr['team1']['name'] = 'Takım';
                    $matchArr['team1']['image_url'] = env('MEDIA_END').'default-team.png';
                    if(!empty($match->season)){
                        $matchArr['team1']['league'] = $match->season->league->toArray();
                    }
                }else{
                    $matchArr['team1'] = $match->team1->toArray();
                    
                    if (!empty($match->team1->image)) {
                        $matchArr['team1']['image_url'] = env('MEDIA_END').$match->team1->image;
                    }else{
                        $matchArr['team1']['image_url'] = env('MEDIA_END').'default-team.png';
                    }

                    if(!empty($match->season)){
                        $matchArr['team1']['league'] = $match->season->league->toArray();
                        //$matchArr['team1']['league']['province'] = $match->season->league->league_province->first()->province->toArray();
                        $matchArr['team1']['league']['season'] = $match->season->toArray();

                        if($match->season->polymorphic_ctype_id == 32){
                            $matchArr['team1']['league']['season']['type'] = 'elimination';
                        }else if($match->season->polymorphic_ctype_id == 33){
                            $matchArr['team1']['league']['season']['type'] = 'fixture';
                        }else if($match->season->polymorphic_ctype_id == 34){
                            $matchArr['team1']['league']['season']['type'] = 'point';
                        }
                    }
                }

                if(empty($match->team2)){
                    $matchArr['team2']['id'] = 0;
                    $matchArr['team2']['name'] = 'Takım';
                    $matchArr['team2']['image_url'] = env('MEDIA_END').'default-team.png';
                    if(!empty($match->season)){
                        $matchArr['team2']['league'] = $match->season->league->toArray();
                    }
                }else{
                    $matchArr['team2'] = $match->team2->toArray();

                    if (!empty($match->team2->image)) {
                        $matchArr['team2']['image_url'] = env('MEDIA_END').$match->team2->image;
                    }else{
                        $matchArr['team2']['image_url'] = env('MEDIA_END').'default-team.png';
                    }
                    if(!empty($match->season)){
                        $matchArr['team2']['league'] = $match->season->league->toArray();
                        //$matchArr['team2']['league']['province'] = $match->season->league->league_province->first()->province->toArray();
                        $matchArr['team2']['league']['season'] = $match->season->toArray();

                        if($match->season->polymorphic_ctype_id == 32){
                            $matchArr['team2']['league']['season']['type'] = 'elimination';
                        }else if($match->season->polymorphic_ctype_id == 33){
                            $matchArr['team2']['league']['season']['type'] = 'fixture';
                        }else if($match->season->polymorphic_ctype_id == 34){
                            $matchArr['team2']['league']['season']['type'] = 'point';
                        }
                    }
                }

                if(!empty($match->coordinator)){
                    $matchArr['coordinator'] = $match->coordinator->toArray();
                    $matchArr['coordinator']['fullname'] = $match->coordinator->user->first_name.' '.$match->coordinator->user->last_name;
                    
                    if (!empty($match->coordinator->user->image)) {
                        $matchArr['coordinator']['image_url'] = env('MEDIA_END').$match->coordinator->user->image;
                    }else{
                        $matchArr['coordinator']['image_url'] = env('MEDIA_END').'default-player.png';
                    }

                }else{
                    $matchArr['coordinator'] = null;
                }

                if(!empty($match->referee)){
                    $matchArr['referee'] = $match->referee->toArray();
                    $matchArr['referee']['fullname'] = $match->referee->user->first_name.' '.$match->referee->user->last_name;

                    if (!empty($match->referee->user->image)) {
                        $matchArr['referee']['image_url'] = env('MEDIA_END').$match->referee->user->image;
                    }else{
                        $matchArr['referee']['image_url'] = env('MEDIA_END').'default-player.png';
                    }

                }else{
                    $matchArr['referee'] = null;
                }

                if(!empty($match->ground)){
                    $matchArr['ground'] = $match->ground->toArray();
                }else{
                    $matchArr['ground'] = null;
                }

                if(!empty($match->season)){
                    $matchArr['league']['id'] = $match->season->league->id;
                    $matchArr['league']['name'] = $match->season->league->name;
                }

                $summ['match'] = $matchArr;
                $results[] = $summ;

            }

        }
        
        $data['count'] = count($results);

        $data['next'] = null;
        if(!empty($page) && (count($results) / $pageCount) > $page){
            $data['next'] = env('MEDIA_END').'teams/-id-/match_summaries/?page='.($page+1);
        }

        $data['previous'] = null;
        if(!empty($page) && $page > 1){
            $data['previous'] = env('MEDIA_END').'teams/-id-/match_summaries/?page='.($page-1);
        }
        
        $data['results'] = array_values(collect($results)->slice($pageStart, $pageCount)->toArray());
    
        return response()->json($data, 200);
    }

    public function teaSeasonPerformance($id, Request $request)
    {

        $data = array();

        $team = Team::find($id);
        $seasonId = null;
        if(!empty($request->input('seasonId'))){
            $seasonId = $request->input('seasonId');
            $season = Season::find($seasonId);
            $team_season = TeamTeamSeason::where('season_id', $season->id)->where('team_id', $team->id)->first();

            $data = $team_season->toArray();
            $data['season'] = $team_season->season_id;
            $data['squad_locked_player'] = $team_season->squad_locked_player_id;


            $data['team'] = $team->toArray();
            
            if (!empty($team->image)) {
                $data['team']['image_url'] = env('MEDIA_END').$team->image;
            }else{
                $data['team']['image_url'] = env('MEDIA_END').'default-team.png';
            }
            $data['team']['province'] = $team->province->id;
            $data['team']['district'] = $team->district->id;
            $data['team']['captain'] = $team->captain_id;

            $data['team']['league'] = $team->league->toArray();
            $data['team']['league']['province'] = $team->league->league_province->first()->province->toArray();
            $data['team']['league']['season'] = null;

            $teamid = $team->id;
            $data['last_five_match_result'] = array();
            $l5ms = Match::where('season_id', $seasonId)->where('completed', true)->
                where(function ($query) use ($teamid) {
                    $query->where('team1_id', $teamid)
                        ->orWhere('team2_id', $teamid);
                })->orderBy('date', 'desc')->limit(5)->get();
            
            foreach ($l5ms as $l5m) {
                if($l5m->team1_goal == $l5m->team2_goal){
                    $data['last_five_match_result'][] = 0;
                }else if($l5m->team1_goal > $l5m->team2_goal){
                    $data['last_five_match_result'][] = 1;
                }else if($l5m->team1_goal < $l5m->team2_goal){
                    $data['last_five_match_result'][] = 2;
                }
            }

            $data['season_performance']['yellow_card'] = $team_season->yellow_card;
            $data['season_performance']['red_card'] = $team_season->red_card;
            $data['season_performance']['gk_save'] = $team_season->gk_save;
            /*
            $r = 0.0;
            foreach ($team_season->team->team_playerteamhistory as $value) {
                $r += $value->rating;
            }
            $data['season_performance']['total_rating'] = ($team_season->team->team_playerteamhistory->count() > 0) ? number_format((float)($r / $team_season->team->team_playerteamhistory->count()), 1, '.', '') : 0;
            */
            return response()->json($data, 200);
        
        }
    }

    public function teaLastMatchSquad($id)
    {

        $data = array();

        $team = Team::find($id);
        $teamid = $team->id;
        $match = Match::where('completed', true)->
                where(function ($query) use ($teamid) {
                    $query->where('team1_id', $teamid)
                        ->orWhere('team2_id', $teamid);
                })->orderBy('date', 'desc')->first();

        if(!empty($match)){

            foreach ($match->match_player->where('team_id', $teamid) as $mplayer) {
                
                $mplyr = $mplayer->toArray();

                if(empty($mplayer->guest_name)){
                    if(!empty($mplayer->player->user)){
                        $mplyr['fullname'] = $mplayer->player->user->first_name.' '.$mplayer->player->user->last_name;
                    }else{
                        $mplyr['fullname'] = '';
                    }                        
                    $mplyr['is_guest'] = false;
                    $mplyr['player'] = $mplayer->player->id;
                    $mplyr['value'] = (int)$mplayer->player->value;
                    $mplyr['old_system_value'] = (int)$mplayer->player->old_system_value;

                    if(!empty($mplayer->player->user->image)){
                        $mplyr['image_url'] = env('MEDIA_END').$mplayer->player->user->image;
                    }else{
                        $mplyr['image_url'] = env('MEDIA_END').'default-player.png';
                    }

                }else{

                    $mplyr['fullname'] = $mplayer->guest_name;
                    $mplyr['is_guest'] = true;
                    $mplyr['player'] = null;
                    $mplyr['value'] = null;
                    $mplyr['image_url'] =env('MEDIA_END').'default-player.png';

                }

                $mplyr['position'] = $mplayer->player_position->toArray();

                $mplyr['goal'] = $mplayer->match_action_goal->count();
                $mplyr['red_card'] = $mplayer->match_action_red_card->count();
                $mplyr['yellow_card'] = $mplayer->match_action_yellow_card->count();

                if($mplayer->position_id == 1){
                    $mplyr['saving'] = $mplayer->match->match_action->where('type', 'saving')->where('team_id', $team->id)->count();
                }else{
                    $mplyr['saving'] = 0;
                }


                $mplyr['team'] = $team->toArray();
                
                if (!empty($team->image)) {
                    $mplyr['team']['image_url'] = env('MEDIA_END').$team->image;
                }else{
                    $mplyr['team']['image_url'] = env('MEDIA_END').'default-team.png';
                }

                $mplyr['team']['league'] = $team->league->toArray();
                $mplyr['team']['league']['province'] = $team->league->league_province->first()->province->toArray();
                $mplyr['team']['league']['season'] = null;



                $matchArr = $match->toArray();
                $matchArr['video_status'] = (!empty($match->match_video)) ? $match->match_video->status : 'recordable';
                $matchArr['season'] = $match->season_id;

                if(empty($match->team1)){
                    $matchArr['team1']['id'] = 0;
                    $matchArr['team1']['name'] = 'Takım';
                    $matchArr['team1']['image_url'] = env('MEDIA_END').'default-team.png';
                    $matchArr['team1']['league'] = $match->season->league->toArray();
                }else{
                    $matchArr['team1'] = $match->team1->toArray();
                    
                    if (!empty($match->team1->image)) {
                        $matchArr['team1']['image_url'] = env('MEDIA_END').$match->team1->image;
                    }else{
                        $matchArr['team1']['image_url'] = env('MEDIA_END').'default-team.png';
                    }

                    $matchArr['team1']['league'] = $match->season->league->toArray();
                    //$matchArr['team1']['league']['province'] = $match->season->league->league_province->first()->province->toArray();
                    $matchArr['team1']['league']['season'] = null;
                }

                if(empty($match->team2)){
                    $matchArr['team2']['id'] = 0;
                    $matchArr['team2']['name'] = 'Takım';
                    $matchArr['team2']['image_url'] = env('MEDIA_END').'default-team.png';
                    $matchArr['team2']['league'] = $match->season->league->toArray();
                }else{
                    $matchArr['team2'] = $match->team2->toArray();
                
                    if (!empty($match->team2->image)) {
                        $matchArr['team2']['image_url'] = env('MEDIA_END').$match->team2->image;
                    }else{
                        $matchArr['team2']['image_url'] = env('MEDIA_END').'default-team.png';
                    }

                    $matchArr['team2']['league'] = $match->season->league->toArray();
                    //$matchArr['team2']['league']['province'] = $match->season->league->league_province->first()->province->toArray();
                    $matchArr['team2']['league']['season'] = null;
                }

                
                $matchArr['match_player'] = null;

                $mplyr['match'] = $matchArr;

                $data[] = $mplyr;

            }

        }

        return response()->json($data, 200);
    }

    public function teaRecentMatches($id, Request $request)
    {


        $data = array();
        $data['results'] = array();
        $results = array();

        $team = Team::find($id);

        $matches1 = collect($team->match1)->where('completed', true);
        $matches2 = collect($team->match2)->where('completed', true);
        $matches = $matches1->merge($matches2)->sortByDesc('date');
        
        if(!empty($request->input('limit'))){
            $matches = $matches->slice(0, $request->input('limit'));
        }

        foreach ($matches as $match) {

            $matchArr = $match->toArray();
            $matchArr['video_status'] = (!empty($match->match_video)) ? $match->match_video->status : 'recordable';

            if(empty($match->team1)){
                $matchArr['team1']['id'] = 0;
                $matchArr['team1']['name'] = 'Takım';
                $matchArr['team1']['image_url'] = env('MEDIA_END').'default-team.png';
                $matchArr['team1']['league'] = $match->season->league->toArray();
            }else{
                $matchArr['team1'] = $match->team1->toArray();
                
                if (!empty($match->team1->image)) {
                    $matchArr['team1']['image_url'] = env('MEDIA_END').$match->team1->image;
                }else{
                    $matchArr['team1']['image_url'] = env('MEDIA_END').'default-team.png';
                }

                $matchArr['team1']['league'] = $match->season->league->toArray();
                //$matchArr['team1']['league']['province'] = $match->season->league->league_province->first()->province->toArray();
                $matchArr['team1']['league']['season'] = $match->season->toArray();

                if($match->season->polymorphic_ctype_id == 32){
                    $matchArr['team1']['league']['season']['type'] = 'elimination';
                }else if($match->season->polymorphic_ctype_id == 33){
                    $matchArr['team1']['league']['season']['type'] = 'fixture';
                }else if($match->season->polymorphic_ctype_id == 34){
                    $matchArr['team1']['league']['season']['type'] = 'point';
                }

            }


            if(empty($match->team2)){
                $matchArr['team2']['id'] = 0;
                $matchArr['team2']['name'] = 'Takım';
                $matchArr['team2']['image_url'] = env('MEDIA_END').'default-team.png';
                $matchArr['team2']['league'] = $match->season->league->toArray();
            }else{
                $matchArr['team2'] = $match->team2->toArray();

                if (!empty($match->team2->image)) {
                    $matchArr['team2']['image_url'] = env('MEDIA_END').$match->team2->image;
                }else{
                    $matchArr['team2']['image_url'] = env('MEDIA_END').'default-team.png';
                }
                
                $matchArr['team2']['league'] = $match->season->league->toArray();
                //$matchArr['team2']['league']['province'] = $match->season->league->league_province->first()->province->toArray();
                $matchArr['team2']['league']['season'] = $match->season->toArray();

                if($match->season->polymorphic_ctype_id == 32){
                    $matchArr['team2']['league']['season']['type'] = 'elimination';
                }else if($match->season->polymorphic_ctype_id == 33){
                    $matchArr['team2']['league']['season']['type'] = 'fixture';
                }else if($match->season->polymorphic_ctype_id == 34){
                    $matchArr['team2']['league']['season']['type'] = 'point';
                }
            }

            if(!empty($match->coordinator)){
                $matchArr['coordinator'] = $match->coordinator->toArray();
                $matchArr['coordinator']['fullname'] = $match->coordinator->user->first_name.' '.$match->coordinator->user->last_name;
                
                if(!empty($match->coordinator->user->image)){
                    $matchArr['coordinator']['image_url'] = env('MEDIA_END').$match->coordinator->user->image;
                }else{
                    $matchArr['coordinator']['image_url'] = env('MEDIA_END').'default-player.png';
                }
            }else{
                $matchArr['coordinator'] = null;
            }

            if(!empty($match->referee)){
                $matchArr['referee'] = $match->referee->toArray();
                $matchArr['referee']['fullname'] = $match->referee->user->first_name.' '.$match->referee->user->last_name;
                
                if(!empty($match->referee->user->image)){
                    $matchArr['referee']['image_url'] = env('MEDIA_END').$match->referee->user->image;
                }else{
                    $matchArr['referee']['image_url'] = env('MEDIA_END').'default-player.png';
                }
            }else{
                $matchArr['referee'] = null;
            }

            if(!empty($match->ground)){
                $matchArr['ground'] = $match->ground->toArray();
            }else{
                $matchArr['ground'] = null;
            }
            
            $matchArr['league']['id'] = $match->season->league->id;
            $matchArr['league']['name'] = $match->season->league->name;

            $results[] = $matchArr;
            
        }
        
        $data = array_values(collect($results)->toArray());
    
        return response()->json($data, 200);
    }

    public function teaPlayers($id)
    {

        $dataee = array();
        $data = array();
        $team = Team::find($id);

        /***** Aktif Sezon gg*****/
        $activeSeason = null;
        $activeSeasons = array();
        $existSeasons = array();
        $t_seasons = $team->team_teamseason;

        foreach ($t_seasons as $t_season) {
            if($t_season->season->active == true){
                if($t_season->season->polymorphic_ctype_id != 32){
                    if($t_season->season->start_date <= Carbon::now()->format('Y-m-d') && $t_season->season->end_date >= Carbon::now()->format('Y-m-d')){
                        $activeSeasons[] = $t_season->season;
                    }
                    $existSeasons[] = $t_season->season;
                }
            }
        }

        if(count($activeSeasons) > 1){
            foreach ($activeSeasons as $season) {
                if($season->polymorphic_ctype_id == 33){

                    $fs_group_teams_id = array();
                    foreach ($season->league_fixtureseason->groups as $group) {
                        foreach ($group->group_teams as $group_team) {
                            $fs_group_teams_id[] = $group_team->team_id;
                        }
                    }
                    if(array_search($team->id, $fs_group_teams_id)){
                        $activeSeason = $season;
                    }

                }
            }
        }else{
            if(count($existSeasons)){
                $activeSeason = $existSeasons[0];
            }
        }

        if(empty($activeSeason)){
            $activeSeason = $activeSeasons[0];
        }elseif(!empty($activeSeason)){

            if(!empty($team->team_teamseason->where('season_id', $activeSeason->id)->first())){
                $remaining_transfer_count = $team->team_teamseason->where('season_id', $activeSeason->id)->first()->remaining_transfer_count;
                $squad_locked = $team->team_teamseason->where('season_id', $activeSeason->id)->first()->squad_locked;
            }else{
                $remaining_transfer_count = $activeSeason->allowed_transfer_count;
                $squad_locked = false;
            }
        }
        /***** Aktif Sezon gg*****/

        $players = $team->player;

        foreach ($players as $player) {
            $plyr = $player->toArray();

            $plyr['activeSeason'] = $activeSeason;
            $plyr['activeSeasons'] = $activeSeasons;
            $plyr['existSeasons'] = $existSeasons;

            $plyr['position'] = (!empty($player->players_position)) ? $player->players_position->toArray() : null;
            $plyr['position2'] = (!empty($player->players_position2)) ? $player->players_position2->toArray() : null;
            $plyr['position3'] = (!empty($player->players_position3)) ? $player->players_position3->toArray() : null;

            $plyr['value'] = (int)$player->value;
            $plyr['old_system_value'] = (int)$player->old_system_value;
            
            $plyr['user']['first_name'] = $player->user->first_name;
            $plyr['user']['last_name'] = $player->user->last_name;
            $plyr['user']['fullname'] = $player->user->first_name.' '.$player->user->last_name;

            if(!empty($player->user->image)){
                $plyr['user']['image_url'] = env('MEDIA_END').$player->user->image;
            }else{
                $plyr['user']['image_url'] = env('MEDIA_END').'default-player.png';
            }

            $plyr['user']['age'] = (!empty($player->user->profile->birth_date)) ? Carbon::parse($player->user->profile->birth_date)->age : null;

            $plyr['user']['profile'] = $player->user->profile->toArray();
            array_forget($plyr, 'user.profile.identity_number');
            $plyr['user']['profile']['province'] = (!empty($player->user->profile->province)) ? $player->user->profile->province->toArray() : null;
            $plyr['user']['profile']['district'] = (!empty($player->user->profile->district)) ? $player->user->profile->district->toArray() : null;

            $plyr['team'] = $team->toArray();
            
            if (!empty($team->image)) {
                $plyr['team']['image_url'] = env('MEDIA_END').$team->image;
            }else{
                $plyr['team']['image_url'] = env('MEDIA_END').'default-team.png';
            }

            $plyr['team']['player'] = null;
            $plyr['team']['league'] = $team->league->toArray();
            $plyr['team']['league']['province'] = $team->league->league_province->first()->province->toArray();
            $plyr['team']['league']['season'] = null;

            $plyr['history_total'] = array('match' => TeamPlayerTeamHistory::where('player_id', $player->id)->sum('match'), 'goal' => TeamPlayerTeamHistory::where('player_id', $player->id)->sum('goal'), 'gk_save' => TeamPlayerTeamHistory::where('player_id', $player->id)->sum('gk_save'));

            if(!is_null($activeSeason)){
                $playerteamhistory = $player->team_playerteamhistory->where('season_id', $activeSeason->id)->where('team_id', $id)->first();
                $plyr['playerteamhistory'] = $playerteamhistory;
            }
            
            if(!empty($playerteamhistory)){
                $plyr['total_match'] = (int)$playerteamhistory->match;
                $plyr['total_goal'] = (int)$playerteamhistory->goal;
                $plyr['total_rate'] = (float)$playerteamhistory->rating;
                if($plyr['position']['code'] == "KL"){
                    $plyr['total_gk_save'] = (float)$playerteamhistory->gk_save;
                }else{
                    $plyr['total_gk_save'] = "-";
                }
                //$plyr['five_match_rate'] = 7.6;
            }else{
                $plyr['total_match'] = 0;
                $plyr['total_goal'] = 0;
                $plyr['total_rate'] = 0.0;
                $plyr['total_gk_save'] = 0;
                //$plyr['five_match_rate'] = 7.6;
            }

            $data[] = $plyr;
        }
        
        return response()->json($data, 200);
    }

    public function teaPenalties($id)
    {

        $data = array();
        $data['player'] = array();
        $data['team'] = array();
        $_ids = array();
        
        $team = Team::find($id);
        $player_penalties = $team->player_penalties;

        foreach ($player_penalties as $ppenalty) {

            $penalty = $ppenalty->penalty;

            $pArr = $penalty->toArray();

            $pArr['match']['id'] = $penalty->match->id;
            $pArr['match']['datetime'] = $penalty->match->date.'T'.$penalty->match->time;


            $pArr['match']['team1']['id'] = $penalty->match->team1->id;
            $pArr['match']['team1']['name'] = $penalty->match->team1->name;
            if(!empty($penalty->match->team1->image)){
                $pArr['match']['team1']['image_url'] = env('MEDIA_END').$penalty->match->team1->image;
            }else{
                $pArr['match']['team1']['image_url'] = env('MEDIA_END').'default-team.png';
            }

            $pArr['match']['team2']['id'] = $penalty->match->team2->id;
            $pArr['match']['team2']['name'] = $penalty->match->team2->name;
            if(!empty($penalty->match->team2->image)){
                $pArr['match']['team2']['image_url'] = env('MEDIA_END').$penalty->match->team2->image;
            }else{
                $pArr['match']['team2']['image_url'] = env('MEDIA_END').'default-team.png';
            }

            $pArr['match']['season']['id'] = $penalty->season->id;
            $pArr['match']['season']['name'] = $penalty->season->name;
            $pArr['match']['season']['fullname'] = $penalty->season->year.' '.$penalty->season->league->name.' '.$penalty->season->name;
            $pArr['match']['season']['year'] = $penalty->season->year;

            if($penalty->season->polymorphic_ctype_id == 32){
                $pArr['match']['season']['type'] = 'elimination';
            }else if($penalty->season->polymorphic_ctype_id == 33){
                $pArr['match']['season']['type'] = 'fixture';
            }else if($penalty->season->polymorphic_ctype_id == 34){
                $pArr['match']['season']['type'] = 'point';
            }

            $pArr['match']['season']['start_date'] = $penalty->season->start_date;
            $pArr['match']['season']['end_date'] = $penalty->season->end_date;

            //$pArr['ctype'] = $penalty->ctype->toArray();
            if($penalty->ctype->model == 'penalplayer'){

                $pArr['penal_type'] = $penalty->player_penalty->penalty_type->toArray();
                //$pArr['ctype']['player_penalty'] = $penalty->player_penalty->toArray();

                if(!empty($penalty->player_penalty->player)){
                    $pArr['player'] = $penalty->player_penalty->player->toArray();

                    $pArr['player']['fullname'] = $penalty->player_penalty->player->user->first_name.' '.$penalty->player_penalty->player->user->last_name;
                    if(!empty($penalty->player_penalty->player->user->image)){
                        $pArr['player']['image_url'] = env('MEDIA_END').$penalty->player_penalty->player->user->image;
                    }else{
                        $pArr['player']['image_url'] = env('MEDIA_END').'default-player.png';
                    }

                    $pArr['player']['position'] = (!empty($penalty->player_penalty->player->players_position)) ? $penalty->player_penalty->player->players_position->toArray() : null;
                    $pArr['player']['position2'] = (!empty($penalty->player_penalty->player->players_position2)) ? $penalty->player_penalty->player->players_position2->toArray() : null;
                    $pArr['player']['position3'] = (!empty($penalty->player_penalty->player->players_position3)) ? $penalty->player_penalty->player->players_position3->toArray() : null;

                    $pArr['player']['value'] = (int)$penalty->player_penalty->player->value;
                    $pArr['player']['old_system_value'] = (int)$penalty->player_penalty->player->old_system_value;

                    if(!empty($penalty->player_penalty->team)){
                        $pArr['player']['team'] = $penalty->player_penalty->team->toArray();
                        
                        if (!empty($penalty->player_penalty->team->image)) {
                            $pArr['player']['team']['image_url'] = env('MEDIA_END').$penalty->player_penalty->team->image;
                        }else{
                            $pArr['player']['team']['image_url'] = env('MEDIA_END').'default-team.png';
                        }

                        $pArr['player']['team']['league'] = $penalty->player_penalty->team->league->toArray();
                        $pArr['player']['team']['league']['province'] = $penalty->player_penalty->team->league->league_province->first()->province->toArray();
                        $pArr['player']['team']['league']['season'] = null;
                    }else{
                        $pArr['player']['team'] = null;
                    }

                    $pArr['image'] = $pArr['player']['image_url'];
                    $pArr['guest_name'] = '';
                }else{
                    $pArr['player'] = null;
                    $pArr['guest_name'] = $penalty->player_penalty->guest_name;
                    $pArr['image'] = env('MEDIA_END').'default-player.png';
                }

                if(!empty($pArr['player']['fullname'])){
                    $pArr['who'] = $pArr['player']['fullname'];
                }else{
                    $pArr['who'] = $pArr['guest_name'];
                }
                $pArr['team'] = $pArr['player']['team'];
                
                $data['player'][] = $pArr;
                
            }
        }
        

        $team = Team::find($id);
        $team_penalties = $team->team_penalties;

        foreach ($team_penalties as $tpenalty) {

            $penalty = $tpenalty->penalty;
            $pArr = $penalty->toArray();

            $pArr['match']['id'] = $penalty->match->id;
            $pArr['match']['datetime'] = $penalty->match->date.'T'.$penalty->match->time;


            $pArr['match']['team1']['id'] = $penalty->match->team1->id;
            $pArr['match']['team1']['name'] = $penalty->match->team1->name;
            if(!empty($penalty->match->team1->image)){
                $pArr['match']['team1']['image_url'] = env('MEDIA_END').$penalty->match->team1->image;
            }else{
                $pArr['match']['team1']['image_url'] = env('MEDIA_END').'default-team.png';
            }

            $pArr['match']['team2']['id'] = $penalty->match->team2->id;
            $pArr['match']['team2']['name'] = $penalty->match->team2->name;
            if(!empty($penalty->match->team2->image)){
                $pArr['match']['team2']['image_url'] = env('MEDIA_END').$penalty->match->team2->image;
            }else{
                $pArr['match']['team2']['image_url'] = env('MEDIA_END').'default-team.png';
            }

            $pArr['match']['season']['id'] = $penalty->season->id;
            $pArr['match']['season']['name'] = $penalty->season->name;
            $pArr['match']['season']['fullname'] = $penalty->season->year.' '.$penalty->season->league->name.' '.$penalty->season->name;
            $pArr['match']['season']['year'] = $penalty->season->year;

            if($penalty->season->polymorphic_ctype_id == 32){
                $pArr['match']['season']['type'] = 'elimination';
            }else if($penalty->season->polymorphic_ctype_id == 33){
                $pArr['match']['season']['type'] = 'fixture';
            }else if($penalty->season->polymorphic_ctype_id == 34){
                $pArr['match']['season']['type'] = 'point';
            }

            $pArr['match']['season']['start_date'] = $penalty->season->start_date;
            $pArr['match']['season']['end_date'] = $penalty->season->end_date;

            //$pArr['ctype'] = $penalty->ctype->toArray();
            if($penalty->ctype->model == 'penalteam'){

                $pArr['penal_type'] = $penalty->team_penalty->penalty_type->toArray();

                if(!empty($penalty->team_penalty->team)){
                    $pArr['team'] = $penalty->team_penalty->team->toArray();

                    if(!empty($penalty->team_penalty->team->image)){
                        $pArr['team']['image_url'] = env('MEDIA_END').$penalty->team_penalty->team->image;
                    }else{
                        $pArr['team']['image_url'] = env('MEDIA_END').'default-team.png';
                    }

                    $pArr['team']['league'] = $penalty->team_penalty->team->league->toArray();
                    $pArr['team']['league']['province'] = $penalty->team_penalty->team->league->league_province->first()->province->toArray();
                    $pArr['team']['league']['season'] = null;
                }else{
                    $pArr['team'] = null;
                }
            
                $data['team'][] = $pArr;
            }
        }
        
        return response()->json($data, 200);
        
    }

    public function teaPressConferences($id, Request $request)
    {

        $data = array();
        $data['results'] = array();
        $results = array();

        $page = null;
        $pageCount = 12;
        $pageStart = 0;

        if(!empty($request->input('page'))){
            $page = $request->input('page');
            $pageStart = ($page - 1) * $pageCount;
        }

        $team = Team::find($id);

        $matches = Match::where('team1_id', $id)->orWhere('team2_id', $id)->orderBy('created_at', 'desc')->get();

        foreach ($matches as $match) {

            $press_conference = $match->match_press; 
                
            if(!empty($press_conference) && !empty($press_conference->embed_code)){

                $pconference = $press_conference;

                $pcArr = $pconference->toArray();
                
                if ($pconference->embed_source == 'app'){
                    $pcArr['embed_image_url'] = url('default.jpg');
                }else if ($pconference->embed_source == 'vimeo'){
                    $pcArr['embed_image_url'] = url('default.jpg');
                }else if ($pconference->embed_source == 'youtube'){
                    $pcArr['embed_image_url'] = 'https://i3.ytimg.com/vi/'.$pconference->embed_code.'/hqdefault.jpg';
                }

                if ($pconference->embed_source == 'app'){
                    $pcArr['embed_url'] = $pconference->embed_code;
                }else if ($pconference->embed_source == 'vimeo'){
                    $pcArr['embed_url'] = 'https://player.vimeo.com/video/'.$pconference->embed_code;
                }else if ($pconference->embed_source == 'youtube'){
                    $pcArr['embed_url'] = 'https://www.youtube.com/embed/'.$pconference->embed_code;
                }


                $matchArr = $pconference->match->toArray();
                $matchArr['video_status'] = (!empty($pconference->match->match_video)) ? $pconference->match->match_video->status : 'recordable';

                if(empty($pconference->match->team1)){
                    $matchArr['team1']['id'] = 0;
                    $matchArr['team1']['name'] = 'Takım';
                    $matchArr['team1']['image_url'] = env('MEDIA_END').'default-team.png';
                    $matchArr['team1']['league'] = $pconference->match->season->league->toArray();
                }else{
                    $matchArr['team1'] = $pconference->match->team1->toArray();
                    
                    if (!empty($pconference->match->team1->image)) {
                        $matchArr['team1']['image_url'] = env('MEDIA_END').$pconference->match->team1->image;
                    }else{
                        $matchArr['team1']['image_url'] = env('MEDIA_END').'default-team.png';
                    }
                    
                    $matchArr['team1']['league'] = $pconference->match->season->league->toArray();
                    //$matchArr['team1']['league']['province'] = $pconference->match->season->league->league_province->first()->province->toArray();
                    $matchArr['team1']['league']['season'] = null;
                }

                if(empty($pconference->match->team2)){
                    $matchArr['team2']['id'] = 0;
                    $matchArr['team2']['name'] = 'Takım';
                    $matchArr['team2']['image_url'] = env('MEDIA_END').'default-team.png';
                    $matchArr['team2']['league'] = $pconference->match->season->league->toArray();
                }else{
                    $matchArr['team2'] = $pconference->match->team2->toArray();
                    if (!empty($pconference->match->team2->image)) {
                        $matchArr['team2']['image_url'] = env('MEDIA_END').$pconference->match->team2->image;
                    }else{
                        $matchArr['team2']['image_url'] = env('MEDIA_END').'default-team.png';
                    }
                    $matchArr['team2']['league'] = $pconference->match->season->league->toArray();
                    //$matchArr['team2']['league']['province'] = $pconference->match->season->league->league_province->first()->province->toArray();
                    $matchArr['team2']['league']['season'] = null;
                }

                if(!empty($pconference->match->coordinator)){
                    $matchArr['coordinator'] = $pconference->match->coordinator->toArray();
                    $matchArr['coordinator']['fullname'] = $pconference->match->coordinator->user->first_name.' '.$pconference->match->coordinator->user->last_name;
                    
                    if(!empty($pconference->match->coordinator->user->image)){
                        $matchArr['coordinator']['image_url'] = env('MEDIA_END').$pconference->match->coordinator->user->image;
                    }else{
                        $matchArr['coordinator']['image_url'] = env('MEDIA_END').'default-player.png';
                    }
                }else{
                    $matchArr['coordinator'] = null;
                }

                if(!empty($pconference->match->referee)){
                    $matchArr['referee'] = $pconference->match->referee->toArray();
                    $matchArr['referee']['fullname'] = $pconference->match->referee->user->first_name.' '.$pconference->match->referee->user->last_name;
                    
                    if(!empty($pconference->match->referee->user->image)){
                        $matchArr['referee']['image_url'] = env('MEDIA_END').$pconference->match->referee->user->image;
                    }else{
                        $matchArr['referee']['image_url'] = env('MEDIA_END').'default-player.png';
                    }
                }else{
                    $matchArr['referee'] = null;
                }

                if(!empty($pconference->match->ground)){
                    $matchArr['ground'] = $pconference->match->ground->toArray();
                }else{
                    $matchArr['ground'] = null;
                }
                $matchArr['league']['id'] = $pconference->match->season->league->id;
                $matchArr['league']['name'] = $pconference->match->season->league->name;

                $pcArr['match'] = $matchArr;


                $teamArr = $team->toArray();

                if (!empty($team->image)) {
                    $teamArr['image_url'] = env('MEDIA_END').$team->image;
                }else{
                    $teamArr['image_url'] = env('MEDIA_END').'default-team.png';
                }

                $teamArr['league'] = $pconference->match->season->league->toArray();
                $teamArr['league']['province'] = $pconference->match->season->league->league_province->first()->province->toArray();
                $teamArr['league']['season'] = $pconference->match->season->toArray();

                if($pconference->match->season->polymorphic_ctype_id == 32){
                    $teamArr['league']['season']['type'] = 'elimination';
                }else if($pconference->match->season->polymorphic_ctype_id == 33){
                    $teamArr['league']['season']['type'] = 'fixture';
                }else if($pconference->match->season->polymorphic_ctype_id == 34){
                    $teamArr['league']['season']['type'] = 'point';
                }

                $pcArr['team'] = $teamArr;

                $pcArr['players'] = array();

                $results[] = $pcArr;
            }

        }

        $data['count'] = count($results);

        $data['next'] = null;
        if(!empty($page) && (count($results) / $pageCount) > $page){
            $data['next'] = env('MEDIA_END').'players/-id-/press_conferences/?page='.($page+1);
        }

        $data['previous'] = null;
        if(!empty($page) && $page > 1){
            $data['previous'] = env('MEDIA_END').'players/-id-/press_conferences/?page='.($page-1);
        }

        $data['results'] = array_values(collect($results)->sortByDesc('created_at')->slice($pageStart, $pageCount)->toArray());
        //$data['results'] = $team->team_press;

        return response()->json($data, 200);
    }

    public function teaChampionships($id)
    {

        $data = array();

        $championships = Championships::where('team_id', $id);
        $championships = $championships->orderBy('year','desc')->get();

        foreach ($championships as $championship) {
            $arr = array();

            $arr = $championship->toArray();
            $arr['season'] = $championship->season->toArray();
            $arr['season']['fullname'] = $championship->season->year.' '.$championship->season->league->name.' '.$championship->season->name;

            if($championship->season->polymorphic_ctype_id == 32){
                $arr['season']['type'] = 'elimination';
            }else if($championship->season->polymorphic_ctype_id == 33){
                $arr['season']['type'] = 'fixture';
            }else if($championship->season->polymorphic_ctype_id == 34){
                $arr['season']['type'] = 'point';
            }

            $arr['team'] = $championship->team->toArray();
            
            if (!empty($championship->team->image)) {
                $arr['team']['image_url'] = env('MEDIA_END').$championship->team->image;
            }else{
                $arr['team']['image_url'] = env('MEDIA_END').'default-team.png';
            }

            $data[] = $arr;
        }

        return response()->json($data, 200);
    }

    public function teaAssignCaptain(Request $request)
    {
        $team = Auth::user()->player->team;
        $team->captain_id = $request->input('new_captain');
        $team->save();
        
        return response()->json(array('success' => true), 200);
    }

    public function teaReleasePlayer(Request $request)
    {
        $player = Player::find($request->input('player'));
        $team = $player->team;

        if($player->id == $team->captain->id){
            return response()->json(array('error' => 'Takımdan ayrılmak için önce kaptanlığı başka bir oyuncuya devretmelisiniz.'), 400);    
        }

        if(!empty($player->team)){
            $last_team_player = TeamTeamPlayer::where('player_id', $player->id)->where('team_id', $team->id)->orderBy('created_at', 'desc')->first();
            if(!empty($last_team_player) && empty($last_team_player->released_date)){
                $last_team_player->released_date = Carbon::now();
                $last_team_player->save();
            }
        }

        $player->team_id = null;
        $player->save();

        return response()->json(array('success' => true), 200);
    }

    public function teaSocial($id)
    {

        $data = array();

        $team = Team::find($id);

        //$data = $team->toArray();

        $data['id'] = $team->id;
        $data['facebook'] = $team->facebook;
        $data['twitter'] = $team->twitter;
        $data['instagram'] = $team->instagram;
        
        return response()->json($data, 200);
    }

    public function teaSocialUpdate($id, Request $request)
    {

        $data = array();

        $team = Team::find($id);

        $team->facebook = $request->facebook;
        $team->twitter = $request->twitter;
        $team->instagram = $request->instagram;
        $team->save();
        
        return response()->json($team, 200);
    }

    /********** TAKIM ***********/
    /****************************/

    /****************************/
    /********* OYUNCU ***********/

    public function playerSearch(Request $request)
    {
        $data = array();
        $search = $request->input('search');
        $province_id = $request->input('user__profile__province');

        $users = User::select(
            'users.id',
            DB::raw("CONCAT(users.first_name, ' ', users.last_name) as fullname"),
            'users.first_name',
            'users.last_name',
            'users.image',
            //'players.id as playerr_id',
            'players.number',
            'players.foot',
            'players.weight',
            'players.height',
            'players.facebook',
            'players.twitter',
            'players.instagram',
            'players.value',
            'profiles.province_id'
            
        )->leftJoin('players', 'users.id', '=', 'players.user_id')->leftJoin('profiles', 'users.id', '=', 'profiles.user_id');

        $users = $users->where(function ($query) use ($search) {
            $query->where(DB::raw("REPLACE(CONCAT(users.first_name, ' ', users.last_name),'  ',' ')"), 'ilike', '%'.$search.'%');
        });

        if(!empty($province_id)){
            $users = $users->where('profiles.province_id', $province_id);
        }
        $users = $users->limit(5)->get();

        $data['results'] = array();
        foreach ($users as $user) {
            $userArr = $user->toArray();
            array_forget($userArr, 'id');
            array_forget($userArr, 'username');
            array_forget($userArr, 'email');
            array_forget($userArr, 'email_confirmed');
            array_forget($userArr, 'infoshare');
            array_forget($userArr, 'is_staff');
            array_forget($userArr, 'is_superuser');
            array_forget($userArr, 'last_login');
            array_forget($userArr, 'phone');
            array_forget($userArr, 'phone_confirmed');
            array_forget($userArr, 'phone_code');


            if(!empty($user->player)){
                $userArr['id'] = $user->player->id;
            }else{
                $userArr['id'] = 'asd';
            }

            if(!empty($user->image)){
                $userArr['image_url'] = env('MEDIA_END').$user->image;
            }else{
                $userArr['image_url'] = env('MEDIA_END').'default-player.png';
            }

            $data['results'][] = $userArr;
        }
        
        $data['count'] = $users->count();
        $data['next'] = null;
        $data['previous'] = null;

        return response()->json($data, 200);
    }

    public function players($id)
    {
        $data = array();

        $player = Player::find($id);

        $data = $player->toArray();

        if(!empty($player->players_position)){
            $data['position'] = $player->players_position->toArray();
        }else{
            $data['position'] = null;
        }

        if(!empty($player->players_position2)){
            $data['position2'] = $player->players_position2->toArray();
        }else{
            $data['position2'] = null;
        }

        if(!empty($player->players_position3)){
            $data['position3'] = $player->players_position3->toArray();
        }else{
            $data['position3'] = null;
        }

        $data['value'] = (int)$player->value;
        $data['old_system_value'] = (int)$player->old_system_value;
        
        $data['total_match'] = 0; //CHECK
        $data['total_goal'] = 0; //CHECK
        $data['total_rate'] = 0; //CHECK
        $data['five_match_rate'] = 0; //CHECK

        if(!empty($player->team)){
            $data['team'] = $player->team->toArray();
            
            if (!empty($player->team->image)) {
                $data['team']['image_url'] = env('MEDIA_END').$player->team->image;
            }else{
                $data['team']['image_url'] = env('MEDIA_END').'default-team.png';
            }

            $dLeague = $player->team->league;
            $dSeason = $dLeague->seasonApi->first();

            $data['team']['league'] = $dLeague->toArray();
            $data['team']['league']['province'] = $dLeague->league_province->first()->province->toArray();

            $data['team']['league']['season'] = $dSeason->toArray();
            $data['team']['league']['season']['fullname'] = $dSeason->year.' '.$dLeague->name.' '.$dSeason->name;

            if($dSeason->polymorphic_ctype_id == 32){
                $data['team']['league']['season']['type'] = 'elimination';
            }else if($dSeason->polymorphic_ctype_id == 33){
                $data['team']['league']['season']['type'] = 'fixture';
            }else if($dSeason->polymorphic_ctype_id == 34){
                $data['team']['league']['season']['type'] = 'point';
            }
        }else{
            $data['team'] = null;
        }

        $data['user'] = $player->user->toArray();

        array_forget($data, 'user.username');
        array_forget($data, 'user.email');
        array_forget($data, 'user.email_confirmed');
        array_forget($data, 'user.infoshare');
        array_forget($data, 'user.is_staff');
        array_forget($data, 'user.is_superuser');
        array_forget($data, 'user.last_login');
        array_forget($data, 'user.phone');
        array_forget($data, 'user.phone_confirmed');
        array_forget($data, 'user.phone_code');


        $data['user']['fullname'] = $player->user->first_name.' '.$player->user->last_name;
        $data['user']['age'] = Carbon::parse($player->user->profile->birth_date)->age;
        
        if(!empty($player->user->image)){
            $data['user']['image_url'] = env('MEDIA_END').$player->user->image;
        }else{
            $data['user']['image_url'] = env('MEDIA_END').'default-player.png';
        }

        $data['user']['profile'] = $player->user->profile->toArray();

        array_forget($data, 'user.profile.identity_number');

        $data['user']['profile']['province'] = (!empty($player->user->profile->province)) ? $player->user->profile->province->toArray() : null;
        $data['user']['profile']['district'] = (!empty($player->user->profile->district)) ? $player->user->profile->district->toArray() : null;

        $data['history_total'] = array('match' => TeamPlayerTeamHistory::where('player_id', $id)->sum('match'), 'goal' => TeamPlayerTeamHistory::where('player_id', $id)->sum('goal'));

        return response()->json($data, 200);
    }

    public function plaSeason($id)
    {
        $data = array();
        $player = Player::find($id);

        $seasonArr = array();

        foreach ($player->team_playerteamhistory as $teamhistory) {

            $seArr = $teamhistory->season->toArray();
            $seArr['fullname'] = $teamhistory->season->year.' '.$teamhistory->season->league->name.' '.$teamhistory->season->name;

            $seasonArr[] = $seArr;
        }

        $data = $seasonArr;

        return response()->json($data, 200);
    }

    public function plaLastRatings($id, Request $request)
    {
        $data = array();
        $limit = $request->input('limit');
        $player = Player::find($id);

        $match_players = $player->match_player;

        $avg_rate = 0;
        $cntlimit = 0;
        foreach ($match_players as $match_player) {
            $match_completed = $match_player->match->completed;
            if($match_player->match->completed){
                
                $cntlimit++;
                
                $rate = array();
                $rate['date'] = $match_player->match->date;
                $rate['rating'] = $match_player->rating;
                $rate['team'] = $match_player->team->name;

                $data['ratings'][] = $rate;

                $avg_rate += $match_player->rating;

                if($cntlimit == $limit){
                    break;
                }
            }
        }
        //$data['avg'] = number_format(($avg_rate / $cntlimit), 1, '.', '');
        if($cntlimit > 0){
            $data['avg'] = $avg_rate / $cntlimit;
        }else{
            $data['avg'] = 0;
        }

        return response()->json($data, 200);
    }

    public function plaGoals($id, Request $request)
    {
        $data = array();
        $data['results'] = array();
        $results = array();

        $page = null;
        $pageCount = 12;
        $pageStart = 0;

        if(!empty($request->input('page'))){
            $page = $request->input('page');
            $pageStart = ($page - 1) * $pageCount;
        }

        $player = Player::find($id);
        
        foreach ($player->match_player as $mplayer) {
            
            foreach ($mplayer->match_action_goal->where('embed_code', '!=', null) as $maction) {

                //return response()->json($maction, 200);
                $maArr = $maction->toArray();

                if ($maction->embed_source == 'app'){
                    $maArr['embed_image_url'] = url('default.jpg');
                }else if ($maction->embed_source == 'vimeo'){
                    $maArr['embed_image_url'] = url('default.jpg');
                }else if ($maction->embed_source == 'youtube'){
                    $maArr['embed_image_url'] = 'https://i3.ytimg.com/vi/'.$maction->embed_code.'/hqdefault.jpg';
                }

                $maArr['embed_url'] = null;
                if ($maction->embed_source == 'app'){
                    $maArr['embed_url'] = $maction->embed_code;
                }else if ($maction->embed_source == 'vimeo'){
                    $maArr['embed_url'] = 'https://player.vimeo.com/video/'.$maction->embed_code;
                }else if ($maction->embed_source == 'youtube'){
                    $maArr['embed_url'] = 'https://www.youtube.com/embed/'.$maction->embed_code;
                }
                
                $maArr['match'] = $maction->match->toArray();
                $maArr['match']['video_status'] = (!empty($maction->match->match_video)) ? $maction->match->match_video->status : 'recordable';

                if(!empty($maction->match->coordinator)){
                    $maArr['match']['coordinator'] = $maction->match->coordinator->toArray();
                    $maArr['match']['coordinator']['fullname'] = $maction->match->coordinator->user->first_name.' '.$maction->match->coordinator->user->last_name;
                    
                    if(!empty($maction->match->coordinator->user->image)){
                        $maArr['match']['coordinator']['image_url'] = env('MEDIA_END').$maction->match->coordinator->user->image;
                    }else{
                        $maArr['match']['coordinator']['image_url'] = env('MEDIA_END').'default-player.png';
                    }
                }else{
                    $maArr['match']['coordinator'] = null;
                }

                if(!empty($maction->match->referee)){
                    $maArr['match']['referee'] = $maction->match->referee->toArray();
                    $maArr['match']['referee']['fullname'] = $maction->match->referee->user->first_name.' '.$maction->match->referee->user->last_name;
                    
                    if(!empty($maction->match->referee->user->image)){
                        $maArr['match']['referee']['image_url'] = env('MEDIA_END').$maction->match->referee->user->image;
                    }else{
                        $maArr['match']['referee']['image_url'] = env('MEDIA_END').'default-player.png';
                    }
                }else{
                    $maArr['match']['referee'] = null;
                }

                if(!empty($maction->match->ground)){
                    $maArr['match']['ground'] = $maction->match->ground->toArray();
                }else{
                    $maArr['match']['ground'] = null;
                }

                $maArr['match']['league'] = $maction->match->season->league->toArray();
            
                if(empty($maction->match->team1)){
                    $maArr['match']['team1']['id'] = 0;
                    $maArr['match']['team1']['name'] = 'Takım';
                    $maArr['match']['team1']['image_url'] = env('MEDIA_END').'default-team.png';
                    $maArr['match']['team1']['league'] = $maction->match->season->league->toArray();
                }else{
                    $maArr['match']['team1'] = $maction->match->team1->toArray();
                    if (!empty($maction->match->team1->image)) {
                        $maArr['match']['team1']['image_url'] = env('MEDIA_END').$maction->match->team1->image;
                    }else{
                        $maArr['match']['team1']['image_url'] = env('MEDIA_END').'default-team.png';
                    }
                    $maArr['match']['team1']['league'] = $maction->match->team1->league->toArray();
                    //$maArr['match']['team1']['league']['province'] = $maction->match->team1->league->league_province->first()->province->toArray();
                    $maArr['match']['team1']['league']['season'] = $maction->match->season->toArray();

                    if($maction->match->season->polymorphic_ctype_id == 32){
                        $maArr['match']['team1']['league']['season']['type'] = 'elimination';
                    }else if($maction->match->season->polymorphic_ctype_id == 33){
                        $maArr['match']['team1']['league']['season']['type'] = 'fixture';
                    }else if($maction->match->season->polymorphic_ctype_id == 34){
                        $maArr['match']['team1']['league']['season']['type'] = 'point';
                    }
                }

                if(empty($maction->match->team2)){
                    $maArr['match']['team2']['id'] = 0;
                    $maArr['match']['team2']['name'] = 'Takım';
                    $maArr['match']['team2']['image_url'] = env('MEDIA_END').'default-team.png';
                    $maArr['match']['team2']['league'] = $maction->match->season->league->toArray();
                }else{
                    $maArr['match']['team2'] = $maction->match->team2->toArray();
                    if (!empty($maction->match->team2->image)) {
                        $maArr['match']['team2']['image_url'] = env('MEDIA_END').$maction->match->team2->image;
                    }else{
                        $maArr['match']['team2']['image_url'] = env('MEDIA_END').'default-team.png';
                    }
                    $maArr['match']['team2']['league'] = $maction->match->team2->league->toArray();
                    //$maArr['match']['team2']['league']['province'] = $maction->match->team2->league->league_province->first()->province->toArray();
                    $maArr['match']['team2']['league']['season'] = $maction->match->season->toArray();

                    if($maction->match->season->polymorphic_ctype_id == 32){
                        $maArr['match']['team2']['league']['season']['type'] = 'elimination';
                    }else if($maction->match->season->polymorphic_ctype_id == 33){
                        $maArr['match']['team2']['league']['season']['type'] = 'fixture';
                    }else if($maction->match->season->polymorphic_ctype_id == 34){
                        $maArr['match']['team2']['league']['season']['type'] = 'point';
                    }
                }

                if(!empty($maArr['seconds'])){
                    $value = $maArr['minute'];
                    $maArr['minute'] = Carbon::createFromTime(00, 00, 00)->addSeconds($value)->format('i:s');
                }
                
                //if($maArr['match']['video_status'] == 'finished'){
                    $results[] = $maArr;
                //}

            }
            
        }

        $data['count'] = count($results);

        $data['next'] = null;
        if(!empty($page) && (count($results) / $pageCount) > $page){
            $data['next'] = env('MEDIA_END').'players/-id-/goals/?page='.($page+1);
        }

        $data['previous'] = null;
        if(!empty($page) && $page > 1){
            $data['previous'] = env('MEDIA_END').'players/-id-/goals/?page='.($page-1);
        }

        $data['results'] = array_values(collect($results)->slice($pageStart, $pageCount)->toArray());

        return response()->json($data, 200);
    }

    public function plaCompetition($id, Request $request)
    {
        $data = array();

        $player = Player::find($id);
        $type = $request->input('type');
        $season = $player->team_playerteamhistory->first()->season;

        if($type == 'goal') {
            $season_tpth_datas = collect($season->team_playerteamhistory_goal);
        } else if($type == 'yellow_card') {
            $season_tpth_datas = collect($season->team_playerteamhistory_yellow_card);
        } else if($type == 'red_card') {
            $season_tpth_datas = collect($season->team_playerteamhistory_red_card);
        } else if($type == 'save') {
            $season_tpth_datas = collect($season->team_playerteamhistory_save);
        }

        $oncesonrasayisi = 4;
        $index = 0;
        foreach ($season_tpth_datas as $season_tpth_data) {

            if($season_tpth_data->player_id == $id){
                break;
            }
            $index++;
        }


        $baslangicsayisi = 0;
        if($index-$oncesonrasayisi < 0){
            $baslangicsayisi = 0;
        }else{
            $baslangicsayisi = $index-$oncesonrasayisi;
        }


        foreach ($season_tpth_datas->slice($baslangicsayisi, ($oncesonrasayisi*2)+1) as $stpth_data) {

            $stpth_dataArr = $stpth_data->toArray();

            $stpth_dataArr['player'] = $stpth_data->player->toArray();
            $stpth_dataArr['player']['fullname'] = $stpth_data->player->user->first_name.' '.$stpth_data->player->user->last_name;

            if(!empty($stpth_data->player->user->image)){
                $stpth_dataArr['player']['image_url'] = env('MEDIA_END').$stpth_data->player->user->image;
            }else{
                $stpth_dataArr['player']['image_url'] = env('MEDIA_END').'default-player.png';
            }

            $stpth_dataArr['player']['position'] = (!empty($stpth_data->player->players_position)) ? $stpth_data->player->players_position->toArray() : null;
            $stpth_dataArr['player']['position2'] = (!empty($stpth_data->player->players_position2)) ? $stpth_data->player->players_position2->toArray() : null;
            $stpth_dataArr['player']['position3'] = (!empty($stpth_data->player->players_position3)) ? $stpth_data->player->players_position3->toArray() : null;

            $stpth_dataArr['player']['value'] = (int)$stpth_data->player->value;
            $stpth_dataArr['player']['old_system_value'] = (int)$stpth_data->player->old_system_value;

            $stpth_dataArr['team'] = $stpth_data->team->toArray();
            $stpth_dataArr['season'] = $stpth_data->season->toArray();

            $data[] = $stpth_dataArr;

        }

        return response()->json($data, 200);
    }

    public function plaPerformanceHistory($id)
    {
        $data = array();

        $player = Player::find($id);
        foreach ($player->team_playerteamhistory as $phistory) {


            if($phistory->season->polymorphic_ctype_id == 33 || $phistory->season->polymorphic_ctype_id == 34){

                $phArr = $phistory->toArray();

                if(!empty($phistory->season->league_elimination_baseseason)){
                    $e_season_id = $phistory->season->league_elimination_baseseason->season_ptr_id;
                    $es_phistory = TeamPlayerTeamHistory::where('player_id', $id)->where('season_id', $e_season_id)->first();

                    if(!empty($es_phistory)){
                        $phArr['es_match'] = $es_phistory->match;
                        $phArr['es_goal'] = $es_phistory->goal;
                        $phArr['es_gk_save'] = $es_phistory->gk_save;
                        $phArr['es_yellow_card'] = $es_phistory->yellow_card;
                        $phArr['es_red_card'] = $es_phistory->red_card;
                    }else{
                        $phArr['es_match'] = 0;
                        $phArr['es_goal'] = 0;
                        $phArr['es_gk_save'] = 0;
                        $phArr['es_yellow_card'] = 0;
                        $phArr['es_red_card'] = 0;
                    }
                }else{
                    $phArr['es_match'] = 0;
                    $phArr['es_goal'] = 0;
                    $phArr['es_gk_save'] = 0;
                    $phArr['es_yellow_card'] = 0;
                    $phArr['es_red_card'] = 0;
                }


                $phArr['match'] = $phistory->match + $phArr['es_match'];
                $phArr['goal'] = $phistory->goal + $phArr['es_goal'];
                $phArr['gk_save'] = $phistory->gk_save + $phArr['es_gk_save'];
                $phArr['yellow_card'] = $phistory->yellow_card + $phArr['es_yellow_card'];
                $phArr['red_card'] = $phistory->red_card + $phArr['es_red_card'];


                $phArr['player'] = $phistory->player->toArray();
                $phArr['player']['fullname'] = $phistory->player->user->first_name.' '.$phistory->player->user->last_name;
                
                if(!empty($phistory->player->user->image)){
                    $phArr['player']['image_url'] = env('MEDIA_END').$phistory->player->user->image;
                }else{
                    $phArr['player']['image_url'] = env('MEDIA_END').'default-player.png';
                }

                $phArr['player']['position'] = (!empty($phistory->player->players_position)) ? $phistory->player->players_position->toArray() : null;
                $phArr['player']['position2'] = (!empty($phistory->player->players_position2)) ? $phistory->player->players_position2->toArray() : null;
                $phArr['player']['position3'] = (!empty($phistory->player->players_position3)) ? $phistory->player->players_position3->toArray() : null;

                $phArr['player']['value'] = (int)$phistory->player->value;
                $phArr['player']['old_system_value'] = (int)$phistory->player->old_system_value;

                $phArr['player']['team'] = $phistory->team->toArray();
                
                if (!empty($phistory->team->image)) {
                    $phArr['player']['team']['image_url'] = env('MEDIA_END').$phistory->team->image;
                }else{
                    $phArr['player']['team']['image_url'] = env('MEDIA_END').'default-team.png';
                }

                $phArr['player']['team']['league'] = $phistory->team->league->toArray();
                $phArr['player']['team']['league']['province'] = $phistory->team->league->league_province->first()->province->toArray();
                $phArr['player']['team']['league']['season'] = $phistory->season->toArray();

                if($phistory->season->polymorphic_ctype_id == 32){
                    $phArr['player']['team']['league']['season']['type'] = 'elimination';
                }else if($phistory->season->polymorphic_ctype_id == 33){
                    $phArr['player']['team']['league']['season']['type'] = 'fixture';
                }else if($phistory->season->polymorphic_ctype_id == 34){
                    $phArr['player']['team']['league']['season']['type'] = 'point';
                }

                $phArr['team'] = $phistory->team->toArray();
                
                if (!empty($phistory->team->image)) {
                    $phArr['team']['image_url'] = env('MEDIA_END').$phistory->team->image;
                }else{
                    $phArr['team']['image_url'] = env('MEDIA_END').'default-team.png';
                }

                $phArr['team']['league'] = $phistory->team->league->toArray();
                $phArr['team']['league']['province'] = $phistory->team->league->league_province->first()->province->toArray();
                $phArr['team']['league']['season'] = $phistory->season->toArray();

                if($phistory->season->polymorphic_ctype_id == 32){
                    $phArr['team']['league']['season']['type'] = 'elimination';
                }else if($phistory->season->polymorphic_ctype_id == 33){
                    $phArr['team']['league']['season']['type'] = 'fixture';
                }else if($phistory->season->polymorphic_ctype_id == 34){
                    $phArr['team']['league']['season']['type'] = 'point';
                }

                $phArr['season'] = $phistory->season->toArray();
                $phArr['season']['fullname'] = $phistory->season->year.' '.$phistory->season->league->name.' '.$phistory->season->name;
                if($phistory->season->polymorphic_ctype_id == 32){
                    $phArr['season_type'] = 'elimination';
                }else if($phistory->season->polymorphic_ctype_id == 33){
                    $phArr['season_type'] = 'fixture';
                }else if($phistory->season->polymorphic_ctype_id == 34){
                    $phArr['season_type'] = 'point';
                }

                $results[] = $phArr;

            }
            
        }

        if(!empty($results)){
            $data = collect($results);
        }else{
            $data = array();
        }

        return response()->json($data, 200);
    }

    public function plaRating($id, Request $request)
    {

        $player = Player::find($id);
        $seasonId = $request->input('seasonId');

        $playerteamhistorys = $player->team_playerteamhistory->where('season_id', $seasonId);
        //return response()->json($playerteamhistorys, 200);
        $data = array();
        $data['match_count'] = 0;
        $data['goal_count'] = 0;
        $data['red_card'] = 0;
        $data['yellow_card'] = 0;
        $data['rating_avg'] = 0;
        $data['saving_avg'] = 0;

        $data['goal_per_match'] = 0;
        $data['card_per_match'] = 0;
        
        foreach ($playerteamhistorys as $playerteamhistory) {
            if($playerteamhistory->match > 0) {
                $data['match_count'] += (int)$playerteamhistory->match;
                $data['goal_count'] += (int)$playerteamhistory->goal;
                $data['red_card'] += (int)$playerteamhistory->red_card;
                $data['yellow_card'] += (int)$playerteamhistory->yellow_card;
                $data['rating_avg'] += (float)$playerteamhistory->rating * (int)$playerteamhistory->match;
                $data['saving_avg'] += (float)$playerteamhistory->gk_save;
            }
        }

        if($data['match_count'] > 0){
            $data['rating_avg'] = $data['rating_avg'] / $data['match_count'];
            $data['saving_avg'] = $data['saving_avg'] / $data['match_count'];
            $data['goal_per_match'] = ((float)( $data['goal_count'] / $data['match_count'] ));
            $data['card_per_match'] = number_format(((float)( $data['red_card'] + $data['yellow_card'] ) / $data['match_count']), 2, '.', ',');
        }else{
            $data['rating_avg'] = 0;
            $data['saving_avg'] = 0;
            $data['goal_per_match'] = 0;
            $data['card_per_match'] = 0;
        }
   
        return response()->json($data, 200);
    }

    public function plaMatches($id, Request $request)
    {
        $data = array();
        $data['results'] = array();
        $results = array();

        $page = null;
        $pageCount = 12;
        $pageStart = 0;

        if(!empty($request->input('page'))){
            $page = $request->input('page');
            $pageStart = ($page - 1) * $pageCount;
        }

        $player = Player::find($id);
        
        foreach ($player->match_player as $mplayer) {

            $matchArr = $mplayer->match->toArray();
            $matchArr['p_team'] = $mplayer->team_id;
            $matchArr['video_status'] = (!empty($mplayer->match->match_video)) ? $mplayer->match->match_video->status : 'recordable';

            if(empty($mplayer->match->team1)){
                $matchArr['team1']['id'] = 0;
                $matchArr['team1']['name'] = 'Takım';
                $matchArr['team1']['image_url'] = env('MEDIA_END').'default-team.png';
                $matchArr['team1']['league'] = $mplayer->match->season->league->toArray();
            }else{
                $matchArr['team1'] = $mplayer->match->team1->toArray();
                
                if (!empty($mplayer->match->team1->image)) {
                    $matchArr['team1']['image_url'] = env('MEDIA_END').$mplayer->match->team1->image;
                }else{
                    $matchArr['team1']['image_url'] = env('MEDIA_END').'default-team.png';
                }
                $matchArr['team1']['league'] = $mplayer->match->season->league->toArray();
                //$matchArr['team1']['league']['province'] = $mplayer->match->season->league->league_province->first()->province->toArray();
                $matchArr['team1']['league']['season'] = $mplayer->match->season->toArray();
            }

            if(empty($mplayer->match->team2)){
                $matchArr['team2']['id'] = 0;
                $matchArr['team2']['name'] = 'Takım';
                $matchArr['team2']['image_url'] = env('MEDIA_END').'default-team.png';
                $matchArr['team2']['league'] = $mplayer->match->season->league->toArray();
            }else{
                $matchArr['team2'] = $mplayer->match->team2->toArray();
                
                if (!empty($mplayer->match->team2->image)) {
                    $matchArr['team2']['image_url'] = env('MEDIA_END').$mplayer->match->team2->image;
                }else{
                    $matchArr['team2']['image_url'] = env('MEDIA_END').'default-team.png';
                }
                $matchArr['team2']['league'] = $mplayer->match->season->league->toArray();
                //$matchArr['team2']['league']['province'] = $mplayer->match->season->league->league_province->first()->province->toArray();
                $matchArr['team2']['league']['season'] = $mplayer->match->season->toArray();
            }

            if(!empty($mplayer->match->coordinator)){
                $matchArr['coordinator'] = $mplayer->match->coordinator->toArray();
                $matchArr['coordinator']['fullname'] = $mplayer->match->coordinator->user->first_name.' '.$mplayer->match->coordinator->user->last_name;
                
                if(!empty($mplayer->match->coordinator->user->image)){
                    $matchArr['coordinator']['image_url'] = env('MEDIA_END').$mplayer->match->coordinator->user->image;
                }else{
                    $matchArr['coordinator']['image_url'] = env('MEDIA_END').'default-player.png';
                }
            }else{
                $matchArr['coordinator'] = null;
            }

            if(!empty($mplayer->match->referee)){
                $matchArr['referee'] = $mplayer->match->referee->toArray();
                $matchArr['referee']['fullname'] = $mplayer->match->referee->user->first_name.' '.$mplayer->match->referee->user->last_name;
                
                if(!empty($mplayer->match->referee->user->image)){
                    $matchArr['referee']['image_url'] = env('MEDIA_END').$mplayer->match->referee->user->image;
                }else{
                    $matchArr['referee']['image_url'] = env('MEDIA_END').'default-player.png';
                }
            }else{
                $matchArr['referee'] = null;
            }

            if(!empty($mplayer->match->ground)){
                $matchArr['ground'] = $mplayer->match->ground->toArray();
            }else{
                $matchArr['ground'] = null;
            }
            
            $matchArr['league']['id'] = $mplayer->match->season->league->id;
            $matchArr['league']['name'] = $mplayer->match->season->league->name;

            $results[] = $matchArr;
            
        }
        
        $data['count'] = count($results);

        $data['next'] = null;
        if(!empty($page) && (count($results) / $pageCount) > $page){
            $data['next'] = env('MEDIA_END').'players/-id-/matches/?page='.($page+1);
        }

        $data['previous'] = null;
        if(!empty($page) && $page > 1){
            $data['previous'] = env('MEDIA_END').'players/-id-/matches/?page='.($page-1);
        }
        
        $data['results'] = array_values(collect($results)->slice($pageStart, $pageCount)->toArray());
    
        return response()->json($data, 200);
    }

    public function plaPressConferences($id, Request $request)
    {

        $data = array();
        $data['results'] = array();
        $results = array();

        $page = null;
        $pageCount = 12;
        $pageStart = 0;

        if(!empty($request->input('page'))){
            $page = $request->input('page');
            $pageStart = ($page - 1) * $pageCount;
        }

        $player = Player::find($id);
        
        foreach ($player->match_player as $mplayer) {
            
            $press_conferences_player = $mplayer->press_conferences_player;

            if(!empty($press_conferences_player)){

                $press_conference = $press_conferences_player->press_conference;

                $pcArr = $press_conference->toArray();
                
                if ($press_conference->embed_source == 'app'){
                    $pcArr['embed_image_url'] = url('default.jpg');
                }else if ($press_conference->embed_source == 'vimeo'){
                    $pcArr['embed_image_url'] = url('default.jpg');
                }else if ($press_conference->embed_source == 'youtube'){
                    $pcArr['embed_image_url'] = 'https://i3.ytimg.com/vi/'.$press_conference->embed_code.'/hqdefault.jpg';
                }

                if ($press_conference->embed_source == 'app'){
                    $pcArr['embed_url'] = $press_conference->embed_code;
                }else if ($press_conference->embed_source == 'vimeo'){
                    $pcArr['embed_url'] = 'https://player.vimeo.com/video/'.$press_conference->embed_code;
                }else if ($press_conference->embed_source == 'youtube'){
                    $pcArr['embed_url'] = 'https://www.youtube.com/embed/'.$press_conference->embed_code;
                }


                $matchArr = $press_conference->match->toArray();
                $matchArr['video_status'] = (!empty($press_conference->match->match_video)) ? $press_conference->match->match_video->status : 'recordable';

                if(empty($press_conference->match->team1)){
                    $matchArr['team1']['id'] = 0;
                    $matchArr['team1']['name'] = 'Takım';
                    $matchArr['team1']['image_url'] = env('MEDIA_END').'default-team.png';
                    $matchArr['team1']['league'] = $press_conference->match->season->league->toArray();
                }else{
                    $matchArr['team1'] = $press_conference->match->team1->toArray();
                    
                    if (!empty($press_conference->match->team1->image)) {
                        $matchArr['team1']['image_url'] = env('MEDIA_END').$press_conference->match->team1->image;
                    }else{
                        $matchArr['team1']['image_url'] = env('MEDIA_END').'default-team.png';
                    }
                    $matchArr['team1']['league'] = $press_conference->match->season->league->toArray();
                    //$matchArr['team1']['league']['province'] = $press_conference->match->season->league->league_province->first()->province->toArray();
                    $matchArr['team1']['league']['season'] = $press_conference->match->season->toArray();
                }


                if(empty($press_conference->match->team2)){
                    $matchArr['team2']['id'] = 0;
                    $matchArr['team2']['name'] = 'Takım';
                    $matchArr['team2']['image_url'] = env('MEDIA_END').'default-team.png';
                    $matchArr['team2']['league'] = $press_conference->match->season->league->toArray();
                }else{
                    $matchArr['team2'] = $press_conference->match->team2->toArray();
                    
                    if (!empty($press_conference->match->team2->image)) {
                        $matchArr['team2']['image_url'] = env('MEDIA_END').$press_conference->match->team2->image;
                    }else{
                        $matchArr['team2']['image_url'] = env('MEDIA_END').'default-team.png';
                    }
                    $matchArr['team2']['league'] = $press_conference->match->season->league->toArray();
                    //$matchArr['team2']['league']['province'] = $press_conference->match->season->league->league_province->first()->province->toArray();
                    $matchArr['team2']['league']['season'] = $press_conference->match->season->toArray();
                }

                if(!empty($press_conference->match->coordinator)){
                    $matchArr['coordinator'] = $press_conference->match->coordinator->toArray();
                    $matchArr['coordinator']['fullname'] = $press_conference->match->coordinator->user->first_name.' '.$press_conference->match->coordinator->user->last_name;
                    
                    if(!empty($press_conference->match->coordinator->user->image)){
                        $matchArr['coordinator']['image_url'] = env('MEDIA_END').$press_conference->match->coordinator->user->image;
                    }else{
                        $matchArr['coordinator']['image_url'] = env('MEDIA_END').'default-player.png';
                    }
                }else{
                    $matchArr['coordinator'] = null;
                }

                if(!empty($press_conference->match->referee)){
                    $matchArr['referee'] = $press_conference->match->referee->toArray();
                    $matchArr['referee']['fullname'] = $press_conference->match->referee->user->first_name.' '.$press_conference->match->referee->user->last_name;
                    
                    if(!empty($press_conference->match->referee->user->image)){
                        $matchArr['referee']['image_url'] = env('MEDIA_END').$press_conference->match->referee->user->image;
                    }else{
                        $matchArr['referee']['image_url'] = env('MEDIA_END').'default-player.png';
                    }
                }else{
                    $matchArr['referee'] = null;
                }

                if(!empty($press_conference->match->ground)){
                    $matchArr['ground'] = $press_conference->match->ground->toArray();
                }else{
                    $matchArr['ground'] = null;
                }
                $matchArr['league']['id'] = $press_conference->match->season->league->id;
                $matchArr['league']['name'] = $press_conference->match->season->league->name;


                $pcArr['match'] = $matchArr;

                $results[] = $pcArr;
            }

        }

        $data['count'] = count($results);

        $data['next'] = null;
        if(!empty($page) && (count($results) / $pageCount) > $page){
            $data['next'] = env('MEDIA_END').'players/-id-/press_conferences/?page='.($page+1);
        }

        $data['previous'] = null;
        if(!empty($page) && $page > 1){
            $data['previous'] = env('MEDIA_END').'players/-id-/press_conferences/?page='.($page-1);
        }

        $data['results'] = array_values(collect($results)->slice($pageStart, $pageCount)->toArray());
        //$data['results'] = $results;

        return response()->json($data, 200);
    }

    public function plaPlayervideo($id)
    {

        $player = Player::find($id);

        if(!empty($player)){
            return response()->json(['embed_url' => $player->embed_code], 200);
        }else{
            return response()->json(['embed_url' => ''], 200);
        }
    }

    public function plaPhotoSave($id, Request $request)
    {
        $data = array();

        $conf = array('width' => 200, 'height' => 200, 'aspectRatio' => 0, 'widen' => true, 'heighten' => true, 'crop' => true);
        $photo = $request->file('file');
        
        $fileName = str_slug(str_before($photo->getClientOriginalName(), '.'), '_').'_'.str_random(5).'.'.$photo->getClientOriginalExtension();
        $imageObj = Image::make($photo);

        if ($conf['widen']) {
            $imageObj->widen($conf['width'], function ($constraint) {
                $constraint->upsize();
            });
        }

        $ext = $photo->getClientOriginalExtension();
        if($ext == "png"){
            $imageObj->save(env('MEDIA_PATH').'user-images/'.$fileName, 93);
        }else{
            $imageObj->save(env('MEDIA_PATH').'user-images/'.$fileName, 93, 'jpg');
        }

        $player = Player::find($id);

        $user = $player->user;
        $user->image = 'user-images/'.$fileName;
        $user->save();

        $data['location'] = $user->image;
        $data['player_id'] = $id;
        $data['status'] = 'success';
        
        return response()->json($data, 200);
    }

    public function sosyalligtrigger(Request $request)
    {
        $user = User::find(116086);






        /*
        //$token = "xVngD8p8GFdVeEQtd2N4amtWC6POoFn5T65GXgCFmG9khBhByScdqOikMys3etw6";
        $campaignCode = "SOSYALLIG3MTEST";
        $referenceCode = "SL74456386352578";

        $postdata["CampaignCode"] = $campaignCode;
        $postdata["ReferenceCode"] = $referenceCode;

        $query = "https://api.test.sosyallig.com/api/programs/campaign";

        $headers = [
            'Content-type: application/json',
            'token: xVngD8p8GFdVeEQtd2N4amtWC6POoFn5T65GXgCFmG9khBhByScdqOikMys3etw6',
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $query);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postdata));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $server_output = curl_exec($ch);

        curl_close($ch);

        dd($server_output);
        */
    }

    /********* OYUNCU ***********/
    /****************************/
    
    public function groTables()
    {

        $data = array();
        $province_id = Auth::user()->profile->province_id;
        $team_id = Auth::user()->player->team_id;

        $grounds = Ground::select('id', 'name', 'slug')->where('province_id', $province_id)->orderBy('name', 'asc')->get();

        foreach ($grounds as $ground) {

            $g['ground'] = $ground->toArray();
            $g['dates'] = array();

            $noww = Carbon::now();

            for ($i=0; $i < 7; $i++) {
            
                $gdate = array();
                $gdate['hours'] = array();
                $gdate['date'] = $noww->copy()->addDays($i)->format('Y-m-d');

                $dayDiff = $noww->copy()->diffInDays($noww->copy()->startOfWeek());
                $diff = ($i + $dayDiff) % 7;

                $ghours = $ground->reservation_groundtable->where('date', $gdate['date'])->where('active', true);
                
                foreach ($ghours as $ghour) {

                    $_hour['id'] = $ghour->id;
                    $_hour['hour'] = $ghour->time;
                    $_hour['status'] = $ghour->status;
                    $_hour['is_can_be_reserved'] = true;

                    $_hour['matches'] = array();
                    $_hour_matches = Match::where('ground_id', $ground->id)->where('date', $gdate['date'])->where('time', $ghour->time)->get();

                    foreach ($_hour_matches as $match) {
                        $_hmarr = $match;

                        if(!empty($match->team1)){
                            $_hmarr['team1_name'] = $match->team1->name;
                            if (!empty($match->team1->image)) {
                                $_hmarr['team1_image_url'] = env('MEDIA_END').$match->team1->image;
                            }else{
                                $_hmarr['team1_image_url'] = env('MEDIA_END').'default-team.png';
                            }
                        }

                        if(!empty($match->team2)){
                            $_hmarr['team2_name'] = $match->team2->name;
                            if (!empty($match->team2->image)) {
                                $_hmarr['team2_image_url'] = env('MEDIA_END').$match->team2->image;
                            }else{
                                $_hmarr['team2_image_url'] = env('MEDIA_END').'default-team.png';
                            }
                        }

                        $_hour['matches'][] = $_hmarr;
                    }
                    
                    //$_hour['matches'] = Match::where('date', $gdate['date'])->where('time', $ghour->time)->get();
                    //$_hour['matches'][] = $gdate['date'];
                    //$_hour['matches'][] = $ghour->time;

                    $_hour['reservations'] = array();

                    foreach ($ghour->reservation as $reser) {

                        if($reser->match_id == null){
                            $_reser['id'] = $reser->id;
                            $_reser['match'] = $reser->match_id;

                            $_reser['offers'] = array();
                            $_reser['confirmed_offer'] = $reser->reservation_offer->where('status', 'confirmed')->first();

                            if(!empty($reser->reservation_offer->where('status', 'confirmed')->first())){
                                if (!empty($_reser['confirmed_offer']['team']['image'])) {
                                    $_reser['confirmed_offer']['team']['image_url'] = env('MEDIA_END').$reser->reservation_offer->team->image;
                                }else{
                                    $_reser['confirmed_offer']['team']['image_url'] = env('MEDIA_END').'default-team.png';
                                }
                            }

                            $_reser['status'] = $reser->status;

                            if(is_null($reser->match_id)){
                                $_reser['is_can_be_cancelled'] = true;
                                $_reser['is_can_be_offered'] = true;
                            }else{
                                $_reser['is_can_be_cancelled'] = false;
                                $_reser['is_can_be_offered'] = true;
                            }

                            $dTeamSeason = $reser->team_teamseason->toArray();
                            $dTeam = $reser->team_teamseason->team->toArray();
                            $dSeason = $reser->team_teamseason->season->toArray();
                            $dLeague = $reser->team_teamseason->season->league->toArray();
                            $dProvince = $reser->team_teamseason->season->league->league_province->first()->province->toArray();

                            $dOffers = $reser->reservation_offer->toArray();

                            $_reser['teamseason'] = $dTeamSeason;
                            $_reser['teamseason']['season'] = $dSeason;
                            $_reser['teamseason']['season']['fullname'] = $dSeason['year'].' '.$dLeague['name'].' '.$dSeason['name'];

                            if($dSeason['polymorphic_ctype_id'] == 32){
                                $_reser['teamseason']['season']['type'] = 'elimination';
                            }else if($dSeason['polymorphic_ctype_id'] == 33){
                                $_reser['teamseason']['season']['type'] = 'fixture';
                            }else if($dSeason['polymorphic_ctype_id'] == 34){
                                $_reser['teamseason']['season']['type'] = 'point';
                            }

                            $_reser['teamseason']['team'] = $dTeam;
                            
                            if (!empty($reser->team_teamseason->team->image)) {
                                $_reser['teamseason']['team']['image_url'] = env('MEDIA_END').$reser->team_teamseason->team->image;
                            }else{
                                $_reser['teamseason']['team']['image_url'] = env('MEDIA_END').'default-team.png';
                            }

                            $_reser['teamseason']['team']['league'] = $dLeague;
                            $_reser['teamseason']['team']['league']['province'] = $dProvince;
                            $_reser['teamseason']['team']['league']['season'] = $dSeason;
                            $_reser['teamseason']['team']['league']['season']['fullname'] = $dSeason['year'].' '.$dLeague['name'].' '.$dSeason['name'];


                            if($dSeason['polymorphic_ctype_id'] == 32){
                                $_reser['teamseason']['team']['league']['season']['type'] = 'elimination';
                            }else if($dSeason['polymorphic_ctype_id'] == 33){
                                $_reser['teamseason']['team']['league']['season']['type'] = 'fixture';
                            }else if($dSeason['polymorphic_ctype_id'] == 34){
                                $_reser['teamseason']['team']['league']['season']['type'] = 'point';
                            }
                            
                            foreach ($reser->reservation_offer as $resOffer) {

                                $rLeague = $resOffer->team->league->toArray();
                                $rSeason = $resOffer->team->league->seasonApi->first()->toArray();
                                $rProvince = $resOffer->team->league->league_province->first()->province->toArray();

                                $_resOffer = $resOffer->toArray();
                                $_resOffer['is_can_be_cancelled'] = true; // düzenlenecek
                                
                                $_resOffer['team'] = $resOffer->team->toArray();
                                
                                if (!empty($resOffer->team->image)) {
                                    $_resOffer['team']['image_url'] = env('MEDIA_END').$resOffer->team->image;
                                }else{
                                    $_resOffer['team']['image_url'] = env('MEDIA_END').'default-team.png';
                                }

                                $_resOffer['team']['league'] = $rLeague;
                                $_resOffer['team']['league']['province'] = $rProvince;

                                $_resOffer['team']['league']['season'] = $rSeason;
                                $_resOffer['team']['league']['season']['fullname'] = $rSeason['year'].' '.$rLeague['name'].' '.$rSeason['name'];

                                if($rSeason['polymorphic_ctype_id'] == 32){
                                    $_resOffer['team']['league']['season']['type'] = 'elimination';
                                }else if($rSeason['polymorphic_ctype_id'] == 33){
                                    $_resOffer['team']['league']['season']['type'] = 'fixture';
                                }else if($rSeason['polymorphic_ctype_id'] == 34){
                                    $_resOffer['team']['league']['season']['type'] = 'point';
                                }
                                
                                if($team_id == $resOffer->team_id){
                                    $_hour['is_can_be_reserved'] = true;
                                }else{
                                    $_hour['is_can_be_reserved'] = true;
                                }
                                
                                $_reser['offers'][] = $_resOffer;
                            }

                            if($team_id == $dTeam['id']){
                                $_hour['is_can_be_reserved'] = false;
                            } 

                            $_hour['reservations'][] = $_reser;
                        }
                    }
                    $gdate['hours'][] = $_hour;
                    
                }
                //return response()->json($gdate, 200);
                $ghoursSubs = $ground->reservation_groundsubscription->where('day', $diff)->where('active', true);
                
                foreach ($ghoursSubs as $gsubhour) {
                    $filtered = collect($gdate['hours'])->values()->where('hour', $gsubhour->time)->count();
                    if($filtered == 0){
                        //$_hourSub['hhh'] = $filtered; //$gdate['hours'];
                        $_hourSub['id'] = null; 
                        $_hourSub['hour'] = $gsubhour->time;
                        $_hourSub['status'] = 'empty';
                        $_hourSub['is_can_be_reserved'] = true;

                        $_hourSub['matches'] = array();
                        $_hour_sub_matches = Match::where('ground_id', $ground->id)->where('date', $gdate['date'])->where('time', $gsubhour->time)->get();

                        foreach ($_hour_sub_matches as $match) {
                            $_hmarr = $match;

                            if(!empty($match->team1)){
                                $_hmarr['team1_name'] = $match->team1->name;
                                if (!empty($match->team1->image)) {
                                    $_hmarr['team1_image_url'] = env('MEDIA_END').$match->team1->image;
                                }else{
                                    $_hmarr['team1_image_url'] = env('MEDIA_END').'default-team.png';
                                }
                            }

                            if(!empty($match->team2)){
                                $_hmarr['team2_name'] = $match->team2->name;
                                if (!empty($match->team2->image)) {
                                    $_hmarr['team2_image_url'] = env('MEDIA_END').$match->team2->image;
                                }else{
                                    $_hmarr['team2_image_url'] = env('MEDIA_END').'default-team.png';
                                }
                            }

                            $_hourSub['matches'][] = $_hmarr;
                        }

                        //$_hourSub['matches'][] = $gdate['date'];
                        //$_hourSub['matches'][] = $gsubhour->time;

                        $_hourSub['reservations'] = array();
                        $gdate['hours'][] = $_hourSub;
                    }
                }

                if(!empty($gdate['hours'])){
                    //$gdate['hours'] = collect($gdate['hours'])->sortBy('hour')->values()->all();
                    $ground_hours = ReservationGroundtime::orderBy('order')->get();
                    $gdate_reorder = array();
                    
                    foreach ($ground_hours as $ground_hour) {
                         if(collect($gdate['hours'])->where('hour', $ground_hour->time)->values()->count() > 0){
                            $gdate_reorder[] = collect($gdate['hours'])->where('hour', $ground_hour->time)->values()->first();
                         }
                    }

                    $gdate['hours'] = $gdate_reorder;
                    $g['dates'][] = $gdate;
                }

            }

            if(count($g['dates']) > 0){
                $data[] = $g;
            }

        }

        return response()->json($data, 200);
    }

    public function reservations(Request $request)
    {
        $dateCheck = null;
        if(!empty($request->input('ground_table'))){
            $reservationGroundtable = ReservationGroundtable::find($request->input('ground_table'));
            $dateCheck = Carbon::parse($reservationGroundtable->date)->format('Y-m-d');
        }elseif(!empty($request->input('ground'))){
            $dateCheck = Carbon::parse($request->input('subscripton_date'))->format('Y-m-d');
        }

        $team = Auth::user()->player->team;
        $league = Auth::user()->player->team->league;
        $season = Season::where('league_id', $league->id)->where('start_date', '<=', $dateCheck)->where('end_date', '>=', $dateCheck)->where('polymorphic_ctype_id', '34')->first();

        if(!empty($season)){
            $teamSeason = TeamTeamSeason::where('season_id', $season->id)->where('team_id', $team->id)->first();
        }else{
            $teamSeason = null;
            return response()->json(array('message' => 'Bu tarihte aktif bir sezon bulunmuyor. Başka bir tarihe rezeryon yapmayı deneyiniz.'), 400);
        }


        if(!empty($request->input('ground_table'))){

            $reservationGroundtable = ReservationGroundtable::find($request->input('ground_table'));

            $reservation = new Reservation();
            $reservation->ground_table_id = $reservationGroundtable->id;
            $reservation->captain_id = Auth::user()->player->id;
            $reservation->status = 'open';

            if(!empty($teamSeason)){
                $reservation->teamseason_id = $teamSeason->id;
            }else{

                $teamSeason = new TeamTeamSeason();
                $teamSeason->squad_locked = false;
                $teamSeason->point = $season->league_pointseason->min_team_point;
                $teamSeason->season_id = $season->id;
                $teamSeason->team_id = $team->id;
                $teamSeason->remaining_transfer_count = $season->allowed_transfer_count;
                $teamSeason->playoff_member = true;
                $teamSeason->drawn = 0;
                $teamSeason->goal_against = 0;
                $teamSeason->goal_for = 0;
                $teamSeason->lost = 0;
                $teamSeason->match_total = 0;
                $teamSeason->won = 0;
                $teamSeason->gk_save = 0;
                $teamSeason->red_card = 0;
                $teamSeason->yellow_card = 0;
                $teamSeason->save();

                $reservation->teamseason_id = $teamSeason->id;
            }
            
            $reservation->save();

            return response()->json($reservation, 200);

        }elseif(!empty($request->input('ground'))){
            
            $reservationGroundtable = ReservationGroundtable::where('ground_id', $request->input('ground'))->
                where('date', Carbon::parse($request->input('subscripton_date'))->format('Y-m-d'))->
                where('time', $request->input('subscripton_time'))->first();

            if(empty($reservationGroundtable)){
                $reservationGroundtable = new ReservationGroundtable();
                $reservationGroundtable->ground_id = $request->input('ground');
                $reservationGroundtable->date = Carbon::parse($request->input('subscripton_date'))->format('Y-m-d');
                $reservationGroundtable->time = $request->input('subscripton_time');
                $reservationGroundtable->status = 'empty';
                $reservationGroundtable->active = true;
                $reservationGroundtable->save();
            }else{
                $reservationGroundtable->active = true;
                $reservationGroundtable->save();
            }

            $reservation = new Reservation();
            $reservation->ground_table_id = $reservationGroundtable->id;
            $reservation->captain_id = Auth::user()->player->id;
            $reservation->status = 'open';

            if(!empty($teamSeason)){
                $reservation->teamseason_id = $teamSeason->id;
            }else{

                $teamSeason = new TeamTeamSeason();
                $teamSeason->squad_locked = false;
                $teamSeason->point = $season->league_pointseason->min_team_point;
                $teamSeason->season_id = $season->id;
                $teamSeason->team_id = $team->id;
                $teamSeason->remaining_transfer_count = $season->allowed_transfer_count;
                $teamSeason->playoff_member = true;
                $teamSeason->drawn = 0;
                $teamSeason->goal_against = 0;
                $teamSeason->goal_for = 0;
                $teamSeason->lost = 0;
                $teamSeason->match_total = 0;
                $teamSeason->won = 0;
                $teamSeason->gk_save = 0;
                $teamSeason->red_card = 0;
                $teamSeason->yellow_card = 0;
                $teamSeason->save();

                $reservation->teamseason_id = $teamSeason->id;
            }
            
            $reservation->save();

            return response()->json($reservationGroundtable, 200);

        }
    }

    public function reservationOffers(Request $request)
    {

        $reservationOffer = new ReservationOffer();
        $reservationOffer->status = 'wait';
        $reservationOffer->player_id = Auth::user()->player->id;
        $reservationOffer->reservation_id = $request->input('reservation');
        $reservationOffer->team_id = $team_id = Auth::user()->player->team_id;
        $reservationOffer->save();


        $_rOffer = array();
        $_rOffer['id'] = $reservationOffer->id;
        $_rOffer['player'] = $reservationOffer->player_id;
        $_rOffer['reservation'] = $reservationOffer->reservation_id;
        $_rOffer['team'] = $reservationOffer->team_id;

        return response()->json($_rOffer, 201);
    }

    public function reservationsDelete($rid)
    {

        if(!empty($rid)){
            $reservation = Reservation::find($rid);
            $reservation->delete();

            return response('', 204);
        }
    }

    public function reservationOffersDelete($oid)
    {

        if(!empty($oid)){
            $reservationOffer = ReservationOffer::find($oid);
            $reservationOffer->delete();

            return response('', 204);
        }
    }

    public function leaSeaGalleryBefore($id, $sid, Request $request)
    {
        $data = array();
        $season = Season::find($sid);
        $season_image = $season->season_image_before;
        foreach ($season_image as $image) {
            $image->image = env('MEDIA_END').$image->data;
            $data[] = $image;
        }
        return response()->json($data, 200);
    }

    public function leaSeaGalleryNow($id, $sid, Request $request)
    {
        $data = array();
        $season = Season::find($sid);
        $season_image = $season->season_image_now;
        foreach ($season_image as $image) {
            $image->image = env('MEDIA_END').$image->data;
            $data[] = $image;
        }
        return response()->json($data, 200);
    }

    public function leaSeaGalleryAfter($id, $sid, Request $request)
    {
        $data = array();
        $season = Season::find($sid);
        $season_image = $season->season_image_after;
        foreach ($season_image as $image) {
            $image->image = env('MEDIA_END').$image->data;
            $data[] = $image;
        }
        return response()->json($data, 200);
    }

    public function leaSeaGetDataInfo($id, $sid, Request $request)
    {

        $data = array();
        
        $league = League::find($id);
        $season = Season::find($sid);

        if(!empty($season->season_info_slide_photo)){
            $data['info_slide_photo'] = env('MEDIA_END').$season->season_info_slide_photo->data;
        }

        if(!empty($season->season_info_text)){
            $data['info_text'] = $season->season_info_text->data;
        }

        if(!empty($season->season_info_img1)){
            $data['info_img1'] = env('MEDIA_END').$season->season_info_img1->data;
        }

        if(!empty($season->season_info_img2)){
            $data['info_img2'] = env('MEDIA_END').$season->season_info_img2->data;
        }

        return response()->json($data, 200);
    }

    public function leaSeaGetDataBefore($id, $sid, Request $request)
    {

        $data = array();
        
        $league = League::find($id);
        $season = Season::find($sid);

        if(!empty($season->season_now_teaser)){
            $data['now_teaser'] = $season->season_now_teaser->data;
        }

        if(!empty($season->season_image_before_slide)){
            $data['before_slide_photo'] = env('MEDIA_END').$season->season_image_before_slide->data;
        }

        if(!empty($season->season_before_teaser_text)){
            $data['before_teaser_text'] = $season->season_before_teaser_text->data;
        }

        if(!empty($season->season_before_teaser)){
            $data['before_teaser'] = $season->season_before_teaser->data;
            if(!empty($season->season_before_teaser_img)){
                $data['before_teaser_img'] = env('MEDIA_END').$season->season_before_teaser_img->data;
            }
        }

        if(!empty($season->season_before_playlist)){
            $data['before_playlist'] = $season->season_before_playlist->data;
        }

        if(!empty($season->season_temsilci_team)){
            if(!empty($season->season_temsilci_team->data)){

                $_temp_temsilci_team = Team::find($season->season_temsilci_team->data);

                $temsilci_team['name'] = $_temp_temsilci_team->name;
                if(!empty($_temp_temsilci_team->image)){
                    $temsilci_team['imageUrl'] = env('MEDIA_END').$_temp_temsilci_team->image;
                }else{
                    $temsilci_team['imageUrl'] = env('MEDIA_END').'default-team.png';
                }

                $data['temsilciteam'] = $temsilci_team;

                $_temp_player = array();
                foreach ($_temp_temsilci_team->player as $_player) {
                    $_temp_pl['name'] = $_player->user->first_name .' '.$_player->user->last_name;

                    if(!empty($_player->user->image)){
                        $_temp_pl['imageUrl'] = env('MEDIA_END').$_player->user->image;
                    }else{
                        $_temp_pl['imageUrl'] = env('MEDIA_END').'default-player.png';
                    }

                    $_temp_player[] = $_temp_pl;
                }

                $data['temsilciteam']['players'] = $_temp_player;

            }else{
                $data['temsilciteam'] = array();
            }
            
        }

        $_katilimci_teams = null;
        if(!empty($season->season_katilimci_teams->data)){
            $teams_ids = json_decode($season->season_katilimci_teams->data);
            $_katilimci_teams = Team::whereIn('id', $teams_ids)->orderBy('name', 'asc')->get();

            foreach ($_katilimci_teams as $kteam) {
                $_temp_katilimci_team = array();
                $_temp_katilimci_team['name'] = $kteam->name;
                if(!empty($kteam->image)){
                    $_temp_katilimci_team['imageUrl'] = env('MEDIA_END').$kteam->image;
                }else{
                    $_temp_katilimci_team['imageUrl'] = env('MEDIA_END').'default-team.png';
                }
                
                $data['teams'][] = $_temp_katilimci_team;
            }
        }

        $championships_seasons = Season::whereIn('id', $league->season->pluck('id'))->where('end_date', '<', $season->end_date)->where('active', true)->orderBy('end_date' ,'desc')->get();
        $i = 0;
        foreach ($championships_seasons as $championships_season) {
            if(!empty($championships_season->championships)){

                $_temp_championships = array();
                $_temp_championships['name'] = $championships_season->championships->year.' Türkiye Şampiyonu';
                $_temp_championships['team'] = $championships_season->championships->team->name.' ('.$championships_season->championships->team->province->name.')';
                $_temp_championships['logo'] = env('MEDIA_END').$championships_season->championships->team->image;

                $data['championships'][] = $_temp_championships;

                if(count($data['championships']) == 4){
                    break;
                }
            }
        }



        $birOncekiSeasonControl = false;
        $birOncekiSeason = null;
        foreach ($league->seasonApi as $s) {
            if($birOncekiSeasonControl){
                $birOncekiSeason = $s;
                break;
            }
            if($s->id == $season->id){
                $birOncekiSeasonControl = true;
            }
        }

        $data['before_season_id'] = $birOncekiSeason->id;

        /*
        $data['panorama'] = array();
        $player_mvp = SeasonBeforeAfter::where('type', 'player_mvp')->where('season_id', $birOncekiSeason->id)->where('status', 'after')->first();
        if(!empty($player_mvp->data)){
            $_temp_player = array();
            $_player = Player::find($player_mvp->data);
            $_temp_player['player']['name'] = $_player->user->first_name .' '.$_player->user->last_name;

            if(!empty($_player->user->image)){
                $_temp_player['player']['imageUrl'] = env('MEDIA_END').$_player->user->image;
            }else{
                $_temp_player['player']['imageUrl'] = env('MEDIA_END').'default-player.png';
            }
            
            $_temp_player['team']['name'] = $_player->team->name;
            $_temp_player['p'] = 'Gol Kralı ve MVP';

            $data['panorama'][] = $_temp_player;
        }

        $player_beststriker = SeasonBeforeAfter::where('type', 'player_beststriker')->where('season_id', $birOncekiSeason->id)->where('status', 'after')->first();
        if(!empty($player_beststriker->data)){
            $_temp_player = array();
            $_player = Player::find($player_beststriker->data);
            $_temp_player['player']['name'] = $_player->user->first_name .' '.$_player->user->last_name;

            if(!empty($_player->user->image)){
                $_temp_player['player']['imageUrl'] = env('MEDIA_END').$_player->user->image;
            }else{
                $_temp_player['player']['imageUrl'] = env('MEDIA_END').'default-player.png';
            }
            
            $_temp_player['team']['name'] = $_player->team->name;
            $_temp_player['p'] = 'En İyi Forvet';

            $data['panorama'][] = $_temp_player;
        }

        $_player = SeasonBeforeAfter::where('type', 'player_bestmiddle')->where('season_id', $birOncekiSeason->id)->where('status', 'after')->first();
        if(!empty($_player->data)){
            $_temp_player = array();
            $_player = Player::find($_player->data);
            $_temp_player['player']['name'] = $_player->user->first_name .' '.$_player->user->last_name;

            if(!empty($_player->user->image)){
                $_temp_player['player']['imageUrl'] = env('MEDIA_END').$_player->user->image;
            }else{
                $_temp_player['player']['imageUrl'] = env('MEDIA_END').'default-player.png';
            }
            
            $_temp_player['team']['name'] = $_player->team->name;
            $_temp_player['p'] = 'En İyi Ortasaha';

            $data['panorama'][] = $_temp_player;
        }

        $player_bestdefance = SeasonBeforeAfter::where('type', 'player_bestdefance')->where('season_id', $birOncekiSeason->id)->where('status', 'after')->first();
        if(!empty($player_bestdefance->data)){
            $_temp_player = array();
            $_player = Player::find($player_bestdefance->data);
            $_temp_player['player']['name'] = $_player->user->first_name .' '.$_player->user->last_name;

            if(!empty($_player->user->image)){
                $_temp_player['player']['imageUrl'] = env('MEDIA_END').$_player->user->image;
            }else{
                $_temp_player['player']['imageUrl'] = env('MEDIA_END').'default-player.png';
            }
            
            $_temp_player['team']['name'] = $_player->team->name;
            $_temp_player['p'] = 'En İyi Defans';

            $data['panorama'][] = $_temp_player;
        }

        $player_bestgoalkeeper = SeasonBeforeAfter::where('type', 'player_bestgoalkeeper')->where('season_id', $birOncekiSeason->id)->where('status', 'after')->first();
        if(!empty($player_bestgoalkeeper->data)){
            $_temp_player = array();
            $_player = Player::find($player_bestgoalkeeper->data);
            $_temp_player['player']['name'] = $_player->user->first_name .' '.$_player->user->last_name;

            if(!empty($_player->user->image)){
                $_temp_player['player']['imageUrl'] = env('MEDIA_END').$_player->user->image;
            }else{
                $_temp_player['player']['imageUrl'] = env('MEDIA_END').'default-player.png';
            }
            
            $_temp_player['team']['name'] = $_player->team->name;
            $_temp_player['p'] = 'En İyi Kaleci';

            $data['panorama'][] = $_temp_player;
        }
        */


        $data['panorama'] = array();

        $player_mvp = SeasonBeforeAfter::where('type', 'player_mvp')->where('season_id', $birOncekiSeason->id)->where('status', 'after')->first();
        if(!empty($player_mvp->data)){
            $_temp_player = array();
            $_player = Player::find($player_mvp->data);
            $_temp_player['player']['name'] = $_player->user->first_name .' '.$_player->user->last_name;

            if(!empty($_player->user->image)){
                $_temp_player['player']['imageUrl'] = env('MEDIA_END').$_player->user->image;
            }else{
                $_temp_player['player']['imageUrl'] = env('MEDIA_END').'default-player.png';
            }
            
            if(!empty($_player->team)){
                $_temp_player['team']['name'] = $_player->team->name;
            }else{
                $_temp_player['team']['name'] = '';
            }
            $_temp_player['p'] = 'MVP';

            $data['panorama'][] = $_temp_player;
        }

        $player_kog = SeasonBeforeAfter::where('type', 'player_kog')->where('season_id', $birOncekiSeason->id)->where('status', 'after')->first();
        if(!empty($player_kog->data)){
            $_temp_player = array();
            $_player = Player::find($player_kog->data);
            $_temp_player['player']['name'] = $_player->user->first_name .' '.$_player->user->last_name;

            if(!empty($_player->user->image)){
                $_temp_player['player']['imageUrl'] = env('MEDIA_END').$_player->user->image;
            }else{
                $_temp_player['player']['imageUrl'] = env('MEDIA_END').'default-player.png';
            }
            
            if(!empty($_player->team)){
                $_temp_player['team']['name'] = $_player->team->name;
            }else{
                $_temp_player['team']['name'] = '';
            }
            $_temp_player['p'] = 'Gol Kralı';

            $data['panorama'][] = $_temp_player;
        }

        $player_beststriker = SeasonBeforeAfter::where('type', 'player_beststriker')->where('season_id', $birOncekiSeason->id)->where('status', 'after')->first();
        if(!empty($player_beststriker->data)){
            $_temp_player = array();
            $_player = Player::find($player_beststriker->data);
            $_temp_player['player']['name'] = $_player->user->first_name .' '.$_player->user->last_name;

            if(!empty($_player->user->image)){
                $_temp_player['player']['imageUrl'] = env('MEDIA_END').$_player->user->image;
            }else{
                $_temp_player['player']['imageUrl'] = env('MEDIA_END').'default-player.png';
            }
            
            if(!empty($_player->team)){
                $_temp_player['team']['name'] = $_player->team->name;
            }else{
                $_temp_player['team']['name'] = '';
            }
            $_temp_player['p'] = 'En İyi Forvet';

            $data['panorama'][] = $_temp_player;
        }

        $_player = SeasonBeforeAfter::where('type', 'player_bestmiddle')->where('season_id', $birOncekiSeason->id)->where('status', 'after')->first();
        if(!empty($_player->data)){
            $_temp_player = array();
            $_player = Player::find($_player->data);
            $_temp_player['player']['name'] = $_player->user->first_name .' '.$_player->user->last_name;

            if(!empty($_player->user->image)){
                $_temp_player['player']['imageUrl'] = env('MEDIA_END').$_player->user->image;
            }else{
                $_temp_player['player']['imageUrl'] = env('MEDIA_END').'default-player.png';
            }
            
            if(!empty($_player->team)){
                $_temp_player['team']['name'] = $_player->team->name;
            }else{
                $_temp_player['team']['name'] = '';
            }
            $_temp_player['p'] = 'En İyi Ortasaha';

            $data['panorama'][] = $_temp_player;
        }

        $player_bestdefance = SeasonBeforeAfter::where('type', 'player_bestdefance')->where('season_id', $birOncekiSeason->id)->where('status', 'after')->first();
        if(!empty($player_bestdefance->data)){
            $_temp_player = array();
            $_player = Player::find($player_bestdefance->data);
            $_temp_player['player']['name'] = $_player->user->first_name .' '.$_player->user->last_name;

            if(!empty($_player->user->image)){
                $_temp_player['player']['imageUrl'] = env('MEDIA_END').$_player->user->image;
            }else{
                $_temp_player['player']['imageUrl'] = env('MEDIA_END').'default-player.png';
            }
            
            if(!empty($_player->team)){
                $_temp_player['team']['name'] = $_player->team->name;
            }else{
                $_temp_player['team']['name'] = '';
            }
            $_temp_player['p'] = 'En İyi Defans';

            $data['panorama'][] = $_temp_player;
        }

        $player_bestgoalkeeper = SeasonBeforeAfter::where('type', 'player_bestgoalkeeper')->where('season_id', $birOncekiSeason->id)->where('status', 'after')->first();
        if(!empty($player_bestgoalkeeper->data)){
            $_temp_player = array();
            $_player = Player::find($player_bestgoalkeeper->data);
            $_temp_player['player']['name'] = $_player->user->first_name .' '.$_player->user->last_name;

            if(!empty($_player->user->image)){
                $_temp_player['player']['imageUrl'] = env('MEDIA_END').$_player->user->image;
            }else{
                $_temp_player['player']['imageUrl'] = env('MEDIA_END').'default-player.png';
            }
            
            if(!empty($_player->team)){
                $_temp_player['team']['name'] = $_player->team->name;
            }else{
                $_temp_player['team']['name'] = '';
            }
            $_temp_player['p'] = 'En İyi Kaleci';

            $data['panorama'][] = $_temp_player;
        }


        return response()->json($data, 200);
    }

    public function leaSeaGetDataAfter($id, $sid, Request $request)
    {
        $data = array();
        
        $league = League::find($id);
        $season = Season::find($sid);

        $eTree = EliminationTree::where('season_id', $season->league_eliminationseasonApi->season_ptr_id)->where('type', 'trfinal')->first();

        if(!empty($eTree)){
            $data['trfinal_treeid'] = $eTree->id;
        }else{
            $data['trfinal_treeid'] = null;
        }

        if(!empty($season->season_image_after_slide)){
            $data['slide_photo'] = env('MEDIA_END').$season->season_image_after_slide->data;
        }

        if(!empty($season->season_after_teaser_text)){
            $data['after_teaser_text'] = $season->season_after_teaser_text->data;
        }

        if(!empty($season->season_after_teaser)){
            $data['after_teaser'] = $season->season_after_teaser->data;
            if(!empty($season->season_after_teaser_img)){
                $data['after_teaser_img'] = env('MEDIA_END').$season->season_after_teaser_img->data;
            }
        }

        $season = Season::find($sid);

        $_katilimci_teams = null;
        if(!empty($season->season_katilimci_teams->data)){
            $teams_ids = json_decode($season->season_katilimci_teams->data);
            $_katilimci_teams = Team::whereIn('id', $teams_ids)->orderBy('name', 'asc')->get();

            foreach ($_katilimci_teams as $kteam) {
                $_temp_katilimci_team = array();
                $_temp_katilimci_team['name'] = $kteam->name;
                if(!empty($kteam->image)){
                    $_temp_katilimci_team['imageUrl'] = env('MEDIA_END').$kteam->image;
                }else{
                    $_temp_katilimci_team['imageUrl'] = env('MEDIA_END').'default-team.png';
                }
                
                $data['teams'][] = $_temp_katilimci_team;
            }
        }
        
        if(!empty($season->championships->team->name)){
            $_temp_kunye = array();
            $_temp_kunye['place'] = 1;
            $_temp_kunye['name'] = 'Şampiyon';
            $_temp_kunye['team'] = $season->championships->team->name;
            if(!empty($season->championships->team->image)){
                $_temp_kunye['logo'] = env('MEDIA_END').$season->championships->team->image;
            }else{
                $_temp_kunye['logo'] = env('MEDIA_END').'default-team.png';
            }
            $data['kunye'][] = $_temp_kunye;
        }

        $season_team2 = SeasonBeforeAfter::where('type', 'team2_id')->where('season_id', $sid)->where('status', 'after')->first();
        if(!empty($season_team2->data)){
            $team2 = Team::find($season_team2->data);
            $_temp_kunye = array();
            $_temp_kunye['place'] = 2;
            $_temp_kunye['name'] = 'İkinci';
            $_temp_kunye['team'] = $team2->name;
            if(!empty($team2->image)){
                $_temp_kunye['logo'] = env('MEDIA_END').$team2->image;
            }else{
                $_temp_kunye['logo'] = env('MEDIA_END').'default-team.png';
            }
            $data['kunye'][] = $_temp_kunye;
        }

        $season_team3 = SeasonBeforeAfter::where('type', 'team3_id')->where('season_id', $sid)->where('status', 'after')->first();
        if(!empty($season_team3->data)){
            $team3 = Team::find($season_team3->data);
            $_temp_kunye = array();
            $_temp_kunye['place'] = 3;
            $_temp_kunye['name'] = 'Üçüncü';
            $_temp_kunye['team'] = $team3->name;
            if(!empty($team3->image)){
                $_temp_kunye['logo'] = env('MEDIA_END').$team3->image;
            }else{
                $_temp_kunye['logo'] = env('MEDIA_END').'default-team.png';
            }
            $data['kunye'][] = $_temp_kunye;
        }

        $season_team4 = SeasonBeforeAfter::where('type', 'team4_id')->where('season_id', $sid)->where('status', 'after')->first();
        if(!empty($season_team4->data)){
            $team4 = Team::find($season_team4->data);
            $_temp_kunye = array();
            $_temp_kunye['place'] = 4;
            $_temp_kunye['name'] = 'Üçüncü';
            $_temp_kunye['team'] = $team4->name;
            if(!empty($team4->image)){
                $_temp_kunye['logo'] = env('MEDIA_END').$team4->image;
            }else{
                $_temp_kunye['logo'] = env('MEDIA_END').'default-team.png';
            }
            $data['kunye'][] = $_temp_kunye;
        }


        $data['panorama'] = array();

        $player_mvp = SeasonBeforeAfter::where('type', 'player_mvp')->where('season_id', $sid)->where('status', 'after')->first();
        if(!empty($player_mvp->data)){
            $_temp_player = array();
            $_player = Player::find($player_mvp->data);
            $_temp_player['player']['name'] = $_player->user->first_name .' '.$_player->user->last_name;

            if(!empty($_player->user->image)){
                $_temp_player['player']['imageUrl'] = env('MEDIA_END').$_player->user->image;
            }else{
                $_temp_player['player']['imageUrl'] = env('MEDIA_END').'default-player.png';
            }
            
            if(!empty($_player->team)){
                $_temp_player['team']['name'] = $_player->team->name;
            }else{
                $_temp_player['team']['name'] = "";
            }
            $_temp_player['p'] = 'MVP';

            $data['panorama'][] = $_temp_player;
        }

        $player_kog = SeasonBeforeAfter::where('type', 'player_kog')->where('season_id', $sid)->where('status', 'after')->first();
        if(!empty($player_kog->data)){
            $_temp_player = array();
            $_player = Player::find($player_kog->data);
            $_temp_player['player']['name'] = $_player->user->first_name .' '.$_player->user->last_name;

            if(!empty($_player->user->image)){
                $_temp_player['player']['imageUrl'] = env('MEDIA_END').$_player->user->image;
            }else{
                $_temp_player['player']['imageUrl'] = env('MEDIA_END').'default-player.png';
            }
            if(!empty($_player->team)){
                $_temp_player['team']['name'] = $_player->team->name;
            }else{
                $_temp_player['team']['name'] = "";
            }
            $_temp_player['p'] = 'Gol Kralı';

            $data['panorama'][] = $_temp_player;
        }

        $player_beststriker = SeasonBeforeAfter::where('type', 'player_beststriker')->where('season_id', $sid)->where('status', 'after')->first();
        if(!empty($player_beststriker->data)){
            $_temp_player = array();
            $_player = Player::find($player_beststriker->data);
            $_temp_player['player']['name'] = $_player->user->first_name .' '.$_player->user->last_name;

            if(!empty($_player->user->image)){
                $_temp_player['player']['imageUrl'] = env('MEDIA_END').$_player->user->image;
            }else{
                $_temp_player['player']['imageUrl'] = env('MEDIA_END').'default-player.png';
            }
            
            if(!empty($_player->team)){
                $_temp_player['team']['name'] = $_player->team->name;
            }else{
                $_temp_player['team']['name'] = "";
            }
            $_temp_player['p'] = 'En İyi Forvet';

            $data['panorama'][] = $_temp_player;
        }

        $_player = SeasonBeforeAfter::where('type', 'player_bestmiddle')->where('season_id', $sid)->where('status', 'after')->first();
        if(!empty($_player->data)){
            $_temp_player = array();
            $_player = Player::find($_player->data);
            $_temp_player['player']['name'] = $_player->user->first_name .' '.$_player->user->last_name;

            if(!empty($_player->user->image)){
                $_temp_player['player']['imageUrl'] = env('MEDIA_END').$_player->user->image;
            }else{
                $_temp_player['player']['imageUrl'] = env('MEDIA_END').'default-player.png';
            }
            
            if(!empty($_player->team)){
                $_temp_player['team']['name'] = $_player->team->name;
            }else{
                $_temp_player['team']['name'] = "";
            }
            $_temp_player['p'] = 'En İyi Ortasaha';

            $data['panorama'][] = $_temp_player;
        }

        $player_bestdefance = SeasonBeforeAfter::where('type', 'player_bestdefance')->where('season_id', $sid)->where('status', 'after')->first();
        if(!empty($player_bestdefance->data)){
            $_temp_player = array();
            $_player = Player::find($player_bestdefance->data);
            $_temp_player['player']['name'] = $_player->user->first_name .' '.$_player->user->last_name;

            if(!empty($_player->user->image)){
                $_temp_player['player']['imageUrl'] = env('MEDIA_END').$_player->user->image;
            }else{
                $_temp_player['player']['imageUrl'] = env('MEDIA_END').'default-player.png';
            }
            
            if(!empty($_player->team)){
                $_temp_player['team']['name'] = $_player->team->name;
            }else{
                $_temp_player['team']['name'] = "";
            }
            $_temp_player['p'] = 'En İyi Defans';

            $data['panorama'][] = $_temp_player;
        }

        $player_bestgoalkeeper = SeasonBeforeAfter::where('type', 'player_bestgoalkeeper')->where('season_id', $sid)->where('status', 'after')->first();
        if(!empty($player_bestgoalkeeper->data)){
            $_temp_player = array();
            $_player = Player::find($player_bestgoalkeeper->data);
            $_temp_player['player']['name'] = $_player->user->first_name .' '.$_player->user->last_name;

            if(!empty($_player->user->image)){
                $_temp_player['player']['imageUrl'] = env('MEDIA_END').$_player->user->image;
            }else{
                $_temp_player['player']['imageUrl'] = env('MEDIA_END').'default-player.png';
            }
            
            if(!empty($_player->team)){
                $_temp_player['team']['name'] = $_player->team->name;
            }else{
                $_temp_player['team']['name'] = "";
            }
            $_temp_player['p'] = 'En İyi Kaleci';

            $data['panorama'][] = $_temp_player;
        }

        if(!empty($season->season_before_playlist)){
            $data['before_playlist'] = $season->season_before_playlist->data;
        }


        $championships_seasons = Season::whereIn('id', $league->season->pluck('id'))->where('end_date', '<', $season->end_date)->where('active', true)->orderBy('end_date' ,'desc')->get();
        $i = 0;
        foreach ($championships_seasons as $championships_season) {
            if(!empty($championships_season->championships)){

                $_temp_championships = array();
                $_temp_championships['name'] = $championships_season->championships->year.' Türkiye Şampiyonu';
                $_temp_championships['team'] = $championships_season->championships->team->name.' ('.$championships_season->championships->team->province->name.')';
                $_temp_championships['logo'] = env('MEDIA_END').$championships_season->championships->team->image;

                $data['championships'][] = $_temp_championships;

                if(count($data['championships']) == 7){
                    break;
                }
            }
        }

        return response()->json($data, 200);
    }

    public function getBanner($str, Request $request)
    {

        $data = array();
        $banner = Banner::where('slug', $str)->first();

        if(!empty($banner)){
            $data['imageUrl'] = env('MEDIA_END').$banner->image;
            $data['href'] = $banner->url;
            $data['alt'] = '';
        }else{
            $data['imageUrl'] = '';
            $data['href'] = '';
            $data['alt'] = ''; 
        }

        return response()->json($data, 200);
    }

    public function getRules($type, Request $request)
    {

        $types = array();
        $types['34'] = 1;
        $types['33'] = 2;
        $types['trfinal'] = 3;
        $types['bilyonercup'] = 4;

        $data = array();

        $rules = Rules::where('id', $types[$type])->orderBy('order')->limit(1)->get();

        return response()->json($rules, 200);
    }


}

