<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Validator;

use Carbon\Carbon;
use App\User;

use App\Season;


use App\Rules;


class RulesController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        return view('dashboard');
    }

    public function rules_crud($id = null) {
        $rules = NULL;
        if(!is_null($id)){
            $rules = Rules::find($id);
        }
        return view('dashboard', array('model_data' => $rules));
    }

    public function rules_save(Request $request) {

        //dd($request->input());
        $text = "";

        if ($request->crud == 'delete') {
            /*
            $model = Penalty::find($request->id);
            
            if($model->polymorphic_ctype_id == 44){
                $player_penalty = PlayerPenalty::find($model->id);
                $player_penalty->delete();
            }else if($model->polymorphic_ctype_id == 47){
                $team_penalty = TeamPenalty::find($model->id);
                $team_penalty->delete();
            }

            $model->delete();
            */
            $text = 'Başarıyla Silindi...';
        }else{

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $model = new Rules();
            }else if($request->crud == 'edit'){
                $model = Rules::find($request->id);
            }
            
            $rules = array();
            $formConfig = config('forms.'.$request->segment);

            if(!is_null($formConfig)){
                foreach ($formConfig as $key => $value){
                    if(!empty(data_get($value, 'validation'))){
                        $rules[$key] = data_get($value, 'validation');
                    }
                }
            }
            
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                //dd($validator->errors());
                return \Redirect::back()->withErrors($validator)->withInput();
            }

            $model->name = $request->name;
            $model->rule_text = $request->rule_text;
            $model->order = $request->order;
            $model->save();

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $text = 'Başarıyla Eklendi...';
            }else if($request->crud == 'edit'){
                $text = 'Başarıyla Güncellendi...';
            }
        }

        return redirect('rules')->with('message', array('text' => $text, 'status' => 'success'));
    }


}
