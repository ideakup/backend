<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Validator;

use Carbon\Carbon;
use App\User;

use App\Team;
use App\TeamSurvey;

use App\TeamTeamPlayer;
use App\TeamTeamSeason;
use App\TeamTeamSeasonAddPointLog;

use App\Player;
use App\Transfer;
use App\TransferSeason;

use App\League;
use App\Province;
use App\District;

class TeamController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard');
    }

    public function crud($id = null)
    {
        $team = NULL;
        if(!is_null($id)){
            $team = Team::find($id);
        }
        return view('dashboard', array('model_data' => $team));
    }

    public function save(Request $request)
    {
        
        $text = "";
        //dd($request->input());
        if ($request->crud == 'delete') {

            $model = Team::find($request->id);
            
            if(!empty($model->team_survey)){
                $model->team_survey->delete();
            }
            $model->delete();
            
            $text = 'Başarıyla Silindi...';
    
        }else{

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $model = new Team();
                $model->status = 'active';
            }else if($request->crud == 'edit'){
                $model = Team::find($request->id);
            }

            $rules = array();
            $formConfig = config('forms.'.$request->segment);

            if(!is_null($formConfig)){
                foreach ($formConfig as $key => $value){
                    if(!empty(data_get($value, 'validation'))){
                        $rules[$key] = data_get($value, 'validation');
                    }
                }
            }
            
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
               return \Redirect::back()->withErrors($validator)->withInput();
            }

            $league_id = null;
            if(!empty($request->province_id)){
                $province = Province::find($request->province_id);
                if(!empty($province->league_province)){
                    if(!empty($province->league_province->whereNotIn('league_id', [26])->first()->league)){
                        $league_id = $province->league_province->whereNotIn('league_id', [26])->first()->league->id;
                    }
                }
            }


            $model->name = $request->name;

            $model->tactic_id = $request->tactic_id;
            $model->province_id = $request->province_id;
            $model->district_id = $request->district_id;

            $model->league_id = $league_id;

            $model->captain_id = $request->captain_id;

            $model->facebook = $request->facebook;
            $model->twitter = $request->twitter;
            $model->instagram = $request->instagram;
            $model->save();

            if(!is_null($request->potential_id) || !is_null($request->satisfaction_id)) {

                if ($request->crud == 'add') {
                    $team_survey = new TeamSurvey();
                    $team_survey->team_id = $model->id;
                }else if($request->crud == 'edit'){
                    $team_survey = TeamSurvey::where('team_id', $model->id)->first();
                    if(is_null($team_survey)){
                        $team_survey = new TeamSurvey();
                        $team_survey->team_id = $model->id; 
                    }
                }

                if(is_null($request->potential_id)){
                    $team_survey->potential_id = 5;
                }else{
                    $team_survey->potential_id = $request->potential_id;
                }

                if(is_null($request->satisfaction_id)){
                    $team_survey->satisfaction_id = 13;
                }else{
                    $team_survey->satisfaction_id = $request->satisfaction_id;
                }

                if(is_null($request->satisfaction_jogo_id)){
                    $team_survey->satisfaction_jogo_id = 1;
                }else{
                    $team_survey->satisfaction_jogo_id = $request->satisfaction_jogo_id;
                }
                $team_survey->save();
            }

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $text = 'Başarıyla Eklendi...';
            }else if($request->crud == 'edit'){
                $text = 'Başarıyla Güncellendi...';
            }
        }

        return redirect($request->segment)->with('message', array('text' => $text, 'status' => 'success'));
    }

    public function setCaptain($id, $pid)
    {
        $team = Team::find($id);
        $team->captain_id = $pid;
        $team->save();
        
        $text = 'Başarıyla Kaptan Atandı...';
        return redirect('teams/edit/'.$id.'?t=tab_03')->with('message', array('text' => $text, 'status' => 'success'));
    }

    public function releasePlayer($id, $pid)
    {

        $team = Team::find($id);
        $player = Player::find($pid);

        if(!empty($player->team)){

            $last_team_player = TeamTeamPlayer::where('player_id', $player->id)->where('team_id', $team->id)->orderBy('created_at', 'desc')->first();
            if(!empty($last_team_player) && empty($last_team_player->released_date)){
                $last_team_player->released_date = Carbon::now();
                $last_team_player->save();
            }

        }

        $player->team_id = null;
        $player->save();

        if($team->captain_id == $pid && $team->player->count() > 0){

            $team->captain_id = $team->player->first()->id;
            $team->save();

        }

        $text = 'Oyuncu Başarıyla Serbest Bırakıldı...';
        return redirect('teams/edit/'.$id.'?t=tab_03')->with('message', array('text' => $text, 'status' => 'success'));

    }

    public function setPlayOff($id, $tsid)
    {

        $team = Team::find($id);
        $teamTeamSeason = TeamTeamSeason::find($tsid);

        $teamTeamSeason->playoff_member = !$teamTeamSeason->playoff_member;
        $teamTeamSeason->save();

        $text = 'Playoff Üyeliği Başarıyla Değiştirildi...';
        return redirect('teams/edit/'.$id.'?t=tab_02')->with('message', array('text' => $text, 'status' => 'success'));

    }

    public function setSquadLocked($id, $tsid)
    {
        $team = Team::find($id);
        $teamTeamSeason = TeamTeamSeason::find($tsid);

        if($teamTeamSeason->squad_locked){
            $teamTeamSeason->squad_locked = !$teamTeamSeason->squad_locked;
            $teamTeamSeason->squad_locked_date = null;
            $teamTeamSeason->squad_locked_player_id = null;
        }else{
            $teamTeamSeason->squad_locked = !$teamTeamSeason->squad_locked;
            $teamTeamSeason->squad_locked_date = Carbon::now();
            $teamTeamSeason->squad_locked_player_id = null;
        }
        
        $teamTeamSeason->save();
        
        $text = 'Kadro Kilit Durumu Başarıyla Değiştirildi...';
        return redirect('teams/edit/'.$id.'?t=tab_02')->with('message', array('text' => $text, 'status' => 'success'));

    }

    public function requestTermination(Request $request)
    {

        $team = Team::find($request->input('team_id'));
        //dump($request->input());

        if($request->input('operation') == 'passive' || $request->input('operation') == 'accept'){

            foreach ($team->player as $player) {
                $player->team_id = null;
                $player->save();
            }

            foreach ($team->match1->where('completed', false) as $match1) {
                if(!empty($match1->match_action)){
                    foreach ($match1->match_action as $match_action) {
                        $match_action->delete();
                    }
                }

                if(!empty($match1->match_player)){
                    foreach ($match1->match_player as $match_player) {

                        foreach($match_player->match_player_panorama as $match_player_panorama){
                            if(!empty($match_player_panorama)){
                                $match_player_panorama->delete();
                            }
                        }

                        if(!empty($match_player->press_conferences_player)){
                            $match_player->press_conferences_player->delete();
                        }
                        $match_player->delete();
                    }
                }

                if(!empty($match1->match_video)){
                    $match1->match_video->delete();
                }

                if(!empty($match1->match_image)){
                    foreach ($match1->match_image as $match_image) {
                        $match_image->delete();
                    }
                }

                if(!empty($match1->match_press)){
                    $match1->match_press->delete();
                }

                if(!empty($match1->reservation)){
                    
                    if(!empty($match1->reservation->reservation_offer)){
                        foreach ($match1->reservation->reservation_offer as $reservation_offer) {
                            $reservation_offer->delete();
                        }
                    }
                    $match1->reservation->delete();
                }

                $match1->delete();
            }

            foreach ($team->match2->where('completed', false) as $match2) {
                if(!empty($match2->match_action)){
                    foreach ($match2->match_action as $match_action) {
                        $match_action->delete();
                    }
                }

                if(!empty($match2->match_player)){
                    foreach ($match2->match_player as $match_player) {
                        if(!empty($match_player->press_conferences_player)){
                            $match_player->press_conferences_player->delete();
                        }

                        if(!empty($match_player->match_player_panorama)){
                            foreach ($match_player->match_player_panorama as $match_player_panorama) {
                                $match_player_panorama->delete();
                            }
                        }


                        $match_player->delete();
                    }
                }

                if(!empty($match2->match_video)){
                    $match2->match_video->delete();
                }

                if(!empty($match2->match_image)){
                    foreach ($match2->match_image as $match_image) {
                        $match_image->delete();
                    }
                }

                if(!empty($match2->match_press)){
                    $match2->match_press->delete();
                }

                if(!empty($match2->reservation)){
                    
                    if(!empty($match2->reservation->reservation_offer)){
                        foreach ($match2->reservation->reservation_offer as $reservation_offer) {
                            $reservation_offer->delete();
                        }
                    }
                    $match2->reservation->delete();
                }

                $match2->delete();
            }

            $team->status = 'passive';
            $team->captain_id = null;
            $text = 'Başarıyla Silindi...';

        }else if($request->input('operation') == 'cancel'){
            $team->status = 'active';
            $text = 'Fesih Talebi Başarıyla İptal Edildi...';
        }

        //dd($team);
        $team->save();

        return redirect('teams/edit/'.$request->input('team_id').'?t=tab_04')->with('message', array('text' => $text, 'status' => 'success'));

    }

    public function addinsideplayer($id)
    {
        
        $team = NULL;
        if(!is_null($id)){
            $team = Team::find($id);
        }
        return view('dashboard', array('model_data' => $team));

    }

    public function addinsideplayer_save($id, $pid)
    {

        $team = Team::find($id);
        $captain = $team->captain;
        $requested_player = Player::find($pid);

        /***** Aktif Sezon gg*****/
        $activeSeason = null;
        $activeSeasons = array();
        $existSeasons = array();
        $t_seasons = $team->league->seasonApi;


        foreach ($t_seasons as $t_season) {
            if($t_season->active == true){
                if($t_season->polymorphic_ctype_id != 32){
                    if($t_season->start_date <= Carbon::now()->format('Y-m-d') && $t_season->end_date >= Carbon::now()->format('Y-m-d')){
                        $activeSeasons[] = $t_season;
                    }
                    $existSeasons[] = $t_season;
                }
            }
        }

        if(count($activeSeasons) > 1){
            foreach ($activeSeasons as $season) {
                if($season->polymorphic_ctype_id == 33){

                    $fs_group_teams_id = array();
                    foreach ($season->league_fixtureseason->groups as $group) {
                        foreach ($group->group_teams as $group_team) {
                            $fs_group_teams_id[] = $group_team->team_id;
                        }
                    }
                    if(array_search($team->id, $fs_group_teams_id)){
                        $activeSeason = $season;
                    }

                }
            }
        }else{
            if(count($existSeasons)){
                $activeSeason = $existSeasons[0];
            }
        }

        if(empty($activeSeason)){
            $activeSeason = $activeSeasons[0];
        }elseif(!empty($activeSeason)){

            if(!empty($team->team_teamseason->where('season_id', $activeSeason->id)->first())){
                $remaining_transfer_count = $team->team_teamseason->where('season_id', $activeSeason->id)->first()->remaining_transfer_count;
                $squad_locked = $team->team_teamseason->where('season_id', $activeSeason->id)->first()->squad_locked;
            }else{
                $remaining_transfer_count = $activeSeason->allowed_transfer_count;
                $squad_locked = false;
            }
        }
        /***** Aktif Sezon gg*****/

        if(!empty($activeSeason)){
            $teamSeason = $team->team_teamseason->where('season_id', $activeSeason->id)->first();
            if(!empty($teamSeason)){
                if($teamSeason->squad_locked){
                    $teamSeason->remaining_transfer_count = $teamSeason->remaining_transfer_count - 1;
                    $teamSeason->save();
                }
            }else{
                $teamSeason = new TeamTeamSeason();
                $teamSeason->squad_locked = false;
                if($activeSeason->polymorphic_ctype_id == 33){
                    $teamSeason->point = 0;
                }else if($activeSeason->polymorphic_ctype_id == 34){
                    $teamSeason->point = $activeSeason->league_pointseason->min_team_point;
                }
                
                $teamSeason->season_id = $activeSeason->id;
                $teamSeason->team_id = $team->id;
                $teamSeason->remaining_transfer_count = $activeSeason->allowed_transfer_count;
                $teamSeason->playoff_member = true;
                $teamSeason->drawn = 0;
                $teamSeason->goal_against = 0;
                $teamSeason->goal_for = 0;
                $teamSeason->lost = 0;
                $teamSeason->match_total = 0;
                $teamSeason->won = 0;
                $teamSeason->gk_save = 0;
                $teamSeason->red_card = 0;
                $teamSeason->yellow_card = 0;
                $teamSeason->save();
            }
        }

        if(!empty($requested_player->team)){
            if($requested_player->team->captain_id == $requested_player->id){
                $requested_player->team->captain_id = null;
                $requested_player->team->save();
            }
        }


        if(!empty($requested_player->team)){
            $teamTeamPlayer = TeamTeamPlayer::where('player_id', $requested_player->id)->where('player_id', $requested_player->team->id)->whereNull('released_date')->first();
            if(!empty($teamTeamPlayer)){
                $teamTeamPlayer->released_date = Carbon::now()->format('Y-m-d');
                $teamTeamPlayer->save();
            }
        }
        
        $teamTeamPlayerNew = new TeamTeamPlayer();
        $teamTeamPlayerNew->player_id = $requested_player->id;
        $teamTeamPlayerNew->team_id = $team->id;
        $teamTeamPlayerNew->joined_date = Carbon::now()->format('Y-m-d');
        $teamTeamPlayerNew->save();


        $transfer = new Transfer();
        $transfer->requesting_captain_id = $captain->id;
        $transfer->requesting_team_id = $captain->team_id;

        $transfer->requested_player_id = $requested_player->id;
        if (!empty($requested_player->team_id)) {
            $transfer->requested_team_id = $requested_player->team_id;
        }

        $transfer->status = "accept";
        $transfer->save();

        if(!empty($activeSeason)){

            $transferSeason = new TransferSeason();
            $transferSeason->transfer_id = $transfer->id;
            $transferSeason->season_id = $activeSeason->id;
            $transferSeason->save();

        }


        $requested_player->team_id = $team->id;
        $requested_player->save();

        $text = 'Transfer Başarıyla Gerçekleşti...';
        return redirect('teams/edit/'.$id.'?t=tab_03')->with('message', array('text' => $text, 'status' => 'success'));

    }

    public function addPoint(Request $request)
    {
        
        $team = Team::find($request->input('team_id'));
        $teamSeason = TeamTeamSeason::find($request->input('tseason_id'));

        $newPoint = $teamSeason->point + $request->input('point');


        $teamSeasonAddPointLog = new TeamTeamSeasonAddPointLog();

        $teamSeasonAddPointLog->point_add = $request->input('point');
        $teamSeasonAddPointLog->point_new = $newPoint;
        $teamSeasonAddPointLog->teamseason_id = $teamSeason->id;

        $teamSeasonAddPointLog->user_id = Auth::user()->id;
        $teamSeasonAddPointLog->meta = null;
        $teamSeasonAddPointLog->point_type = 'Manual';

        $teamSeasonAddPointLog->save();

        $teamSeason->point += $request->input('point');
        $teamSeason->save();

        $text = 'Puan Başarıyla Gerçekleşti...';
        return redirect('teams/edit/'.$team->id.'?t=tab_02')->with('message', array('text' => $text, 'status' => 'success'));
    }

    public function addTransfer(Request $request)
    {
        
        $team = Team::find($request->input('team_id'));
        $teamSeason = TeamTeamSeason::find($request->input('tseason_id'));

        $teamSeason->remaining_transfer_count = $teamSeason->remaining_transfer_count + $request->input('transfer');
        $teamSeason->save();

        $text = 'Transfer Hakkı Başarıyla Eklendi...';
        return redirect('teams/edit/'.$team->id.'?t=tab_02')->with('message', array('text' => $text, 'status' => 'success'));
    }


    public function teamLogoDelete($id)
    {
        $team = Team::find($id);
        $team->image = null;
        $team->save();

        $text = 'Takım Logosu Başarıyla Silindi...';
        return redirect('teams/edit/'.$id.'?t=tab_05')->with('message', array('text' => $text, 'status' => 'success'));
    }

    public function teamCoverDelete($id)
    {
        $team = Team::find($id);
        $team->cover_image = null;
        $team->save();

        $text = 'Takım Profil Fotoğrafı Başarıyla Silindi...';
        return redirect('teams/edit/'.$id.'?t=tab_05')->with('message', array('text' => $text, 'status' => 'success'));

    }

}






