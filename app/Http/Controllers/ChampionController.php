<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Validator;

use Carbon\Carbon;
use App\User;

use App\League;
use App\LeaguesProvince;
use App\Championships;

class ChampionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	return view('dashboard');
    }

    public function crud($id = null)
    {
        $champions = NULL;
        if(!is_null($id)){
            $champions = Championships::find($id);
        }
        return view('dashboard', array('model_data' => $champions));
    }

    public function save(Request $request)
    {
        // dd($request->input());
        $text = "";
        if ($request->crud == 'delete') {

            $model = Championships::find($request->id);

            $model->delete();
            $text = 'Başarıyla Silindi...';

        }else{

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $model = new Championships();
            }else if($request->crud == 'edit'){
                $model = Championships::find($request->id);
            }

            $rules = array();
            $formConfig = config('forms.'.$request->segment);

            if(!is_null($formConfig)){
                foreach ($formConfig as $key => $value){
                    if(!empty(data_get($value, 'validation'))){
                        $rules[$key] = data_get($value, 'validation');
                    }
                }
            }
            
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
               return \Redirect::back()->withErrors($validator)->withInput();
            }

            $model->season_id = $request->season_id;
            $model->team_id = $request->team_id;
            $model->year = $request->year;
            $model->point = $request->point;
            $model->save();


            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $text = 'Başarıyla Eklendi...';
            }else if($request->crud == 'edit'){
                $text = 'Başarıyla Güncellendi...';
            }
        }

        return redirect($request->segment)->with('message', array('text' => $text, 'status' => 'success'));
    }

}