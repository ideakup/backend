<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Validator;

use Carbon\Carbon;
use App\User;

use App\Conference;
use App\ConferencesDistrict;

class ConferenceController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        if(request()->segment(1) == 'conferences' && !Auth::user()->hasPermissionTo('view_conference')){
            return redirect('/dashboard')->with('message', array('text' => "Bu sayfaya ulaşmak için yetkiniz bulunmuyor!", 'status' => 'error'));
        }
        return view('dashboard');
    }

    public function crud($id = null) {
        if(
            (request()->segment(2) == 'add' && !Auth::user()->hasPermissionTo('add_conference')) || 
            (request()->segment(2) == 'edit' && !Auth::user()->hasPermissionTo('change_conference')) || 
            (request()->segment(2) == 'delete' && !Auth::user()->hasPermissionTo('delete_conference'))
        ) {
            return redirect('/roles')->with('message', array('text' => "Bu sayfaya ulaşmak için yetkiniz bulunmuyor!", 'status' => 'error'));
        }

        $conference = NULL;
        if(!is_null($id)){
            $conference = Conference::find($id);
        }
        return view('dashboard', array('model_data' => $conference));
    }

    public function save(Request $request) {
        //dd($request->input());
        $text = "";
        if ($request->crud == 'delete') {

            $model = Conference::find($request->id);

            foreach ($model->conferences_district as $value) {
                $value->delete();
            }

            $model->delete();
            $text = 'Başarıyla Silindi...';

        }else{

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $model = new Conference();
            }else if($request->crud == 'edit'){
                $model = Conference::find($request->id);
            }

            $rules = array();
            $formConfig = config('forms.'.$request->segment);

            if(!is_null($formConfig)){
                foreach ($formConfig as $key => $value){
                    if(!empty(data_get($value, 'validation'))){
                        $rules[$key] = data_get($value, 'validation');
                    }
                }
            }
            
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
               return \Redirect::back()->withErrors($validator)->withInput();
            }
            
            $model->name = $request->name;
            $model->province_id = $request->province_id;
            $model->save();

            /* EDIT */
            if($request->crud == 'edit'){
                foreach ($model[data_get($value, 'relationship')] as $value_cd) {
                    $value_cd->delete();
                }
            }

            if(!empty($request->district_id)){
                foreach ($request->district_id as $value_cd) {
                    $cd = new ConferencesDistrict();
                    $cd->conference_id = $model->id;
                    $cd->district_id = $value_cd;
                    $cd->save();
                }
            }
            
            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $text = 'Başarıyla Eklendi...';
            }else if($request->crud == 'edit'){
                $text = 'Başarıyla Güncellendi...';
            }
        }

        return redirect($request->segment)->with('message', array('text' => $text, 'status' => 'success'));
    }

}