<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Validator;

use Carbon\Carbon;
use App\User;

use App\Season;
use App\TeamTeamSeason;

use App\Match;
use App\MatchVideo;
use App\MatchPlayer;
use App\MatchAction;
use App\MatchPanorama;
use App\MatchImage;

use App\PointType;

use App\PressConferences;
use App\PressConferencesPlayers;

use App\Team;
use App\Player;
use App\TeamPlayerTeamHistory;

use App\PanoramaType;
use App\PanoramaTypePrize;

use App\MounthlyPanorama;

class PanoramaController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }

    public function monthly_panorama() {
        
        $text = "Bu sayfaya ulaşmak için yetkiniz bulunmuyor!";
        if(request()->segment(1) == 'banners' && !Auth::user()->hasPermissionTo('add_banner')){
            return redirect('/dashboard')->with('message', array('text' => $text, 'status' => 'error'));
        }

        return view('dashboard');
    }

    public function monthly_panorama_player_crud($id = null) {
        
        $text = "Bu sayfaya ulaşmak için yetkiniz bulunmuyor!";
        if(request()->segment(1) == 'banners' && !Auth::user()->hasPermissionTo('add_banner')){
            return redirect('/dashboard')->with('message', array('text' => $text, 'status' => 'error'));
        }

        $monthly_panorama = NULL;
        if(!is_null($id)){
            $monthly_panorama = MounthlyPanorama::find($id);
        }
        return view('dashboard', array('model_data' => $monthly_panorama));
    }

    public function monthly_panorama_player_save(Request $request) {
        //dd($request->input());
        $text = "";
        if ($request->crud == 'delete') {

            $model = MounthlyPanorama::find($request->id);

            $cnt = 0;

            if($cnt == 0){
                $model->delete();
                $text = 'Başarıyla Silindi...';
            }else{
                $text = 'Bu panormanın uygulandığı en az bir kayıt bulunduğu için silinemedi!';
                return \Redirect::back()->withInput()->with('message', array('text' => $text, 'status' => 'error'));
            }

        }else{

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $model = new MounthlyPanorama();
            }else if($request->crud == 'edit'){
                $model = MounthlyPanorama::find($request->id);
            }

            $rules = array();
            $formConfig = config('forms.'.$request->segment);

            if(!is_null($formConfig)){
                foreach ($formConfig as $key => $value){
                    if(!empty(data_get($value, 'validation'))){
                        $rules[$key] = data_get($value, 'validation');
                    }
                }
            }
            
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
               return \Redirect::back()->withErrors($validator)->withInput();
            }

            $model->panorama_id = $request->panorama_id;
            $model->season_id = $request->season_id;
            $model->team_id = $request->team_id;
            $model->player_id = $request->player_id;
            $model->date = Carbon::parse($request->date);

            $model->save();            

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $text = 'Başarıyla Eklendi...';
            }else if($request->crud == 'edit'){
                $text = 'Başarıyla Güncellendi...';
            }
        }

        return redirect('monthly_panorama')->with('message', array('text' => $text, 'status' => 'success'));
    }

    public function monthly_panorama_team_crud($id = null) {
        $monthly_panorama = NULL;
        if(!is_null($id)){
            $monthly_panorama = MounthlyPanorama::find($id);
        }
        return view('dashboard', array('model_data' => $monthly_panorama));
    }

    public function monthly_panorama_team_save(Request $request) {
        //dd($request->input());
        $text = "";
        if ($request->crud == 'delete') {

            $model = MounthlyPanorama::find($request->id);

            $cnt = 0;

            if($cnt == 0){
                $model->delete();
                $text = 'Başarıyla Silindi...';
            }else{
                $text = 'Bu panormanın uygulandığı en az bir kayıt bulunduğu için silinemedi!';
                return \Redirect::back()->withInput()->with('message', array('text' => $text, 'status' => 'error'));
            }

        }else{

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $model = new MounthlyPanorama();
            }else if($request->crud == 'edit'){
                $model = MounthlyPanorama::find($request->id);
            }

            $rules = array();
            $formConfig = config('forms.'.$request->segment);

            if(!is_null($formConfig)){
                foreach ($formConfig as $key => $value){
                    if(!empty(data_get($value, 'validation'))){
                        $rules[$key] = data_get($value, 'validation');
                    }
                }
            }
            
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
               return \Redirect::back()->withErrors($validator)->withInput();
            }

            $model->panorama_id = $request->panorama_id;
            $model->season_id = $request->season_id;
            $model->team_id = $request->team_id;
            $model->date = Carbon::parse($request->date);

            $model->save();            

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $text = 'Başarıyla Eklendi...';
            }else if($request->crud == 'edit'){
                $text = 'Başarıyla Güncellendi...';
            }
        }

        return redirect('monthly_panorama')->with('message', array('text' => $text, 'status' => 'success'));
    }

    public function panorama_types() {
        $text = "Bu sayfaya ulaşmak için yetkiniz bulunmuyor!";
        if(request()->segment(1) == 'banners' && !Auth::user()->hasPermissionTo('add_banner')){
            return redirect('/dashboard')->with('message', array('text' => $text, 'status' => 'error'));
        }
        return view('dashboard');
    }

    public function panorama_type_crud($id = null) {
        $text = "Bu sayfaya ulaşmak için yetkiniz bulunmuyor!";
        if(request()->segment(1) == 'banners' && !Auth::user()->hasPermissionTo('add_banner')){
            return redirect('/dashboard')->with('message', array('text' => $text, 'status' => 'error'));
        }
        $panorama_type = NULL;
        if(!is_null($id)){
            $panorama_type = PanoramaType::find($id);
        }
        return view('dashboard', array('model_data' => $panorama_type));
    }

    public function panorama_type_save(Request $request) {
        //dd($request->input());
        $text = "";
        if ($request->crud == 'delete') {

            $model = PanoramaType::find($request->id);

            $cnt = 0;
            /*
            if($model->to_who == 'player'){
                $cnt = PanoramaType::where('penal_type_id', $model->id)->count();
            }else if($model->to_who == 'team'){
                $cnt = PanoramaType::where('penal_type_id', $model->id)->count();
            }
            */

            if($cnt == 0){
                $model->panorama_types_prize->delete();
                $model->delete();
                $text = 'Başarıyla Silindi...';
            }else{
                $text = 'Bu panormanın uygulandığı en az bir kayıp bulunduğu için silinemedi!';
                return \Redirect::back()->withInput()->with('message', array('text' => $text, 'status' => 'error'));
            }

        }else{

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $model = new PanoramaType();
            }else if($request->crud == 'edit'){
                $model = PanoramaType::find($request->id);
            }

            $rules = array();
            $formConfig = config('forms.'.$request->segment);

            if(!is_null($formConfig)){
                foreach ($formConfig as $key => $value){
                    if(!empty(data_get($value, 'validation'))){
                        $rules[$key] = data_get($value, 'validation');
                    }
                }
            }
            
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
               return \Redirect::back()->withErrors($validator)->withInput();
            }

            $model->name = $request->name;
            $model->to_who = $request->to_who;
            $model->league_id = $request->league_id;
            $model->season_id = $request->season_id;

            $model->polymorphic_ctype_id = 46;
            $model->order = 0;
            $model->active = true;
            $model->save();

            if ($request->crud == 'add') {
                $model_ptp = new PanoramaTypePrize();
                $model_ptp->period = $request->period;
            }else if($request->crud == 'edit'){
                $model_ptp = $model->panorama_types_prize;
            }

            $model_ptp->panoramatype_ptr_id = $model->id;
            $model_ptp->position_id = $request->position_id;
            $model_ptp->value = 1;

            $model_ptp->save();
            

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $text = 'Başarıyla Eklendi...';
            }else if($request->crud == 'edit'){
                $text = 'Başarıyla Güncellendi...';
            }
        }

        return redirect($request->segment)->with('message', array('text' => $text, 'status' => 'success'));
    }

    public function panorama_type_LogoDelete($id){
        $panoramaType = PanoramaType::find($id);
        $panoramaType->image = null;
        $panoramaType->save();

        $text = 'Sponsor Logosu Başarıyla Silindi...';
        return redirect('panorama_types/edit/'.$id.'?t=tab_02')->with('message', array('text' => $text, 'status' => 'success'));
    }

}
