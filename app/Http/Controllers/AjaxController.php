<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Auth;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use Carbon\Carbon;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

use App\Conference;
use App\ConferencesDistrict;
use App\Coordinator;
use App\District;
use App\Player;
use App\Profile;
use App\Province;
use App\Referee;
use App\User;

use App\Ground;
use App\GroundImage;
use App\GroundsProperty;
use App\GroundProperty;

use App\Reservation;
use App\ReservationOffer;
use App\ReservationGroundtime;
use App\ReservationGroundsubscription;
use App\ReservationGroundtable;

use App\League;
use App\LeagueGalleries;
use App\Season;
use App\SeasonBeforeAfter;

use App\Team;
use App\Match;
use App\MatchPlayer;
use App\MatchAction;
use App\MatchPanorama;
use App\MatchImage;

use App\PlayerPosition;
use App\PointType;
use App\Tactic;

use App\TeamSurvey;
use App\TeamPotential;
use App\TeamSatisfaction;
use App\TeamSatisfactionJogo;

use App\TeamTeamSeason;
use App\TeamTeamPlayer;
use App\EliminationTree;
use App\Group;
use App\Banner;
use App\Championships;


use App\Penalty;
use App\PlayerPenalty;
use App\PenaltyTypes;

use App\MounthlyPanorama;
use App\PanoramaType;
use App\PanoramaTypePrize;

use App\News;
use App\NewsTypes;
use App\Agenda;
use App\Tag;
use App\Rules;

use App\Galleries;
use App\PhotoGallery;
use App\PhotoGalleryItem;

use App\LeagueGalleryItems;

    
class AjaxController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getUsersAjax()
    {
    	
        $searchArray = array();
        foreach ($_REQUEST['columns'] as $column) {
            $searchArray[] = $column['search']['value'];
        }

        $order_by = array('id', 'identity', 'first_name', 'email', 'province_district', 'user_detail', 'phone',  'date_joined', 'icons', 'actions');
        $filter_by = array('id', 'identity', 'first_name', 'email', 'province_district', 'user_detail', 'phone',  'date_joined', 'icons', 'actions');

        $users = User::
        select( 'users.id',
                'users.is_superuser',
                'users.is_staff',
                'users.is_active',
                'users.email_confirmed',
                'users.phone_confirmed',
                'users.infoshare',
                'users.first_name',
                'users.last_name',
                'users.phone',
                'users.email',
                'users.date_joined',
                'profiles.identity_number as identity',

                'players.id as player_id',
                'teams.name as team_name',

                'coordinators.id as coordinator_id',
                'referees.id as referee_id',

                'provinces.name as province_name',
                'districts.name as district_name'
        )->
        leftJoin('profiles', 'users.id', '=', 'profiles.user_id')->
        
        leftJoin('players', 'users.id', '=', 'players.user_id')->
        leftJoin('teams', 'teams.id', '=', 'players.team_id')->

        leftJoin('coordinators', 'users.id', '=', 'coordinators.user_id')->
        leftJoin('referees', 'users.id', '=', 'referees.user_id')->

        leftJoin('provinces', 'profiles.province_id', '=', 'provinces.id')->
        leftJoin('districts', 'profiles.district_id', '=', 'districts.id');

        //RoleProvince
        $roleProvince = collect(Auth::user()->roleprovince)->pluck('province_id');
        if($roleProvince->count() > 0){
            $users = $users->whereIn('provinces.id', $roleProvince);
        }
        
        
        foreach ($searchArray as $key => $search) {
            
            if(!empty($search)){

                if($filter_by[$key] == 'identity'){

                    $users = $users->where('profiles.identity_number', $search);

                }else if($filter_by[$key] == 'first_name'){

                    $users = $users->where(function ($query) use ($search) {
                        $query->where(DB::raw("REPLACE(CONCAT(users.first_name, ' ', users.last_name),'  ',' ')"), 'ilike', '%'.$search.'%');
                    });

                }else if($filter_by[$key] == 'email'){

                    $users = $users->where('users.email', 'ilike', '%'.$search.'%');

                }else if($filter_by[$key] == 'province_district'){

                    $province_district = explode('|', $search);
                    $users = $users->where('provinces.id', $province_district[0]);

                    if(!empty($province_district[1])){
                        $dist = explode(',', $province_district[1]);
                        $users = $users->whereIn('districts.id', $dist);
                    }

                }else if($filter_by[$key] == 'phone'){

                    $users = $users->where('users.phone', $search);

                }
                
            }
        }

        if(!empty($_REQUEST['searchUserType'])){

            if($_REQUEST['searchUserType'] == 'coordinator'){
                
                $users = $users->whereNotNull('coordinators.id');
            
            }else if($_REQUEST['searchUserType'] == 'referee'){
                
                $users = $users->whereNotNull('referees.id');
            
            }else if($_REQUEST['searchUserType'] == 'player'){

                $users = $users->whereNotNull('players.id');

            }else if($_REQUEST['searchUserType'] == 'superuser'){
                
                $users = $users->where('users.is_superuser', true);
            
            }else if($_REQUEST['searchUserType'] == 'only_user'){
                
                $users = $users->whereNull('coordinators.id')->whereNull('referees.id')->whereNull('players.id');
            
            }
        }

        if(!empty($_REQUEST['searchTeam'])){
            if($_REQUEST['searchTeam'] == 'yes'){
                $users = $users->whereNotNull('teams.id');
            }else if($_REQUEST['searchTeam'] == 'no'){
                $users = $users->whereNull('teams.id');
            }
        }

        if(!empty($_REQUEST['searchActive'])){
            if($_REQUEST['searchActive'] == 'yes'){
                $users = $users->where('users.is_active', true);
            }else if($_REQUEST['searchActive'] == 'no'){
                $users = $users->where('users.is_active', false);
            }
        }

        if(!empty($_REQUEST['searchRole'])){
            $users = $users->role($_REQUEST['searchRole']);
        }

        if(!empty($_REQUEST['searchInfoShare'])){
            if($_REQUEST['searchInfoShare'] == 'yes'){
                $users = $users->where('users.infoshare', true);
            }else if($_REQUEST['searchInfoShare'] == 'no'){
                $users = $users->where('users.infoshare', false);
            }else if($_REQUEST['searchInfoShare'] == 'null'){
                $users = $users->whereNull('users.infoshare');
            }
        }
        

        //searchStaff
        //searchUserType
        //searchTeam
        //searchActive

        $users = $users->orderBy($order_by[$_REQUEST['order'][0]['column']], $_REQUEST['order'][0]['dir'])->limit(10000)->get();

        $iTotalRecords = $users->count();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        for ($i = $iDisplayStart; $i < $end; $i++) {
        
            $date_joined = Carbon::parse($users[$i]->date_joined);

            $coordinator_detail = ' ';
            if(!is_null($users[$i]->coordinator_id)){
                $coordinator_detail = 'coordinator';
            }

            $referee_detail = ' ';
            if(!is_null($users[$i]->referee_id)){
                $referee_detail = 'referee';
            }

            $player_detail = ' ';
            if(!is_null($users[$i]->player_id)){
                if(!is_null($users[$i]->team_name)){
                    $user_team = $users[$i]->team_name;
                }else{
                    $user_team = " - ";
                }
                $player_detail = 'player:'.$user_team;
            }

            $only_user_detail = ' ';
            if($coordinator_detail == ' ' && $referee_detail == ' ' && $player_detail == ' '){
                $only_user_detail = 'only_user';
            }

            if(empty($users[$i]->district_name)){
                $province_district = $users[$i]->province_name;
            }else{
                $province_district = $users[$i]->province_name.'/'.$users[$i]->district_name;
            }

            $staff_status = null;

            if($users[$i]->is_superuser){
                $staff_status = 2; 
            }elseif($users[$i]->is_staff){
                $staff_status = 1;
            }else{
                $staff_status = '';
            }

            if(is_null($users[$i]->infoshare)){
                $user_infoshare = 2;
            }else if($users[$i]->infoshare == true){
                $user_infoshare = 1;
            }else if($users[$i]->infoshare == false){
                $user_infoshare = '';
            }

            $records["data"][] = array(
            	$users[$i]->id,
                $users[$i]->identity,
            	$users[$i]->first_name .' '. $users[$i]->last_name,
                $users[$i]->email,
                $province_district,
            	$coordinator_detail.'::'.$referee_detail.'::'.$player_detail.'::'.$only_user_detail,
                $users[$i]->phone,
                $date_joined->format('d.m.Y'),
                $staff_status.'::'.$users[$i]->is_active.'::'.$users[$i]->email_confirmed.'::'.$users[$i]->phone_confirmed.'::'.$user_infoshare,
                '',
            );
            
        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
        
    }

    public function getPlayersAjax()
    {
        
        $searchArray = array();
        foreach ($_REQUEST['columns'] as $column) {
            $searchArray[] = $column['search']['value'];
        }

        $province_id = Team::find($_REQUEST['team_id'])->province->id;

        $order_by = array('player_id', 'identity', 'first_name', 'email', 'province_district', 'user_detail', 'phone',  'date_joined', 'icons', 'actions');
        $filter_by = array('player_id', 'identity', 'first_name', 'email', 'province_district', 'user_detail', 'phone',  'date_joined', 'icons', 'actions');

        $users = User::
        select( 'users.id',
                'users.is_superuser',
                'users.is_staff',
                'users.is_active',
                'users.email_confirmed',
                'users.phone_confirmed',
                'users.first_name',
                'users.last_name',
                'users.phone',
                'users.email',
                'users.date_joined',
                'profiles.identity_number as identity',

                'players.id as player_id',
                'teams.name as team_name',

                'coordinators.id as coordinator_id',
                'referees.id as referee_id',

                'provinces.name as province_name',
                'districts.name as district_name'
        )->
        leftJoin('profiles', 'users.id', '=', 'profiles.user_id')->
        
        leftJoin('players', 'users.id', '=', 'players.user_id')->
        leftJoin('teams', 'teams.id', '=', 'players.team_id')->

        leftJoin('coordinators', 'users.id', '=', 'coordinators.user_id')->
        leftJoin('referees', 'users.id', '=', 'referees.user_id')->

        leftJoin('provinces', 'profiles.province_id', '=', 'provinces.id')->
        leftJoin('districts', 'profiles.district_id', '=', 'districts.id');
        
        $users = $users->where('provinces.id', $province_id);

        foreach ($searchArray as $key => $search) {
            
            if(!empty($search)){

                if($filter_by[$key] == 'identity'){

                    $users = $users->where('profiles.identity_number', $search);

                }else if($filter_by[$key] == 'first_name'){

                    $users = $users->where(function ($query) use ($search) {
                        $query->where(DB::raw("REPLACE(CONCAT(users.first_name, ' ', users.last_name),'  ',' ')"), 'ilike', '%'.$search.'%');
                    });

                }else if($filter_by[$key] == 'email'){

                    $users = $users->where('users.email', 'ilike', '%'.$search.'%');

                }else if($filter_by[$key] == 'province_district'){

                    //$province_district = explode('|', $search);
                    //$users = $users->where('provinces.id', $province_district[0]);

                    //if(!empty($province_district[1])){
                        $dist = explode(',', $search);
                        $users = $users->whereIn('districts.id', $dist);
                    //}

                }else if($filter_by[$key] == 'phone'){

                    $users = $users->where('users.phone', $search);

                }
                
            }
        }

        $users = $users->whereNotNull('players.id');

        if(!empty($_REQUEST['searchTeam'])){
            if($_REQUEST['searchTeam'] == 'yes'){
                $users = $users->whereNotNull('teams.id');
            }else if($_REQUEST['searchTeam'] == 'no'){
                $users = $users->whereNull('teams.id');
            }
        }

        if(!empty($_REQUEST['searchActive'])){
            if($_REQUEST['searchActive'] == 'yes'){
                $users = $users->where('users.is_active', true);
            }else if($_REQUEST['searchActive'] == 'no'){
                $users = $users->where('users.is_active', false);
            }
        }

        if(!empty($_REQUEST['searchRole'])){
            $users = $users->role($_REQUEST['searchRole']);
        }



        //searchStaff
        //searchUserType
        //searchTeam
        //searchActive

        $users = $users->orderBy($order_by[$_REQUEST['order'][0]['column']], $_REQUEST['order'][0]['dir'])->limit(10000)->get();

        $iTotalRecords = $users->count();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        for ($i = $iDisplayStart; $i < $end; $i++) {
        
            $date_joined = Carbon::parse($users[$i]->date_joined);

            

            $player_detail = $users[$i]->team_name;;

           

            if(empty($users[$i]->district_name)){
                $province_district = $users[$i]->province_name;
            }else{
                $province_district = $users[$i]->province_name.'/'.$users[$i]->district_name;
            }

            $staff_status = null;

            if($users[$i]->is_superuser){
                $staff_status = 2; 
            }elseif($users[$i]->is_staff){
                $staff_status = 1;
            }else{
                $staff_status = '';
            }

            $records["data"][] = array(
                $users[$i]->player_id,
                $users[$i]->identity,
                $users[$i]->first_name .' '. $users[$i]->last_name,
                $users[$i]->email,
                $province_district,
                $player_detail,
                $users[$i]->phone,
                $date_joined->format('d.m.Y'),
                $staff_status.'::'.$users[$i]->is_active.'::'.$users[$i]->email_confirmed.'::'.$users[$i]->phone_confirmed,
                '',
            );
            
        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
        
    }

    public function getRolesAjax()
    {
        $searchArray = array();
        foreach ($_REQUEST['columns'] as $column) {
            $searchArray[] = $column['search']['value'];
        }

        $order_by = array('id', 'name', 'actions');
        $filter_by = array('id', 'name', 'actions');

        $roles = Role::orderBy($order_by[$_REQUEST['order'][0]['column']], $_REQUEST['order'][0]['dir'])->limit(10000)->get();

        $iTotalRecords = $roles->count();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        for ($i = $iDisplayStart; $i < $end; $i++) {
        
            $records["data"][] = array(
                $roles[$i]->id,
                $roles[$i]->description,
                '',
            );
            
        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
        
    }

    public function getConferencesAjax()
    {
        
        $searchArray = array();
        foreach ($_REQUEST['columns'] as $column) {
            $searchArray[] = $column['search']['value'];
        }

        $order_by = array('id', 'name', 'province_id', 'actions');
        $filter_by = array('id', 'name', 'province_id', 'actions');
        
        $conferences = Conference::orderBy($order_by[$_REQUEST['order'][0]['column']], $_REQUEST['order'][0]['dir']);

        foreach ($searchArray as $key => $search) {
            if(!empty($search)){
                if($filter_by[$key] == 'name'){
                    $conferences = $conferences->where('name', 'ilike', '%'.$search.'%');
                }else if($filter_by[$key] == 'province_id'){
                    $conferences = $conferences->where('province_id', $search);
                }
            }
        }

        //RoleProvince
        $roleProvince = collect(Auth::user()->roleprovince)->pluck('province_id');
        if($roleProvince->count() > 0){
            $conferences = $conferences->whereIn('province_id', $roleProvince);
        }

        $conferences = $conferences->get();

        $iTotalRecords = $conferences->count();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        for ($i = $iDisplayStart; $i < $end; $i++) {
           
            $records["data"][] = array(
                $conferences[$i]->id,
                $conferences[$i]->name,
                $conferences[$i]->province->name,
                '',
            );
            
        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
        
    }

    public function getProvincesAjax()
    {
        
        foreach ($_REQUEST['columns'] as $column) {
            $arrayName[] = $column['search']['value'];
        }

        $order_by = array('id', 'name', 'plate_code', 'actions');
        
        $provinces = Province::orderBy($order_by[$_REQUEST['order'][0]['column']], $_REQUEST['order'][0]['dir'])->get();

        $iTotalRecords = $provinces->count();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        for ($i = $iDisplayStart; $i < $end; $i++) {
           
            $records["data"][] = array(
                $provinces[$i]->id,
                $provinces[$i]->name,
                $provinces[$i]->plate_code,
                '',
            );
            
        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
        
    }

    public function getDistrictsAjax()
    {
        
        $searchArray = array();
        foreach ($_REQUEST['columns'] as $column) {
            $searchArray[] = $column['search']['value'];
        }

        $order_by = array('id', 'name', 'province_id', 'actions');
        $filter_by = array('id', 'name', 'province_id', 'actions');
        
        $districts = District::orderBy($order_by[$_REQUEST['order'][0]['column']], $_REQUEST['order'][0]['dir']);

        foreach ($searchArray as $key => $search) {
            if(!empty($search)){
                if($filter_by[$key] == 'province_id'){
                    $districts = $districts->where('province_id', $search);
                }
            }
        }

        //RoleProvince
        $roleProvince = collect(Auth::user()->roleprovince)->pluck('province_id');
        if($roleProvince->count() > 0){
            $districts = $districts->whereIn('province_id', $roleProvince);
        }

        $districts = $districts->get();

        $iTotalRecords = $districts->count();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        for ($i = $iDisplayStart; $i < $end; $i++) {
           
            $records["data"][] = array(
                $districts[$i]->id,
                $districts[$i]->name,
                $districts[$i]->province->name,
                '',
            );
            
        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
        
    }

    public function getGroundsAjax()
    {
        
        $searchArray = array();
        foreach ($_REQUEST['columns'] as $column) {
            $searchArray[] = $column['search']['value'];
        }

        $order_by = array('id', 'name', 'province_name', 'actions');
        $filter_by = array('id', 'name', 'province_district', 'actions');

        $grounds = Ground::
        select( 'grounds.id',
                'grounds.name',
                'provinces.name as province_name',
                'districts.name as district_name'
        )->
        leftJoin('provinces', 'grounds.province_id', '=', 'provinces.id')->
        leftJoin('districts', 'grounds.district_id', '=', 'districts.id');

        
        foreach ($searchArray as $key => $search) {
            
            if(!empty($search)){

                if($filter_by[$key] == 'name'){

                    $grounds = $grounds->where('grounds.name', 'ilike', '%'.$search.'%');

                }else if($filter_by[$key] == 'province_district'){

                    $province_district = explode('|', $search);
                    $grounds = $grounds->where('provinces.id', $province_district[0]);
                    if(!empty($province_district[1])){

                        $dist = explode(',', $province_district[1]);
                        $grounds = $grounds->whereIn('districts.id', $dist);
                        
                    }

                }
                
            }
            
        }
        

        //RoleProvince
        $roleProvince = collect(Auth::user()->roleprovince)->pluck('province_id');
        if($roleProvince->count() > 0){
            $grounds = $grounds->whereIn('provinces.id', $roleProvince);
        }

        
        $grounds = $grounds->orderBy($order_by[$_REQUEST['order'][0]['column']], $_REQUEST['order'][0]['dir'])->get();
        //dd($grounds);
        $iTotalRecords = $grounds->count();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        for ($i = $iDisplayStart; $i < $end; $i++) {
            
            if(empty($grounds[$i]->district_name)){
                $province_district = $grounds[$i]->province_name;
            }else{
                $province_district = $grounds[$i]->province_name.'/'.$grounds[$i]->district_name;
            }

            $records["data"][] = array(
                $grounds[$i]->id,
                $grounds[$i]->name,
                $province_district,
                '',
            );
            
        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
        
    }

    public function getGroundPropertiesAjax()
    {
        
        $searchArray = array();
        foreach ($_REQUEST['columns'] as $column) {
            $searchArray[] = $column['search']['value'];
        }

        $order_by = array('id', 'name', 'actions');
        $filter_by = array('id', 'name', 'actions');
        
        $ground_properties = GroundProperty::orderBy($order_by[$_REQUEST['order'][0]['column']], $_REQUEST['order'][0]['dir']);

        foreach ($searchArray as $key => $search) {
            if(!empty($search)){
                if($filter_by[$key] == 'name'){
                    $ground_properties = $ground_properties->where('name', 'ilike', '%'.$search.'%');
                }
            }
        }
        $ground_properties = $ground_properties->get();

        $iTotalRecords = $ground_properties->count();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        for ($i = $iDisplayStart; $i < $end; $i++) {
           
            $records["data"][] = array(
                $ground_properties[$i]->id,
                $ground_properties[$i]->name,
                '',
            );
            
        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
        
    }

    public function getGroundHourAjax()
    {

        $searchArray = array();
        foreach ($_REQUEST['columns'] as $column) {
            $searchArray[] = $column['search']['value'];
        }

        $order_by = array('sort_id', 'time', 'day1', 'day2', 'day3', 'day4', 'day5', 'day6', 'day7');
        $filter_by = array('sort_id', 'time', 'day1', 'day2', 'day3', 'day4', 'day5', 'day6', 'day7');
        
        $records = array();
        $records["data"] = array();
        $records["colName"] = array();
        if(!empty($_REQUEST['select2_week_start_date'])){
            for ($f = 0; $f <= 6; $f++) {
                $records["colName"][] = Carbon::parse($_REQUEST['select2_week_start_date'])->startOfWeek()->addDays($f)->format('d.m.Y');
            }
        }

        $ground_hours = ReservationGroundtime::orderBy('order')->get();

        $iTotalRecords = $ground_hours->count();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        for ($i = $iDisplayStart; $i < $end; $i++) {
            
            $timee = Carbon::parse($ground_hours[$i]->time)->format('H:i');
            $records["data"][] = array(
                $i,
                $timee,
                '0|'.ReservationGroundsubscription::where('ground_id', $_REQUEST['ground_id'])->where('day', 0)->where('active', true)->where('time', $timee)->count().'|'.ReservationGroundtable::where('ground_id', $_REQUEST['ground_id'])->where('date', Carbon::parse($_REQUEST['select2_week_start_date'])->startOfWeek()->addDays(0)->format('Y-m-d'))->where('active', true)->where('time', $timee)->count().'|',
                '1|'.ReservationGroundsubscription::where('ground_id', $_REQUEST['ground_id'])->where('day', 1)->where('active', true)->where('time', $timee)->count().'|'.ReservationGroundtable::where('ground_id', $_REQUEST['ground_id'])->where('date', Carbon::parse($_REQUEST['select2_week_start_date'])->startOfWeek()->addDays(1)->format('Y-m-d'))->where('active', true)->where('time', $timee)->count().'|',
                '2|'.ReservationGroundsubscription::where('ground_id', $_REQUEST['ground_id'])->where('day', 2)->where('active', true)->where('time', $timee)->count().'|'.ReservationGroundtable::where('ground_id', $_REQUEST['ground_id'])->where('date', Carbon::parse($_REQUEST['select2_week_start_date'])->startOfWeek()->addDays(2)->format('Y-m-d'))->where('active', true)->where('time', $timee)->count().'|',
                '3|'.ReservationGroundsubscription::where('ground_id', $_REQUEST['ground_id'])->where('day', 3)->where('active', true)->where('time', $timee)->count().'|'.ReservationGroundtable::where('ground_id', $_REQUEST['ground_id'])->where('date', Carbon::parse($_REQUEST['select2_week_start_date'])->startOfWeek()->addDays(3)->format('Y-m-d'))->where('active', true)->where('time', $timee)->count().'|',
                '4|'.ReservationGroundsubscription::where('ground_id', $_REQUEST['ground_id'])->where('day', 4)->where('active', true)->where('time', $timee)->count().'|'.ReservationGroundtable::where('ground_id', $_REQUEST['ground_id'])->where('date', Carbon::parse($_REQUEST['select2_week_start_date'])->startOfWeek()->addDays(4)->format('Y-m-d'))->where('active', true)->where('time', $timee)->count().'|',
                '5|'.ReservationGroundsubscription::where('ground_id', $_REQUEST['ground_id'])->where('day', 5)->where('active', true)->where('time', $timee)->count().'|'.ReservationGroundtable::where('ground_id', $_REQUEST['ground_id'])->where('date', Carbon::parse($_REQUEST['select2_week_start_date'])->startOfWeek()->addDays(5)->format('Y-m-d'))->where('active', true)->where('time', $timee)->count().'|',
                '6|'.ReservationGroundsubscription::where('ground_id', $_REQUEST['ground_id'])->where('day', 6)->where('active', true)->where('time', $timee)->count().'|'.ReservationGroundtable::where('ground_id', $_REQUEST['ground_id'])->where('date', Carbon::parse($_REQUEST['select2_week_start_date'])->startOfWeek()->addDays(6)->format('Y-m-d'))->where('active', true)->where('time', $timee)->count().'|',
            );
            
        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
        
    }

    public function getLeaguesAjax()
    {
        
        $searchArray = array();
        foreach ($_REQUEST['columns'] as $column) {
            $searchArray[] = $column['search']['value'];
        }

        $order_by = array('id', 'name', 'province', 'active', 'actions');
        $filter_by = array('id', 'name', 'province', 'active', 'actions');
        
        $leagues = League::
        select( 'leagues.id',
                'leagues.name',
                'leagues.active',
                DB::raw("count(leagues_provinces.province_id) as count")
        )->
        leftJoin('leagues_provinces', 'leagues.id', '=', 'leagues_provinces.league_id');
        
        foreach ($searchArray as $key => $search) {
            if(!empty($search)){
                if($filter_by[$key] == 'name'){
                    $leagues = $leagues->where('leagues.name', 'ilike', '%'.$search.'%');
                }else if($filter_by[$key] == 'province'){
                    $leagues = $leagues->where('leagues_provinces.province_id', $search);
                }else if($filter_by[$key] == 'active'){
                    $leagues = $leagues->where('leagues.active', $search);
                }
            }
        }

        //RoleProvince
        $roleProvince = collect(Auth::user()->roleprovince)->pluck('province_id');
        if($roleProvince->count() > 0){
            $leagues = $leagues->whereIn('leagues_provinces.province_id', $roleProvince);
        }

        $leagues = $leagues->orderBy($order_by[$_REQUEST['order'][0]['column']], $_REQUEST['order'][0]['dir'])->groupBy('leagues.id')->get();

        $iTotalRecords = $leagues->count();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        for ($i = $iDisplayStart; $i < $end; $i++) {
            
            if($leagues[$i]->league_province->count() == 0){
                $l_p_name = ' - ';
            }else if($leagues[$i]->league_province->count() == 1){
                $l_p_name = $leagues[$i]->league_province->first()->province->name;
            }else{
                $l_p_name = $leagues[$i]->league_province->count().' Şehir';
            }

            $records["data"][] = array(
                $leagues[$i]->id,
                $leagues[$i]->name,
                $l_p_name,
                $leagues[$i]->active,
                ''
            );
            
        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
        
    }

    public function getLeagueGalleriesAjax(Request $request)
    {
        
        $searchArray = array();
        foreach ($_REQUEST['columns'] as $column) {
            $searchArray[] = $column['search']['value'];
        }

        $order_by = array('id', 'name', 'league', 'show_home', 'actions');
        $filter_by = array('id', 'name', 'league', 'show_home', 'actions');
        
        $league_galleries = LeagueGalleries::
        select( 'league_galleries.id as id',
                'league_galleries.name as name',
                'league_galleries.show_home',
                'leagues.id as lidd',
                'leagues.name as league',
                'leagues.active'
        )->
        leftJoin('leagues', 'leagues.id', '=', 'league_galleries.league_id')->
        leftJoin('leagues_provinces', 'leagues.id', '=', 'leagues_provinces.league_id')->
        where('league_galleries.league_id', $request->input('league_id'));


        //RoleProvince
        /*
        $roleProvince = collect(Auth::user()->roleprovince)->pluck('province_id');
        if($roleProvince->count() > 0){
            $league_galleries = $league_galleries->whereIn('leagues_provinces.province_id', $roleProvince);
        }
        */

        $league_galleries = $league_galleries->orderBy($order_by[$_REQUEST['order'][0]['column']], $_REQUEST['order'][0]['dir'])->get();
        //echo json_encode($league_galleries);

        $iTotalRecords = $league_galleries->count();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        for ($i = $iDisplayStart; $i < $end; $i++) {

            $records["data"][] = array(
                $league_galleries[$i]->id,
                $league_galleries[$i]->name,
                $league_galleries[$i]->league,
                $league_galleries[$i]->show_home,
                ''
            );
            
        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
        
    }

    public function getChampionsAjax(Request $request)
    {
        
        $searchArray = array();
        foreach ($_REQUEST['columns'] as $column) {
            $searchArray[] = $column['search']['value'];
        }

        $order_by = array('id', 'season', 'team', 'year', 'point', 'actions');
        $filter_by = array('id', 'season', 'team', 'year', 'point', 'actions');
        
        $champions = Championships::
        select( 'championships.id',
                'championships.season_id',
                'championships.team_id',
                'championships.year',
                'championships.point',
                'teams.name as team',
                'leagues.id as leagueId',
                DB::raw("case when championships.season_id is null then '' else CONCAT(seasons.year, ' ', leagues.name, ' ', seasons.name) end as season")

                //DB::raw("count(leagues_provinces.province_id) as count")
        )->
        leftJoin('seasons', 'seasons.id', '=', 'championships.season_id')->
        leftJoin('leagues', 'leagues.id', '=', 'seasons.league_id')->
        leftJoin('teams', 'teams.id', '=', 'championships.team_id');
        //leftJoin('leagues_provinces', 'leagues.id', '=', 'leagues_provinces.league_id');
        
        foreach ($searchArray as $key => $search) {
            if(!empty($search)){
                if($filter_by[$key] == 'year'){
                    $champions = $champions->where('championships.year', $search);
                }
            }
        }

        if(!empty($request->input('leagueId'))){
            $champions = $champions->where('leagues.id', $request->input('leagueId'));
        }

        //RoleProvince
        /*
        $roleProvince = collect(Auth::user()->roleprovince)->pluck('province_id');
        if($roleProvince->count() > 0){
            $leagues = $leagues->whereIn('leagues_provinces.province_id', $roleProvince);
        }
        */

        $champions = $champions->orderBy($order_by[$_REQUEST['order'][0]['column']], $_REQUEST['order'][0]['dir'])->get();

        $iTotalRecords = $champions->count();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        for ($i = $iDisplayStart; $i < $end; $i++) {
  
            $records["data"][] = array(
                $champions[$i]->id,
                $champions[$i]->season,
                $champions[$i]->team,
                $champions[$i]->year,
                $champions[$i]->point,
                ''
            );
            
        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
        
    }

    public function getSeasonsAjax()
    {
        
        $searchArray = array();
        foreach ($_REQUEST['columns'] as $column) {
            $searchArray[] = $column['search']['value'];
        }

        $order_by = array('id', 'league_id', 'year', 'name', 'polymorphic_ctype_id', 'start_date', 'end_date', 'actions');
        $filter_by = array('id', 'league', 'year', 'name', 'polymorphic_ctype_id', 'start_date', 'end_date', 'actions');

        $seasons = Season::
        select( 'seasons.id',
                'seasons.league_id',
                'seasons.name',
                'seasons.polymorphic_ctype_id',
                'seasons.year',
                'seasons.start_date',
                'seasons.end_date',
                'seasons.active',
                DB::raw("count(leagues_provinces.province_id) as count")
        )->
        leftJoin('leagues', 'leagues.id', '=', 'seasons.league_id')->
        leftJoin('leagues_provinces', 'leagues.id', '=', 'leagues_provinces.league_id');

        foreach ($searchArray as $key => $search) {
            if(!empty($search)){
                if($filter_by[$key] == 'league'){
                    $seasons = $seasons->where('seasons.league_id', $search);
                }else if($filter_by[$key] == 'name'){
                    $seasons = $seasons->where('seasons.name', 'ilike', '%'.$search.'%');
                }else if($filter_by[$key] == 'polymorphic_ctype_id'){
                    $seasons = $seasons->where('seasons.polymorphic_ctype_id', $search);
                }else if($filter_by[$key] == 'year'){
                    $seasons = $seasons->where('seasons.year', $search);
                }else if($filter_by[$key] == 'active'){
                    $seasons = $seasons->where('seasons.active', $search);
                }
            }
        }



        if(!Auth::user()->hasPermissionTo('view_pointseason')){
            $seasons = $seasons->where('seasons.polymorphic_ctype_id', '!=', 34);
        }

        if(!Auth::user()->hasPermissionTo('view_fixtureseason')){
            $seasons = $seasons->where('seasons.polymorphic_ctype_id', '!=', 33);
        }

        if(!Auth::user()->hasPermissionTo('view_eliminationseason')){
            $seasons = $seasons->where('seasons.polymorphic_ctype_id', '!=', 32);
        }

        //RoleProvince
        $roleProvince = collect(Auth::user()->roleprovince)->pluck('province_id');
        if($roleProvince->count() > 0){
            $seasons = $seasons->whereIn('leagues_provinces.province_id', $roleProvince);
        }

        $seasons = $seasons->orderBy($order_by[$_REQUEST['order'][0]['column']], $_REQUEST['order'][0]['dir'])->groupBy('seasons.id')->get();


        $iTotalRecords = $seasons->count();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        for ($i = $iDisplayStart; $i < $end; $i++) {
        
            $records["data"][] = array(
                $seasons[$i]->id,
                $seasons[$i]->league->name.'::'.$seasons[$i]->league->id,
                $seasons[$i]->year,
                $seasons[$i]->name,
                $seasons[$i]->content_type->model,
                Carbon::parse($seasons[$i]->start_date)->format('d.m.Y'),
                Carbon::parse($seasons[$i]->end_date)->format('d.m.Y'),
                '',
            );
        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
        
    }

    public function getSeasonsAddBonusAjax()
    {
        
        $searchArray = array();
        foreach ($_REQUEST['columns'] as $column) {
            $searchArray[] = $column['search']['value'];
        }

        $order_by = array('id', 'league', 'name', 'year', 'start_date', 'end_date', 'sumadded', 'month');
        $filter_by = array('id', 'league', 'name', 'year', 'start_date', 'end_date', 'sumadded', 'month');

        $seasons = Season::
        select( 'seasons.id',
                'seasons.league_id',
                'seasons.name',
                'seasons.polymorphic_ctype_id',
                'seasons.year',
                'seasons.start_date',
                'seasons.end_date',
                'seasons.active'
        )->
        leftJoin('leagues', 'leagues.id', '=', 'seasons.league_id')->where('seasons.polymorphic_ctype_id', 34)->where('seasons.end_date', '>=', Carbon::now()->subMonth()->startOfMonth());
        

        $seasons = $seasons->orderBy($order_by[$_REQUEST['order'][0]['column']], $_REQUEST['order'][0]['dir'])->get();


        $iTotalRecords = $seasons->count();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $monthNames = array('Ocak', 'Şubat', 'Mart', 'Nisan', 'Mayıs', 'Haziran', 'Temmuz', 'Ağustos', 'Eylül', 'Ekim', 'Kasım', 'Aralık');
        
        for ($i = $iDisplayStart; $i < $end; $i++) {
            
            $records["data"][] = array(
                $seasons[$i]->id,
                $seasons[$i]->league->name,
                $seasons[$i]->name,
                $seasons[$i]->year,
                Carbon::parse($seasons[$i]->start_date)->format('d.m.Y'),
                Carbon::parse($seasons[$i]->end_date)->format('d.m.Y'),
                '',
                $monthNames[Carbon::now()->subMonth()->format('n')-1]
            );
            
        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    public function getTeamsAjax()
    {
        
        $searchArray = array();
        foreach ($_REQUEST['columns'] as $column) {
            $searchArray[] = $column['search']['value'];
        }

        $order_by = array('id', 'name', 'province_id', 'district_id', 'captain_id', 'phone', 'created_at', 'potential', 'status', 'ms', 'ks', 'actions');
        $filter_by = array('id', 'name', 'province_id', 'district_id', 'captain_id', 'phone', 'created_at', 'potential', 'status', 'ms', 'ks', 'actions');


        //DB::connection()->enableQueryLog();
        //$team_teamplayer_count_sql = "(SELECT COUNT(team_teamplayer.team_id) FROM team_teamplayer WHERE team_teamplayer.team_id = teams.id AND released_date IS NULL)";
        $__ms_before = explode('|', $searchArray[9]);

        if(empty($__ms_before[2]) && empty($__ms_before[3])){
            $team_match_count_sql = "(SELECT COUNT(matches.id) FROM matches WHERE matches.season_id = (SELECT seasons.id FROM team_teamseason LEFT JOIN seasons ON seasons.id = team_teamseason.season_id WHERE team_teamseason.team_id = teams.id AND seasons.end_date > NOW() ORDER BY seasons.end_date DESC LIMIT 1) AND (matches.team1_id = teams.id OR matches.team2_id = teams.id))";
        }else{

            $__ms_before[2] = Carbon::parse($__ms_before[2])->format('Y-m-d');
            $__ms_before[3] = Carbon::parse($__ms_before[3])->format('Y-m-d');

            $team_match_count_sql = "(SELECT COUNT(matches.id) FROM matches WHERE (matches.date >= '".$__ms_before[2]."' AND matches.date <= '".$__ms_before[3]."') AND (matches.team1_id = teams.id OR matches.team2_id = teams.id))";
        }

        //$matches = $matches->where('date', '>=', $start_date)->where('date', '<=', $end_date);

        //dd($team_match_count_sql);


        $teams = Team::
        select(
            'teams.*', 
            'teams.name', 
            'teams.province_id', 
            'teams.district_id',
            'teams.captain_id',
            'teams.status',

            'users.first_name as captain_first_name', 
            'users.last_name as captain_last_name', 
            'users.phone as captain_phone',
            
            'provinces.name as province_name',
            'districts.name as district_name',
            
            'team_potentials.id as team_potential_id',
            'team_satisfactions.id as team_satisfactions_id',
            'team_satisfactions_jogo.id as team_satisfactions_jogo_id',
            
            //DB::raw($team_teamplayer_count_sql." as team_teamplayer_count"),
            DB::raw($team_match_count_sql." as team_match_count")
        )->
        leftJoin('players', 'players.id', '=', 'teams.captain_id')->
        leftJoin('users', 'users.id', '=', 'players.user_id')->
        leftJoin('provinces', 'provinces.id', '=', 'teams.province_id')->
        leftJoin('districts', 'districts.id', '=', 'teams.district_id')->

        leftJoin('team_surveys', 'team_surveys.team_id', '=', 'teams.id')->
        leftJoin('team_potentials', 'team_potentials.id', '=', 'team_surveys.potential_id')->
        leftJoin('team_satisfactions', 'team_satisfactions.id', '=', 'team_surveys.satisfaction_id')->
        leftJoin('team_satisfactions_jogo', 'team_satisfactions_jogo.id', '=', 'team_surveys.satisfaction_jogo_id');
        // Sabri Anıl Özkan
        foreach ($searchArray as $key => $search) {
            if(!empty($search)){
                if($filter_by[$key] == 'name'){
                    $teams = $teams->where('teams.name', 'ilike', '%'.$search.'%');
                }else if($filter_by[$key] == 'province_id'){
                    $teams = $teams->where('teams.province_id', $search);
                }else if($filter_by[$key] == 'district_id'){
                    
                    if(str_contains($search, ',')){
                        $dArray = explode(',', $search);
                        $teams = $teams->whereIn('teams.district_id', $dArray);
                    }else{
                        $teams = $teams->where('teams.district_id', $search); 
                    }

                }else if($filter_by[$key] == 'captain_id'){
                    $teams = $teams->where('teams.captain_id', $search);
                }else if($filter_by[$key] == 'phone'){
                    $teams = $teams->where('users.phone', 'ilike', '%'.$search.'%');
                }else if($filter_by[$key] == 'potential'){
                    $__potential = explode('|', $search);
                    if(!empty($__potential[0])){
                        $teams = $teams->where('team_potentials.id', $__potential[0]);
                    }
                    if(!empty($__potential[1])){
                        $teams = $teams->where('team_satisfactions.id', $__potential[1]);
                    }
                    if(!empty($__potential[2])){
                        $teams = $teams->where('team_satisfactions_jogo.id', $__potential[2]);
                    }
                }else if($filter_by[$key] == 'status'){
                    $teams = $teams->where('teams.status', $search);
                }else if($filter_by[$key] == 'ms'){



                    $__ms = explode('|', $search);
                    if(!empty($__ms[0])){
                        $teams = $teams->where(DB::raw($team_match_count_sql), '>=', $__ms[0]);
                    }
                    if(!empty($__ms[1])){
                        $teams = $teams->where(DB::raw($team_match_count_sql), '<=', $__ms[1]);
                    }

                    //print_r(Carbon::parse($__ms[2])->format('Y-m-d'));
                    //print_r(Carbon::parse($__ms[3])->format('Y-m-d'));
                    //die;

                }else if($filter_by[$key] == 'ks'){
                    $__ks = explode('|', $search);
                    if(!empty($__ks[0])){
                        $teams = $teams->where(DB::raw($team_teamplayer_count_sql), '>=', $__ks[0]);
                    }
                    if(!empty($__ks[1])){
                        $teams = $teams->where(DB::raw($team_teamplayer_count_sql), '<=', $__ks[1]);
                    }
                }
            }
        }

        //RoleProvince
        $roleProvince = collect(Auth::user()->roleprovince)->pluck('province_id');
        if($roleProvince->count() > 0){
            $teams = $teams->whereIn('teams.province_id', $roleProvince);
        }

        $teams = $teams->orderBy($order_by[$_REQUEST['order'][0]['column']], $_REQUEST['order'][0]['dir'])->limit(10000)->get();

        //dd(DB::getQueryLog());


        $iTotalRecords = $teams->count();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        for ($i = $iDisplayStart; $i < $end; $i++) {

            $records["data"][] = array(
                $teams[$i]->id,
                $teams[$i]->name,
                $teams[$i]->province_name,
                $teams[$i]->district_name,
                $teams[$i]->captain_first_name.' '.$teams[$i]->captain_last_name,
                $teams[$i]->captain_phone,
                Carbon::parse($teams[$i]->created_at)->format('d.m.Y'),
                $teams[$i]->team_potential_id. '::' .$teams[$i]->team_satisfactions_id. '::' .$teams[$i]->team_satisfactions_jogo_id, 
                $teams[$i]->status,
                $teams[$i]->team_match_count,
                $teams[$i]->player->count(), //$teams[$i]->team_teamplayer_count,
                '',
            );
            
        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    public function getMatchesAjax(Request $request)
    {
        $searchArray = array();
        foreach ($_REQUEST['columns'] as $column) {
            $searchArray[] = $column['search']['value'];
        }
        //dd($searchArray);
        $order_by = array('id', 'season_name', 'team1_name', 'team2_name', 'date', 'time', 'videodurumu', 'completed', 'actions');
        //$filter_by = array('id', 'season', 'team1_id', 'team2_id', 'date', 'time', 'videodurumu', 'completed', 'actions');
        $filter_by = array('', 'season_id', 'team_name', '', 'date', '', 'videodurumu', 'completed', '');

        $matches = Match::select(
            'matches.id',
            'matches.season_id as season_id',
            DB::raw("case when matches.season_id is null then '' else CONCAT(seasons.year, ' ', leagues.name, ' ', seasons.name) end as season_name"),
            'matches.team1_id',
            'matches.team1_goal',
            't1.name as team1_name',
            'matches.team2_id',
            'matches.team2_goal',
            't2.name as team2_name',
            'matches.date',
            'matches.time',
            'matches.completed',
            'matches.completed_status',
            'match_video.status as video_status'
        )->
        leftJoin('teams as t1', 't1.id', '=', 'matches.team1_id')->
        leftJoin('teams as t2', 't2.id', '=', 'matches.team2_id')->
        leftJoin('seasons', 'seasons.id', '=', 'matches.season_id')->
        leftJoin('leagues', 'leagues.id', '=', 'seasons.league_id')->
        leftJoin('leagues_provinces', 'leagues.id', '=', 'leagues_provinces.league_id')->
        leftJoin('match_video', 'matches.id', '=', 'match_video.match_id');

        foreach ($searchArray as $key => $search) {
            if(!empty($search)){
                if($filter_by[$key] == 'season_id'){
                    $matches = $matches->where('season_id', $search);
                }else if($filter_by[$key] == 'team_name'){

                    $matches = $matches->where(function ($query) use ($search) {
                        $query->where('t1.name', 'ilike', '%'.$search.'%')->
                        orWhere('t2.name', 'ilike', '%'.$search.'%');
                    });
                    
                }else if($filter_by[$key] == 'date'){

                    if(!empty($search) && $search != '::'){

                        $dateee = explode('::',$search);
                        
                        $start_date = null;
                        if(!empty($dateee[0])){
                            $start_date = Carbon::parse($dateee[0])->format('Y-m-d');
                        }

                        $end_date = null;
                        if(!empty($dateee[1])){
                            $end_date = Carbon::parse($dateee[1])->format('Y-m-d');
                        }

                        if (!empty($start_date) && !empty($end_date)) {
                            $matches = $matches->where('date', '>=', $start_date)->where('date', '<=', $end_date);
                        }else{
                            if (!empty($start_date)) {
                                $matches = $matches->where('date', $start_date);
                            }
                            if (!empty($end_date)) {
                                $matches = $matches->where('date', $end_date);
                            }
                        }
                    }
                    
                }else if($filter_by[$key] == 'videodurumu'){
                    $matches = $matches->where('match_video.status', $search);
                }else if($filter_by[$key] == 'completed'){
                    $matches = $matches->where('matches.completed', $search);
                }
            }
        }
        
        //RoleProvince
        $roleProvince = collect(Auth::user()->roleprovince)->pluck('province_id');
        if($roleProvince->count() > 0){
            $matches = $matches->whereIn('leagues_provinces.province_id', $roleProvince);
        }

        if(!empty($request->input('leagueId'))){
            $matches = $matches->where('seasons.league_id', $request->input('leagueId'));
        }



        //$matchesCount = $matches->orderBy($order_by[$_REQUEST['order'][0]['column']], $_REQUEST['order'][0]['dir'])->count();
        //echo $matchesCount;
        //die;
        $matches = $matches->orderBy($order_by[$_REQUEST['order'][0]['column']], $_REQUEST['order'][0]['dir'])->limit(10000)->get()->unique();

        $iTotalRecords = $matches->count();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        for ($i = $iDisplayStart; $i < $end; $i++) {

            $team1_g = '';
            $team2_g = '';

            $comp_s = '';

            if($matches[$i]->completed){

                if(!empty($matches[$i]->completed_status)){
                    $comp_s = '<span class="label label-light-info label-rounded mr-2">H</span>';
                }

                if($matches[$i]->team1_goal > $matches[$i]->team2_goal){
                    $team1_g = $comp_s.'<span class="label label-rounded label-outline-success mr-2">'.$matches[$i]->team1_goal.'</span>';
                    $team2_g = $comp_s.'<span class="label label-rounded label-outline-danger mr-2">'.$matches[$i]->team2_goal.'</span>';
                }else if($matches[$i]->team1_goal < $matches[$i]->team2_goal){
                    $team1_g = $comp_s.'<span class="label label-rounded label-outline-danger mr-2">'.$matches[$i]->team1_goal.'</span>';
                    $team2_g = $comp_s.'<span class="label label-rounded label-outline-success mr-2">'.$matches[$i]->team2_goal.'</span>';
                }else if($matches[$i]->team1_goal == $matches[$i]->team2_goal){
                    $team1_g = $comp_s.'<span class="label label-rounded label-outline-warning mr-2">'.$matches[$i]->team1_goal.'</span>';
                    $team2_g = $comp_s.'<span class="label label-rounded label-outline-warning mr-2">'.$matches[$i]->team2_goal.'</span>';
                }
            }

            $records["data"][] = array(
                $matches[$i]->id,
                $matches[$i]->season_name,
                (!empty($matches[$i]->team1_id)) ? $team1_g.$matches[$i]->team1->name : '',
                (!empty($matches[$i]->team2_id)) ? $team2_g.$matches[$i]->team2->name : '',
                Carbon::parse($matches[$i]->date)->format('d.m.Y'),
                Carbon::parse($matches[$i]->time)->format('H:i'),
                $matches[$i]->video_status,
                $matches[$i]->completed,
                ''
            );
        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);

    }

    public function getReservationsAjax()
    {
        
        $searchArray = array();
        foreach ($_REQUEST['columns'] as $column) {
            $searchArray[] = $column['search']['value'];
        }

        //'id', 'province_id', 'ground_id', 'date', 'time', 'team_id', 'offer', 'status', 'actions'
        //'reservation_id', 'province_name', 'ground_name', 'reservation_groundtable_date', 'reservation_groundtable_time', 'team_id', 'reservation_offer_count', 'status', 'actions'

        $order_by = array('reservation_id', 'province_name', 'ground_name', 'reservation_groundtable_date', 'reservation_groundtable_time', 'team_id', 'reservation_offer_count', 'status', 'actions');
        $filter_by = array('', 'provinces.id', 'grounds.id', '', '', '', '', 'reservations.status', '');

        $reservations = Reservation::
        select(
            'reservations.id as reservation_id',
            'reservations.ground_table_id',
            'reservations.match_id as reservations_match_id',
            'reservations.status',

            'reservation_groundtable.date as reservation_groundtable_date',
            'reservation_groundtable.time as reservation_groundtable_time',

            'grounds.name as ground_name',
            'provinces.name as province_name',

            'reservation_groundtable.id as reservation_groundtable_id',
            'grounds.id as grounds_id',
            'provinces.id as provinces_id',

            'teams.name as team_name',

            DB::raw('(SELECT COUNT(*) AS ad FROM reservation_offer WHERE reservation_offer.reservation_id = reservations.id) as reservation_offer_count')
        )->
        leftJoin('reservation_groundtable', 'reservation_groundtable.id', '=', 'reservations.ground_table_id')->
        leftJoin('grounds', 'grounds.id', '=', 'reservation_groundtable.ground_id')->
        leftJoin('provinces', 'provinces.id', '=', 'grounds.province_id')->
        leftJoin('team_teamseason', 'team_teamseason.id', '=', 'reservations.teamseason_id')->
        leftJoin('teams', 'teams.id', '=', 'team_teamseason.team_id');

        
        for ($i=0; $i < count($_REQUEST['columns']); $i++) {

            if(!empty($_REQUEST['columns'][$i]['search']['value'])){
                $reservations = $reservations->where($filter_by[$i], $_REQUEST['columns'][$i]['search']['value']);
                //$reservations = $reservations->where('provinces.id', '34');
            }
            //$reservations = $reservations->orderBy($order_by[$_REQUEST['order'][$i]['column']], $_REQUEST['order'][$i]['dir']);
        }
        $reservations = $reservations->whereNull('reservations.match_id');

        for ($i=0; $i < count($_REQUEST['order']); $i++) {
            $reservations = $reservations->orderBy($order_by[$_REQUEST['order'][$i]['column']], $_REQUEST['order'][$i]['dir']);
        }

        //RoleProvince
        $roleProvince = collect(Auth::user()->roleprovince)->pluck('province_id');
        if($roleProvince->count() > 0){
            $reservations = $reservations->whereIn('provinces.id', $roleProvince);
        }

        $reservations = $reservations->get();

        $iTotalRecords = $reservations->count();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);


        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        for ($i = $iDisplayStart; $i < $end; $i++) {

            $records["data"][] = array(
                $reservations[$i]->reservation_id,
                $reservations[$i]->province_name,
                $reservations[$i]->ground_name,
                Carbon::parse($reservations[$i]->reservation_groundtable_date)->format('d.m.Y'),//Carbon::parse($reservations[$i]->date)->format('d.m.Y'),
                Carbon::parse($reservations[$i]->reservation_groundtable_time)->format('H:i'),//Carbon::parse($reservations[$i]->time)->format('H:i'),
                $reservations[$i]->team_name,
                $reservations[$i]->reservation_offer_count,
                $reservations[$i]->status,
                ''
            );

        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);

    }

    public function getEliminationTreesAjax(Request $request)
    {
        //dd($request->input('treeId'));
        $searchArray = array();
        foreach ($_REQUEST['columns'] as $column) {
            $searchArray[] = $column['search']['value'];
        }

        $order_by = array('id', 'name', 'season', 'type', 'actions');
        $filter_by = array('id', 'name', 'season', 'type', 'actions');
        
        $elimination_tree = EliminationTree::orderBy($order_by[$_REQUEST['order'][0]['column']], $_REQUEST['order'][0]['dir']);

        if(!empty($request->input('treeId'))){
            $elimination_tree = $elimination_tree->where('season_id', $request->input('treeId'));
        }

        foreach ($searchArray as $key => $search) {
            if(!empty($search)){
                if($filter_by[$key] == 'name'){
                    $elimination_tree = $elimination_tree->where('name', 'ilike', '%'.$search.'%');
                }
            }
        }
        $elimination_tree = $elimination_tree->get();


        $iTotalRecords = $elimination_tree->count();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        for ($i = $iDisplayStart; $i < $end; $i++) {

            $season = $elimination_tree[$i]->league_eliminationseason->season_ptr;
            $season_name = $season->year.' '.$season->league->name.' '.$season->name;

            $type = '';
            if($elimination_tree[$i]->type == 'conference'){
                $type = 'Konferans';
            }elseif($elimination_tree[$i]->type == 'final'){
                $type = 'Final';
            }elseif($elimination_tree[$i]->type == 'trfinal'){
                $type = 'TR Final';
            }

            $records["data"][] = array(
                $elimination_tree[$i]->id,
                $elimination_tree[$i]->name,
                $season_name,
                $type,
                '',
            );
            
        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
        
    }

    public function getFixtureGroupAjax(Request $request)
    {
        //dd($request->input('treeId'));
        $searchArray = array();
        foreach ($_REQUEST['columns'] as $column) {
            $searchArray[] = $column['search']['value'];
        }

        $order_by = array('id', 'name', 'season', 'actions');
        $filter_by = array('id', 'name', 'season', 'actions');
        
        $fixture_group = Group::orderBy($order_by[$_REQUEST['order'][0]['column']], $_REQUEST['order'][0]['dir']);

        if(!empty($request->input('treeId'))){
            $fixture_group = $fixture_group->where('season_id', $request->input('treeId'));
        }

        foreach ($searchArray as $key => $search) {
            if(!empty($search)){
                if($filter_by[$key] == 'name'){
                    $fixture_group = $fixture_group->where('name', 'ilike', '%'.$search.'%');
                }
            }
        }
        $fixture_group = $fixture_group->get();


        $iTotalRecords = $fixture_group->count();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        for ($i = $iDisplayStart; $i < $end; $i++) {

            $season = $fixture_group[$i]->league_fixtureseason->season_ptr;
            $season_name = $season->year.' '.$season->league->name.' '.$season->name;

            $records["data"][] = array(
                $fixture_group[$i]->id,
                $fixture_group[$i]->name,
                $season_name,
                '',
            );
            
        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
        
    }

   public function getPlayerPositionsAjax()
    {
        
        $searchArray = array();
        foreach ($_REQUEST['columns'] as $column) {
            $searchArray[] = $column['search']['value'];
        }

        $order_by = array('id', 'name', 'code', 'order', 'actions');
        $filter_by = array('id', 'name', 'code', 'order', 'actions');
        
        $player_position = PlayerPosition::orderBy($order_by[$_REQUEST['order'][0]['column']], $_REQUEST['order'][0]['dir']);

        foreach ($searchArray as $key => $search) {
            if(!empty($search)){
                if($filter_by[$key] == 'name'){
                    $player_position = $player_position->where('name', 'ilike', '%'.$search.'%');
                }
            }
        }
        $player_position = $player_position->get();


        $iTotalRecords = $player_position->count();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        for ($i = $iDisplayStart; $i < $end; $i++) {

            $records["data"][] = array(
                $player_position[$i]->id,
                $player_position[$i]->name,
                $player_position[$i]->code,
                $player_position[$i]->order,
                '',
            );
            
        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
        
    }

    public function getPointTypesAjax()
    {
        
        $searchArray = array();
        foreach ($_REQUEST['columns'] as $column) {
            $searchArray[] = $column['search']['value'];
        }

        $order_by = array('id', 'name', 'min_point', 'max_point', 'win_score', 'defeat_score', 'draw_score', 'actions');
        $filter_by = array('id', 'name', 'min_point', 'max_point', 'win_score', 'defeat_score', 'draw_score', 'actions');
        
        $point_type = PointType::orderBy($order_by[$_REQUEST['order'][0]['column']], $_REQUEST['order'][0]['dir']);

        foreach ($searchArray as $key => $search) {
            if(!empty($search)){
                if($filter_by[$key] == 'name'){
                    $point_type = $point_type->where('name', 'ilike', '%'.$search.'%');
                }
            }
        }
        $point_type = $point_type->get();

        $iTotalRecords = $point_type->count();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        for ($i = $iDisplayStart; $i < $end; $i++) {

            $records["data"][] = array(
                $point_type[$i]->id,
                $point_type[$i]->name,
                $point_type[$i]->min_point,
                $point_type[$i]->max_point,
                $point_type[$i]->win_score,
                $point_type[$i]->defeat_score,
                $point_type[$i]->draw_score,
                '',
            );
            
        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
        
    }

    public function getTacticsAjax()
    {

        $searchArray = array();
        foreach ($_REQUEST['columns'] as $column) {
            $searchArray[] = $column['search']['value'];
        }

        $order_by = array('id', 'name', 'actions');
        $filter_by = array('id', 'name', 'actions');
        
        $tactic = Tactic::orderBy($order_by[$_REQUEST['order'][0]['column']], $_REQUEST['order'][0]['dir']);

        foreach ($searchArray as $key => $search) {
            if(!empty($search)){
                if($filter_by[$key] == 'name'){
                    $tactic = $tactic->where('name', 'ilike', '%'.$search.'%');
                }
            }
        }

        $tactic = $tactic->get();

        $iTotalRecords = $tactic->count();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        for ($i = $iDisplayStart; $i < $end; $i++) {

            $records["data"][] = array(
                $tactic[$i]->id,
                $tactic[$i]->name,
                '',
            );
            
        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
        
    }

    public function getTeamPotentialsAjax()
    {

        $searchArray = array();
        foreach ($_REQUEST['columns'] as $column) {
            $searchArray[] = $column['search']['value'];
        }

        $order_by = array('id', 'name', 'actions');
        $filter_by = array('id', 'name', 'actions');
        
        $team_potential = TeamPotential::orderBy($order_by[$_REQUEST['order'][0]['column']], $_REQUEST['order'][0]['dir']);

        foreach ($searchArray as $key => $search) {
            if(!empty($search)){
                if($filter_by[$key] == 'name'){
                    $team_potential = $team_potential->where('name', 'ilike', '%'.$search.'%');
                }
            }
        }
        $team_potential = $team_potential->get();

        $iTotalRecords = $team_potential->count();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        for ($i = $iDisplayStart; $i < $end; $i++) {

            $records["data"][] = array(
                $team_potential[$i]->id,
                $team_potential[$i]->name,
                '',
            );
            
        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
        
    }

    public function getTeamSatisfactionsAjax()
    {

        $searchArray = array();
        foreach ($_REQUEST['columns'] as $column) {
            $searchArray[] = $column['search']['value'];
        }

        $order_by = array('id', 'name', 'actions');
        $filter_by = array('id', 'name', 'actions');
        
        $team_satisfaction = TeamSatisfaction::orderBy($order_by[$_REQUEST['order'][0]['column']], $_REQUEST['order'][0]['dir']);

        foreach ($searchArray as $key => $search) {
            if(!empty($search)){
                if($filter_by[$key] == 'name'){
                    $team_satisfaction = $team_satisfaction->where('name', 'ilike', '%'.$search.'%');
                }
            }
        }
        $team_satisfaction = $team_satisfaction->get();

        $iTotalRecords = $team_satisfaction->count();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        for ($i = $iDisplayStart; $i < $end; $i++) {

            $records["data"][] = array(
                $team_satisfaction[$i]->id,
                $team_satisfaction[$i]->name,
                '',
            );
            
        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    public function getTeamSatisfactionsJogoAjax()
    {

        $searchArray = array();
        foreach ($_REQUEST['columns'] as $column) {
            $searchArray[] = $column['search']['value'];
        }

        $order_by = array('id', 'name', 'actions');
        $filter_by = array('id', 'name', 'actions');
        
        $team_satisfaction_jogo = TeamSatisfactionJogo::orderBy($order_by[$_REQUEST['order'][0]['column']], $_REQUEST['order'][0]['dir']);

        foreach ($searchArray as $key => $search) {
            if(!empty($search)){
                if($filter_by[$key] == 'name'){
                    $team_satisfaction_jogo = $team_satisfaction_jogo->where('name', 'ilike', '%'.$search.'%');
                }
            }
        }
        $team_satisfaction_jogo = $team_satisfaction_jogo->get();

        $iTotalRecords = $team_satisfaction_jogo->count();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        for ($i = $iDisplayStart; $i < $end; $i++) {

            $records["data"][] = array(
                $team_satisfaction_jogo[$i]->id,
                $team_satisfaction_jogo[$i]->name,
                '',
            );
            
        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    public function getBannersAjax()
    {

        $searchArray = array();
        foreach ($_REQUEST['columns'] as $column) {
            $searchArray[] = $column['search']['value'];
        }

        $order_by = array('id', 'name', 'actions');
        $filter_by = array('id', 'name', 'actions');
        
        $banners = Banner::orderBy($order_by[$_REQUEST['order'][0]['column']], $_REQUEST['order'][0]['dir']);

        foreach ($searchArray as $key => $search) {
            if(!empty($search)){
                if($filter_by[$key] == 'name'){
                    $banners = $banners->where('name', 'ilike', '%'.$search.'%');
                }
            }
        }
        $banners = $banners->get();

        $iTotalRecords = $banners->count();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        for ($i = $iDisplayStart; $i < $end; $i++) {

            $records["data"][] = array(
                $banners[$i]->id,
                $banners[$i]->name,
                '',
            );
            
        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    public function getPenaltyTypesAjax()
    {

        $searchArray = array();
        foreach ($_REQUEST['columns'] as $column) {
            $searchArray[] = $column['search']['value'];
        }

        $order_by = array('id', 'name', 'to_who', 'actions');
        $filter_by = array('id', 'name', 'to_who', 'actions');
        
        $penalty_types = PenaltyTypes::orderBy($order_by[$_REQUEST['order'][0]['column']], $_REQUEST['order'][0]['dir']);

        foreach ($searchArray as $key => $search) {
            if(!empty($search)){
                if($filter_by[$key] == 'name'){
                    $penalty_types = $penalty_types->where('name', 'ilike', '%'.$search.'%');
                }
            }
        }

        $penalty_types = $penalty_types->get();

        $iTotalRecords = $penalty_types->count();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        for ($i = $iDisplayStart; $i < $end; $i++) {

            $records["data"][] = array(
                $penalty_types[$i]->id,
                $penalty_types[$i]->name,
                $penalty_types[$i]->to_who,
                '',
            );
            
        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    public function getPenaltiesAjax()
    {

        $searchArray = array();
        foreach ($_REQUEST['columns'] as $column) {
            $searchArray[] = $column['search']['value'];
        }

        $order_by = array('id', 'season', 'match', 'team', 'player', 'polymorphic_ctype_id', 'type', 'actions');
        $filter_by = array('id', 'season', 'match', 'team', 'player', 'polymorphic_ctype_id', 'type', 'actions');
        
        $penalty = Penalty::orderBy($order_by[$_REQUEST['order'][0]['column']], $_REQUEST['order'][0]['dir']);

        foreach ($searchArray as $key => $search) {
            if(!empty($search)){
                if($filter_by[$key] == 'name'){
                    $penalty = $penalty->where('name', 'ilike', '%'.$search.'%');
                }
            }
        }

        $penalty = $penalty->get();

        $iTotalRecords = $penalty->count();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        for ($i = $iDisplayStart; $i < $end; $i++) {

            if($penalty[$i]->polymorphic_ctype_id == 44){
                $penType = PenaltyTypes::find($penalty[$i]->player_penalty->penal_type_id)->name;
                $toWho = PenaltyTypes::find($penalty[$i]->player_penalty->penal_type_id)->to_who;
                if(!empty($penalty[$i]->player_penalty->team_id)){
                    $penTeam = Team::find($penalty[$i]->player_penalty->team_id)->name;
                }else{
                    $penTeam = '';
                }
                if(!empty($penalty[$i]->player_penalty->player_id)){
                    $penPlayer = Player::find($penalty[$i]->player_penalty->player_id)->user->first_name .' '. Player::find($penalty[$i]->player_penalty->player_id)->user->last_name;  
                }else{
                    $penPlayer = $penalty[$i]->player_penalty->guest_name;
                }

            }else if($penalty[$i]->polymorphic_ctype_id == 47){
                $penType = PenaltyTypes::find($penalty[$i]->team_penalty->penal_type_id)->name;
                $toWho = PenaltyTypes::find($penalty[$i]->team_penalty->penal_type_id)->to_who;
                $penTeam = Team::find($penalty[$i]->team_penalty->team_id)->name;
                $penPlayer = '';
            }

        
            $records["data"][] = array(
                $penalty[$i]->id,
                $penalty[$i]->season->year.' '.$penalty[$i]->league->name.' '.$penalty[$i]->season->name,
                $penalty[$i]->match->team1->name.' - '.$penalty[$i]->match->team2->name,
                $penTeam,
                $penPlayer,
                $penType,
                $toWho,
                '',
            );
            


        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    public function getPanoramaTypesAjax()
    {

        $searchArray = array();
        foreach ($_REQUEST['columns'] as $column) {
            $searchArray[] = $column['search']['value'];
        }

        $order_by = array('id', 'name', 'to_who', 'season_id', 'actions');
        $filter_by = array('id', 'name', 'to_who', 'season_id', 'actions');
        
        $panorama_types = PanoramaType::orderBy($order_by[$_REQUEST['order'][0]['column']], $_REQUEST['order'][0]['dir']);

        foreach ($searchArray as $key => $search) {
            if(!empty($search)){
                if($filter_by[$key] == 'name'){
                    $panorama_types = $panorama_types->where('name', 'ilike', '%'.$search.'%');
                }
            }
        }

        $panorama_types = $panorama_types->get();

        $iTotalRecords = $panorama_types->count();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        for ($i = $iDisplayStart; $i < $end; $i++) {
            $season = Season::find($panorama_types[$i]->season_id);
            $season_name = '';
            if(!empty($season)){
                $season_name = $season->year.' '.$season->league->name.' '.$season->name;
            }
       
            $records["data"][] = array(
                $panorama_types[$i]->id,
                $panorama_types[$i]->name,
                $panorama_types[$i]->to_who,
                $panorama_types[$i]->panorama_types_prize->period,
                $season_name,
                '',
            );
            
        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    public function getPanoramasAjax()
    {

        $searchArray = array();
        foreach ($_REQUEST['columns'] as $column) {
            $searchArray[] = $column['search']['value'];
        }

        $order_by = array('id', 'season', 'to_who', 'type', 'period', 'team-player', 'actions');
        $filter_by = array('id', 'season', 'to_who', 'type', 'period', 'team-player', 'actions');
        
        $panorama = MounthlyPanorama::orderBy($order_by[$_REQUEST['order'][0]['column']], $_REQUEST['order'][0]['dir']);

        foreach ($searchArray as $key => $search) {
            if(!empty($search)){
                if($filter_by[$key] == 'name'){
                    $panorama = $panorama->where('name', 'ilike', '%'.$search.'%');
                }
            }
        }

        $panorama = $panorama->get();

        $iTotalRecords = $panorama->count();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        for ($i = $iDisplayStart; $i < $end; $i++) {

            $seasonName = '';
            if(!empty($panorama[$i]->season_id)){
                $season = Season::find($panorama[$i]->season_id);
                $seasonName = $season->year.' '.$season->league->name.' '.$season->name;
            }

            $teamName = '';
            if(!empty($panorama[$i]->team_id)){
                $team = Team::find($panorama[$i]->team_id);
                $teamName = $team->name;
            }

            $playerName = '';
            if(!empty($panorama[$i]->player_id)){
                $player = Player::find($panorama[$i]->player_id);
                $playerName = $player->user->first_name.' '.$player->user->last_name;
            }
        
            $records["data"][] = array(
                $panorama[$i]->id,
                $seasonName,
                $panorama[$i]->panorama_type->to_who,
                $panorama[$i]->panorama_type->name,
                $panorama[$i]->panorama_type->panorama_types_prize->period,
                '('.$teamName . ') ' .$playerName,
                ''
            );

        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }



    public function getNewsAjax()
    {

        $searchArray = array();
        foreach ($_REQUEST['columns'] as $column) {
            $searchArray[] = $column['search']['value'];
        }

        $order_by = array('id', 'name', 'actions');
        $filter_by = array('id', 'name', 'actions');
        
        $news = News::orderBy($order_by[$_REQUEST['order'][0]['column']], $_REQUEST['order'][0]['dir']);

        foreach ($searchArray as $key => $search) {
            if(!empty($search)){
                if($filter_by[$key] == 'name'){
                    $news = $news->where('name', 'ilike', '%'.$search.'%');
                }
            }
        }

        $news = $news->get();

        $iTotalRecords = $news->count();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        for ($i = $iDisplayStart; $i < $end; $i++) {

            $records["data"][] = array(
                $news[$i]->id,
                $news[$i]->name,
                $news[$i]->order,
                '',
            );
            
        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    public function getNewsTypesAjax()
    {

        $searchArray = array();
        foreach ($_REQUEST['columns'] as $column) {
            $searchArray[] = $column['search']['value'];
        }

        $order_by = array('id', 'name', 'order', 'actions');
        $filter_by = array('id', 'name', 'order', 'actions');
        
        $news_types = NewsTypes::orderBy($order_by[$_REQUEST['order'][0]['column']], $_REQUEST['order'][0]['dir']);

        foreach ($searchArray as $key => $search) {
            if(!empty($search)){
                if($filter_by[$key] == 'name'){
                    $news_types = $news_types->where('name', 'ilike', '%'.$search.'%');
                }
            }
        }

        $news_types = $news_types->get();

        $iTotalRecords = $news_types->count();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        for ($i = $iDisplayStart; $i < $end; $i++) {

            $records["data"][] = array(
                $news_types[$i]->id,
                $news_types[$i]->name,
                $news_types[$i]->order,
                '',
            );
            
        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    public function getAgendaAjax()
    {

        $searchArray = array();
        foreach ($_REQUEST['columns'] as $column) {
            $searchArray[] = $column['search']['value'];
        }

        $order_by = array('id', 'name', 'link', 'actions');
        $filter_by = array('id', 'name', 'link', 'actions');
        
        $agenda = Agenda::orderBy($order_by[$_REQUEST['order'][0]['column']], $_REQUEST['order'][0]['dir']);

        foreach ($searchArray as $key => $search) {
            if(!empty($search)){
                if($filter_by[$key] == 'name'){
                    $agenda = $agenda->where('name', 'ilike', '%'.$search.'%');
                }
            }
        }

        $agenda = $agenda->get();

        $iTotalRecords = $agenda->count();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        for ($i = $iDisplayStart; $i < $end; $i++) {

            $records["data"][] = array(
                $agenda[$i]->id,
                $agenda[$i]->name,
                $agenda[$i]->link,
                $agenda[$i]->order,
                '',
            );
            
        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    public function getTagsAjax()
    {

        $searchArray = array();
        foreach ($_REQUEST['columns'] as $column) {
            $searchArray[] = $column['search']['value'];
        }

        $order_by = array('id', 'name', 'order', 'actions');
        $filter_by = array('id', 'name', 'order', 'actions');
        
        $tags = Tag::orderBy($order_by[$_REQUEST['order'][0]['column']], $_REQUEST['order'][0]['dir']);

        foreach ($searchArray as $key => $search) {
            if(!empty($search)){
                if($filter_by[$key] == 'name'){
                    $tags = $tags->where('name', 'ilike', '%'.$search.'%');
                }
            }
        }

        $tags = $tags->get();

        $iTotalRecords = $tags->count();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        for ($i = $iDisplayStart; $i < $end; $i++) {

            $records["data"][] = array(
                $tags[$i]->id,
                $tags[$i]->name,
                '',
            );
            
        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    public function getRulesAjax()
    {

        $searchArray = array();
        foreach ($_REQUEST['columns'] as $column) {
            $searchArray[] = $column['search']['value'];
        }

        $order_by = array('id', 'name', 'order', 'actions');
        $filter_by = array('id', 'name', 'order', 'actions');
        
        $rules = Rules::orderBy($order_by[$_REQUEST['order'][0]['column']], $_REQUEST['order'][0]['dir']);

        foreach ($searchArray as $key => $search) {
            if(!empty($search)){
                if($filter_by[$key] == 'name'){
                    $rules = $rules->where('name', 'ilike', '%'.$search.'%');
                }
            }
        }

        $rules = $rules->get();

        $iTotalRecords = $rules->count();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        for ($i = $iDisplayStart; $i < $end; $i++) {

            $records["data"][] = array(
                $rules[$i]->id,
                $rules[$i]->name,
                $rules[$i]->order,
                '',
            );
            
        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

 
    /************************************/

    public function getDistrictsPAjax($pid)
    {
        $result = array();

        $districts = District::where('slug', 'like', '%'. str_slug($_REQUEST['q'], '-') .'%');
        if(!empty($pid)){
            $districts = $districts->where('province_id', $pid);
        }
        $districts = $districts->get();

        $result['total_count'] = $districts->count();
        $result['incomplete_results'] = true;
        $result['items'] = $districts;

        echo json_encode($result);        
    }

    public function getDistrictsFAjax($pid)
    {
        $result = array();
        if(!empty($pid)){
            $districts = District::where('province_id', $pid)->get();
        }

        $result['total_count'] = $districts->count();
        $result['incomplete_results'] = true;
        $result['items'] = $districts;

        echo json_encode($result);
    }

    public function getUsersFAjax($search)
    {
        $result = array();

        if(!empty($search)){
            $users = User::
            select(
                'users.first_name',
                'users.last_name',
                'players.id as player_id',
                'teams.name as team_name',
                'profiles.identity_number as identity'
            )->
            
            leftJoin('players', 'users.id', '=', 'players.user_id')->
            leftJoin('teams', 'teams.id', '=', 'players.team_id')->
            leftJoin('profiles', 'users.id', '=', 'profiles.user_id')->

            /*
            where(function ($query) use ($search) {
                $query->where('users.first_name', 'ilike', '%'.$search.'%')->
                orWhere('users.last_name', 'ilike', '%'.$search.'%')->
                orWhere('profiles.identity_number', 'ilike', '%'.$search.'%');
            })->
            */

            where(function ($query) use ($search) {
               $query->where(DB::raw("REPLACE(CONCAT(users.first_name, ' ', users.last_name),'  ',' ')"), 'ilike', '%'.$search.'%')->
                orWhere('profiles.identity_number', 'ilike', '%'.$search.'%');
            })->
            
            whereNotNull('players.id')->
            whereNull('teams.name')->
            limit(20)->get();
        }

        foreach ($users as $user) {
            $result['items'][] = array('id' => $user->player_id, 'text' => '(TC:'.$user->identity.') '.$user->first_name.' '.$user->last_name);
        }
        
        echo json_encode($result);
    }

    public function getUsersCAjax($search)
    {
        $result = array();

        if(!empty($search)){
            $users = User::
            select(
                'users.first_name',
                'users.last_name',
                'players.id as player_id',
                'teams.name as team_name',
                'profiles.identity_number as identity'
            )->
            
            leftJoin('players', 'users.id', '=', 'players.user_id')->
            leftJoin('teams', 'teams.id', '=', 'players.team_id')->
            leftJoin('profiles', 'users.id', '=', 'profiles.user_id')->

            where(function ($query) use ($search) {
                $query->where(DB::raw("REPLACE(CONCAT(users.first_name, ' ', users.last_name),'  ',' ')"), 'ilike', '%'.$search.'%')->
                orWhere('profiles.identity_number', 'ilike', '%'.$search.'%');
            })->
            
            whereNotNull('players.id')->
            whereNotNull('teams.name')->
            limit(20)->get();
        }

        foreach ($users as $user) {
            $result['items'][] = array('id' => $user->player_id, 'text' => '(TC:'.$user->identity.') '.$user->first_name.' '.$user->last_name);
        }
        
        echo json_encode($result);
    }

    public function getUsersReAjax($sid, $search = null)
    {

        $league_id = Season::find($sid)->league->id;
        
        $result = array();

        $users = User::
        select(
            'users.first_name',
            'users.last_name',
            'referees.id as referees_id_'
        )->
        leftJoin('referees', 'referees.user_id', '=', 'users.id')->
        leftJoin('referees_leagues', 'referees_leagues.referee_id', '=', 'referees.id');

        if(!empty($search)){
            $users = $users->where(function ($query) use ($search) {
                $query->where('users.first_name', 'ilike', '%'.$search.'%')->
                orWhere('users.last_name', 'ilike', '%'.$search.'%');
            });
        }
        
        $users = $users->whereNotNull('referees.id')->
        where('referees_leagues.league_id', '=', $league_id)->
        limit(20)->get();
        
        foreach ($users as $user) {
            $result['items'][] = array('id' => $user->referees_id_, 'text' => $user->first_name.' '.$user->last_name);
        }
        
        echo json_encode($result);
    }

    public function getUsersCoAjax($sid, $search = null)
    {
        $league_id = Season::find($sid)->league->id;
        
        $result = array();

        $users = User::
        select(
            'users.first_name',
            'users.last_name',
            'coordinators.id as coordinators_id_'
        )->
        leftJoin('coordinators', 'coordinators.user_id', '=', 'users.id')->
        leftJoin('coordinators_leagues', 'coordinators_leagues.coordinator_id', '=', 'coordinators.id');

        if(!empty($search)){
            $users = $users->where(function ($query) use ($search) {
                $query->where('users.first_name', 'ilike', '%'.$search.'%')->
                orWhere('users.last_name', 'ilike', '%'.$search.'%');
            });
        }
        
        $users = $users->whereNotNull('coordinators.id')->
        where('coordinators_leagues.league_id', '=', $league_id)->
        limit(20)->get();
        
        foreach ($users as $user) {
            $result['items'][] = array('id' => $user->coordinators_id_, 'text' => $user->first_name.' '.$user->last_name);
        }
        
        echo json_encode($result);
    }

    public function getSeasonsFAjax($search = null)
    {
        $result = array();

        $seasons = Season::
        select( 'seasons.id',
                'seasons.name',
                'seasons.year',
                'leagues.name as league_name',
                'leagues.id as league_id',
                DB::raw("count(leagues_provinces.province_id) as count")
        )->
        leftJoin('leagues', 'leagues.id', '=', 'seasons.league_id')->
        leftJoin('leagues_provinces', 'leagues.id', '=', 'leagues_provinces.league_id')->
        where('end_date', '>=', Carbon::now())->
        where('polymorphic_ctype_id', '!=', 32);
        
        if(!empty($search)){
            $seasons = $seasons->where(function ($query) use ($search) {
                $query->where('seasons.name', 'ilike', '%'.$search.'%')->
                orWhere('leagues.name', 'ilike', '%'.$search.'%');
            });
        }
        
        //RoleProvince
        $roleProvince = collect(Auth::user()->roleprovince)->pluck('province_id');
        if($roleProvince->count() > 0){
            $seasons = $seasons->whereIn('leagues_provinces.province_id', $roleProvince);
        }

        $seasons = $seasons->orderBy('end_date', 'desc')->groupBy('seasons.id', 'leagues.id', 'leagues.name')->get();
        
        foreach ($seasons as $season) {
            $result['items'][] = array('id' => $season->id, 'text' => $season->league_name.' '.$season->year.' '.$season->name);
        }
        
        echo json_encode($result);
    }

    public function getSeasonsFAllAjax($search = null)
    {
        $result = array();

        $seasons = Season::
        select( 'seasons.id',
                'seasons.name',
                'seasons.year',
                'leagues.name as league_name',
                'leagues.id as league_id',
                DB::raw("count(leagues_provinces.province_id) as count")
        )->
        leftJoin('leagues', 'leagues.id', '=', 'seasons.league_id')->
        leftJoin('leagues_provinces', 'leagues.id', '=', 'leagues_provinces.league_id')->
        where('polymorphic_ctype_id', '!=', 32);
        
        if(!empty($search)){
            $seasons = $seasons->where(function ($query) use ($search) {
                $query->where('seasons.name', 'ilike', '%'.$search.'%')->
                orWhere('leagues.name', 'ilike', '%'.$search.'%');
            });
        }
        
        //RoleProvince
        $roleProvince = collect(Auth::user()->roleprovince)->pluck('province_id');
        if($roleProvince->count() > 0){
            $seasons = $seasons->whereIn('leagues_provinces.province_id', $roleProvince);
        }

        $seasons = $seasons->orderBy('end_date', 'desc')->groupBy('seasons.id', 'leagues.id', 'leagues.name')->get();
        
        foreach ($seasons as $season) {
            $result['items'][] = array('id' => $season->id, 'text' => $season->league_name.' '.$season->year.' '.$season->name);
        }
        
        echo json_encode($result);
    }


    public function getSeasonsLAjax($lid)
    {
        if(!empty($lid)){
            $result = array();

            $seasons = Season::
            select( 'seasons.id',
                    'seasons.name',
                    'seasons.year',
                    'leagues.name as league_name',
                    'leagues.id as league_id'
            )->
            leftJoin('leagues', 'leagues.id', '=', 'seasons.league_id')->
            where('end_date', '>=', Carbon::now())->
            //where('polymorphic_ctype_id', '!=', 32);
            where('league_id', $lid)->orderBy('end_date', 'desc')->get();
        }

        $result['total_count'] = $seasons->count();
        $result['incomplete_results'] = true;

        foreach ($seasons as $season) {
            $result['items'][] = array('id' => $season->id, 'text' => $season->league_name.' '.$season->year.' '.$season->name);
        }
        
        echo json_encode($result);
    }

    public function getSeasonsLAllAjax($lid)
    {
        if(!empty($lid)){
            $result = array();

            $seasons = Season::
            select( 'seasons.id',
                    'seasons.name',
                    'seasons.year',
                    'leagues.name as league_name',
                    'leagues.id as league_id'
            )->
            leftJoin('leagues', 'leagues.id', '=', 'seasons.league_id')->
            //where('end_date', '>=', Carbon::now())->
            //where('polymorphic_ctype_id', '!=', 32);
            where('league_id', $lid)->orderBy('end_date', 'desc')->get();
        }

        $result['total_count'] = $seasons->count();
        $result['incomplete_results'] = true;

        foreach ($seasons as $season) {
            $result['items'][] = array('id' => $season->id, 'text' => $season->league_name.' '.$season->year.' '.$season->name);
        }
        
        echo json_encode($result);
    }

    public function getSeasonsPanormaAllAjax($pid)
    {
        if(!empty($pid)){
            $result = array();

            $panorama_types = PanoramaType::find($pid);
            $season_id = $panorama_types->season_id;

            $seasons = Season::
            select( 'seasons.id',
                    'seasons.name',
                    'seasons.year',
                    'leagues.name as league_name',
                    'leagues.id as league_id'
            )->
            leftJoin('leagues', 'leagues.id', '=', 'seasons.league_id');

            if(!empty($season_id)){
                $seasons = $seasons->where('seasons.id', $season_id);
            }
            
            $seasons = $seasons->orderBy('end_date', 'desc')->get();
        }

        $result['total_count'] = $seasons->count();
        $result['incomplete_results'] = true;

        foreach ($seasons as $season) {
            $result['items'][] = array('id' => $season->id, 'text' => $season->league_name.' '.$season->year.' '.$season->name);
        }
        
        echo json_encode($result);
    }

    public function getSeasonsEAjax($lid)
    {
        if(!empty($lid)){
            $result = array();

            $seasons = Season::
            select( 'seasons.id',
                    'seasons.name',
                    'seasons.year',
                    'leagues.name as league_name',
                    'leagues.id as league_id'
            )->
            leftJoin('leagues', 'leagues.id', '=', 'seasons.league_id')->
            //where('end_date', '>=', Carbon::now())->
            where('polymorphic_ctype_id', '!=', 32)->
            where('league_id', $lid)->orderBy('end_date', 'desc')->get();
        }

        $result['total_count'] = $seasons->count();
        $result['incomplete_results'] = true;

        foreach ($seasons as $season) {
            $result['items'][] = array('id' => $season->id, 'text' => $season->league_name.' '.$season->year.' '.$season->name);
        }
        
        echo json_encode($result);
    }

    public function getSeasonsE2Ajax($lid)
    {
        if(!empty($lid)){
            $result = array();

            $seasons = Season::
            select( 'seasons.id',
                    'seasons.name',
                    'seasons.year',
                    'leagues.name as league_name',
                    'leagues.id as league_id'
            )->
            leftJoin('leagues', 'leagues.id', '=', 'seasons.league_id')->
            //where('end_date', '>=', Carbon::now())->
            where('polymorphic_ctype_id', 32)->
            where('league_id', $lid)->orderBy('end_date', 'desc')->get();
        }

        $result['total_count'] = $seasons->count();
        $result['incomplete_results'] = true;

        foreach ($seasons as $season) {
            $result['items'][] = array('id' => $season->id, 'text' => $season->year.' '.$season->league_name.' '.$season->name);
        }
        
        echo json_encode($result);
    }

    public function getSeasonMatchesAjax($sid, $search = null)
    {

        $result = array();
        $matches = Match::select(
            'matches.id',
            'matches.season_id as season_id',
            //DB::raw("case when matches.season_id is null then '' else CONCAT(seasons.year, ' ', leagues.name, ' ', seasons.name) end as season_name"),
            //'matches.team1_id',
            //'matches.team1_goal',
            't1.name as team1_name',
            //'matches.team2_id',
            //'matches.team2_goal',
            't2.name as team2_name',
            'matches.date',
            //'matches.time',
            //'matches.completed',
            //'matches.completed_status',
            DB::raw("CONCAT('(', matches.date, ') ', t1.name, ' - ', t2.name) as match_name")
        )->
        leftJoin('teams as t1', 't1.id', '=', 'matches.team1_id')->
        leftJoin('teams as t2', 't2.id', '=', 'matches.team2_id');

        $matches = $matches->where(function ($query) use ($search) {
            $query->where('t1.name', 'ilike', '%'.$search.'%')->
            orWhere('t2.name', 'ilike', '%'.$search.'%');
        });

        $matches = $matches->where('season_id', $sid)->get();

        foreach ($matches as $match) {
            $result['items'][] = array('id' => $match->id, 'text' => $match->match_name);
        }

        echo json_encode($result);
    }

    public function getPlayerPenaltyTypesSAjax()
    {

        $result = array();
        $penalty_types = PenaltyTypes::where('to_who', 'player')->get();
        

        foreach ($penalty_types as $penalty_type) {
            $result['items'][] = array('id' => $penalty_type->id, 'text' => $penalty_type->name);
        }

        echo json_encode($result);
    }

    public function getTeamPenaltyTypesSAjax()
    {

        $result = array();
        $penalty_types = PenaltyTypes::where('to_who', 'team')->get();
        

        foreach ($penalty_types as $penalty_type) {
            $result['items'][] = array('id' => $penalty_type->id, 'text' => $penalty_type->name);
        }

        echo json_encode($result);
    }

    public function getTeamsFAjax($sid, $search = null)
    {

        $result = array();
        $seasonTeams = TeamTeamSeason::
        select(
            'team_teamseason.id',
            'team_teamseason.team_id',
            'teams.name as team_name'
        )->
        leftJoin('teams', 'teams.id', '=', 'team_teamseason.team_id');

        if($sid > 0){
            $seasonTeams = $seasonTeams->where('team_teamseason.season_id', $sid);
        }

        if(!empty($search)){
            $seasonTeams = $seasonTeams->where('teams.name', 'ilike', '%'.$search.'%');
        }

        $seasonTeams = $seasonTeams->limit(100)->get();

        foreach ($seasonTeams as $team) {
            $result['items'][] = array('id' => $team->team_id, 'text' => $team->team_name);
        }

        echo json_encode($result);
    }

    public function getTeamsRAjax($sid, $search = null)
    {
        $__provinces = null;
        $provinces = Season::find($sid)->league->league_province;

        if(!empty($provinces)){
            foreach ($provinces as $province) {
                $__provinces[] = $province->province_id;
            }   
        }

        $teams = Team::whereIn('province_id', $__provinces)->where('status', 'not like', 'passive');

        if(!empty($search)){
            $teams = $teams->where('name', 'ilike', '%'.$search.'%');
        }
        
        $teams = $teams->orderBy('name', 'asc')->get();

        $result = array();
        foreach ($teams as $team) {
            $result['items'][] = array('id' => $team->id, 'text' => $team->name, 's' => $team->status);
        }

        echo json_encode($result);
    }

    public function getTeamsSAjax($sid, $search = null)
    {   

        // Seçilen Elemeli Sezonun Base Sezonundaki takımları getir
        $result = array();

        $season = Season::find($sid);
        $seasonId = $season->league_eliminationseason->base_season_id;
        
        $seasonTeams = TeamTeamSeason::
        select(
            'team_teamseason.id',
            'team_teamseason.team_id',
            'teams.name as team_name'
        )->
        leftJoin('teams', 'teams.id', '=', 'team_teamseason.team_id');

        if($sid > 0){
            $seasonTeams = $seasonTeams->where('team_teamseason.season_id', $seasonId);
        }

        if(!empty($search)){
            $seasonTeams = $seasonTeams->where('teams.name', 'ilike', '%'.$search.'%');
        }
        $seasonTeams = $seasonTeams->where('team_teamseason.playoff_member', 'true');
        $seasonTeams = $seasonTeams->limit(100)->get();

        foreach ($seasonTeams as $team) {
            $result['items'][] = array('id' => $team->team_id, 'text' => $team->team_name);
        }

        echo json_encode($result);



        
        /*
        $__provinces = null;
        $provinces = Season::find($sid)->league->league_province;

        if(!empty($provinces)){
            foreach ($provinces as $province) {
                $__provinces[] = $province->province_id;
            }   
        }

        $teams = Team::whereIn('province_id', $__provinces);

        if(!empty($search)){
            $teams = $teams->where('name', 'ilike', '%'.$search.'%');
        }
        
        $teams = $teams->orderBy('name', 'asc')->get();

        foreach ($teams as $team) {
            $result['items'][] = array('id' => $team->id, 'text' => $team->name);
        }
        */
        //echo json_encode($result);
    }

    public function getTeamsAllSAjax($search = null)
    {   

        // Seçilen Elemeli Sezonun Base Sezonundaki takımları getir
        $result = array();

        $teams = Team::where('status', 'active');

        if(!empty($search)){
            $teams = $teams->where('name', 'ilike', '%'.$search.'%');
        }
        
        $teams = $teams->limit(20)->get();

        foreach ($teams as $team) {
            $province_name = $team->province->name;
            $result['items'][] = array('id' => $team->id, 'text' => $team->name.' ('.$province_name.')');
        }

        echo json_encode($result);
    }

    public function getPanoramaTeamsAllSAjax($sid, $search = null)
    {   

        // Seçilen Elemeli Sezonun Base Sezonundaki takımları getir
        $result = array();

        //$panorama_types = PanoramaType::find($pid);
        //$season_id = $sid;

        $teams = TeamTeamSeason::
        select(
            'team_teamseason.id',
            'team_teamseason.team_id',
            'teams.name as team_name'
        )->
        leftJoin('teams', 'teams.id', '=', 'team_teamseason.team_id');

        if(!empty($sid)){
            $teams = $teams->where('team_teamseason.season_id', $sid);
        }

        if(!empty($search)){
            $teams = $teams->where('teams.name', 'ilike', '%'.$search.'%');
        }

        $teams = $teams->limit(10)->get();

        foreach ($teams as $team) {
            $province_name = $team->team->province->name;
            $result['items'][] = array('id' => $team->team_id, 'text' => $team->team_name.' ('.$province_name.')');
        }

        echo json_encode($result);
    }

    public function getGroundsFAjax($sid, $search)
    {

        $__provinces = null;
        $provinces = Season::find($sid)->league->league_province;

        if(!empty($provinces)){
            foreach ($provinces as $province) {
                $__provinces[] = $province->province_id;
            }   
        }

        $result = array();

        $grounds = Ground::whereIn('grounds.province_id', $__provinces);
        $grounds = $grounds->where('grounds.name', 'ilike', '%'.$search.'%')->limit(100)->get();

        foreach ($grounds as $ground) {
            $result['items'][] = array('id' => $ground->id, 'text' => $ground->name);
        }

        echo json_encode($result);
    }

    public function getGroundsReAjax($sid, $search = null)
    {

        $__provinces = null;
        $provinces = Season::find($sid)->league->league_province;

        if(!empty($provinces)){
            foreach ($provinces as $province) {
                $__provinces[] = $province->province_id;
            }   
        }
        
        $result = array();

        $grounds = Ground::whereIn('grounds.province_id', $__provinces);
        if(!empty($search)){
            $grounds = $grounds->where('grounds.name', 'ilike', '%'.$search.'%');
        }
        $grounds = $grounds->orderBy('name', 'asc')->get();

        foreach ($grounds as $ground) {
            $result['items'][] = array('id' => $ground->id, 'text' => $ground->name);
        }

        echo json_encode($result);
    }

    public function getGroundsPAjax($search)
    {

        $result['items'] = array();
        $grounds = Ground::select('*');
        if(!empty($search) && $search != 'undefined'){
            $grounds = $grounds->where('grounds.name', 'ilike', '%'.$search.'%');
        }
        if(!empty($_GET['province'])){
            $grounds = $grounds->where('grounds.province_id', $_GET['province']);
        }

        //RoleProvince
        $roleProvince = collect(Auth::user()->roleprovince)->pluck('province_id');
        if($roleProvince->count() > 0){
            $grounds = $grounds->whereIn('grounds.province_id', $roleProvince);
        }

        $grounds = $grounds->get();

        foreach ($grounds as $ground) {
            $result['items'][] = array('id' => $ground->id, 'text' => $ground->name);
        }

        echo json_encode($result);
    }

    public function getGroundHourFAjax()
    {

        $result['items'] = array();
        if(!empty($_GET['ground_id']) && !empty($_GET['date'])){
            $sDate = Carbon::parse($_GET['date']);
            $sowDate = Carbon::parse($_GET['date'])->startOfWeek();
            $diffDays = $sDate->copy()->diffInDays($sowDate->copy());

            $table = ReservationGroundtable::where('ground_id', $_GET['ground_id'])->where('date', $sDate->format('Y-m-d'))->where('status', 'empty')->where('active', true)->get();

            foreach ($table as $t) {
                $matchCount = Match::where('ground_id', $_GET['ground_id'])->where('date', $sDate->format('Y-m-d'))->where('time', $t->time)->count();
                if($matchCount == 0){
                    $result['items'][] = array('id' => $t->id, 'text' => Carbon::parse($t->time)->format('H:i'));
                }
            }

            $subs = ReservationGroundsubscription::where('ground_id', $_GET['ground_id'])->where('day', $diffDays)->where('active', true)->get();
            foreach ($subs as $s) {

                $filtered = collect($result['items'])->values()->where('text', Carbon::parse($s->time)->format('H:i'))->count();
                $matchCount = Match::where('ground_id', $_GET['ground_id'])->where('date', $sDate->format('Y-m-d'))->where('time', $s->time)->count();
                if($filtered == 0 && $matchCount == 0){
                    $result['items'][] = array('id' => Carbon::parse($s->time)->format('H:i'), 'text' => Carbon::parse($s->time)->format('H:i'));
                }

            }

            if(!empty($result['items'])){
                $result['items'] = collect($result['items'])->sortBy('text')->values()->all();
            }

            echo json_encode($result);
        }
    }

    public function getGroundHourFEAjax()
    {

        $result['items'] = array();

        if(!empty($_GET['ground_id']) && !empty($_GET['date'])){
            $sDate = Carbon::parse($_GET['date']);
            $sowDate = Carbon::parse($_GET['date'])->startOfWeek();
            $diffDays = $sDate->copy()->diffInDays($sowDate->copy());

            $table = ReservationGroundtable::where('ground_id', $_GET['ground_id'])->where('date', $sDate->format('Y-m-d'))->where('status', 'empty')->where('active', true)->get();
            foreach ($table as $t) {
                //$matchCount = Match::where('ground_id', $_GET['ground_id'])->where('date', $sDate->format('Y-m-d'))->where('time', $t->time)->count();
                //if($matchCount == 0){
                    $result['items'][] = array('id' => Carbon::parse($t->time)->format('H:i:s'), 'text' => Carbon::parse($t->time)->format('H:i').' (Rez. Açık)');
                //}
            }

            $subs = ReservationGroundsubscription::where('ground_id', $_GET['ground_id'])->where('day', $diffDays)->where('active', true)->get();
            foreach ($subs as $s) {
                //$filtered = collect($result['items'])->values()->where('text', Carbon::parse($s->time)->format('H:i'))->count();
                //$matchCount = Match::where('ground_id', $_GET['ground_id'])->where('date', $sDate->format('Y-m-d'))->where('time', $s->time)->count();
                //if($filtered == 0 && $matchCount == 0){
                    $result['items'][] = array('id' => Carbon::parse($s->time)->format('H:i:s'), 'text' => Carbon::parse($s->time)->format('H:i').' (Rez. Açık)');
                //}
            }
    
        }

        $hours = ReservationGroundtime::all();
        foreach ($hours as $hour) {
            $result['items'][] = array('id' => Carbon::parse($hour->time)->format('H:i:s'), 'text' => Carbon::parse($hour->time)->format('H:i').' ');
        }

        $result['items'][] = array('id' => 'custom', 'text' => ' Saat Ekle');

        if(!empty($result['items'])){
            $result['items'] = collect($result['items'])->sortByDesc('text')->unique('id')->sortBy('text')->values()->all();
        }

        echo json_encode($result);
    }

    public function getTeamPlayerAjax($tid, $search = null)
    {
        $result['items'] = array();
        
        $teamplayers = TeamTeamPlayer::select('*')->
                        leftJoin('teams', 'teams.id', '=', 'team_teamplayer.team_id')->
                        leftJoin('players', 'players.id', '=', 'team_teamplayer.player_id')->
                        leftJoin('users', 'players.user_id', '=', 'users.id');

        if(!empty($search)){
            $teamplayers = $teamplayers->where(function ($query) use ($search) {
                $query->where(DB::raw("REPLACE(CONCAT(users.first_name, ' ', users.last_name),'  ',' ')"), 'ilike', '%'.$search.'%');
            });
        }

        $teamplayers = $teamplayers->where('team_teamplayer.team_id', $tid)->get();

        foreach ($teamplayers as $teamplayer) {

            if(!empty($teamplayer->team)){
                $teamName = '('.$teamplayer->team->name.') ';
            }else{
                $teamName = '';
            }

            if(!empty($teamplayer->player_id)){
                $player_fullname = $teamName.$teamplayer->player->user->first_name.' '.$teamplayer->player->user->last_name;
            }else{
                $player_fullname = $teamName.$teamplayer->guest_name .'(Misafir)';
            }

            $result['items'][] = array('id' => $teamplayer->player_id, 'text' => $player_fullname);
        }
        
        echo json_encode($result);
    }

    public function getMatchPlayerTeamAjax($mid, $tid)
    {

        $result['items'] = array();
        $matchplayers = MatchPlayer::where('match_id', $mid)->where('team_id', $tid)->get();

        foreach ($matchplayers as $matchplayer) {

            if(!empty($matchplayer->player_id)){
                $player_fullname = $matchplayer->player->user->first_name.' '.$matchplayer->player->user->last_name;
            }else{
                $player_fullname = $matchplayer->guest_name;
            }

            $result['items'][] = array('id' => $matchplayer->id, 'name' => $player_fullname);
        }
        
        echo json_encode($result);
    }

    public function getMatchPlayerAjax($mid, $search = null)
    {
        $result['items'] = array();
        
        $matchplayers = MatchPlayer::select('match_player.id as mpid','*')->where('match_player.match_id', $mid)->
                        leftJoin('teams', 'teams.id', '=', 'match_player.team_id')->
                        leftJoin('players', 'players.id', '=', 'match_player.player_id')->
                        leftJoin('users', 'players.user_id', '=', 'users.id');

        $matchplayers = $matchplayers->where(function ($query) use ($search) {
            $query->where(DB::raw("REPLACE(CONCAT(users.first_name, ' ', users.last_name),'  ',' ')"), 'ilike', '%'.$search.'%');
        });

        $matchplayers = $matchplayers->get();

        foreach ($matchplayers as $matchplayer) {

            if(!empty($matchplayer->team)){
                $teamName = '('.$matchplayer->team->name.') ';
            }else{
                $teamName = '';
            }

            if(!empty($matchplayer->player_id)){
                $player_fullname = $teamName.$matchplayer->player->user->first_name.' '.$matchplayer->player->user->last_name;
            }else{
                $player_fullname = $teamName.$matchplayer->guest_name .'(Misafir)';
            }

            $result['items'][] = array('id' => $matchplayer->mpid, 'text' => $player_fullname);
        }
        
        echo json_encode($result);
    }

    public function getMatchTeamAjax($mid, $search = null)
    {
        $result['items'] = array();
        
        $match = Match::find($mid);

        $result['items'][] = array('id' => $match->team1->id, 'text' => $match->team1->name);
        $result['items'][] = array('id' => $match->team2->id, 'text' => $match->team2->name);
        
        echo json_encode($result);
    }

    public function getMatchPlayerFAjax($mpid)
    {

        $matchplayer = MatchPlayer::find($mpid);
        $__player_id = null;
        if(is_null($matchplayer->player_id)){
            if($matchplayer->team_id == $matchplayer->match->team1_id){
                $__player_id = 'guest1';
            }else if($matchplayer->team_id == $matchplayer->match->team2_id){
                $__player_id = 'guest2';
            }
        }else{
            $__player_id = $matchplayer->player_id;
        }

        $result['items'] = array(
            'id' => $matchplayer->id, 
            'player_id' => $__player_id, 
            'guest_name' => $matchplayer->guest_name, 
            'position_id' => $matchplayer->position_id, 
            'rating' => $matchplayer->rating, 
            'saving' => $matchplayer->saving
        );
        
        echo json_encode($result);
    }

    public function getMatchActionFAjax($maid)
    {

        $matchaction = MatchAction::find($maid);

        $result['items'] = array(
            'id' => $matchaction->id,
            'embed_source' => $matchaction->embed_source,
            'embed_code' => $matchaction->embed_code,
            'team_id' => $matchaction->team_id,
            'match_player_id' => $matchaction->match_player_id,
            'type' => $matchaction->type,
            'minute' => $matchaction->minute
        );
        
        echo json_encode($result);
    }

    public function getMatchPanoramaFAjax($mpaid)
    {

        $matchpanorama = MatchPanorama::find($mpaid);

        $result['items'] = array(
            'id' => $matchpanorama->id,
            'panorama_id' => $matchpanorama->panorama_id,
            'match_player_id' => $matchpanorama->match_player_id,
            'embed_source' => $matchpanorama->embed_source,
            'embed_code' => $matchpanorama->embed_code
        );
        
        echo json_encode($result);
    }

    public function getLeagueGalleryItemFAjax($lgiid)
    {

        $leaguegalleryitems = LeagueGalleryItems::find($lgiid);

        $result['items'] = array(
            'league_gallery_items_id' => $leaguegalleryitems->id,
            'embed_source' => $leaguegalleryitems->embed_source,
            'embed_code' => $leaguegalleryitems->embed_code,
            'description' => $leaguegalleryitems->description,
        );
        
        echo json_encode($result);
    }

    public function setTeamsPAjax($id, $val)
    {
        $team_survey = TeamSurvey::where('team_id', $id)->first();

        if(is_null($team_survey)){
            $team_survey = new TeamSurvey();
            $team_survey->team_id = $id;
            $team_survey->satisfaction_id = 13;
            $team_survey->satisfaction_jogo_id = 1;
        }

        $team_survey->potential_id = $val;
        $team_survey->save();

        echo json_encode(array('text' => 'Başarıyla Güncellendi...', 'status' => 'success'));
    }

    public function setTeamsSAjax($id, $val)
    {
        $team_survey = TeamSurvey::where('team_id', $id)->first();

        if(is_null($team_survey)){
            $team_survey = new TeamSurvey();
            $team_survey->team_id = $id;
            $team_survey->potential_id = 5;
            $team_survey->satisfaction_jogo_id = 1;
        }

        $team_survey->satisfaction_id = $val;
        $team_survey->save();

        echo json_encode(array('text' => 'Başarıyla Güncellendi...', 'status' => 'success'));
    }

    public function setTeamsSJAjax($id, $val)
    {
        $team_survey = TeamSurvey::where('team_id', $id)->first();

        if(is_null($team_survey)){
            $team_survey = new TeamSurvey();
            $team_survey->team_id = $id;
            $team_survey->potential_id = 5;
            $team_survey->satisfaction_id = 13;
        }

        $team_survey->satisfaction_jogo_id = $val;
        $team_survey->save();

        echo json_encode(array('text' => 'Başarıyla Güncellendi...', 'status' => 'success'));
    }

    public function setGroundHourAjax(Request $request)
    {

        if($request->input('type') == 'ground-table-subscription-item'){

            $subs = ReservationGroundsubscription::where('ground_id', $request->input('ground_id'))->
                where('day', $request->input('day'))->
                where('time', $request->input('time'))->first();

            if(empty($subs)){
                $subs_new = new ReservationGroundsubscription();
                $subs_new->ground_id = $request->input('ground_id');
                $subs_new->day = $request->input('day');
                $subs_new->time = $request->input('time');
                $subs_new->active = $request->input('val');
                $subs_new->save();
            }else{
                $subs->active = $request->input('val');
                $subs->save();
            }

            if($request->input('val')){
                $table = ReservationGroundtable::where('ground_id', $request->input('ground_id'))->
                    where('date', Carbon::parse($request->input('week_start_date'))->startOfWeek()->addDays($request->input('day'))->format('Y-m-d'))->
                    where('time', $request->input('time'))->first();
                if(!empty($table)){
                    $table->active = false;
                    $table->save();
                }
            }

        }else if($request->input('type') == 'ground-table-item'){
            
            $table = ReservationGroundtable::where('ground_id', $request->input('ground_id'))->
            where('date', Carbon::parse($request->input('week_start_date'))->startOfWeek()->addDays($request->input('day'))->format('Y-m-d'))->
            where('time', $request->input('time'))->first();
            
            if(empty($table)){
                $table_new = new ReservationGroundtable();
                $table_new->ground_id = $request->input('ground_id');
                $table_new->date = Carbon::parse($request->input('week_start_date'))->startOfWeek()->addDays($request->input('day'))->format('Y-m-d');
                $table_new->time = $request->input('time');
                $table_new->status = 'empty';
                $table_new->active = $request->input('val');
                $table_new->save();
            }else{
                $table->active = $request->input('val');
                $table->save();
            }

        }

        echo json_encode(array('text' => 'Başarıyla Güncellendi...', 'status' => 'success'));
    }

    public function setReservationOffersAjax(Request $request)
    {

        if(!empty($request->input('rid')) && !empty($request->input('roid')) && !empty($request->input('val'))){
            
            $reservation = Reservation::find($request->input('rid'));
            $reservationOffer = ReservationOffer::find($request->input('roid'));

            if($request->input('val') == 'wait'){
                $reservation->status = 'open';
                $reservationOffer->status = 'wait';
            }else if($request->input('val') == 'confirmed'){
                $reservation->status = 'confirmed';
                $reservationOffer->status = 'confirmed';
            }
            
            $reservation->save();
            $reservationOffer->save();

            echo json_encode(array('text' => 'Başarıyla Güncellendi...', 'status' => 'success', 'roid' => $request->input('roid'), 'val' => $request->input('val')));

        }
    }



    public function matchImageSave(Request $request)
    {
        $conf = array('width' => 900, 'height' => 600, 'aspectRatio' => 0, 'widen' => true, 'heighten' => true, 'crop' => true);
        $photo = $request->file('file');
        
        $fileName = str_slug(str_before($photo->getClientOriginalName(), '.'), '_').'_'.str_random(5).'.'.$photo->getClientOriginalExtension();
        $imageObj = Image::make($photo);

        if ($conf['widen']) {
            $imageObj->widen($conf['width'], function ($constraint) {
                $constraint->upsize();
            });
        }

        $ext = $photo->getClientOriginalExtension();
        if($ext == "png"){
            $imageObj->save(env('MEDIA_PATH').'match-images/'.$fileName, 93);
        }else{
            $imageObj->save(env('MEDIA_PATH').'match-images/'.$fileName, 93, 'jpg');
        }

        $matchimage = new MatchImage();
        $matchimage->image = 'match-images/'.$fileName;
        $matchimage->match_id = $request->input('mid');
        $matchimage->save();

        echo json_encode(array('location' => $matchimage->image, 'id' => $matchimage->id, 'mid' => $matchimage->match_id, 'status' => 'success'));
    }

    public function newImageSave(Request $request)
    {   
        $new = News::find($request->nid);
        $pgalleryitem = null;


        if(empty($new->photo_gallery)){

            $gallery = new Galleries();
            $gallery->name = $new->name;
            $gallery->created_by_id = Auth::user()->id;
            $gallery->active = true;
            $gallery->polymorphic_ctype_id = 22;
            $gallery->save();

            $pgallery = new PhotoGallery();
            $pgallery->gallery_ptr_id = $gallery->id;
            $pgallery->save();


            $new->photo_gallery_id = $gallery->id;
            $new->save();   
        }

        $conf = array('width' => 900, 'height' => 600, 'aspectRatio' => 0, 'widen' => true, 'heighten' => true, 'crop' => true);
        $photo = $request->file('file');
        
        $fileName = str_slug(str_before($photo->getClientOriginalName(), '.'), '_').'_'.str_random(5).'.'.$photo->getClientOriginalExtension();
        $imageObj = Image::make($photo);

        if ($conf['widen']) {
            $imageObj->widen($conf['width'], function ($constraint) {
                $constraint->upsize();
            });
        }

        $ext = $photo->getClientOriginalExtension();
        if($ext == "png"){
            $imageObj->save(env('MEDIA_PATH').'galleries/'.$fileName, 93);
        }else{
            $imageObj->save(env('MEDIA_PATH').'galleries/'.$fileName, 93, 'jpg');
        }


        $pgalleryitem = PhotoGalleryItem::where('gallery_id', $new->photo_gallery_id)->first();
        if(empty($pgalleryitem)){
            $pgalleryitem = new PhotoGalleryItem();
            $pgalleryitem->gallery_id = $new->photo_gallery_id;
        }

        $pgalleryitem->image = 'galleries/'.$fileName;
        $pgalleryitem->save();

        echo json_encode(array('location' => $pgalleryitem->image, 'id' => $pgalleryitem->id, 'nid' => $new->id, 'status' => 'success'));
    }


    public function groundImageSave(Request $request)
    {   

        $conf = array('width' => 900, 'height' => 600, 'aspectRatio' => 0, 'widen' => true, 'heighten' => true, 'crop' => true);
        $photo = $request->file('file');
        
        $fileName = str_slug(str_before($photo->getClientOriginalName(), '.'), '_').'_'.str_random(5).'.'.$photo->getClientOriginalExtension();
        $imageObj = Image::make($photo);

        if ($conf['widen']) {
            $imageObj->widen($conf['width'], function ($constraint) {
                $constraint->upsize();
            });
        }

        $ext = $photo->getClientOriginalExtension();
        if($ext == "png"){
            $imageObj->save(env('MEDIA_PATH').'ground-images/'.$fileName, 93);
        }else{
            $imageObj->save(env('MEDIA_PATH').'ground-images/'.$fileName, 93, 'jpg');
        }

        $g_image = new GroundImage();
        $g_image->ground_id = $request->id;
        $g_image->image = 'ground-images/'.$fileName;
        $g_image->save(); 

        echo json_encode(array('location' => $g_image->image, 'id' => $request->id, 'gid' => $g_image->id, 'status' => 'success'));
    }

    public function teamLogoSave(Request $request)
    {
        $conf = array('width' => 200, 'height' => 200, 'aspectRatio' => 0, 'widen' => true, 'heighten' => true, 'crop' => true);
        $photo = $request->file('file');
        
        $fileName = str_slug(str_before($photo->getClientOriginalName(), '.'), '_').'_'.str_random(5).'.'.$photo->getClientOriginalExtension();
        $imageObj = Image::make($photo);

        if ($conf['widen']) {
            $imageObj->widen($conf['width'], function ($constraint) {
                $constraint->upsize();
            });
        }

        $ext = $photo->getClientOriginalExtension();
        if($ext == "png"){
            $imageObj->save(env('MEDIA_PATH').'team-logos/'.$fileName, 93);
        }else{
            $imageObj->save(env('MEDIA_PATH').'team-logos/'.$fileName, 93, 'jpg');
        }

        $team = Team::find($request->input('tid'));
        $team->image = 'team-logos/'.$fileName;
        $team->save();

        echo json_encode(array('location' => $team->image, 'id' => $team->id, 'status' => 'success'));
    }

    public function teamCoverSave(Request $request)
    {
        $conf = array('width' => 1200, 'height' => 800, 'aspectRatio' => 0, 'widen' => true, 'heighten' => true, 'crop' => true);
        $photo = $request->file('file');
        
        $fileName = str_slug(str_before($photo->getClientOriginalName(), '.'), '_').'_'.str_random(5).'.'.$photo->getClientOriginalExtension();
        $imageObj = Image::make($photo);

        if ($conf['widen']) {
            $imageObj->widen($conf['width'], function ($constraint) {
                $constraint->upsize();
            });
        }

        $ext = $photo->getClientOriginalExtension();
        if($ext == "png"){
            $imageObj->save(env('MEDIA_PATH').'team-cover-images/'.$fileName, 93);
        }else{
            $imageObj->save(env('MEDIA_PATH').'team-cover-images/'.$fileName, 93, 'jpg');
        }

        $team = Team::find($request->input('tid'));
        $team->cover_image = 'team-cover-images/'.$fileName;
        $team->save();

        echo json_encode(array('location' => $team->cover_image, 'id' => $team->id, 'status' => 'success'));
    }

    public function userPhotoSave(Request $request)
    {
        $conf = array('width' => 200, 'height' => 200, 'aspectRatio' => 0, 'widen' => true, 'heighten' => true, 'crop' => true);
        $photo = $request->file('file');
        
        $fileName = str_slug(str_before($photo->getClientOriginalName(), '.'), '_').'_'.str_random(5).'.'.$photo->getClientOriginalExtension();
        $imageObj = Image::make($photo);

        if ($conf['widen']) {
            $imageObj->widen($conf['width'], function ($constraint) {
                $constraint->upsize();
            });
        }

        $ext = $photo->getClientOriginalExtension();
        if($ext == "png"){
            $imageObj->save(env('MEDIA_PATH').'user-images/'.$fileName, 93);
        }else{
            $imageObj->save(env('MEDIA_PATH').'user-images/'.$fileName, 93, 'jpg');
        }

        $user = User::find($request->input('uid'));
        $user->image = 'user-images/'.$fileName;
        $user->save();

        echo json_encode(array('location' => $user->image, 'id' => $user->id, 'status' => 'success'));
    }

    public function seasonImageSave(Request $request)
    {
        $conf = array('width' => 900, 'height' => 600, 'aspectRatio' => 0, 'widen' => true, 'heighten' => true, 'crop' => true);
        $photo = $request->file('file');
        
        $fileName = str_slug(str_before($photo->getClientOriginalName(), '.'), '_').'_'.str_random(5).'.'.$photo->getClientOriginalExtension();
        $imageObj = Image::make($photo);

        if ($conf['widen']) {
            $imageObj->widen($conf['width'], function ($constraint) {
                $constraint->upsize();
            });
        }

        $ext = $photo->getClientOriginalExtension();
        if($ext == "png"){
            $imageObj->save(env('MEDIA_PATH').'season-images/'.$fileName, 93);
        }else{
            $imageObj->save(env('MEDIA_PATH').'season-images/'.$fileName, 93, 'jpg');
        }

        if($request->input('season_status') == 'info_slide_photo' || $request->input('season_status') == 'info_img1' || $request->input('season_status') == 'info_img2'){
            $seasonimage = SeasonBeforeAfter::where('status', 'info')->where('season_id', $request->input('sid'))->where('type', $request->input('season_status'))->first();
            if(empty($seasonimage)){
                $seasonimage = new SeasonBeforeAfter();    
            }
            $seasonimage->status = 'info';
            $seasonimage->type = $request->input('season_status');

        }else if ($request->input('season_status') == 'before_slide' || $request->input('season_status') == 'after_slide' || $request->input('season_status') == 'after_teaser_img' || $request->input('season_status') == 'before_teaser_img') {
            $seasonimage = SeasonBeforeAfter::where('status', $request->input('season_status'))->where('season_id', $request->input('sid'))->where('type', 'image')->first();
            if(empty($seasonimage)){
                $seasonimage = new SeasonBeforeAfter();    
            }
            $seasonimage->status = $request->input('season_status');
            $seasonimage->type = 'image';
        }else{
            $seasonimage = new SeasonBeforeAfter();
            $seasonimage->status = $request->input('season_status');
            $seasonimage->type = 'image';
        }

        $seasonimage->data = 'season-images/'.$fileName;
        $seasonimage->season_id = $request->input('sid');
        
        $seasonimage->save();

        echo json_encode(array('location' => $seasonimage->data, 'id' => $seasonimage->id, 'sid' => $seasonimage->season_id, 'status' => 'success'));
    }

    public function bannerPhotoSave(Request $request)
    {
        //$conf = array('width' => 200, 'height' => 200, 'aspectRatio' => 0, 'widen' => true, 'heighten' => true, 'crop' => true);
        $photo = $request->file('file');
        
        $fileName = str_slug(str_before($photo->getClientOriginalName(), '.'), '_').'_'.str_random(5).'.'.$photo->getClientOriginalExtension();
        $imageObj = Image::make($photo);
        /*
        if ($conf['widen']) {
            $imageObj->widen($conf['width'], function ($constraint) {
                $constraint->upsize();
            });
        }
        */
        $ext = $photo->getClientOriginalExtension();
        if($ext == "png"){
            $imageObj->save(env('MEDIA_PATH').'banner-images/'.$fileName, 93);
        }else{
            $imageObj->save(env('MEDIA_PATH').'banner-images/'.$fileName, 93, 'jpg');
        }

        $banner = Banner::find($request->input('uid'));
        $banner->image = 'banner-images/'.$fileName;
        $banner->save();

        echo json_encode(array('location' => $banner->image, 'id' => $banner->id, 'status' => 'success'));
    }

    public function agendaPhotoSave(Request $request)
    {
        $conf = array('width' => 2000, 'height' => 470, 'aspectRatio' => 0, 'widen' => true, 'heighten' => true, 'crop' => true);
        $photo = $request->file('file');
        
        $fileName = str_slug(str_before($photo->getClientOriginalName(), '.'), '_').'_'.str_random(5).'.'.$photo->getClientOriginalExtension();
        $imageObj = Image::make($photo);

        if ($conf['widen']) {
            $imageObj->widen($conf['width'], function ($constraint) {
                $constraint->upsize();
            });
        }

        $ext = $photo->getClientOriginalExtension();
        if($ext == "png"){
            $imageObj->save(env('MEDIA_PATH').$fileName, 93);
        }else{
            $imageObj->save(env('MEDIA_PATH').$fileName, 93, 'jpg');
        }

        $agenda = Agenda::find($request->input('aid'));
        $agenda->image = $fileName;
        $agenda->save();

        echo json_encode(array('location' => $agenda->image, 'id' => $agenda->id, 'status' => 'success'));
    }

    public function panorama_type_LogoSave(Request $request)
    {
        $conf = array('width' => 200, 'height' => 200, 'aspectRatio' => 0, 'widen' => true, 'heighten' => true, 'crop' => true);
        $photo = $request->file('file');
        
        $fileName = str_slug(str_before($photo->getClientOriginalName(), '.'), '_').'_'.str_random(5).'.'.$photo->getClientOriginalExtension();
        $imageObj = Image::make($photo);

        if ($conf['widen']) {
            $imageObj->widen($conf['width'], function ($constraint) {
                $constraint->upsize();
            });
        }

        $ext = $photo->getClientOriginalExtension();
        if($ext == "png"){
            $imageObj->save(env('MEDIA_PATH').'sponsor-logos/'.$fileName, 93);
        }else{
            $imageObj->save(env('MEDIA_PATH').'sponsor-logos/'.$fileName, 93, 'jpg');
        }

        $panoramaType = PanoramaType::find($request->input('ptid'));
        $panoramaType->image = 'sponsor-logos/'.$fileName;
        $panoramaType->save();

        echo json_encode(array('location' => $panoramaType->image, 'id' => $panoramaType->id, 'status' => 'success'));
    }

}