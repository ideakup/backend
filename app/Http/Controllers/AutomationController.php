<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Validator;

use App\Classes\SmsClass;

use Carbon\Carbon;

use App\Agenda;
use App\Championships;
use App\Conference;
use App\Coordinator;
use App\District;
use App\EliminationTree;
use App\EliminationTreeItem;
use App\Galleries;
use App\Ground;
use App\GroundProperty;
use App\GroundImage;
use App\Group;
use App\LeagueGalleries;
use App\LeagueGalleryItems;
use App\League;
use App\MatchAction;
use App\MatchImage;
use App\MatchPanorama;
use App\MatchPlayer;
use App\MatchVideo;
use App\Match;
use App\News;
use App\NewsTypes;
use App\Nostalgia;
use App\PanoramaType;
use App\PenaltyTypes;
use App\Penalty;
use App\PhotoGalleryItem;
use App\PlayerPosition;
use App\PlayerValue;
use App\Player;
use App\PointType;
use App\PressConferences;
use App\Profile;
use App\Province;
use App\Referee;
use App\ReservationGroundsubscription;
use App\ReservationGroundtable;
use App\ReservationOffer;
use App\Reservation;
use App\Season;
use App\Tactic;
use App\Tag;
use App\TeamPlayerTeamHistory;
use App\TeamPotential;
use App\TeamSatisfaction;
use App\TeamSurvey;
use App\TeamTeamPlayer;
use App\TeamTeamSeason;
use App\TeamTeamSeasonAddPointLog;
use App\Team;
use App\Transfer;
use App\User;
use App\VideoGalleryItem;
use App\PasswordReset;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class AutomationController extends Controller {

    protected $matchTemp;
    protected $teamSeasonTemp;

    public function __construct() {
        $this->matchTemp = collect();
        $this->teamSeasonTemp = collect();
    }

    public function index() {

        phpinfo();
        /*
        $team = Team::find(135812);
        dump($team->player_penalties->first()->penalty);

        $team = Team::find(153441);
        dd($team->team_penalties->first()->penalty);
        */
    }

    public function dateNow() {
        dump(Carbon::now());
    }

    public function addPermission() {
        //$permission = Permission::create(['name' => 'add_teamplayer_fromoutside']);
    }

    public function smsTest() {
        
        $smsHelper = new SmsClass();
        $smsHelper->smsTest();

    }

    public function checkDate($param) {
        // match_image_
        $listArray = array('nnnn', 'smalls_1', 'smalls_2', 'smalls_3', 'smalls_4', 'gallery_1', 'gallery_2', 'gallery_3', 'match_action_1', 'match_action_2', 'match_action_3', 'match_action_4', 'match_action_5', 'match_action_6', 'match_action_7', 'match_action_8', 'match_action_9', 'match_action_10', 'match_action_11', 'match_action_12', 'match_action_13', 'match_action_14', 'match_action_15', 'match_action_16', 'match_action_17', 'match_action_18', 'match_action_19', 'match_action_20', 'match_action_21', 'match_action_22', 'match_action_23', 'match_action_24', 'match_action_25', 'match_action_26', 'match_action_27', 'match_action_28', 'match_action_29', 'match_action_30', 'match_action_31', 'match_action_32', 'match_action_33', 'match_action_34', 'match_action_35', 'match_action_36', 'match_action_37', 'match_action_38', 'match_action_39', 'match_action_40', 'match_action_41', 'match_action_42', 'match_action_43', 'match_action_44', 'match_action_45', 'match_action_46', 'match_action_47', 'match_action_48', 'match_action_49', 'match_action_50', 'match_action_51', 'match_action_52', 'match_action_53', 'match_action_54', 'match_action_55', 'match_action_56', 'match_action_57', 'match_action_58', 'match_action_59', 'match_action_60', 'match_action_61', 'match_action_62', 'match_action_63', 'match_action_64', 'match_image_1', 'match_image_2', 'match_image_3', 'match_image_4', 'match_image_5', 'match_image_6', 'match_image_7', 'match_image_8', 'match_image_9', 'match_image_10', 'match_image_11', 'match_image_12', 'match_image_13', 'match_image_14', 'match_image_15', 'match_image_16', 'match_image_17', 'match_image_18', 'match_image_19', 'match_image_20', 'match_image_21', 'match_image_22', 'match_image_23', 'match_image_24', 'match_image_25', 'match_image_26', 'match_image_27', 'match_image_28', 'match_image_29', 'match_image_30', 'match_image_31', 'match_image_32', 'match_image_33', 'match_image_34', 'match_image_35', 'match_image_36', 'match_image_37', 'match_image_38', 'match_image_39', 'match_image_40', 'match_image_41', 'match_image_42', 'match_image_43', 'match_image_44', 'match_image_45', 'match_image_46', 'match_image_47', 'match_image_48', 'match_image_49', 'match_image_50', 'match_image_51', 'match_image_52', 'match_image_53', 'match_image_54', 'match_image_55', 'match_image_56', 'match_image_57', 'match_image_58', 'match_image_59', 'match_image_60', 'match_image_61', 'match_image_62', 'match_image_63', 'match_image_64', 'match_image_65', 'match_image_66', 'match_image_67', 'match_image_68', 'match_image_69', 'match_image_70', 'match_image_71', 'match_image_72', 'match_image_73', 'match_image_74', 'match_image_75', 'match_image_76', 'match_image_77', 'match_image_78', 'match_image_79', 'match_image_80', 'match_image_81', 'match_image_82', 'match_image_83', 'match_image_84', 'match_image_85', 'match_image_86', 'match_image_87', 'match_image_88', 'match_image_89', 'match_image_90', 'match_image_91', 'match_image_92', 'match_image_93', 'match_image_94', 'match_image_95', 'match_image_96', 'match_image_97', 'match_image_98', 'match_image_99', 'match_image_100', 'match_image_101', 'match_image_102', 'match_panorama_1', 'match_panorama_2', 'match_panorama_3', 'match_panorama_4', 'match_panorama_5', 'match_panorama_6', 'match_panorama_7', 'match_player_1', 'match_player_2', 'match_player_3', 'match_player_4', 'match_player_5', 'match_player_6', 'match_player_7', 'match_player_8', 'match_player_9', 'match_player_10', 'match_player_11', 'match_player_12', 'match_player_13', 'match_player_14', 'match_player_15', 'match_player_16', 'match_player_17', 'match_player_18', 'match_player_19', 'match_player_20', 'match_player_21', 'match_player_22', 'match_player_23', 'match_player_24', 'match_player_25', 'match_player_26', 'match_player_27', 'match_player_28', 'match_player_29', 'match_player_30', 'match_player_31', 'match_player_32', 'match_player_33', 'match_player_34', 'match_player_35', 'match_player_36', 'match_player_37', 'match_player_38', 'match_player_39', 'match_player_40', 'match_player_41', 'match_player_42', 'match_player_43', 'match_player_44', 'match_player_45', 'match_player_46', 'match_player_47', 'match_player_48', 'match_player_49', 'match_player_50', 'match_player_51', 'match_player_52', 'match_player_53', 'match_player_54', 'match_player_55', 'match_player_56', 'match_player_57', 'match_player_58', 'match_player_59', 'match_player_60', 'match_player_61', 'match_player_62', 'match_player_63', 'match_player_64', 'match_player_65', 'match_player_66', 'match_player_67', 'match_player_68', 'match_player_69', 'match_player_70', 'match_player_71', 'match_player_72', 'match_player_73', 'match_player_74', 'match_player_75', 'match_player_76', 'match_player_77', 'match_player_78', 'match_player_79', 'match_player_80', 'match_player_81', 'match_player_82', 'match_player_83', 'match_player_84', 'match_player_85', 'match_player_86', 'match_player_87', 'match_player_88', 'match_player_89', 'match_player_90', 'match_player_91', 'match_player_92', 'match_player_93', 'match_player_94', 'match_player_95', 'match_player_96', 'match_player_97', 'match_player_98', 'match_player_99', 'match_player_100', 'match_player_101', 'match_player_102', 'match_player_103', 'match_player_104', 'match_player_105', 'match_player_106', 'match_player_107', 'match_player_108', 'match_player_109', 'match_player_110', 'match_player_111', 'match_player_112', 'match_player_113', 'match_player_114', 'match_player_115', 'match_player_116', 'match_player_117', 'match_player_118', 'match_player_119', 'match_player_120', 'match_player_121', 'match_player_122', 'match_player_123', 'match_player_124', 'match_player_125', 'match_player_126', 'match_player_127', 'match_player_128', 'match_player_129', 'match_player_130', 'match_player_131', 'match_player_132', 'match_player_133', 'match_player_134', 'match_player_135', 'match_player_136', 'match_player_137', 'match_player_138', 'match_player_139', 'match_player_140', 'match_player_141', 'match_player_142', 'match_player_143', 'match_player_144', 'match_player_145', 'match_video_1', 'match_video_2', 'match_video_3', 'match_video_4', 'match_video_5', 'match_video_6', 'match_video_7', 'match_video_8', 'match_video_9', 'match_video_10', 'match_video_11', 'match_video_12', 'match_1', 'match_2', 'match_3', 'match_4', 'match_5', 'match_6', 'match_7', 'match_8', 'match_9', 'match_10', 'match_11', 'match_12', 'new_1', 'new_2', 'new_3', 'photogalleryitem_1', 'photogalleryitem_2', 'photogalleryitem_3', 'playervalue_1', 'playervalue_2', 'playervalue_3', 'playervalue_4', 'playervalue_5', 'playervalue_6', 'playervalue_7', 'playervalue_8', 'playervalue_9', 'playervalue_10', 'playervalue_11', 'playervalue_12', 'playervalue_13', 'playervalue_14', 'player_1', 'player_2', 'player_3', 'player_4', 'player_5', 'player_6', 'player_7', 'player_8', 'player_9', 'player_10', 'player_11', 'player_12', 'player_13', 'player_14', 'player_15', 'player_16', 'player_17', 'player_18', 'player_19', 'player_20', 'player_21', 'player_22', 'pressconferences_1', 'pressconferences_2', 'pressconferences_3', 'pressconferences_4', 'pressconferences_5', 'pressconferences_6', 'pressconferences_7', 'pressconferences_8', 'profile_1', 'profile_2', 'profile_3', 'profile_4', 'profile_5', 'profile_6', 'profile_7', 'profile_8', 'profile_9', 'profile_10', 'profile_11', 'profile_12', 'profile_13', 'profile_14', 'profile_15', 'profile_16', 'profile_17', 'profile_18', 'profile_19', 'profile_20', 'profile_21', 'profile_22', 'profile_23', 'team_playerteamhistory_1', 'team_playerteamhistory_2', 'team_playerteamhistory_3', 'team_playerteamhistory_4', 'team_playerteamhistory_5', 'team_playerteamhistory_6', 'team_playerteamhistory_7', 'team_playerteamhistory_8', 'team_playerteamhistory_9', 'team_playerteamhistory_10', 'team_playerteamhistory_11', 'team_playerteamhistory_12', 'team_playerteamhistory_13', 'team_playerteamhistory_14', 'team_playerteamhistory_15', 'team_playerteamhistory_16', 'team_playerteamhistory_17', 'teamsurvey_1', 'teamsurvey_2', 'teamplayer_1', 'teamplayer_2', 'teamplayer_3', 'teamplayer_4', 'teamplayer_5', 'teamplayer_6', 'teamplayer_7', 'teamplayer_8', 'teamplayer_9', 'teamplayer_10', 'teamplayer_11', 'teamseason_1', 'teamseason_2', 'teamseason_3', 'teamseason_4', 'teamseason_5', 'teamseason_6', 'teamseason_7', 'team_1', 'team_2', 'team_3', 'team_4', 'transfer_1', 'transfer_2', 'transfer_3', 'transfer_4', 'transfer_5', 'transfer_6', 'transfer_7', 'transfer_8', 'transfer_9', 'transfer_10', 'transfer_11', 'transfer_12', 'transfer_13', 'transfer_14', 'transfer_15', 'transfer_16', 'transfer_17', 'transfer_18', 'transfer_19', 'transfer_20', 'transfer_21', 'transfer_22', 'user_1', 'user_2', 'user_3', 'user_4', 'user_5', 'user_6', 'user_7', 'user_8', 'user_9', 'user_10', 'user_11', 'user_12', 'user_13', 'user_14', 'user_15', 'user_16', 'user_17', 'user_18', 'user_19', 'user_20', 'user_21', 'user_22', 'user_23', 'videogalleryitem_1', 'videogalleryitem_2');

        $forward = $listArray[0];

        for ( $i=0; $i < count($listArray); $i++ ) { 
            if($listArray[$i] == $param){
                if($i+1 == count($listArray)){
                    $forward = '';
                    //dump($forward);
                }else{
                    $forward = $listArray[$i+1];
                    //dump($forward);
                }
                break;
            }
        }

        $block = 10000;

        if($param == 'nnnn'){

            $models = PasswordReset::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }

        }

        if($param == 'smalls_1'){

            $models = Agenda::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }

            $models = Championships::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }

            $models = Conference::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }

            $models = Coordinator::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }

            $models = District::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }

            $models = EliminationTree::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }

            $models = EliminationTreeItem::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }

            $models = GroundImage::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }

        }

        if($param == 'smalls_2'){

            $models = GroundProperty::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }

            $models = Ground::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }

            $models = Group::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }

            $models = LeagueGalleries::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }

            $models = LeagueGalleryItems::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }

            $models = League::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }

            $models = NewsTypes::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }

            $models = Nostalgia::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }

            $models = PanoramaType::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }

            $models = PenaltyTypes::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }

        }

        if($param == 'smalls_3'){

            $models = Penalty::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }

            $models = PlayerPosition::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }

            $models = PointType::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }

            $models = Province::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }

            $models = Referee::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }

            $models = ReservationGroundsubscription::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }

        }

        if($param == 'smalls_4'){

            $models = ReservationGroundtable::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }

            $models = ReservationOffer::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }

            $models = Reservation::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }

            $models = Season::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }

            $models = Tactic::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }

            $models = Tag::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }

            $models = TeamPotential::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }

            $models = TeamSatisfaction::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }

            $models = TeamTeamSeasonAddPointLog::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }

        }





        if($param == 'gallery_1'){
            $models = Galleries::offset(0*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'gallery_2'){
            $models = Galleries::offset(1*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'gallery_3'){
            $models = Galleries::offset(2*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }




        if($param == 'match_action_1'){
            $models = MatchAction::offset(0*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_action_2'){
            $models = MatchAction::offset(1*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_action_3'){
            $models = MatchAction::offset(2*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_action_4'){
            $models = MatchAction::offset(3*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_action_5'){
            $models = MatchAction::offset(4*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_action_6'){
            $models = MatchAction::offset(5*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_action_7'){
            $models = MatchAction::offset(6*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_action_8'){
            $models = MatchAction::offset(7*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_action_9'){
            $models = MatchAction::offset(8*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_action_10'){
            $models = MatchAction::offset(9*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_action_11'){
            $models = MatchAction::offset(10*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_action_12'){
            $models = MatchAction::offset(11*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_action_13'){
            $models = MatchAction::offset(12*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_action_14'){
            $models = MatchAction::offset(13*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_action_15'){
            $models = MatchAction::offset(14*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_action_16'){
            $models = MatchAction::offset(15*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_action_17'){
            $models = MatchAction::offset(16*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_action_18'){
            $models = MatchAction::offset(17*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_action_19'){
            $models = MatchAction::offset(18*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_action_20'){
            $models = MatchAction::offset(19*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_action_21'){
            $models = MatchAction::offset(20*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_action_22'){
            $models = MatchAction::offset(21*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_action_23'){
            $models = MatchAction::offset(22*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_action_24'){
            $models = MatchAction::offset(23*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_action_25'){
            $models = MatchAction::offset(24*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_action_26'){
            $models = MatchAction::offset(25*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_action_27'){
            $models = MatchAction::offset(26*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_action_28'){
            $models = MatchAction::offset(27*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_action_29'){
            $models = MatchAction::offset(28*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_action_30'){
            $models = MatchAction::offset(29*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_action_31'){
            $models = MatchAction::offset(30*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_action_32'){
            $models = MatchAction::offset(31*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_action_33'){
            $models = MatchAction::offset(32*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_action_34'){
            $models = MatchAction::offset(33*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_action_35'){
            $models = MatchAction::offset(34*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_action_36'){
            $models = MatchAction::offset(35*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_action_37'){
            $models = MatchAction::offset(36*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_action_38'){
            $models = MatchAction::offset(37*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_action_39'){
            $models = MatchAction::offset(38*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_action_40'){
            $models = MatchAction::offset(39*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_action_41'){
            $models = MatchAction::offset(40*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_action_42'){
            $models = MatchAction::offset(41*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_action_43'){
            $models = MatchAction::offset(42*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_action_44'){
            $models = MatchAction::offset(43*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_action_45'){
            $models = MatchAction::offset(44*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_action_46'){
            $models = MatchAction::offset(45*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_action_47'){
            $models = MatchAction::offset(46*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_action_48'){
            $models = MatchAction::offset(47*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_action_49'){
            $models = MatchAction::offset(48*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_action_50'){
            $models = MatchAction::offset(49*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_action_51'){
            $models = MatchAction::offset(50*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_action_52'){
            $models = MatchAction::offset(51*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_action_53'){
            $models = MatchAction::offset(52*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_action_54'){
            $models = MatchAction::offset(53*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_action_55'){
            $models = MatchAction::offset(54*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_action_56'){
            $models = MatchAction::offset(55*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_action_57'){
            $models = MatchAction::offset(56*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_action_58'){
            $models = MatchAction::offset(57*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_action_59'){
            $models = MatchAction::offset(58*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_action_60'){
            $models = MatchAction::offset(59*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_action_61'){
            $models = MatchAction::offset(60*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_action_62'){
            $models = MatchAction::offset(61*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_action_63'){
            $models = MatchAction::offset(62*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_action_64'){
            $models = MatchAction::offset(63*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }




        if($param == 'match_image_1'){
            $models = MatchImage::offset(0*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_2'){
            $models = MatchImage::offset(1*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_3'){
            $models = MatchImage::offset(2*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_4'){
            $models = MatchImage::offset(3*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_5'){
            $models = MatchImage::offset(4*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_6'){
            $models = MatchImage::offset(5*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_7'){
            $models = MatchImage::offset(6*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_8'){
            $models = MatchImage::offset(7*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_9'){
            $models = MatchImage::offset(8*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_10'){
            $models = MatchImage::offset(9*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_11'){
            $models = MatchImage::offset(10*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_12'){
            $models = MatchImage::offset(11*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_13'){
            $models = MatchImage::offset(12*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_14'){
            $models = MatchImage::offset(13*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_15'){
            $models = MatchImage::offset(14*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_16'){
            $models = MatchImage::offset(15*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_17'){
            $models = MatchImage::offset(16*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_18'){
            $models = MatchImage::offset(17*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_19'){
            $models = MatchImage::offset(18*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_20'){
            $models = MatchImage::offset(19*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_21'){
            $models = MatchImage::offset(20*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_22'){
            $models = MatchImage::offset(21*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_23'){
            $models = MatchImage::offset(22*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_24'){
            $models = MatchImage::offset(23*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_25'){
            $models = MatchImage::offset(24*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_26'){
            $models = MatchImage::offset(25*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_27'){
            $models = MatchImage::offset(26*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_28'){
            $models = MatchImage::offset(27*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_29'){
            $models = MatchImage::offset(28*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_30'){
            $models = MatchImage::offset(29*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_31'){
            $models = MatchImage::offset(30*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_32'){
            $models = MatchImage::offset(31*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_33'){
            $models = MatchImage::offset(32*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_34'){
            $models = MatchImage::offset(33*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_35'){
            $models = MatchImage::offset(34*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_36'){
            $models = MatchImage::offset(35*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_37'){
            $models = MatchImage::offset(36*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_38'){
            $models = MatchImage::offset(37*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_39'){
            $models = MatchImage::offset(38*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_40'){
            $models = MatchImage::offset(39*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_41'){
            $models = MatchImage::offset(40*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_42'){
            $models = MatchImage::offset(41*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_43'){
            $models = MatchImage::offset(42*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_44'){
            $models = MatchImage::offset(43*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_45'){
            $models = MatchImage::offset(44*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_46'){
            $models = MatchImage::offset(45*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_47'){
            $models = MatchImage::offset(46*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_48'){
            $models = MatchImage::offset(47*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_49'){
            $models = MatchImage::offset(48*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_50'){
            $models = MatchImage::offset(49*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_51'){
            $models = MatchImage::offset(50*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_52'){
            $models = MatchImage::offset(51*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_53'){
            $models = MatchImage::offset(52*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_54'){
            $models = MatchImage::offset(53*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_55'){
            $models = MatchImage::offset(54*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_56'){
            $models = MatchImage::offset(55*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_57'){
            $models = MatchImage::offset(56*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_58'){
            $models = MatchImage::offset(57*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_59'){
            $models = MatchImage::offset(58*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_60'){
            $models = MatchImage::offset(59*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_61'){
            $models = MatchImage::offset(60*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_62'){
            $models = MatchImage::offset(61*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_63'){
            $models = MatchImage::offset(62*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_64'){
            $models = MatchImage::offset(63*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_65'){
            $models = MatchImage::offset(64*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_66'){
            $models = MatchImage::offset(65*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_67'){
            $models = MatchImage::offset(66*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_68'){
            $models = MatchImage::offset(67*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_69'){
            $models = MatchImage::offset(68*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_70'){
            $models = MatchImage::offset(69*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_71'){
            $models = MatchImage::offset(70*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_72'){
            $models = MatchImage::offset(71*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_73'){
            $models = MatchImage::offset(72*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_74'){
            $models = MatchImage::offset(73*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_75'){
            $models = MatchImage::offset(74*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_76'){
            $models = MatchImage::offset(75*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_77'){
            $models = MatchImage::offset(76*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_78'){
            $models = MatchImage::offset(77*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_79'){
            $models = MatchImage::offset(78*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_80'){
            $models = MatchImage::offset(79*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_81'){
            $models = MatchImage::offset(80*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_82'){
            $models = MatchImage::offset(81*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_83'){
            $models = MatchImage::offset(82*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_84'){
            $models = MatchImage::offset(83*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_85'){
            $models = MatchImage::offset(84*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_86'){
            $models = MatchImage::offset(85*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_87'){
            $models = MatchImage::offset(86*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_88'){
            $models = MatchImage::offset(87*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_89'){
            $models = MatchImage::offset(88*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_90'){
            $models = MatchImage::offset(89*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_91'){
            $models = MatchImage::offset(90*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_92'){
            $models = MatchImage::offset(91*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_93'){
            $models = MatchImage::offset(92*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_94'){
            $models = MatchImage::offset(93*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_95'){
            $models = MatchImage::offset(94*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_96'){
            $models = MatchImage::offset(95*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_97'){
            $models = MatchImage::offset(96*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_98'){
            $models = MatchImage::offset(97*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_99'){
            $models = MatchImage::offset(98*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_100'){
            $models = MatchImage::offset(99*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_101'){
            $models = MatchImage::offset(100*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_image_102'){
            $models = MatchImage::offset(101*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }





        if($param == 'match_panorama_1'){
            $models = MatchPanorama::offset(0*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_panorama_2'){
            $models = MatchPanorama::offset(1*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_panorama_3'){
            $models = MatchPanorama::offset(2*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_panorama_4'){
            $models = MatchPanorama::offset(3*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_panorama_5'){
            $models = MatchPanorama::offset(4*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_panorama_6'){
            $models = MatchPanorama::offset(5*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_panorama_7'){
            $models = MatchPanorama::offset(6*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }





        // match_player - 1380 K
        if($param == 'match_player_1'){
            $models = MatchPlayer::offset(0*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_2'){
            $models = MatchPlayer::offset(1*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_3'){
            $models = MatchPlayer::offset(2*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_4'){
            $models = MatchPlayer::offset(3*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_5'){
            $models = MatchPlayer::offset(4*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_6'){
            $models = MatchPlayer::offset(5*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_7'){
            $models = MatchPlayer::offset(6*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_8'){
            $models = MatchPlayer::offset(7*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_9'){
            $models = MatchPlayer::offset(8*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_10'){
            $models = MatchPlayer::offset(9*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_11'){
            $models = MatchPlayer::offset(10*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_12'){
            $models = MatchPlayer::offset(11*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_13'){
            $models = MatchPlayer::offset(12*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_14'){
            $models = MatchPlayer::offset(13*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_15'){
            $models = MatchPlayer::offset(14*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_16'){
            $models = MatchPlayer::offset(15*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_17'){
            $models = MatchPlayer::offset(16*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_18'){
            $models = MatchPlayer::offset(17*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_19'){
            $models = MatchPlayer::offset(18*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_20'){
            $models = MatchPlayer::offset(19*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_21'){
            $models = MatchPlayer::offset(20*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_22'){
            $models = MatchPlayer::offset(21*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_23'){
            $models = MatchPlayer::offset(22*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_24'){
            $models = MatchPlayer::offset(23*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_25'){
            $models = MatchPlayer::offset(24*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_26'){
            $models = MatchPlayer::offset(25*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_27'){
            $models = MatchPlayer::offset(26*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_28'){
            $models = MatchPlayer::offset(27*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_29'){
            $models = MatchPlayer::offset(28*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_30'){
            $models = MatchPlayer::offset(29*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_31'){
            $models = MatchPlayer::offset(30*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_32'){
            $models = MatchPlayer::offset(31*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_33'){
            $models = MatchPlayer::offset(32*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_34'){
            $models = MatchPlayer::offset(33*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_35'){
            $models = MatchPlayer::offset(34*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_36'){
            $models = MatchPlayer::offset(35*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_37'){
            $models = MatchPlayer::offset(36*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_38'){
            $models = MatchPlayer::offset(37*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_39'){
            $models = MatchPlayer::offset(38*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_40'){
            $models = MatchPlayer::offset(39*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_41'){
            $models = MatchPlayer::offset(40*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_42'){
            $models = MatchPlayer::offset(41*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_43'){
            $models = MatchPlayer::offset(42*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_44'){
            $models = MatchPlayer::offset(43*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_45'){
            $models = MatchPlayer::offset(44*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_46'){
            $models = MatchPlayer::offset(45*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_47'){
            $models = MatchPlayer::offset(46*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_48'){
            $models = MatchPlayer::offset(47*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_49'){
            $models = MatchPlayer::offset(48*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_50'){
            $models = MatchPlayer::offset(49*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_51'){
            $models = MatchPlayer::offset(50*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_52'){
            $models = MatchPlayer::offset(51*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_53'){
            $models = MatchPlayer::offset(52*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_54'){
            $models = MatchPlayer::offset(53*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_55'){
            $models = MatchPlayer::offset(54*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_56'){
            $models = MatchPlayer::offset(55*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_57'){
            $models = MatchPlayer::offset(56*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_58'){
            $models = MatchPlayer::offset(57*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_59'){
            $models = MatchPlayer::offset(58*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_60'){
            $models = MatchPlayer::offset(59*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_61'){
            $models = MatchPlayer::offset(60*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_62'){
            $models = MatchPlayer::offset(61*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_63'){
            $models = MatchPlayer::offset(62*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_64'){
            $models = MatchPlayer::offset(63*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_65'){
            $models = MatchPlayer::offset(64*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_66'){
            $models = MatchPlayer::offset(65*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_67'){
            $models = MatchPlayer::offset(66*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_68'){
            $models = MatchPlayer::offset(67*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_69'){
            $models = MatchPlayer::offset(68*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_70'){
            $models = MatchPlayer::offset(69*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_71'){
            $models = MatchPlayer::offset(70*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_72'){
            $models = MatchPlayer::offset(71*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_73'){
            $models = MatchPlayer::offset(72*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_74'){
            $models = MatchPlayer::offset(73*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_75'){
            $models = MatchPlayer::offset(74*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_76'){
            $models = MatchPlayer::offset(75*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_77'){
            $models = MatchPlayer::offset(76*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_78'){
            $models = MatchPlayer::offset(77*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_79'){
            $models = MatchPlayer::offset(78*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_80'){
            $models = MatchPlayer::offset(79*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_81'){
            $models = MatchPlayer::offset(80*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_82'){
            $models = MatchPlayer::offset(81*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_83'){
            $models = MatchPlayer::offset(82*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_84'){
            $models = MatchPlayer::offset(83*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_85'){
            $models = MatchPlayer::offset(84*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_86'){
            $models = MatchPlayer::offset(85*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_87'){
            $models = MatchPlayer::offset(86*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_88'){
            $models = MatchPlayer::offset(87*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_89'){
            $models = MatchPlayer::offset(88*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_90'){
            $models = MatchPlayer::offset(89*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_91'){
            $models = MatchPlayer::offset(90*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_92'){
            $models = MatchPlayer::offset(91*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_93'){
            $models = MatchPlayer::offset(92*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_94'){
            $models = MatchPlayer::offset(93*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_95'){
            $models = MatchPlayer::offset(94*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_96'){
            $models = MatchPlayer::offset(95*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_97'){
            $models = MatchPlayer::offset(96*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_98'){
            $models = MatchPlayer::offset(97*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_99'){
            $models = MatchPlayer::offset(98*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_100'){
            $models = MatchPlayer::offset(99*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_101'){
            $models = MatchPlayer::offset(100*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_102'){
            $models = MatchPlayer::offset(101*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_103'){
            $models = MatchPlayer::offset(102*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_104'){
            $models = MatchPlayer::offset(103*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_105'){
            $models = MatchPlayer::offset(104*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_106'){
            $models = MatchPlayer::offset(105*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_107'){
            $models = MatchPlayer::offset(106*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_108'){
            $models = MatchPlayer::offset(107*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_109'){
            $models = MatchPlayer::offset(108*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_110'){
            $models = MatchPlayer::offset(109*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_111'){
            $models = MatchPlayer::offset(110*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_112'){
            $models = MatchPlayer::offset(111*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_113'){
            $models = MatchPlayer::offset(112*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_114'){
            $models = MatchPlayer::offset(113*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_115'){
            $models = MatchPlayer::offset(114*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_116'){
            $models = MatchPlayer::offset(115*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_117'){
            $models = MatchPlayer::offset(116*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_118'){
            $models = MatchPlayer::offset(117*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_119'){
            $models = MatchPlayer::offset(118*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_120'){
            $models = MatchPlayer::offset(119*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_121'){
            $models = MatchPlayer::offset(120*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_122'){
            $models = MatchPlayer::offset(121*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_123'){
            $models = MatchPlayer::offset(122*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_124'){
            $models = MatchPlayer::offset(123*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_125'){
            $models = MatchPlayer::offset(124*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_126'){
            $models = MatchPlayer::offset(125*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_127'){
            $models = MatchPlayer::offset(126*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_128'){
            $models = MatchPlayer::offset(127*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_129'){
            $models = MatchPlayer::offset(128*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_130'){
            $models = MatchPlayer::offset(129*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_131'){
            $models = MatchPlayer::offset(130*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_132'){
            $models = MatchPlayer::offset(131*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_133'){
            $models = MatchPlayer::offset(132*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_134'){
            $models = MatchPlayer::offset(133*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_135'){
            $models = MatchPlayer::offset(134*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_136'){
            $models = MatchPlayer::offset(135*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_137'){
            $models = MatchPlayer::offset(136*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_138'){
            $models = MatchPlayer::offset(137*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_139'){
            $models = MatchPlayer::offset(138*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_140'){
            $models = MatchPlayer::offset(139*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_141'){
            $models = MatchPlayer::offset(140*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_142'){
            $models = MatchPlayer::offset(141*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_143'){
            $models = MatchPlayer::offset(142*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_144'){
            $models = MatchPlayer::offset(143*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_player_145'){
            $models = MatchPlayer::offset(144*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }




        if($param == 'match_video_1'){
            $models = MatchVideo::offset(0*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_video_2'){
            $models = MatchVideo::offset(1*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_video_3'){
            $models = MatchVideo::offset(2*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_video_4'){
            $models = MatchVideo::offset(3*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_video_5'){
            $models = MatchVideo::offset(4*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_video_6'){
            $models = MatchVideo::offset(5*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_video_7'){
            $models = MatchVideo::offset(6*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_video_8'){
            $models = MatchVideo::offset(7*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_video_9'){
            $models = MatchVideo::offset(8*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_video_10'){
            $models = MatchVideo::offset(9*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_video_11'){
            $models = MatchVideo::offset(10*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_video_12'){
            $models = MatchVideo::offset(11*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }





        if($param == 'match_1'){
            $models = Match::offset(0*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_2'){
            $models = Match::offset(1*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_3'){
            $models = Match::offset(2*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_4'){
            $models = Match::offset(3*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_5'){
            $models = Match::offset(4*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_6'){
            $models = Match::offset(5*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_7'){
            $models = Match::offset(6*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_8'){
            $models = Match::offset(7*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_9'){
            $models = Match::offset(8*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_10'){
            $models = Match::offset(9*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_11'){
            $models = Match::offset(10*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'match_12'){
            $models = Match::offset(11*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }





        if($param == 'new_1'){
            $models = News::offset(0*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->date = Carbon::parse($model->date);
                $model->save();
            }
        }

        if($param == 'new_2'){
            $models = News::offset(1*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->date = Carbon::parse($model->date);
                $model->save();
            }
        }

        if($param == 'new_3'){
            $models = News::offset(2*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->date = Carbon::parse($model->date);
                $model->save();
            }
        }





        if($param == 'photogalleryitem_1'){
            $models = PhotoGalleryItem::offset(0*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'photogalleryitem_2'){
            $models = PhotoGalleryItem::offset(1*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'photogalleryitem_3'){
            $models = PhotoGalleryItem::offset(2*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }





        if($param == 'playervalue_1'){
            $models = PlayerValue::offset(0*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'playervalue_2'){
            $models = PlayerValue::offset(1*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'playervalue_3'){
            $models = PlayerValue::offset(2*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'playervalue_4'){
            $models = PlayerValue::offset(3*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'playervalue_5'){
            $models = PlayerValue::offset(4*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'playervalue_6'){
            $models = PlayerValue::offset(5*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'playervalue_7'){
            $models = PlayerValue::offset(6*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'playervalue_8'){
            $models = PlayerValue::offset(7*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'playervalue_9'){
            $models = PlayerValue::offset(8*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'playervalue_10'){
            $models = PlayerValue::offset(9*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'playervalue_11'){
            $models = PlayerValue::offset(10*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'playervalue_12'){
            $models = PlayerValue::offset(11*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'playervalue_13'){
            $models = PlayerValue::offset(12*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'playervalue_14'){
            $models = PlayerValue::offset(13*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }





        // players - 210 K
        if($param == 'player_1'){
            $models = Player::offset(0*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'player_2'){
            $models = Player::offset(1*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'player_3'){
            $models = Player::offset(2*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'player_4'){
            $models = Player::offset(3*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'player_5'){
            $models = Player::offset(4*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'player_6'){
            $models = Player::offset(5*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'player_7'){
            $models = Player::offset(6*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'player_8'){
            $models = Player::offset(7*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'player_9'){
            $models = Player::offset(8*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'player_10'){
            $models = Player::offset(9*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'player_11'){
            $models = Player::offset(10*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'player_12'){
            $models = Player::offset(11*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'player_13'){
            $models = Player::offset(12*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'player_14'){
            $models = Player::offset(13*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'player_15'){
            $models = Player::offset(14*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'player_16'){
            $models = Player::offset(15*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'player_17'){
            $models = Player::offset(16*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'player_18'){
            $models = Player::offset(17*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'player_19'){
            $models = Player::offset(18*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'player_20'){
            $models = Player::offset(19*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'player_21'){
            $models = Player::offset(20*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'player_22'){
            $models = Player::offset(21*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }





        if($param == 'pressconferences_1'){
            $models = PressConferences::offset(0*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'pressconferences_2'){
            $models = PressConferences::offset(1*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'pressconferences_3'){
            $models = PressConferences::offset(2*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'pressconferences_4'){
            $models = PressConferences::offset(3*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'pressconferences_5'){
            $models = PressConferences::offset(4*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'pressconferences_6'){
            $models = PressConferences::offset(5*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'pressconferences_7'){
            $models = PressConferences::offset(6*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'pressconferences_8'){
            $models = PressConferences::offset(7*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }





        // profile - 210 K
        if($param == 'profile_1'){
            $models = Profile::offset(0*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'profile_2'){
            $models = Profile::offset(1*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'profile_3'){
            $models = Profile::offset(2*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'profile_4'){
            $models = Profile::offset(3*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'profile_5'){
            $models = Profile::offset(4*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'profile_6'){
            $models = Profile::offset(5*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'profile_7'){
            $models = Profile::offset(6*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'profile_8'){
            $models = Profile::offset(7*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'profile_9'){
            $models = Profile::offset(8*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'profile_10'){
            $models = Profile::offset(9*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'profile_11'){
            $models = Profile::offset(10*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'profile_12'){
            $models = Profile::offset(11*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'profile_13'){
            $models = Profile::offset(12*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'profile_14'){
            $models = Profile::offset(13*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'profile_15'){
            $models = Profile::offset(14*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'profile_16'){
            $models = Profile::offset(15*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'profile_17'){
            $models = Profile::offset(16*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'profile_18'){
            $models = Profile::offset(17*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'profile_19'){
            $models = Profile::offset(18*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'profile_20'){
            $models = Profile::offset(19*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'profile_21'){
            $models = Profile::offset(20*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'profile_22'){
            $models = Profile::offset(21*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'profile_23'){
            $models = Profile::offset(22*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }





        // TeamPlayerTeamHistory - 159 K
        if($param == 'team_playerteamhistory_1'){
            $models = TeamPlayerTeamHistory::offset(0*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'team_playerteamhistory_2'){
            $models = TeamPlayerTeamHistory::offset(1*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'team_playerteamhistory_3'){
            $models = TeamPlayerTeamHistory::offset(2*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'team_playerteamhistory_4'){
            $models = TeamPlayerTeamHistory::offset(3*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'team_playerteamhistory_5'){
            $models = TeamPlayerTeamHistory::offset(4*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'team_playerteamhistory_6'){
            $models = TeamPlayerTeamHistory::offset(5*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'team_playerteamhistory_7'){
            $models = TeamPlayerTeamHistory::offset(6*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'team_playerteamhistory_8'){
            $models = TeamPlayerTeamHistory::offset(7*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'team_playerteamhistory_9'){
            $models = TeamPlayerTeamHistory::offset(8*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'team_playerteamhistory_10'){
            $models = TeamPlayerTeamHistory::offset(9*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'team_playerteamhistory_11'){
            $models = TeamPlayerTeamHistory::offset(10*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'team_playerteamhistory_12'){
            $models = TeamPlayerTeamHistory::offset(11*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'team_playerteamhistory_13'){
            $models = TeamPlayerTeamHistory::offset(12*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'team_playerteamhistory_14'){
            $models = TeamPlayerTeamHistory::offset(13*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'team_playerteamhistory_15'){
            $models = TeamPlayerTeamHistory::offset(14*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'team_playerteamhistory_16'){
            $models = TeamPlayerTeamHistory::offset(15*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'team_playerteamhistory_17'){
            $models = TeamPlayerTeamHistory::offset(16*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }





        if($param == 'teamsurvey_1'){
            $models = TeamSurvey::offset(0*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'teamsurvey_2'){
            $models = TeamSurvey::offset(1*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }





        if($param == 'teamplayer_1'){
            $models = TeamTeamPlayer::offset(0*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'teamplayer_2'){
            $models = TeamTeamPlayer::offset(1*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'teamplayer_3'){
            $models = TeamTeamPlayer::offset(2*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'teamplayer_4'){
            $models = TeamTeamPlayer::offset(3*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'teamplayer_5'){
            $models = TeamTeamPlayer::offset(4*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'teamplayer_6'){
            $models = TeamTeamPlayer::offset(5*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'teamplayer_7'){
            $models = TeamTeamPlayer::offset(6*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'teamplayer_8'){
            $models = TeamTeamPlayer::offset(7*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'teamplayer_9'){
            $models = TeamTeamPlayer::offset(8*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'teamplayer_10'){
            $models = TeamTeamPlayer::offset(9*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'teamplayer_11'){
            $models = TeamTeamPlayer::offset(10*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }





        if($param == 'teamseason_1'){
            $models = TeamTeamSeason::offset(0*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'teamseason_2'){
            $models = TeamTeamSeason::offset(1*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'teamseason_3'){
            $models = TeamTeamSeason::offset(2*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'teamseason_4'){
            $models = TeamTeamSeason::offset(3*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'teamseason_5'){
            $models = TeamTeamSeason::offset(4*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'teamseason_6'){
            $models = TeamTeamSeason::offset(5*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'teamseason_7'){
            $models = TeamTeamSeason::offset(6*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }





        if($param == 'team_1'){
            $models = Team::offset(0*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'team_2'){
            $models = Team::offset(1*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'team_3'){
            $models = Team::offset(2*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'team_4'){
            $models = Team::offset(3*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }





        // Transfer - 210 K
        if($param == 'transfer_1'){
            $models = Transfer::offset(0*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->replied_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'transfer_2'){
            $models = Transfer::offset(1*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->replied_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'transfer_3'){
            $models = Transfer::offset(2*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->replied_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'transfer_4'){
            $models = Transfer::offset(3*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->replied_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'transfer_5'){
            $models = Transfer::offset(4*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->replied_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'transfer_6'){
            $models = Transfer::offset(5*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->replied_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'transfer_7'){
            $models = Transfer::offset(6*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->replied_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'transfer_8'){
            $models = Transfer::offset(7*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->replied_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'transfer_9'){
            $models = Transfer::offset(8*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->replied_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'transfer_10'){
            $models = Transfer::offset(9*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->replied_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'transfer_11'){
            $models = Transfer::offset(10*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->replied_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'transfer_12'){
            $models = Transfer::offset(11*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->replied_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'transfer_13'){
            $models = Transfer::offset(12*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->replied_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'transfer_14'){
            $models = Transfer::offset(13*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->replied_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'transfer_15'){
            $models = Transfer::offset(14*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->replied_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'transfer_16'){
            $models = Transfer::offset(15*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->replied_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'transfer_17'){
            $models = Transfer::offset(16*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->replied_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'transfer_18'){
            $models = Transfer::offset(17*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->replied_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'transfer_19'){
            $models = Transfer::offset(18*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->replied_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'transfer_20'){
            $models = Transfer::offset(19*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->replied_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'transfer_21'){
            $models = Transfer::offset(20*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->replied_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'transfer_22'){
            $models = Transfer::offset(21*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->replied_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }





        // User - 220 K
        if($param == 'user_1'){
            $models = User::offset(0*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'user_2'){
            $models = User::offset(1*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'user_3'){
            $models = User::offset(2*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'user_4'){
            $models = User::offset(3*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'user_5'){
            $models = User::offset(4*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'user_6'){
            $models = User::offset(5*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'user_7'){
            $models = User::offset(6*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'user_8'){
            $models = User::offset(7*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'user_9'){
            $models = User::offset(8*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'user_10'){
            $models = User::offset(9*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'user_11'){
            $models = User::offset(10*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'user_12'){
            $models = User::offset(11*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'user_13'){
            $models = User::offset(12*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'user_14'){
            $models = User::offset(13*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'user_15'){
            $models = User::offset(14*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'user_16'){
            $models = User::offset(15*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'user_17'){
            $models = User::offset(16*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'user_18'){
            $models = User::offset(17*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'user_19'){
            $models = User::offset(18*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'user_20'){
            $models = User::offset(19*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'user_21'){
            $models = User::offset(20*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'user_22'){
            $models = User::offset(21*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'user_23'){
            $models = User::offset(22*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }





        if($param == 'videogalleryitem_1'){
            $models = VideoGalleryItem::offset(0*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }

        if($param == 'videogalleryitem_2'){
            $models = VideoGalleryItem::offset(1*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
                $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
                $model->save();
            }
        }




        echo '<a href="'.url('/date').'/'.$forward.'">Sıradaki</a>';

        echo '<script>document.querySelector("body").onload = function(){
            console.log("asd");
            setTimeout(function(){  window.location = "'.url('/date').'/'.$forward.'"; }, 2000);
        };</script>';

        dd('OK: '.$param);



        //dd($teams);
    }

    public function checkModelDate($modelName){

        $modelName = '\\App\\'.$modelName;
        $models = $modelName::whereNotNull('created_at')->orWhereNotNull('updated_at')->get();
        foreach ($models as $model) {
            $model->timestamps = false;
            $model->created_at = Carbon::parse($model->created_at)->copy()->format('Y-m-d H:i:sO');
            $model->updated_at = Carbon::parse($model->updated_at)->copy()->format('Y-m-d H:i:sO');
            $model->save();
        }

    }

    public function playerDegerHesapla($player_id) {

        echo 'Ayın MVPsi, Oyuncu Ayın Centilmeni/Mevki/Golü, Sezonun MVPsi, Oyuncu Sezonun Centilmeni/Mevki/Golü bilgileri veritabanında mevcut değil. Bu sebeple hesaplama dışında tutulmuştur.';

        $pointTotalPlayer = 100000;

        $pointMatch = 5000;
        $pointMatchRating = 3000;
        $pointMatchSave = 2000;

        $pointGoal = 3000;

        $pointYellowCard = -5000;
        $pointRedCard = -15000;

        $panoramaPoint = array(1 => array(8000, 'Maçın Gümüş Oyuncusu'), 2 => array(5000, 'Maçın Golü'), 3 => array(15000, 'Maçın Altın Oyuncusu'), 4 => array(5000, 'Maçın Dinamik Oyuncusu'), 5 => array(5000, 'Maçın Defansı'), 6 => array(5000, 'Maçın Orta Sahası'), 7 => array(5000, 'Maçın Forveti'), 8 => array(5000, 'Maçın Kalecisi'));

        $seasonCross = array(4, 3.5, 2.5, 1.5);
        $seasonCrossIndex = 0;

        $seasonTRCross = array(10, 3);
        $seasonTRCrossIndex = 0;

        $seasonList = array();
        $seasonTRList = array();

        $player = Player::find($player_id);
        $user = $player->user;
        $league_provinces = $user->profile->province->league_province;

        /*********** Sezon sıralama ve çarpan değerleri hesaplaması ***********/
        dump('(id: '.$user->id.') (player_id: '.$user->player->id.') '.$user->first_name.' '.$user->last_name.' ('.$user->profile->province->id.' - '.$user->profile->province->name.')');

        // Türkiye Finalleri Ligi ve Avrupa Finalleri Ligi ayrı tutulacak.
        foreach ($league_provinces as $league_province) {

            if($league_province->league->league_province->count() == 1){
                
                //dump($league_province->league->name);
                $seasons = $league_province->league->seasonApi;
                foreach ($seasons as $season) {

                    if($season->polymorphic_ctype_id == 32){ $seasonType = 'elimination'; }else if($season->polymorphic_ctype_id == 33){ $seasonType = 'fixture'; }else if($season->polymorphic_ctype_id == 34){ $seasonType = 'point'; }
                    //DETAY dump('('.$seasonCross[$seasonCrossIndex].') (id:'.$season->id.') - '.$season->year.' - '.$season->name.' - '.$seasonType);
                    $seasonList[] = array($seasonCross[$seasonCrossIndex], $season->id, $seasonType, ($season->year.' - '.$season->name), $season->year);

                    if(!empty($season->league_eliminationseasonApi)){

                        if($season->league_eliminationseasonApi->season_ptr->polymorphic_ctype_id == 32){ $seasonRType = 'elimination'; }else if($season->league_eliminationseasonApi->season_ptr->polymorphic_ctype_id == 33){ $seasonRType = 'fixture'; }else if($season->league_eliminationseasonApi->season_ptr->polymorphic_ctype_id == 34){ $seasonRType = 'point'; }

                        //DETAY dump('('.$seasonCross[$seasonCrossIndex].') (id:'.$season->league_eliminationseasonApi->season_ptr->id.') - '.' ---- '.$season->league_eliminationseasonApi->season_ptr->year.' - '.$season->league_eliminationseasonApi->season_ptr->name.' - '.$seasonRType);

                        $seasonList[] = array($seasonCross[$seasonCrossIndex], $season->league_eliminationseasonApi->season_ptr->id, $seasonRType, ($season->league_eliminationseasonApi->season_ptr->year.' - '.$season->league_eliminationseasonApi->season_ptr->name), $season->league_eliminationseasonApi->season_ptr->year );

                    }

                    if(count($seasonCross)-1 > $seasonCrossIndex){
                        $seasonCrossIndex++;
                    }
                }
                
                //DETAY dump('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'); 
                $all_seasons = $league_province->league->season->where('active', true);
                foreach ($all_seasons as $season) {

                    if($season->polymorphic_ctype_id == 32){ $seasonEType = 'elimination'; }else if($season->polymorphic_ctype_id == 33){ $seasonEType = 'fixture'; }else if($season->polymorphic_ctype_id == 34){ $seasonEType = 'point'; }
                    $is_valid = array_search($season->id, array_column($seasonList, 1));
                    //DETAY dump('---('.end($seasonCross).') (id:'.$season->id.') - '.$season->year.' - '.$season->name.' - '.$seasonEType.' - '.$is_valid.' - '.( ($is_valid !== false) ? $is_valid : 'false') );
                    
                    if($is_valid === false){
                        $seasonList[] = array(end($seasonCross), $season->id, $seasonEType, ($season->year.' - '.$season->name), $season->year);
                    }
                }
                //DETAY dump($seasonList);
            }else if($league_province->league->league_province->count() > 1){
                //dump('???? '.$league_province->league->name);

                $seasonsTR = $league_province->league->seasonApi;
                foreach ($seasonsTR as $seasonTR) {

                    if($seasonTR->polymorphic_ctype_id == 32){ $seasonType = 'elimination'; }else if($seasonTR->polymorphic_ctype_id == 33){ $seasonType = 'fixture'; }else if($seasonTR->polymorphic_ctype_id == 34){ $seasonType = 'point'; }
                    //DETAY dump('('.$seasonTRCross[$seasonTRCrossIndex].') (id:'.$seasonTR->id.') - '.$seasonTR->year.' - '.$seasonTR->name.' - '.$seasonType);
                    $seasonTRList[] = array($seasonTRCross[$seasonTRCrossIndex], $seasonTR->id, $seasonType, ($seasonTR->year.' - '.$seasonTR->name), $seasonTR->year);

                    if(!empty($seasonTR->league_eliminationseasonApi)){

                        if($seasonTR->league_eliminationseasonApi->season_ptr->polymorphic_ctype_id == 32){ $seasonRType = 'elimination'; }else if($seasonTR->league_eliminationseasonApi->season_ptr->polymorphic_ctype_id == 33){ $seasonRType = 'fixture'; }else if($seasonTR->league_eliminationseasonApi->season_ptr->polymorphic_ctype_id == 34){ $seasonRType = 'point'; }

                        //DETAY dump('('.$seasonTRCross[$seasonTRCrossIndex].') (id:'.$seasonTR->league_eliminationseasonApi->season_ptr->id.') - '.' ---- '.$seasonTR->league_eliminationseasonApi->season_ptr->year.' - '.$seasonTR->league_eliminationseasonApi->season_ptr->name.' - '.$seasonRType);

                        $seasonTRList[] = array($seasonTRCross[$seasonTRCrossIndex], $seasonTR->league_eliminationseasonApi->season_ptr->id, $seasonRType, ($seasonTR->league_eliminationseasonApi->season_ptr->year.' - '.$seasonTR->league_eliminationseasonApi->season_ptr->name), $seasonTR->league_eliminationseasonApi->season_ptr->year );

                    }

                    if(count($seasonTRCross)-1 > $seasonTRCrossIndex){
                        $seasonTRCrossIndex++;
                    }
                }

                $all_seasonsTR = $league_province->league->season->where('active', true);
                foreach ($all_seasonsTR as $seasonTR) {

                    if($seasonTR->polymorphic_ctype_id == 32){ $seasonEType = 'elimination'; }else if($seasonTR->polymorphic_ctype_id == 33){ $seasonEType = 'fixture'; }else if($seasonTR->polymorphic_ctype_id == 34){ $seasonEType = 'point'; }
                    $is_valid = array_search($seasonTR->id, array_column($seasonTRList, 1));
                    //DETAY dump('---('.end($seasonTRCross).') (id:'.$seasonTR->id.') - '.$seasonTR->year.' - '.$seasonTR->name.' - '.$seasonEType.' - '.$is_valid.' - '.( ($is_valid !== false) ? $is_valid : 'false') );
                    
                    if($is_valid === false){
                        $seasonTRList[] = array(end($seasonTRCross), $seasonTR->id, $seasonEType, ($seasonTR->year.' - '.$seasonTR->name), $seasonTR->year);
                    }
                }
                //DETAY dump($seasonTRList);
            }else{
                //DETAY dump('sıkıntı olmasın !!!');                
                //DETAY dump('???? '.$league_province->league->name);
            }
            //DETAY dump('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'); 
        }
        /*********** Sezon sıralama ve çarpan değerleri hesaplaması ***********/
        /*
            - Sezon Çarpanı
        */
        $matchTotalPointArray = array();

        foreach ($user->player->match_player as $match_player) {

            $match = $match_player->match;
            $season = $match_player->match->season;

            $is_valid = array_search($season->id, array_column($seasonList, 1));

            if($season->year >= 2017 && $is_valid !== false){

                dump('Sezon ID: '.$seasonList[$is_valid][1].' | Adı: '.$seasonList[$is_valid][3].' | Çarpanı: '.$seasonList[$is_valid][0]);
                dump('Maç ID: '.$match_player->match_id);
                dump('Oyuncu oynadığı maç : +'.$pointMatch);
                dump('Maçtaki Rating : (['.((is_null($match_player->rating) ? 0 : $match_player->rating)).']-5) * +'.$pointMatchRating.' (Rating 6 dan küçükse hesaplama dışı tutulur.)');
                dump('Oyuncu Gol : ['.$match_player->match_action_goal->count().'] * +'.$pointGoal);
                dump('Oyuncu Kurtarış : ['.((is_null($match_player->saving) ? 0 : $match_player->saving)).'] * +'.$pointMatchSave);
                dump('Oyuncu Sarı Kart : ['.$match_player->match_action_yellow_card->count().'] * '.$pointYellowCard);
                dump('Oyuncu Kırmızı Kart : ['.$match_player->match_action_red_card->count().'] * '.$pointRedCard);

                $panoramaTotalPoint = 0;
                foreach ($match_player->match_player_panorama as $panorama) {
                    $panoramaTotalPoint += $panoramaPoint[$panorama->panorama_id][0];
                    dump($panoramaPoint[$panorama->panorama_id][1].' +'.$panoramaPoint[$panorama->panorama_id][0]);
                }

                if(is_null($match_player->rating)){
                    $ratingPoint = 0;
                }else{
                    if($match_player->rating >= 6){ //(Rating 6 dan küçükse hesaplama dışı tutulur.)
                        $ratingPoint = ($match_player->rating - 5) * $pointMatchRating;
                    }else{
                        $ratingPoint = 0;
                    }
                }
                
                $matchPoint = $seasonList[$is_valid][0] * (
                    $pointMatch + 
                    $ratingPoint + 
                    ($match_player->match_action_goal->count() * $pointGoal) + 
                    (((is_null($match_player->saving) ? 0 : $match_player->saving)) * $pointMatchSave) + 
                    ($match_player->match_action_yellow_card->count() * $pointYellowCard) + 
                    ($match_player->match_action_red_card->count() * $pointRedCard) +
                    $panoramaTotalPoint
                );

                dump('TOPLAM PUAN: '. $matchPoint);
                $matchTotalPointArray[] = array('match_id' => $match_player->match_id, 'point' => $matchPoint);
                dump('***************************************************************************');
            }
        }

        foreach ($seasonList as $seasonl) {
            
            if($seasonl[4] < 2017){

                $playerHistory = TeamPlayerTeamHistory::where('player_id', $player->id)->where('season_id', $seasonl[1])->get();
                $playerHistoryPoint = 0;

                //(2019 ve öncesinde panorama kaydı olmadığından bu bölümde hesaplama dışı bırakıldı.)
                foreach ($playerHistory as $history) {
                    if(is_null($history->rating)){
                        $ratingPoint = 0;
                    }else{
                        if($history->rating >= 6){ //(Rating 6 dan küçükse hesaplama dışı tutulur.)
                            $ratingPoint = ($history->rating - 5) * $pointMatchRating;
                        }else{
                            $ratingPoint = 0;
                        }
                    }

                    $playerHistoryPoint += end($seasonCross) * (
                        ($history->match * $pointMatch) + 
                        ($history->match * $ratingPoint) + 
                        ($history->goal * $pointGoal) + 
                        (((is_null($history->gk_saving) ? 0 : $history->gk_saving)) * $pointMatchSave) + 
                        ($history->yellow_card * $pointYellowCard) + 
                        ($history->red_card * $pointRedCard)
                    );

                    dump('Hesaplanmış Tüm Sezon Puanlarıdır.');
                    dump('Sezon ID: '.$seasonl[1].' | Adı: '.$seasonl[3].' | Çarpanı: '.$seasonl[0]);

                    dump('Oyuncu oynadığı maç : MS['.$history->match.'] * +'.$pointMatch);
                    dump('Maçtaki Rating : MS['.$history->match.'] * (['.((is_null($history->rating) ? 0 : $history->rating)).']-5) * +'.$pointMatchRating.' (Rating 6 dan küçükse hesaplama dışı tutulur.)');
                    dump('Oyuncu Gol : ['.$history->goal.'] * +'.$pointGoal);
                    dump('Oyuncu Kurtarış : ['.((is_null($history->gk_saving) ? 0 : $history->gk_saving)).'] * +'.$pointMatchSave);
                    dump('Oyuncu Sarı Kart : ['.$history->yellow_card.'] * '.$pointYellowCard);
                    dump('Oyuncu Kırmızı Kart : ['.$history->red_card.'] * '.$pointRedCard);


                    dump('TOPLAM PUAN: '. $playerHistoryPoint);
                    $matchTotalPointArray[] = array('season_id' => $seasonl[1], 'point' => $playerHistoryPoint);

                    dump('***************************************************************************');
                }
            }
        }

        foreach ($seasonTRList as $seasonlTR) {
            
            $playerHistoryTR = TeamPlayerTeamHistory::where('player_id', $player->id)->where('season_id', $seasonlTR[1])->get();
            $playerHistoryTRPoint = 0;

            foreach ($playerHistoryTR as $historyTR) {
                if(is_null($historyTR->rating)){
                    $ratingPoint = 0;
                }else{
                    if($historyTR->rating >= 6){ //(Rating 6 dan küçükse hesaplama dışı tutulur.)
                        $ratingPoint = ($historyTR->rating - 5) * $pointMatchRating;
                    }else{
                        $ratingPoint = 0;
                    }
                }

                $playerHistoryTRPoint += end($seasonTRCross) * (
                    ($historyTR->match * $pointMatch) + 
                    ($historyTR->match * $ratingPoint) + 
                    ($historyTR->goal * $pointGoal) + 
                    (((is_null($historyTR->gk_saving) ? 0 : $historyTR->gk_saving)) * $pointMatchSave) + 
                    ($historyTR->yellow_card * $pointYellowCard) + 
                    ($historyTR->red_card * $pointRedCard)
                );

                dump('Hesaplanmış Tüm Sezon Puanlarıdır. (Türkiye Finalleri)');
                dump('Sezon ID: '.$seasonlTR[1].' | Adı: '.$seasonlTR[3].' | Çarpanı: '.$seasonlTR[0]);

                dump('Oyuncu oynadığı maç : MS['.$historyTR->match.'] * +'.$pointMatch);
                dump('Maçtaki Rating : MS['.$historyTR->match.'] * (['.((is_null($historyTR->rating) ? 0 : $historyTR->rating)).']-5) * +'.$pointMatchRating.' (Rating 6 dan küçükse hesaplama dışı tutulur.)');
                dump('Oyuncu Gol : ['.$historyTR->goal.'] * +'.$pointGoal);
                dump('Oyuncu Kurtarış : ['.((is_null($historyTR->gk_saving) ? 0 : $historyTR->gk_saving)).'] * +'.$pointMatchSave);
                dump('Oyuncu Sarı Kart : ['.$historyTR->yellow_card.'] * '.$pointYellowCard);
                dump('Oyuncu Kırmızı Kart : ['.$historyTR->red_card.'] * '.$pointRedCard);


                dump('TOPLAM PUAN: '. $playerHistoryTRPoint);
                $matchTotalPointArray[] = array('season_id_tr' => $seasonlTR[1], 'point' => $playerHistoryTRPoint);

                dump('***************************************************************************');
            }
        }

        dump('Maç Başı Puan Özeti');
        dump($matchTotalPointArray);

        foreach ($matchTotalPointArray as $value) {
            //dump($value['point']);
            $pointTotalPlayer += $value['point'];
            //dump($pointTotalPlayer);
        }

        $calculetedTotalPoint = (round($pointTotalPlayer / 10000)*10);
        
        if($calculetedTotalPoint < 1000){
            dump('GENEL TOPLAM PUAN: '. $calculetedTotalPoint .'K RB$');
        }else{
            $calculetedTotalPoint = round($calculetedTotalPoint/100)/10;
            dump('GENEL TOPLAM PUAN: '. $calculetedTotalPoint .'M RB$');
        }

        echo '<a href="'.url('/playerdegerguncelle').'/'.$player_id.'">'.$user->first_name.' '.$user->last_name.' değerini güncelle. </a> (player_id: '.$user->player->id.') '.$user->first_name.' '.$user->last_name.' ('.$user->profile->province->name.')<br>';
        dd('GENEL TOPLAM PUAN: '. $pointTotalPlayer.' RB$');
    }

    public function playerDegerGuncelle($player_id){

        $player = Player::find($player_id);

        if(!empty($player)){
            $playerPoint = $this->hesapla($player_id);
            $player->value = $playerPoint;
            $player->save();
            echo 'Oyuncu Değeri başarıyla güncellendi.';
        }else{
            echo 'Oyuncu bulunamadı.';
        }
    }

    public function degerUygula($offset, $limit) {

        $users = User::where('is_active', true)->offset($offset)->limit($limit)->orderBy('id')->get();
        //dump($users);
        echo 'OK ---- offset:'.$offset.' - limit:'.$limit.'</br>';
        echo '<a href="'.url('/degeruygula').'/'.($offset+$limit).'/'.$limit.'">Sıradaki</a> (offset:'.($offset+$limit).' - limit:'.$limit.') <br>';
        foreach ($users as $user) {

            $isPlayer = false;
            $playerPoint = 0;
            if(!empty($user->player) && !empty($user->profile->province_id)){
                $isPlayer = true;
                $playerPoint = $this->hesapla($user->player->id);
                $user->player->value = $playerPoint;
                $user->player->save();
                echo 'UID:'.$user->id.' / '.$user->first_name.' '.$user->last_name.' ('.$user->player->id.') #'.$isPlayer.'<br>';
            }else{
                $isPlayer = false;
                echo 'UID:'.$user->id.' / '.$user->first_name.' '.$user->last_name.' # Oyuncu Değil<br>';
            }

            if($isPlayer){
                echo '<script>document.querySelector("body").onload = function(){
                    console.log("asd");
                    setTimeout(function(){  window.location = "'.url('/degeruygula').'/'.($offset+$limit).'/'.$limit.'"; }, 5000);
                };</script>';
            }
        }
    }

    public function hesapla($player_id) {

        // AÇÇ echo 'Ayın MVPsi, Oyuncu Ayın Centilmeni/Mevki/Golü, Sezonun MVPsi, Oyuncu Sezonun Centilmeni/Mevki/Golü bilgileri veritabanında mevcut değil. Bu sebeple hesaplama dışında tutulmuştur.';

        $pointTotalPlayer = 100000;

        $pointMatch = 5000;
        $pointMatchRating = 3000;
        $pointMatchSave = 2000;

        $pointGoal = 3000;

        $pointYellowCard = -5000;
        $pointRedCard = -15000;

        $panoramaPoint = array(1 => array(8000, 'Maçın Gümüş Oyuncusu'), 2 => array(5000, 'Maçın Golü'), 3 => array(15000, 'Maçın Altın Oyuncusu'), 4 => array(5000, 'Maçın Dinamik Oyuncusu'), 5 => array(5000, 'Maçın Defansı'), 6 => array(5000, 'Maçın Orta Sahası'), 7 => array(5000, 'Maçın Forveti'), 8 => array(5000, 'Maçın Kalecisi'));

        $seasonCross = array(4, 3.5, 2.5, 1.5);
        $seasonCrossIndex = 0;

        $seasonTRCross = array(10, 3);
        $seasonTRCrossIndex = 0;

        $seasonList = array();
        $seasonTRList = array();

        $player = Player::find($player_id);
        $user = $player->user;
        $league_provinces = $user->profile->province->league_province;

        /*********** Sezon sıralama ve çarpan değerleri hesaplaması ***********/
        // AÇÇ dump('(id: '.$user->id.') (player_id: '.$user->player->id.') '.$user->first_name.' '.$user->last_name.' ('.$user->profile->province->id.' - '.$user->profile->province->name.')');

        // Türkiye Finalleri Ligi ve Avrupa Finalleri Ligi ayrı tutulacak.
        foreach ($league_provinces as $league_province) {

            if($league_province->league->league_province->count() == 1){
                
                // AÇÇ dump($league_province->league->name);
                $seasons = $league_province->league->seasonApi;
                foreach ($seasons as $season) {

                    if($season->polymorphic_ctype_id == 32){ $seasonType = 'elimination'; }else if($season->polymorphic_ctype_id == 33){ $seasonType = 'fixture'; }else if($season->polymorphic_ctype_id == 34){ $seasonType = 'point'; }
                    // dump('('.$seasonCross[$seasonCrossIndex].') (id:'.$season->id.') - '.$season->year.' - '.$season->name.' - '.$seasonType);
                    $seasonList[] = array($seasonCross[$seasonCrossIndex], $season->id, $seasonType, ($season->year.' - '.$season->name), $season->year);

                    if(!empty($season->league_eliminationseasonApi)){

                        if($season->league_eliminationseasonApi->season_ptr->polymorphic_ctype_id == 32){ $seasonRType = 'elimination'; }else if($season->league_eliminationseasonApi->season_ptr->polymorphic_ctype_id == 33){ $seasonRType = 'fixture'; }else if($season->league_eliminationseasonApi->season_ptr->polymorphic_ctype_id == 34){ $seasonRType = 'point'; }

                        // dump('('.$seasonCross[$seasonCrossIndex].') (id:'.$season->league_eliminationseasonApi->season_ptr->id.') - '.' ---- '.$season->league_eliminationseasonApi->season_ptr->year.' - '.$season->league_eliminationseasonApi->season_ptr->name.' - '.$seasonRType);

                        $seasonList[] = array($seasonCross[$seasonCrossIndex], $season->league_eliminationseasonApi->season_ptr->id, $seasonRType, ($season->league_eliminationseasonApi->season_ptr->year.' - '.$season->league_eliminationseasonApi->season_ptr->name), $season->league_eliminationseasonApi->season_ptr->year );

                    }

                    if(count($seasonCross)-1 > $seasonCrossIndex){
                        $seasonCrossIndex++;
                    }
                }
                
                // dump('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'); 
                
                $all_seasons = $league_province->league->season->where('active', true);
                foreach ($all_seasons as $season) {

                    if($season->polymorphic_ctype_id == 32){ $seasonEType = 'elimination'; }else if($season->polymorphic_ctype_id == 33){ $seasonEType = 'fixture'; }else if($season->polymorphic_ctype_id == 34){ $seasonEType = 'point'; }
                    $is_valid = array_search($season->id, array_column($seasonList, 1));
                    // dump('---('.end($seasonCross).') (id:'.$season->id.') - '.$season->year.' - '.$season->name.' - '.$seasonEType.' - '.$is_valid.' - '.( ($is_valid !== false) ? $is_valid : 'false') );
                    
                    if($is_valid === false){
                        $seasonList[] = array(end($seasonCross), $season->id, $seasonEType, ($season->year.' - '.$season->name), $season->year);
                    }
                }
                
                // AÇÇ dump($seasonList);
                

            }else if($league_province->league->league_province->count() > 1){
                // AÇÇ dump('???? '.$league_province->league->name);

                $seasonsTR = $league_province->league->seasonApi;
                foreach ($seasonsTR as $seasonTR) {

                    if($seasonTR->polymorphic_ctype_id == 32){ $seasonType = 'elimination'; }else if($seasonTR->polymorphic_ctype_id == 33){ $seasonType = 'fixture'; }else if($seasonTR->polymorphic_ctype_id == 34){ $seasonType = 'point'; }
                    // dump('('.$seasonTRCross[$seasonTRCrossIndex].') (id:'.$seasonTR->id.') - '.$seasonTR->year.' - '.$seasonTR->name.' - '.$seasonType);
                    $seasonTRList[] = array($seasonTRCross[$seasonTRCrossIndex], $seasonTR->id, $seasonType, ($seasonTR->year.' - '.$seasonTR->name), $seasonTR->year);

                    if(!empty($seasonTR->league_eliminationseasonApi)){

                        if($seasonTR->league_eliminationseasonApi->season_ptr->polymorphic_ctype_id == 32){ $seasonRType = 'elimination'; }else if($seasonTR->league_eliminationseasonApi->season_ptr->polymorphic_ctype_id == 33){ $seasonRType = 'fixture'; }else if($seasonTR->league_eliminationseasonApi->season_ptr->polymorphic_ctype_id == 34){ $seasonRType = 'point'; }

                        // dump('('.$seasonTRCross[$seasonTRCrossIndex].') (id:'.$seasonTR->league_eliminationseasonApi->season_ptr->id.') - '.' ---- '.$seasonTR->league_eliminationseasonApi->season_ptr->year.' - '.$seasonTR->league_eliminationseasonApi->season_ptr->name.' - '.$seasonRType);

                        $seasonTRList[] = array($seasonTRCross[$seasonTRCrossIndex], $seasonTR->league_eliminationseasonApi->season_ptr->id, $seasonRType, ($seasonTR->league_eliminationseasonApi->season_ptr->year.' - '.$seasonTR->league_eliminationseasonApi->season_ptr->name), $seasonTR->league_eliminationseasonApi->season_ptr->year );

                    }

                    if(count($seasonTRCross)-1 > $seasonTRCrossIndex){
                        $seasonTRCrossIndex++;
                    }
                }


                $all_seasonsTR = $league_province->league->season->where('active', true);
                foreach ($all_seasonsTR as $seasonTR) {

                    if($seasonTR->polymorphic_ctype_id == 32){ $seasonEType = 'elimination'; }else if($seasonTR->polymorphic_ctype_id == 33){ $seasonEType = 'fixture'; }else if($seasonTR->polymorphic_ctype_id == 34){ $seasonEType = 'point'; }
                    $is_valid = array_search($seasonTR->id, array_column($seasonTRList, 1));
                    // dump('---('.end($seasonTRCross).') (id:'.$seasonTR->id.') - '.$seasonTR->year.' - '.$seasonTR->name.' - '.$seasonEType.' - '.$is_valid.' - '.( ($is_valid !== false) ? $is_valid : 'false') );
                    
                    if($is_valid === false){
                        $seasonTRList[] = array(end($seasonTRCross), $seasonTR->id, $seasonEType, ($seasonTR->year.' - '.$seasonTR->name), $seasonTR->year);
                    }
                }
                
                // AÇÇ dump($seasonTRList);

            }else{
                // AÇÇ dump('sıkıntı olmasın !!!');                
                // AÇÇ dump('???? '.$league_province->league->name);
            }
            // AÇÇ dump('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'); 

        }
        /*********** Sezon sıralama ve çarpan değerleri hesaplaması ***********/

        //dd($user->player->match_player);

     

        /*
            - Sezon Çarpanı
        */
        $matchTotalPointArray = array();

        foreach ($user->player->match_player as $match_player) {

            $match = $match_player->match;
            $season = $match_player->match->season;
            if(!empty($season)){
                $is_valid = array_search($season->id, array_column($seasonList, 1));

                if($season->year >= 2017 && $is_valid !== false){

                    //dump($seasonList[$is_valid]);
                    // AÇÇ dump('Sezon ID: '.$seasonList[$is_valid][1].' | Adı: '.$seasonList[$is_valid][3].' | Çarpanı: '.$seasonList[$is_valid][0]);
                    // AÇÇ dump('Maç ID: '.$match_player->match_id);

                    // AÇÇ dump('Oyuncu oynadığı maç : +'.$pointMatch);
                    // AÇÇ dump('Maçtaki Rating : (['.((is_null($match_player->rating) ? 0 : $match_player->rating)).']-5) * +'.$pointMatchRating.' (Rating 6 dan küçükse hesaplama dışı tutulur.)');
                    // AÇÇ dump('Oyuncu Gol : ['.$match_player->match_action_goal->count().'] * +'.$pointGoal);
                    // AÇÇ dump('Oyuncu Kurtarış : ['.((is_null($match_player->saving) ? 0 : $match_player->saving)).'] * +'.$pointMatchSave);
                    // AÇÇ dump('Oyuncu Sarı Kart : ['.$match_player->match_action_yellow_card->count().'] * '.$pointYellowCard);
                    // AÇÇ dump('Oyuncu Kırmızı Kart : ['.$match_player->match_action_red_card->count().'] * '.$pointRedCard);

                    $panoramaTotalPoint = 0;
                    foreach ($match_player->match_player_panorama as $panorama) {
                        $panoramaTotalPoint += $panoramaPoint[$panorama->panorama_id][0];
                        // AÇÇ dump($panoramaPoint[$panorama->panorama_id][1].' +'.$panoramaPoint[$panorama->panorama_id][0]);
                    }

                    if(is_null($match_player->rating)){
                        $ratingPoint = 0;
                    }else{
                        if($match_player->rating >= 6){ //(Rating 6 dan küçükse hesaplama dışı tutulur.)
                            $ratingPoint = ($match_player->rating - 5) * $pointMatchRating;
                        }else{
                            $ratingPoint = 0;
                        }
                    }
                    
                    $matchPoint = $seasonList[$is_valid][0] * (
                        $pointMatch + 
                        $ratingPoint + 
                        ($match_player->match_action_goal->count() * $pointGoal) + 
                        (((is_null($match_player->saving) ? 0 : $match_player->saving)) * $pointMatchSave) + 
                        ($match_player->match_action_yellow_card->count() * $pointYellowCard) + 
                        ($match_player->match_action_red_card->count() * $pointRedCard) +
                        $panoramaTotalPoint
                    );

                    // AÇÇ dump('TOPLAM PUAN: '. $matchPoint);
                    $matchTotalPointArray[] = array('match_id' => $match_player->match_id, 'point' => $matchPoint);
                    //dump("match id: ".$match_player->match_id." -- (".$season->id.") (".$seasonType.") (".Carbon::parse($season->start_date)->format('d.m.Y')." / ".Carbon::parse($season->end_date)->format('d.m.Y').") ".$season->name);
                    //$this->seasonCarpan($season);
                    // AÇÇ dump('***************************************************************************');
                }
            }
        }

        //dump($seasonList);

        foreach ($seasonList as $seasonl) {
            
            if($seasonl[4] < 2017){
                //dump($seasonl);
                $playerHistory = TeamPlayerTeamHistory::where('player_id', $player->id)->where('season_id', $seasonl[1])->get();
                //dump($playerHistory);

                $playerHistoryPoint = 0;

                //(2019 ve öncesinde panorama kaydı olmadığından bu bölümde hesaplama dışı bırakıldı.)
                foreach ($playerHistory as $history) {
                    if(is_null($history->rating)){
                        $ratingPoint = 0;
                    }else{
                        if($history->rating >= 6){ //(Rating 6 dan küçükse hesaplama dışı tutulur.)
                            $ratingPoint = ($history->rating - 5) * $pointMatchRating;
                        }else{
                            $ratingPoint = 0;
                        }
                    }

                    $playerHistoryPoint += end($seasonCross) * (
                        ($history->match * $pointMatch) + 
                        ($history->match * $ratingPoint) + 
                        ($history->goal * $pointGoal) + 
                        (((is_null($history->gk_saving) ? 0 : $history->gk_saving)) * $pointMatchSave) + 
                        ($history->yellow_card * $pointYellowCard) + 
                        ($history->red_card * $pointRedCard)
                    );

                    // AÇÇ dump('Hesaplanmış Tüm Sezon Puanlarıdır.');
                    // AÇÇ dump('Sezon ID: '.$seasonl[1].' | Adı: '.$seasonl[3].' | Çarpanı: '.$seasonl[0]);

                    // AÇÇ dump('Oyuncu oynadığı maç : MS['.$history->match.'] * +'.$pointMatch);
                    // AÇÇ dump('Maçtaki Rating : MS['.$history->match.'] * (['.((is_null($history->rating) ? 0 : $history->rating)).']-5) * +'.$pointMatchRating.' (Rating 6 dan küçükse hesaplama dışı tutulur.)');
                    // AÇÇ dump('Oyuncu Gol : ['.$history->goal.'] * +'.$pointGoal);
                    // AÇÇ dump('Oyuncu Kurtarış : ['.((is_null($history->gk_saving) ? 0 : $history->gk_saving)).'] * +'.$pointMatchSave);
                    // AÇÇ dump('Oyuncu Sarı Kart : ['.$history->yellow_card.'] * '.$pointYellowCard);
                    // AÇÇ dump('Oyuncu Kırmızı Kart : ['.$history->red_card.'] * '.$pointRedCard);


                    // AÇÇ dump('TOPLAM PUAN: '. $playerHistoryPoint);
                    $matchTotalPointArray[] = array('season_id' => $seasonl[1], 'point' => $playerHistoryPoint);

                    // AÇÇ dump('***************************************************************************');

                }

            }
            
        }

        foreach ($seasonTRList as $seasonlTR) {
            
            //dump($seasonlTR);
            $playerHistoryTR = TeamPlayerTeamHistory::where('player_id', $player->id)->where('season_id', $seasonlTR[1])->get();
            //dump($playerHistoryTR);

            $playerHistoryTRPoint = 0;

            foreach ($playerHistoryTR as $historyTR) {
                if(is_null($historyTR->rating)){
                    $ratingPoint = 0;
                }else{
                    if($historyTR->rating >= 6){ //(Rating 6 dan küçükse hesaplama dışı tutulur.)
                        $ratingPoint = ($historyTR->rating - 5) * $pointMatchRating;
                    }else{
                        $ratingPoint = 0;
                    }
                }

                $playerHistoryTRPoint += end($seasonTRCross) * (
                    ($historyTR->match * $pointMatch) + 
                    ($historyTR->match * $ratingPoint) + 
                    ($historyTR->goal * $pointGoal) + 
                    (((is_null($historyTR->gk_saving) ? 0 : $historyTR->gk_saving)) * $pointMatchSave) + 
                    ($historyTR->yellow_card * $pointYellowCard) + 
                    ($historyTR->red_card * $pointRedCard)
                );

                // AÇÇ dump('Hesaplanmış Tüm Sezon Puanlarıdır. (Türkiye Finalleri)');
                // AÇÇ dump('Sezon ID: '.$seasonlTR[1].' | Adı: '.$seasonlTR[3].' | Çarpanı: '.$seasonlTR[0]);

                // AÇÇ dump('Oyuncu oynadığı maç : MS['.$historyTR->match.'] * +'.$pointMatch);
                // AÇÇ dump('Maçtaki Rating : MS['.$historyTR->match.'] * (['.((is_null($historyTR->rating) ? 0 : $historyTR->rating)).']-5) * +'.$pointMatchRating.' (Rating 6 dan küçükse hesaplama dışı tutulur.)');
                // AÇÇ dump('Oyuncu Gol : ['.$historyTR->goal.'] * +'.$pointGoal);
                // AÇÇ dump('Oyuncu Kurtarış : ['.((is_null($historyTR->gk_saving) ? 0 : $historyTR->gk_saving)).'] * +'.$pointMatchSave);
                // AÇÇ dump('Oyuncu Sarı Kart : ['.$historyTR->yellow_card.'] * '.$pointYellowCard);
                // AÇÇ dump('Oyuncu Kırmızı Kart : ['.$historyTR->red_card.'] * '.$pointRedCard);


                // AÇÇ dump('TOPLAM PUAN: '. $playerHistoryTRPoint);
                $matchTotalPointArray[] = array('season_id_tr' => $seasonlTR[1], 'point' => $playerHistoryTRPoint);

                // AÇÇ dump('***************************************************************************');

            }

        }


        // AÇÇ dump('--------------------------');
        // AÇÇ dump('Maç Başı Puan Özeti');
        // AÇÇ dump($matchTotalPointArray);

        foreach ($matchTotalPointArray as $value) {
            //dump($value['point']);
            $pointTotalPlayer += $value['point'];
            //dump($pointTotalPlayer);
        }

        /*
        $calculetedTotalPoint = (round($pointTotalPlayer / 10000)*10);
        
        if($calculetedTotalPoint < 1000){
            // AÇÇ dump('GENEL TOPLAM PUAN: '. $calculetedTotalPoint .'K RB$');
        }else{
            $calculetedTotalPoint = round($calculetedTotalPoint/100)/10;
            // AÇÇ dump('GENEL TOPLAM PUAN: '. $calculetedTotalPoint .'M RB$');
        }
        */

       
        
        return $pointTotalPlayer;
        //dd('GENEL TOPLAM PUAN: '.$pointTotalPlayer.' RB$');
    }

    public function videoUrlDegistir($offset, $limit) {

        $match_videos = MatchVideo::where('full_embed_code', 'ilike', '%bo3studio%')->orWhere('summary_embed_code', 'ilike', '%bo3studio%')->offset($offset)->limit($limit)->orderBy('id')->get();

        $match_videos_count = MatchVideo::count();
        //dump($users);
        echo 'OK ---- offset:'.$offset.' - limit:'.$limit.' - count:'.$match_videos_count.'</br>';
        echo '<a href="'.url('/videourldegistir').'/'.($offset+$limit).'/'.$limit.'">Sıradaki</a> (offset:'.($offset+$limit).' - limit:'.$limit.') <br>';
        
        foreach ($match_videos as $video) {
            if(str_contains($video->full_embed_code, 'bo3studio')){
                $video->full_embed_code = str_replace_first('bo3studio.com', 'api.yazbu.com', $video->full_embed_code);
                $video->save();
            }
            if(str_contains($video->summary_embed_code, 'bo3studio')){
                $video->summary_embed_code = str_replace_first('bo3studio.com', 'api.yazbu.com', $video->summary_embed_code);
                $video->save();
            }
        }

        echo '<script>document.querySelector("body").onload = function(){
            console.log("asd");
            setTimeout(function(){  window.location = "'.url('/videourldegistir').'/'.($offset+$limit).'/'.$limit.'"; }, 5000);
        };</script>';
    }

    public function takimlariAc() {

        $teams = Team::whereNull('created_at')->get();
        foreach ($teams as $team) {
            $team->created_at = Carbon::now()->format('Y-m-d H:i:s').'+00';
            $team->updated_at = Carbon::now()->format('Y-m-d H:i:s').'+00';
            $team->save();
        }
        dd($teams);
    }

    
    public function sezonList($mode = null) { // null, autorun, cronrun

        $dt = Carbon::now()->format('Y-m-d');
        $seasons = Season::where('polymorphic_ctype_id', '!=', 32)->whereDate('end_date', '>=', $dt)->orderBy('id', 'DEC')->get();

        echo "<h2> Sezon Puan Hesapla </h2><br>";

        foreach ($seasons as $season) {

            // /sezonpuanhesapla/{sezon_id}/{mode}

            echo " <a href='/sezonpuanhesapla/".$season->id."/' target='_blank'>preview</a> ";
            echo " > ";
            echo " <a href='/sezonpuanhesapla/".$season->id."/autorun' target='_blank'>autorun</a> ";
            echo " > ";
            echo " <a href='/sezonpuanhesapla/".$season->id."/cronrun' target='_blank'>cronrun</a> ";

            echo " --> (".$season->start_date." - ".$season->end_date.") ".$season->league->name." - ".$season->year." - ".$season->name." (".$season->match->count().")";
            echo "<br><br>";
            
        }

        echo "<h2> Sezon İstatistik Hesapla </h2><br>";

        foreach ($seasons as $season) {

            // /sezonistatistikhesapla/{sezon_id}/{offset}/{limit}/{mode}

            echo " <a href='/sezonistatistikhesapla/".$season->id."/0/20' target='_blank'>preview</a> ";
            echo " > ";
            echo " <a href='/sezonistatistikhesapla/".$season->id."/0/20/autorun' target='_blank'>autorun</a> ";
            echo " > ";
            echo " <a href='/sezonistatistikhesapla/".$season->id."/0/10000/cronrun' target='_blank'>cronrun</a> ";

            echo " --> (".$season->start_date." - ".$season->end_date.") ".$season->league->name." - ".$season->year." - ".$season->name." (".$season->match->count().")";
            echo "<br><br>";
            
        }
    }

    public function sezonPuanHesapla($sezon_id, $mode = null) { // null, autorun, cronrun
        ini_set('max_execution_time', 120);
        $season = Season::find($sezon_id);

        $min_point = 0;
        if($season->polymorphic_ctype_id == 33){
            $min_point = $season->league_fixtureseason->min_team_point;
        }else if($season->polymorphic_ctype_id == 34){
            $min_point = $season->league_pointseason->min_team_point;
        }

        if($mode != 'cronrun'){
            dump('Season ID: '.$sezon_id.' - '.$season->name.' - Min Point: '.$min_point.' - Season Type: '.$season->content_type->model);
        }

        $i = 0;
        $istop = 100000;

        foreach ($season->matchOrdered as $match) {
            //dump($match);
            $marr = collect();
            $marr->model = 'Match';
            $marr->id = $match->id;

            $marr->date = $match->date;
            $marr->time = $match->time;
            $marr->completed = $match->completed;
            $marr->team1_goal = $match->team1_goal;
            $marr->team2_goal = $match->team2_goal;

            $marr->season_id = $match->season_id;
            $marr->team1_id = $match->team1_id;
            $marr->team2_id = $match->team2_id;
            $marr->team1_point = 0; //$match->team1_point;
            $marr->team2_point = 0; //$match->team2_point;
            $marr->team1_point_before = 0; //$match->team1_point_before;
            $marr->team2_point_before = 0; //$match->team2_point_before;


            $marr->completed_status = $match->completed_status;

            $this->matchTemp[] = $marr;

            $i++;
            if($i == $istop){
                break;
            }
        }

        $bonusDateControl = Carbon::parse($season->start_date);
        $season_end_date = Carbon::parse($season->end_date);

        $in = 0;
        //dump($season->end_date);

        while ($bonusDateControl->copy()->startOfMonth() < $season_end_date) {

            foreach ($this->matchTemp->where('date', '>=', $bonusDateControl->copy()->startOfMonth()->format('Y-m-d'))->where('date', '<=', $bonusDateControl->copy()->endOfMonth()->format('Y-m-d')) as $match) {
                $in++;
                
                $teamSeason1 = $this->teamSeasonTemp->where('season_id', $sezon_id)->where('team_id', $match->team1_id)->first();
                if(empty($teamSeason1)){
                    $ts1 = TeamTeamSeason::where('season_id', $sezon_id)->where('team_id', $match->team1_id)->first();
                                        
                    $teamSeason1 = collect();
                    $teamSeason1->id = $ts1->id;
                    $teamSeason1->model = 'TeamSeason';
                    $teamSeason1->point = $min_point;
                    $teamSeason1->season_id = $sezon_id;
                    $teamSeason1->team_id = $match->team1_id;

                    $teamSeason1->drawn = 0;
                    $teamSeason1->goal_against = 0;
                    $teamSeason1->goal_for = 0;
                    $teamSeason1->lost = 0;
                    $teamSeason1->match_total = 0;
                    $teamSeason1->won = 0;
                    $teamSeason1->gk_save = 0;
                    $teamSeason1->red_card = 0;
                    $teamSeason1->yellow_card = 0;

                    $teamSeason1->add_point_manuel = 0;
                    $teamSeason1->add_point_month_bonus = 0;

                    $apl_msb = collect();
                    foreach ($ts1->teamseason_add_point_log_manuel->where('created_at', '!=', null) as $value) {
                        $apl_msb_temp = collect();
                        $apl_msb_temp->point_add = $value->point_add;
                        $apl_msb_temp->teamseason_id = $value->teamseason_id;
                        $apl_msb_temp->date = (!empty($value->created_at)) ? Carbon::parse($value->created_at) : null;
                        $apl_msb_temp->added = false;

                        $apl_msb->push($apl_msb_temp);
                    }

                    $teamSeason1->add_point_log_manuel = $apl_msb;

                    $apl_ms = collect();
                    foreach ($ts1->teamseason_add_point_log_mounthly_bonus as $value) {
                        $apl_ms_temp = collect();
                        $apl_ms_temp->point_add = $value->point_add;
                        $apl_ms_temp->teamseason_id = $value->teamseason_id;
                        $apl_ms_temp->year = json_decode($value->meta)->year;
                        $apl_ms_temp->month = json_decode($value->meta)->month;
                        $apl_ms_temp->added = false;

                        $apl_ms->push($apl_ms_temp);
                    }

                    $teamSeason1->add_point_log_monthly_bonus = $apl_ms;

                    $this->teamSeasonTemp->push($teamSeason1);
                }

                $teamSeason2 = $this->teamSeasonTemp->where('season_id', $sezon_id)->where('team_id', $match->team2_id)->first();
                if(empty($teamSeason2)){
                    $ts2 = TeamTeamSeason::where('season_id', $sezon_id)->where('team_id', $match->team2_id)->first();

                    $teamSeason2 = collect(array());
                    $teamSeason2->id = $ts2->id;
                    $teamSeason2->model = 'TeamSeason';
                    $teamSeason2->point = $min_point;
                    $teamSeason2->season_id = $sezon_id;
                    $teamSeason2->team_id = $match->team2_id;

                    $teamSeason2->drawn = 0;
                    $teamSeason2->goal_against = 0;
                    $teamSeason2->goal_for = 0;
                    $teamSeason2->lost = 0;
                    $teamSeason2->match_total = 0;
                    $teamSeason2->won = 0;
                    $teamSeason2->gk_save = 0;
                    $teamSeason2->red_card = 0;
                    $teamSeason2->yellow_card = 0;

                    $teamSeason2->add_point_manuel = 0;
                    $teamSeason2->add_point_month_bonus = 0;


                    $apl_msb = collect();
                    foreach ($ts2->teamseason_add_point_log_manuel->where('created_at', '!=', null) as $value) {
                        $apl_msb_temp = collect();
                        $apl_msb_temp->point_add = $value->point_add;
                        $apl_msb_temp->teamseason_id = $value->teamseason_id;
                        $apl_msb_temp->date = (!empty($value->created_at)) ? Carbon::parse($value->created_at) : null;
                        $apl_msb_temp->added = false;

                        $apl_msb->push($apl_msb_temp);
                    }

                    $teamSeason2->add_point_log_manuel = $apl_msb;


                    $apl_ms = collect();
                    foreach ($ts2->teamseason_add_point_log_mounthly_bonus as $value) {
                        $apl_ms_temp = collect();
                        $apl_ms_temp->point_add = $value->point_add;
                        $apl_ms_temp->teamseason_id = $value->teamseason_id;
                        $apl_ms_temp->year = json_decode($value->meta)->year;
                        $apl_ms_temp->month = json_decode($value->meta)->month;
                        $apl_ms_temp->added = false;

                        $apl_ms->push($apl_ms_temp);
                    }
                    $teamSeason2->add_point_log_monthly_bonus = $apl_ms;

                    $this->teamSeasonTemp->push($teamSeason2);
                }

                $dbTeamSeason1 = TeamTeamSeason::where('season_id', $match->season_id)->where('team_id', $match->team1_id)->first();
                $tempTeamSeason1 = $this->teamSeasonTemp->where('season_id', $match->season_id)->where('team_id', $match->team1_id)->first();

                $dbTeamSeason2 = TeamTeamSeason::where('season_id', $match->season_id)->where('team_id', $match->team2_id)->first();
                $tempTeamSeason2 = $this->teamSeasonTemp->where('season_id', $match->season_id)->where('team_id', $match->team2_id)->first();

                // MANUEL BONUS // TARİHLİ
                if($tempTeamSeason1->add_point_log_manuel->where('added', false)->count() > 0){
                    foreach ($tempTeamSeason1->add_point_log_manuel->where('added', false) as $add_point_lm) {
                        if($add_point_lm->date <= Carbon::parse($match->date.' '.$match->time)){
                            $tempTeamSeason1->point = $tempTeamSeason1->point + $add_point_lm->point_add;
                            $add_point_lm->added = true;
                            if($mode != 'cronrun'){
                                echo '<h4> --> '.Carbon::parse($add_point_lm->date)->format('Y-m-d H:i').' - '.Team::find($match->team1_id)->name.' Manuel Puan Eklendi '.$add_point_lm->point_add.'</h4>';
                            }
                        }
                    }
                }
                if($tempTeamSeason2->add_point_log_manuel->where('added', false)->count() > 0){
                    foreach ($tempTeamSeason2->add_point_log_manuel->where('added', false) as $add_point_lm) {
                        if($add_point_lm->date <= Carbon::parse($match->date.' '.$match->time)){
                            $tempTeamSeason2->point = $tempTeamSeason2->point + $add_point_lm->point_add;
                            $add_point_lm->added = true;
                            if($mode != 'cronrun'){
                                echo '<h4> --> '.Carbon::parse($add_point_lm->date)->format('Y-m-d H:i').' - '.Team::find($match->team2_id)->name.' Manuel Puan Eklendi '.$add_point_lm->point_add.'</h4>';
                            }
                        }
                    }
                }
                

                //dump($match);
                $this->teamseasons_statistics_update($match);

                //Sadece Puan durumunu yazdır...
                if($mode != 'cronrun'){
                    echo '<h3> '.$match->id.' '.$match->date.' '.$match->time.' - '. Team::find($match->team1_id)->name .' ('.$match->team1_point.') '.$match->team1_goal.'-'.$match->team2_goal.' ('.$match->team2_point.') '.Team::find($match->team2_id)->name.' </h3>';

                    if($in % 50 == 0){
                        
                        echo '<table border="1" cellpadding="2" cellspacing="0" >';
                        echo '<tr>';
                        echo '<td style="text-align:center"> '.'Team ID'.' </td>';
                        echo '<td style="text-align:center"> '.'r1'.' </td>';
                        //echo '<td style="text-align:center"> '.'r2'.' </td>';
                        echo '<td> '.'Takım Adı'.' </td>';
                        echo '<td style="text-align:center"> '.'O'.' </td>';
                        echo '<td style="text-align:center"> '.'G'.' </td>';
                        echo '<td style="text-align:center"> '.'B'.' </td>';
                        echo '<td style="text-align:center"> '.'M'.' </td>';
                        echo '<td style="text-align:center"> '.'A'.' </td>';
                        echo '<td style="text-align:center"> '.'Y'.' </td>';
                        echo '<td style="text-align:center"> '.'Ave.'.' </td>';
                        echo '<td style="text-align:center"> '.'Puan'.' </td>';
                        echo '</tr>';

                        foreach ($this->teamSeasonTemp->sortByDesc('point') as $teamSeason) {

                            // $dbTeamSeason = TeamTeamSeason::where('season_id', $teamSeason->season_id)->where('team_id', $teamSeason->team_id)->first();
                        
                            echo '<tr>';
                            echo '<td style="text-align:center"> '.$teamSeason->team_id.' </td>';
                            

                            //echo '<td style="text-align:center"> '.$teamSeason->point.' </td>';
                            echo '<td style="background-color:'.PointType::where('min_point', '<=', $teamSeason->point)->where('max_point', '>=', $teamSeason->point)->first()->color.'"> </td>';

                            echo '<td> '.Team::find($teamSeason->team_id)->name.' </td>';


                            $__match_total = '#FFF';
                            // if ($teamSeason->match_total != $dbTeamSeason->match_total) { $__match_total = '#ffbebe'; }
                            $__won = '#FFF';
                            // if ($teamSeason->won != $dbTeamSeason->won) { $__won = '#ffbebe'; }
                            $__drawn = '#FFF';
                            // if ($teamSeason->drawn != $dbTeamSeason->drawn) { $__drawn = '#ffbebe'; }
                            $__lost = '#FFF';
                            // if ($teamSeason->lost != $dbTeamSeason->lost) { $__lost = '#ffbebe'; }
                            $__goal_for = '#FFF';
                            // if ($teamSeason->goal_for != $dbTeamSeason->goal_for) { $__goal_for = '#ffbebe'; }
                            $__goal_against = '#FFF';
                            // if ($teamSeason->goal_against != $dbTeamSeason->goal_against) { $__goal_against = '#ffbebe'; }
                            $__ave = '#FFF';
                            // if (($teamSeason->goal_for - $dbTeamSeason->goal_against) != ($dbTeamSeason->goal_for - $dbTeamSeason->goal_against)) { $__ave = '#ffbebe'; }
                            $__point = '#FFF';
                            // if ($teamSeason->point != $dbTeamSeason->point) { $__point = '#ffbebe'; }


                            echo '<td style="text-align:center; background-color:'.$__match_total.'"> '.$teamSeason->match_total.' </td>';
                            echo '<td style="text-align:center; background-color:'.$__won.'"> '.$teamSeason->won.' </td>';
                            echo '<td style="text-align:center; background-color:'.$__drawn.'"> '.$teamSeason->drawn.' </td>';
                            echo '<td style="text-align:center; background-color:'.$__lost.'"> '.$teamSeason->lost.' </td>';
                            echo '<td style="text-align:center; background-color:'.$__goal_for.'"> '.$teamSeason->goal_for.' </td>';
                            echo '<td style="text-align:center; background-color:'.$__goal_against.'"> '.$teamSeason->goal_against.' </td>';
                            echo '<td style="text-align:center; background-color:'.$__ave.'"> '.($teamSeason->goal_for - $teamSeason->goal_against).' </td>';
                            echo '<td style="text-align:center; background-color:'.$__point.'"> '.$teamSeason->point.' ('.$teamSeason->add_point_manuel.'/'.$teamSeason->add_point_month_bonus.') </td>';
                            echo '</tr>';
                        }

                        echo '</table>';

                        echo '<br><br><br><br>';
                    }
                }
            }

            
            if($mode != 'cronrun'){
                echo '<h3>AY SONU - AYLIK BONUS EKLE - '.$bonusDateControl->format('Y').' - '.$bonusDateControl->format('n').'</h3>';
            }

            foreach ($this->teamSeasonTemp as $teamSeasonn) {

                if($teamSeasonn->add_point_log_monthly_bonus->where('added', false)->where('year', $bonusDateControl->format('Y'))->where('month', $bonusDateControl->format('n'))->count() > 0) {

                    $add_bonus_value = $teamSeasonn->add_point_log_monthly_bonus->where('added', false)->where('year', $bonusDateControl->format('Y'))->where('month', $bonusDateControl->format('n'))->first();

                    $teamSeasonn->add_point_month_bonus = $teamSeasonn->add_point_month_bonus + $add_bonus_value->point_add;
                    $teamSeasonn->point = $teamSeasonn->point + $add_bonus_value->point_add;
                    $add_bonus_value->added = true;
                    if($mode != 'cronrun'){
                        echo '<h4> *-> '.Team::find($teamSeasonn->team_id)->name.' Bonus Puan Eklendi '.$add_bonus_value->point_add.'</h4>';
                    }

                }
            }
            

            $bonusDateControl->endOfMonth()->addDay();
        }    


        if($mode != 'cronrun'){
            echo '<table border="1" cellpadding="2" cellspacing="0" >';
            echo '<tr>';
            echo '<td style="text-align:center"> '.'Team ID'.' </td>';
            echo '<td style="text-align:center"> '.'r1'.' </td>';
            echo '<td style="text-align:center"> '.'r2'.' </td>';
            echo '<td> '.'Takım Adı'.' </td>';
            echo '<td style="text-align:center"> '.'O'.' </td>';
            echo '<td style="text-align:center"> '.'G'.' </td>';
            echo '<td style="text-align:center"> '.'B'.' </td>';
            echo '<td style="text-align:center"> '.'M'.' </td>';
            echo '<td style="text-align:center"> '.'A'.' </td>';
            echo '<td style="text-align:center"> '.'Y'.' </td>';
            echo '<td style="text-align:center"> '.'Ave.'.' </td>';
            echo '<td style="text-align:center"> '.'Puan'.' </td>';
            echo '</tr>';

            foreach ($this->teamSeasonTemp->sortByDesc('point') as $teamSeason) {

                $dbTeamSeason = TeamTeamSeason::where('season_id', $teamSeason->season_id)->where('team_id', $teamSeason->team_id)->first();

                echo '<tr>';
                echo '<td style="text-align:center"> '.$teamSeason->team_id.' </td>';

                echo '<td style="background-color:'.PointType::where('min_point', '<=', $teamSeason->point)->where('max_point', '>=', $teamSeason->point)->first()->color.'"> </td>';
                echo '<td style="background-color:'.PointType::where('min_point', '<=', $dbTeamSeason->point)->where('max_point', '>=', $dbTeamSeason->point)->first()->color.'"> </td>';

                echo '<td> '.Team::find($teamSeason->team_id)->name.' </td>';


                $__match_total = '#FFF';
                if ($teamSeason->match_total != $dbTeamSeason->match_total) { $__match_total = '#ffbebe'; }
                $__won = '#FFF';
                if ($teamSeason->won != $dbTeamSeason->won) { $__won = '#ffbebe'; }
                $__drawn = '#FFF';
                if ($teamSeason->drawn != $dbTeamSeason->drawn) { $__drawn = '#ffbebe'; }
                $__lost = '#FFF';
                if ($teamSeason->lost != $dbTeamSeason->lost) { $__lost = '#ffbebe'; }
                $__goal_for = '#FFF';
                if ($teamSeason->goal_for != $dbTeamSeason->goal_for) { $__goal_for = '#ffbebe'; }
                $__goal_against = '#FFF';
                if ($teamSeason->goal_against != $dbTeamSeason->goal_against) { $__goal_against = '#ffbebe'; }
                $__ave = '#FFF';
                if (($teamSeason->goal_for - $dbTeamSeason->goal_against) != ($dbTeamSeason->goal_for - $dbTeamSeason->goal_against)) { $__ave = '#ffbebe'; }
                $__point = '#FFF';
                if ($teamSeason->point != $dbTeamSeason->point) { $__point = '#ffbebe'; }


                echo '<td style="text-align:center; background-color:'.$__match_total.'"> '.$teamSeason->match_total.' * '.$dbTeamSeason->match_total.' </td>';
                echo '<td style="text-align:center; background-color:'.$__won.'"> '.$teamSeason->won.' * '.$dbTeamSeason->won.' </td>';
                echo '<td style="text-align:center; background-color:'.$__drawn.'"> '.$teamSeason->drawn.' * '.$dbTeamSeason->drawn.' </td>';
                echo '<td style="text-align:center; background-color:'.$__lost.'"> '.$teamSeason->lost.' * '.$dbTeamSeason->lost.' </td>';
                echo '<td style="text-align:center; background-color:'.$__goal_for.'"> '.$teamSeason->goal_for.' * '.$dbTeamSeason->goal_for.' </td>';
                echo '<td style="text-align:center; background-color:'.$__goal_against.'"> '.$teamSeason->goal_against.' * '.$dbTeamSeason->goal_against.' </td>';
                echo '<td style="text-align:center; background-color:'.$__ave.'"> '.($teamSeason->goal_for - $teamSeason->goal_against).' * '.($dbTeamSeason->goal_for - $dbTeamSeason->goal_against).' </td>';
                echo '<td style="text-align:center; background-color:'.$__point.'"> '.$teamSeason->point.' ('.$teamSeason->add_point_manuel.'/'.$teamSeason->add_point_month_bonus.') * '.$dbTeamSeason->point.' </td>';
                echo '</tr>';
            }

            echo '</table>';
        }

        if($mode == 'autorun' || $mode == 'cronrun'){
            foreach ($this->matchTemp as $value) {

                $_match = Match::find($value->id);

                $_match->team1_point = $value->team1_point;
                $_match->team2_point = $value->team2_point;

                if($value->team1_point_before <= $min_point){
                    $team1_point_before = $min_point;
                }else{
                    $team1_point_before = $value->team1_point_before;
                }

                if($value->team2_point_before <= $min_point){
                    $team2_point_before = $min_point;
                }else{
                    $team2_point_before = $value->team2_point_before;
                }

                $_match->team1_point_before = $team1_point_before;
                $_match->team2_point_before = $team2_point_before;
                
                $_match->save();
            }

            foreach ($this->teamSeasonTemp as $value) {
                $_teamSeason = TeamTeamSeason::find($value->id);
                $_teamSeason->point = $value->point;
                $_teamSeason->drawn = $value->drawn;
                $_teamSeason->goal_against = $value->goal_against;
                $_teamSeason->goal_for = $value->goal_for;
                $_teamSeason->lost = $value->lost;
                $_teamSeason->match_total = $value->match_total;
                $_teamSeason->won = $value->won;
                $_teamSeason->save();
            }

            echo 'Düzeltme Tamamlandı';
            
        }


        if ($mode != 'autorun' && $mode != 'cronrun') {
            
            echo " <br><br><a href='/sezonpuanhesapla/".$season->id."/autorun'>autorun</a><br><br> ";

        }

        die;
    }

    public function teamseasons_statistics_update($match) {
        // POINTS
        $season = Season::find($match->season_id);
        $min_point = 0;
        if($season->polymorphic_ctype_id == 33){ // fixtureseason
            $min_point = $season->league_fixtureseason->min_team_point;
        }else if($season->polymorphic_ctype_id == 34){ // pointseason
            $min_point = $season->league_pointseason->min_team_point;
        }

        $match->team1_point_before = $this->teamSeasonTemp->where('season_id', $match->season_id)->where('team_id', $match->team1_id)->first()->point;
        $match->team2_point_before = $this->teamSeasonTemp->where('season_id', $match->season_id)->where('team_id', $match->team2_id)->first()->point;

        if(is_null($match->completed_status)){
            //$match->team1_goal = Match::find($match->id)->first()->team1_goal;
            //$match->team2_goal = Match::find($match->id)->first()->team2_goal;
        }else{
            //HÜKMEN DURUMU GOLLERİ
            if(Season::find($match->season_id)->polymorphic_ctype_id == 34){
                // Season.POINT
                if($match->completed_status == 1){
                    $match->team1_goal = 3;
                    $match->team2_goal = 0;
                }else if($match->completed_status == 2){
                    $match->team1_goal = 0;
                    $match->team2_goal = 3;
                }
            }else if(Season::find($match->season_id)->polymorphic_ctype_id == 33){
                // Season.FIXTURE
                if($match->completed_status == 1){
                    $match->team1_goal = 5;
                    $match->team2_goal = 0;
                }else if($match->completed_status == 2){
                    $match->team1_goal = 0;
                    $match->team2_goal = 5;
                }
            }else if(Season::find($match->season_id)->polymorphic_ctype_id == 32){
                if(Season::find($match->season_id)->league_eliminationseason->base_season->polymorphic_ctype_id == 34){
                    // Season.POINT
                    if($match->completed_status == 1){
                        $match->team1_goal = 3;
                        $match->team2_goal = 0;
                    }else if($match->completed_status == 2){
                        $match->team1_goal = 0;
                        $match->team2_goal = 3;
                    }
                }else if(Season::find($match->season_id)->league_eliminationseason->base_season->polymorphic_ctype_id == 33){
                    // Season.FIXTURE
                    if($match->completed_status == 1){
                        $match->team1_goal = 5;
                        $match->team2_goal = 0;
                    }else if($match->completed_status == 2){
                        $match->team1_goal = 0;
                        $match->team2_goal = 5;
                    }
                }
            }
        }

         /***** calculate_points *******/
        if(Season::find($match->season_id)->polymorphic_ctype_id == 33 || Season::find($match->season_id)->polymorphic_ctype_id == 34){
            
            // takımların durumunu bul
            if($match->team1_goal > $match->team2_goal){
                $team1_status = 'win';
                $team2_status = 'defeat';
            }else if($match->team1_goal < $match->team2_goal){
                $team1_status = 'defeat';
                $team2_status = 'win';
            }else if($match->team1_goal == $match->team2_goal){
                $team1_status = 'draw';
                $team2_status = 'draw';
            }

            if(Season::find($match->season_id)->polymorphic_ctype_id == 34){
                // Season.POINT
                // takımların renk durumlarını tespit et
                if($match->team1_point_before < $min_point){
                    $team1_point_before = $min_point;
                }else{
                    $team1_point_before = $match->team1_point_before;
                }
                
                if($match->team2_point_before < $min_point){
                    $team2_point_before = $min_point;
                }else{
                    $team2_point_before = $match->team2_point_before;
                }

                $team1_point_type = PointType::where('min_point', '<=', $team1_point_before)->where('max_point', '>=', $team1_point_before)->first();
                $team2_point_type = PointType::where('min_point', '<=', $team2_point_before)->where('max_point', '>=', $team2_point_before)->first();
                
                //dump("--------");
                //dump($match->team1_point_before);
                //dump($team1_point_type);

                //dump($match->team2_point_before);
                //dump($team2_point_type);
                //dump("--------");
                
                // renk durumuna göre takımlara puan ver
                if ($team1_status == 'win'){
                    $team1_point = $team2_point_type->win_score;
                }else if ($team1_status == 'defeat'){
                    $team1_point = -$team2_point_type->defeat_score;
                }else if ($team1_status == 'draw'){
                    $team1_point = $team2_point_type->draw_score;
                }

                if ($team2_status == 'win'){
                    $team2_point = $team1_point_type->win_score;
                }else if ($team2_status == 'defeat'){
                    $team2_point = -$team1_point_type->defeat_score;
                }else if ($team2_status == 'draw'){
                    $team2_point = $team1_point_type->draw_score;
                }
                

            }else if(Season::find($match->season_id)->polymorphic_ctype_id == 33){
                // Season.FIXTURE
                // Normal puanlama
                if ($team1_status == 'win'){
                    $team1_point = 3;
                } else if ($team1_status == 'defeat'){
                    $team1_point = 0;
                } else if ($team1_status == 'draw'){
                    $team1_point = 1;
                }

                if ($team2_status == 'win'){
                    $team2_point = 3;
                } else if ($team2_status == 'defeat'){
                    $team2_point = 0;
                } else if ($team2_status == 'draw'){
                    $team2_point = 1;
                }

            }
          
        }else{

            $team1_point = 0;
            $team2_point = 0;

        }

        $match->team1_point = $team1_point;
        $match->team2_point = $team2_point;
        
        /***** teamseasons_update_by_match *******/
        //Tamamlanmış maçlar için teamseason kayıtlarında güncellemeler yapar
        
        //Güncelleme ve Silme işleminden önce istatistikler geri alınır yani - işleme yapılır
        //Eğer yeni tamamlanmış veya güncellenen maç verileri ise ekleme yapılır yani + işlem yapılır

        $team1_season = $this->teamSeasonTemp->where('season_id', $match->season_id)->where('team_id', $match->team1_id)->first();
        $team2_season = $this->teamSeasonTemp->where('season_id', $match->season_id)->where('team_id', $match->team2_id)->first();

        // POINTS
        $season = Season::find($match->season_id);
        $min_point = 0;
        if($season->polymorphic_ctype_id == 33){ // fixtureseason
            $min_point = $season->league_fixtureseason->min_team_point;
        }else if($season->polymorphic_ctype_id == 34){ // pointseason
            $min_point = $season->league_pointseason->min_team_point;
        }

        if(Season::find($match->season_id)->polymorphic_ctype_id == 34 || Season::find($match->season_id)->polymorphic_ctype_id == 33){
            
            $team1_season->point += $match->team1_point;
            if($team1_season->point < $min_point){
                $team1_season->point = $min_point;
            }

            $team2_season->point += $match->team2_point;
            if($team2_season->point < $min_point){
                $team2_season->point = $min_point;
            }

        } 

        // GOAL FOR - GOAL AGAINST
        $team1_season->goal_for += $match->team1_goal;
        $team1_season->goal_against += $match->team2_goal;

        $team2_season->goal_for += $match->team2_goal;
        $team2_season->goal_against += $match->team1_goal;


        // WON - DRAWN - LOST
        if ($match->team1_goal == $match->team2_goal){
            $team1_season->drawn += 1;
            $team2_season->drawn += 1;
            //dump($match->id.' - '.$match->team1_id.' - '.$match->team2_id.' - ('.$match->team1_goal.' - '.$match->team2_goal.') drawn');
        }else if ($match->team1_goal > $match->team2_goal){
            $team1_season->won += 1;
            $team2_season->lost += 1;
            //dump($match->id.' - '.$match->team1_id.' - '.$match->team2_id.' - ('.$match->team1_goal.' - '.$match->team2_goal.') t1 won');
        }else if ($match->team1_goal < $match->team2_goal){
            $team1_season->lost += 1;
            $team2_season->won += 1;
            //dump($match->id.' - '.$match->team1_id.' - '.$match->team2_id.' - ('.$match->team1_goal.' - '.$match->team2_goal.') t2 won');
        }

        // MATCH TOTAL
        $team1_season->match_total += 1;
        $team2_season->match_total += 1;
        /***** teamseasons_update_by_match *******/

        //dump($match);
        //dump('----');
        return false;
    }

    public function sezonIstatistikHesapla($sezon_id, $offset, $limit, $mode = null) { // null, autorun, cronrun
        ini_set('max_execution_time', 120);

        $match_completed_status = 'completed:true';
        $matches = Match::where('season_id', $sezon_id)->where('completed', true)->offset($offset)->limit($limit)->orderBy('date')->orderBy('time')->get();

        if($mode != 'cronrun'){
            echo 'OK ---- offset:'.$offset.' - limit:'.$limit.'</br>';
            dump(count($matches));
        }

        if(count($matches)){
            foreach ($matches as $model) {
                if($mode != 'cronrun'){
                    echo '---------------------------------------------------------------------------------------------------------------------------------- <br>';
                    echo '--> Maç ID: '.$model->id.' <br>';
                }
                $this->players_history_update($model, $match_completed_status, $mode);
            }
            
            if($mode == 'autorun'){
                echo '<br> bekleyiniz... <br>';
                echo '<script>document.querySelector("body").onload = function(){
                    setTimeout(function(){  window.location = "'.url('/sezonistatistikhesapla').'/'.$sezon_id.'/'.($offset+$limit).'/'.$limit.'/'.((!empty($mode)) ? $mode : '').'"; }, 5000);
                };</script>';
            }else{
                echo "<br><br><a href='".url('/sezonistatistikhesapla').'/'.$sezon_id.'/'.($offset-$limit).'/'.$limit.'/'.((!empty($mode)) ? $mode : '')."'> önceki sayfa </a> ------ <a href='".url('/sezonistatistikhesapla').'/'.$sezon_id.'/'.($offset+$limit).'/'.$limit.'/'.((!empty($mode)) ? $mode : '')."'> sonraki sayfa </a><br><br>";
            }
        }else{
            echo 'finished ';
        }
        echo 'Düzeltme Tamamlandı';
    }

    public function players_history_update($match, $match_completed_status, $mode) { // null, autorun, cronrun
        //Maç tamamlandı durumuna alındığında oyuncuların tüm sezon istatistikleri
        //Maç silme işleminde eğer tamamlanan bir mç ise hesaplama tekrar yapılır
        //Maç güncellemesinde tamamlandı moduna alınan durumlarda yeniden hesaplama yapılır
        //dump($match);
        $season_id = $match->season_id;
        $match_id = $match->id;
        
        $statisticsArr = array();
        //Maçın oyuncularının player id lerini al, misafirleri dışla
        $all_player_IDs = array();
        
        foreach ($match->match_player_is_not_guest as $mplayer) {

            $sArr = array();
            $sArr['player_id'] = $mplayer->player_id;
            $sArr['team_id'] = $mplayer->team_id;
            $sArr['season_id'] = $season_id;

            $sArr['gk_save'] = 0;
            $sArr['match'] = 0;
            $sArr['goal'] = 0;
            $sArr['yellow_card'] = 0;
            $sArr['red_card'] = 0;
            $sArr['saving'] = 0;

            $statisticsArr[] = collect($sArr);
            $all_player_IDs[] = $mplayer->player;
        }

        //dump($statisticsArr);
        //dump($all_player_IDs);
        
        foreach ($all_player_IDs as $player) {
            //dump($player->id.'----------------');
            $player->match_player = $player->match_player->filter(function ($value) use ($season_id, $player) {
                return $value->match->season_id == $season_id && $value->team_id == $player->team_id && $value->match->completed == true;
            });
        }

        //dump('----');
        //dump($all_player_IDs);
        $statistics = collect($statisticsArr);
        //dump('******');
        //dump($statistics);




        foreach ($all_player_IDs as $player) {
            $pstat = $statistics->where('player_id', $player->id)->first();
            foreach ($player->match_player as $mplayer) {

                //rate-sil $pstat['rate_total'] = $pstat['rate_total'] + $mplayer->rating;
                //$pstat['gk_save'] = $pstat['gk_save'] + $mplayer->saving;

                if($mplayer->position_id == 1){
                    $pstat['gk_save'] += $mplayer->match->match_action->where('type', 'saving')->where('team_id', $mplayer->team_id)->count();
                }else{
                    //$pstat['gk_save'] = 0;
                }

                $pstat['match'] = $pstat['match'] + 1;

                if(is_null($match->completed_status)){
                    foreach ($mplayer->match_action as $maction) {
                        if($maction->type != 'critical' && $maction->type != 'saving'){
                            $pstat[$maction->type] = $pstat[$maction->type] + 1;
                        }
                    }
                }
            }
        }

        
        foreach ($all_player_IDs as $player) {
            $tpth = $player->team_playerteamhistory->where('season_id', $season_id)->where('team_id', $player->team_id)->first();
            $stats = $statistics->where('season_id', $season_id)->where('team_id', $player->team_id)->where('player_id', $player->id)->first();
            
            if(empty($tpth)){
                $tpth = new TeamPlayerTeamHistory();
                $tpth->season_id = $season_id;
                $tpth->team_id = $player->team_id;
                $tpth->player_id = $player->id;
            }
            
            if(!empty($stats)){

                $tpthArr = array();
                $tpthArr["player_id"] = $tpth->player_id;
                $tpthArr["team_id"] = $tpth->team_id;
                $tpthArr["season_id"] = $tpth->season_id;
                //rate-sil $tpthArr["rate_total"] = $tpth->rate_total;
                $tpthArr["gk_save"] = $tpth->gk_save;
                $tpthArr["rating"] = 0;
                $tpthArr["match"] = $tpth->match;
                $tpthArr["goal"] = $tpth->goal;
                $tpthArr["yellow_card"] = $tpth->yellow_card;
                $tpthArr["red_card"] = $tpth->red_card;

                if($stats->diff($tpthArr)->count() && $mode != 'cronrun'){

                    echo "<br>p_id: ".$stats['player_id']." - ".$player->user->first_name ." ". $player->user->last_name . " //--// " .
                    "t_id: ".$stats['team_id']." - " . $player->team->name . "<br>" .
                    "";

                    echo "gk_save: ".$tpth->gk_save." - ".
                    "match: ".$tpth->match." - ".
                    "goal: ".$tpth->goal." - ".
                    "yellow_card: ".$tpth->yellow_card." - ".
                    "red_card: ".$tpth->red_card." <br>";

                    echo "gk_save: ".$stats['gk_save']." - ".
                    "match: ".$stats['match']." - ".
                    "goal: ".$stats['goal']." - ".
                    "yellow_card: ".$stats['yellow_card']." - ".
                    "red_card: ".$stats['red_card']." <br>";

                    echo "<span style='color:red;'> !!! farklılık var </span><br>";
                }

                // dump($stats);
                // dump($tpthArr);
                // dump($stats->diff($tpthArr)->count());

                if($mode == 'autorun' || $mode == 'cronrun'){
                    $tpth->gk_save = $stats['gk_save'];
                    $tpth->rating = 0;
                    $tpth->match = $stats['match'];
                    $tpth->goal = $stats['goal'];
                    $tpth->yellow_card = $stats['yellow_card'];
                    $tpth->red_card = $stats['red_card'];
                    $tpth->save();
                }

            }
            // dump($tpth);
        }
    }

    public function rbkirmizi() {
        //where('date', $date)->

        $phones = array();
        $date = Carbon::now()->subDay()->format('Y-m-d');
        $date = '2020-06-24';

        $matches_t1 = Match::where('date', $date)->where(DB::raw('(team1_point_before + team1_point)'), '>', 20)->where('team1_point_before', '<', 20)->where('completed', true)->get();
        foreach ($matches_t1 as $match) {
            //dump($match);
            if(!empty($match->team1->captain) && ($match->team1->captain->user->infoshare || is_null($match->team1->captain->user->infoshare))){
                $phones[] = $match->team1->captain->user->phone;
            }
        }

        $matches_t2 = Match::where('date', $date)->where(DB::raw('(team2_point_before + team2_point)'), '>', 20)->where('team2_point_before', '<', 20)->where('completed', true)->get();
        foreach ($matches_t2 as $match) {
            //dump($match);
            if(!empty($match->team2->captain) && ($match->team2->captain->user->infoshare || is_null($match->team2->captain->user->infoshare))){
                $phones[] = $match->team2->captain->user->phone;
            }
        }

        $phones[] = '5552617703';
        //dump($phones);

        if(count($phones)){
            $smsHelper = new SmsClass();
            $smsHelper->smsSendRbkirmizi(implode(",", $phones));
        }
        
        return 'OK';
    }


    /* 00
    public function players_deger_update($match, $match_completed_status)
    {

        // dump($match);
        // dump($match_season->id);
        // AÇÇ echo 'Ayın MVPsi, Oyuncu Ayın Centilmeni/Mevki/Golü, Sezonun MVPsi, Oyuncu Sezonun Centilmeni/Mevki/Golü bilgileri veritabanında mevcut değil. Bu sebeple hesaplama dışında tutulmuştur.';
        
        $match_season = $match->season;
        $league_provinces = $match->season->league->league_province;

        $pointTotalPlayer = 100000;

        $pointMatch = 5000;
        $pointMatchRating = 3000;
        $pointMatchSave = 2000;

        $pointGoal = 3000;

        $pointYellowCard = -5000;
        $pointRedCard = -15000;

        $panoramaPoint = array(1 => array(8000, 'Maçın Gümüş Oyuncusu'), 2 => array(5000, 'Maçın Golü'), 3 => array(15000, 'Maçın Altın Oyuncusu'), 4 => array(5000, 'Maçın Dinamik Oyuncusu'), 5 => array(5000, 'Maçın Defansı'), 6 => array(5000, 'Maçın Orta Sahası'), 7 => array(5000, 'Maçın Forveti'), 8 => array(5000, 'Maçın Kalecisi'));

        $seasonCross = array(4, 3.5, 2.5, 1.5);
        $seasonCrossIndex = 0;

        $seasonTRCross = array(10, 3);
        $seasonTRCrossIndex = 0;

        $seasonList = array();
        $seasonTRList = array();
        
        foreach ($league_provinces as $league_province) {

            if($league_province->league->league_province->count() == 1){
                
                // AÇÇ dump($league_province->league->name);
                $seasons = $league_province->league->seasonApi;
                foreach ($seasons as $season) {

                    if($season->polymorphic_ctype_id == 32){ $seasonType = 'elimination'; }else if($season->polymorphic_ctype_id == 33){ $seasonType = 'fixture'; }else if($season->polymorphic_ctype_id == 34){ $seasonType = 'point'; }
                    ////ac dump('('.$seasonCross[$seasonCrossIndex].') (id:'.$season->id.') - '.$season->year.' - '.$season->name.' - '.$seasonType);
                    $seasonList[] = array($seasonCross[$seasonCrossIndex], $season->id, $seasonType, ($season->year.' - '.$season->name), $season->year);

                    if(!empty($season->league_eliminationseasonApi)){

                        if($season->league_eliminationseasonApi->season_ptr->polymorphic_ctype_id == 32){ $seasonRType = 'elimination'; }else if($season->league_eliminationseasonApi->season_ptr->polymorphic_ctype_id == 33){ $seasonRType = 'fixture'; }else if($season->league_eliminationseasonApi->season_ptr->polymorphic_ctype_id == 34){ $seasonRType = 'point'; }

                        ////ac dump('('.$seasonCross[$seasonCrossIndex].') (id:'.$season->league_eliminationseasonApi->season_ptr->id.') - '.' ---- '.$season->league_eliminationseasonApi->season_ptr->year.' - '.$season->league_eliminationseasonApi->season_ptr->name.' - '.$seasonRType);

                        $seasonList[] = array($seasonCross[$seasonCrossIndex], $season->league_eliminationseasonApi->season_ptr->id, $seasonRType, ($season->league_eliminationseasonApi->season_ptr->year.' - '.$season->league_eliminationseasonApi->season_ptr->name), $season->league_eliminationseasonApi->season_ptr->year );

                    }

                    if(count($seasonCross)-1 > $seasonCrossIndex){
                        $seasonCrossIndex++;
                    }
                }
                
                ////ac dump('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'); 
                
                $all_seasons = $league_province->league->season->where('active', true);
                foreach ($all_seasons as $season) {

                    if($season->polymorphic_ctype_id == 32){ $seasonEType = 'elimination'; }else if($season->polymorphic_ctype_id == 33){ $seasonEType = 'fixture'; }else if($season->polymorphic_ctype_id == 34){ $seasonEType = 'point'; }
                    $is_valid = array_search($season->id, array_column($seasonList, 1));
                    ////ac dump('---('.end($seasonCross).') (id:'.$season->id.') - '.$season->year.' - '.$season->name.' - '.$seasonEType.' - '.$is_valid.' - '.( ($is_valid !== false) ? $is_valid : 'false') );
                    
                    if($is_valid === false){
                        $seasonList[] = array(end($seasonCross), $season->id, $seasonEType, ($season->year.' - '.$season->name), $season->year);
                    }
                }
                
                // AÇÇ dump($seasonList);
                

            }else if($league_province->league->league_province->count() > 1){
                // AÇÇ dump('???? '.$league_province->league->name);

                $seasonsTR = $league_province->league->seasonApi;
                foreach ($seasonsTR as $seasonTR) {

                    if($seasonTR->polymorphic_ctype_id == 32){ $seasonType = 'elimination'; }else if($seasonTR->polymorphic_ctype_id == 33){ $seasonType = 'fixture'; }else if($seasonTR->polymorphic_ctype_id == 34){ $seasonType = 'point'; }
                    ////ac dump('('.$seasonTRCross[$seasonTRCrossIndex].') (id:'.$seasonTR->id.') - '.$seasonTR->year.' - '.$seasonTR->name.' - '.$seasonType);
                    $seasonTRList[] = array($seasonTRCross[$seasonTRCrossIndex], $seasonTR->id, $seasonType, ($seasonTR->year.' - '.$seasonTR->name), $seasonTR->year);

                    if(!empty($seasonTR->league_eliminationseasonApi)){

                        if($seasonTR->league_eliminationseasonApi->season_ptr->polymorphic_ctype_id == 32){ $seasonRType = 'elimination'; }else if($seasonTR->league_eliminationseasonApi->season_ptr->polymorphic_ctype_id == 33){ $seasonRType = 'fixture'; }else if($seasonTR->league_eliminationseasonApi->season_ptr->polymorphic_ctype_id == 34){ $seasonRType = 'point'; }

                        ////ac dump('('.$seasonTRCross[$seasonTRCrossIndex].') (id:'.$seasonTR->league_eliminationseasonApi->season_ptr->id.') - '.' ---- '.$seasonTR->league_eliminationseasonApi->season_ptr->year.' - '.$seasonTR->league_eliminationseasonApi->season_ptr->name.' - '.$seasonRType);

                        $seasonTRList[] = array($seasonTRCross[$seasonTRCrossIndex], $seasonTR->league_eliminationseasonApi->season_ptr->id, $seasonRType, ($seasonTR->league_eliminationseasonApi->season_ptr->year.' - '.$seasonTR->league_eliminationseasonApi->season_ptr->name), $seasonTR->league_eliminationseasonApi->season_ptr->year );

                    }

                    if(count($seasonTRCross)-1 > $seasonTRCrossIndex){
                        $seasonTRCrossIndex++;
                    }
                }


                $all_seasonsTR = $league_province->league->season->where('active', true);
                foreach ($all_seasonsTR as $seasonTR) {

                    if($seasonTR->polymorphic_ctype_id == 32){ $seasonEType = 'elimination'; }else if($seasonTR->polymorphic_ctype_id == 33){ $seasonEType = 'fixture'; }else if($seasonTR->polymorphic_ctype_id == 34){ $seasonEType = 'point'; }
                    $is_valid = array_search($seasonTR->id, array_column($seasonTRList, 1));
                    ////ac dump('---('.end($seasonTRCross).') (id:'.$seasonTR->id.') - '.$seasonTR->year.' - '.$seasonTR->name.' - '.$seasonEType.' - '.$is_valid.' - '.( ($is_valid !== false) ? $is_valid : 'false') );
                    
                    if($is_valid === false){
                        $seasonTRList[] = array(end($seasonTRCross), $seasonTR->id, $seasonEType, ($seasonTR->year.' - '.$seasonTR->name), $seasonTR->year);
                    }
                }
                
                // AÇÇ dump($seasonTRList);

            }else{
                // AÇÇ dump('sıkıntı olmasın !!!');                
                // AÇÇ dump('???? '.$league_province->league->name);
            }
            // AÇÇ dump('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'); 
        }

        if(count($seasonList) > 0){
            foreach ($match->match_player_is_not_guest as $match_player) {

                if(!empty($match_season)){

                    $matchTotalPointArray = array();
                    $is_valid = array_search($match_season->id, array_column($seasonList, 1));
                    
                    $panoramaTotalPoint = 0;
                    foreach ($match_player->match_player_panorama as $panorama) {
                        $panoramaTotalPoint += $panoramaPoint[$panorama->panorama_id][0];
                    }

                    if(is_null($match_player->rating)){
                        $ratingPoint = 0;
                    }else{
                        if($match_player->rating >= 6){ //(Rating 6 dan küçükse hesaplama dışı tutulur.)
                            $ratingPoint = ($match_player->rating - 5) * $pointMatchRating;
                        }else{
                            $ratingPoint = 0;
                        }
                    }
                    
                    $matchPoint = $seasonList[$is_valid][0] * (
                        $pointMatch + 
                        $ratingPoint + 
                        ($match_player->match_action_goal->count() * $pointGoal) + 
                        (((is_null($match_player->saving) ? 0 : $match_player->saving)) * $pointMatchSave) + 
                        ($match_player->match_action_yellow_card->count() * $pointYellowCard) + 
                        ($match_player->match_action_red_card->count() * $pointRedCard) +
                        $panoramaTotalPoint
                    );

                    $matchTotalPointArray = array('match_id' => $match_player->match_id, 'point' => $matchPoint, 'match_player' => $match_player->id, 'player' => $match_player->player->id);

                    $player_val = Player::find($match_player->player->id);
                    if($match_completed_status == 'completed:true'){
                        $player_val->value += $matchPoint;
                    }else if($match_completed_status == 'completed:false'){
                        $player_val->value -= $matchPoint;
                    }
                    $player_val->save();
                }
            }
        }

        if(count($seasonTRList) > 0){
            foreach ($match->match_player_is_not_guest as $match_player) {

                if(!empty($match_season)){
                    
                    $matchTotalPointArray = array();
                    $is_valid = array_search($match_season->id, array_column($seasonTRList, 1));
                    
                    $playerHistoryTRPoint = 0;
                    foreach ($match_player->match_player_panorama as $panorama) {
                        $playerHistoryTRPoint += $panoramaPoint[$panorama->panorama_id][0];
                    }

                    if(is_null($match_player->rating)){
                        $ratingPoint = 0;
                    }else{
                        if($match_player->rating >= 6){ //(Rating 6 dan küçükse hesaplama dışı tutulur.)
                            $ratingPoint = ($match_player->rating - 5) * $pointMatchRating;
                        }else{
                            $ratingPoint = 0;
                        }
                    }
                    
                    $matchPoint = $seasonTRList[$is_valid][0] * (
                        $pointMatch + 
                        $ratingPoint + 
                        ($match_player->match_action_goal->count() * $pointGoal) + 
                        (((is_null($match_player->saving) ? 0 : $match_player->saving)) * $pointMatchSave) + 
                        ($match_player->match_action_yellow_card->count() * $pointYellowCard) + 
                        ($match_player->match_action_red_card->count() * $pointRedCard) +
                        $playerHistoryTRPoint
                    );

                    $matchTotalPointArray = array('match_id' => $match_player->match_id, 'point' => $matchPoint, 'match_player' => $match_player->id, 'player' => $match_player->player->id);
                    
                    $player_val = Player::find($match_player->player->id);
                    if($match_completed_status == 'completed:true'){
                        $player_val->value += $matchPoint;
                    }else if($match_completed_status == 'completed:false'){
                        $player_val->value -= $matchPoint;
                    }
                    $player_val->save();
                }
            }
        }

        return true;
    }
    */

    public function playerPhotocheck($offset, $limit){

        echo '<a href="'.url('/playerphotocheck')."/".($offset+$limit)."/".$limit.'">sonraki</a> OK ---- offset:'.$offset.' - limit:'.$limit.'</br>';

        $users = User::offset($offset)->limit($limit)->orderBy('id', 'asc')->get();

        foreach ($users as $user) {

            if(!empty($user->image)){

                if(file_exists('/'.env('MEDIA_PATH').$user->image)){
                    echo '( '.$user->id.' )->';
                    echo '<img height="100px" src="'.env('MEDIA_END').$user->image.'" style="background-color:red;" />';
                }else{
                    echo '( '.$user->id.'_logo_yok)-> /'.env('MEDIA_END').$user->image;
                    echo ' ';
                }

            }
        
        }
    }

    public function teamPhotocheck($offset, $limit){

        echo '<a href="'.url('/teamphotocheck')."/".($offset+$limit)."/".$limit.'">sonraki</a> OK ---- offset:'.$offset.' - limit:'.$limit.'</br>';

        $teams = Team::offset($offset)->limit($limit)->orderBy('id', 'asc')->get();

        foreach ($teams as $team) {

            if(!empty($team->image)){

                if(file_exists('/'.env('MEDIA_PATH').$team->image)){
                    echo '( '.$team->id.'_logo )->';
                    echo '<img height="100px" src="'.env('MEDIA_END').$team->image.'" style="background-color:red;" />';
                }else{
                    echo '<br>( '.$team->id.'_logo_yok)-> /'.env('MEDIA_END').$team->image;
                    echo ' <br>';

                    $team->image = null;
                    $team->save();

                }

            }

            if(!empty($team->cover_image)){

                if(file_exists('/'.env('MEDIA_PATH').$team->cover_image)){
                    echo '( '.$team->id.'_cover )->';
                    echo '<img height="100px" src="'.env('MEDIA_END').$team->cover_image.'" style="background-color:red;" />';
                }else{
                    echo '<br>( '.$team->id.'_cover_yok) - /'.env('MEDIA_END').$team->cover_image;
                    echo ' <br>';
                    
                    $team->cover_image = null;
                    $team->save();
                }

            }
        
        }
    }

    public function matchVideocheck($offset, $limit){

        echo '<a href="'.url('/matchvideocheck')."/".($offset+$limit)."/".$limit.'">sonraki</a> OK ---- offset:'.$offset.' - limit:'.$limit.'</br></br>';

        $matchVideos = MatchVideo::offset($offset)->limit($limit)->orderBy('id', 'desc')->get();
        
        foreach ($matchVideos as $matchVideo) {

            if (!empty($matchVideo->full_embed_code)){
                if ($matchVideo->full_source == 'app'){
                    
                    $checkUrl = $this->checkExternalFile($matchVideo->full_embed_code);
                    echo " (".$checkUrl .") - matchID: ".$matchVideo->match_id." - matchVideoID: ".$matchVideo->id." - full_source: ".$matchVideo->full_source." !!! ".$matchVideo->full_embed_code." </br> ";

                    if($checkUrl != 200 && $checkUrl != 301){
                        $matchVideo->full_embed_code = null;
                        $matchVideo->save();
                    }

                }else if ($matchVideo->full_source == 'youtube'){

                    $checkUrl = $this->checkExternalFile('https://www.youtube.com/oembed?format=json&url=http://www.youtube.com/watch?v='.$matchVideo->full_embed_code);
                    echo " (".$checkUrl .") - matchID: ".$matchVideo->match_id." - matchVideoID: ".$matchVideo->id." - full_source: ".$matchVideo->full_source." !!! ".$matchVideo->full_embed_code." </br> ";

                    if($checkUrl != 200 && $checkUrl != 301){
                        $matchVideo->full_embed_code = null;
                        $matchVideo->save();
                    }

                }
            }

            if (!empty($matchVideo->summary_embed_code)){
                if ($matchVideo->summary_source == 'app'){
                    
                    $checkUrl = $this->checkExternalFile($matchVideo->summary_embed_code);
                    echo " (".$checkUrl .") - matchID: ".$matchVideo->match_id." - matchVideoID: ".$matchVideo->id." - summary_source: ".$matchVideo->summary_source." !!! ".$matchVideo->summary_embed_code." </br> ";

                    if($checkUrl != 200 && $checkUrl != 301){
                        $matchVideo->summary_embed_code = null;
                        $matchVideo->save();
                    }

                }else if ($matchVideo->summary_source == 'youtube'){

                    $checkUrl = $this->checkExternalFile('https://www.youtube.com/oembed?format=json&url=http://www.youtube.com/watch?v='.$matchVideo->summary_embed_code);
                    echo " (".$checkUrl .") - matchID: ".$matchVideo->match_id." - matchVideoID: ".$matchVideo->id." - summary_source: ".$matchVideo->summary_source." !!! ".$matchVideo->summary_embed_code." </br> ";

                    if($checkUrl != 200 && $checkUrl != 301){
                        $matchVideo->summary_embed_code = null;
                        $matchVideo->save();
                    }

                }

            }
        
        }

        echo '<a href="'.url('/matchvideocheck')."/".($offset+$limit)."/".$limit.'">sonraki</a> OK ---- offset:'.$offset.' - limit:'.$limit.'</br></br>';
        echo '<script>document.querySelector("body").onload = function(){
                    setTimeout(function(){  window.location = "'.url('/matchvideocheck').'/'.($offset+$limit).'/'.$limit.'"; }, 2000);
                };</script>';
    }

    public function matchVideocheck2($offset, $limit){

        echo '<a href="'.url('/matchvideocheck2')."/".($offset+$limit)."/".$limit.'">sonraki</a> OK ---- offset:'.$offset.' - limit:'.$limit.'</br></br>';

        $matchVideos = MatchVideo::where('summary_embed_code', 'like', '%&%')->offset($offset)->limit($limit)->orderBy('id', 'desc')->get();
        
        foreach ($matchVideos as $matchVideo) {
            
            echo explode("&", $matchVideo->summary_embed_code)[0].' ------ '.$matchVideo->summary_embed_code.' <br>';
            
            $matchVideo->summary_embed_code = explode("&", $matchVideo->summary_embed_code)[0];
            $matchVideo->save();

            

            /*
            if (!empty($matchVideo->full_embed_code)){
                if ($matchVideo->full_source == 'app'){
                    
                    $checkUrl = $this->checkExternalFile($matchVideo->full_embed_code);
                    echo " (".$checkUrl .") - matchID: ".$matchVideo->match_id." - matchVideoID: ".$matchVideo->id." - full_source: ".$matchVideo->full_source." !!! ".$matchVideo->full_embed_code." </br> ";

                    if($checkUrl != 200 && $checkUrl != 301){
                        $matchVideo->full_embed_code = null;
                        $matchVideo->save();
                    }

                }else if ($matchVideo->full_source == 'youtube'){

                    $checkUrl = $this->checkExternalFile('https://www.youtube.com/oembed?format=json&url=http://www.youtube.com/watch?v='.$matchVideo->full_embed_code);
                    echo " (".$checkUrl .") - matchID: ".$matchVideo->match_id." - matchVideoID: ".$matchVideo->id." - full_source: ".$matchVideo->full_source." !!! ".$matchVideo->full_embed_code." </br> ";

                    if($checkUrl != 200 && $checkUrl != 301){
                        $matchVideo->full_embed_code = null;
                        $matchVideo->save();
                    }

                }
            }

            if (!empty($matchVideo->summary_embed_code)){
                if ($matchVideo->summary_source == 'app'){
                    
                    $checkUrl = $this->checkExternalFile($matchVideo->summary_embed_code);
                    echo " (".$checkUrl .") - matchID: ".$matchVideo->match_id." - matchVideoID: ".$matchVideo->id." - summary_source: ".$matchVideo->summary_source." !!! ".$matchVideo->summary_embed_code." </br> ";

                    if($checkUrl != 200 && $checkUrl != 301){
                        $matchVideo->summary_embed_code = null;
                        $matchVideo->save();
                    }

                }else if ($matchVideo->summary_source == 'youtube'){

                    $checkUrl = $this->checkExternalFile('https://www.youtube.com/oembed?format=json&url=http://www.youtube.com/watch?v='.$matchVideo->summary_embed_code);
                    echo " (".$checkUrl .") - matchID: ".$matchVideo->match_id." - matchVideoID: ".$matchVideo->id." - summary_source: ".$matchVideo->summary_source." !!! ".$matchVideo->summary_embed_code." </br> ";

                    if($checkUrl != 200 && $checkUrl != 301){
                        $matchVideo->summary_embed_code = null;
                        $matchVideo->save();
                    }

                }
            }
            */
        
        }

        echo '<a href="'.url('/matchvideocheck2')."/".($offset+$limit)."/".$limit.'">sonraki</a> OK ---- offset:'.$offset.' - limit:'.$limit.'</br></br>';
        //echo '<script>document.querySelector("body").onload = function(){
        //            setTimeout(function(){  window.location = "'.url('/matchvideocheck2').'/'.($offset+$limit).'/'.$limit.'"; }, 2000);
        //        };</script>';
    }

    public function matchPanoramaVideocheck($offset, $limit){

        echo '<a href="'.url('/matchpanoramavideocheck')."/".($offset+$limit)."/".$limit.'">sonraki</a> OK ---- offset:'.$offset.' - limit:'.$limit.'</br></br>';

        $matchPanoramas = MatchPanorama::whereNotNull('embed_code')->offset($offset)->limit($limit)->orderBy('id', 'desc')->get();
        
        foreach ($matchPanoramas as $matchPanorama) {

            if (!empty($matchPanorama->embed_code)){
                if ($matchPanorama->embed_source == 'app'){
                    
                    $checkUrl = $this->checkExternalFile($matchPanorama->embed_code);
                    echo " (".$checkUrl .") - matchID: ".$matchPanorama->match_id." - matchPanoramaID: ".$matchPanorama->id." - embed_source: ".$matchPanorama->embed_source." !!! ".$matchPanorama->embed_code." </br> ";

                    if($checkUrl != 200 && $checkUrl != 301){
                        $matchPanorama->embed_code = null;
                        $matchPanorama->save();
                    }

                }else if ($matchPanorama->embed_source == 'youtube'){

                    $checkUrl = $this->checkExternalFile('https://www.youtube.com/oembed?format=json&url=http://www.youtube.com/watch?v='.$matchPanorama->embed_code);
                    echo " (".$checkUrl .") - matchID: ".$matchPanorama->match_id." - matchPanoramaID: ".$matchPanorama->id." - embed_source: ".$matchPanorama->embed_source." !!! ".$matchPanorama->embed_code." </br> ";

                    if($checkUrl != 200 && $checkUrl != 301){
                        $matchPanorama->embed_code = null;
                        $matchPanorama->save();
                    }

                }
            }
        
        }

        echo '<a href="'.url('/matchpanoramavideocheck')."/".($offset+$limit)."/".$limit.'">sonraki</a> OK ---- offset:'.$offset.' - limit:'.$limit.'</br></br>';
        echo '<script>document.querySelector("body").onload = function(){
                    setTimeout(function(){  window.location = "'.url('/matchpanoramavideocheck').'/'.($offset+$limit).'/'.$limit.'"; }, 2000);
                };</script>';
    }

    public function matchPressConVideocheck($offset, $limit){

        echo '<a href="'.url('/matchpressconvideocheck')."/".($offset+$limit)."/".$limit.'">sonraki</a> OK ---- offset:'.$offset.' - limit:'.$limit.'</br></br>';

        $pressConferences = PressConferences::offset($offset)->limit($limit)->orderBy('id', 'desc')->get();
        
        foreach ($pressConferences as $pressConference) {

            if (!empty($pressConference->embed_code)){
                if ($pressConference->embed_source == 'app'){
                    
                    $checkUrl = $this->checkExternalFile($pressConference->embed_code);
                    echo " (".$checkUrl .") - matchID: ".$pressConference->match_id." - pressConferenceID: ".$pressConference->id." - embed_source: ".$pressConference->embed_source." !!! ".$pressConference->embed_code." </br> ";

                    if($checkUrl != 200 && $checkUrl != 301){
                        $pressConference->embed_code = null;
                        $pressConference->save();
                    }

                }else if ($pressConference->embed_source == 'youtube'){

                    $checkUrl = $this->checkExternalFile('https://www.youtube.com/oembed?format=json&url=http://www.youtube.com/watch?v='.$pressConference->embed_code);
                    echo " (".$checkUrl .") - matchID: ".$pressConference->match_id." - pressConferenceID: ".$pressConference->id." - embed_source: ".$pressConference->embed_source." !!! ".$pressConference->embed_code." </br> ";

                    if($checkUrl != 200 && $checkUrl != 301){
                        $pressConference->embed_code = null;
                        $pressConference->save();
                    }

                }
            }
        
        }

        echo '<a href="'.url('/matchpressconvideocheck')."/".($offset+$limit)."/".$limit.'">sonraki</a> OK ---- offset:'.$offset.' - limit:'.$limit.'</br></br>';
        echo '<script>document.querySelector("body").onload = function(){
                    setTimeout(function(){  window.location = "'.url('/matchpressconvideocheck').'/'.($offset+$limit).'/'.$limit.'"; }, 2000);
                };</script>';
    }

    public function matchActionVideocheck($offset, $limit){

        echo '<a href="'.url('/matchactionvideocheck')."/".($offset+$limit)."/".$limit.'">sonraki</a> OK ---- offset:'.$offset.' - limit:'.$limit.'</br></br>';

        $matchActions = MatchAction::offset($offset)->limit($limit)->orderBy('id', 'desc')->get();
        
        foreach ($matchActions as $matchAction) {

            if (!empty($matchAction->embed_code)){
                if ($matchAction->embed_source == 'app'){
                    
                    $checkUrl = $this->checkExternalFile($matchAction->embed_code);
                    echo " (".$checkUrl .") - matchID: ".$matchAction->match_id." - matchActionID: ".$matchAction->id." - embed_source: ".$matchAction->embed_source." !!! ".$matchAction->embed_code." </br> ";

                    if($checkUrl != 200 && $checkUrl != 301){
                        $matchAction->embed_code = null;
                        $matchAction->save();
                    }

                }else if ($matchAction->embed_source == 'youtube'){

                    $checkUrl = $this->checkExternalFile('https://www.youtube.com/oembed?format=json&url=http://www.youtube.com/watch?v='.$matchAction->embed_code);
                    echo " (".$checkUrl .") - matchID: ".$matchAction->match_id." - matchActionID: ".$matchAction->id." - embed_source: ".$matchAction->embed_source." !!! ".$matchAction->embed_code." </br> ";

                    if($checkUrl != 200 && $checkUrl != 301){
                        $matchAction->embed_code = null;
                        $matchAction->save();
                    }

                }
            }
        
        }

        echo '<a href="'.url('/matchactionvideocheck')."/".($offset+$limit)."/".$limit.'">sonraki</a> OK ---- offset:'.$offset.' - limit:'.$limit.'</br></br>';
        echo '<script>document.querySelector("body").onload = function(){
                    setTimeout(function(){  window.location = "'.url('/matchactionvideocheck').'/'.($offset+$limit).'/'.$limit.'"; }, 1000);
                };</script>';
                
    }



    function checkExternalFile($url)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_exec($ch);
        $retCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        return $retCode;
    }

    function aiPlayer($pid){

        $positionNameArr = array();
        $positionNameArr['FV'] = 'Forvet';
        $positionNameArr['SLA'] = 'Orta Saha';
        $positionNameArr['SĞA'] = 'Orta Saha';
        $positionNameArr['SLB'] = 'Defans';
        $positionNameArr['STP'] = 'Defans';
        $positionNameArr['SĞB'] = 'Defans';
        $positionNameArr['KL'] = 'Kaleci';


        $panoramaNameArr = array();
        $panoramaNameArr['Kaleci'] = 'Maçın Kalecisi';
        $panoramaNameArr['Forvet'] = 'Maçın Forveti';
        $panoramaNameArr['Orta Saha'] = 'Maçın Orta Sahası';
        $panoramaNameArr['Defans'] = 'Maçın Defansı';

        $panoramaNameArr['Dinamik'] = 'Maçın Dinamik Oyuncusu';
        $panoramaNameArr['Altın'] = 'Maçın Altın Oyuncusu';
        $panoramaNameArr['Golü'] = 'Maçın Golü';
        $panoramaNameArr['Gümüş'] = 'Maçın Gümüş Oyuncusu';

        // dump($panoramaNameArr);

        $player = Player::find($pid);
        $team = $player->team;

        $data = collect();
        $data->player = collect();
        $data->player_data = collect();
        $data->player_last_week_data = collect();
        $data->player_season_data = collect();


        $result = collect();
        /* son x maçta ki veriler
        - takımın attığı gol
        - takımın yediği gol
        - oyuncunun attığı gol
        - maç tarih saati
        */

        $activeSeason = null;
        $activeSeasons = array();
        $team_season_ids = $team->team_teamseason->pluck('season_id');
        //dump($team_season_ids);

        foreach ($team_season_ids as $season_id) {
            $season = Season::find($season_id);
            if($season->active == true){
                if($season->polymorphic_ctype_id != 32){
                    if($season->start_date <= Carbon::now()->format('Y-m-d') && $season->end_date >= Carbon::now()->format('Y-m-d')){
                        $activeSeasons[] = $season;
                    }
                }
            }
        }


        if(count($activeSeasons) == 1){

            $activeSeason = $activeSeasons[0];
            
            $colPlayer = collect();
            $colPlayer->player_id = $player->id;
            $colPlayer->full_name = $player->user->first_name.' '.$player->user->last_name;
            $colPlayer->team_id = $player->team_id;
            $colPlayer->team_name = $player->team->name;
            $colPlayer->position_id = $player->players_position->id;
            $colPlayer->position_name = $player->players_position->name;
            $colPlayer->position_code = $player->players_position->code;

            $data->player = $colPlayer;

            $player_matches = $player->match_player->all();

            foreach ($player_matches as $player_match) {
                if($player_match->match->season_id == $activeSeason->id && $player_match->match->completed) {
                    $colMatchData = collect();
                    $colMatchData->match_id = $player_match->match->id;
                    $colMatchData->total_goal = $player_match->match->match_action->where('type', 'goal')->count();
                    $colMatchData->opponent_goal = $player_match->match->match_action->where('type', 'goal')->where('team_id', '!=', $player_match->team_id)->count();
                    $colMatchData->team_goal = $player_match->match->match_action->where('type', 'goal')->where('team_id', $player_match->team_id)->count();
                    $colMatchData->player_goal = $player_match->match_action_goal->count();
                    $colMatchData->match_date = Carbon::parse($player_match->match->date." ".$player_match->match->time)->format('Y-m-d H:i:s');
                    $colMatchData->season_id = $player_match->match->season_id;
                    
                    $colMatchData->panorama = collect();
                    foreach ($player_match->match_player_panorama as $panorama) {
                        if($panorama->panorama_type->panorama_types_prize->period == 'match'){
                            $colMatchData->panorama->push($panorama->panorama_type->name);
                        }
                    }
                    
                    $data->player_data->push($colMatchData);
                }
            }

            /*
            dump('Ortalama');
            dump($data->player_data->avg('opponent_goal'));
            dump('Max');
            dump($data->player_data->max('opponent_goal'));
            dump('Median');
            dump($data->player_data->median('opponent_goal'));
            dump('Min');
            dump($data->player_data->min('opponent_goal'));
            dump('Mode');
            dump($data->player_data->mode('opponent_goal'));
            dump('Sum');
            dump($data->player_data->sum('opponent_goal'));
            dump('Count');
            dump($data->player_data->count('opponent_goal'));
            */
            
            for ($lastMatchCount=$data->player_data->count(); $lastMatchCount > 0; $lastMatchCount--) {

                $dynColIndexName = 'player_last_'.$lastMatchCount.'_match_data';
                /* Last X Match */
                $data_player_data_last_x_match = $data->player_data->slice(0,$lastMatchCount);
                $colLastXMatchData = collect();
                $colLastXMatchData->season_id = $activeSeason->id;
                $colLastXMatchData->match_count = $data_player_data_last_x_match->count();
                $colLastXMatchData->sum_total_goal = $data_player_data_last_x_match->sum('total_goal');
                $colLastXMatchData->sum_opponent_goal = $data_player_data_last_x_match->sum('opponent_goal');
                $colLastXMatchData->sum_team_goal = $data_player_data_last_x_match->sum('team_goal');
                $colLastXMatchData->sum_player_goal = $data_player_data_last_x_match->sum('player_goal');
                $colLastXMatchData->avg_total_goal = $data_player_data_last_x_match->avg('total_goal');
                $colLastXMatchData->avg_opponent_goal = $data_player_data_last_x_match->avg('opponent_goal');
                $colLastXMatchData->avg_team_goal = $data_player_data_last_x_match->avg('team_goal');
                $colLastXMatchData->avg_player_goal = $data_player_data_last_x_match->avg('player_goal');

                if($lastMatchCount == 1){
                    $colLastXMatchData->panorama = $data_player_data_last_x_match->first()->panorama;
                }

                $data->$dynColIndexName = $colLastXMatchData;
                /* Last X Match */

            }

            /* This Season */
            $colSeasonData = collect();
            $colSeasonData->season_id = $activeSeason->id;
            $colSeasonData->match_count = $data->player_data->count();
            $colSeasonData->sum_total_goal = $data->player_data->sum('total_goal');
            $colSeasonData->sum_opponent_goal = $data->player_data->sum('opponent_goal');
            $colSeasonData->sum_team_goal = $data->player_data->sum('team_goal');
            $colSeasonData->sum_player_goal = $data->player_data->sum('player_goal');
            $colSeasonData->avg_total_goal = $data->player_data->avg('total_goal');
            $colSeasonData->avg_opponent_goal = $data->player_data->avg('opponent_goal');
            $colSeasonData->avg_team_goal = $data->player_data->avg('team_goal');
            $colSeasonData->avg_player_goal = $data->player_data->avg('player_goal');
            $data->player_season_data = $colSeasonData;


            /* Date Limits */
            $startDate = Carbon::now(); //->subMonths(2)->subWeeks(2)->startOfDay(); // bugünden itibaren olması için Carbon::now()->startOfDay() kullan
            $lastWeekEndDate = $startDate->copy()->subWeeks(1);
            $lastMonthEndDate = $startDate->copy()->subMonths(1);

            /*
            dump("startDate: ".$startDate);
            dump("lastWeekEndDate: ".$lastWeekEndDate);
            dump("lastMonthEndDate: ".$lastMonthEndDate);
            dump($data->player_data->where('match_date', '<=', $startDate)->where('match_date', '>=', $lastWeekEndDate)->all());
            */

            $data_player_data_last_week = $data->player_data->where('match_date', '<=', $startDate)->where('match_date', '>=', $lastWeekEndDate);
            $colLastWeekData = collect();
            $colLastWeekData->season_id = $activeSeason->id;
            $colLastWeekData->match_count = $data_player_data_last_week->count();
            $colLastWeekData->sum_total_goal = $data_player_data_last_week->sum('total_goal');
            $colLastWeekData->sum_opponent_goal = $data_player_data_last_week->sum('opponent_goal');
            $colLastWeekData->sum_team_goal = $data_player_data_last_week->sum('team_goal');
            $colLastWeekData->sum_player_goal = $data_player_data_last_week->sum('player_goal');
            $colLastWeekData->avg_total_goal = $data_player_data_last_week->avg('total_goal');
            $colLastWeekData->avg_opponent_goal = $data_player_data_last_week->avg('opponent_goal');
            $colLastWeekData->avg_team_goal = $data_player_data_last_week->avg('team_goal');
            $colLastWeekData->avg_player_goal = $data_player_data_last_week->avg('player_goal');
            $data->player_last_week_data = $colLastWeekData;

            //dump($data->player_data->where('match_date', '<=', $startDate)->where('match_date', '>=', $lastMonthEndDate)->all());
            $data_player_data_last_month = $data->player_data->where('match_date', '<=', $startDate)->where('match_date', '>=', $lastMonthEndDate);
            $colLastMonthData = collect();
            $colLastMonthData->season_id = $activeSeason->id;
            $colLastMonthData->match_count = $data_player_data_last_month->count();
            $colLastMonthData->sum_total_goal = $data_player_data_last_month->sum('total_goal');
            $colLastMonthData->sum_opponent_goal = $data_player_data_last_month->sum('opponent_goal');
            $colLastMonthData->sum_team_goal = $data_player_data_last_month->sum('team_goal');
            $colLastMonthData->sum_player_goal = $data_player_data_last_month->sum('player_goal');
            $colLastMonthData->avg_total_goal = $data_player_data_last_month->avg('total_goal');
            $colLastMonthData->avg_opponent_goal = $data_player_data_last_month->avg('opponent_goal');
            $colLastMonthData->avg_team_goal = $data_player_data_last_month->avg('team_goal');
            $colLastMonthData->avg_player_goal = $data_player_data_last_month->avg('player_goal');
            $data->player_last_month_data = $colLastMonthData;
            /* Date Limits */

        }else{
            dump('aktif sezon da sorun var');
        }

        /*
        $asd = collect();
        $asd->position_name='Forvet';
        $asd->collection_name='player_last_1_match_data';
        $asd->data_name='panorama';
        $asd->operator='contains';
        $asd->limit=1;
        $asd->factor=1;
        $asd->result=0;
        $asd->text = 'Geçtiğimiz maçta Maçın Forveti seçildin. Böyle Devam!';
        */

        $dbData = $this->getDbData();

        echo 'player_id: <strong>'. $data->player->player_id .'</strong> - full_name: <strong>'. $data->player->full_name .'</strong> - team_id: <strong>'. $data->player->team_id .'</strong> - team_name: <strong>'. $data->player->team_name .'</strong> - position_id: <strong>'. $data->player->position_id .'</strong> - position_name: <strong>'. $data->player->position_name .'</strong> - position_code: <strong>'. $data->player->position_code .'</strong>';
        
        foreach ($dbData->where('position_name', $positionNameArr[$data->player->position_code]) as $key => $value) {
            $collection_name = $value->collection_name;
            $data_name = $value->data_name;

            if(!empty($data->$collection_name)){
                
                if($value->operator == 'contains'){
                    /*
                        dump('??----------------');
                        dump($value->operator);
                        dump('2 ----------------');
                        dump($panoramaNameArr[$positionNameArr[$data->player->position_code]]);

                        
                        dump('????----------------');
                        dump($data->$collection_name->$data_name);
                        dump($data->$collection_name->$data_name->contains($panoramaNameArr[$positionNameArr[$data->player->position_code]]));
                        dump($value->factor);
                    */

                    if($data->$collection_name->$data_name->contains($panoramaNameArr[$positionNameArr[$data->player->position_code]])){
                        $value->result = 1 * $value->factor;
                        $value->resultCalc = $value->limit.' x '.$value->factor.' = '. ($value->limit*$value->factor).' -> (limit x factor)';
                        $value->text = str_replace('--X--', $data->$collection_name->$data_name, $value->text);
                        $result->push($value);
                    }

                }else if($this->cif($data->$collection_name->$data_name, $value->operator, $value->limit)){
                    //dump('----------------');
                    //dump($value);
                    //dump($data->$collection_name->$data_name);
                    $value->result = $data->$collection_name->$data_name * $value->factor;
                    $value->resultCalc = $value->limit.' x '.$value->factor.' = '. ($value->limit*$value->factor).' -> (limit x factor)';
                    $value->text = str_replace('--X--', $data->$collection_name->$data_name, $value->text);
                    
                    $result->push($value);
                }
            }

        }

        if($result->count() > 0){
            foreach ($result as $res) {
                dump($res);
            }
        }else{
            echo '<h2>!!! Gösterilecek uygun bir cümle bulunamadı.</h2>';
        }

        //$data->player_data = $data->player_data->sortBy('match_date');
        //dump($data->player_data->where('match_date', '>=', Carbon::now()->subMonths(3)->addDays(1))->all());
        
        //$strDate = Carbon::now()->subMonths(2)->subWeeks(2);
        //dump($strDate);
        //dump($data->player_data->where('match_date', '<=', $strDate)->where('match_date', '>=', $strDate->subWeek())->all()); //
        
        dump($data);

        /*
        dump($player);
        dump($player->players_position);
        dump($player->team);
        dump($player->match_player);
        dump($player->team_playerteamhistory);
        dump($player->team_teamplayer);
        */



        echo '<style>td { text-align: center; }</style>';
        echo '<h1>Sentence List</h1>';
        echo '<table width="100%" border="1">';
        echo '<tr>';
        echo '<th>Position</th>';
        echo '<th>Name</th>';
        echo '<th>Data</th>';
        echo '<th>Operator</th>';
        echo '<th>Limit</th>';
        echo '<th>Sentence</th>';
        echo '</tr>';
        foreach ($dbData as $value) {
            echo '<tr>';
            echo '<td>'.$value->position_name.'</td>';
            echo '<td>'.$value->collection_name.'</td>';
            echo '<td>'.$value->data_name.'</td>';
            echo '<td>'.$value->operator.'</td>';
            echo '<td>'.$value->limit.'</td>';
            echo '<td>'.$value->text.'</td>';
            echo '</tr>';
        }
        echo '</table>';

        die;

    }

    function cif($value1, $operator, $value2){
        if($operator == '<='){
            return $value1 <= $value2;
        }else if($operator == '>='){
            return $value1 >= $value2;            
        }else if($operator == '>'){
            return $value1 > $value2;
        }else if($operator == '<'){
            return $value1 < $value2;
        }else if($operator == '=='){
            return $value1 == $value2;
        }
    }

    function getDbData(){
        $dbCollection = collect();

        $asd = collect();
        $asd->position_name='Kaleci';
        $asd->collection_name='player_last_1_match_data';
        $asd->data_name='sum_opponent_goal';
        $asd->operator='<=';
        $asd->limit=3;
        $asd->factor=1;
        $asd->result=0;
        $asd->text = 'Son maçında sadece “--X--” gol yedin. Bir halı saha kalecisi için muhteşem performans, tebrikler!';
        $dbCollection->push($asd);

        $asd = collect();
        $asd->position_name='Kaleci';
        $asd->collection_name='player_last_1_match_data';
        $asd->data_name='sum_opponent_goal';
        $asd->operator='>=';
        $asd->limit=5;
        $asd->factor=1;
        $asd->result=0;
        $asd->text = 'Takımın son maçında “--X--” gol yedi. Takımını sırtlaman gerekiyor!';
        $dbCollection->push($asd);

        $asd = collect();
        $asd->position_name='Kaleci';
        $asd->collection_name='player_season_data';
        $asd->data_name='avg_opponent_goal';
        $asd->operator='<=';
        $asd->limit=2;
        $asd->factor=1;
        $asd->result=0;
        $asd->text = 'Bu sezon ortalama “--X--” yenen gol ile oynuyorsun. Harika performans!';
        $dbCollection->push($asd);

        $asd = collect();
        $asd->position_name='Kaleci';
        $asd->collection_name='player_season_data';
        $asd->data_name='avg_opponent_goal';
        $asd->operator='>';
        $asd->limit=2;
        $asd->factor=1;
        $asd->result=0;
        $asd->text = 'Bu sezon ortalama “--X--” yenen gol ile oynuyorsun. Ortalamanı düşürmek için daha çok çalışmalısın!';
        $dbCollection->push($asd);

        $asd = collect();
        $asd->position_name='Kaleci';
        $asd->collection_name='player_last_X_match_data';
        $asd->data_name='sum_opponent_goal';
        $asd->operator='==';
        $asd->limit=0;
        $asd->factor=1;
        $asd->result=0;
        $asd->text = 'Takımın senin oynadığın “--X--” maçta gol yemedi. Sağlam performans!';
        $dbCollection->push($asd);

        $asd = collect();
        $asd->position_name='Defans';
        $asd->collection_name='player_last_1_match_data';
        $asd->data_name='sum_opponent_goal';
        $asd->operator='<=';
        $asd->limit=3;
        $asd->factor=2;
        $asd->result=0;
        $asd->text = 'Son maçta harika bir savunma performansı sergilediniz. Böyle devam!';
        $dbCollection->push($asd);

        $asd = collect();
        $asd->position_name='Defans';
        $asd->collection_name='player_last_1_match_data';
        $asd->data_name='sum_opponent_goal';
        $asd->operator='>=';
        $asd->limit=5;
        $asd->factor=1;
        $asd->result=0;
        $asd->text = 'Takımın son maçında “--X--” gol yedi. Savunmanız alarm veriyor!';
        $dbCollection->push($asd);

        $asd = collect();
        $asd->position_name='Defans';
        $asd->collection_name='player_last_1_match_data';
        $asd->data_name='sum_player_goal';
        $asd->operator='>=';
        $asd->limit=1;
        $asd->factor=1;
        $asd->result=0;
        $asd->text = 'Son müsabakada takımına gol katkısı da verdin. Tebrikler!';
        $dbCollection->push($asd);

        $asd = collect();
        $asd->position_name='Defans';
        $asd->collection_name='player_last_X_match_data';
        $asd->data_name='sum_opponent_goal';
        $asd->operator='==';
        $asd->limit=0;
        $asd->factor=1;
        $asd->result=0;
        $asd->text = 'Takımın son maçında “--X--” gol yedi. Savunmanız alarm veriyor!';
        $dbCollection->push($asd);

        $asd = collect();
        $asd->position_name='Orta Saha';
        $asd->collection_name='player_last_week_data';
        $asd->data_name='sum_player_goal';
        $asd->operator='>=';
        $asd->limit=1;
        $asd->factor=1;
        $asd->result=0;
        $asd->text = 'Bu hafta çok iyi performans gösterdin. Attığın “--X--” gol takımının galibiyetine yardımcı oldu. Bravo!';
        $dbCollection->push($asd);

        $asd = collect();
        $asd->position_name='Orta Saha';
        $asd->collection_name='player_last_week_data';
        $asd->data_name='sum_opponent_goal';
        $asd->operator='>=';
        $asd->limit=5;
        $asd->factor=1;
        $asd->result=0;
        $asd->text = 'Takımın bu hafta fazla gol yedi. Takım savunmasına yardım etmelisin!';
        $dbCollection->push($asd);

        $asd = collect();
        $asd->position_name='Orta Saha';
        $asd->collection_name='player_last_5_match_data';
        $asd->data_name='sum_player_goal';
        $asd->operator='>=';
        $asd->limit=1;
        $asd->factor=1;
        $asd->result=0;
        $asd->text = 'Son 5 karşılaşmada “--X--” gol katkısında bulundun. Daha fazlasını yapabilirsin!';
        $dbCollection->push($asd);

        $asd = collect();
        $asd->position_name='Orta Saha';
        $asd->collection_name='player_last_5_match_data';
        $asd->data_name='sum_player_goal';
        $asd->operator='==';
        $asd->limit=0;
        $asd->factor=1;
        $asd->result=0;
        $asd->text = 'Son 5 karşılaşmada takımına gol katkısında bulunamadın. Daha fazlasını yapabilirsin!';
        $dbCollection->push($asd);

        $asd = collect();
        $asd->position_name='Forvet';
        $asd->collection_name='player_last_1_match_data';
        $asd->data_name='sum_player_goal';
        $asd->operator='>=';
        $asd->limit=1;
        $asd->factor=1;
        $asd->result=0;
        $asd->text = 'Son karşılaşmada “--X--” gol attın. Ligin en formda oyuncularından birisin, tebrikler!';
        $dbCollection->push($asd);

        $asd = collect();
        $asd->position_name='Forvet';
        $asd->collection_name='player_last_1_match_data';
        $asd->data_name='sum_player_goal';
        $asd->operator='==';
        $asd->limit=0;
        $asd->factor=1;
        $asd->result=0;
        $asd->text = 'Son karşılaşmada ağları sarsmayı başaramadın. Bir sorun mu var?';
        $dbCollection->push($asd);

        $asd = collect();
        $asd->position_name='Forvet';
        $asd->collection_name='player_last_5_match_data';
        $asd->data_name='sum_player_goal';
        $asd->operator='>=';
        $asd->limit=10;
        $asd->factor=1;
        $asd->result=0;
        $asd->text = 'Son 5 karşılaşmada “--X--” gol katkısında bulundun. Daha fazlasını yapabilirsin!';
        $dbCollection->push($asd);

        $asd = collect();
        $asd->position_name='Forvet';
        $asd->collection_name='player_last_5_match_data';
        $asd->data_name='sum_player_goal';
        $asd->operator='==';
        $asd->limit=0;
        $asd->factor=1;
        $asd->result=0;
        $asd->text = 'Son 5 karşılaşmada takımına gol katkısında bulunamadın. Daha fazlasını yapabilirsin!';
        $dbCollection->push($asd);

        

        $asd = collect();
        $asd->position_name='Kaleci';
        $asd->collection_name='player_last_1_match_data';
        $asd->data_name='panorama';
        $asd->operator='contains';
        $asd->limit=1;
        $asd->factor=10;
        $asd->result=0;
        $asd->text = 'Geçtiğimiz maçta Maçın Kalecisi seçildin. Böyle Devam!';
        $dbCollection->push($asd);

        $asd = collect();
        $asd->position_name='Defans';
        $asd->collection_name='player_last_1_match_data';
        $asd->data_name='panorama';
        $asd->operator='contains';
        $asd->limit=1;
        $asd->factor=10;
        $asd->result=0;
        $asd->text = 'Geçtiğimiz maçta Maçın Defansı seçildin. Böyle Devam!';
        $dbCollection->push($asd);

        $asd = collect();
        $asd->position_name='Orta Saha';
        $asd->collection_name='player_last_1_match_data';
        $asd->data_name='panorama';
        $asd->operator='contains';
        $asd->limit=1;
        $asd->factor=10;
        $asd->result=0;
        $asd->text = 'Geçtiğimiz maçta Maçın Orta Sahası seçildin. Böyle Devam!';
        $dbCollection->push($asd);

        $asd = collect();
        $asd->position_name='Forvet';
        $asd->collection_name='player_last_1_match_data';
        $asd->data_name='panorama';
        $asd->operator='contains';
        $asd->limit=1;
        $asd->factor=1;
        $asd->result=0;
        $asd->text = 'Geçtiğimiz maçta Maçın Forveti seçildin. Böyle Devam!';
        $dbCollection->push($asd);

        return($dbCollection);
    }



}














