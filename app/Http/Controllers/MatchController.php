<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Validator;

use App\Classes\SmsClass;

use Carbon\Carbon;
use App\User;

use App\Season;
use App\TeamTeamSeason;

use App\Match;
use App\MatchVideo;
use App\MatchPlayer;
use App\MatchAction;
use App\MatchPanorama;
use App\MatchImage;

use App\PointType;

use App\PressConferences;
use App\PressConferencesPlayers;

use App\Team;
use App\Player;
use App\TeamPlayerTeamHistory;

class MatchController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        return view('dashboard');
    }

    public function crud($id = null) {
        $match = NULL;
        if(!is_null($id)){
            $match = Match::find($id);
        }
        return view('dashboard', array('model_data' => $match));
    }

    public function save(Request $request) {

        //dump($season);
        //dd($request->input());

        $text = "";
        $match_completed_status = null;
        $smsSendMatchCompletedControl = false;
        $smsSendNewMatchControl = false;

        if ($request->crud == 'delete') {

            $model = Match::find($request->id);
            
            if($model->season->polymorphic_ctype_id == 32){
                if(!empty($model->eliminationtree_items)){
                    $eliminationtree_item = $model->eliminationtree_items;
                    $eliminationtree_item->match_id = null;
                    $eliminationtree_item->save();
                }
            }
            
            if(!empty($model->match_action)){
                foreach ($model->match_action as $match_action) {
                    $match_action->delete();
                }
            }

            if(!empty($model->match_player)){
                foreach ($model->match_player as $match_player) {
                    if(!empty($match_player->press_conferences_player)){
                        $match_player->press_conferences_player->delete();
                    }
                    $match_player->delete();
                }
            }

            if(!empty($model->match_video)){
                $model->match_video->delete();
            }
            
            if(!empty($model->match_image)){
                foreach ($model->match_image as $match_image) {
                    $match_image->delete();
                }
            }
            
            if(!empty($model->match_press)){
                $model->match_press->delete();
            }

            if(!empty($model->reservation)){
                if(!empty($model->reservation->reservation_offer)){
                    foreach ($model->reservation->reservation_offer as $reservation_offer) {
                        $reservation_offer->delete();
                    }
                }
                $model->reservation->delete();
            }

            $model->delete();
            $text = 'Başarıyla Silindi...';
            
        }else{

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $model = new Match();
            }else if($request->crud == 'edit'){
                $model = Match::find($request->id);
            }
            
            $rules = array();
            $formConfig = config('forms.'.$request->segment);

            if(!is_null($formConfig)){
                foreach ($formConfig as $key => $value){
                    if(!empty(data_get($value, 'validation'))){
                        $rules[$key] = data_get($value, 'validation');
                    }
                }
            }
            
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
               return \Redirect::back()->withErrors($validator)->withInput();
            }

            $season = Season::find($request->season_id);
            if($season->polymorphic_ctype_id != 32){ /* elimination = 32 */
            
                /* Start - 5 Maç Kontrolü */
                    $team1_id = $request->team1_id;
                    $teamSeason1 = TeamTeamSeason::where('season_id', $request->season_id)->where('team_id', $request->team1_id)->first();
                    $match1Count = Match::where('season_id', $request->season_id)->where('completed', true)->
                    where(function ($query) use ($team1_id) {
                        $query->where('team1_id', $team1_id)->orWhere('team2_id', $team1_id);
                    })->count();
                    
                    if(!empty($teamSeason1)){
                        if($match1Count >= 5 && !$teamSeason1->squad_locked){
                            $text = $teamSeason1->team->name.' takımı 5 maç yapmış. Önce takımı kilitlemelisiniz.';
                            return redirect($request->segment)->with('message', array('text' => $text, 'status' => 'error'));
                        }
                    }

                    $team2_id = $request->team2_id;
                    $teamSeason2 = TeamTeamSeason::where('season_id', $request->season_id)->where('team_id', $request->team2_id)->first();
                    $match2Count = Match::where('season_id', $request->season_id)->where('completed', true)->
                    where(function ($query) use ($team2_id) {
                        $query->where('team1_id', $team2_id)->orWhere('team2_id', $team2_id);
                    })->count();
                    
                    if(!empty($teamSeason2)){
                        if($match2Count >= 5 && !$teamSeason2->squad_locked){
                            $text = $teamSeason2->team->name.' takımı 5 maç yapmış. Önce takımı kilitlemelisiniz.';
                            return redirect($request->segment)->with('message', array('text' => $text, 'status' => 'error'));
                        }
                    }
                /* END - 5 Maç Kontrolü */

            }else{
                $team1_id = $request->team1_id;
                $teamSeason1 = TeamTeamSeason::where('season_id', $request->season_id)->where('team_id', $request->team1_id)->first();
                $teamSeason1->squad_locked = true;
                $teamSeason1->save();

                $team2_id = $request->team2_id;
                $teamSeason2 = TeamTeamSeason::where('season_id', $request->season_id)->where('team_id', $request->team2_id)->first();
                $teamSeason2->squad_locked = true;
                $teamSeason2->save();
            }
       
            $model->season_id = $request->season_id;
            $model->team1_id = $request->team1_id;
            $model->team2_id = $request->team2_id;
            $model->ground_id = $request->ground_id;
            $model->date = Carbon::parse($request->date)->format('Y-m-d');
            $model->time = $request->time_e;
            $model->referee_id = $request->referee_id;
            $model->coordinator_id = $request->coordinator_id;


            if ($request->crud == 'add') {

                $model->team1_goal = 0;
                $model->team2_goal = 0;
                $model->team1_point = 0;
                $model->team2_point = 0;
                $model->completed = false;

                $smsSendNewMatchControl = true;

            }else if($request->crud == 'edit'){
                
                if(!empty($request->completed) && $model->completed == false){
                    //dump('cb:'.$request->completed.' - md:'.$model->completed.' - tamamlandıya değiştir.');
                    $match_completed_status = 'completed:true'; // tamamlandıya değiştir.
                    $model->completed_status = $request->completed_status;
                    $model->completed_at = Carbon::now();
                    $model->completed = true;
                    $smsSendMatchCompletedControl = true;
                }else if(empty($request->completed) && $model->completed == true){
                    //dump('cb:'.$request->completed.' - md:'.$model->completed.' - devam ediyora değiştir.');
                    $match_completed_status = 'completed:false'; // devam ediyora değiştir.
                    $model->completed_status = null;
                    $model->completed_at = null;
                    $model->completed = false;
                }else{
                    // değişiklik yok.
                    // dump('cb:'.$request->completed.' - md:'.$model->completed.' - değişiklik yok.');
                }

            }
            
            //dump($model);
            $model->save();

            $season = Season::find($request->season_id);
            $min_point = 0;
            
            if($season->polymorphic_ctype_id == 33){
                $min_point = $season->league_fixtureseason->min_team_point;
            }else if($season->polymorphic_ctype_id == 34){
                $min_point = $season->league_pointseason->min_team_point;
            }

            $teamSeason1 = TeamTeamSeason::where('season_id', $request->season_id)->where('team_id', $request->team1_id)->first();
            if(empty($teamSeason1)){

                $teamSeason = new TeamTeamSeason();
                if($season->polymorphic_ctype_id == 32){ /* elimination = 32 */
                    $teamSeason->squad_locked = true;
                }else{
                    $teamSeason->squad_locked = false;
                }

                $teamSeason->point = $min_point;
                $teamSeason->season_id = $request->season_id;
                $teamSeason->team_id = $request->team1_id;

                $teamSeason->remaining_transfer_count = $season->allowed_transfer_count;
                $teamSeason->playoff_member = true;
                $teamSeason->drawn = 0;
                $teamSeason->goal_against = 0;
                $teamSeason->goal_for = 0;
                $teamSeason->lost = 0;
                $teamSeason->match_total = 0;
                $teamSeason->won = 0;
                $teamSeason->gk_save = 0;
                $teamSeason->red_card = 0;
                $teamSeason->yellow_card = 0;

                $teamSeason->save();
            }

            $teamSeason2 = TeamTeamSeason::where('season_id', $request->season_id)->where('team_id', $request->team2_id)->first();
            if(empty($teamSeason2)){

                $teamSeason = new TeamTeamSeason();
                if($season->polymorphic_ctype_id == 32){ /* elimination = 32 */
                    $teamSeason->squad_locked = true;
                }else{
                    $teamSeason->squad_locked = false;
                }

                $teamSeason->point = $min_point;
                $teamSeason->season_id = $request->season_id;
                $teamSeason->team_id = $request->team2_id;

                $teamSeason->remaining_transfer_count = $season->allowed_transfer_count;
                $teamSeason->playoff_member = true;
                $teamSeason->drawn = 0;
                $teamSeason->goal_against = 0;
                $teamSeason->goal_for = 0;
                $teamSeason->lost = 0;
                $teamSeason->match_total = 0;
                $teamSeason->won = 0;
                $teamSeason->gk_save = 0;
                $teamSeason->red_card = 0;
                $teamSeason->yellow_card = 0;

                $teamSeason->save();
            }

            if ($request->crud == 'add') {

                $match_video = MatchVideo::where('match_id', $model->id)->first();
                if (is_null($match_video)) {
                    $match_video = new MatchVideo();
                    $match_video->match_id = $model->id;
                }

                $match_video->status = 'recordable';
                $match_video->live_source = 'app';
                $match_video->live_embed_code = null;
                $match_video->full_source = 'app';
                $match_video->full_embed_code = null;
                $match_video->summary_source = 'app';
                $match_video->summary_embed_code = null;
                $match_video->save();

            }else if($request->crud == 'edit'){

                if(!empty($match_completed_status)){
                    //Tamamlanmış maçlar için teamseason kayıtlarında güncellemeler yapar
                    $this->teamseasons_statistics_update($model, $match_completed_status);
                    $this->players_history_update($model, $match_completed_status);
                    $this->players_deger_update($model, $match_completed_status);
                }

            }
            /*
            if($smsSendMatchCompletedControl && $model->sendsms != 1){
                //SMS GÖNDER; //Mac Tamamlandiya Alininca Takim Kadrosundaki Tum Oyunculara Gonder
                $smsHelper = new SmsClass();
                $smsHelper->smsSendMatchCompletedTrigger($model);
                $model->sendsms = 1;
                $model->save();
            }

            if($smsSendNewMatchControl){
                //SMS GÖNDER; //Mac Olusturulunca Takim Kaptanina Gonder
                $smsHelper = new SmsClass();
                $smsHelper->smsSendNewMatch($model);
            }
            */

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $text = 'Başarıyla Eklendi...';
            }else if($request->crud == 'edit'){
                $text = 'Başarıyla Güncellendi...';
            }
        }

        return redirect($request->segment)->with('message', array('text' => $text, 'status' => 'success'));
    }

    public function teamseasons_statistics_update($match, $match_completed_status)
    {
        //dump($match_completed_status);
        if($match_completed_status == 'completed:true'){
            // tamamlandıya değiştir.
            $match->team1_point_before = $match->team1->team_teamseason->where('season_id', $match->season->id)->first()->point;
            $match->team2_point_before = $match->team2->team_teamseason->where('season_id', $match->season->id)->first()->point;

            if(is_null($match->completed_status)){
                $match->team1_goal = $match->match_action->where('team_id', $match->team1_id)->where('type', 'goal')->count();
                $match->team2_goal = $match->match_action->where('team_id', $match->team2_id)->where('type', 'goal')->count();
            }else{
                //HÜKMEN DURUMU GOLLERİ
                if($match->season->polymorphic_ctype_id == 34){
                    // Season.POINT
                    if($match->completed_status == 1){
                        $match->team1_goal = 3;
                        $match->team2_goal = 0;
                    }else if($match->completed_status == 2){
                        $match->team1_goal = 0;
                        $match->team2_goal = 3;
                    }
                }else if($match->season->polymorphic_ctype_id == 33){
                    // Season.FIXTURE
                    if($match->completed_status == 1){
                        $match->team1_goal = 5;
                        $match->team2_goal = 0;
                    }else if($match->completed_status == 2){
                        $match->team1_goal = 0;
                        $match->team2_goal = 5;
                    }
                }else if($match->season->polymorphic_ctype_id == 32){
                    if($match->season->league_eliminationseason->base_season->polymorphic_ctype_id == 34){
                        // Season.POINT
                        if($match->completed_status == 1){
                            $match->team1_goal = 3;
                            $match->team2_goal = 0;
                        }else if($match->completed_status == 2){
                            $match->team1_goal = 0;
                            $match->team2_goal = 3;
                        }
                    }else if($match->season->league_eliminationseason->base_season->polymorphic_ctype_id == 33){
                        // Season.FIXTURE
                        if($match->completed_status == 1){
                            $match->team1_goal = 5;
                            $match->team2_goal = 0;
                        }else if($match->completed_status == 2){
                            $match->team1_goal = 0;
                            $match->team2_goal = 5;
                        }
                    }
                }
            }

            /***** calculate_points *******/
            if($match->season->polymorphic_ctype_id == 33 || $match->season->polymorphic_ctype_id == 34){
                
                // takımların durumunu bul
                if($match->team1_goal > $match->team2_goal){
                    $team1_status = 'win';
                    $team2_status = 'defeat';
                }else if($match->team1_goal < $match->team2_goal){
                    $team1_status = 'defeat';
                    $team2_status = 'win';
                }else if($match->team1_goal == $match->team2_goal){
                    $team1_status = 'draw';
                    $team2_status = 'draw';
                }

                if($match->season->polymorphic_ctype_id == 34){
                    // Season.POINT
                    // takımların renk durumlarını tespit et
                    $team1_point_type = PointType::where('min_point', '<=', $match->team1_point_before)->where('max_point', '>=', $match->team1_point_before)->first();
                    $team2_point_type = PointType::where('min_point', '<=', $match->team2_point_before)->where('max_point', '>=', $match->team2_point_before)->first();

                    //dump('team1_point_type: '.$team1_status);
                    //dump($team1_point_type);
                    
                    //dump('team2_point_type: '.$team2_status);
                    //dump($team2_point_type);

                    // renk durumuna göre takımlara puan ver
                    if ($team1_status == 'win'){
                        $team1_point = $team2_point_type->win_score;
                    }else if ($team1_status == 'defeat'){
                        $team1_point = -$team2_point_type->defeat_score;
                    }else if ($team1_status == 'draw'){
                        $team1_point = $team2_point_type->draw_score;
                    }

                    if ($team2_status == 'win'){
                        $team2_point = $team1_point_type->win_score;
                    }else if ($team2_status == 'defeat'){
                        $team2_point = -$team1_point_type->defeat_score;
                    }else if ($team2_status == 'draw'){
                        $team2_point = $team1_point_type->draw_score;
                    }

                }else if($match->season->polymorphic_ctype_id == 33){
                    // Season.FIXTURE
                    // Normal puanlama
                    if ($team1_status == 'win'){
                        $team1_point = 3;
                    } else if ($team1_status == 'defeat'){
                        $team1_point = 0;
                    } else if ($team1_status == 'draw'){
                        $team1_point = 1;
                    }

                    if ($team2_status == 'win'){
                        $team2_point = 3;
                    } else if ($team2_status == 'defeat'){
                        $team2_point = 0;
                    } else if ($team2_status == 'draw'){
                        $team2_point = 1;
                    }

                }
              
            }else{

                $team1_point = 0;
                $team2_point = 0;

            }

            //dump('team1_point: '.$team1_point);
            //dump('team2_point: '.$team2_point);

            /***** calculate_points *******/
            $match->team1_point = $team1_point;
            $match->team2_point = $team2_point;
            //dump($match);
            $match->save();

            /***** teamseasons_update_by_match *******/
            //Tamamlanmış maçlar için teamseason kayıtlarında güncellemeler yapar
            
            //Güncelleme ve Silme işleminden önce istatistikler geri alınır yani - işleme yapılır
            //Eğer yeni tamamlanmış veya güncellenen maç verileri ise ekleme yapılır yani + işlem yapılır

            $team1_season = $match->team1->team_teamseason->where('season_id', $match->season->id)->first();
            $team2_season = $match->team2->team_teamseason->where('season_id', $match->season->id)->first();


            // POINTS
            $season = Season::find($match->season_id);
            $min_point = 0;
            if($season->polymorphic_ctype_id == 33){ // fixtureseason
                $min_point = $season->league_fixtureseason->min_team_point;
            }else if($season->polymorphic_ctype_id == 34){ // pointseason
                $min_point = $season->league_pointseason->min_team_point;
            }

            if($match->season->polymorphic_ctype_id == 34 || $match->season->polymorphic_ctype_id == 33){
                
                $team1_season->point += $match->team1_point;
                if($team1_season->point < $min_point){
                    $team1_season->point = $min_point;
                }

                $team2_season->point += $match->team2_point;
                if($team2_season->point < $min_point){
                    $team2_season->point = $min_point;
                }

            } 

            // GOAL FOR - GOAL AGAINST
            $team1_season->goal_for += $match->team1_goal;
            $team1_season->goal_against += $match->team2_goal;

            $team2_season->goal_for += $match->team2_goal;
            $team2_season->goal_against += $match->team1_goal;

            // WON - DRAWN - LOST
            if ($match->team1_goal == $match->team2_goal){
                $team1_season->drawn += 1;
                $team2_season->drawn += 1;
            }else if ($match->team1_goal > $match->team2_goal){
                $team1_season->won += 1;
                $team2_season->lost += 1;
            }else if ($match->team1_goal < $match->team2_goal){
                $team1_season->lost += 1;
                $team2_season->won += 1;
            }

            // MATCH TOTAL
            $team1_season->match_total += 1;
            $team2_season->match_total += 1;
            


            $team1_season->gk_save += $match->match_action->where('team_id', $match->team1_id)->where('type', 'saving')->count();
            $team2_season->gk_save += $match->match_action->where('team_id', $match->team2_id)->where('type', 'saving')->count();

            $team1_season->red_card += $match->match_action->where('team_id', $match->team1_id)->where('type', 'red_card')->count();
            $team2_season->red_card += $match->match_action->where('team_id', $match->team2_id)->where('type', 'red_card')->count();

            $team1_season->yellow_card += $match->match_action->where('team_id', $match->team1_id)->where('type', 'yellow_card')->count();
            $team2_season->yellow_card += $match->match_action->where('team_id', $match->team2_id)->where('type', 'yellow_card')->count();



            //dump('team1_season: ');
            //dump($team1_season);
            
            //dump('team2_season: ');
            //dump($team2_season);

            $team1_season->save();
            $team2_season->save();
            /***** teamseasons_update_by_match *******/

        }else if($match_completed_status == 'completed:false'){
            // devam ediyora değiştir;

            /***** teamseasons_update_by_match *******/
            //Tamamlanmış maçlar için teamseason kayıtlarında güncellemeler yapar
            
            //Güncelleme ve Silme işleminden önce istatistikler geri alınır yani - işleme yapılır
            //Eğer yeni tamamlanmış veya güncellenen maç verileri ise ekleme yapılır yani + işlem yapılır

            $team1_season = $match->team1->team_teamseason->where('season_id', $match->season->id)->first();
            $team2_season = $match->team2->team_teamseason->where('season_id', $match->season->id)->first();

            // POINTS
            $team1_season->point = $match->team1_point_before;
            $team2_season->point = $match->team2_point_before;

            // GOAL FOR - GOAL AGAINST
            $team1_season->goal_for -= $match->team1_goal;
            $team1_season->goal_against -= $match->team2_goal;

            $team2_season->goal_for -= $match->team2_goal;
            $team2_season->goal_against -= $match->team1_goal;

            // WON - DRAWN - LOST
            if ($match->team1_goal == $match->team2_goal){
                $team1_season->drawn -= 1;
                $team2_season->drawn -= 1;
            }else if ($match->team1_goal > $match->team2_goal){
                $team1_season->won -= 1;
                $team2_season->lost -= 1;
            }else if ($match->team1_goal < $match->team2_goal){
                $team1_season->lost -= 1;
                $team2_season->won -= 1;
            }

            // MATCH TOTAL
            $team1_season->match_total -= 1;
            $team2_season->match_total -= 1;

            $match->team1_goal = 0;
            $match->team2_goal = 0;
            $match->team1_point = 0;
            $match->team2_point = 0;

            

            $team1_season->gk_save -= $match->match_action->where('team_id', $match->team1_id)->where('type', 'saving')->count();
            $team2_season->gk_save -= $match->match_action->where('team_id', $match->team2_id)->where('type', 'saving')->count();

            $team1_season->red_card -= $match->match_action->where('team_id', $match->team1_id)->where('type', 'red_card')->count();
            $team2_season->red_card -= $match->match_action->where('team_id', $match->team2_id)->where('type', 'red_card')->count();

            $team1_season->yellow_card -= $match->match_action->where('team_id', $match->team1_id)->where('type', 'yellow_card')->count();
            $team2_season->yellow_card -= $match->match_action->where('team_id', $match->team2_id)->where('type', 'yellow_card')->count();



            //dump($match);

            //dump('team1_season: ');
            //dump($team1_season);
            
            //dump('team2_season: ');
            //dump($team2_season);
            
            
            $team1_season->save();
            $team2_season->save();
            /***** teamseasons_update_by_match *******/

            $match->save();

        }

    }

    public function players_history_update($match, $match_completed_status)
    {
        //Maç tamamlandı durumuna alındığında oyuncuların tüm sezon istatistikleri
        //Maç silme işleminde eğer tamamlanan bir mç ise hesaplama tekrar yapılır
        //Maç güncellemesinde tamamlandı moduna alınan durumlarda yeniden hesaplama yapılır
        //dump($match);
        $season_id = $match->season_id;
        $match_id = $match->id;
        
        $statisticsArr = array();
        //Maçın oyuncularının player id lerini al, misafirleri dışla
        $all_player_IDs = array();
        
        foreach ($match->match_player_is_not_guest as $mplayer) {

            $sArr = array();
            $sArr['player_id'] = $mplayer->player_id;
            $sArr['team_id'] = $mplayer->team_id;
            $sArr['season_id'] = $season_id;

            //rate-sil $sArr['rate_total'] = 0;

            $sArr['gk_save'] = 0;
            //rate-sil $sArr['rating'] = 0;
            $sArr['match'] = 0;
            $sArr['goal'] = 0;
            $sArr['yellow_card'] = 0;
            $sArr['red_card'] = 0;
            $sArr['saving'] = 0;

            $statisticsArr[] = collect($sArr);
            $all_player_IDs[] = $mplayer->player;
        }

        //dump($statisticsArr);
        //dump($all_player_IDs);
        
        foreach ($all_player_IDs as $player) {
            //dump($player->id.'----------------');
            $player->match_player = $player->match_player->filter(function ($value) use ($season_id, $player) {
                return $value->match->season_id == $season_id && $value->team_id == $player->team_id && $value->match->completed == true;
            });
        }
        //dump('----');
        //dump($all_player_IDs);
        $statistics = collect($statisticsArr);
        //dump('******');
        //dump($statistics);
        foreach ($all_player_IDs as $player) {
            $pstat = $statistics->where('player_id', $player->id)->first();
            foreach ($player->match_player as $mplayer) {

                //rate-sil $pstat['rate_total'] = $pstat['rate_total'] + $mplayer->rating;
                if($mplayer->position_id == 1){
                    $pstat['gk_save'] = $pstat['gk_save'] + $match->match_action->where('team_id', $mplayer->team_id)->where('type', 'saving')->count();
                }
                $pstat['match'] = $pstat['match'] + 1;
                // BURAYA EKLE
                if(is_null($match->completed_status)){
                    foreach ($mplayer->match_action as $maction) {
                        if($maction->type != 'critical' && $maction->type != 'saving'){
                            $pstat[$maction->type] = $pstat[$maction->type] + 1;
                        }
                    }
                }
            }
            /*rate-sil 
            if($pstat['rate_total'] == 0){
                $pstat['rating'] = 0;
            }else{
                $pstat['rating'] = number_format((float)($pstat['rate_total'] / $pstat['match']), 2, '.', '');
            }
            */
            //dump($pstat);
        }


        foreach ($all_player_IDs as $player) {
            $tpth = $player->team_playerteamhistory->where('season_id', $season_id)->where('team_id', $player->team_id)->first();
            $stats = $statistics->where('season_id', $season_id)->where('team_id', $player->team_id)->where('player_id', $player->id)->first();
            
            if(empty($tpth)){
                $tpth = new TeamPlayerTeamHistory();
                $tpth->season_id = $season_id;
                $tpth->team_id = $player->team_id;
                $tpth->player_id = $player->id;
            }

            if(!empty($stats)){
                $tpth->gk_save = $stats['gk_save'];
                $tpth->rating = 0;
                $tpth->match = $stats['match'];
                $tpth->goal = $stats['goal'];
                $tpth->yellow_card = $stats['yellow_card'];
                $tpth->red_card = $stats['red_card'];
                $tpth->save();
            }
        }

        //dump('İlgili Player ID leri');
        //dump($statistics);

    }

    public function players_deger_update($match, $match_completed_status)
    {

        // dump($match);
        // dump($match_season->id);
        // AÇÇ echo 'Ayın MVPsi, Oyuncu Ayın Centilmeni/Mevki/Golü, Sezonun MVPsi, Oyuncu Sezonun Centilmeni/Mevki/Golü bilgileri veritabanında mevcut değil. Bu sebeple hesaplama dışında tutulmuştur.';
        
        $match_season = $match->season;
        $league_provinces = $match->season->league->league_province;

        $pointTotalPlayer = 100000;

        $pointMatch = 5000;
        $pointMatchRating = 3000;
        $pointMatchSave = 2000;

        $pointGoal = 3000;

        $pointYellowCard = -5000;
        $pointRedCard = -15000;

        $panoramaPoint = array(1 => array(8000, 'Maçın Gümüş Oyuncusu'), 2 => array(5000, 'Maçın Golü'), 3 => array(15000, 'Maçın Altın Oyuncusu'), 4 => array(5000, 'Maçın Dinamik Oyuncusu'), 5 => array(5000, 'Maçın Defansı'), 6 => array(5000, 'Maçın Orta Sahası'), 7 => array(5000, 'Maçın Forveti'), 8 => array(5000, 'Maçın Kalecisi'));

        $seasonCross = array(4, 3.5, 2.5, 1.5);
        $seasonCrossIndex = 0;

        $seasonTRCross = array(10, 3);
        $seasonTRCrossIndex = 0;

        $seasonList = array();
        $seasonTRList = array();
        
        foreach ($league_provinces as $league_province) {

            if($league_province->league->league_province->count() == 1){
                
                // AÇÇ dump($league_province->league->name);
                $seasons = $league_province->league->seasonApi;
                foreach ($seasons as $season) {

                    if($season->polymorphic_ctype_id == 32){ $seasonType = 'elimination'; }else if($season->polymorphic_ctype_id == 33){ $seasonType = 'fixture'; }else if($season->polymorphic_ctype_id == 34){ $seasonType = 'point'; }
                    ////ac dump('('.$seasonCross[$seasonCrossIndex].') (id:'.$season->id.') - '.$season->year.' - '.$season->name.' - '.$seasonType);
                    $seasonList[] = array($seasonCross[$seasonCrossIndex], $season->id, $seasonType, ($season->year.' - '.$season->name), $season->year);

                    if(!empty($season->league_eliminationseasonApi)){

                        if($season->league_eliminationseasonApi->season_ptr->polymorphic_ctype_id == 32){ $seasonRType = 'elimination'; }else if($season->league_eliminationseasonApi->season_ptr->polymorphic_ctype_id == 33){ $seasonRType = 'fixture'; }else if($season->league_eliminationseasonApi->season_ptr->polymorphic_ctype_id == 34){ $seasonRType = 'point'; }

                        ////ac dump('('.$seasonCross[$seasonCrossIndex].') (id:'.$season->league_eliminationseasonApi->season_ptr->id.') - '.' ---- '.$season->league_eliminationseasonApi->season_ptr->year.' - '.$season->league_eliminationseasonApi->season_ptr->name.' - '.$seasonRType);

                        $seasonList[] = array($seasonCross[$seasonCrossIndex], $season->league_eliminationseasonApi->season_ptr->id, $seasonRType, ($season->league_eliminationseasonApi->season_ptr->year.' - '.$season->league_eliminationseasonApi->season_ptr->name), $season->league_eliminationseasonApi->season_ptr->year );

                    }

                    if(count($seasonCross)-1 > $seasonCrossIndex){
                        $seasonCrossIndex++;
                    }
                }
                
                ////ac dump('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'); 
                
                $all_seasons = $league_province->league->season->where('active', true);
                foreach ($all_seasons as $season) {

                    if($season->polymorphic_ctype_id == 32){ $seasonEType = 'elimination'; }else if($season->polymorphic_ctype_id == 33){ $seasonEType = 'fixture'; }else if($season->polymorphic_ctype_id == 34){ $seasonEType = 'point'; }
                    $is_valid = array_search($season->id, array_column($seasonList, 1));
                    ////ac dump('---('.end($seasonCross).') (id:'.$season->id.') - '.$season->year.' - '.$season->name.' - '.$seasonEType.' - '.$is_valid.' - '.( ($is_valid !== false) ? $is_valid : 'false') );
                    
                    if($is_valid === false){
                        $seasonList[] = array(end($seasonCross), $season->id, $seasonEType, ($season->year.' - '.$season->name), $season->year);
                    }
                }
                
                // AÇÇ dump($seasonList);
                

            }else if($league_province->league->league_province->count() > 1){
                // AÇÇ dump('???? '.$league_province->league->name);

                $seasonsTR = $league_province->league->seasonApi;
                foreach ($seasonsTR as $seasonTR) {

                    if($seasonTR->polymorphic_ctype_id == 32){ $seasonType = 'elimination'; }else if($seasonTR->polymorphic_ctype_id == 33){ $seasonType = 'fixture'; }else if($seasonTR->polymorphic_ctype_id == 34){ $seasonType = 'point'; }
                    ////ac dump('('.$seasonTRCross[$seasonTRCrossIndex].') (id:'.$seasonTR->id.') - '.$seasonTR->year.' - '.$seasonTR->name.' - '.$seasonType);
                    $seasonTRList[] = array($seasonTRCross[$seasonTRCrossIndex], $seasonTR->id, $seasonType, ($seasonTR->year.' - '.$seasonTR->name), $seasonTR->year);

                    if(!empty($seasonTR->league_eliminationseasonApi)){

                        if($seasonTR->league_eliminationseasonApi->season_ptr->polymorphic_ctype_id == 32){ $seasonRType = 'elimination'; }else if($seasonTR->league_eliminationseasonApi->season_ptr->polymorphic_ctype_id == 33){ $seasonRType = 'fixture'; }else if($seasonTR->league_eliminationseasonApi->season_ptr->polymorphic_ctype_id == 34){ $seasonRType = 'point'; }

                        ////ac dump('('.$seasonTRCross[$seasonTRCrossIndex].') (id:'.$seasonTR->league_eliminationseasonApi->season_ptr->id.') - '.' ---- '.$seasonTR->league_eliminationseasonApi->season_ptr->year.' - '.$seasonTR->league_eliminationseasonApi->season_ptr->name.' - '.$seasonRType);

                        $seasonTRList[] = array($seasonTRCross[$seasonTRCrossIndex], $seasonTR->league_eliminationseasonApi->season_ptr->id, $seasonRType, ($seasonTR->league_eliminationseasonApi->season_ptr->year.' - '.$seasonTR->league_eliminationseasonApi->season_ptr->name), $seasonTR->league_eliminationseasonApi->season_ptr->year );

                    }

                    if(count($seasonTRCross)-1 > $seasonTRCrossIndex){
                        $seasonTRCrossIndex++;
                    }
                }


                $all_seasonsTR = $league_province->league->season->where('active', true);
                foreach ($all_seasonsTR as $seasonTR) {

                    if($seasonTR->polymorphic_ctype_id == 32){ $seasonEType = 'elimination'; }else if($seasonTR->polymorphic_ctype_id == 33){ $seasonEType = 'fixture'; }else if($seasonTR->polymorphic_ctype_id == 34){ $seasonEType = 'point'; }
                    $is_valid = array_search($seasonTR->id, array_column($seasonTRList, 1));
                    ////ac dump('---('.end($seasonTRCross).') (id:'.$seasonTR->id.') - '.$seasonTR->year.' - '.$seasonTR->name.' - '.$seasonEType.' - '.$is_valid.' - '.( ($is_valid !== false) ? $is_valid : 'false') );
                    
                    if($is_valid === false){
                        $seasonTRList[] = array(end($seasonTRCross), $seasonTR->id, $seasonEType, ($seasonTR->year.' - '.$seasonTR->name), $seasonTR->year);
                    }
                }
                
                // AÇÇ dump($seasonTRList);

            }else{
                // AÇÇ dump('sıkıntı olmasın !!!');                
                // AÇÇ dump('???? '.$league_province->league->name);
            }
            // AÇÇ dump('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'); 
        }

        if(count($seasonList) > 0){
            foreach ($match->match_player_is_not_guest as $match_player) {

                if(!empty($match_season)){

                    $matchTotalPointArray = array();
                    $is_valid = array_search($match_season->id, array_column($seasonList, 1));
                    
                    $panoramaTotalPoint = 0;
                    foreach ($match_player->match_player_panorama as $panorama) {
                        $panoramaTotalPoint += $panoramaPoint[$panorama->panorama_id][0];
                    }
                    /*
                    if(is_null($match_player->rating)){
                        $ratingPoint = 0;
                    }else{
                        if($match_player->rating >= 6){ //(Rating 6 dan küçükse hesaplama dışı tutulur.)
                            $ratingPoint = ($match_player->rating - 5) * $pointMatchRating;
                        }else{
                            $ratingPoint = 0;
                        }
                    }
                    */
                    $matchPoint = $seasonList[$is_valid][0] * (
                        $pointMatch + 
                        // $ratingPoint + 
                        ($match_player->match_action_goal->count() * $pointGoal) + 
                        (((is_null($match_player->saving) ? 0 : $match_player->saving)) * $pointMatchSave) + 
                        ($match_player->match_action_yellow_card->count() * $pointYellowCard) + 
                        ($match_player->match_action_red_card->count() * $pointRedCard) +
                        $panoramaTotalPoint
                    );

                    $matchTotalPointArray = array('match_id' => $match_player->match_id, 'point' => $matchPoint, 'match_player' => $match_player->id, 'player' => $match_player->player->id);

                    $player_val = Player::find($match_player->player->id);
                    if($match_completed_status == 'completed:true'){
                        $player_val->value += $matchPoint;
                    }else if($match_completed_status == 'completed:false'){
                        $player_val->value -= $matchPoint;
                    }
                    $player_val->save();
                }
            }
        }

        if(count($seasonTRList) > 0){
            foreach ($match->match_player_is_not_guest as $match_player) {

                if(!empty($match_season)){
                    
                    $matchTotalPointArray = array();
                    $is_valid = array_search($match_season->id, array_column($seasonTRList, 1));
                    
                    $playerHistoryTRPoint = 0;
                    foreach ($match_player->match_player_panorama as $panorama) {
                        $playerHistoryTRPoint += $panoramaPoint[$panorama->panorama_id][0];
                    }
                    /*
                    if(is_null($match_player->rating)){
                        $ratingPoint = 0;
                    }else{
                        if($match_player->rating >= 6){ //(Rating 6 dan küçükse hesaplama dışı tutulur.)
                            $ratingPoint = ($match_player->rating - 5) * $pointMatchRating;
                        }else{
                            $ratingPoint = 0;
                        }
                    }
                    */
                    $matchPoint = $seasonTRList[$is_valid][0] * (
                        $pointMatch + 
                        // $ratingPoint + 
                        ($match_player->match_action_goal->count() * $pointGoal) + 
                        (((is_null($match_player->saving) ? 0 : $match_player->saving)) * $pointMatchSave) + 
                        ($match_player->match_action_yellow_card->count() * $pointYellowCard) + 
                        ($match_player->match_action_red_card->count() * $pointRedCard) +
                        $playerHistoryTRPoint
                    );

                    $matchTotalPointArray = array('match_id' => $match_player->match_id, 'point' => $matchPoint, 'match_player' => $match_player->id, 'player' => $match_player->player->id);
                    
                    $player_val = Player::find($match_player->player->id);
                    if($match_completed_status == 'completed:true'){
                        $player_val->value += $matchPoint;
                    }else if($match_completed_status == 'completed:false'){
                        $player_val->value -= $matchPoint;
                    }
                    $player_val->save();
                }
            }
        }

        return true;
    }
    

    /*********************/
    /*********************/
    /*********************/
    /*********************/

    public function match_video_save(Request $request)
    {

        //dd($request->input());
        $text = "";
        if ($request->crud == 'delete') {

        }else{

            $model = MatchVideo::where('match_id', $request->id)->first();

            /* ADD */
            if (is_null($model)) {
                $model = new MatchVideo();
                $model->match_id = $request->id;
            }

            $rules = array();
            $formConfig = config('forms.match_video');

            if(!is_null($formConfig)){
                foreach ($formConfig as $key => $value){
                    if(!empty(data_get($value, 'validation'))){
                        $rules[$key] = data_get($value, 'validation');
                    }
                }
            }
            
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
               return \Redirect::back()->withErrors($validator)->withInput();
            }

            $model->status = $request->status;
            $model->live_source = $request->live_source;
            $model->live_embed_code = $request->live_embed_code;
            $model->full_source = $request->full_source;
            $model->full_embed_code = $request->full_embed_code;
            $model->summary_source = $request->summary_source;
            $model->summary_embed_code = $request->summary_embed_code;

            $model->save();

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $text = 'Başarıyla Eklendi...';
            }else if($request->crud == 'edit'){
                $text = 'Başarıyla Güncellendi...';
            }
        }

        return redirect($request->segment.'/'.$request->crud.'/'.$request->id.'?t=tab_02')->with('message', array('text' => $text, 'status' => 'success'));
    }

    public function match_player_save(Request $request)
    {

        $text = "";
        if ($request->crud == 'delete') {

        }else{
            
            if(empty($request->match_player_id)){
                /* ADD */
                if($request->player_id == 'guest1' || $request->player_id == 'guest2'){
                    $model = null;
                }else{
                    $model = MatchPlayer::where('match_id', $request->id)->where('player_id', $request->player_id)->first();
                }

                if (is_null($model)) {
                    $model = new MatchPlayer();
                    $model->match_id = $request->id;
                }
                
            }else{
                /* EDIT */
                $model = MatchPlayer::find($request->match_player_id);
            }

            $rules = array();
            $formConfig = config('forms.match_player');

            if(!is_null($formConfig)){
                foreach ($formConfig as $key => $value){
                    if(!empty(data_get($value, 'validation'))){
                        $rules[$key] = data_get($value, 'validation');
                    }
                }
            }
            
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
               return \Redirect::back()->withErrors($validator)->withInput();
            }

            if(empty($request->match_player_id)){
                if(!empty($request->player_id)){
                    if($request->player_id == 'guest1'){
                        $_match = Match::find($request->id);
                        $team_id = $_match->team1->id;
                        $model->guest_name = $request->guest_name;
                    }elseif($request->player_id == 'guest2'){
                        $_match = Match::find($request->id);
                        $team_id = $_match->team2->id;
                        $model->guest_name = $request->guest_name;
                    }else{
                        $player = Player::find($request->player_id);
                        $team_id = $player->team_id;
                        $model->player_id = $player->id;
                    }
                }
            }else{
                /* EDIT */
                $model->guest_name = $request->guest_name;
                $team_id = $model->team_id;
            }

            $model->rating = $request->rating;
            $model->saving = $request->saving;
            $model->match_id = $request->match_id;
            $model->position_id = $request->position_id;
            $model->team_id = $team_id;
            $model->save();

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $text = 'Başarıyla Eklendi...';
            }else if($request->crud == 'edit'){
                $text = 'Başarıyla Güncellendi...';
            }
        }

        return redirect($request->segment.'/'.$request->crud.'/'.$request->id.'?t=tab_03')->with('message', array('text' => $text, 'status' => 'success'));
    }

    public function match_player_delete($id, $mpid)
    {

        $model = MatchPlayer::find($mpid);
        if(!empty($model)){

            if($model->match_action->count() > 0){
                $text = 'Bu oyuncuya ait maç aksiyonları var. Bu sebeple silinemedi.';
                $succ = 'error';
            }else{
                $model->delete();
                $text = 'Başarıyla Silindi...';
                $succ = 'success';
            }

            return redirect('matches/edit/'.$id.'?t=tab_03')->with('message', array('text' => $text, 'status' => $succ));
        }
    }

    public function match_action_save(Request $request)
    {
        //dd($request->input());
        $text = "";
        if ($request->crud == 'delete') {

        }else{
            
            if(empty($request->match_action_id)){
                /* ADD */
                $model = new MatchAction();
                $model->match_id = $request->match_id;
            }else{
                /* EDIT */
                $model = MatchAction::find($request->match_action_id);
            }

            $rules = array();
            $formConfig = config('forms.match_action');

            if(!is_null($formConfig)){
                foreach ($formConfig as $key => $value){
                    if(!empty(data_get($value, 'validation'))){
                        $rules[$key] = data_get($value, 'validation');
                    }
                }
            }
            
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                dd($validator);
               return \Redirect::back()->withErrors($validator)->withInput();
            }

            $mplayer = MatchPlayer::find($request->match_player_id);

            $model->embed_source = $request->embed_source;
            $model->embed_code = $request->embed_code;
            $model->type = $request->type;
            $model->minute = $request->minute;
            $model->match_player_id = $request->match_player_id;
            $model->team_id = $mplayer->team_id;
            $model->save();

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $text = 'Başarıyla Eklendi...';
            }else if($request->crud == 'edit'){
                $text = 'Başarıyla Güncellendi...';
            }
        }

        return redirect($request->segment.'/'.$request->crud.'/'.$request->id.'?t=tab_04')->with('message', array('text' => $text, 'status' => 'success'));
    }

    public function match_action_delete($id, $maid)
    {
        //dd($id.' - '.$maid);
        $model = MatchAction::find($maid);
        if(!empty($model)){
            $model->delete();
            $text = 'Başarıyla Silindi...';
            
            return redirect('matches/edit/'.$id.'?t=tab_04')->with('message', array('text' => $text, 'status' => 'success'));
        }
    }

    public function match_panorama_save(Request $request)
    {

        $text = "";
        if ($request->crud == 'delete') {

        }else{
            
            if(empty($request->match_panorama_id)){
                /* ADD */
                $model = new MatchPanorama();
                $model->match_id = $request->match_id;
                $mpCount = MatchPanorama::where('match_id', $request->match_id)->where('panorama_id', $request->panorama_id)->count(); 
            }else{
                /* EDIT */
                $model = MatchPanorama::find($request->match_panorama_id);
                $mpCount = MatchPanorama::where('match_id', $request->match_id)->where('panorama_id', $request->panorama_id)->where('id', '!=', $model->id)->count();
            }

            if($mpCount > 0){
                $text = 'Aynı panorama türünden sadece 1 tane ekleyebilirsiniz.';
                return redirect($request->segment.'/'.$request->crud.'/'.$request->id.'?t=tab_05')->with('message', array('text' => $text, 'status' => 'error'));
            }

            $rules = array();
            $formConfig = config('forms.match_panorama');

            if(!is_null($formConfig)){
                foreach ($formConfig as $key => $value){
                    if(!empty(data_get($value, 'validation'))){
                        $rules[$key] = data_get($value, 'validation');
                    }
                }
            }
            
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
               return \Redirect::back()->withErrors($validator)->withInput();
            }

            $model->match_player_id = $request->match_player_id_mpa;
            $model->panorama_id = $request->panorama_id;
            $model->embed_source = $request->embed_source_mpa;
            $model->embed_code = $request->embed_code_mpa;
            $model->save();


            if($model->match->completed && !is_null(MatchPlayer::find($request->match_player_id_mpa)->player_id)){
                $match_season = $model->match->season;
                $league_provinces = $model->match->season->league->league_province;


                $panoramaPoint = array(1 => array(8000, 'Maçın Gümüş Oyuncusu'), 2 => array(5000, 'Maçın Golü'), 3 => array(15000, 'Maçın Altın Oyuncusu'), 4 => array(5000, 'Maçın Dinamik Oyuncusu'), 5 => array(5000, 'Maçın Defansı'), 6 => array(5000, 'Maçın Orta Sahası'), 7 => array(5000, 'Maçın Forveti'), 8 => array(5000, 'Maçın Kalecisi'));

                $seasonCross = array(4, 3.5, 2.5, 1.5);
                $seasonCrossIndex = 0;

                $seasonTRCross = array(10, 3);
                $seasonTRCrossIndex = 0;

                $seasonList = array();
                $seasonTRList = array();
                
                foreach ($league_provinces as $league_province) {

                    if($league_province->league->league_province->count() == 1){
                        
                        $seasons = $league_province->league->seasonApi;
                        foreach ($seasons as $season) {

                            if($season->polymorphic_ctype_id == 32){ $seasonType = 'elimination'; }else if($season->polymorphic_ctype_id == 33){ $seasonType = 'fixture'; }else if($season->polymorphic_ctype_id == 34){ $seasonType = 'point'; }
                            $seasonList[] = array($seasonCross[$seasonCrossIndex], $season->id, $seasonType, ($season->year.' - '.$season->name), $season->year);

                            if(!empty($season->league_eliminationseasonApi)){
                                if($season->league_eliminationseasonApi->season_ptr->polymorphic_ctype_id == 32){ $seasonRType = 'elimination'; }else if($season->league_eliminationseasonApi->season_ptr->polymorphic_ctype_id == 33){ $seasonRType = 'fixture'; }else if($season->league_eliminationseasonApi->season_ptr->polymorphic_ctype_id == 34){ $seasonRType = 'point'; }

                                $seasonList[] = array($seasonCross[$seasonCrossIndex], $season->league_eliminationseasonApi->season_ptr->id, $seasonRType, ($season->league_eliminationseasonApi->season_ptr->year.' - '.$season->league_eliminationseasonApi->season_ptr->name), $season->league_eliminationseasonApi->season_ptr->year );
                            }

                            if(count($seasonCross)-1 > $seasonCrossIndex){
                                $seasonCrossIndex++;
                            }
                        }
                        
                        $all_seasons = $league_province->league->season->where('active', true);
                        foreach ($all_seasons as $season) {

                            if($season->polymorphic_ctype_id == 32){ $seasonEType = 'elimination'; }else if($season->polymorphic_ctype_id == 33){ $seasonEType = 'fixture'; }else if($season->polymorphic_ctype_id == 34){ $seasonEType = 'point'; }
                            $is_valid = array_search($season->id, array_column($seasonList, 1));
                            
                            if($is_valid === false){
                                $seasonList[] = array(end($seasonCross), $season->id, $seasonEType, ($season->year.' - '.$season->name), $season->year);
                            }
                        }
                        

                    }else if($league_province->league->league_province->count() > 1){

                        $seasonsTR = $league_province->league->seasonApi;
                        foreach ($seasonsTR as $seasonTR) {

                            if($seasonTR->polymorphic_ctype_id == 32){ $seasonType = 'elimination'; }else if($seasonTR->polymorphic_ctype_id == 33){ $seasonType = 'fixture'; }else if($seasonTR->polymorphic_ctype_id == 34){ $seasonType = 'point'; }
                            $seasonTRList[] = array($seasonTRCross[$seasonTRCrossIndex], $seasonTR->id, $seasonType, ($seasonTR->year.' - '.$seasonTR->name), $seasonTR->year);

                            if(!empty($seasonTR->league_eliminationseasonApi)){
                                if($seasonTR->league_eliminationseasonApi->season_ptr->polymorphic_ctype_id == 32){ $seasonRType = 'elimination'; }else if($seasonTR->league_eliminationseasonApi->season_ptr->polymorphic_ctype_id == 33){ $seasonRType = 'fixture'; }else if($seasonTR->league_eliminationseasonApi->season_ptr->polymorphic_ctype_id == 34){ $seasonRType = 'point'; }

                                $seasonTRList[] = array($seasonTRCross[$seasonTRCrossIndex], $seasonTR->league_eliminationseasonApi->season_ptr->id, $seasonRType, ($seasonTR->league_eliminationseasonApi->season_ptr->year.' - '.$seasonTR->league_eliminationseasonApi->season_ptr->name), $seasonTR->league_eliminationseasonApi->season_ptr->year );
                            }

                            if(count($seasonTRCross)-1 > $seasonTRCrossIndex){
                                $seasonTRCrossIndex++;
                            }
                        }


                        $all_seasonsTR = $league_province->league->season->where('active', true);
                        foreach ($all_seasonsTR as $seasonTR) {

                            if($seasonTR->polymorphic_ctype_id == 32){ $seasonEType = 'elimination'; }else if($seasonTR->polymorphic_ctype_id == 33){ $seasonEType = 'fixture'; }else if($seasonTR->polymorphic_ctype_id == 34){ $seasonEType = 'point'; }
                            $is_valid = array_search($seasonTR->id, array_column($seasonTRList, 1));
                            
                            if($is_valid === false){
                                $seasonTRList[] = array(end($seasonTRCross), $seasonTR->id, $seasonEType, ($seasonTR->year.' - '.$seasonTR->name), $seasonTR->year);
                            }
                        }

                    }else{
                        // AÇÇ dump('sıkıntı olmasın !!!');                
                        // AÇÇ dump('???? '.$league_province->league->name);
                    }

                }

                if(count($seasonList) > 0){

                    $match_player = MatchPlayer::find($model->match_player_id);
                    if(!empty($match_season)){

                        $is_valid = array_search($match_season->id, array_column($seasonList, 1));
                        
                        $panoramaTotalPoint = 0;
                        foreach ($match_player->match_player_panorama as $panorama) {
                            $panoramaTotalPoint += $panoramaPoint[$panorama->panorama_id][0];
                        }

                        $matchPoint = $seasonList[$is_valid][0] * (
                            $panoramaTotalPoint
                        );

                        $player_val = Player::find($match_player->player_id);
                        $player_val->value += $matchPoint;
                        $player_val->save();
                    }

                }

                if(count($seasonTRList) > 0){

                    $match_player = MatchPlayer::find($model->match_player_id);
                    if(!empty($match_season)){
                        
                        //$matchTotalPointArray = array();
                        $is_valid = array_search($match_season->id, array_column($seasonTRList, 1));
                        
                        $playerHistoryTRPoint = 0;
                        foreach ($match_player->match_player_panorama as $panorama) {
                            $playerHistoryTRPoint += $panoramaPoint[$panorama->panorama_id][0];
                        }
                        
                        $matchPoint = $seasonTRList[$is_valid][0] * (
                            $playerHistoryTRPoint
                        );

                        //$matchTotalPointArray = array('match_id' => $match_player->match_id, 'point' => $matchPoint, 'match_player' => $match_player->id, 'player' => $match_player->player->id);
                        
                        $player_val = Player::find($match_player->player_id);
                        $player_val->value += $matchPoint;
                        $player_val->save();
                    }
                    
                }
            }





            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $text = 'Başarıyla Eklendi...';
            }else if($request->crud == 'edit'){
                $text = 'Başarıyla Güncellendi...';
            }
        }

        return redirect($request->segment.'/'.$request->crud.'/'.$request->id.'?t=tab_05')->with('message', array('text' => $text, 'status' => 'success'));
    }

    public function match_panorama_delete($id, $mpaid)
    {
        
        $model = MatchPanorama::find($mpaid);

        if(!empty($model)){

            if($model->match->completed && !is_null($model->match_player_id)){
                $match_season = $model->match->season;
                $league_provinces = $model->match->season->league->league_province;


                $panoramaPoint = array(1 => array(8000, 'Maçın Gümüş Oyuncusu'), 2 => array(5000, 'Maçın Golü'), 3 => array(15000, 'Maçın Altın Oyuncusu'), 4 => array(5000, 'Maçın Dinamik Oyuncusu'), 5 => array(5000, 'Maçın Defansı'), 6 => array(5000, 'Maçın Orta Sahası'), 7 => array(5000, 'Maçın Forveti'), 8 => array(5000, 'Maçın Kalecisi'));

                $seasonCross = array(4, 3.5, 2.5, 1.5);
                $seasonCrossIndex = 0;

                $seasonTRCross = array(10, 3);
                $seasonTRCrossIndex = 0;

                $seasonList = array();
                $seasonTRList = array();
                
                foreach ($league_provinces as $league_province) {

                    if($league_province->league->league_province->count() == 1){
                        
                        $seasons = $league_province->league->seasonApi;
                        foreach ($seasons as $season) {

                            if($season->polymorphic_ctype_id == 32){ $seasonType = 'elimination'; }else if($season->polymorphic_ctype_id == 33){ $seasonType = 'fixture'; }else if($season->polymorphic_ctype_id == 34){ $seasonType = 'point'; }
                            $seasonList[] = array($seasonCross[$seasonCrossIndex], $season->id, $seasonType, ($season->year.' - '.$season->name), $season->year);

                            if(!empty($season->league_eliminationseasonApi)){
                                if($season->league_eliminationseasonApi->season_ptr->polymorphic_ctype_id == 32){ $seasonRType = 'elimination'; }else if($season->league_eliminationseasonApi->season_ptr->polymorphic_ctype_id == 33){ $seasonRType = 'fixture'; }else if($season->league_eliminationseasonApi->season_ptr->polymorphic_ctype_id == 34){ $seasonRType = 'point'; }

                                $seasonList[] = array($seasonCross[$seasonCrossIndex], $season->league_eliminationseasonApi->season_ptr->id, $seasonRType, ($season->league_eliminationseasonApi->season_ptr->year.' - '.$season->league_eliminationseasonApi->season_ptr->name), $season->league_eliminationseasonApi->season_ptr->year );
                            }

                            if(count($seasonCross)-1 > $seasonCrossIndex){
                                $seasonCrossIndex++;
                            }
                        }
                        
                        $all_seasons = $league_province->league->season->where('active', true);
                        foreach ($all_seasons as $season) {

                            if($season->polymorphic_ctype_id == 32){ $seasonEType = 'elimination'; }else if($season->polymorphic_ctype_id == 33){ $seasonEType = 'fixture'; }else if($season->polymorphic_ctype_id == 34){ $seasonEType = 'point'; }
                            $is_valid = array_search($season->id, array_column($seasonList, 1));
                            
                            if($is_valid === false){
                                $seasonList[] = array(end($seasonCross), $season->id, $seasonEType, ($season->year.' - '.$season->name), $season->year);
                            }
                        }
                        

                    }else if($league_province->league->league_province->count() > 1){

                        $seasonsTR = $league_province->league->seasonApi;
                        foreach ($seasonsTR as $seasonTR) {

                            if($seasonTR->polymorphic_ctype_id == 32){ $seasonType = 'elimination'; }else if($seasonTR->polymorphic_ctype_id == 33){ $seasonType = 'fixture'; }else if($seasonTR->polymorphic_ctype_id == 34){ $seasonType = 'point'; }
                            $seasonTRList[] = array($seasonTRCross[$seasonTRCrossIndex], $seasonTR->id, $seasonType, ($seasonTR->year.' - '.$seasonTR->name), $seasonTR->year);

                            if(!empty($seasonTR->league_eliminationseasonApi)){
                                if($seasonTR->league_eliminationseasonApi->season_ptr->polymorphic_ctype_id == 32){ $seasonRType = 'elimination'; }else if($seasonTR->league_eliminationseasonApi->season_ptr->polymorphic_ctype_id == 33){ $seasonRType = 'fixture'; }else if($seasonTR->league_eliminationseasonApi->season_ptr->polymorphic_ctype_id == 34){ $seasonRType = 'point'; }

                                $seasonTRList[] = array($seasonTRCross[$seasonTRCrossIndex], $seasonTR->league_eliminationseasonApi->season_ptr->id, $seasonRType, ($seasonTR->league_eliminationseasonApi->season_ptr->year.' - '.$seasonTR->league_eliminationseasonApi->season_ptr->name), $seasonTR->league_eliminationseasonApi->season_ptr->year );
                            }

                            if(count($seasonTRCross)-1 > $seasonTRCrossIndex){
                                $seasonTRCrossIndex++;
                            }
                        }


                        $all_seasonsTR = $league_province->league->season->where('active', true);
                        foreach ($all_seasonsTR as $seasonTR) {

                            if($seasonTR->polymorphic_ctype_id == 32){ $seasonEType = 'elimination'; }else if($seasonTR->polymorphic_ctype_id == 33){ $seasonEType = 'fixture'; }else if($seasonTR->polymorphic_ctype_id == 34){ $seasonEType = 'point'; }
                            $is_valid = array_search($seasonTR->id, array_column($seasonTRList, 1));
                            
                            if($is_valid === false){
                                $seasonTRList[] = array(end($seasonTRCross), $seasonTR->id, $seasonEType, ($seasonTR->year.' - '.$seasonTR->name), $seasonTR->year);
                            }
                        }

                    }else{
                        // AÇÇ dump('sıkıntı olmasın !!!');                
                        // AÇÇ dump('???? '.$league_province->league->name);
                    }

                }

                if(count($seasonList) > 0){

                    $match_player = $model->match_player_panorama;
                    if(!empty($match_season)){

                        $is_valid = array_search($match_season->id, array_column($seasonList, 1));
                        
                        $panoramaTotalPoint = 0;
                        foreach ($match_player->match_player_panorama as $panorama) {
                            $panoramaTotalPoint += $panoramaPoint[$panorama->panorama_id][0];
                        }

                        $matchPoint = $seasonList[$is_valid][0] * (
                            $panoramaTotalPoint
                        );

                        $player_val = Player::find($match_player->player_id);
                        $player_val->value -= $matchPoint;
                        $player_val->save();
                    }

                }

                if(count($seasonTRList) > 0){

                    $match_player = $model->match_player_panorama;
                    if(!empty($match_season)){
                        
                        $is_valid = array_search($match_season->id, array_column($seasonTRList, 1));
                        
                        $playerHistoryTRPoint = 0;
                        foreach ($match_player->match_player_panorama as $panorama) {
                            $playerHistoryTRPoint += $panoramaPoint[$panorama->panorama_id][0];
                        }
                        
                        $matchPoint = $seasonTRList[$is_valid][0] * (
                            $playerHistoryTRPoint
                        );
                        
                        $player_val = Player::find($match_player->player_id);
                        $player_val->value -= $matchPoint;
                        $player_val->save();
                    }
                    
                }
            }


            $model->delete();
            $text = 'Başarıyla Silindi...';
            
            return redirect('matches/edit/'.$id.'?t=tab_05')->with('message', array('text' => $text, 'status' => 'success'));
        }
    }
    
    public function matchImageDescSave(Request $request)
    {

        if(!empty($request->input('match_image'))){
            foreach ($request->input('match_image') as $key => $value) {
                $match_image = MatchImage::find($key);
                $match_image->description = $value;
                $match_image->save();
            }
        }
        
        $text = 'Başarıyla Kaydedildi...';
        return redirect('matches/edit/'.$request->input('match_id').'?t=tab_07')->with('message', array('text' => $text, 'status' => 'success'));
    }

    public function matchImageDelete($id, $miid)
    {

        $model = MatchImage::find($miid);
        if(!empty($model)){
            $model->delete();
            $text = 'Başarıyla Silindi...';
            
            return redirect('matches/edit/'.$id.'?t=tab_07')->with('message', array('text' => $text, 'status' => 'success'));
        }
    }

    public function match_press_save(Request $request)
    {

        //dump($request->input());
        $text = "";
        if ($request->crud == 'delete') {

            /*
                $model = MatchVideo::find($request->id);
                //$model->team_survey->delete();
                $model->delete();
                
                $text = 'Başarıyla Silindi...';
            */

        }else{

            $model = PressConferences::where('match_id', $request->id)->first();

            /* ADD */
            if (is_null($model)) {
                $model = new PressConferences();
                $model->match_id = $request->id;
            }

            $rules = array();
            $formConfig = config('forms.match_press');

            if(!is_null($formConfig)){
                foreach ($formConfig as $key => $value){
                    if(!empty(data_get($value, 'validation'))){
                        $rules[$key] = data_get($value, 'validation');
                    }
                }
            }
            
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
               return \Redirect::back()->withErrors($validator)->withInput();
            }

            $model->embed_source = $request->embed_source_mp;
            $model->embed_code = $request->embed_code;
            $model->team_id = $request->team_id_mp;
            $model->save();

            /*
            embed_source_mp
            embed_code_mp
            team_id_mp
            player_id_mp
            */

             /* EDIT */
            //dd($model->press_conferences_players);
            
            foreach ($model->press_conferences_players as $value_pcp) {
                $value_pcp->delete();
            }
            

            if(!empty($request->players_id_mp)){
                foreach ($request->players_id_mp as $value_pl) {
                    $pcp = new PressConferencesPlayers();
                    $pcp->pressconference_id = $model->id;
                    $pcp->matchplayer_id = $value_pl;
                    $pcp->save();
                }
            }

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $text = 'Başarıyla Eklendi...';
            }else if($request->crud == 'edit'){
                $text = 'Başarıyla Güncellendi...';
            }
        }

        return redirect($request->segment.'/'.$request->crud.'/'.$request->id.'?t=tab_06')->with('message', array('text' => $text, 'status' => 'success'));
    }

}
