<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Validator;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use Carbon\Carbon;
use App\User;
use App\Profile;
use App\Player;
use App\Team;
use App\TeamTeamSeason;
use App\TeamTeamPlayer;
use App\Transfer;
use App\TransferSeason;

use App\UserRoleHasProvince;

class UserController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        if(request()->segment(1) == 'users' && !Auth::user()->hasPermissionTo('view_user')){
            return redirect('/dashboard')->with('message', array('text' => "Bu sayfaya ulaşmak için yetkiniz bulunmuyor!", 'status' => 'error'));
        }
    	return view('dashboard');
    }

    public function crud($id = null) {
        if(
            (request()->segment(2) == 'edit' && !Auth::user()->hasPermissionTo('change_user')) || 
            (request()->segment(2) == 'delete' && !Auth::user()->hasPermissionTo('delete_user'))
        ) {
            return redirect('/roles')->with('message', array('text' => "Bu sayfaya ulaşmak için yetkiniz bulunmuyor!", 'status' => 'error'));
        }

        $user = NULL;
        if(!is_null($id)) {
            $user = User::find($id);
        }
        return view('dashboard', array('model_data' => $user));
    }

    public function save(Request $request) {
        $text = "";
        if ($request->crud == 'delete'){

            $model = User::find($request->id);
            $model->is_active = false;
            $model->save();
         
            $text = 'Başarıyla Silindi...';

        }else{

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $model = new User();
            }else if($request->crud == 'edit'){
                $model = User::find($request->id);
            }

            $rules = array();
            $formConfig = config('forms.'.$request->segment);

            if(!is_null($formConfig)){
                foreach ($formConfig as $key => $value){
                    if(!empty(data_get($value, 'validation'))){
                        $rules[$key] = data_get($value, 'validation');
                    }
                }
            }
            
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                return \Redirect::back()->withErrors($validator)->withInput();
            }

            $model->username = $request->email;
            $model->first_name = $request->first_name;
            $model->last_name = $request->last_name;
            $model->email = $request->email;
            $model->phone = $request->phone;
            $model->save();

            $profile = $model->profile;
            $profile->identity_number = $request->identity_number;
            $profile->birth_date = Carbon::parse($request->birth_date)->format('Y-m-d');
            $profile->province_id = $request->province;
            $profile->district_id = $request->district_id;
            $profile->save();
        
            $player = $model->player;

            if (!empty($player)) {
                $player->facebook = $request->facebook;
                $player->twitter = $request->twitter;
                $player->instagram = $request->instagram;
                $player->number = $request->number;
                $player->foot = $request->foot;
                $player->weight = $request->weight;
                $player->height = $request->height;
                $player->position_id = $request->position_id;
                $player->position2_id = $request->position2_id;
                $player->position3_id = $request->position3_id;
                $player->save();
            }

            if($model->getRoleNames()->count() > 0){
                foreach ($model->getRoleNames() as $role_name) {
                    $model->removeRole($role_name);
                }
                foreach ($model->roleprovince as $role_province) {
                    $role_province->delete();
                }
            }
            if(!empty($request->role)){
                $role = Role::find($request->role);
                $model->assignRole($role->name);

                if(!empty($request->role_province)){
                    foreach ($request->role_province as $role_province) {
                        $roleProvince = new UserRoleHasProvince();
                        $roleProvince->user_id = $model->id;
                        $roleProvince->province_id = $role_province;
                        $roleProvince->save();
                    }
                }
            }

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $text = 'Başarıyla Eklendi...';
            }else if($request->crud == 'edit'){
                $text = 'Başarıyla Güncellendi...';
            }
        }

        return redirect($request->segment)->with('message', array('text' => $text, 'status' => 'success'));
    }

    public function addoutsideplayer_crud($id = null) {
        if(
            (request()->segment(2) == 'edit' && !Auth::user()->hasPermissionTo('add_user')) || 
            (request()->segment(2) == 'edit' && !Auth::user()->hasPermissionTo('change_user')) || 
            (request()->segment(2) == 'delete' && !Auth::user()->hasPermissionTo('delete_user'))
        ) {
            return redirect('/roles')->with('message', array('text' => "Bu sayfaya ulaşmak için yetkiniz bulunmuyor!", 'status' => 'error'));
        }

        $user = NULL;
        return view('dashboard', array('model_data' => $user));
    }

    public function addoutsideplayer_save(Request $request) {
        $text = "";

        $isPlayer = true;
        $emailCount = User::where('username', $request->input('email'))->orWhere('email', $request->input('email'))->count();
        $phoneCount = User::where('phone', $request->input('phone'))->count();

        if($request->input('identity_number') != null){
            $identityNumberCount = Profile::where('identity_number', $request->input('identity_number'))->count();
        }else{
            $identityNumberCount = 0;
        }


        $returnMessage = null;

        if($emailCount > 0){
            $returnMessage .= 'Bu eposta adresi alanına sahip kullanıcı zaten mevcut. ';
        }

        if($phoneCount > 0){
            $returnMessage .= 'Bu telefon alanına sahip kullanıcı zaten mevcut. ';
        }
        
        if($identityNumberCount > 0){
            $returnMessage .= 'Bu TC Kimlik alanına sahip kullanıcı zaten mevcut. ';
        }

        if (!empty($returnMessage)) {

            return \Redirect::back()->with('message', array('text' => $returnMessage, 'status' => 'error'))->withInput($request->input());
        }

        $rules = array();
        $formConfig = config('forms.'.$request->segment);

        if(!is_null($formConfig)){
            foreach ($formConfig as $key => $value){
                if(!empty(data_get($value, 'validation'))){
                    $rules[$key] = data_get($value, 'validation');
                }
            }
        }
        
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return \Redirect::back()->withErrors($validator)->withInput($request->input());
        }

        

        $user = new User();

        $user->username = $request->email;
        $user->email = $request->email;

        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->is_staff = false;
        $user->is_superuser = false;

        $user->is_active = false; // SMS ONAYLANDIĞINDA TRUE OLACAK;
        $user->infoshare = false;

        //$user->last_login = Carbon::now();
        $user->date_joined = Carbon::now();
        $user->created_at = Carbon::now();
        $user->updated_at = Carbon::now();
        $user->image = '';
        $user->phone = $request->phone;
        $user->email_confirmed = false; // E MAIL ONAYLANDIĞINDA TRUE OLACAK;
        $user->phone_confirmed = false; // SMS ONAYLANDIĞINDA TRUE OLACAK;

        $user->password = $this->django_password_create(str_random(16));
        //$user->privacy = $request->input('privacy');
        $user->phone_code = mt_rand(100000, 999000); // SMS ONAYLANDIĞINDA NULL OLACAK;
        $user->save();



        $profile = new Profile();

        $profile->user_id = $user->id;
        $profile->province_id = $request->province;
        $profile->district_id = $request->district_id;

        $profile->identity_number = $request->input('profile')['identity_number'];
        $profile->birth_date = $request->input('profile')['birth_date'];
        
        $profile->created_at = Carbon::now();
        $profile->updated_at = Carbon::now();
        $profile->save();



        $player = new Player();

        $player->user_id = $user->id;
        $player->facebook = $request->facebook;
        $player->twitter = $request->twitter;
        $player->instagram = $request->instagram;
        $player->position_id = $request->position_id;
        $player->position2_id = $request->position2_id;
        $player->position3_id = $request->position3_id;
        $player->weight = $request->weight;
        $player->height = $request->height;
        $player->foot = $request->foot;
        $player->number = $request->number;

        $player->value = 100000;
        $player->old_system_value = 0;

        $player->created_at = Carbon::now();
        $player->updated_at = Carbon::now();
        $player->save();


        //SMS GÖNDER;
        //$smsHelper = new SmsClass();
        //$smsHelper->smsSend($user->phone, $user->phone_code);




        $team = Team::find($request->id);
        $captain = $team->captain;
        $requested_player = Player::find($player->id);

        /***** Aktif Sezon gg *****/
        $activeSeason = null;
        $activeSeasons = array();
        $existSeasons = array();
        $t_seasons = $team->league->seasonApi;


        foreach ($t_seasons as $t_season) {
            if($t_season->active == true){
                if($t_season->polymorphic_ctype_id != 32){
                    if($t_season->start_date <= Carbon::now()->format('Y-m-d') && $t_season->end_date >= Carbon::now()->format('Y-m-d')){
                        $activeSeasons[] = $t_season;
                    }
                    $existSeasons[] = $t_season;
                }
            }
        }

        if(count($activeSeasons) > 1){
            foreach ($activeSeasons as $season) {
                if($season->polymorphic_ctype_id == 33){

                    $fs_group_teams_id = array();
                    foreach ($season->league_fixtureseason->groups as $group) {
                        foreach ($group->group_teams as $group_team) {
                            $fs_group_teams_id[] = $group_team->team_id;
                        }
                    }
                    if(array_search($team->id, $fs_group_teams_id)){
                        $activeSeason = $season;
                    }

                }
            }
        }else{
            if(count($existSeasons)){
                $activeSeason = $existSeasons[0];
            }
        }

        if(empty($activeSeason)){
            $activeSeason = $activeSeasons[0];
        }elseif(!empty($activeSeason)){

            if(!empty($team->team_teamseason->where('season_id', $activeSeason->id)->first())){
                $remaining_transfer_count = $team->team_teamseason->where('season_id', $activeSeason->id)->first()->remaining_transfer_count;
                $squad_locked = $team->team_teamseason->where('season_id', $activeSeason->id)->first()->squad_locked;
            }else{
                $remaining_transfer_count = $activeSeason->allowed_transfer_count;
                $squad_locked = false;
            }
        }
        /***** Aktif Sezon gg*****/

        if(!empty($activeSeason)){
            $teamSeason = $team->team_teamseason->where('season_id', $activeSeason->id)->first();
            if(!empty($teamSeason)){
                if($teamSeason->squad_locked){
                    $teamSeason->remaining_transfer_count = $teamSeason->remaining_transfer_count - 1;
                    $teamSeason->save();
                }
            }else{
                $teamSeason = new TeamTeamSeason();
                $teamSeason->squad_locked = false;
                if($activeSeason->polymorphic_ctype_id == 33){
                    $teamSeason->point = 0;
                }else if($activeSeason->polymorphic_ctype_id == 34){
                    $teamSeason->point = $activeSeason->league_pointseason->min_team_point;
                }
                
                $teamSeason->season_id = $activeSeason->id;
                $teamSeason->team_id = $team->id;
                $teamSeason->remaining_transfer_count = $activeSeason->allowed_transfer_count;
                $teamSeason->playoff_member = true;
                $teamSeason->drawn = 0;
                $teamSeason->goal_against = 0;
                $teamSeason->goal_for = 0;
                $teamSeason->lost = 0;
                $teamSeason->match_total = 0;
                $teamSeason->won = 0;
                $teamSeason->gk_save = 0;
                $teamSeason->red_card = 0;
                $teamSeason->yellow_card = 0;
                $teamSeason->save();
            }
        }

        if(!empty($requested_player->team)){
            if($requested_player->team->captain_id == $requested_player->id){
                $requested_player->team->captain_id = null;
                $requested_player->team->save();
            }
        }


        if(!empty($requested_player->team)){
            $teamTeamPlayer = TeamTeamPlayer::where('player_id', $requested_player->id)->where('player_id', $requested_player->team->id)->whereNull('released_date')->first();
            if(!empty($teamTeamPlayer)){
                $teamTeamPlayer->released_date = Carbon::now()->format('Y-m-d');
                $teamTeamPlayer->save();
            }
        }
        
        $teamTeamPlayerNew = new TeamTeamPlayer();
        $teamTeamPlayerNew->player_id = $requested_player->id;
        $teamTeamPlayerNew->team_id = $team->id;
        $teamTeamPlayerNew->joined_date = Carbon::now()->format('Y-m-d');
        $teamTeamPlayerNew->save();


        $transfer = new Transfer();
        $transfer->requesting_captain_id = $captain->id;
        $transfer->requesting_team_id = $captain->team_id;

        $transfer->requested_player_id = $requested_player->id;
        if (!empty($requested_player->team_id)) {
            $transfer->requested_team_id = $requested_player->team_id;
        }

        $transfer->status = "accept";
        $transfer->save();

        if(!empty($activeSeason)){

            $transferSeason = new TransferSeason();
            $transferSeason->transfer_id = $transfer->id;
            $transferSeason->season_id = $activeSeason->id;
            $transferSeason->save();

        }


        $requested_player->team_id = $team->id;
        $requested_player->save();


        $text = 'Site Dışından Oyuncu Ekleme Başarıyla Tamamlandı...';
        return redirect('teams/edit/'.$request->id.'?t=tab_03')->with('message', array('text' => $text, 'status' => 'success'));
    }

    public function userPhotoDelete($id) {
        $user = User::find($id);
        $user->image = null;
        $user->save();

        $text = 'Kullanıcı Fotoğrafı Başarıyla Silindi...';
        return redirect('users/edit/'.$id.'?t=tab_02')->with('message', array('text' => $text, 'status' => 'success'));
    }

    public function password($id = null) {
        $text = "Bu sayfaya ulaşmak için yetkiniz bulunmuyor!";
        if(
            (request()->segment(2) == 'edit' && !Auth::user()->hasPermissionTo('change_user')) ||
            (request()->segment(2) == 'delete' && !Auth::user()->hasPermissionTo('delete_user'))
        ){
            return redirect('/roles')->with('message', array('text' => $text, 'status' => 'error'));
        }

        $user = NULL;
        if(!is_null($id)){
            $user = User::find($id);
        }
        return view('dashboard', array('model_data' => $user));
    }

    public function save_password(Request $request) {
        $text = "";
        $model = User::find($request->id);
        $model->password = $this->django_password_create($request->input('password'));
        $model->save();

        $text = 'Başarıyla Güncellendi...';
        return redirect($request->segment)->with('message', array('text' => $text, 'status' => 'success'));
    }

    public function django_password_create(string $password) {
        $algorithm = "pbkdf2_sha256";
        $iterations = 150000;

        $salt = base64_encode(openssl_random_pseudo_bytes(9));
        //$salt = base64_encode($salt);

        $hash = hash_pbkdf2("SHA256", $password, $salt, $iterations, 0, true);    
        $toDBStr = $algorithm ."$". $iterations ."$". $salt ."$". base64_encode($hash);

        return $toDBStr;
    }

}
