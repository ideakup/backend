<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Validator;

use Carbon\Carbon;
use App\User;

use App\Season;

use App\PlayerPosition;
use App\PointType;
use App\Tactic;
use App\TeamPotential;
use App\TeamSatisfaction;
use App\TeamSatisfactionJogo;

use App\EliminationTree;
use App\EliminationTreesTeam;
use App\EliminationTreeItem;

use App\Group;
use App\GroupTeam;

use App\Match;
use App\MatchVideo;
use App\TeamTeamSeason;

class SharedController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function elimination_tree()
    {
    	return view('dashboard');
    }

    public function elimination_tree_crud($id = null)
    {
        $elimination_tree = NULL;
        if(!is_null($id)){
            $elimination_tree = EliminationTree::find($id);
        }
        return view('dashboard', array('model_data' => $elimination_tree));
    }

    public function elimination_tree_save(Request $request)
    {
        // dd($request->input());
        $arr = array();

        if($request->type == "trfinal"){
            // türkiye finalleri özel
            $arr[0]['level'] = 0;
            $arr[0]['level_title'] = 'Final';

            $arr[1]['level'] = 1;
            $arr[1]['level_title'] = 'Yarı Final';

            $arr[2]['level'] = 2;
            $arr[2]['level_title'] = 'Çeyrek Final';

            $arr[3]['level'] = 3;
            $arr[3]['level_title'] = 'Son 16';
        }else{
            $arr[0]['level'] = 0;
            $arr[0]['level_title'] = 'Final';

            $arr[1]['level'] = 1;
            $arr[1]['level_title'] = 'Yarı Final';

            $arr[2]['level'] = 2;
            $arr[2]['level_title'] = 'Çeyrek Final';

            $arr[3]['level'] = 3;
            $arr[3]['level_title'] = 'Eleme Turu';

            $arr[4]['level'] = 4;
            $arr[4]['level_title'] = 'Eleme Turu';

            $arr[5]['level'] = 5;
            $arr[5]['level_title'] = 'Eleme Turu';

            $arr[6]['level'] = 6;
            $arr[6]['level_title'] = 'Eleme Turu';
        }

        //dd($request->input());
        $text = "";
        if ($request->crud == 'delete') {

            $model = EliminationTree::find($request->id);

            if(!empty($model->items)){
                foreach ($model->items as $value) {
                    $value->delete();
                }
            }

            if(!empty($model->eliminaton_tree_teams)){
                foreach ($model->eliminaton_tree_teams as $value) {
                    $value->delete();
                }
            }

            $model->delete();

            $text = 'Başarıyla Silindi...';

        }else{

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $model = new EliminationTree();
            }else if($request->crud == 'edit'){
                $model = EliminationTree::find($request->id);
            }

            $rules = array();
            $formConfig = config('forms.'.$request->segment);

            if(!is_null($formConfig)){
                foreach ($formConfig as $key => $value){
                    if(!empty(data_get($value, 'validation'))){
                        $rules[$key] = data_get($value, 'validation');
                    }
                }
            }
            
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
               return \Redirect::back()->withErrors($validator)->withInput();
            }

            $model->name = $request->name;
            $model->type = $request->type;
            $model->season_id = $request->season_id;
            $model->tree_build = $request->tree_build;
            $model->save();
            
            if($request->type == "trfinal"){
                // türkiye finalleri özel
                if ($request->crud == 'add') {

                    if(!empty($request->teams)){
                        foreach ($request->teams as $teamId) {

                            $elimination_trees_team = new EliminationTreesTeam();
                            $elimination_trees_team->eliminationtree_id = $model->id;
                            $elimination_trees_team->team_id = $teamId;
                            $elimination_trees_team->save();
                            
                        }
                    }

                    for ($i=0; $i < $request->input('tree_build'); $i++) { 
                            
                            if($i == 0){
                                $etItem = new EliminationTreeItem();
                                $etItem->side = null;
                                $etItem->level = $i;
                                $etItem->level_title = 'Final';
                                $etItem->tree_id = $model->id;
                                $etItem->save();
                            }
                            if($i == 1){

                                $etItem = new EliminationTreeItem();
                                $etItem->side = 'third';
                                $etItem->level = 0;
                                $etItem->level_title = 'Üçüncülük Maçı';
                                $etItem->tree_id = $model->id;
                                
                                $etItem->save();
                                
                                $side= 'left';
                                for($j=0; $j < pow(2, $i); $j++){
                                    $etItem = new EliminationTreeItem();
                                    $etItem->side = $side;
                                    $etItem->level = $i;
                                    $etItem->level_title = 'Yarı Final';
                                    $etItem->tree_id = $model->id;
                                    $etItem->line_group = $j+1;
                                    $etItem->save();
                                    $side= 'right';
                                }

                                
                            }
                            if($i == 2){
                                
                                $side= 'left';
                                for($j=0; $j < pow(2, $i); $j++){
                                    $etItem = new EliminationTreeItem();
                                    $etItem->side = $side;
                                    $etItem->level = $i;
                                    $etItem->level_title = 'Çeyrek Final';
                                    $etItem->tree_id = $model->id;
                                    $etItem->line_group = $j+1;
                                    $etItem->save();
                                    if( $side == 'left'){
                                        $side = 'right';
                                    }
                                    elseif( $side == 'right'){
                                         $side = 'left';
                                    }

                                }

                                
                            }
                            if($i == 3){
                                
                                $side= 'left';
                                for($j=0; $j < pow(2, $i); $j++){
                                    $etItem = new EliminationTreeItem();
                                    $etItem->side = $side;
                                    $etItem->level = $i;
                                    $etItem->level_title = 'Son 16';
                                    $etItem->tree_id = $model->id;
                                    $etItem->line_group = $j+1;
                                    $etItem->save();
                                    if( $side == 'left'){
                                        $side = 'right';
                                    }
                                    elseif( $side == 'right'){
                                         $side = 'left';
                                    }

                                }

                                
                            }
                            

                    }

                }else if($request->crud == 'edit'){

                    if(!empty($request->teams)){
                        foreach ($request->teams as $teamId) {
                            $elimination_trees_team_count = EliminationTreesTeam::where('eliminationtree_id', $model->id)->where('team_id', $teamId)->count();
                            if($elimination_trees_team_count == 0){
                                $elimination_trees_team = new EliminationTreesTeam();
                                $elimination_trees_team->eliminationtree_id = $model->id;
                                $elimination_trees_team->team_id = $teamId;
                                $elimination_trees_team->save();
                            }
                        }  
                    }  

                    $deleteTeams = array();
                    foreach ($model->eliminaton_tree_teams as $etTeam) {
                        //dump($etTeam->team_id);
                        if(!in_array($etTeam->team_id, $request->teams)){
                            
                            $deleteTeams[] = $etTeam->team_id;
                        }
                    }

                    foreach ($deleteTeams as $deleteTeam) {
                        //dump($deleteTeam. ' silinecek');
                        $delete_elimination_trees_team = EliminationTreesTeam::where('eliminationtree_id', $model->id)->where('team_id', $deleteTeam)->first();
                        $delete_elimination_trees_team->delete();
                    }

                    if ($request->input('tree_build') > $model->items_distinct->count()){
                        
                        for ($i=$model->items_distinct->count(); $i < $request->input('tree_build'); $i++) {

                            $etItem = new EliminationTreeItem();
                            $etItem->side = 'left';
                            $etItem->level = $arr[$i]['level'];
                            $etItem->level_title = $arr[$i]['level_title'];
                            $etItem->tree_id = $model->id;
                            $etItem->save();

                            $etItem = new EliminationTreeItem();
                            $etItem->side = 'right';
                            $etItem->level = $arr[$i]['level'];
                            $etItem->level_title = $arr[$i]['level_title'];
                            $etItem->tree_id = $model->id;
                            $etItem->save(); 
                        }
                        //dd('ekle '.$request->input('tree_build').'-'.$model->items_distinct->count());
                    }

                    if ($request->input('tree_build') < $model->items_distinct->count()){

                        for ($i=$request->input('tree_build'); $i < $model->items_distinct->count(); $i++) {
                            $etItems = EliminationTreeItem::where('tree_id', $model->id)->where('level', $i)->get();
                            foreach ($etItems as $etItem) {
                                $etItem->delete();
                            }
                        }
                        //dd('sil '.$request->input('tree_build').'-'.$model->items_distinct->count());
                    }
                    //dump($request->input('tree_build'));
                    $num = 1;
                    for ($i=$request->input('tree_build')-1; $i > 2; $i--) {
                        //dump($i.' - '.$num);
                        $etItems = EliminationTreeItem::where('tree_id', $model->id)->where('level', $i)->get();
                        foreach ($etItems as $etItem) {
                            $etItem->level_title = $num.'. '.$arr[$i]['level_title'];
                            $etItem->save();
                        }
                        //dump($i);
                        $num++;

                    }

                }
            
            }else{

                if ($request->crud == 'add') {

                    if(!empty($request->teams)){
                        foreach ($request->teams as $teamId) {

                            $elimination_trees_team = new EliminationTreesTeam();
                            $elimination_trees_team->eliminationtree_id = $model->id;
                            $elimination_trees_team->team_id = $teamId;
                            $elimination_trees_team->save();
                            
                        }
                    }

                    for ($i=0; $i < $request->input('tree_build'); $i++) { 
                        if($i == 0){
                            $etItem = new EliminationTreeItem();
                            $etItem->level = $arr[$i]['level'];
                            $etItem->level_title = $arr[$i]['level_title'];
                            $etItem->tree_id = $model->id;
                            $etItem->save();
                        }else{

                            $a = '';
                            if($i > 2){
                                $a = ($request->input('tree_build') - $i).' .';
                            }

                            $etItem = new EliminationTreeItem();
                            $etItem->side = 'left';
                            $etItem->level = $arr[$i]['level'];
                            $etItem->level_title = $a.$arr[$i]['level_title'];
                            $etItem->tree_id = $model->id;
                            $etItem->save();

                            $etItem = new EliminationTreeItem();
                            $etItem->side = 'right';
                            $etItem->level = $arr[$i]['level'];
                            $etItem->level_title = $a.$arr[$i]['level_title'];
                            $etItem->tree_id = $model->id;
                            $etItem->save(); 
                        }

                    }

                }else if($request->crud == 'edit'){

                    if(!empty($request->teams)){
                        foreach ($request->teams as $teamId) {
                            $elimination_trees_team_count = EliminationTreesTeam::where('eliminationtree_id', $model->id)->where('team_id', $teamId)->count();
                            if($elimination_trees_team_count == 0){
                                $elimination_trees_team = new EliminationTreesTeam();
                                $elimination_trees_team->eliminationtree_id = $model->id;
                                $elimination_trees_team->team_id = $teamId;
                                $elimination_trees_team->save();
                            }
                        }  
                    }  

                    $deleteTeams = array();
                    foreach ($model->eliminaton_tree_teams as $etTeam) {
                        //dump($etTeam->team_id);
                        if(!in_array($etTeam->team_id, $request->teams)){
                            
                            $deleteTeams[] = $etTeam->team_id;
                        }
                    }

                    foreach ($deleteTeams as $deleteTeam) {
                        //dump($deleteTeam. ' silinecek');
                        $delete_elimination_trees_team = EliminationTreesTeam::where('eliminationtree_id', $model->id)->where('team_id', $deleteTeam)->first();
                        $delete_elimination_trees_team->delete();
                    }

                    if ($request->input('tree_build') > $model->items_distinct->count()){
                        
                        for ($i=$model->items_distinct->count(); $i < $request->input('tree_build'); $i++) {

                            $etItem = new EliminationTreeItem();
                            $etItem->side = 'left';
                            $etItem->level = $arr[$i]['level'];
                            $etItem->level_title = $arr[$i]['level_title'];
                            $etItem->tree_id = $model->id;
                            $etItem->save();

                            $etItem = new EliminationTreeItem();
                            $etItem->side = 'right';
                            $etItem->level = $arr[$i]['level'];
                            $etItem->level_title = $arr[$i]['level_title'];
                            $etItem->tree_id = $model->id;
                            $etItem->save(); 
                        }
                        //dd('ekle '.$request->input('tree_build').'-'.$model->items_distinct->count());
                    }

                    if ($request->input('tree_build') < $model->items_distinct->count()){

                        for ($i=$request->input('tree_build'); $i < $model->items_distinct->count(); $i++) {
                            $etItems = EliminationTreeItem::where('tree_id', $model->id)->where('level', $i)->get();
                            foreach ($etItems as $etItem) {
                                $etItem->delete();
                            }
                        }
                        //dd('sil '.$request->input('tree_build').'-'.$model->items_distinct->count());
                    }
                    //dump($request->input('tree_build'));
                    $num = 1;
                    for ($i=$request->input('tree_build')-1; $i > 2; $i--) {
                        //dump($i.' - '.$num);
                        $etItems = EliminationTreeItem::where('tree_id', $model->id)->where('level', $i)->get();
                        foreach ($etItems as $etItem) {
                            $etItem->level_title = $num.'. '.$arr[$i]['level_title'];
                            $etItem->save();
                        }
                        //dump($i);
                        $num++;

                    }

                }

            }
            
            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $text = 'Başarıyla Eklendi...';
            }else if($request->crud == 'edit'){
                $text = 'Başarıyla Güncellendi...';
            }
        }

        if(!empty($request->tree_id)){
            return redirect($request->segment.'?tree_id='.$request->tree_id)->with('message', array('text' => $text, 'status' => 'success'));
        }else{
            return redirect($request->segment)->with('message', array('text' => $text, 'status' => 'success'));
        }
    }


    public function elimination_tree_tree_crud($id = null)
    {
        $elimination_tree = NULL;
        if(!is_null($id)){
            $elimination_tree = EliminationTree::find($id);
        }
        return view('dashboard', array('model_data' => $elimination_tree));
    }


    public function elimination_tree_item_save(Request $request)
    {

        $text = '';
        if ($request->crud == 'tree') {
            
            $model = EliminationTree::find($request->id);
            $create_match = array();
            

            if($model->type == 'trfinal'){
                foreach ($model->items as $etItem) {

                    if(empty($etItem->match_id)){
                        if($etItem->level == 0 && $etItem->level_title == 'Final'){
                            $etItem->team1_id = $request->input('final_team1');
                            $etItem->team2_id = $request->input('final_team2');
                            
                            if(!empty($request->input('final_match_date'))){
                                $etItem->date = Carbon::parse($request->input('final_match_date'))->format('Y-m-d');
                            }else{
                                $etItem->date = null;
                            }
                            
                            if(!empty($request->input('final_match_time'))){
                                $etItem->time = $request->input('final_match_time');
                            }else{
                                $etItem->time = null;
                            }

                            if(!empty($request->input('final_match_create'))){
                                $create_match[] = array('slug' => 'final_', 'model' => $etItem);
                            }
                        }    
                        elseif($etItem->level == 0 && $etItem->level_title == 'Üçüncülük Maçı'){
                            $etItem->team1_id = $request->input('third_team1');
                            $etItem->team2_id = $request->input('third_team2');
                            
                            if(!empty($request->input('third_match_date'))){
                                $etItem->date = Carbon::parse($request->input('third_match_date'))->format('Y-m-d');
                            }else{
                                $etItem->date = null;
                            }
                            
                            if(!empty($request->input('third_match_time'))){
                                $etItem->time = $request->input('third_match_time');
                            }else{
                                $etItem->time = null;
                            }

                            if(!empty($request->input('third_match_create'))){
                                $create_match[] = array('slug' => 'final_', 'model' => $etItem);
                            }
                        }else if($etItem->level == 1){
                            $etItem->team1_id = $request->input('semifinal_'.$etItem->side.'_team1');
                            $etItem->team2_id = $request->input('semifinal_'.$etItem->side.'_team2');
                            
                            if(!empty($request->input('semifinal_'.$etItem->side.'_match_date'))){
                                $etItem->date = Carbon::parse($request->input('semifinal_'.$etItem->side.'_match_date'))->format('Y-m-d');
                            }else{
                                $etItem->date = null;
                            }
                            
                            if(!empty($request->input('semifinal_'.$etItem->side.'_match_time'))){
                                $etItem->time = $request->input('semifinal_'.$etItem->side.'_match_time');
                            }else{
                                $etItem->time = null;
                            }

                            if(!empty($request->input('semifinal_'.$etItem->side.'_match_create'))){
                                $create_match[] = array('slug' => 'semifinal_', 'model' => $etItem);
                            }

                        }else if($etItem->level == 2){
                            $etItem->team1_id = $request->input('quarterfinal_'.$etItem->side.'_match'.$etItem->line_group.'_team1');
                            $etItem->team2_id = $request->input('quarterfinal_'.$etItem->side.'_match'.$etItem->line_group.'_team2');
                            
                            if(!empty($request->input('quarterfinal_'.$etItem->side.'_match'.$etItem->line_group.'_match_date'))){
                                $etItem->date = Carbon::parse($request->input('quarterfinal_'.$etItem->side.'_match'.$etItem->line_group.'_match_date'))->format('Y-m-d');
                            }else{
                                $etItem->date = null;
                            }
                            
                            if(!empty($request->input('quarterfinal_'.$etItem->side.'_match'.$etItem->line_group.'_match_time'))){
                                $etItem->time = $request->input('quarterfinal_'.$etItem->side.'_match'.$etItem->line_group.'_match_time');
                            }else{
                                $etItem->time = null;
                            }

                            if(!empty($request->input('quarterfinal_'.$etItem->side.'_match'.$etItem->line_group.'_match_create'))){
                                $create_match[] = array('slug' => 'quarterfinal_', 'model' => $etItem);
                            }

                        }else if($etItem->level == 3){
                            $etItem->team1_id = $request->input('tour1_'.$etItem->side.'_match'.$etItem->line_group.'_team1');
                            $etItem->team2_id = $request->input('tour1_'.$etItem->side.'_match'.$etItem->line_group.'_team2');
                            
                            if(!empty($request->input('tour1_'.$etItem->side.'_match'.$etItem->line_group.'_match_date'))){
                                $etItem->date = Carbon::parse($request->input('tour1_'.$etItem->side.'_match'.$etItem->line_group.'_match_date'))->format('Y-m-d');
                            }else{
                                $etItem->date = null;
                            }
                            
                            if(!empty($request->input('tour1_'.$etItem->side.'_match'.$etItem->line_group.'_match_time'))){
                                $etItem->time = $request->input('tour1_'.$etItem->side.'_match'.$etItem->line_group.'_match_time');
                            }else{
                                $etItem->time = null;
                            }

                            if(!empty($request->input('tour1_'.$etItem->side.'_match'.$etItem->line_group.'_match_create'))){
                                $create_match[] = array('slug' => 'tour1_'.$etItem->side.'_', 'model' => $etItem);
                            }

                        }
                    }
                    
                    $etItem->save();

                    $text = ' Eleme Ağacı Başarıyla Güncellendi...';
                }
                
                $season = $model->league_eliminationseason->season_ptr;
                foreach ($create_match as $key => $value) {
                    //dump($value['slug']);
                    $_eliminationTree = $value['model'];

                    if(!empty($_eliminationTree->team1_id) && !empty($_eliminationTree->team2_id) && !empty($_eliminationTree->date) && !empty($_eliminationTree->time)){

                        $match = new Match();
                        $match->season_id = $season->id;
                        $match->team1_id = $_eliminationTree->team1_id;
                        $match->team2_id = $_eliminationTree->team2_id;
                        $match->date = $_eliminationTree->date;
                        $match->time = $_eliminationTree->time;

                        $match->team1_goal = 0;
                        $match->team2_goal = 0;
                        $match->team1_point = 0;
                        $match->team2_point = 0;

                        $match->completed = false;
                        $match->save();

                        $min_point = 10;

                        $teamSeason1 = TeamTeamSeason::where('season_id', $season->id)->where('team_id', $_eliminationTree->team1_id)->first();
                        if(empty($teamSeason1)){

                            $teamSeason = new TeamTeamSeason();

                            if($season->polymorphic_ctype_id == 32){ /*elimination*/
                                $teamSeason->squad_locked = true;
                            }else{
                                $teamSeason->squad_locked = false;
                            }
                            
                            $teamSeason->point = $min_point;
                            $teamSeason->season_id = $season->id;
                            $teamSeason->team_id = $_eliminationTree->team1_id;

                            $teamSeason->remaining_transfer_count = $season->allowed_transfer_count;
                            $teamSeason->playoff_member = true;
                            $teamSeason->drawn = 0;
                            $teamSeason->goal_against = 0;
                            $teamSeason->goal_for = 0;
                            $teamSeason->lost = 0;
                            $teamSeason->match_total = 0;
                            $teamSeason->won = 0;
                            $teamSeason->gk_save = 0;
                            $teamSeason->red_card = 0;
                            $teamSeason->yellow_card = 0;

                            $teamSeason->save();

                        }

                        $teamSeason2 = TeamTeamSeason::where('season_id', $season->id)->where('team_id', $_eliminationTree->team2_id)->first();
                        if(empty($teamSeason2)){

                            $teamSeason = new TeamTeamSeason();

                            if($season->polymorphic_ctype_id == 32){ /*elimination*/
                                $teamSeason->squad_locked = true;
                            }else{
                                $teamSeason->squad_locked = false;
                            }
                            
                            $teamSeason->point = $min_point;
                            $teamSeason->season_id = $season->id;
                            $teamSeason->team_id = $_eliminationTree->team2_id;

                            $teamSeason->remaining_transfer_count = $season->allowed_transfer_count;
                            $teamSeason->playoff_member = true;
                            $teamSeason->drawn = 0;
                            $teamSeason->goal_against = 0;
                            $teamSeason->goal_for = 0;
                            $teamSeason->lost = 0;
                            $teamSeason->match_total = 0;
                            $teamSeason->won = 0;
                            $teamSeason->gk_save = 0;
                            $teamSeason->red_card = 0;
                            $teamSeason->yellow_card = 0;

                            $teamSeason->save();

                        }


                        $match_video = MatchVideo::where('match_id', $match->id)->first();
                        if (is_null($match_video)) {
                            $match_video = new MatchVideo();
                            $match_video->match_id = $match->id;
                        }

                        $match_video->status = 'recordable';
                        $match_video->live_source = 'app';
                        $match_video->live_embed_code = null;
                        $match_video->full_source = 'app';
                        $match_video->full_embed_code = null;
                        $match_video->summary_source = 'app';
                        $match_video->summary_embed_code = null;
                        $match_video->save();
                        

                        $_eliminationTree->match_id = $match->id;
                        $_eliminationTree->save();


                        $text .= ' İşaretlenmiş Maçlar Başarıyla Oluşturuldu...';

                    }else{

                        $text = 'Maç Oluşturulamadı. Eksik Bilgileri Tamamlayınız...';
                        return redirect($request->segment.'/'.$request->crud.'/'.$request->id)->with('message', array('text' => $text, 'status' => 'error'));

                    }
                }

            }else{

                foreach ($model->items as $etItem) {
                    if(empty($etItem->match_id)){
                        if($etItem->level == 0){
                            $etItem->team1_id = $request->input('final_team1');
                            $etItem->team2_id = $request->input('final_team2');
                            
                            if(!empty($request->input('final_match_date'))){
                                $etItem->date = Carbon::parse($request->input('final_match_date'))->format('Y-m-d');
                            }else{
                                $etItem->date = null;
                            }
                            
                            if(!empty($request->input('final_match_time'))){
                                $etItem->time = $request->input('final_match_time');
                            }else{
                                $etItem->time = null;
                            }

                            if(!empty($request->input('final_match_create'))){
                                $create_match[] = array('slug' => 'final_', 'model' => $etItem);
                            }

                        }else if($etItem->level == 1){
                            $etItem->team1_id = $request->input('semifinal_'.$etItem->side.'_team1');
                            $etItem->team2_id = $request->input('semifinal_'.$etItem->side.'_team2');
                            
                            if(!empty($request->input('semifinal_'.$etItem->side.'_match_date'))){
                                $etItem->date = Carbon::parse($request->input('semifinal_'.$etItem->side.'_match_date'))->format('Y-m-d');
                            }else{
                                $etItem->date = null;
                            }
                            
                            if(!empty($request->input('semifinal_'.$etItem->side.'_match_time'))){
                                $etItem->time = $request->input('semifinal_'.$etItem->side.'_match_time');
                            }else{
                                $etItem->time = null;
                            }

                            if(!empty($request->input('semifinal_'.$etItem->side.'_match_create'))){
                                $create_match[] = array('slug' => 'semifinal_', 'model' => $etItem);
                            }

                        }else if($etItem->level == 2){
                            $etItem->team1_id = $request->input('quarterfinal_'.$etItem->side.'_team1');
                            $etItem->team2_id = $request->input('quarterfinal_'.$etItem->side.'_team2');
                            
                            if(!empty($request->input('quarterfinal_'.$etItem->side.'_match_date'))){
                                $etItem->date = Carbon::parse($request->input('quarterfinal_'.$etItem->side.'_match_date'))->format('Y-m-d');
                            }else{
                                $etItem->date = null;
                            }
                            
                            if(!empty($request->input('quarterfinal_'.$etItem->side.'_match_time'))){
                                $etItem->time = $request->input('quarterfinal_'.$etItem->side.'_match_time');
                            }else{
                                $etItem->time = null;
                            }

                            if(!empty($request->input('quarterfinal_'.$etItem->side.'_match_create'))){
                                $create_match[] = array('slug' => 'quarterfinal_', 'model' => $etItem);
                            }

                        }else if($etItem->level == 3){
                            $etItem->team1_id = $request->input('tour1_'.$etItem->side.'_team1');
                            $etItem->team2_id = $request->input('tour1_'.$etItem->side.'_team2');
                            
                            if(!empty($request->input('tour1_'.$etItem->side.'_match_date'))){
                                $etItem->date = Carbon::parse($request->input('tour1_'.$etItem->side.'_match_date'))->format('Y-m-d');
                            }else{
                                $etItem->date = null;
                            }
                            
                            if(!empty($request->input('tour1_'.$etItem->side.'_match_time'))){
                                $etItem->time = $request->input('tour1_'.$etItem->side.'_match_time');
                            }else{
                                $etItem->time = null;
                            }

                            if(!empty($request->input('tour1_'.$etItem->side.'_match_create'))){
                                $create_match[] = array('slug' => 'tour1_'.$etItem->side.'_', 'model' => $etItem);
                            }

                        }else if($etItem->level == 4){
                            $etItem->team1_id = $request->input('tour2_'.$etItem->side.'_team1');
                            $etItem->team2_id = $request->input('tour2_'.$etItem->side.'_team2');
                            
                            if(!empty($request->input('tour2_'.$etItem->side.'_match_date'))){
                                $etItem->date = Carbon::parse($request->input('tour2_'.$etItem->side.'_match_date'))->format('Y-m-d');
                            }else{
                                $etItem->date = null;
                            }
                            
                            if(!empty($request->input('tour2_'.$etItem->side.'_match_time'))){
                                $etItem->time = $request->input('tour2_'.$etItem->side.'_match_time');
                            }else{
                                $etItem->time = null;
                            }

                            if(!empty($request->input('tour2_'.$etItem->side.'_match_create'))){
                                $create_match[] = array('slug' => 'tour2_'.$etItem->side.'_', 'model' => $etItem);
                            }

                        }else if($etItem->level == 5){
                            $etItem->team1_id = $request->input('tour3_'.$etItem->side.'_team1');
                            $etItem->team2_id = $request->input('tour3_'.$etItem->side.'_team2');
                            
                            if(!empty($request->input('tour3_'.$etItem->side.'_match_date'))){
                                $etItem->date = Carbon::parse($request->input('tour3_'.$etItem->side.'_match_date'))->format('Y-m-d');
                            }else{
                                $etItem->date = null;
                            }
                            
                            if(!empty($request->input('tour3_'.$etItem->side.'_match_time'))){
                                $etItem->time = $request->input('tour3_'.$etItem->side.'_match_time');
                            }else{
                                $etItem->time = null;
                            }

                            if(!empty($request->input('tour3_'.$etItem->side.'_match_create'))){
                                $create_match[] = array('slug' => 'tour3_'.$etItem->side.'_', 'model' => $etItem);
                            }

                        }else if($etItem->level == 6){
                            
                            $etItem->team1_id = $request->input('tour4_'.$etItem->side.'_team1');
                            $etItem->team2_id = $request->input('tour4_'.$etItem->side.'_team2');
                            
                            if(!empty($request->input('tour4_'.$etItem->side.'_match_date'))){
                                $etItem->date = Carbon::parse($request->input('tour4_'.$etItem->side.'_match_date'))->format('Y-m-d');
                            }else{
                                $etItem->date = null;
                            }
                            
                            if(!empty($request->input('tour4_'.$etItem->side.'_match_time'))){
                                $etItem->time = $request->input('tour4_'.$etItem->side.'_match_time');
                            }else{
                                $etItem->time = null;
                            }

                            if(!empty($request->input('tour4_'.$etItem->side.'_match_create'))){
                                $create_match[] = array('slug' => 'tour4_'.$etItem->side.'_', 'model' => $etItem);
                            }

                        }
                    }

                    $etItem->save();

                    $text = ' Eleme Ağacı Başarıyla Güncellendi...';
                }
                
                $season = $model->league_eliminationseason->season_ptr;
                foreach ($create_match as $key => $value) {
                    //dump($value['slug']);
                    $_eliminationTree = $value['model'];

                    if(!empty($_eliminationTree->team1_id) && !empty($_eliminationTree->team2_id) && !empty($_eliminationTree->date) && !empty($_eliminationTree->time)){

                        $match = new Match();
                        $match->season_id = $season->id;
                        $match->team1_id = $_eliminationTree->team1_id;
                        $match->team2_id = $_eliminationTree->team2_id;
                        $match->date = $_eliminationTree->date;
                        $match->time = $_eliminationTree->time;

                        $match->team1_goal = 0;
                        $match->team2_goal = 0;
                        $match->team1_point = 0;
                        $match->team2_point = 0;

                        $match->completed = false;
                        $match->save();

                        $min_point = 10;

                        $teamSeason1 = TeamTeamSeason::where('season_id', $season->id)->where('team_id', $_eliminationTree->team1_id)->first();
                        if(empty($teamSeason1)){

                            $teamSeason = new TeamTeamSeason();

                            if($season->polymorphic_ctype_id == 32){ /*elimination*/
                                $teamSeason->squad_locked = true;
                            }else{
                                $teamSeason->squad_locked = false;
                            }
                            
                            $teamSeason->point = $min_point;
                            $teamSeason->season_id = $season->id;
                            $teamSeason->team_id = $_eliminationTree->team1_id;

                            $teamSeason->remaining_transfer_count = $season->allowed_transfer_count;
                            $teamSeason->playoff_member = true;
                            $teamSeason->drawn = 0;
                            $teamSeason->goal_against = 0;
                            $teamSeason->goal_for = 0;
                            $teamSeason->lost = 0;
                            $teamSeason->match_total = 0;
                            $teamSeason->won = 0;
                            $teamSeason->gk_save = 0;
                            $teamSeason->red_card = 0;
                            $teamSeason->yellow_card = 0;

                            $teamSeason->save();

                        }

                        $teamSeason2 = TeamTeamSeason::where('season_id', $season->id)->where('team_id', $_eliminationTree->team2_id)->first();
                        if(empty($teamSeason2)){

                            $teamSeason = new TeamTeamSeason();

                            if($season->polymorphic_ctype_id == 32){ /*elimination*/
                                $teamSeason->squad_locked = true;
                            }else{
                                $teamSeason->squad_locked = false;
                            }
                            
                            $teamSeason->point = $min_point;
                            $teamSeason->season_id = $season->id;
                            $teamSeason->team_id = $_eliminationTree->team2_id;

                            $teamSeason->remaining_transfer_count = $season->allowed_transfer_count;
                            $teamSeason->playoff_member = true;
                            $teamSeason->drawn = 0;
                            $teamSeason->goal_against = 0;
                            $teamSeason->goal_for = 0;
                            $teamSeason->lost = 0;
                            $teamSeason->match_total = 0;
                            $teamSeason->won = 0;
                            $teamSeason->gk_save = 0;
                            $teamSeason->red_card = 0;
                            $teamSeason->yellow_card = 0;

                            $teamSeason->save();

                        }

                        $match_video = MatchVideo::where('match_id', $match->id)->first();
                        if (is_null($match_video)) {
                            $match_video = new MatchVideo();
                            $match_video->match_id = $match->id;
                        }

                        $match_video->status = 'recordable';
                        $match_video->live_source = 'app';
                        $match_video->live_embed_code = null;
                        $match_video->full_source = 'app';
                        $match_video->full_embed_code = null;
                        $match_video->summary_source = 'app';
                        $match_video->summary_embed_code = null;
                        $match_video->save();



                        $_eliminationTree->match_id = $match->id;
                        $_eliminationTree->save();

                        $text .= ' İşaretlenmiş Maçlar Başarıyla Oluşturuldu...';

                    }else{

                        $text = 'Maç Oluşturulamadı. Eksik Bilgileri Tamamlayınız...';
                        return redirect($request->segment.'/'.$request->crud.'/'.$request->id)->with('message', array('text' => $text, 'status' => 'error'));

                    }
                
                }

            }

        }

        if(!empty($request->tree_id)){
            return redirect($request->segment.'?tree_id='.$request->tree_id)->with('message', array('text' => $text, 'status' => 'success'));
        }else{
            return redirect($request->segment)->with('message', array('text' => $text, 'status' => 'success'));
        }

    }




    public function fixture_group()
    {
        return view('dashboard');
    }

    public function fixture_group_crud($id = null)
    {
        $fixture_group = NULL;
        if(!is_null($id)){
            $fixture_group = Group::find($id);
        }
        return view('dashboard', array('model_data' => $fixture_group));
    }

    public function fixture_group_save(Request $request)
    {
        //dd($request->input());
        $arr = array();
        $text = "";

        if ($request->crud == 'delete') {
            
                $model = Group::find($request->id);
                if(!empty($model->group_teams)){
                    foreach ($model->group_teams as $group_team) {

                        $teamSeason = TeamTeamSeason::where('season_id', $request->season_id)->where('team_id', $group_team->team_id)->first();
                        if(!empty($teamSeason)){
                            $teamSeason->delete();
                        }
                        
                        $group_team->delete();

                    }
                }

                $model->delete();
                $text = 'Başarıyla Silindi...';

        }else{

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $model = new Group();
            }else if($request->crud == 'edit'){
                $model = Group::find($request->id);
            }

            $rules = array();
            $formConfig = config('forms.'.$request->segment);

            if(!is_null($formConfig)){
                foreach ($formConfig as $key => $value){
                    if(!empty(data_get($value, 'validation'))){
                        $rules[$key] = data_get($value, 'validation');
                    }
                }
            }
            
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
               return \Redirect::back()->withErrors($validator)->withInput();
            }

            $model->name = $request->name;
            $model->season_id = $request->tree_id;
            $model->active = (!empty($request->active)) ? true : false;
            $model->save();
            


            if ($request->crud == 'add') {

                if(!empty($request->teams)){
                    foreach ($request->teams as $teamId) {

                        $fixture_group_team = new GroupTeam();
                        $fixture_group_team->group_id = $model->id;
                        $fixture_group_team->team_id = $teamId;
                        $fixture_group_team->save();
                        
                        $season = Season::find($request->season_id);
                        $teamSeason = TeamTeamSeason::where('season_id', $request->season_id)->where('team_id', $teamId)->first();
                        if(empty($teamSeason)){

                            $teamSeason = new TeamTeamSeason();

                            $teamSeason->squad_locked = false;
                            $teamSeason->point = $season->league_fixtureseason->min_team_point;
                            $teamSeason->season_id = $request->season_id;
                            $teamSeason->team_id = $teamId;

                            $teamSeason->remaining_transfer_count = $season->allowed_transfer_count;
                            $teamSeason->playoff_member = true;
                            $teamSeason->drawn = 0;
                            $teamSeason->goal_against = 0;
                            $teamSeason->goal_for = 0;
                            $teamSeason->lost = 0;
                            $teamSeason->match_total = 0;
                            $teamSeason->won = 0;
                            $teamSeason->gk_save = 0;
                            $teamSeason->red_card = 0;
                            $teamSeason->yellow_card = 0;

                            $teamSeason->save();
                        }

                    }
                }

            }else if($request->crud == 'edit'){
                
                if(!empty($request->teams)){
                    foreach ($request->teams as $teamId) {

                        $group_team_count = GroupTeam::where('group_id', $model->id)->where('team_id', $teamId)->count();
                        if($group_team_count == 0){

                            $group_team = new GroupTeam();
                            $group_team->group_id = $model->id;
                            $group_team->team_id = $teamId;
                            $group_team->save();
                        
                        }

                        $season = Season::find($request->season_id);
                        $teamSeason = TeamTeamSeason::where('season_id', $request->season_id)->where('team_id', $teamId)->first();
                        if(empty($teamSeason)){

                            $teamSeason = new TeamTeamSeason();

                            $teamSeason->squad_locked = false;
                            $teamSeason->point = $season->league_fixtureseason->min_team_point;
                            $teamSeason->season_id = $request->season_id;
                            $teamSeason->team_id = $teamId;

                            $teamSeason->remaining_transfer_count = $season->allowed_transfer_count;
                            $teamSeason->playoff_member = true;
                            $teamSeason->drawn = 0;
                            $teamSeason->goal_against = 0;
                            $teamSeason->goal_for = 0;
                            $teamSeason->lost = 0;
                            $teamSeason->match_total = 0;
                            $teamSeason->won = 0;
                            $teamSeason->gk_save = 0;
                            $teamSeason->red_card = 0;
                            $teamSeason->yellow_card = 0;

                            $teamSeason->save();
                        }



                    }  
                }  

                $deleteTeams = array();
                foreach ($model->group_teams as $etTeam) {
                    if(!in_array($etTeam->team_id, $request->teams)){
                        
                        $deleteTeams[] = $etTeam->team_id;
                    }
                }

                foreach ($deleteTeams as $deleteTeam) {

                    $teamSeason = TeamTeamSeason::where('season_id', $request->season_id)->where('team_id', $deleteTeam)->first();
                    if(!empty($teamSeason)){
                        $teamSeason->delete();
                    }

                    $delete_group_team = GroupTeam::where('group_id', $model->id)->where('team_id', $deleteTeam)->first();
                    $delete_group_team->delete();
                }
                
            }
            
            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $text = 'Başarıyla Eklendi...';
            }else if($request->crud == 'edit'){
                $text = 'Başarıyla Güncellendi...';
            }
        }

        if(!empty($request->tree_id)){
            return redirect($request->segment.'?tree_id='.$request->tree_id)->with('message', array('text' => $text, 'status' => 'success'));
        }else{
            return redirect($request->segment)->with('message', array('text' => $text, 'status' => 'success'));
        }
    }

    public function fixture_group_tree_crud($id = null)
    {
        $elimination_tree = NULL;
        if(!is_null($id)){
            $elimination_tree = EliminationTree::find($id);
        }
        return view('dashboard', array('model_data' => $elimination_tree));
    }

    public function fixture_group_item_save(Request $request)
    {

        //dd($request->input());
        $text = '';
        if ($request->crud == 'tree') {
            
            $model = EliminationTree::find($request->id);
            $create_match = array();

            foreach ($model->items as $etItem) {
                if(empty($etItem->match_id)){
                    if($etItem->level == 0){
                        $etItem->team1_id = $request->input('final_team1');
                        $etItem->team2_id = $request->input('final_team2');
                        
                        if(!empty($request->input('final_match_date'))){
                            $etItem->date = Carbon::parse($request->input('final_match_date'))->format('Y-m-d');
                        }else{
                            $etItem->date = null;
                        }
                        
                        if(!empty($request->input('final_match_time'))){
                            $etItem->time = $request->input('final_match_time');
                        }else{
                            $etItem->time = null;
                        }

                        if(!empty($request->input('final_match_create'))){
                            $create_match[] = array('slug' => 'final_', 'model' => $etItem);
                        }

                    }else if($etItem->level == 1){
                        $etItem->team1_id = $request->input('semifinal_'.$etItem->side.'_team1');
                        $etItem->team2_id = $request->input('semifinal_'.$etItem->side.'_team2');
                        
                        if(!empty($request->input('semifinal_'.$etItem->side.'_match_date'))){
                            $etItem->date = Carbon::parse($request->input('semifinal_'.$etItem->side.'_match_date'))->format('Y-m-d');
                        }else{
                            $etItem->date = null;
                        }
                        
                        if(!empty($request->input('semifinal_'.$etItem->side.'_match_time'))){
                            $etItem->time = $request->input('semifinal_'.$etItem->side.'_match_time');
                        }else{
                            $etItem->time = null;
                        }

                        if(!empty($request->input('semifinal_'.$etItem->side.'_match_create'))){
                            $create_match[] = array('slug' => 'semifinal_', 'model' => $etItem);
                        }

                    }else if($etItem->level == 2){
                        $etItem->team1_id = $request->input('quarterfinal_'.$etItem->side.'_team1');
                        $etItem->team2_id = $request->input('quarterfinal_'.$etItem->side.'_team2');
                        
                        if(!empty($request->input('quarterfinal_'.$etItem->side.'_match_date'))){
                            $etItem->date = Carbon::parse($request->input('quarterfinal_'.$etItem->side.'_match_date'))->format('Y-m-d');
                        }else{
                            $etItem->date = null;
                        }
                        
                        if(!empty($request->input('quarterfinal_'.$etItem->side.'_match_time'))){
                            $etItem->time = $request->input('quarterfinal_'.$etItem->side.'_match_time');
                        }else{
                            $etItem->time = null;
                        }

                        if(!empty($request->input('quarterfinal_'.$etItem->side.'_match_create'))){
                            $create_match[] = array('slug' => 'quarterfinal_', 'model' => $etItem);
                        }

                    }else if($etItem->level == 3){
                        $etItem->team1_id = $request->input('tour1_'.$etItem->side.'_team1');
                        $etItem->team2_id = $request->input('tour1_'.$etItem->side.'_team2');
                        
                        if(!empty($request->input('tour1_'.$etItem->side.'_match_date'))){
                            $etItem->date = Carbon::parse($request->input('tour1_'.$etItem->side.'_match_date'))->format('Y-m-d');
                        }else{
                            $etItem->date = null;
                        }
                        
                        if(!empty($request->input('tour1_'.$etItem->side.'_match_time'))){
                            $etItem->time = $request->input('tour1_'.$etItem->side.'_match_time');
                        }else{
                            $etItem->time = null;
                        }

                        if(!empty($request->input('tour1_'.$etItem->side.'_match_create'))){
                            $create_match[] = array('slug' => 'tour1_'.$etItem->side.'_', 'model' => $etItem);
                        }

                    }else if($etItem->level == 4){
                        $etItem->team1_id = $request->input('tour2_'.$etItem->side.'_team1');
                        $etItem->team2_id = $request->input('tour2_'.$etItem->side.'_team2');
                        
                        if(!empty($request->input('tour2_'.$etItem->side.'_match_date'))){
                            $etItem->date = Carbon::parse($request->input('tour2_'.$etItem->side.'_match_date'))->format('Y-m-d');
                        }else{
                            $etItem->date = null;
                        }
                        
                        if(!empty($request->input('tour2_'.$etItem->side.'_match_time'))){
                            $etItem->time = $request->input('tour2_'.$etItem->side.'_match_time');
                        }else{
                            $etItem->time = null;
                        }

                        if(!empty($request->input('tour2_'.$etItem->side.'_match_create'))){
                            $create_match[] = array('slug' => 'tour2_'.$etItem->side.'_', 'model' => $etItem);
                        }

                    }else if($etItem->level == 5){
                        $etItem->team1_id = $request->input('tour3_'.$etItem->side.'_team1');
                        $etItem->team2_id = $request->input('tour3_'.$etItem->side.'_team2');
                        
                        if(!empty($request->input('tour3_'.$etItem->side.'_match_date'))){
                            $etItem->date = Carbon::parse($request->input('tour3_'.$etItem->side.'_match_date'))->format('Y-m-d');
                        }else{
                            $etItem->date = null;
                        }
                        
                        if(!empty($request->input('tour3_'.$etItem->side.'_match_time'))){
                            $etItem->time = $request->input('tour3_'.$etItem->side.'_match_time');
                        }else{
                            $etItem->time = null;
                        }

                        if(!empty($request->input('tour3_'.$etItem->side.'_match_create'))){
                            $create_match[] = array('slug' => 'tour3_'.$etItem->side.'_', 'model' => $etItem);
                        }

                    }else if($etItem->level == 6){
                        
                        $etItem->team1_id = $request->input('tour4_'.$etItem->side.'_team1');
                        $etItem->team2_id = $request->input('tour4_'.$etItem->side.'_team2');
                        
                        if(!empty($request->input('tour4_'.$etItem->side.'_match_date'))){
                            $etItem->date = Carbon::parse($request->input('tour4_'.$etItem->side.'_match_date'))->format('Y-m-d');
                        }else{
                            $etItem->date = null;
                        }
                        
                        if(!empty($request->input('tour4_'.$etItem->side.'_match_time'))){
                            $etItem->time = $request->input('tour4_'.$etItem->side.'_match_time');
                        }else{
                            $etItem->time = null;
                        }

                        if(!empty($request->input('tour4_'.$etItem->side.'_match_create'))){
                            $create_match[] = array('slug' => 'tour4_'.$etItem->side.'_', 'model' => $etItem);
                        }

                    }
                }

                $etItem->save();

                $text = ' Eleme Ağacı Başarıyla Güncellendi...';
            }

            //dd($create_match);
            //dd($request->input());
            //dump($model->league_eliminationseason->season_ptr->polymorphic_ctype_id);
            
            $season = $model->league_eliminationseason->season_ptr;
            foreach ($create_match as $key => $value) {
                //dump($value['slug']);
                $_eliminationTree = $value['model'];

                if(!empty($_eliminationTree->team1_id) && !empty($_eliminationTree->team2_id) && !empty($_eliminationTree->date) && !empty($_eliminationTree->time)){

                    $match = new Match();
                    $match->season_id = $season->id;
                    $match->team1_id = $_eliminationTree->team1_id;
                    $match->team2_id = $_eliminationTree->team2_id;
                    $match->date = $_eliminationTree->date;
                    $match->time = $_eliminationTree->time;

                    $match->team1_goal = 0;
                    $match->team2_goal = 0;
                    $match->team1_point = 0;
                    $match->team2_point = 0;

                    $match->completed = false;
                    $match->save();

                    $min_point = 10;

                    $teamSeason1 = TeamTeamSeason::where('season_id', $season->id)->where('team_id', $_eliminationTree->team1_id)->first();
                    if(empty($teamSeason1)){

                        $teamSeason = new TeamTeamSeason();

                        if($season->polymorphic_ctype_id == 32){ /*elimination*/
                            $teamSeason->squad_locked = true;
                        }else{
                            $teamSeason->squad_locked = false;
                        }
                            
                        $teamSeason->point = $min_point;
                        $teamSeason->season_id = $season->id;
                        $teamSeason->team_id = $_eliminationTree->team1_id;

                        $teamSeason->remaining_transfer_count = $season->allowed_transfer_count;
                        $teamSeason->playoff_member = true;
                        $teamSeason->drawn = 0;
                        $teamSeason->goal_against = 0;
                        $teamSeason->goal_for = 0;
                        $teamSeason->lost = 0;
                        $teamSeason->match_total = 0;
                        $teamSeason->won = 0;
                        $teamSeason->gk_save = 0;
                        $teamSeason->red_card = 0;
                        $teamSeason->yellow_card = 0;

                        $teamSeason->save();

                    }

                    $teamSeason2 = TeamTeamSeason::where('season_id', $season->id)->where('team_id', $_eliminationTree->team2_id)->first();
                    if(empty($teamSeason2)){

                        $teamSeason = new TeamTeamSeason();

                        if($season->polymorphic_ctype_id == 32){ /*elimination*/
                            $teamSeason->squad_locked = true;
                        }else{
                            $teamSeason->squad_locked = false;
                        }
                            
                        $teamSeason->point = $min_point;
                        $teamSeason->season_id = $season->id;
                        $teamSeason->team_id = $_eliminationTree->team2_id;

                        $teamSeason->remaining_transfer_count = $season->allowed_transfer_count;
                        $teamSeason->playoff_member = true;
                        $teamSeason->drawn = 0;
                        $teamSeason->goal_against = 0;
                        $teamSeason->goal_for = 0;
                        $teamSeason->lost = 0;
                        $teamSeason->match_total = 0;
                        $teamSeason->won = 0;
                        $teamSeason->gk_save = 0;
                        $teamSeason->red_card = 0;
                        $teamSeason->yellow_card = 0;

                        $teamSeason->save();

                    }

                    $_eliminationTree->match_id = $match->id;
                    $_eliminationTree->save();

                    $text .= ' İşaretlenmiş Maçlar Başarıyla Oluşturuldu...';

                }else{

                    $text = 'Maç Oluşturulamadı. Eksik Bilgileri Tamamlayınız...';
                    return redirect($request->segment.'/'.$request->crud.'/'.$request->id)->with('message', array('text' => $text, 'status' => 'error'));

                }
            
            }

        }

        if(!empty($request->tree_id)){
            return redirect($request->segment.'?tree_id='.$request->tree_id)->with('message', array('text' => $text, 'status' => 'success'));
        }else{
            return redirect($request->segment)->with('message', array('text' => $text, 'status' => 'success'));
        }
    }




    public function player_position()
    {
        $text = "Bu sayfaya ulaşmak için yetkiniz bulunmuyor!";
        if(request()->segment(1) == 'player_positions' && !Auth::user()->hasPermissionTo('view_position')){
            return redirect('/dashboard')->with('message', array('text' => $text, 'status' => 'error'));
        }
        return view('dashboard');
    }

    public function player_position_crud($id = null)
    {
        $text = "Bu sayfaya ulaşmak için yetkiniz bulunmuyor!";
        if(
            (request()->segment(2) == 'edit' && !Auth::user()->hasPermissionTo('change_position')) ||
            (request()->segment(2) == 'delete' && !Auth::user()->hasPermissionTo('delete_position'))
        ){
            return redirect('/player_position')->with('message', array('text' => $text, 'status' => 'error'));
        }
        $player_position = NULL;
        if(!is_null($id)){
            $player_position = PlayerPosition::find($id);
        }
        return view('dashboard', array('model_data' => $player_position));
    }

    public function player_position_save(Request $request)
    {
        //dd($request->input());
        $text = "";
        if ($request->crud == 'delete') {

            $model = PlayerPosition::find($request->id);

            if(empty($model->player) && empty($model->player2) && empty($model->player3)){
                $model->delete();
                $text = 'Başarıyla Silindi...';
            }else{
                $text = 'Bu pozisyon en az bir oyuncuya atandığı için silinemedi!';
                return \Redirect::back()->withInput()->with('message', array('text' => $text, 'status' => 'error'));
            }

        }else{

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $model = new PlayerPosition();
            }else if($request->crud == 'edit'){
                $model = PlayerPosition::find($request->id);
            }

            $rules = array();
            $formConfig = config('forms.'.$request->segment);

            if(!is_null($formConfig)){
                foreach ($formConfig as $key => $value){
                    if(!empty(data_get($value, 'validation'))){
                        $rules[$key] = data_get($value, 'validation');
                    }
                }
            }
            
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
               return \Redirect::back()->withErrors($validator)->withInput();
            }

            $model->name = $request->name;
            $model->code = $request->code;
            $model->order = $request->order;
            $model->save();

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $text = 'Başarıyla Eklendi...';
            }else if($request->crud == 'edit'){
                $text = 'Başarıyla Güncellendi...';
            }
        }

        return redirect($request->segment)->with('message', array('text' => $text, 'status' => 'success'));
    }


    public function point_type()
    {
        $text = "Bu sayfaya ulaşmak için yetkiniz bulunmuyor!";
        if(request()->segment(1) == 'point_types' && !Auth::user()->hasPermissionTo('view_pointtype')){
            return redirect('/dashboard')->with('message', array('text' => $text, 'status' => 'error'));
        }
        return view('dashboard');
    }

    public function point_type_crud($id = null)
    {
        $text = "Bu sayfaya ulaşmak için yetkiniz bulunmuyor!";
        if(
            (request()->segment(2) == 'edit' && !Auth::user()->hasPermissionTo('change_pointtype')) ||
            (request()->segment(2) == 'delete' && !Auth::user()->hasPermissionTo('delete_pointtype'))
        ){
            return redirect('/point_type')->with('message', array('text' => $text, 'status' => 'error'));
        }
        $point_type = NULL;
        if(!is_null($id)){
            $point_type = PointType::find($id);
        }
        return view('dashboard', array('model_data' => $point_type));
    }

    public function point_type_save(Request $request)
    {
        //dd($request->input());
        $text = "";
        if ($request->crud == 'delete') {

            $model = PointType::find($request->id);

            $model->delete();
            $text = 'Başarıyla Silindi...';
        }else{

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $model = new PointType();
            }else if($request->crud == 'edit'){
                $model = PointType::find($request->id);
            }

            $rules = array();
            $formConfig = config('forms.'.$request->segment);

            if(!is_null($formConfig)){
                foreach ($formConfig as $key => $value){
                    if(!empty(data_get($value, 'validation'))){
                        $rules[$key] = data_get($value, 'validation');
                    }
                }
            }
            
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
               return \Redirect::back()->withErrors($validator)->withInput();
            }

            $model->name = $request->name;
            $model->active = (!empty($request->active)) ? true : false;
            $model->description = $request->description;
            $model->min_point = $request->min_point;
            $model->max_point = $request->max_point;
            $model->win_score = $request->win_score;
            $model->defeat_score = $request->defeat_score;
            $model->draw_score = $request->draw_score;
            $model->color = $request->color;
            $model->save();

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $text = 'Başarıyla Eklendi...';
            }else if($request->crud == 'edit'){
                $text = 'Başarıyla Güncellendi...';
            }
        }

        return redirect($request->segment)->with('message', array('text' => $text, 'status' => 'success'));
    }


    public function tactic()
    {
        $text = "Bu sayfaya ulaşmak için yetkiniz bulunmuyor!";
        if(request()->segment(1) == 'tactics' && !Auth::user()->hasPermissionTo('view_tactic')){
            return redirect('/dashboard')->with('message', array('text' => $text, 'status' => 'error'));
        }
        return view('dashboard');
    }

    public function tactic_crud($id = null)
    {
        $text = "Bu sayfaya ulaşmak için yetkiniz bulunmuyor!";
        if(
            (request()->segment(2) == 'edit' && !Auth::user()->hasPermissionTo('change_tactic')) ||
            (request()->segment(2) == 'delete' && !Auth::user()->hasPermissionTo('delete_tactic'))
        ){
            return redirect('/tactic')->with('message', array('text' => $text, 'status' => 'error'));
        }
        $tactic = NULL;
        if(!is_null($id)){
            $tactic = Tactic::find($id);
        }
        return view('dashboard', array('model_data' => $tactic));
    }

    public function tactic_save(Request $request)
    {
        //dd($request->input());
        $text = "";
        if ($request->crud == 'delete') {

            $model = Tactic::find($request->id);

            if($model->team->count() == 0){
                $model->delete();
                $text = 'Başarıyla Silindi...';
            }else{
                $text = 'Bu taktik en az bir takıma atandığı için silinemedi!';
                return \Redirect::back()->withInput()->with('message', array('text' => $text, 'status' => 'error'));
            }

            $text = 'Başarıyla Silindi...';
        }else{

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $model = new Tactic();
            }else if($request->crud == 'edit'){
                $model = Tactic::find($request->id);
            }

            $rules = array();
            $formConfig = config('forms.'.$request->segment);

            if(!is_null($formConfig)){
                foreach ($formConfig as $key => $value){
                    if(!empty(data_get($value, 'validation'))){
                        $rules[$key] = data_get($value, 'validation');
                    }
                }
            }
            
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
               return \Redirect::back()->withErrors($validator)->withInput();
            }

            $model->name = $request->name;
            $model->save();

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $text = 'Başarıyla Eklendi...';
            }else if($request->crud == 'edit'){
                $text = 'Başarıyla Güncellendi...';
            }
        }

        return redirect($request->segment)->with('message', array('text' => $text, 'status' => 'success'));
    }


    public function team_potential()
    {
        $text = "Bu sayfaya ulaşmak için yetkiniz bulunmuyor!";
        if(request()->segment(1) == 'team_potentials' && !Auth::user()->hasPermissionTo('view_potential')){
            return redirect('/dashboard')->with('message', array('text' => $text, 'status' => 'error'));
        }
        return view('dashboard');
    }

    public function team_potential_crud($id = null)
    {
        $text = "Bu sayfaya ulaşmak için yetkiniz bulunmuyor!";
        if(
            (request()->segment(2) == 'edit' && !Auth::user()->hasPermissionTo('change_potential')) ||
            (request()->segment(2) == 'delete' && !Auth::user()->hasPermissionTo('delete_potential'))
        ){
            return redirect('/team_potential')->with('message', array('text' => $text, 'status' => 'error'));
        }
        $team_potential = NULL;
        if(!is_null($id)){
            $team_potential = TeamPotential::find($id);
        }
        return view('dashboard', array('model_data' => $team_potential));
    }

    public function team_potential_save(Request $request)
    {
        //dd($request->input());
        $text = "";
        if ($request->crud == 'delete') {

            $model = TeamPotential::find($request->id);

            if(empty($model->team_survey)){
                $model->delete();
                $text = 'Başarıyla Silindi...';
            }else{
                $text = 'Bu takım potansiyeli en az bir takıma atandığı için silinemedi!';
                return \Redirect::back()->withInput()->with('message', array('text' => $text, 'status' => 'error'));
            }

            $text = 'Başarıyla Silindi...';
        }else{

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $model = new TeamPotential();
            }else if($request->crud == 'edit'){
                $model = TeamPotential::find($request->id);
            }

            $rules = array();
            $formConfig = config('forms.'.$request->segment);

            if(!is_null($formConfig)){
                foreach ($formConfig as $key => $value){
                    if(!empty(data_get($value, 'validation'))){
                        $rules[$key] = data_get($value, 'validation');
                    }
                }
            }
            
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
               return \Redirect::back()->withErrors($validator)->withInput();
            }

            $model->name = $request->name;
            $model->save();

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $text = 'Başarıyla Eklendi...';
            }else if($request->crud == 'edit'){
                $text = 'Başarıyla Güncellendi...';
            }
        }

        return redirect($request->segment)->with('message', array('text' => $text, 'status' => 'success'));
    }


    public function team_satisfaction()
    {
        $text = "Bu sayfaya ulaşmak için yetkiniz bulunmuyor!";
        if(request()->segment(1) == 'team_satisfactions' && !Auth::user()->hasPermissionTo('view_satisfaction')){
            return redirect('/dashboard')->with('message', array('text' => $text, 'status' => 'error'));
        }
        return view('dashboard');
    }

    public function team_satisfaction_crud($id = null)
    {
        $text = "Bu sayfaya ulaşmak için yetkiniz bulunmuyor!";
        if(
            (request()->segment(2) == 'edit' && !Auth::user()->hasPermissionTo('change_satisfaction')) ||
            (request()->segment(2) == 'delete' && !Auth::user()->hasPermissionTo('delete_satisfaction'))
        ){
            return redirect('/team_satisfaction')->with('message', array('text' => $text, 'status' => 'error'));
        }
        $team_satisfaction = NULL;
        if(!is_null($id)){
            $team_satisfaction = TeamSatisfaction::find($id);
        }
        return view('dashboard', array('model_data' => $team_satisfaction));
    }

    public function team_satisfaction_save(Request $request)
    {
        //dd($request->input());
        $text = "";
        if ($request->crud == 'delete') {

            $model = TeamSatisfaction::find($request->id);

            if(empty($model->team_survey)){
                $model->delete();
                $text = 'Başarıyla Silindi...';
            }else{
                $text = 'Bu takım potansiyeli en az bir takıma atandığı için silinemedi!';
                return \Redirect::back()->withInput()->with('message', array('text' => $text, 'status' => 'error'));
            }

            $text = 'Başarıyla Silindi...';
        }else{

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $model = new TeamSatisfaction();
            }else if($request->crud == 'edit'){
                $model = TeamSatisfaction::find($request->id);
            }

            $rules = array();
            $formConfig = config('forms.'.$request->segment);

            if(!is_null($formConfig)){
                foreach ($formConfig as $key => $value){
                    if(!empty(data_get($value, 'validation'))){
                        $rules[$key] = data_get($value, 'validation');
                    }
                }
            }
            
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
               return \Redirect::back()->withErrors($validator)->withInput();
            }

            $model->name = $request->name;
            $model->save();

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $text = 'Başarıyla Eklendi...';
            }else if($request->crud == 'edit'){
                $text = 'Başarıyla Güncellendi...';
            }
        }

        return redirect($request->segment)->with('message', array('text' => $text, 'status' => 'success'));
    }


    public function team_satisfaction_jogo()
    {
        $text = "Bu sayfaya ulaşmak için yetkiniz bulunmuyor!";
        if(request()->segment(1) == 'team_satisfactions_jogo' && !Auth::user()->hasPermissionTo('view_satisfaction_jogo')){
            return redirect('/dashboard')->with('message', array('text' => $text, 'status' => 'error'));
        }
        return view('dashboard');
    }

    public function team_satisfaction_jogo_crud($id = null)
    {
        $text = "Bu sayfaya ulaşmak için yetkiniz bulunmuyor!";
        if(
            (request()->segment(2) == 'edit' && !Auth::user()->hasPermissionTo('change_satisfaction_jogo')) ||
            (request()->segment(2) == 'delete' && !Auth::user()->hasPermissionTo('delete_satisfaction_jogo'))
        ){
            return redirect('/team_satisfaction_jogo')->with('message', array('text' => $text, 'status' => 'error'));
        }
        $team_satisfaction_jogo = NULL;
        if(!is_null($id)){
            $team_satisfaction_jogo = TeamSatisfactionJogo::find($id);
        }
        return view('dashboard', array('model_data' => $team_satisfaction_jogo));
    }

    public function team_satisfaction_jogo_save(Request $request)
    {
        //dd($request->input());
        $text = "";
        if ($request->crud == 'delete') {

            $model = TeamSatisfactionJogo::find($request->id);

            if(empty($model->team_survey)){
                $model->delete();
                $text = 'Başarıyla Silindi...';
            }else{
                $text = 'Bu takım potansiyeli en az bir takıma atandığı için silinemedi!';
                return \Redirect::back()->withInput()->with('message', array('text' => $text, 'status' => 'error'));
            }

            $text = 'Başarıyla Silindi...';
        }else{

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $model = new TeamSatisfactionJogo();
            }else if($request->crud == 'edit'){
                $model = TeamSatisfactionJogo::find($request->id);
            }

            $rules = array();
            $formConfig = config('forms.'.$request->segment);

            if(!is_null($formConfig)){
                foreach ($formConfig as $key => $value){
                    if(!empty(data_get($value, 'validation'))){
                        $rules[$key] = data_get($value, 'validation');
                    }
                }
            }
            
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
               return \Redirect::back()->withErrors($validator)->withInput();
            }

            $model->name = $request->name;
            $model->save();

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $text = 'Başarıyla Eklendi...';
            }else if($request->crud == 'edit'){
                $text = 'Başarıyla Güncellendi...';
            }
        }

        return redirect($request->segment)->with('message', array('text' => $text, 'status' => 'success'));
    }

}
