<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Validator;

use Carbon\Carbon;
use App\User;

use App\League;
use App\LeaguesProvince;
use App\LeagueGalleries;
use App\LeagueGalleryItems;


class LeagueController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	return view('dashboard');
    }

    public function crud($id = null)
    {
        $league = NULL;
        if(!is_null($id)){
            $league = League::find($id);
        }
        return view('dashboard', array('model_data' => $league));
    }

    public function save(Request $request)
    {
        //dd($request->input());
        $text = "";
        if ($request->crud == 'delete') {

            $model = League::find($request->id);

            foreach ($model->league_province as $value) {
                $value->delete();
            }

            $model->delete();
            $text = 'Başarıyla Silindi...';

        }else{

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $model = new League();
            }else if($request->crud == 'edit'){
                $model = League::find($request->id);
            }

            $rules = array();
            $formConfig = config('forms.'.$request->segment);

            if(!is_null($formConfig)){
                foreach ($formConfig as $key => $value){
                    if(!empty(data_get($value, 'validation'))){
                        $rules[$key] = data_get($value, 'validation');
                    }
                }
            }
            
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
               return \Redirect::back()->withErrors($validator)->withInput();
            }

            $model->name = $request->name;
            $model->facebook = $request->facebook;
            $model->twitter = $request->twitter;
            $model->instagram = $request->instagram;
            $model->active = (!empty($request->active)) ? true : false;
            $model->save();

             /* EDIT */
            if($request->crud == 'edit'){
                foreach ($model->league_province as $value_lp) {
                    $value_lp->delete();
                }
            }

            if(!empty($request->province)){
                foreach ($request->province as $value_lp) {
                    if(!empty($value_lp)){
                        $lp = new LeaguesProvince();
                        $lp->league_id = $model->id;
                        $lp->province_id = $value_lp;
                        $lp->save();
                    }
                }
            }

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $text = 'Başarıyla Eklendi...';
            }else if($request->crud == 'edit'){
                $text = 'Başarıyla Güncellendi...';
            }
        }

        return redirect($request->segment)->with('message', array('text' => $text, 'status' => 'success'));
    }

    public function league_gallery()
    {
        return view('dashboard');
    }

    public function crud_league_gallery($id = null)
    {
        $league_gallery = NULL;
        if(!is_null($id)){
            $league_gallery = LeagueGalleries::find($id);
        }
        return view('dashboard', array('model_data' => $league_gallery));
    }

    public function save_league_gallery(Request $request)
    {
        //dd($request->input());
        $text = "";
        if ($request->crud == 'delete') {

            $model = LeagueGalleries::find($request->id);

            /*
            foreach ($model->league_province as $value) {
                $value->delete();
            }
            */

            $model->delete();
            $text = 'Başarıyla Silindi...';

        }else{

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $model = new LeagueGalleries();
            }else if($request->crud == 'edit'){
                $model = LeagueGalleries::find($request->id);
            }

            $rules = array();
            $formConfig = config('forms.'.$request->segment);

            if(!is_null($formConfig)){
                foreach ($formConfig as $key => $value){
                    if(!empty(data_get($value, 'validation'))){
                        $rules[$key] = data_get($value, 'validation');
                    }
                }
            }
            
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
               return \Redirect::back()->withErrors($validator)->withInput();
            }

            $model->league_id = $request->league_id;
            $model->name = $request->name;
            $model->order = $request->order;
            $model->show_home = (!empty($request->show_home)) ? true : false;
            $model->save();

            /* EDIT */
            /*
            if($request->crud == 'edit'){
                foreach ($model->league_province as $value_lp) {
                    $value_lp->delete();
                }
            }

            if(!empty($request->province)){
                foreach ($request->province as $value_lp) {
                    if(!empty($value_lp)){
                        $lp = new LeaguesProvince();
                        $lp->league_id = $model->id;
                        $lp->province_id = $value_lp;
                        $lp->save();
                    }
                }
            }
            */

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $text = 'Başarıyla Eklendi...';
            }else if($request->crud == 'edit'){
                $text = 'Başarıyla Güncellendi...';
            }
        }

        return redirect($request->segment.'?league_id='.$request->league_id)->with('message', array('text' => $text, 'status' => 'success'));
    }

    public function save_league_gallery_items(Request $request)
    {
        //dd($request->input());
        $text = "";
        if ($request->crud == 'delete') {

        }else{
            /* ADD - EDIT */
            if(empty($request->league_gallery_items_id)){
                $model = new LeagueGalleryItems();
            }else{
                $model = LeagueGalleryItems::find($request->league_gallery_items_id);
            }

            $rules = array();
            $formConfig = config('forms.league_gallery_items');

            if(!is_null($formConfig)){
                foreach ($formConfig as $key => $value){
                    if(!empty(data_get($value, 'validation'))){
                        $rules[$key] = data_get($value, 'validation');
                    }
                }
            }
            
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
               return \Redirect::back()->withErrors($validator)->withInput();
            }

            $model->gallery_id = $request->id;
            $model->embed_source = $request->embed_source;
            $model->embed_code = $request->embed_code;
            $model->description = $request->description;

            $model->save();


            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $text = 'Başarıyla Eklendi...';
            }else if($request->crud == 'edit'){
                $text = 'Başarıyla Güncellendi...';
            }
        }

        return redirect($request->segment.'/'.$request->crud.'/'.$request->id.'?t=tab_02')->with('message', array('text' => $text, 'status' => 'success'));
    }

    public function delete_league_gallery_items($id, $lgiid)
    {
        //dd($id.' - '.$maid);
        $model = LeagueGalleryItems::find($lgiid);
        if(!empty($model)){
            $model->delete();
            $text = 'Başarıyla Silindi...';
            return redirect('league_gallery/edit/'.$id.'?t=tab_02')->with('message', array('text' => $text, 'status' => 'success'));
        }
    }

}