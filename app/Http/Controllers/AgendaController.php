<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Validator;

use Carbon\Carbon;
use App\User;

use App\Agenda;

class AgendaController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('dashboard');
    }

    public function crud($id = null)
    {
        $agenda = NULL;
        if(!is_null($id)){
            $agenda = Agenda::find($id);
        }
        return view('dashboard', array('model_data' => $agenda));
    }

    public function save(Request $request)
    {
        // dd($request->input());
        $text = "";
        if ($request->crud == 'delete') {

            $model = Agenda::find($request->id);

            $model->delete();
            $text = 'Başarıyla Silindi...';

        }else{

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $model = new Agenda();
            }else if($request->crud == 'edit'){
                $model = Agenda::find($request->id);
            }

            $rules = array();
            $formConfig = config('forms.'.$request->segment);

            if(!is_null($formConfig)){
                foreach ($formConfig as $key => $value){
                    if(!empty(data_get($value, 'validation'))){
                        $rules[$key] = data_get($value, 'validation');
                    }
                }
            }
            
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
               return \Redirect::back()->withErrors($validator)->withInput();
            }

            $model->name = $request->name;
            $model->slug = $request->slug;
            $model->link = $request->link;
            $model->save();


            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $text = 'Başarıyla Eklendi...';
            }else if($request->crud == 'edit'){
                $text = 'Başarıyla Güncellendi...';
            }
        }

        return redirect($request->segment)->with('message', array('text' => $text, 'status' => 'success'));
    }

    public function agendaPhotoDelete($id) {
        $agenda = Agenda::find($id);
        $agenda->image = null;
        $agenda->save();

        $text = 'Gündem Fotoğrafı Başarıyla Silindi...';
        return redirect('agenda/edit/'.$id.'?t=tab_02')->with('message', array('text' => $text, 'status' => 'success'));
    }

}
