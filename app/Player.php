<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Player extends Model
{
    protected $table = 'players';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;


    public function players_position()
    {
        return $this->hasOne('App\PlayerPosition', 'id', 'position_id');
    }

    public function players_position2()
    {
        return $this->hasOne('App\PlayerPosition', 'id', 'position2_id');
    }

    public function players_position3()
    {
        return $this->hasOne('App\PlayerPosition', 'id', 'position3_id');
    }

    public function team()
    {
        return $this->hasOne('App\Team', 'id', 'team_id');
    }

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }


	//public.match_player.match_player_player_id_76c08d05_fk_players_id
    public function match_player()
    {
        return $this->hasMany('App\MatchPlayer', 'player_id', 'id')->orderBy('id', 'desc');
    }

	//public.player_penalties.player_penalties_player_id_66d726bc_fk_players_id


	//public.player_values.player_values_player_id_4726359f_fk_players_id
    public function player_value()
    {
        return $this->hasMany('App\PlayerValue', 'player_id', 'id');
    }


	//public.reservation_offer.reservation_offer_player_id_b6d05d0b_fk_players_id


	//public.reservations.reservations_captain_id_9a3342a2_fk_players_id


	//public.team_playerteamhistory.team_playerteamhistory_player_id_069e0282_fk_players_id
    public function team_playerteamhistory()
    {
        return $this->hasMany('App\TeamPlayerTeamHistory', 'player_id', 'id')->orderBy('season_id', 'desc');
    }

	//public.team_teamplayer.team_teamplayer_player_id_ce0badb2_fk_players_id
    public function team_teamplayer()
    {
        return $this->hasMany('App\TeamTeamPlayer', 'player_id', 'id')->whereNull('released_date');
    }


	//public.team_teamseason.team_teamseason_squad_locked_player_id_d24c4ce7_fk_players_id


	//public.teams.teams_captain_id_d742b9f7_fk_players_id


	//public.transfers.transfers_requested_player_id_5c5741cf_fk_players_id
    public function transfers_requested()
    {
        return $this->hasMany('App\Transfer', 'requested_player_id', 'id')->where('status', 'pending');
    }

	//public.transfers.transfers_requesting_captain_id_78bb8e63_fk_players_id
    public function transfers_requesting()
    {
        return $this->hasMany('App\Transfer', 'requesting_captain_id', 'id');
    }

    
    /*
    //***************
    //Get Date Format
    public function getCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    //***************
    //Set Date Format
    public function setCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function setUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }
    */
    
}