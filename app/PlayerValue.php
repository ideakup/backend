<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class PlayerValue extends Model
{
    protected $table = 'player_values';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;


    //public.player_values.player_values_player_id_4726359f_fk_players_id
	public function player()
    {
        return $this->belongsTo('App\Player', 'player_id', 'id');
    }

	//public.player_values.player_values_season_id_44f1a140_fk_seasons_id
	public function season()
    {
        return $this->belongsTo('App\Season', 'season_id', 'id');
    }

    
    /*
    //***************
    //Get Date Format
    public function getCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    //***************
    //Set Date Format
    public function setCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function setUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }
    */
    
}