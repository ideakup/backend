<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class TeamSurvey extends Model
{
    protected $table = 'team_surveys';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;

	public function team_potential()
    {
        return $this->hasOne('App\TeamPotential', 'id', 'potential_id');
    }

	public function team_satisfaction()
    {
        return $this->hasOne('App\TeamSatisfaction', 'id', 'satisfaction_id');
    }

    public function team_satisfaction_jogo()
    {
        return $this->hasOne('App\TeamSatisfactionJogo', 'id', 'satisfaction_jogo_id');
    }

	public function team()
    {
        return $this->hasOne('App\Team', 'id', 'team_id');
    }

    
    /*
    //***************
    //Get Date Format
    public function getCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    //***************
    //Set Date Format
    public function setCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function setUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }
    */
    
}