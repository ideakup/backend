<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class EliminationTreeItem extends Model
{
    protected $table = 'eliminationtree_items';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;

	//public.eliminationtree_items.eliminationtree_items_match_id_059193c8_fk_matches_id
    public function match()
    {
        return $this->hasOne('App\Match', 'id', 'match_id');
    }

	//public.eliminationtree_items.eliminationtree_items_team1_id_4a24d842_fk_teams_id
    public function team1()
    {
        return $this->hasOne('App\Team', 'id', 'team1_id');
    }

	//public.eliminationtree_items.eliminationtree_items_team2_id_44b0732c_fk_teams_id
    public function team2()
    {
        return $this->hasOne('App\Team', 'id', 'team2_id');
    }

	//public.eliminationtree_items.eliminationtree_items_tree_id_f13db7cf_fk_eliminaton_trees_id
    public function elimination_tree()
    {
        return $this->hasOne('App\EliminationTree', 'id', 'tree_id');
    }

    
    /*
    //***************
    //Get Date Format
    public function getCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    //***************
    //Set Date Format
    public function setCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function setUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }
    */
    
}