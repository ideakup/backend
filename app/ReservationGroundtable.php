<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class ReservationGroundtable extends Model
{
    protected $table = 'reservation_groundtable';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;

    //	public.reservation_groundtable.reservation_groundtable_ground_id_ca812d2d_fk_grounds_id	auto
    public function ground()
    {
        return $this->hasOne('App\Ground', 'id', 'ground_id');
    }

	//	public.reservations.reservations_ground_table_id_3c3e5865_fk_reservati	normal
    public function reservation()
    {
        return $this->hasMany('App\Reservation', 'ground_table_id', 'id');
    }

    
    /*
    //***************
    //Get Date Format
    public function getCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    //***************
    //Set Date Format
    public function setCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function setUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }
    */
    
}