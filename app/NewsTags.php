<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class NewsTags extends Model
{
    protected $table = 'news_tags';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;

	//public.news_tags.news_tags_news_id_06acc240_fk_news_id	auto

	//public.news_tags.news_tags_tag_id_b25e7549_fk_tags_id	auto

}