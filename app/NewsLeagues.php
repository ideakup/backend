<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class NewsLeagues extends Model
{
    protected $table = 'news_leagues';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;

	//public.news_leagues.news_leagues_league_id_e34d6430_fk_leagues_id
	public function league()
    {
        return $this->belongsToMany('App\League', 'id', 'league_id');
    }

	//public.news_leagues.news_leagues_news_id_9fc102a9_fk_news_id
	public function news()
    {
        return $this->belongsToMany('App\News', 'id', 'news_id');
    }

}