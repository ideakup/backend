<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class PressConferencesPlayers extends Model
{
    protected $table = 'press_conferences_players';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;

    public function press_conference()
    {
        return $this->hasOne('App\PressConferences', 'id', 'pressconference_id');
    }

}