<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Group extends Model
{
    protected $table = 'groups';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;


    // public.groups.groups_season_id_ac90fa96_fk_league_fixtureseason_season_ptr_id   auto
    public function league_fixtureseason()
    {
        return $this->hasOne('App\LeagueFixtureSeason', 'season_ptr_id', 'season_id');
    }

    // public.groups_teams.groups_teams_group_id_365c9e7b_fk_groups_id normal
    public function group_teams()
    {
        return $this->hasMany('App\GroupTeam', 'group_id', 'id');
    }

    
    /*
    //***************
    //Get Date Format
    public function getCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    //***************
    //Set Date Format
    public function setCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function setUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }
    */
    
}