<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Penalty extends Model
{
    protected $table = 'penalties';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;

	//public.penalties.panorama_penal_league_id_3ecffddd_fk_leagues_id
	public function league()
    {
        return $this->hasOne('App\League', 'id', 'league_id');
    }

	//public.penalties.panorama_penal_match_id_3a1b4f65_fk_matches_id
	public function match()
    {
        return $this->hasOne('App\Match', 'id', 'match_id');
    }

	//public.penalties.panorama_penal_polymorphic_ctype_id_af1a93cb_fk_django_co
    public function ctype()
    {
        return $this->hasOne('App\DjangoContentType', 'id', 'polymorphic_ctype_id');
    }

	//public.penalties.panorama_penal_season_id_471b75c6_fk_seasons_id
	public function season()
    {
        return $this->hasOne('App\Season', 'id', 'season_id');
    }

	//public.player_penalties.panorama_penalplayer_penal_ptr_id_4902942a_fk_panorama_penal_id
    public function player_penalty()
    {
        return $this->hasOne('App\PlayerPenalty', 'penal_ptr_id', 'id');
    }

	//public.team_penalties.panorama_penalteam_penal_ptr_id_be54120c_fk_panorama_penal_id
	public function team_penalty()
    {
        return $this->hasOne('App\TeamPenalty', 'penal_ptr_id', 'id');
    }

    
    /*
    //***************
    //Get Date Format
    public function getCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    //***************
    //Set Date Format
    public function setCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function setUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }
    */
    
}
