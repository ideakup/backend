<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class PlayerPosition extends Model
{
    protected $table = 'player_positions';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;

    //public.match_player.match_player_position_id_32cdbc82_fk_player_positions_id


	//public.panorama_types_prize.panorama_types_prize_position_id_218fbe11_fk_player_po

    public function player()
    {
        return $this->hasMany('App\Player', 'position_id', 'id');
    }

    public function player2()
    {
        return $this->hasMany('App\Player', 'position2_id', 'id');
    }

    public function player3()
    {
        return $this->hasMany('App\Player', 'position3_id', 'id');
    }

    
    /*
    //***************
    //Get Date Format
    public function getCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    //***************
    //Set Date Format
    public function setCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function setUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }
    */
    
}