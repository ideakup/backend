<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class District extends Model
{
    protected $table = 'districts';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;

    public function province()
    {
        return $this->hasOne('App\Province', 'id', 'province_id');
    }

    public function conferences_district()
    {
        return $this->hasMany('App\ConferencesDistrict', 'district_id', 'id');
    }

    public function ground()
    {
        return $this->hasMany('App\Ground', 'district_id', 'id');
    }

    public function profile()
    {
        return $this->hasMany('App\Profile', 'district_id', 'id');
    }

    public function team()
    {
        return $this->hasMany('App\Team', 'district_id', 'id');
    }  

    
    /*
    //***************
    //Get Date Format
    public function getCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    //***************
    //Set Date Format
    public function setCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function setUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }
    */
    
    
}