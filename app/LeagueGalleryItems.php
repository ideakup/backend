<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class LeagueGalleryItems extends Model
{
    protected $table = 'league_gallery_items';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;

    //public.league_gallery_items.league_gallery_items_gallery_id_0ecc2863_fk_league_galleries_id
    public function league()
    {
        return $this->belongsTo('App\LeagueGalleries', 'gallery_id', 'id');
    }

    
    /*
    //***************
    //Get Date Format
    public function getCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    //***************
    //Set Date Format
    public function setCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }

    public function setUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:sO');
    }
    */
    
}