<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class LeagueEliminationSeason extends Model
{
    protected $table = 'league_eliminationseason';
    protected $primaryKey = 'season_ptr_id';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;

    //public.elimination_trees.eliminaton_trees_season_id_99ffb239_fk_league_el
    public function elimination_tree()
    {
        return $this->hasMany('App\EliminationTree', 'season_id', 'season_ptr_id');
    }

    public function elimination_tree_conference()
    {
        return $this->hasMany('App\EliminationTree', 'season_id', 'season_ptr_id')->where('type', 'conference');
    }

    public function elimination_tree_final()
    {
        return $this->hasMany('App\EliminationTree', 'season_id', 'season_ptr_id')->where('type', 'final');
    }

    public function elimination_tree_trfinal()
    {
        return $this->hasMany('App\EliminationTree', 'season_id', 'season_ptr_id')->where('type', 'trfinal');
    }

    //public.league_eliminationseason.league_eliminationseason_base_season_id_e33bd357_fk_seasons_id
    public function base_season()
    {
        return $this->hasOne('App\Season', 'id', 'base_season_id');
    }
    //public.league_eliminationseason.league_eliminationseason_season_ptr_id_9d2df1e2_fk_seasons_id
    public function base_seasonApi()
    {
        return $this->hasOne('App\Season', 'id', 'season_ptr_id');
    }
    public function season_ptr()
    {
        return $this->hasOne('App\Season', 'id', 'season_ptr_id');
    }

}