<?php

return [

    /*
    'accepted'                  => 'The :attribute must be accepted.',
    'custom' => [
        'attribute-name' => [
            'rule-name'         => 'custom-message',
        ],
    ],
    */

    'view'                      => 'Görüntüle',
    'add'                       => 'Ekle',
    'edit'                      => 'Düzenle',
    'delete'                    => 'Sil',



    'conferences'               => 'Konferanslar',
    'provinces'                 => 'Şehirler',
    'districts'                 => 'İlçeler',
    'grounds'                   => 'Halı Sahalar',
    'ground_properties'         => 'Halı Saha Özellikleri',
    'leagues'                   => 'Ligler',
    'seasons'                   => 'Sezonlar',
    'seasonsettings'            => ' ',




];
