<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

    <!--begin::Head-->
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

        <title>{{ config('app.name', 'Laravel') }}</title>
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!--begin::Fonts-->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
        <!--end::Fonts-->

        <!--begin::Page Vendors Styles(used by this page)-->
        <link href="{{ url('assets/plugins/custom/fullcalendar/fullcalendar.bundle.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('assets/plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css">
        <!--end::Page Vendors Styles-->

        <!--begin::Global Theme Styles(used by all pages)-->
        <link href="{{ url('assets/plugins/global/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('assets/plugins/custom/prismjs/prismjs.bundle.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
        <!--end::Global Theme Styles-->

        <!--begin::Layout Themes(used by all pages)-->
        <link href="{{ url('assets/css/themes/layout/header/base/light.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('assets/css/themes/layout/header/menu/light.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('assets/css/themes/layout/brand/dark.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('assets/css/themes/layout/aside/dark.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('js/2mundos-fengyuanchen-cropperjs/dist/cropper.css') }}" rel="stylesheet" type="text/css" />

        <!--end::Layout Themes-->
        <link rel="shortcut icon" href="{{ url('assets/media/logos/favicon.ico') }}" />

        <!-- Styles 
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">-->

    </head>

    <!--end::Head-->

    <!--begin::Body-->
    <body id="kt_body" class="header-fixed header-mobile-fixed subheader-enabled aside-enabled aside-fixed aside-minimize aside-minimize-hoverable page-loading">
        
        <!--[html-partial:include:{"file":"layout.html"}]/-->
        @yield('content')

        <!--[html-partial:include:{"file":"partials/_extras/scrolltop.html"}]/-->
        @include('partials._extras.scrolltop')

        <script>
            var HOST_URL = "{{ url('/') }}"; //"https://preview.keenthemes.com/metronic/theme/html/tools/preview";
        </script>

        <!--begin::Global Config(global config for global JS scripts)-->
        <script>
            var KTAppSettings = {
                "breakpoints": {
                    "sm": 576,
                    "md": 768,
                    "lg": 992,
                    "xl": 1200,
                    "xxl": 1400
                },
                "colors": {
                    "theme": {
                        "base": {
                            "white": "#ffffff",
                            "primary": "#3699FF",
                            "secondary": "#E5EAEE",
                            "success": "#1BC5BD",
                            "info": "#8950FC",
                            "warning": "#FFA800",
                            "danger": "#F64E60",
                            "light": "#E4E6EF",
                            "dark": "#181C32"
                        },
                        "light": {
                            "white": "#ffffff",
                            "primary": "#E1F0FF",
                            "secondary": "#EBEDF3",
                            "success": "#C9F7F5",
                            "info": "#EEE5FF",
                            "warning": "#FFF4DE",
                            "danger": "#FFE2E5",
                            "light": "#F3F6F9",
                            "dark": "#D6D6E0"
                        },
                        "inverse": {
                            "white": "#ffffff",
                            "primary": "#ffffff",
                            "secondary": "#3F4254",
                            "success": "#ffffff",
                            "info": "#ffffff",
                            "warning": "#ffffff",
                            "danger": "#ffffff",
                            "light": "#464E5F",
                            "dark": "#ffffff"
                        }
                    },
                    "gray": {
                        "gray-100": "#F3F6F9",
                        "gray-200": "#EBEDF3",
                        "gray-300": "#E4E6EF",
                        "gray-400": "#D1D3E0",
                        "gray-500": "#B5B5C3",
                        "gray-600": "#7E8299",
                        "gray-700": "#5E6278",
                        "gray-800": "#3F4254",
                        "gray-900": "#181C32"
                    }
                },
                "font-family": "Poppins"
            };
        </script>
        <!--end::Global Config-->

        <!--begin::Global Theme Bundle(used by all pages)-->
        <script src="{{ url('assets/plugins/global/plugins.bundle.js') }}"></script>
        <script src="{{ url('assets/plugins/custom/prismjs/prismjs.bundle.js') }}"></script>
        <script src="{{ url('assets/js/scripts.bundle.js') }}"></script>
        <!--end::Global Theme Bundle-->

        <!--begin::Page Vendors(used by this page)-->
        <script src="{{ url('assets/plugins/custom/fullcalendar/fullcalendar.bundle.js') }}"></script>
        <script src="{{ url('assets/plugins/custom/datatables/datatables.bundle.js') }}"></script>
        <script src="{{ url('assets/plugins/custom/formvalidation/dist/js/locales/tr_TR.js') }}"></script>
        <script src="{{ url('assets/plugins/custom/select2lang/tr.js') }}"></script>
        <script src="{{ url('assets/plugins/custom/tinymce/tinymce.bundle.js') }}"></script>
        <!--end::Page Vendors-->

        <!--begin::Page Scripts(used by this page)-->
        <script src="{{ url('assets/js/pages/widgets.js') }}"></script>
        <script src="{{ url('assets/js/pages/crud/datatables/data-sources/ajax-server-side.js') }}?{{str_random(4)}}"></script>
        <script src="{{ url('assets/js/pages/crud/forms/validation/form-widgets.js') }}"></script>
        <script src="{{ url('assets/js/pages/crud/forms/widgets/bootstrap-switch.js') }}"></script>
        <script src="{{ url('js/2mundos-fengyuanchen-cropperjs/dist/cropper.js') }}"></script>
        <!--<script src="{{ url('assets/js/pages/crud/file-upload/dropzonejs.js') }}"></script>-->
        <!--end::Page Scripts-->

        <!-- <script src="{{ asset('js/app.js') }}"></script> -->
        <script type="text/javascript">

            $(function () {

                tinymce.init({
                    selector: '#before_teaser_text'
                });
                tinymce.init({
                    selector: '#after_teaser_text'
                });
                tinymce.init({
                    selector: '#info_text'
                });
                tinymce.init({
                    selector: '#content',
                    plugins: 'image',
                    toolbar: 'undo redo | formatselect | bold italic | alignleft aligncenter alignright alignjustify | outdent indent | image',
                    image_title: true,
                    automatic_uploads: true,
                    file_picker_types: 'image',
                    file_picker_callback: function (cb, value, meta) {
                        var input = document.createElement('input');
                        input.setAttribute('type', 'file');
                        input.setAttribute('accept', 'image/*');

                        /*
                          Note: In modern browsers input[type="file"] is functional without
                          even adding it to the DOM, but that might not be the case in some older
                          or quirky browsers like IE, so you might want to add it to the DOM
                          just in case, and visually hide it. And do not forget do remove it
                          once you do not need it anymore.
                        */

                        input.onchange = function () {
                          var file = this.files[0];

                          var reader = new FileReader();
                          reader.onload = function () {
                            /*
                              Note: Now we need to register the blob in TinyMCEs image blob
                              registry. In the next release this part hopefully won't be
                              necessary, as we are looking to handle it internally.
                            */
                            var id = 'blobid' + (new Date()).getTime();
                            var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
                            var base64 = reader.result.split(',')[1];
                            var blobInfo = blobCache.create(id, file, base64);
                            blobCache.add(blobInfo);

                            /* call the callback and populate the Title field with the file name */
                            cb(blobInfo.blobUri(), { title: file.name });
                          };
                          reader.readAsDataURL(file);
                        };

                        input.click();
                    }
                });
                tinymce.init({
                    selector: '#rule_text'
                });

                $('#kt_dropzone').dropzone({
                    acceptedFiles: "image/jpeg,image/png",
                    dictDefaultMessage: "Dosyaları yüklemek için buraya bırakın.",
                    dictFallbackMessage: "Tarayıcınız sürükle bırak dosya yüklemelerini desteklemiyor.",
                    dictFallbackText: "Please use the fallback form below to upload your files like in the olden days.",
                    dictFileTooBig: "Dosya yüklemek için çok büyük. Maksimum dosya boyutu: 7.5 MB",
                    dictInvalidFileType: "Bu tür dosyaları yükleyemezsiniz.",
                    dictCancelUpload: "Yüklemeyi iptal et.",
                    dictUploadCanceled: "Yükleme iptal edildi.",
                    dictCancelUploadConfirmation: "Bu yüklemeyi iptal etmek istediğinizden emin misiniz?",
                    dictRemoveFile: "Dosyayı kaldır.",
                    dictRemoveFileConfirmation: null,
                    dictMaxFilesExceeded: "Daha fazla dosya yükleyemezsiniz.",
                    dictFileSizeUnits: { tb: "TB", gb: "GB", mb: "MB", kb: "KB", b: "b" },
                    headers: { 'X-CSRF-TOKEN': $("input[name='_token']").val() },
                    url: "{{ url('/match_image/save') }}", // Set the url for your upload script location
                    paramName: "file", // The name that will be used to transfer the file
                    maxFiles: 10,
                    maxFilesize: 7.5, // MB
                    addRemoveLinks: true,
                    accept: function(file, done) {
                        done();
                    },
                    error: function error(file, message) {
                        thiss = this;
                        if (file.previewElement) {
                            file.previewElement.classList.add("dz-error");

                            if (typeof message !== "string" && message.error) {
                                message = message.error;
                            }

                            var _iterator7 = _createForOfIteratorHelper(file.previewElement.querySelectorAll("[data-dz-errormessage]")),
                            _step7;

                            try {
                                for (_iterator7.s(); !(_step7 = _iterator7.n()).done;) {
                                    var node = _step7.value;
                                    node.textContent = message;
                                }
                            } catch (err) {
                                _iterator7.e(err);
                            } finally {
                                _iterator7.f();
                            }

                            $( file.previewElement ).on( "click", function() {
                              thiss.removeFile(file);
                            });
                        }
                    },
                    init: function () {
                        //console.log('dropzone init');
                    },
                    sending: function(file, xhr, formData){
                        //formData.append('uploadType', 'a');
                        formData.append('mid', $("input[name='id']").val());
                    },
                    success: function(file, xhr, event){
                        var data = jQuery.parseJSON(xhr);
                        // <img class="img-fluid" src="{{ url('/upload') }}/' + data.location + '" />
                        $("#imgMatchImages").prepend('\
                            <div class="col-lg-4 col-sm-12">\
                                <div class="card card-custom overlay">\
                                    <div class="card-body p-0">\
                                        <div class="overlay-wrapper">\
                                          <img src="{{ env('MEDIA_END') }}' + data.location + '" alt="" class="w-100 rounded"/>\
                                        </div>\
                                        <div class="overlay-layer align-items-start justify-content-end pt-5 pr-5">\
                                            <a href="{{ url('match_image/delete/') }}/' + data.mid + '/' + data.id + '" class="btn btn-clean btn-icon" title="Fotoğrafı Sil" ><i class="fas fa-trash-alt"></i></a>\
                                        </div>\
                                    </div>\
                                </div>\
                                <div class="form-group row" style="margin-top: 0.75rem;">\
                                    <div class="col-lg-12">\
                                        <input class="form-control" type="text" name="match_image[' + data.id + ']" placeholder="Fotoğraf Açıklaması" max="160" value="">\
                                    </div>\
                                </div>\
                            </div>\
                        ');

                        //$("#cropPhotoButton").attr("onclick", "photoGalleryThumbnailCrop('{{ url('upload') }}/" + data.location + "', " + data.id + ")");
                        this.removeFile(file);
                    },
                    transformFile: function(file, done) {
                        var myDropZone = this;
                        // Create the image editor overlay
                        var editor = document.createElement('div');
                        editor.style.position = 'fixed';
                        editor.style.left = 0;
                        editor.style.right = 0;
                        editor.style.top = 0;
                        editor.style.bottom = 0;
                        editor.style.zIndex = 9999;
                        editor.style.backgroundColor = '#000';
                        document.body.appendChild(editor);

                        // Create confirm button at the top left of the viewport
                        var buttonConfirm = document.createElement('button');
                        buttonConfirm.style.position = 'absolute';
                        buttonConfirm.style.left = '10px';
                        buttonConfirm.style.top = '10px';
                        buttonConfirm.style.zIndex = 9999;
                        buttonConfirm.textContent = 'Onayla';
                        editor.appendChild(buttonConfirm);
                        buttonConfirm.addEventListener('click', function() {
                            // Get the canvas with image data from Cropper.js
                            var canvas = cropper.getCroppedCanvas();
                            // Turn the canvas into a Blob (file object without a name)
                            canvas.toBlob(function(blob) {
                                // Create a new Dropzone file thumbnail
                                myDropZone.createThumbnail(
                                    blob,
                                    myDropZone.options.thumbnailWidth,
                                    myDropZone.options.thumbnailHeight,
                                    myDropZone.options.thumbnailMethod,
                                    false, 
                                    function(dataURL) {
                                        // Update the Dropzone file thumbnail
                                        myDropZone.emit('thumbnail', file, dataURL);
                                        // Return the file to Dropzone
                                        done(blob);
                                    });
                                });
                            // Remove the editor from the view
                            document.body.removeChild(editor);
                        });

                        var buttonCancel = document.createElement('button');
                        buttonCancel.style.position = 'absolute';
                        buttonCancel.style.right = '10px';
                        buttonCancel.style.top = '10px';
                        buttonCancel.style.zIndex = 9999;
                        buttonCancel.textContent = 'İptal';
                        editor.appendChild(buttonCancel);

                        buttonCancel.addEventListener('click', function() {
                            editor.remove();
                        });

                        /*
                            var selectSize = document.createElement('select');
                            selectSize.setAttribute("id", "selectSize");
                            selectSize.style.position = 'absolute';
                            selectSize.style.left = '40%';
                            selectSize.style.top = '10px';
                            selectSize.style.width = '20%';
                            selectSize.style.zIndex = 9999;

                            var opt = document.createElement('option');
                            opt.setAttribute("label", "title");
                            opt.setAttribute("value", 0);
                            selectSize.appendChild(opt);

                            editor.appendChild(selectSize);
                            
                            selectSize.addEventListener('change', function() {
                                //console.log(this.value);
                                cropper.setAspectRatio(this.value);
                            });
                        */

                        // Create an image node for Cropper.js
                        var image = new Image();
                        image.src = URL.createObjectURL(file);
                        editor.appendChild(image);

                        // Create Cropper.js
                        var cropper = new Cropper(image, { viewMode: 1, aspectRatio: 1.5, zoomable: false, zoomOnTouch:false, zoomOnWheel:false, wheelZoomRatio:false, autoCropArea: 0.95 });
                    }

                })

                $('#kt_dropzone_newpg').dropzone({
                    acceptedFiles: "image/jpeg,image/png",
                    dictDefaultMessage: "Dosyaları yüklemek için buraya bırakın.",
                    dictFallbackMessage: "Tarayıcınız sürükle bırak dosya yüklemelerini desteklemiyor.",
                    dictFallbackText: "Please use the fallback form below to upload your files like in the olden days.",
                    dictFileTooBig: "Dosya yüklemek için çok büyük. Maksimum dosya boyutu: 7.5 MB",
                    dictInvalidFileType: "Bu tür dosyaları yükleyemezsiniz.",
                    dictCancelUpload: "Yüklemeyi iptal et.",
                    dictUploadCanceled: "Yükleme iptal edildi.",
                    dictCancelUploadConfirmation: "Bu yüklemeyi iptal etmek istediğinizden emin misiniz?",
                    dictRemoveFile: "Dosyayı kaldır.",
                    dictRemoveFileConfirmation: null,
                    dictMaxFilesExceeded: "Daha fazla dosya yükleyemezsiniz.",
                    dictFileSizeUnits: { tb: "TB", gb: "GB", mb: "MB", kb: "KB", b: "b" },
                    headers: { 'X-CSRF-TOKEN': $("input[name='_token']").val() },
                    url: "{{ url('/new_image/save') }}", // Set the url for your upload script location
                    paramName: "file", // The name that will be used to transfer the file
                    maxFiles: 1,
                    maxFilesize: 7.5, // MB
                    addRemoveLinks: true,
                    accept: function(file, done) {
                        done();
                    },
                    error: function error(file, message) {
                        thiss = this;
                        if (file.previewElement) {
                            file.previewElement.classList.add("dz-error");

                            if (typeof message !== "string" && message.error) {
                                message = message.error;
                            }

                            var _iterator7 = _createForOfIteratorHelper(file.previewElement.querySelectorAll("[data-dz-errormessage]")),
                            _step7;

                            try {
                                for (_iterator7.s(); !(_step7 = _iterator7.n()).done;) {
                                    var node = _step7.value;
                                    node.textContent = message;
                                }
                            } catch (err) {
                                _iterator7.e(err);
                            } finally {
                                _iterator7.f();
                            }

                            $( file.previewElement ).on( "click", function() {
                              thiss.removeFile(file);
                            });
                        }
                    },
                    init: function () {
                        //console.log('dropzone init');
                    },
                    sending: function(file, xhr, formData){
                        //formData.append('uploadType', 'a');
                        formData.append('nid', $("input[name='id']").val());
                    },
                    success: function(file, xhr, event){
                        var data = jQuery.parseJSON(xhr);
                        $("#imgNewImages").html('<img src="{{ env('MEDIA_END') }}/' + data.location + '" alt="" class="rounded" style="width: 200px; height: auto;" />');
                        //$("#cropPhotoButton").attr("onclick", "photoGalleryThumbnailCrop('{{ url('upload') }}/" + data.location + "', " + data.id + ")");
                        this.removeFile(file);
                    },
                    transformFile: function(file, done) {
                        var myDropZone = this;
                        // Create the image editor overlay
                        var editor = document.createElement('div');
                        editor.style.position = 'fixed';
                        editor.style.left = 0;
                        editor.style.right = 0;
                        editor.style.top = 0;
                        editor.style.bottom = 0;
                        editor.style.zIndex = 9999;
                        editor.style.backgroundColor = '#000';
                        document.body.appendChild(editor);

                        // Create confirm button at the top left of the viewport
                        var buttonConfirm = document.createElement('button');
                        buttonConfirm.style.position = 'absolute';
                        buttonConfirm.style.left = '10px';
                        buttonConfirm.style.top = '10px';
                        buttonConfirm.style.zIndex = 9999;
                        buttonConfirm.textContent = 'Onayla';
                        editor.appendChild(buttonConfirm);
                        buttonConfirm.addEventListener('click', function() {
                            // Get the canvas with image data from Cropper.js
                            var canvas = cropper.getCroppedCanvas();
                            // Turn the canvas into a Blob (file object without a name)
                            canvas.toBlob(function(blob) {
                                // Create a new Dropzone file thumbnail
                                myDropZone.createThumbnail(
                                    blob,
                                    myDropZone.options.thumbnailWidth,
                                    myDropZone.options.thumbnailHeight,
                                    myDropZone.options.thumbnailMethod,
                                    false, 
                                    function(dataURL) {
                                        // Update the Dropzone file thumbnail
                                        myDropZone.emit('thumbnail', file, dataURL);
                                        // Return the file to Dropzone
                                        done(blob);
                                    });
                                });
                            // Remove the editor from the view
                            document.body.removeChild(editor);
                        });

                        var buttonCancel = document.createElement('button');
                        buttonCancel.style.position = 'absolute';
                        buttonCancel.style.right = '10px';
                        buttonCancel.style.top = '10px';
                        buttonCancel.style.zIndex = 9999;
                        buttonCancel.textContent = 'İptal';
                        editor.appendChild(buttonCancel);

                        buttonCancel.addEventListener('click', function() {
                            editor.remove();
                        });

                        /*
                            var selectSize = document.createElement('select');
                            selectSize.setAttribute("id", "selectSize");
                            selectSize.style.position = 'absolute';
                            selectSize.style.left = '40%';
                            selectSize.style.top = '10px';
                            selectSize.style.width = '20%';
                            selectSize.style.zIndex = 9999;

                            var opt = document.createElement('option');
                            opt.setAttribute("label", "title");
                            opt.setAttribute("value", 0);
                            selectSize.appendChild(opt);

                            editor.appendChild(selectSize);
                            
                            selectSize.addEventListener('change', function() {
                                //console.log(this.value);
                                cropper.setAspectRatio(this.value);
                            });
                        */

                        // Create an image node for Cropper.js
                        var image = new Image();
                        image.src = URL.createObjectURL(file);
                        editor.appendChild(image);

                        // Create Cropper.js
                        var cropper = new Cropper(image, { viewMode: 1, aspectRatio: 0, zoomable: false, zoomOnTouch:false, zoomOnWheel:false, wheelZoomRatio:false, autoCropArea: 1 });
                    }

                })

                $('#kt_dropzone_groundpg').dropzone({
                    acceptedFiles: "image/jpeg,image/png",
                    dictDefaultMessage: "Dosyaları yüklemek için buraya bırakın.",
                    dictFallbackMessage: "Tarayıcınız sürükle bırak dosya yüklemelerini desteklemiyor.",
                    dictFallbackText: "Please use the fallback form below to upload your files like in the olden days.",
                    dictFileTooBig: "Dosya yüklemek için çok büyük. Maksimum dosya boyutu: 7.5 MB",
                    dictInvalidFileType: "Bu tür dosyaları yükleyemezsiniz.",
                    dictCancelUpload: "Yüklemeyi iptal et.",
                    dictUploadCanceled: "Yükleme iptal edildi.",
                    dictCancelUploadConfirmation: "Bu yüklemeyi iptal etmek istediğinizden emin misiniz?",
                    dictRemoveFile: "Dosyayı kaldır.",
                    dictRemoveFileConfirmation: null,
                    dictMaxFilesExceeded: "Daha fazla dosya yükleyemezsiniz.",
                    dictFileSizeUnits: { tb: "TB", gb: "GB", mb: "MB", kb: "KB", b: "b" },
                    headers: { 'X-CSRF-TOKEN': $("input[name='_token']").val() },
                    url: "{{ url('/ground_image/save') }}", // Set the url for your upload script location
                    paramName: "file", // The name that will be used to transfer the file
                    maxFiles: 10,
                    maxFilesize: 7.5, // MB
                    addRemoveLinks: true,
                    accept: function(file, done) {
                        done();
                    },
                    error: function error(file, message) {
                        thiss = this;
                        if (file.previewElement) {
                            file.previewElement.classList.add("dz-error");

                            if (typeof message !== "string" && message.error) {
                                message = message.error;
                            }

                            var _iterator7 = _createForOfIteratorHelper(file.previewElement.querySelectorAll("[data-dz-errormessage]")),
                            _step7;

                            try {
                                for (_iterator7.s(); !(_step7 = _iterator7.n()).done;) {
                                    var node = _step7.value;
                                    node.textContent = message;
                                }
                            } catch (err) {
                                _iterator7.e(err);
                            } finally {
                                _iterator7.f();
                            }

                            $( file.previewElement ).on( "click", function() {
                              thiss.removeFile(file);
                            });
                        }
                    },
                    init: function () {
                        //console.log('dropzone init');
                    },
                    sending: function(file, xhr, formData){
                        //formData.append('uploadType', 'a');
                        formData.append('id', $("input[name='id']").val());
                    },
                    success: function(file, xhr, event){
                        var data = jQuery.parseJSON(xhr);
                        // <img class="img-fluid" src="{{ url('/upload') }}/' + data.location + '" />
                        $("#imgGroundImages").prepend('\
                            <div class="col-lg-4 col-sm-12">\
                                <div class="card card-custom overlay mb-5">\
                                    <div class="card-body p-0">\
                                        <div class="overlay-wrapper">\
                                          <img src="{{ env('MEDIA_END') }}' + data.location + '" alt="" class="w-100 rounded"/>\
                                        </div>\
                                        <div class="overlay-layer align-items-start justify-content-end pt-5 pr-5">\
                                            <a href="{{ url('ground_image/delete/') }}/' + data.id + '/' + data.gid + '" class="btn btn-clean btn-icon" title="Fotoğrafı Sil" ><i class="fas fa-trash-alt"></i></a>\
                                        </div>\
                                    </div>\
                                </div>\
                            </div>\
                        ');

                        //$("#cropPhotoButton").attr("onclick", "photoGalleryThumbnailCrop('{{ url('upload') }}/" + data.location + "', " + data.id + ")");
                        this.removeFile(file);
                    },
                    transformFile: function(file, done) {
                        var myDropZone = this;
                        // Create the image editor overlay
                        var editor = document.createElement('div');
                        editor.style.position = 'fixed';
                        editor.style.left = 0;
                        editor.style.right = 0;
                        editor.style.top = 0;
                        editor.style.bottom = 0;
                        editor.style.zIndex = 9999;
                        editor.style.backgroundColor = '#000';
                        document.body.appendChild(editor);

                        // Create confirm button at the top left of the viewport
                        var buttonConfirm = document.createElement('button');
                        buttonConfirm.style.position = 'absolute';
                        buttonConfirm.style.left = '10px';
                        buttonConfirm.style.top = '10px';
                        buttonConfirm.style.zIndex = 9999;
                        buttonConfirm.textContent = 'Onayla';
                        editor.appendChild(buttonConfirm);
                        buttonConfirm.addEventListener('click', function() {
                            // Get the canvas with image data from Cropper.js
                            var canvas = cropper.getCroppedCanvas();
                            // Turn the canvas into a Blob (file object without a name)
                            canvas.toBlob(function(blob) {
                                // Create a new Dropzone file thumbnail
                                myDropZone.createThumbnail(
                                    blob,
                                    myDropZone.options.thumbnailWidth,
                                    myDropZone.options.thumbnailHeight,
                                    myDropZone.options.thumbnailMethod,
                                    false, 
                                    function(dataURL) {
                                        // Update the Dropzone file thumbnail
                                        myDropZone.emit('thumbnail', file, dataURL);
                                        // Return the file to Dropzone
                                        done(blob);
                                    });
                                });
                            // Remove the editor from the view
                            document.body.removeChild(editor);
                        });

                        var buttonCancel = document.createElement('button');
                        buttonCancel.style.position = 'absolute';
                        buttonCancel.style.right = '10px';
                        buttonCancel.style.top = '10px';
                        buttonCancel.style.zIndex = 9999;
                        buttonCancel.textContent = 'İptal';
                        editor.appendChild(buttonCancel);

                        buttonCancel.addEventListener('click', function() {
                            editor.remove();
                        });

                        /*
                            var selectSize = document.createElement('select');
                            selectSize.setAttribute("id", "selectSize");
                            selectSize.style.position = 'absolute';
                            selectSize.style.left = '40%';
                            selectSize.style.top = '10px';
                            selectSize.style.width = '20%';
                            selectSize.style.zIndex = 9999;

                            var opt = document.createElement('option');
                            opt.setAttribute("label", "title");
                            opt.setAttribute("value", 0);
                            selectSize.appendChild(opt);

                            editor.appendChild(selectSize);
                            
                            selectSize.addEventListener('change', function() {
                                //console.log(this.value);
                                cropper.setAspectRatio(this.value);
                            });
                        */

                        // Create an image node for Cropper.js
                        var image = new Image();
                        image.src = URL.createObjectURL(file);
                        editor.appendChild(image);

                        // Create Cropper.js
                        var cropper = new Cropper(image, { viewMode: 1, aspectRatio: 1.7, zoomable: false, zoomOnTouch:false, zoomOnWheel:false, wheelZoomRatio:false, autoCropArea: 0.95 });
                    }

                })

                $('#kt_dropzone_teamlogo').dropzone({
                    acceptedFiles: "image/jpeg,image/png",
                    dictDefaultMessage: "Dosyaları yüklemek için buraya bırakın.",
                    dictFallbackMessage: "Tarayıcınız sürükle bırak dosya yüklemelerini desteklemiyor.",
                    dictFallbackText: "Please use the fallback form below to upload your files like in the olden days.",
                    dictFileTooBig: "Dosya yüklemek için çok büyük. Maksimum dosya boyutu: 7.5 MB",
                    dictInvalidFileType: "Bu tür dosyaları yükleyemezsiniz.",
                    dictCancelUpload: "Yüklemeyi iptal et.",
                    dictUploadCanceled: "Yükleme iptal edildi.",
                    dictCancelUploadConfirmation: "Bu yüklemeyi iptal etmek istediğinizden emin misiniz?",
                    dictRemoveFile: "Dosyayı kaldır.",
                    dictRemoveFileConfirmation: null,
                    dictMaxFilesExceeded: "Daha fazla dosya yükleyemezsiniz.",
                    dictFileSizeUnits: { tb: "TB", gb: "GB", mb: "MB", kb: "KB", b: "b" },
                    headers: { 'X-CSRF-TOKEN': $("input[name='_token']").val() },
                    url: "{{ url('/teams/logo_save') }}", // Set the url for your upload script location
                    paramName: "file", // The name that will be used to transfer the file
                    maxFiles: 1,
                    maxFilesize: 7.5, // MB
                    addRemoveLinks: true,
                    accept: function(file, done) {
                        done();
                    },
                    error: function error(file, message) {
                        thiss = this;
                        if (file.previewElement) {
                            file.previewElement.classList.add("dz-error");

                            if (typeof message !== "string" && message.error) {
                                message = message.error;
                            }

                            var _iterator7 = _createForOfIteratorHelper(file.previewElement.querySelectorAll("[data-dz-errormessage]")),
                            _step7;

                            try {
                                for (_iterator7.s(); !(_step7 = _iterator7.n()).done;) {
                                    var node = _step7.value;
                                    node.textContent = message;
                                }
                            } catch (err) {
                                _iterator7.e(err);
                            } finally {
                                _iterator7.f();
                            }

                            $( file.previewElement ).on( "click", function() {
                              thiss.removeFile(file);
                            });
                        }
                    },
                    init: function () {
                        //console.log('dropzone init');
                    },
                    sending: function(file, xhr, formData){
                        //formData.append('uploadType', 'a');
                        formData.append('tid', $("input[name='id']").val());
                    },
                    success: function(file, xhr, event){
                        var data = jQuery.parseJSON(xhr);
                        $("#imgTeamLogo").html('<img src="{{ env('MEDIA_END') }}/' + data.location + '" alt="" class="rounded" style="width: 200px; height: auto;" />');
                        //$("#cropPhotoButton").attr("onclick", "photoGalleryThumbnailCrop('{{ url('upload') }}/" + data.location + "', " + data.id + ")");
                        this.removeFile(file);
                    },
                    transformFile: function(file, done) {
                        var myDropZone = this;
                        // Create the image editor overlay
                        var editor = document.createElement('div');
                        editor.style.position = 'fixed';
                        editor.style.left = 0;
                        editor.style.right = 0;
                        editor.style.top = 0;
                        editor.style.bottom = 0;
                        editor.style.zIndex = 9999;
                        editor.style.backgroundColor = '#000';
                        document.body.appendChild(editor);

                        // Create confirm button at the top left of the viewport
                        var buttonConfirm = document.createElement('button');
                        buttonConfirm.style.position = 'absolute';
                        buttonConfirm.style.left = '10px';
                        buttonConfirm.style.top = '10px';
                        buttonConfirm.style.zIndex = 9999;
                        buttonConfirm.textContent = 'Onayla';
                        editor.appendChild(buttonConfirm);
                        buttonConfirm.addEventListener('click', function() {
                            // Get the canvas with image data from Cropper.js
                            var canvas = cropper.getCroppedCanvas();
                            // Turn the canvas into a Blob (file object without a name)
                            canvas.toBlob(function(blob) {
                                // Create a new Dropzone file thumbnail
                                myDropZone.createThumbnail(
                                    blob,
                                    myDropZone.options.thumbnailWidth,
                                    myDropZone.options.thumbnailHeight,
                                    myDropZone.options.thumbnailMethod,
                                    false, 
                                    function(dataURL) {
                                        // Update the Dropzone file thumbnail
                                        myDropZone.emit('thumbnail', file, dataURL);
                                        // Return the file to Dropzone
                                        done(blob);
                                    });
                                });
                            // Remove the editor from the view
                            document.body.removeChild(editor);
                        });

                        var buttonCancel = document.createElement('button');
                        buttonCancel.style.position = 'absolute';
                        buttonCancel.style.right = '10px';
                        buttonCancel.style.top = '10px';
                        buttonCancel.style.zIndex = 9999;
                        buttonCancel.textContent = 'İptal';
                        editor.appendChild(buttonCancel);

                        buttonCancel.addEventListener('click', function() {
                            editor.remove();
                        });

                        /*
                            var selectSize = document.createElement('select');
                            selectSize.setAttribute("id", "selectSize");
                            selectSize.style.position = 'absolute';
                            selectSize.style.left = '40%';
                            selectSize.style.top = '10px';
                            selectSize.style.width = '20%';
                            selectSize.style.zIndex = 9999;

                            var opt = document.createElement('option');
                            opt.setAttribute("label", "title");
                            opt.setAttribute("value", 0);
                            selectSize.appendChild(opt);

                            editor.appendChild(selectSize);
                            
                            selectSize.addEventListener('change', function() {
                                //console.log(this.value);
                                cropper.setAspectRatio(this.value);
                            });
                        */

                        // Create an image node for Cropper.js
                        var image = new Image();
                        image.src = URL.createObjectURL(file);
                        editor.appendChild(image);

                        // Create Cropper.js
                        var cropper = new Cropper(image, { viewMode: 1, aspectRatio: 1, zoomable: false, zoomOnTouch:false, zoomOnWheel:false, wheelZoomRatio:false, autoCropArea: 0.95 });
                    }

                })

                $('#kt_dropzone_teamcover').dropzone({
                    acceptedFiles: "image/jpeg,image/png",
                    dictDefaultMessage: "Dosyaları yüklemek için buraya bırakın.",
                    dictFallbackMessage: "Tarayıcınız sürükle bırak dosya yüklemelerini desteklemiyor.",
                    dictFallbackText: "Please use the fallback form below to upload your files like in the olden days.",
                    dictFileTooBig: "Dosya yüklemek için çok büyük. Maksimum dosya boyutu: 7.5 MB",
                    dictInvalidFileType: "Bu tür dosyaları yükleyemezsiniz.",
                    dictCancelUpload: "Yüklemeyi iptal et.",
                    dictUploadCanceled: "Yükleme iptal edildi.",
                    dictCancelUploadConfirmation: "Bu yüklemeyi iptal etmek istediğinizden emin misiniz?",
                    dictRemoveFile: "Dosyayı kaldır.",
                    dictRemoveFileConfirmation: null,
                    dictMaxFilesExceeded: "Daha fazla dosya yükleyemezsiniz.",
                    dictFileSizeUnits: { tb: "TB", gb: "GB", mb: "MB", kb: "KB", b: "b" },
                    headers: { 'X-CSRF-TOKEN': $("input[name='_token']").val() },
                    url: "{{ url('/teams/cover_save') }}", // Set the url for your upload script location
                    paramName: "file", // The name that will be used to transfer the file
                    maxFiles: 1,
                    maxFilesize: 7.5, // MB
                    addRemoveLinks: true,
                    accept: function(file, done) {
                        done();
                    },
                    error: function error(file, message) {
                        thiss = this;
                        if (file.previewElement) {
                            file.previewElement.classList.add("dz-error");

                            if (typeof message !== "string" && message.error) {
                                message = message.error;
                            }

                            var _iterator7 = _createForOfIteratorHelper(file.previewElement.querySelectorAll("[data-dz-errormessage]")),
                            _step7;

                            try {
                                for (_iterator7.s(); !(_step7 = _iterator7.n()).done;) {
                                    var node = _step7.value;
                                    node.textContent = message;
                                }
                            } catch (err) {
                                _iterator7.e(err);
                            } finally {
                                _iterator7.f();
                            }

                            $( file.previewElement ).on( "click", function() {
                              thiss.removeFile(file);
                            });
                        }
                    },
                    init: function () {
                        //console.log('dropzone init');
                    },
                    sending: function(file, xhr, formData){
                        //formData.append('uploadType', 'a');
                        formData.append('tid', $("input[name='id']").val());
                    },
                    success: function(file, xhr, event){
                        var data = jQuery.parseJSON(xhr);
                        $("#imgTeamCover").html('<img src="{{ env('MEDIA_END') }}/' + data.location + '" alt="" class="w-100 rounded" />');
                        //$("#cropPhotoButton").attr("onclick", "photoGalleryThumbnailCrop('{{ url('upload') }}/" + data.location + "', " + data.id + ")");
                        this.removeFile(file);
                    },
                    transformFile: function(file, done) { 
                        var myDropZone = this;
                        // Create the image editor overlay
                        var editor = document.createElement('div');
                        editor.style.position = 'fixed';
                        editor.style.left = 0;
                        editor.style.right = 0;
                        editor.style.top = 0;
                        editor.style.bottom = 0;
                        editor.style.zIndex = 9999;
                        editor.style.backgroundColor = '#000';
                        document.body.appendChild(editor);

                        // Create confirm button at the top left of the viewport
                        var buttonConfirm = document.createElement('button');
                        buttonConfirm.style.position = 'absolute';
                        buttonConfirm.style.left = '10px';
                        buttonConfirm.style.top = '10px';
                        buttonConfirm.style.zIndex = 9999;
                        buttonConfirm.textContent = 'Onayla';
                        editor.appendChild(buttonConfirm);
                        buttonConfirm.addEventListener('click', function() {
                            // Get the canvas with image data from Cropper.js
                            var canvas = cropper.getCroppedCanvas();
                            // Turn the canvas into a Blob (file object without a name)
                            canvas.toBlob(function(blob) {
                                // Create a new Dropzone file thumbnail
                                myDropZone.createThumbnail(
                                    blob,
                                    myDropZone.options.thumbnailWidth,
                                    myDropZone.options.thumbnailHeight,
                                    myDropZone.options.thumbnailMethod,
                                    false, 
                                    function(dataURL) {
                                        // Update the Dropzone file thumbnail
                                        myDropZone.emit('thumbnail', file, dataURL);
                                        // Return the file to Dropzone
                                        done(blob);

                                    });
                                });
                            // Remove the editor from the view
                            document.body.removeChild(editor);
                        });

                        var buttonCancel = document.createElement('button');
                        buttonCancel.style.position = 'absolute';
                        buttonCancel.style.right = '10px';
                        buttonCancel.style.top = '10px';
                        buttonCancel.style.zIndex = 9999;
                        buttonCancel.textContent = 'İptal';
                        editor.appendChild(buttonCancel);

                        buttonCancel.addEventListener('click', function() {
                            editor.remove();
                        });

                        /*
                            var selectSize = document.createElement('select');
                            selectSize.setAttribute("id", "selectSize");
                            selectSize.style.position = 'absolute';
                            selectSize.style.left = '40%';
                            selectSize.style.top = '10px';
                            selectSize.style.width = '20%';
                            selectSize.style.zIndex = 9999;

                            var opt = document.createElement('option');
                            opt.setAttribute("label", "title");
                            opt.setAttribute("value", 0);
                            selectSize.appendChild(opt);

                            editor.appendChild(selectSize);
                            
                            selectSize.addEventListener('change', function() {
                                //console.log(this.value);
                                cropper.setAspectRatio(this.value);
                            });
                        */

                        // Create an image node for Cropper.js
                        var image = new Image();
                        image.src = URL.createObjectURL(file);
                        editor.appendChild(image);

                        // Create Cropper.js
                        var cropper = new Cropper(image, { viewMode: 1, aspectRatio: 1.5, zoomable: false, zoomOnTouch:false, zoomOnWheel:false, wheelZoomRatio:false, autoCropArea: 0.95,
                            ready() {
                                //this.cropper.zoom(-0.5);
                            }
                        });
                    }

                })

                $('#kt_dropzone_userphoto').dropzone({
                    acceptedFiles: "image/jpeg,image/png",
                    dictDefaultMessage: "Dosyaları yüklemek için buraya bırakın.",
                    dictFallbackMessage: "Tarayıcınız sürükle bırak dosya yüklemelerini desteklemiyor.",
                    dictFallbackText: "Please use the fallback form below to upload your files like in the olden days.",
                    dictFileTooBig: "Dosya yüklemek için çok büyük. Maksimum dosya boyutu: 7.5 MB",
                    dictInvalidFileType: "Bu tür dosyaları yükleyemezsiniz.",
                    dictCancelUpload: "Yüklemeyi iptal et.",
                    dictUploadCanceled: "Yükleme iptal edildi.",
                    dictCancelUploadConfirmation: "Bu yüklemeyi iptal etmek istediğinizden emin misiniz?",
                    dictRemoveFile: "Dosyayı kaldır.",
                    dictRemoveFileConfirmation: null,
                    dictMaxFilesExceeded: "Daha fazla dosya yükleyemezsiniz.",
                    dictFileSizeUnits: { tb: "TB", gb: "GB", mb: "MB", kb: "KB", b: "b" },
                    headers: { 'X-CSRF-TOKEN': $("input[name='_token']").val() },
                    url: "{{ url('/users/logo_save') }}", // Set the url for your upload script location
                    paramName: "file", // The name that will be used to transfer the file
                    maxFiles: 1,
                    maxFilesize: 7.5, // MB
                    addRemoveLinks: true,
                    accept: function(file, done) {
                        done();
                    },
                    error: function error(file, message) {
                        thiss = this;
                        if (file.previewElement) {
                            file.previewElement.classList.add("dz-error");

                            if (typeof message !== "string" && message.error) {
                                message = message.error;
                            }

                            var _iterator7 = _createForOfIteratorHelper(file.previewElement.querySelectorAll("[data-dz-errormessage]")),
                            _step7;

                            try {
                                for (_iterator7.s(); !(_step7 = _iterator7.n()).done;) {
                                    var node = _step7.value;
                                    node.textContent = message;
                                }
                            } catch (err) {
                                _iterator7.e(err);
                            } finally {
                                _iterator7.f();
                            }

                            $( file.previewElement ).on( "click", function() {
                              thiss.removeFile(file);
                            });
                        }
                    },
                    init: function () {
                        //console.log('dropzone init');
                    },
                    sending: function(file, xhr, formData){
                        //formData.append('uploadType', 'a');
                        formData.append('uid', $("input[name='id']").val());
                    },
                    success: function(file, xhr, event){
                        var data = jQuery.parseJSON(xhr);
                        $("#imgUserPhoto").html('<img src="{{ env('MEDIA_END') }}/' + data.location + '" alt="" class="rounded" style="width: 200px; height: auto;" />');
                        //$("#cropPhotoButton").attr("onclick", "photoGalleryThumbnailCrop('{{ url('upload') }}/" + data.location + "', " + data.id + ")");
                        this.removeFile(file);
                    },
                    transformFile: function(file, done) {
                        var myDropZone = this;
                        // Create the image editor overlay
                        var editor = document.createElement('div');
                        editor.style.position = 'fixed';
                        editor.style.left = 0;
                        editor.style.right = 0;
                        editor.style.top = 0;
                        editor.style.bottom = 0;
                        editor.style.zIndex = 9999;
                        editor.style.backgroundColor = '#000';
                        document.body.appendChild(editor);

                        // Create confirm button at the top left of the viewport
                        var buttonConfirm = document.createElement('button');
                        buttonConfirm.style.position = 'absolute';
                        buttonConfirm.style.left = '10px';
                        buttonConfirm.style.top = '10px';
                        buttonConfirm.style.zIndex = 9999;
                        buttonConfirm.textContent = 'Onayla';
                        editor.appendChild(buttonConfirm);
                        buttonConfirm.addEventListener('click', function() {
                            // Get the canvas with image data from Cropper.js
                            var canvas = cropper.getCroppedCanvas();
                            // Turn the canvas into a Blob (file object without a name)
                            canvas.toBlob(function(blob) {
                                // Create a new Dropzone file thumbnail
                                myDropZone.createThumbnail(
                                    blob,
                                    myDropZone.options.thumbnailWidth,
                                    myDropZone.options.thumbnailHeight,
                                    myDropZone.options.thumbnailMethod,
                                    false, 
                                    function(dataURL) {
                                        // Update the Dropzone file thumbnail
                                        myDropZone.emit('thumbnail', file, dataURL);
                                        // Return the file to Dropzone
                                        done(blob);
                                    });
                                });
                            // Remove the editor from the view
                            document.body.removeChild(editor);
                        });

                        var buttonCancel = document.createElement('button');
                        buttonCancel.style.position = 'absolute';
                        buttonCancel.style.right = '10px';
                        buttonCancel.style.top = '10px';
                        buttonCancel.style.zIndex = 9999;
                        buttonCancel.textContent = 'İptal';
                        editor.appendChild(buttonCancel);

                        buttonCancel.addEventListener('click', function() {
                            editor.remove();
                        });

                        // Create an image node for Cropper.js
                        var image = new Image();
                        image.src = URL.createObjectURL(file);
                        editor.appendChild(image);

                        // Create Cropper.js
                        var cropper = new Cropper(image, { viewMode: 1, aspectRatio: 1, zoomable: false, zoomOnTouch:false, zoomOnWheel:false, wheelZoomRatio:false, autoCropArea: 0.95 });
                    }

                })

                $('#kt_dropzone_season_before').dropzone({
                    acceptedFiles: "image/jpeg,image/png",
                    dictDefaultMessage: "Dosyaları yüklemek için buraya bırakın.",
                    dictFallbackMessage: "Tarayıcınız sürükle bırak dosya yüklemelerini desteklemiyor.",
                    dictFallbackText: "Please use the fallback form below to upload your files like in the olden days.",
                    dictFileTooBig: "Dosya yüklemek için çok büyük. Maksimum dosya boyutu: 7.5 MB",
                    dictInvalidFileType: "Bu tür dosyaları yükleyemezsiniz.",
                    dictCancelUpload: "Yüklemeyi iptal et.",
                    dictUploadCanceled: "Yükleme iptal edildi.",
                    dictCancelUploadConfirmation: "Bu yüklemeyi iptal etmek istediğinizden emin misiniz?",
                    dictRemoveFile: "Dosyayı kaldır.",
                    dictRemoveFileConfirmation: null,
                    dictMaxFilesExceeded: "Daha fazla dosya yükleyemezsiniz.",
                    dictFileSizeUnits: { tb: "TB", gb: "GB", mb: "MB", kb: "KB", b: "b" },
                    headers: { 'X-CSRF-TOKEN': $("input[name='_token']").val() },
                    url: "{{ url('/season_image/save') }}", // Set the url for your upload script location
                    paramName: "file", // The name that will be used to transfer the file
                    maxFiles: 10,
                    maxFilesize: 7.5, // MB
                    addRemoveLinks: true,
                    accept: function(file, done) {
                        done();
                    },
                    error: function error(file, message) {
                        thiss = this;
                        if (file.previewElement) {
                            file.previewElement.classList.add("dz-error");

                            if (typeof message !== "string" && message.error) {
                                message = message.error;
                            }

                            var _iterator7 = _createForOfIteratorHelper(file.previewElement.querySelectorAll("[data-dz-errormessage]")),
                            _step7;

                            try {
                                for (_iterator7.s(); !(_step7 = _iterator7.n()).done;) {
                                    var node = _step7.value;
                                    node.textContent = message;
                                }
                            } catch (err) {
                                _iterator7.e(err);
                            } finally {
                                _iterator7.f();
                            }

                            $( file.previewElement ).on( "click", function() {
                              thiss.removeFile(file);
                            });
                        }
                    },
                    init: function () {
                        //console.log('dropzone init');
                    },
                    sending: function(file, xhr, formData){
                        //formData.append('uploadType', 'a');
                        formData.append('sid', $("input[name='id']").val());
                        formData.append('season_status', 'before');
                    },
                    success: function(file, xhr, event){
                        var data = jQuery.parseJSON(xhr);
                        // <img class="img-fluid" src="{{ url('/upload') }}/' + data.location + '" />
                        $("#imgSeasonBeforeImages").prepend('\
                            <div class="col-lg-4 col-sm-12">\
                                <div class="card card-custom overlay">\
                                    <div class="card-body p-0">\
                                        <div class="overlay-wrapper">\
                                          <img src="{{ env('MEDIA_END') }}' + data.location + '" alt="" class="w-100 rounded"/>\
                                        </div>\
                                        <div class="overlay-layer align-items-start justify-content-end pt-5 pr-5">\
                                            <a href="{{ url('season_image/delete/') }}/' + data.mid + '/' + data.id + '" class="btn btn-clean btn-icon" title="Fotoğrafı Sil" ><i class="fas fa-trash-alt"></i></a>\
                                        </div>\
                                    </div>\
                                </div>\
                                <div class="form-group row" style="margin-top: 0.75rem;">\
                                    <div class="col-lg-12">\
                                        <input class="form-control" type="text" name="season_before_image[' + data.id + ']" placeholder="Fotoğraf Açıklaması" max="160" value="">\
                                    </div>\
                                </div>\
                            </div>\
                        ');

                        //$("#cropPhotoButton").attr("onclick", "photoGalleryThumbnailCrop('{{ url('upload') }}/" + data.location + "', " + data.id + ")");
                        this.removeFile(file);
                    },
                    transformFile: function(file, done) {
                        var myDropZone = this;
                        // Create the image editor overlay
                        var editor = document.createElement('div');
                        editor.style.position = 'fixed';
                        editor.style.left = 0;
                        editor.style.right = 0;
                        editor.style.top = 0;
                        editor.style.bottom = 0;
                        editor.style.zIndex = 9999;
                        editor.style.backgroundColor = '#000';
                        document.body.appendChild(editor);

                        // Create confirm button at the top left of the viewport
                        var buttonConfirm = document.createElement('button');
                        buttonConfirm.style.position = 'absolute';
                        buttonConfirm.style.left = '10px';
                        buttonConfirm.style.top = '10px';
                        buttonConfirm.style.zIndex = 9999;
                        buttonConfirm.textContent = 'Onayla';
                        editor.appendChild(buttonConfirm);
                        buttonConfirm.addEventListener('click', function() {
                            // Get the canvas with image data from Cropper.js
                            var canvas = cropper.getCroppedCanvas();
                            // Turn the canvas into a Blob (file object without a name)
                            canvas.toBlob(function(blob) {
                                // Create a new Dropzone file thumbnail
                                myDropZone.createThumbnail(
                                    blob,
                                    myDropZone.options.thumbnailWidth,
                                    myDropZone.options.thumbnailHeight,
                                    myDropZone.options.thumbnailMethod,
                                    false, 
                                    function(dataURL) {
                                        // Update the Dropzone file thumbnail
                                        myDropZone.emit('thumbnail', file, dataURL);
                                        // Return the file to Dropzone
                                        done(blob);
                                    });
                                });
                            // Remove the editor from the view
                            document.body.removeChild(editor);
                        });

                        var buttonCancel = document.createElement('button');
                        buttonCancel.style.position = 'absolute';
                        buttonCancel.style.right = '10px';
                        buttonCancel.style.top = '10px';
                        buttonCancel.style.zIndex = 9999;
                        buttonCancel.textContent = 'İptal';
                        editor.appendChild(buttonCancel);

                        buttonCancel.addEventListener('click', function() {
                            editor.remove();
                        });

                        /*
                            var selectSize = document.createElement('select');
                            selectSize.setAttribute("id", "selectSize");
                            selectSize.style.position = 'absolute';
                            selectSize.style.left = '40%';
                            selectSize.style.top = '10px';
                            selectSize.style.width = '20%';
                            selectSize.style.zIndex = 9999;

                            var opt = document.createElement('option');
                            opt.setAttribute("label", "title");
                            opt.setAttribute("value", 0);
                            selectSize.appendChild(opt);

                            editor.appendChild(selectSize);
                            
                            selectSize.addEventListener('change', function() {
                                //console.log(this.value);
                                cropper.setAspectRatio(this.value);
                            });
                        */

                        // Create an image node for Cropper.js
                        var image = new Image();
                        image.src = URL.createObjectURL(file);
                        editor.appendChild(image);

                        // Create Cropper.js
                        var cropper = new Cropper(image, { viewMode: 1, aspectRatio: 1.5, zoomable: false, zoomOnTouch:false, zoomOnWheel:false, wheelZoomRatio:false, autoCropArea: 0.95 });
                    }

                })

                $('#kt_dropzone_season_now').dropzone({
                    acceptedFiles: "image/jpeg,image/png",
                    dictDefaultMessage: "Dosyaları yüklemek için buraya bırakın.",
                    dictFallbackMessage: "Tarayıcınız sürükle bırak dosya yüklemelerini desteklemiyor.",
                    dictFallbackText: "Please use the fallback form below to upload your files like in the olden days.",
                    dictFileTooBig: "Dosya yüklemek için çok büyük. Maksimum dosya boyutu: 7.5 MB",
                    dictInvalidFileType: "Bu tür dosyaları yükleyemezsiniz.",
                    dictCancelUpload: "Yüklemeyi iptal et.",
                    dictUploadCanceled: "Yükleme iptal edildi.",
                    dictCancelUploadConfirmation: "Bu yüklemeyi iptal etmek istediğinizden emin misiniz?",
                    dictRemoveFile: "Dosyayı kaldır.",
                    dictRemoveFileConfirmation: null,
                    dictMaxFilesExceeded: "Daha fazla dosya yükleyemezsiniz.",
                    dictFileSizeUnits: { tb: "TB", gb: "GB", mb: "MB", kb: "KB", b: "b" },
                    headers: { 'X-CSRF-TOKEN': $("input[name='_token']").val() },
                    url: "{{ url('/season_image/save') }}", // Set the url for your upload script location
                    paramName: "file", // The name that will be used to transfer the file
                    maxFiles: 10,
                    maxFilesize: 7.5, // MB
                    addRemoveLinks: true,
                    accept: function(file, done) {
                        done();
                    },
                    error: function error(file, message) {
                        thiss = this;
                        if (file.previewElement) {
                            file.previewElement.classList.add("dz-error");

                            if (typeof message !== "string" && message.error) {
                                message = message.error;
                            }

                            var _iterator7 = _createForOfIteratorHelper(file.previewElement.querySelectorAll("[data-dz-errormessage]")),
                            _step7;

                            try {
                                for (_iterator7.s(); !(_step7 = _iterator7.n()).done;) {
                                    var node = _step7.value;
                                    node.textContent = message;
                                }
                            } catch (err) {
                                _iterator7.e(err);
                            } finally {
                                _iterator7.f();
                            }

                            $( file.previewElement ).on( "click", function() {
                              thiss.removeFile(file);
                            });
                        }
                    },
                    init: function () {
                        //console.log('dropzone init');
                    },
                    sending: function(file, xhr, formData){
                        //formData.append('uploadType', 'a');
                        formData.append('sid', $("input[name='id']").val());
                        formData.append('season_status', 'now');
                    },
                    success: function(file, xhr, event){
                        var data = jQuery.parseJSON(xhr);
                        // <img class="img-fluid" src="{{ url('/upload') }}/' + data.location + '" />
                        $("#imgSeasonNowImages").prepend('\
                            <div class="col-lg-4 col-sm-12">\
                                <div class="card card-custom overlay">\
                                    <div class="card-body p-0">\
                                        <div class="overlay-wrapper">\
                                          <img src="{{ env('MEDIA_END') }}' + data.location + '" alt="" class="w-100 rounded"/>\
                                        </div>\
                                        <div class="overlay-layer align-items-start justify-content-end pt-5 pr-5">\
                                            <a href="{{ url('season_image/delete/') }}/' + data.mid + '/' + data.id + '" class="btn btn-clean btn-icon" title="Fotoğrafı Sil" ><i class="fas fa-trash-alt"></i></a>\
                                        </div>\
                                    </div>\
                                </div>\
                                <div class="form-group row" style="margin-top: 0.75rem;">\
                                    <div class="col-lg-12">\
                                        <input class="form-control" type="text" name="season_now_image[' + data.id + ']" placeholder="Fotoğraf Açıklaması" max="160" value="">\
                                    </div>\
                                </div>\
                            </div>\
                        ');

                        //$("#cropPhotoButton").attr("onclick", "photoGalleryThumbnailCrop('{{ url('upload') }}/" + data.location + "', " + data.id + ")");
                        this.removeFile(file);
                    },
                    transformFile: function(file, done) {
                        var myDropZone = this;
                        // Create the image editor overlay
                        var editor = document.createElement('div');
                        editor.style.position = 'fixed';
                        editor.style.left = 0;
                        editor.style.right = 0;
                        editor.style.top = 0;
                        editor.style.bottom = 0;
                        editor.style.zIndex = 9999;
                        editor.style.backgroundColor = '#000';
                        document.body.appendChild(editor);

                        // Create confirm button at the top left of the viewport
                        var buttonConfirm = document.createElement('button');
                        buttonConfirm.style.position = 'absolute';
                        buttonConfirm.style.left = '10px';
                        buttonConfirm.style.top = '10px';
                        buttonConfirm.style.zIndex = 9999;
                        buttonConfirm.textContent = 'Onayla';
                        editor.appendChild(buttonConfirm);
                        buttonConfirm.addEventListener('click', function() {
                            // Get the canvas with image data from Cropper.js
                            var canvas = cropper.getCroppedCanvas();
                            // Turn the canvas into a Blob (file object without a name)
                            canvas.toBlob(function(blob) {
                                // Create a new Dropzone file thumbnail
                                myDropZone.createThumbnail(
                                    blob,
                                    myDropZone.options.thumbnailWidth,
                                    myDropZone.options.thumbnailHeight,
                                    myDropZone.options.thumbnailMethod,
                                    false, 
                                    function(dataURL) {
                                        // Update the Dropzone file thumbnail
                                        myDropZone.emit('thumbnail', file, dataURL);
                                        // Return the file to Dropzone
                                        done(blob);
                                    });
                                });
                            // Remove the editor from the view
                            document.body.removeChild(editor);
                        });

                        var buttonCancel = document.createElement('button');
                        buttonCancel.style.position = 'absolute';
                        buttonCancel.style.right = '10px';
                        buttonCancel.style.top = '10px';
                        buttonCancel.style.zIndex = 9999;
                        buttonCancel.textContent = 'İptal';
                        editor.appendChild(buttonCancel);

                        buttonCancel.addEventListener('click', function() {
                            editor.remove();
                        });

                        /*
                            var selectSize = document.createElement('select');
                            selectSize.setAttribute("id", "selectSize");
                            selectSize.style.position = 'absolute';
                            selectSize.style.left = '40%';
                            selectSize.style.top = '10px';
                            selectSize.style.width = '20%';
                            selectSize.style.zIndex = 9999;

                            var opt = document.createElement('option');
                            opt.setAttribute("label", "title");
                            opt.setAttribute("value", 0);
                            selectSize.appendChild(opt);

                            editor.appendChild(selectSize);
                            
                            selectSize.addEventListener('change', function() {
                                //console.log(this.value);
                                cropper.setAspectRatio(this.value);
                            });
                        */

                        // Create an image node for Cropper.js
                        var image = new Image();
                        image.src = URL.createObjectURL(file);
                        editor.appendChild(image);

                        // Create Cropper.js
                        var cropper = new Cropper(image, { viewMode: 1, aspectRatio: 1.5, zoomable: false, zoomOnTouch:false, zoomOnWheel:false, wheelZoomRatio:false, autoCropArea: 0.95 });
                    }

                })

                $('#kt_dropzone_season_before_slider').dropzone({
                    acceptedFiles: "image/jpeg,image/png",
                    dictDefaultMessage: "Dosyaları yüklemek için buraya bırakın.",
                    dictFallbackMessage: "Tarayıcınız sürükle bırak dosya yüklemelerini desteklemiyor.",
                    dictFallbackText: "Please use the fallback form below to upload your files like in the olden days.",
                    dictFileTooBig: "Dosya yüklemek için çok büyük. Maksimum dosya boyutu: 7.5 MB",
                    dictInvalidFileType: "Bu tür dosyaları yükleyemezsiniz.",
                    dictCancelUpload: "Yüklemeyi iptal et.",
                    dictUploadCanceled: "Yükleme iptal edildi.",
                    dictCancelUploadConfirmation: "Bu yüklemeyi iptal etmek istediğinizden emin misiniz?",
                    dictRemoveFile: "Dosyayı kaldır.",
                    dictRemoveFileConfirmation: null,
                    dictMaxFilesExceeded: "Daha fazla dosya yükleyemezsiniz.",
                    dictFileSizeUnits: { tb: "TB", gb: "GB", mb: "MB", kb: "KB", b: "b" },
                    headers: { 'X-CSRF-TOKEN': $("input[name='_token']").val() },
                    url: "{{ url('/season_image/save') }}", // Set the url for your upload script location
                    paramName: "file", // The name that will be used to transfer the file
                    maxFiles: 1,
                    maxFilesize: 7.5, // MB
                    addRemoveLinks: true,
                    accept: function(file, done) {
                        done();
                    },
                    error: function error(file, message) {
                        thiss = this;
                        if (file.previewElement) {
                            file.previewElement.classList.add("dz-error");

                            if (typeof message !== "string" && message.error) {
                                message = message.error;
                            }

                            var _iterator7 = _createForOfIteratorHelper(file.previewElement.querySelectorAll("[data-dz-errormessage]")),
                            _step7;

                            try {
                                for (_iterator7.s(); !(_step7 = _iterator7.n()).done;) {
                                    var node = _step7.value;
                                    node.textContent = message;
                                }
                            } catch (err) {
                                _iterator7.e(err);
                            } finally {
                                _iterator7.f();
                            }

                            $( file.previewElement ).on( "click", function() {
                              thiss.removeFile(file);
                            });
                        }
                    },
                    init: function () {
                        //console.log('dropzone init');
                    },
                    sending: function(file, xhr, formData){
                        //formData.append('uploadType', 'a');
                        formData.append('sid', $("input[name='id']").val());
                        formData.append('season_status', 'before_slide');
                    },
                    success: function(file, xhr, event){
                        var data = jQuery.parseJSON(xhr);
                        $("#imgSeasonBeforeSlideImage").html('<img src="{{ env('MEDIA_END') }}/' + data.location + '" alt="" class="rounded" style="width: 200px; height: auto;" />');
                        //$("#cropPhotoButton").attr("onclick", "photoGalleryThumbnailCrop('{{ url('upload') }}/" + data.location + "', " + data.id + ")");
                        this.removeFile(file);
                    },
                    transformFile: function(file, done) {
                        var myDropZone = this;
                        // Create the image editor overlay
                        var editor = document.createElement('div');
                        editor.style.position = 'fixed';
                        editor.style.left = 0;
                        editor.style.right = 0;
                        editor.style.top = 0;
                        editor.style.bottom = 0;
                        editor.style.zIndex = 9999;
                        editor.style.backgroundColor = '#000';
                        document.body.appendChild(editor);

                        // Create confirm button at the top left of the viewport
                        var buttonConfirm = document.createElement('button');
                        buttonConfirm.style.position = 'absolute';
                        buttonConfirm.style.left = '10px';
                        buttonConfirm.style.top = '10px';
                        buttonConfirm.style.zIndex = 9999;
                        buttonConfirm.textContent = 'Onayla';
                        editor.appendChild(buttonConfirm);
                        buttonConfirm.addEventListener('click', function() {
                            // Get the canvas with image data from Cropper.js
                            var canvas = cropper.getCroppedCanvas();
                            // Turn the canvas into a Blob (file object without a name)
                            canvas.toBlob(function(blob) {
                                // Create a new Dropzone file thumbnail
                                myDropZone.createThumbnail(
                                    blob,
                                    myDropZone.options.thumbnailWidth,
                                    myDropZone.options.thumbnailHeight,
                                    myDropZone.options.thumbnailMethod,
                                    false, 
                                    function(dataURL) {
                                        // Update the Dropzone file thumbnail
                                        myDropZone.emit('thumbnail', file, dataURL);
                                        // Return the file to Dropzone
                                        done(blob);
                                    });
                                });
                            // Remove the editor from the view
                            document.body.removeChild(editor);
                        });

                        var buttonCancel = document.createElement('button');
                        buttonCancel.style.position = 'absolute';
                        buttonCancel.style.right = '10px';
                        buttonCancel.style.top = '10px';
                        buttonCancel.style.zIndex = 9999;
                        buttonCancel.textContent = 'İptal';
                        editor.appendChild(buttonCancel);

                        buttonCancel.addEventListener('click', function() {
                            editor.remove();
                        });

                        /*
                            var selectSize = document.createElement('select');
                            selectSize.setAttribute("id", "selectSize");
                            selectSize.style.position = 'absolute';
                            selectSize.style.left = '40%';
                            selectSize.style.top = '10px';
                            selectSize.style.width = '20%';
                            selectSize.style.zIndex = 9999;

                            var opt = document.createElement('option');
                            opt.setAttribute("label", "title");
                            opt.setAttribute("value", 0);
                            selectSize.appendChild(opt);

                            editor.appendChild(selectSize);
                            
                            selectSize.addEventListener('change', function() {
                                //console.log(this.value);
                                cropper.setAspectRatio(this.value);
                            });
                        */

                        // Create an image node for Cropper.js
                        var image = new Image();
                        image.src = URL.createObjectURL(file);
                        editor.appendChild(image);

                        // Create Cropper.js
                        var cropper = new Cropper(image, { viewMode: 1, aspectRatio: 3.36, zoomable: false, zoomOnTouch:false, zoomOnWheel:false, wheelZoomRatio:false, autoCropArea: 0.95 });
                    }

                })

                $('#kt_dropzone_season_after').dropzone({
                    acceptedFiles: "image/jpeg,image/png",
                    dictDefaultMessage: "Dosyaları yüklemek için buraya bırakın.",
                    dictFallbackMessage: "Tarayıcınız sürükle bırak dosya yüklemelerini desteklemiyor.",
                    dictFallbackText: "Please use the fallback form below to upload your files like in the olden days.",
                    dictFileTooBig: "Dosya yüklemek için çok büyük. Maksimum dosya boyutu: 7.5 MB",
                    dictInvalidFileType: "Bu tür dosyaları yükleyemezsiniz.",
                    dictCancelUpload: "Yüklemeyi iptal et.",
                    dictUploadCanceled: "Yükleme iptal edildi.",
                    dictCancelUploadConfirmation: "Bu yüklemeyi iptal etmek istediğinizden emin misiniz?",
                    dictRemoveFile: "Dosyayı kaldır.",
                    dictRemoveFileConfirmation: null,
                    dictMaxFilesExceeded: "Daha fazla dosya yükleyemezsiniz.",
                    dictFileSizeUnits: { tb: "TB", gb: "GB", mb: "MB", kb: "KB", b: "b" },
                    headers: { 'X-CSRF-TOKEN': $("input[name='_token']").val() },
                    url: "{{ url('/season_image/save') }}", // Set the url for your upload script location
                    paramName: "file", // The name that will be used to transfer the file
                    maxFiles: 10,
                    maxFilesize: 7.5, // MB
                    addRemoveLinks: true,
                    accept: function(file, done) {
                        done();
                    },
                    error: function error(file, message) {
                        thiss = this;
                        if (file.previewElement) {
                            file.previewElement.classList.add("dz-error");

                            if (typeof message !== "string" && message.error) {
                                message = message.error;
                            }

                            var _iterator7 = _createForOfIteratorHelper(file.previewElement.querySelectorAll("[data-dz-errormessage]")),
                            _step7;

                            try {
                                for (_iterator7.s(); !(_step7 = _iterator7.n()).done;) {
                                    var node = _step7.value;
                                    node.textContent = message;
                                }
                            } catch (err) {
                                _iterator7.e(err);
                            } finally {
                                _iterator7.f();
                            }

                            $( file.previewElement ).on( "click", function() {
                              thiss.removeFile(file);
                            });
                        }
                    },
                    init: function () {
                        //console.log('dropzone init');
                    },
                    sending: function(file, xhr, formData){
                        //formData.append('uploadType', 'a');
                        formData.append('sid', $("input[name='id']").val());
                        formData.append('season_status', 'after');
                    },
                    success: function(file, xhr, event){
                        var data = jQuery.parseJSON(xhr);
                        // <img class="img-fluid" src="{{ url('/upload') }}/' + data.location + '" />
                        $("#imgSeasonAfterImages").prepend('\
                            <div class="col-lg-4 col-sm-12">\
                                <div class="card card-custom overlay">\
                                    <div class="card-body p-0">\
                                        <div class="overlay-wrapper">\
                                          <img src="{{ env('MEDIA_END') }}' + data.location + '" alt="" class="w-100 rounded"/>\
                                        </div>\
                                        <div class="overlay-layer align-items-start justify-content-end pt-5 pr-5">\
                                            <a href="{{ url('season_image/delete/') }}/' + data.mid + '/' + data.id + '" class="btn btn-clean btn-icon" title="Fotoğrafı Sil" ><i class="fas fa-trash-alt"></i></a>\
                                        </div>\
                                    </div>\
                                </div>\
                                <div class="form-group row" style="margin-top: 0.75rem;">\
                                    <div class="col-lg-12">\
                                        <input class="form-control" type="text" name="season_after_image[' + data.id + ']" placeholder="Fotoğraf Açıklaması" max="160" value="">\
                                    </div>\
                                </div>\
                            </div>\
                        ');

                        //$("#cropPhotoButton").attr("onclick", "photoGalleryThumbnailCrop('{{ url('upload') }}/" + data.location + "', " + data.id + ")");
                        this.removeFile(file);
                    },
                    transformFile: function(file, done) {
                        var myDropZone = this;
                        // Create the image editor overlay
                        var editor = document.createElement('div');
                        editor.style.position = 'fixed';
                        editor.style.left = 0;
                        editor.style.right = 0;
                        editor.style.top = 0;
                        editor.style.bottom = 0;
                        editor.style.zIndex = 9999;
                        editor.style.backgroundColor = '#000';
                        document.body.appendChild(editor);

                        // Create confirm button at the top left of the viewport
                        var buttonConfirm = document.createElement('button');
                        buttonConfirm.style.position = 'absolute';
                        buttonConfirm.style.left = '10px';
                        buttonConfirm.style.top = '10px';
                        buttonConfirm.style.zIndex = 9999;
                        buttonConfirm.textContent = 'Onayla';
                        editor.appendChild(buttonConfirm);
                        buttonConfirm.addEventListener('click', function() {
                            // Get the canvas with image data from Cropper.js
                            var canvas = cropper.getCroppedCanvas();
                            // Turn the canvas into a Blob (file object without a name)
                            canvas.toBlob(function(blob) {
                                // Create a new Dropzone file thumbnail
                                myDropZone.createThumbnail(
                                    blob,
                                    myDropZone.options.thumbnailWidth,
                                    myDropZone.options.thumbnailHeight,
                                    myDropZone.options.thumbnailMethod,
                                    false, 
                                    function(dataURL) {
                                        // Update the Dropzone file thumbnail
                                        myDropZone.emit('thumbnail', file, dataURL);
                                        // Return the file to Dropzone
                                        done(blob);
                                    });
                                });
                            // Remove the editor from the view
                            document.body.removeChild(editor);
                        });

                        var buttonCancel = document.createElement('button');
                        buttonCancel.style.position = 'absolute';
                        buttonCancel.style.right = '10px';
                        buttonCancel.style.top = '10px';
                        buttonCancel.style.zIndex = 9999;
                        buttonCancel.textContent = 'İptal';
                        editor.appendChild(buttonCancel);

                        buttonCancel.addEventListener('click', function() {
                            editor.remove();
                        });

                        /*
                            var selectSize = document.createElement('select');
                            selectSize.setAttribute("id", "selectSize");
                            selectSize.style.position = 'absolute';
                            selectSize.style.left = '40%';
                            selectSize.style.top = '10px';
                            selectSize.style.width = '20%';
                            selectSize.style.zIndex = 9999;

                            var opt = document.createElement('option');
                            opt.setAttribute("label", "title");
                            opt.setAttribute("value", 0);
                            selectSize.appendChild(opt);

                            editor.appendChild(selectSize);
                            
                            selectSize.addEventListener('change', function() {
                                //console.log(this.value);
                                cropper.setAspectRatio(this.value);
                            });
                        */

                        // Create an image node for Cropper.js
                        var image = new Image();
                        image.src = URL.createObjectURL(file);
                        editor.appendChild(image);

                        // Create Cropper.js
                        var cropper = new Cropper(image, { viewMode: 1, aspectRatio: 1.5, zoomable: false, zoomOnTouch:false, zoomOnWheel:false, wheelZoomRatio:false, autoCropArea: 0.95 });
                    }

                })

                $('#kt_dropzone_season_after_slider').dropzone({
                    acceptedFiles: "image/jpeg,image/png",
                    dictDefaultMessage: "Dosyaları yüklemek için buraya bırakın.",
                    dictFallbackMessage: "Tarayıcınız sürükle bırak dosya yüklemelerini desteklemiyor.",
                    dictFallbackText: "Please use the fallback form below to upload your files like in the olden days.",
                    dictFileTooBig: "Dosya yüklemek için çok büyük. Maksimum dosya boyutu: 7.5 MB",
                    dictInvalidFileType: "Bu tür dosyaları yükleyemezsiniz.",
                    dictCancelUpload: "Yüklemeyi iptal et.",
                    dictUploadCanceled: "Yükleme iptal edildi.",
                    dictCancelUploadConfirmation: "Bu yüklemeyi iptal etmek istediğinizden emin misiniz?",
                    dictRemoveFile: "Dosyayı kaldır.",
                    dictRemoveFileConfirmation: null,
                    dictMaxFilesExceeded: "Daha fazla dosya yükleyemezsiniz.",
                    dictFileSizeUnits: { tb: "TB", gb: "GB", mb: "MB", kb: "KB", b: "b" },
                    headers: { 'X-CSRF-TOKEN': $("input[name='_token']").val() },
                    url: "{{ url('/season_image/save') }}", // Set the url for your upload script location
                    paramName: "file", // The name that will be used to transfer the file
                    maxFiles: 1,
                    maxFilesize: 7.5, // MB
                    addRemoveLinks: true,
                    accept: function(file, done) {
                        done();
                    },
                    error: function error(file, message) {
                        thiss = this;
                        if (file.previewElement) {
                            file.previewElement.classList.add("dz-error");

                            if (typeof message !== "string" && message.error) {
                                message = message.error;
                            }

                            var _iterator7 = _createForOfIteratorHelper(file.previewElement.querySelectorAll("[data-dz-errormessage]")),
                            _step7;

                            try {
                                for (_iterator7.s(); !(_step7 = _iterator7.n()).done;) {
                                    var node = _step7.value;
                                    node.textContent = message;
                                }
                            } catch (err) {
                                _iterator7.e(err);
                            } finally {
                                _iterator7.f();
                            }

                            $( file.previewElement ).on( "click", function() {
                              thiss.removeFile(file);
                            });
                        }
                    },
                    init: function () {
                        //console.log('dropzone init');
                    },
                    sending: function(file, xhr, formData){
                        //formData.append('uploadType', 'a');
                        formData.append('sid', $("input[name='id']").val());
                        formData.append('season_status', 'after_slide');
                    },
                    success: function(file, xhr, event){
                        var data = jQuery.parseJSON(xhr);
                        $("#imgSeasonAfterSlideImage").html('<img src="{{ env('MEDIA_END') }}/' + data.location + '" alt="" class="rounded" style="width: 200px; height: auto;" />');
                        //$("#cropPhotoButton").attr("onclick", "photoGalleryThumbnailCrop('{{ url('upload') }}/" + data.location + "', " + data.id + ")");
                        this.removeFile(file);
                    },
                    transformFile: function(file, done) {
                        var myDropZone = this;
                        // Create the image editor overlay
                        var editor = document.createElement('div');
                        editor.style.position = 'fixed';
                        editor.style.left = 0;
                        editor.style.right = 0;
                        editor.style.top = 0;
                        editor.style.bottom = 0;
                        editor.style.zIndex = 9999;
                        editor.style.backgroundColor = '#000';
                        document.body.appendChild(editor);

                        // Create confirm button at the top left of the viewport
                        var buttonConfirm = document.createElement('button');
                        buttonConfirm.style.position = 'absolute';
                        buttonConfirm.style.left = '10px';
                        buttonConfirm.style.top = '10px';
                        buttonConfirm.style.zIndex = 9999;
                        buttonConfirm.textContent = 'Onayla';
                        editor.appendChild(buttonConfirm);
                        buttonConfirm.addEventListener('click', function() {
                            // Get the canvas with image data from Cropper.js
                            var canvas = cropper.getCroppedCanvas();
                            // Turn the canvas into a Blob (file object without a name)
                            canvas.toBlob(function(blob) {
                                // Create a new Dropzone file thumbnail
                                myDropZone.createThumbnail(
                                    blob,
                                    myDropZone.options.thumbnailWidth,
                                    myDropZone.options.thumbnailHeight,
                                    myDropZone.options.thumbnailMethod,
                                    false, 
                                    function(dataURL) {
                                        // Update the Dropzone file thumbnail
                                        myDropZone.emit('thumbnail', file, dataURL);
                                        // Return the file to Dropzone
                                        done(blob);
                                    });
                                });
                            // Remove the editor from the view
                            document.body.removeChild(editor);
                        });

                        var buttonCancel = document.createElement('button');
                        buttonCancel.style.position = 'absolute';
                        buttonCancel.style.right = '10px';
                        buttonCancel.style.top = '10px';
                        buttonCancel.style.zIndex = 9999;
                        buttonCancel.textContent = 'İptal';
                        editor.appendChild(buttonCancel);

                        buttonCancel.addEventListener('click', function() {
                            editor.remove();
                        });

                        /*
                            var selectSize = document.createElement('select');
                            selectSize.setAttribute("id", "selectSize");
                            selectSize.style.position = 'absolute';
                            selectSize.style.left = '40%';
                            selectSize.style.top = '10px';
                            selectSize.style.width = '20%';
                            selectSize.style.zIndex = 9999;

                            var opt = document.createElement('option');
                            opt.setAttribute("label", "title");
                            opt.setAttribute("value", 0);
                            selectSize.appendChild(opt);

                            editor.appendChild(selectSize);
                            
                            selectSize.addEventListener('change', function() {
                                //console.log(this.value);
                                cropper.setAspectRatio(this.value);
                            });
                        */

                        // Create an image node for Cropper.js
                        var image = new Image();
                        image.src = URL.createObjectURL(file);
                        editor.appendChild(image);

                        // Create Cropper.js
                        var cropper = new Cropper(image, { viewMode: 1, aspectRatio: 3.36, zoomable: false, zoomOnTouch:false, zoomOnWheel:false, wheelZoomRatio:false, autoCropArea: 0.95 });
                    }

                })

                $('#kt_dropzone_season_after_teaser_img').dropzone({
                    acceptedFiles: "image/jpeg,image/png",
                    dictDefaultMessage: "Dosyaları yüklemek için buraya bırakın.",
                    dictFallbackMessage: "Tarayıcınız sürükle bırak dosya yüklemelerini desteklemiyor.",
                    dictFallbackText: "Please use the fallback form below to upload your files like in the olden days.",
                    dictFileTooBig: "Dosya yüklemek için çok büyük. Maksimum dosya boyutu: 7.5 MB",
                    dictInvalidFileType: "Bu tür dosyaları yükleyemezsiniz.",
                    dictCancelUpload: "Yüklemeyi iptal et.",
                    dictUploadCanceled: "Yükleme iptal edildi.",
                    dictCancelUploadConfirmation: "Bu yüklemeyi iptal etmek istediğinizden emin misiniz?",
                    dictRemoveFile: "Dosyayı kaldır.",
                    dictRemoveFileConfirmation: null,
                    dictMaxFilesExceeded: "Daha fazla dosya yükleyemezsiniz.",
                    dictFileSizeUnits: { tb: "TB", gb: "GB", mb: "MB", kb: "KB", b: "b" },
                    headers: { 'X-CSRF-TOKEN': $("input[name='_token']").val() },
                    url: "{{ url('/season_image/save') }}", // Set the url for your upload script location
                    paramName: "file", // The name that will be used to transfer the file
                    maxFiles: 1,
                    maxFilesize: 7.5, // MB
                    addRemoveLinks: true,
                    accept: function(file, done) {
                        done();
                    },
                    error: function error(file, message) {
                        thiss = this;
                        if (file.previewElement) {
                            file.previewElement.classList.add("dz-error");

                            if (typeof message !== "string" && message.error) {
                                message = message.error;
                            }

                            var _iterator7 = _createForOfIteratorHelper(file.previewElement.querySelectorAll("[data-dz-errormessage]")),
                            _step7;

                            try {
                                for (_iterator7.s(); !(_step7 = _iterator7.n()).done;) {
                                    var node = _step7.value;
                                    node.textContent = message;
                                }
                            } catch (err) {
                                _iterator7.e(err);
                            } finally {
                                _iterator7.f();
                            }

                            $( file.previewElement ).on( "click", function() {
                              thiss.removeFile(file);
                            });
                        }
                    },
                    init: function () {
                        //console.log('dropzone init');
                    },
                    sending: function(file, xhr, formData){
                        //formData.append('uploadType', 'a');
                        formData.append('sid', $("input[name='id']").val());
                        formData.append('season_status', 'after_teaser_img');
                    },
                    success: function(file, xhr, event){
                        var data = jQuery.parseJSON(xhr);
                        $("#imgSeasonAfterTeaserImage").html('<img src="{{ env('MEDIA_END') }}/' + data.location + '" alt="" class="rounded" style="width: 200px; height: auto;" />');
                        //$("#cropPhotoButton").attr("onclick", "photoGalleryThumbnailCrop('{{ url('upload') }}/" + data.location + "', " + data.id + ")");
                        this.removeFile(file);
                    },
                    transformFile: function(file, done) {
                        var myDropZone = this;
                        // Create the image editor overlay
                        var editor = document.createElement('div');
                        editor.style.position = 'fixed';
                        editor.style.left = 0;
                        editor.style.right = 0;
                        editor.style.top = 0;
                        editor.style.bottom = 0;
                        editor.style.zIndex = 9999;
                        editor.style.backgroundColor = '#000';
                        document.body.appendChild(editor);

                        // Create confirm button at the top left of the viewport
                        var buttonConfirm = document.createElement('button');
                        buttonConfirm.style.position = 'absolute';
                        buttonConfirm.style.left = '10px';
                        buttonConfirm.style.top = '10px';
                        buttonConfirm.style.zIndex = 9999;
                        buttonConfirm.textContent = 'Onayla';
                        editor.appendChild(buttonConfirm);
                        buttonConfirm.addEventListener('click', function() {
                            // Get the canvas with image data from Cropper.js
                            var canvas = cropper.getCroppedCanvas();
                            // Turn the canvas into a Blob (file object without a name)
                            canvas.toBlob(function(blob) {
                                // Create a new Dropzone file thumbnail
                                myDropZone.createThumbnail(
                                    blob,
                                    myDropZone.options.thumbnailWidth,
                                    myDropZone.options.thumbnailHeight,
                                    myDropZone.options.thumbnailMethod,
                                    false, 
                                    function(dataURL) {
                                        // Update the Dropzone file thumbnail
                                        myDropZone.emit('thumbnail', file, dataURL);
                                        // Return the file to Dropzone
                                        done(blob);
                                    });
                                });
                            // Remove the editor from the view
                            document.body.removeChild(editor);
                        });

                        var buttonCancel = document.createElement('button');
                        buttonCancel.style.position = 'absolute';
                        buttonCancel.style.right = '10px';
                        buttonCancel.style.top = '10px';
                        buttonCancel.style.zIndex = 9999;
                        buttonCancel.textContent = 'İptal';
                        editor.appendChild(buttonCancel);

                        buttonCancel.addEventListener('click', function() {
                            editor.remove();
                        });

                        /*
                            var selectSize = document.createElement('select');
                            selectSize.setAttribute("id", "selectSize");
                            selectSize.style.position = 'absolute';
                            selectSize.style.left = '40%';
                            selectSize.style.top = '10px';
                            selectSize.style.width = '20%';
                            selectSize.style.zIndex = 9999;

                            var opt = document.createElement('option');
                            opt.setAttribute("label", "title");
                            opt.setAttribute("value", 0);
                            selectSize.appendChild(opt);

                            editor.appendChild(selectSize);
                            
                            selectSize.addEventListener('change', function() {
                                //console.log(this.value);
                                cropper.setAspectRatio(this.value);
                            });
                        */

                        // Create an image node for Cropper.js
                        var image = new Image();
                        image.src = URL.createObjectURL(file);
                        editor.appendChild(image);

                        // Create Cropper.js
                        var cropper = new Cropper(image, { viewMode: 1, aspectRatio: 0, zoomable: false, zoomOnTouch:false, zoomOnWheel:false, wheelZoomRatio:false, autoCropArea: 0.95 });
                    }

                })

                $('#kt_dropzone_season_before_teaser_img').dropzone({
                    acceptedFiles: "image/jpeg,image/png",
                    dictDefaultMessage: "Dosyaları yüklemek için buraya bırakın.",
                    dictFallbackMessage: "Tarayıcınız sürükle bırak dosya yüklemelerini desteklemiyor.",
                    dictFallbackText: "Please use the fallback form below to upload your files like in the olden days.",
                    dictFileTooBig: "Dosya yüklemek için çok büyük. Maksimum dosya boyutu: 7.5 MB",
                    dictInvalidFileType: "Bu tür dosyaları yükleyemezsiniz.",
                    dictCancelUpload: "Yüklemeyi iptal et.",
                    dictUploadCanceled: "Yükleme iptal edildi.",
                    dictCancelUploadConfirmation: "Bu yüklemeyi iptal etmek istediğinizden emin misiniz?",
                    dictRemoveFile: "Dosyayı kaldır.",
                    dictRemoveFileConfirmation: null,
                    dictMaxFilesExceeded: "Daha fazla dosya yükleyemezsiniz.",
                    dictFileSizeUnits: { tb: "TB", gb: "GB", mb: "MB", kb: "KB", b: "b" },
                    headers: { 'X-CSRF-TOKEN': $("input[name='_token']").val() },
                    url: "{{ url('/season_image/save') }}", // Set the url for your upload script location
                    paramName: "file", // The name that will be used to transfer the file
                    maxFiles: 1,
                    maxFilesize: 7.5, // MB
                    addRemoveLinks: true,
                    accept: function(file, done) {
                        done();
                    },
                    error: function error(file, message) {
                        thiss = this;
                        if (file.previewElement) {
                            file.previewElement.classList.add("dz-error");

                            if (typeof message !== "string" && message.error) {
                                message = message.error;
                            }

                            var _iterator7 = _createForOfIteratorHelper(file.previewElement.querySelectorAll("[data-dz-errormessage]")),
                            _step7;

                            try {
                                for (_iterator7.s(); !(_step7 = _iterator7.n()).done;) {
                                    var node = _step7.value;
                                    node.textContent = message;
                                }
                            } catch (err) {
                                _iterator7.e(err);
                            } finally {
                                _iterator7.f();
                            }

                            $( file.previewElement ).on( "click", function() {
                              thiss.removeFile(file);
                            });
                        }
                    },
                    init: function () {
                        //console.log('dropzone init');
                    },
                    sending: function(file, xhr, formData){
                        //formData.append('uploadType', 'a');
                        formData.append('sid', $("input[name='id']").val());
                        formData.append('season_status', 'before_teaser_img');
                    },
                    success: function(file, xhr, event){
                        var data = jQuery.parseJSON(xhr);
                        $("#imgSeasonBeforeTeaserImage").html('<img src="{{ env('MEDIA_END') }}/' + data.location + '" alt="" class="rounded" style="width: 200px; height: auto;" />');
                        //$("#cropPhotoButton").attr("onclick", "photoGalleryThumbnailCrop('{{ url('upload') }}/" + data.location + "', " + data.id + ")");
                        this.removeFile(file);
                    },
                    transformFile: function(file, done) {
                        var myDropZone = this;
                        // Create the image editor overlay
                        var editor = document.createElement('div');
                        editor.style.position = 'fixed';
                        editor.style.left = 0;
                        editor.style.right = 0;
                        editor.style.top = 0;
                        editor.style.bottom = 0;
                        editor.style.zIndex = 9999;
                        editor.style.backgroundColor = '#000';
                        document.body.appendChild(editor);

                        // Create confirm button at the top left of the viewport
                        var buttonConfirm = document.createElement('button');
                        buttonConfirm.style.position = 'absolute';
                        buttonConfirm.style.left = '10px';
                        buttonConfirm.style.top = '10px';
                        buttonConfirm.style.zIndex = 9999;
                        buttonConfirm.textContent = 'Onayla';
                        editor.appendChild(buttonConfirm);
                        buttonConfirm.addEventListener('click', function() {
                            // Get the canvas with image data from Cropper.js
                            var canvas = cropper.getCroppedCanvas();
                            // Turn the canvas into a Blob (file object without a name)
                            canvas.toBlob(function(blob) {
                                // Create a new Dropzone file thumbnail
                                myDropZone.createThumbnail(
                                    blob,
                                    myDropZone.options.thumbnailWidth,
                                    myDropZone.options.thumbnailHeight,
                                    myDropZone.options.thumbnailMethod,
                                    false, 
                                    function(dataURL) {
                                        // Update the Dropzone file thumbnail
                                        myDropZone.emit('thumbnail', file, dataURL);
                                        // Return the file to Dropzone
                                        done(blob);
                                    });
                                });
                            // Remove the editor from the view
                            document.body.removeChild(editor);
                        });

                        var buttonCancel = document.createElement('button');
                        buttonCancel.style.position = 'absolute';
                        buttonCancel.style.right = '10px';
                        buttonCancel.style.top = '10px';
                        buttonCancel.style.zIndex = 9999;
                        buttonCancel.textContent = 'İptal';
                        editor.appendChild(buttonCancel);

                        buttonCancel.addEventListener('click', function() {
                            editor.remove();
                        });

                        /*
                            var selectSize = document.createElement('select');
                            selectSize.setAttribute("id", "selectSize");
                            selectSize.style.position = 'absolute';
                            selectSize.style.left = '40%';
                            selectSize.style.top = '10px';
                            selectSize.style.width = '20%';
                            selectSize.style.zIndex = 9999;

                            var opt = document.createElement('option');
                            opt.setAttribute("label", "title");
                            opt.setAttribute("value", 0);
                            selectSize.appendChild(opt);

                            editor.appendChild(selectSize);
                            
                            selectSize.addEventListener('change', function() {
                                //console.log(this.value);
                                cropper.setAspectRatio(this.value);
                            });
                        */

                        // Create an image node for Cropper.js
                        var image = new Image();
                        image.src = URL.createObjectURL(file);
                        editor.appendChild(image);

                        // Create Cropper.js
                        var cropper = new Cropper(image, { viewMode: 1, aspectRatio: 0, zoomable: false, zoomOnTouch:false, zoomOnWheel:false, wheelZoomRatio:false, autoCropArea: 0.95 });
                    }

                })

                $('#kt_dropzone_bannerphoto').dropzone({
                    acceptedFiles: "image/jpeg,image/png",
                    dictDefaultMessage: "Dosyaları yüklemek için buraya bırakın.",
                    dictFallbackMessage: "Tarayıcınız sürükle bırak dosya yüklemelerini desteklemiyor.",
                    dictFallbackText: "Please use the fallback form below to upload your files like in the olden days.",
                    dictFileTooBig: "Dosya yüklemek için çok büyük. Maksimum dosya boyutu: 7.5 MB",
                    dictInvalidFileType: "Bu tür dosyaları yükleyemezsiniz.",
                    dictCancelUpload: "Yüklemeyi iptal et.",
                    dictUploadCanceled: "Yükleme iptal edildi.",
                    dictCancelUploadConfirmation: "Bu yüklemeyi iptal etmek istediğinizden emin misiniz?",
                    dictRemoveFile: "Dosyayı kaldır.",
                    dictRemoveFileConfirmation: null,
                    dictMaxFilesExceeded: "Daha fazla dosya yükleyemezsiniz.",
                    dictFileSizeUnits: { tb: "TB", gb: "GB", mb: "MB", kb: "KB", b: "b" },
                    headers: { 'X-CSRF-TOKEN': $("input[name='_token']").val() },
                    url: "{{ url('/banners/image_save') }}", // Set the url for your upload script location
                    paramName: "file", // The name that will be used to transfer the file
                    maxFiles: 1,
                    maxFilesize: 7.5, // MB
                    addRemoveLinks: true,
                    accept: function(file, done) {
                        done();
                    },
                    error: function error(file, message) {
                        thiss = this;
                        if (file.previewElement) {
                            file.previewElement.classList.add("dz-error");

                            if (typeof message !== "string" && message.error) {
                                message = message.error;
                            }

                            var _iterator7 = _createForOfIteratorHelper(file.previewElement.querySelectorAll("[data-dz-errormessage]")),
                            _step7;

                            try {
                                for (_iterator7.s(); !(_step7 = _iterator7.n()).done;) {
                                    var node = _step7.value;
                                    node.textContent = message;
                                }
                            } catch (err) {
                                _iterator7.e(err);
                            } finally {
                                _iterator7.f();
                            }

                            $( file.previewElement ).on( "click", function() {
                              thiss.removeFile(file);
                            });
                        }
                    },
                    init: function () {
                        //console.log('dropzone init');
                    },
                    sending: function(file, xhr, formData){
                        //formData.append('uploadType', 'a');
                        formData.append('uid', $("input[name='id']").val());
                    },
                    success: function(file, xhr, event){
                        var data = jQuery.parseJSON(xhr);
                        $("#imgBannerPhoto").html('<img src="{{ env('MEDIA_END') }}/' + data.location + '" alt="" class="rounded" style="width: 200px; height: auto;" />');
                        //$("#cropPhotoButton").attr("onclick", "photoGalleryThumbnailCrop('{{ url('upload') }}/" + data.location + "', " + data.id + ")");
                        this.removeFile(file);
                    }

                })

                $('#kt_dropzone_info_slide_photo').dropzone({
                    acceptedFiles: "image/jpeg,image/png",
                    dictDefaultMessage: "Dosyaları yüklemek için buraya bırakın.",
                    dictFallbackMessage: "Tarayıcınız sürükle bırak dosya yüklemelerini desteklemiyor.",
                    dictFallbackText: "Please use the fallback form below to upload your files like in the olden days.",
                    dictFileTooBig: "Dosya yüklemek için çok büyük. Maksimum dosya boyutu: 7.5 MB",
                    dictInvalidFileType: "Bu tür dosyaları yükleyemezsiniz.",
                    dictCancelUpload: "Yüklemeyi iptal et.",
                    dictUploadCanceled: "Yükleme iptal edildi.",
                    dictCancelUploadConfirmation: "Bu yüklemeyi iptal etmek istediğinizden emin misiniz?",
                    dictRemoveFile: "Dosyayı kaldır.",
                    dictRemoveFileConfirmation: null,
                    dictMaxFilesExceeded: "Daha fazla dosya yükleyemezsiniz.",
                    dictFileSizeUnits: { tb: "TB", gb: "GB", mb: "MB", kb: "KB", b: "b" },
                    headers: { 'X-CSRF-TOKEN': $("input[name='_token']").val() },
                    url: "{{ url('/season_image/save') }}", // Set the url for your upload script location
                    paramName: "file", // The name that will be used to transfer the file
                    maxFiles: 1,
                    maxFilesize: 7.5, // MB
                    addRemoveLinks: true,
                    accept: function(file, done) {
                        done();
                    },
                    error: function error(file, message) {
                        thiss = this;
                        if (file.previewElement) {
                            file.previewElement.classList.add("dz-error");

                            if (typeof message !== "string" && message.error) {
                                message = message.error;
                            }

                            var _iterator7 = _createForOfIteratorHelper(file.previewElement.querySelectorAll("[data-dz-errormessage]")),
                            _step7;

                            try {
                                for (_iterator7.s(); !(_step7 = _iterator7.n()).done;) {
                                    var node = _step7.value;
                                    node.textContent = message;
                                }
                            } catch (err) {
                                _iterator7.e(err);
                            } finally {
                                _iterator7.f();
                            }

                            $( file.previewElement ).on( "click", function() {
                              thiss.removeFile(file);
                            });
                        }
                    },
                    init: function () {
                        //console.log('dropzone init');
                    },
                    sending: function(file, xhr, formData){
                        //formData.append('uploadType', 'a');
                        formData.append('sid', $("input[name='id']").val());
                        formData.append('season_status', 'info_slide_photo');
                    },
                    success: function(file, xhr, event){
                        var data = jQuery.parseJSON(xhr);
                        $("#imgSeasonInfoSlidePhoto").html('<img src="{{ env('MEDIA_END') }}/' + data.location + '" alt="" class="rounded" style="width: 200px; height: auto;" />');
                        //$("#cropPhotoButton").attr("onclick", "photoGalleryThumbnailCrop('{{ url('upload') }}/" + data.location + "', " + data.id + ")");
                        this.removeFile(file);
                    },
                    transformFile: function(file, done) {
                        var myDropZone = this;
                        // Create the image editor overlay
                        var editor = document.createElement('div');
                        editor.style.position = 'fixed';
                        editor.style.left = 0;
                        editor.style.right = 0;
                        editor.style.top = 0;
                        editor.style.bottom = 0;
                        editor.style.zIndex = 9999;
                        editor.style.backgroundColor = '#000';
                        document.body.appendChild(editor);

                        // Create confirm button at the top left of the viewport
                        var buttonConfirm = document.createElement('button');
                        buttonConfirm.style.position = 'absolute';
                        buttonConfirm.style.left = '10px';
                        buttonConfirm.style.top = '10px';
                        buttonConfirm.style.zIndex = 9999;
                        buttonConfirm.textContent = 'Onayla';
                        editor.appendChild(buttonConfirm);
                        buttonConfirm.addEventListener('click', function() {
                            // Get the canvas with image data from Cropper.js
                            var canvas = cropper.getCroppedCanvas();
                            // Turn the canvas into a Blob (file object without a name)
                            canvas.toBlob(function(blob) {
                                // Create a new Dropzone file thumbnail
                                myDropZone.createThumbnail(
                                    blob,
                                    myDropZone.options.thumbnailWidth,
                                    myDropZone.options.thumbnailHeight,
                                    myDropZone.options.thumbnailMethod,
                                    false, 
                                    function(dataURL) {
                                        // Update the Dropzone file thumbnail
                                        myDropZone.emit('thumbnail', file, dataURL);
                                        // Return the file to Dropzone
                                        done(blob);
                                    });
                                });
                            // Remove the editor from the view
                            document.body.removeChild(editor);
                        });

                        var buttonCancel = document.createElement('button');
                        buttonCancel.style.position = 'absolute';
                        buttonCancel.style.right = '10px';
                        buttonCancel.style.top = '10px';
                        buttonCancel.style.zIndex = 9999;
                        buttonCancel.textContent = 'İptal';
                        editor.appendChild(buttonCancel);

                        buttonCancel.addEventListener('click', function() {
                            editor.remove();
                        });

                        /*
                            var selectSize = document.createElement('select');
                            selectSize.setAttribute("id", "selectSize");
                            selectSize.style.position = 'absolute';
                            selectSize.style.left = '40%';
                            selectSize.style.top = '10px';
                            selectSize.style.width = '20%';
                            selectSize.style.zIndex = 9999;

                            var opt = document.createElement('option');
                            opt.setAttribute("label", "title");
                            opt.setAttribute("value", 0);
                            selectSize.appendChild(opt);

                            editor.appendChild(selectSize);
                            
                            selectSize.addEventListener('change', function() {
                                //console.log(this.value);
                                cropper.setAspectRatio(this.value);
                            });
                        */

                        // Create an image node for Cropper.js
                        var image = new Image();
                        image.src = URL.createObjectURL(file);
                        editor.appendChild(image);

                        // Create Cropper.js
                        var cropper = new Cropper(image, { viewMode: 1, aspectRatio: 3.36, zoomable: false, zoomOnTouch:false, zoomOnWheel:false, wheelZoomRatio:false, autoCropArea: 0.95 });
                    }

                })

                $('#kt_dropzone_info_img1').dropzone({
                    acceptedFiles: "image/jpeg,image/png",
                    dictDefaultMessage: "Dosyaları yüklemek için buraya bırakın.",
                    dictFallbackMessage: "Tarayıcınız sürükle bırak dosya yüklemelerini desteklemiyor.",
                    dictFallbackText: "Please use the fallback form below to upload your files like in the olden days.",
                    dictFileTooBig: "Dosya yüklemek için çok büyük. Maksimum dosya boyutu: 7.5 MB",
                    dictInvalidFileType: "Bu tür dosyaları yükleyemezsiniz.",
                    dictCancelUpload: "Yüklemeyi iptal et.",
                    dictUploadCanceled: "Yükleme iptal edildi.",
                    dictCancelUploadConfirmation: "Bu yüklemeyi iptal etmek istediğinizden emin misiniz?",
                    dictRemoveFile: "Dosyayı kaldır.",
                    dictRemoveFileConfirmation: null,
                    dictMaxFilesExceeded: "Daha fazla dosya yükleyemezsiniz.",
                    dictFileSizeUnits: { tb: "TB", gb: "GB", mb: "MB", kb: "KB", b: "b" },
                    headers: { 'X-CSRF-TOKEN': $("input[name='_token']").val() },
                    url: "{{ url('/season_image/save') }}", // Set the url for your upload script location
                    paramName: "file", // The name that will be used to transfer the file
                    maxFiles: 1,
                    maxFilesize: 7.5, // MB
                    addRemoveLinks: true,
                    accept: function(file, done) {
                        done();
                    },
                    error: function error(file, message) {
                        thiss = this;
                        if (file.previewElement) {
                            file.previewElement.classList.add("dz-error");

                            if (typeof message !== "string" && message.error) {
                                message = message.error;
                            }

                            var _iterator7 = _createForOfIteratorHelper(file.previewElement.querySelectorAll("[data-dz-errormessage]")),
                            _step7;

                            try {
                                for (_iterator7.s(); !(_step7 = _iterator7.n()).done;) {
                                    var node = _step7.value;
                                    node.textContent = message;
                                }
                            } catch (err) {
                                _iterator7.e(err);
                            } finally {
                                _iterator7.f();
                            }

                            $( file.previewElement ).on( "click", function() {
                              thiss.removeFile(file);
                            });
                        }
                    },
                    init: function () {
                        //console.log('dropzone init');
                    },
                    sending: function(file, xhr, formData){
                        //formData.append('uploadType', 'a');
                        formData.append('sid', $("input[name='id']").val());
                        formData.append('season_status', 'info_img1');
                    },
                    success: function(file, xhr, event){
                        var data = jQuery.parseJSON(xhr);
                        $("#imgSeasonInfoImg1").html('<img src="{{ env('MEDIA_END') }}/' + data.location + '" alt="" class="rounded" style="width: 200px; height: auto;" />');
                        //$("#cropPhotoButton").attr("onclick", "photoGalleryThumbnailCrop('{{ url('upload') }}/" + data.location + "', " + data.id + ")");
                        this.removeFile(file);
                    },
                    transformFile: function(file, done) {
                        var myDropZone = this;
                        // Create the image editor overlay
                        var editor = document.createElement('div');
                        editor.style.position = 'fixed';
                        editor.style.left = 0;
                        editor.style.right = 0;
                        editor.style.top = 0;
                        editor.style.bottom = 0;
                        editor.style.zIndex = 9999;
                        editor.style.backgroundColor = '#000';
                        document.body.appendChild(editor);

                        // Create confirm button at the top left of the viewport
                        var buttonConfirm = document.createElement('button');
                        buttonConfirm.style.position = 'absolute';
                        buttonConfirm.style.left = '10px';
                        buttonConfirm.style.top = '10px';
                        buttonConfirm.style.zIndex = 9999;
                        buttonConfirm.textContent = 'Onayla';
                        editor.appendChild(buttonConfirm);
                        buttonConfirm.addEventListener('click', function() {
                            // Get the canvas with image data from Cropper.js
                            var canvas = cropper.getCroppedCanvas();
                            // Turn the canvas into a Blob (file object without a name)
                            canvas.toBlob(function(blob) {
                                // Create a new Dropzone file thumbnail
                                myDropZone.createThumbnail(
                                    blob,
                                    myDropZone.options.thumbnailWidth,
                                    myDropZone.options.thumbnailHeight,
                                    myDropZone.options.thumbnailMethod,
                                    false, 
                                    function(dataURL) {
                                        // Update the Dropzone file thumbnail
                                        myDropZone.emit('thumbnail', file, dataURL);
                                        // Return the file to Dropzone
                                        done(blob);
                                    });
                                });
                            // Remove the editor from the view
                            document.body.removeChild(editor);
                        });

                        var buttonCancel = document.createElement('button');
                        buttonCancel.style.position = 'absolute';
                        buttonCancel.style.right = '10px';
                        buttonCancel.style.top = '10px';
                        buttonCancel.style.zIndex = 9999;
                        buttonCancel.textContent = 'İptal';
                        editor.appendChild(buttonCancel);

                        buttonCancel.addEventListener('click', function() {
                            editor.remove();
                        });

                        /*
                            var selectSize = document.createElement('select');
                            selectSize.setAttribute("id", "selectSize");
                            selectSize.style.position = 'absolute';
                            selectSize.style.left = '40%';
                            selectSize.style.top = '10px';
                            selectSize.style.width = '20%';
                            selectSize.style.zIndex = 9999;

                            var opt = document.createElement('option');
                            opt.setAttribute("label", "title");
                            opt.setAttribute("value", 0);
                            selectSize.appendChild(opt);

                            editor.appendChild(selectSize);
                            
                            selectSize.addEventListener('change', function() {
                                //console.log(this.value);
                                cropper.setAspectRatio(this.value);
                            });
                        */

                        // Create an image node for Cropper.js
                        var image = new Image();
                        image.src = URL.createObjectURL(file);
                        editor.appendChild(image);

                        // Create Cropper.js
                        var cropper = new Cropper(image, { viewMode: 1, aspectRatio: 1.5, zoomable: false, zoomOnTouch:false, zoomOnWheel:false, wheelZoomRatio:false, autoCropArea: 0.95 });
                    }

                })

                $('#kt_dropzone_info_img2').dropzone({
                    acceptedFiles: "image/jpeg,image/png",
                    dictDefaultMessage: "Dosyaları yüklemek için buraya bırakın.",
                    dictFallbackMessage: "Tarayıcınız sürükle bırak dosya yüklemelerini desteklemiyor.",
                    dictFallbackText: "Please use the fallback form below to upload your files like in the olden days.",
                    dictFileTooBig: "Dosya yüklemek için çok büyük. Maksimum dosya boyutu: 7.5 MB",
                    dictInvalidFileType: "Bu tür dosyaları yükleyemezsiniz.",
                    dictCancelUpload: "Yüklemeyi iptal et.",
                    dictUploadCanceled: "Yükleme iptal edildi.",
                    dictCancelUploadConfirmation: "Bu yüklemeyi iptal etmek istediğinizden emin misiniz?",
                    dictRemoveFile: "Dosyayı kaldır.",
                    dictRemoveFileConfirmation: null,
                    dictMaxFilesExceeded: "Daha fazla dosya yükleyemezsiniz.",
                    dictFileSizeUnits: { tb: "TB", gb: "GB", mb: "MB", kb: "KB", b: "b" },
                    headers: { 'X-CSRF-TOKEN': $("input[name='_token']").val() },
                    url: "{{ url('/season_image/save') }}", // Set the url for your upload script location
                    paramName: "file", // The name that will be used to transfer the file
                    maxFiles: 1,
                    maxFilesize: 7.5, // MB
                    addRemoveLinks: true,
                    accept: function(file, done) {
                        done();
                    },
                    error: function error(file, message) {
                        thiss = this;
                        if (file.previewElement) {
                            file.previewElement.classList.add("dz-error");

                            if (typeof message !== "string" && message.error) {
                                message = message.error;
                            }

                            var _iterator7 = _createForOfIteratorHelper(file.previewElement.querySelectorAll("[data-dz-errormessage]")),
                            _step7;

                            try {
                                for (_iterator7.s(); !(_step7 = _iterator7.n()).done;) {
                                    var node = _step7.value;
                                    node.textContent = message;
                                }
                            } catch (err) {
                                _iterator7.e(err);
                            } finally {
                                _iterator7.f();
                            }

                            $( file.previewElement ).on( "click", function() {
                              thiss.removeFile(file);
                            });
                        }
                    },
                    init: function () {
                        //console.log('dropzone init');
                    },
                    sending: function(file, xhr, formData){
                        //formData.append('uploadType', 'a');
                        formData.append('sid', $("input[name='id']").val());
                        formData.append('season_status', 'info_img2');
                    },
                    success: function(file, xhr, event){
                        var data = jQuery.parseJSON(xhr);
                        $("#imgSeasonInfoImg2").html('<img src="{{ env('MEDIA_END') }}/' + data.location + '" alt="" class="rounded" style="width: 200px; height: auto;" />');
                        //$("#cropPhotoButton").attr("onclick", "photoGalleryThumbnailCrop('{{ url('upload') }}/" + data.location + "', " + data.id + ")");
                        this.removeFile(file);
                    },
                    transformFile: function(file, done) {
                        var myDropZone = this;
                        // Create the image editor overlay
                        var editor = document.createElement('div');
                        editor.style.position = 'fixed';
                        editor.style.left = 0;
                        editor.style.right = 0;
                        editor.style.top = 0;
                        editor.style.bottom = 0;
                        editor.style.zIndex = 9999;
                        editor.style.backgroundColor = '#000';
                        document.body.appendChild(editor);

                        // Create confirm button at the top left of the viewport
                        var buttonConfirm = document.createElement('button');
                        buttonConfirm.style.position = 'absolute';
                        buttonConfirm.style.left = '10px';
                        buttonConfirm.style.top = '10px';
                        buttonConfirm.style.zIndex = 9999;
                        buttonConfirm.textContent = 'Onayla';
                        editor.appendChild(buttonConfirm);
                        buttonConfirm.addEventListener('click', function() {
                            // Get the canvas with image data from Cropper.js
                            var canvas = cropper.getCroppedCanvas();
                            // Turn the canvas into a Blob (file object without a name)
                            canvas.toBlob(function(blob) {
                                // Create a new Dropzone file thumbnail
                                myDropZone.createThumbnail(
                                    blob,
                                    myDropZone.options.thumbnailWidth,
                                    myDropZone.options.thumbnailHeight,
                                    myDropZone.options.thumbnailMethod,
                                    false, 
                                    function(dataURL) {
                                        // Update the Dropzone file thumbnail
                                        myDropZone.emit('thumbnail', file, dataURL);
                                        // Return the file to Dropzone
                                        done(blob);
                                    });
                                });
                            // Remove the editor from the view
                            document.body.removeChild(editor);
                        });

                        var buttonCancel = document.createElement('button');
                        buttonCancel.style.position = 'absolute';
                        buttonCancel.style.right = '10px';
                        buttonCancel.style.top = '10px';
                        buttonCancel.style.zIndex = 9999;
                        buttonCancel.textContent = 'İptal';
                        editor.appendChild(buttonCancel);

                        buttonCancel.addEventListener('click', function() {
                            editor.remove();
                        });

                        /*
                            var selectSize = document.createElement('select');
                            selectSize.setAttribute("id", "selectSize");
                            selectSize.style.position = 'absolute';
                            selectSize.style.left = '40%';
                            selectSize.style.top = '10px';
                            selectSize.style.width = '20%';
                            selectSize.style.zIndex = 9999;

                            var opt = document.createElement('option');
                            opt.setAttribute("label", "title");
                            opt.setAttribute("value", 0);
                            selectSize.appendChild(opt);

                            editor.appendChild(selectSize);
                            
                            selectSize.addEventListener('change', function() {
                                //console.log(this.value);
                                cropper.setAspectRatio(this.value);
                            });
                        */

                        // Create an image node for Cropper.js
                        var image = new Image();
                        image.src = URL.createObjectURL(file);
                        editor.appendChild(image);

                        // Create Cropper.js
                        var cropper = new Cropper(image, { viewMode: 1, aspectRatio: 1.5, zoomable: false, zoomOnTouch:false, zoomOnWheel:false, wheelZoomRatio:false, autoCropArea: 0.95 });
                    }

                })

                $('#kt_dropzone_agendaphoto').dropzone({
                    acceptedFiles: "image/jpeg,image/png",
                    dictDefaultMessage: "Dosyaları yüklemek için buraya bırakın.",
                    dictFallbackMessage: "Tarayıcınız sürükle bırak dosya yüklemelerini desteklemiyor.",
                    dictFallbackText: "Please use the fallback form below to upload your files like in the olden days.",
                    dictFileTooBig: "Dosya yüklemek için çok büyük. Maksimum dosya boyutu: 7.5 MB",
                    dictInvalidFileType: "Bu tür dosyaları yükleyemezsiniz.",
                    dictCancelUpload: "Yüklemeyi iptal et.",
                    dictUploadCanceled: "Yükleme iptal edildi.",
                    dictCancelUploadConfirmation: "Bu yüklemeyi iptal etmek istediğinizden emin misiniz?",
                    dictRemoveFile: "Dosyayı kaldır.",
                    dictRemoveFileConfirmation: null,
                    dictMaxFilesExceeded: "Daha fazla dosya yükleyemezsiniz.",
                    dictFileSizeUnits: { tb: "TB", gb: "GB", mb: "MB", kb: "KB", b: "b" },
                    headers: { 'X-CSRF-TOKEN': $("input[name='_token']").val() },
                    url: "{{ url('/agenda/photo_save') }}", // Set the url for your upload script location
                    paramName: "file", // The name that will be used to transfer the file
                    maxFiles: 1,
                    maxFilesize: 7.5, // MB
                    addRemoveLinks: true,
                    accept: function(file, done) {
                        done();
                    },
                    error: function error(file, message) {
                        thiss = this;
                        if (file.previewElement) {
                            file.previewElement.classList.add("dz-error");

                            if (typeof message !== "string" && message.error) {
                                message = message.error;
                            }

                            var _iterator7 = _createForOfIteratorHelper(file.previewElement.querySelectorAll("[data-dz-errormessage]")),
                            _step7;

                            try {
                                for (_iterator7.s(); !(_step7 = _iterator7.n()).done;) {
                                    var node = _step7.value;
                                    node.textContent = message;
                                }
                            } catch (err) {
                                _iterator7.e(err);
                            } finally {
                                _iterator7.f();
                            }

                            $( file.previewElement ).on( "click", function() {
                              thiss.removeFile(file);
                            });
                        }
                    },
                    init: function () {
                        //console.log('dropzone init');
                    },
                    sending: function(file, xhr, formData){
                        //formData.append('uploadType', 'a');
                        formData.append('aid', $("input[name='id']").val());
                    },
                    success: function(file, xhr, event){
                        var data = jQuery.parseJSON(xhr);
                        $("#imgAgendaPhoto").html('<img src="{{ env('MEDIA_END') }}/' + data.location + '" alt="" class="rounded" style="width: 500px; height: auto;" />');
                        //$("#cropPhotoButton").attr("onclick", "photoGalleryThumbnailCrop('{{ url('upload') }}/" + data.location + "', " + data.id + ")");
                        this.removeFile(file);
                    },
                    transformFile: function(file, done) {
                        var myDropZone = this;
                        // Create the image editor overlay
                        var editor = document.createElement('div');
                        editor.style.position = 'fixed';
                        editor.style.left = 0;
                        editor.style.right = 0;
                        editor.style.top = 0;
                        editor.style.bottom = 0;
                        editor.style.zIndex = 9999;
                        editor.style.backgroundColor = '#000';
                        document.body.appendChild(editor);

                        // Create confirm button at the top left of the viewport
                        var buttonConfirm = document.createElement('button');
                        buttonConfirm.style.position = 'absolute';
                        buttonConfirm.style.left = '10px';
                        buttonConfirm.style.top = '10px';
                        buttonConfirm.style.zIndex = 9999;
                        buttonConfirm.textContent = 'Onayla';
                        editor.appendChild(buttonConfirm);
                        buttonConfirm.addEventListener('click', function() {
                            // Get the canvas with image data from Cropper.js
                            var canvas = cropper.getCroppedCanvas();
                            // Turn the canvas into a Blob (file object without a name)
                            canvas.toBlob(function(blob) {
                                // Create a new Dropzone file thumbnail
                                myDropZone.createThumbnail(
                                    blob,
                                    myDropZone.options.thumbnailWidth,
                                    myDropZone.options.thumbnailHeight,
                                    myDropZone.options.thumbnailMethod,
                                    false, 
                                    function(dataURL) {
                                        // Update the Dropzone file thumbnail
                                        myDropZone.emit('thumbnail', file, dataURL);
                                        // Return the file to Dropzone
                                        done(blob);
                                    });
                                });
                            // Remove the editor from the view
                            document.body.removeChild(editor);
                        });

                        var buttonCancel = document.createElement('button');
                        buttonCancel.style.position = 'absolute';
                        buttonCancel.style.right = '10px';
                        buttonCancel.style.top = '10px';
                        buttonCancel.style.zIndex = 9999;
                        buttonCancel.textContent = 'İptal';
                        editor.appendChild(buttonCancel);

                        buttonCancel.addEventListener('click', function() {
                            editor.remove();
                        });

                        // Create an image node for Cropper.js
                        var image = new Image();
                        image.src = URL.createObjectURL(file);
                        editor.appendChild(image);

                        // Create Cropper.js
                        var cropper = new Cropper(image, { viewMode: 1, aspectRatio: 4.26, zoomable: false, zoomOnTouch:false, zoomOnWheel:false, wheelZoomRatio:false, autoCropArea: 1 });
                    }

                })

                $('#kt_dropzone_panorama_typelogo').dropzone({
                    acceptedFiles: "image/jpeg,image/png",
                    dictDefaultMessage: "Dosyaları yüklemek için buraya bırakın.",
                    dictFallbackMessage: "Tarayıcınız sürükle bırak dosya yüklemelerini desteklemiyor.",
                    dictFallbackText: "Please use the fallback form below to upload your files like in the olden days.",
                    dictFileTooBig: "Dosya yüklemek için çok büyük. Maksimum dosya boyutu: 7.5 MB",
                    dictInvalidFileType: "Bu tür dosyaları yükleyemezsiniz.",
                    dictCancelUpload: "Yüklemeyi iptal et.",
                    dictUploadCanceled: "Yükleme iptal edildi.",
                    dictCancelUploadConfirmation: "Bu yüklemeyi iptal etmek istediğinizden emin misiniz?",
                    dictRemoveFile: "Dosyayı kaldır.",
                    dictRemoveFileConfirmation: null,
                    dictMaxFilesExceeded: "Daha fazla dosya yükleyemezsiniz.",
                    dictFileSizeUnits: { tb: "TB", gb: "GB", mb: "MB", kb: "KB", b: "b" },
                    headers: { 'X-CSRF-TOKEN': $("input[name='_token']").val() },
                    url: "{{ url('/panorama_types/logo_save') }}", // Set the url for your upload script location
                    paramName: "file", // The name that will be used to transfer the file
                    maxFiles: 1,
                    maxFilesize: 7.5, // MB
                    addRemoveLinks: true,
                    accept: function(file, done) {
                        done();
                    },
                    error: function error(file, message) {
                        thiss = this;
                        if (file.previewElement) {
                            file.previewElement.classList.add("dz-error");

                            if (typeof message !== "string" && message.error) {
                                message = message.error;
                            }

                            var _iterator7 = _createForOfIteratorHelper(file.previewElement.querySelectorAll("[data-dz-errormessage]")),
                            _step7;

                            try {
                                for (_iterator7.s(); !(_step7 = _iterator7.n()).done;) {
                                    var node = _step7.value;
                                    node.textContent = message;
                                }
                            } catch (err) {
                                _iterator7.e(err);
                            } finally {
                                _iterator7.f();
                            }

                            $( file.previewElement ).on( "click", function() {
                              thiss.removeFile(file);
                            });
                        }
                    },
                    init: function () {
                        //console.log('dropzone init');
                    },
                    sending: function(file, xhr, formData){
                        //formData.append('uploadType', 'a');
                        formData.append('ptid', $("input[name='id']").val());
                    },
                    success: function(file, xhr, event){
                        var data = jQuery.parseJSON(xhr);
                        $("#imgPanoramaTypeLogo").html('<img src="{{ env('MEDIA_END') }}/' + data.location + '" alt="" class="rounded" style="width: 200px; height: auto;" />');
                        //$("#cropPhotoButton").attr("onclick", "photoGalleryThumbnailCrop('{{ url('upload') }}/" + data.location + "', " + data.id + ")");
                        this.removeFile(file);
                    },
                    transformFile: function(file, done) {
                        var myDropZone = this;
                        // Create the image editor overlay
                        var editor = document.createElement('div');
                        editor.style.position = 'fixed';
                        editor.style.left = 0;
                        editor.style.right = 0;
                        editor.style.top = 0;
                        editor.style.bottom = 0;
                        editor.style.zIndex = 9999;
                        editor.style.backgroundColor = '#000';
                        document.body.appendChild(editor);

                        // Create confirm button at the top left of the viewport
                        var buttonConfirm = document.createElement('button');
                        buttonConfirm.style.position = 'absolute';
                        buttonConfirm.style.left = '10px';
                        buttonConfirm.style.top = '10px';
                        buttonConfirm.style.zIndex = 9999;
                        buttonConfirm.textContent = 'Onayla';
                        editor.appendChild(buttonConfirm);
                        buttonConfirm.addEventListener('click', function() {
                            // Get the canvas with image data from Cropper.js
                            var canvas = cropper.getCroppedCanvas();
                            // Turn the canvas into a Blob (file object without a name)
                            canvas.toBlob(function(blob) {
                                // Create a new Dropzone file thumbnail
                                myDropZone.createThumbnail(
                                    blob,
                                    myDropZone.options.thumbnailWidth,
                                    myDropZone.options.thumbnailHeight,
                                    myDropZone.options.thumbnailMethod,
                                    false, 
                                    function(dataURL) {
                                        // Update the Dropzone file thumbnail
                                        myDropZone.emit('thumbnail', file, dataURL);
                                        // Return the file to Dropzone
                                        done(blob);
                                    });
                                });
                            // Remove the editor from the view
                            document.body.removeChild(editor);
                        });

                        var buttonCancel = document.createElement('button');
                        buttonCancel.style.position = 'absolute';
                        buttonCancel.style.right = '10px';
                        buttonCancel.style.top = '10px';
                        buttonCancel.style.zIndex = 9999;
                        buttonCancel.textContent = 'İptal';
                        editor.appendChild(buttonCancel);

                        buttonCancel.addEventListener('click', function() {
                            editor.remove();
                        });

                        /*
                            var selectSize = document.createElement('select');
                            selectSize.setAttribute("id", "selectSize");
                            selectSize.style.position = 'absolute';
                            selectSize.style.left = '40%';
                            selectSize.style.top = '10px';
                            selectSize.style.width = '20%';
                            selectSize.style.zIndex = 9999;

                            var opt = document.createElement('option');
                            opt.setAttribute("label", "title");
                            opt.setAttribute("value", 0);
                            selectSize.appendChild(opt);

                            editor.appendChild(selectSize);
                            
                            selectSize.addEventListener('change', function() {
                                //console.log(this.value);
                                cropper.setAspectRatio(this.value);
                            });
                        */

                        // Create an image node for Cropper.js
                        var image = new Image();
                        image.src = URL.createObjectURL(file);
                        editor.appendChild(image);

                        // Create Cropper.js
                        var cropper = new Cropper(image, { viewMode: 1, zoomable: false, zoomOnTouch:false, zoomOnWheel:false, wheelZoomRatio:false, autoCropArea: 1 });
                    }

                })

            })

            function terminationPassive() {
                $('#operation').val('passive');
                $('form#terminationForm').submit();
            }
            function terminationCancel() {
                $('#operation').val('cancel');
                $('form#terminationForm').submit();
            }
            function terminationAccept() {
                $('#operation').val('accept');
                $('form#terminationForm').submit();
            }

            $(function () {
                $('[data-toggle="popvstt"]').popover({trigger: 'focus'});
                $('[data-toggle="popvstt"]').tooltip({trigger: 'hover'});
                
                $('[data-toggle="tooltip"]').tooltip({trigger: 'hover'});
            })
            
            var select2Option_et = { placeholder: "Seçiniz", allowClear: true, language: "tr", width: "100%" };

            $('.select2_team_et').select2(select2Option_et).on('change', function() {
                // Revalidate the color field when an option is chosen
            });

            $('.datepicker_et').datetimepicker({
                locale: 'tr',
                format: 'DD.MM.YYYY'
            });

            $('.timepicker_et').datetimepicker({
                format: 'HH:mm',
                stepping: 15,
            });

        </script>

        <script type="text/javascript">

            @if(Session::get('message'))
                $(function() {
                    toastr.options = {"closeButton": true,"positionClass": "toast-bottom-right"}
                    toastr.{!!Session::get('message')['status'] !!}('{{Session::get('message')['text']}}')
                });
            @endif

            var select2Option = { placeholder: "Seçiniz", allowClear: true, language: "tr", width: "100%" };

            // loading remote data
            function formatRepo(repo) {
                //console.log(repo);
                if (repo.loading) return repo.text;
                var markup = repo.first_name;
                return markup;
            }

            function formatRepoSelection(repo) {
                return repo.full_name || repo.text;
            }

            function getAjaxData(_selector, _trigger, _url){
                
                var __url__ = '';
                if(_selector == '#select2_players_id_mp'){
                    __url__ = _url+'/{{ Request::segment(3) }}/'+$(_trigger).val();
                }else{
                    __url__ = _url+'/'+$(_trigger).val();
                }

                if($(_trigger).val() == ""){
                    $(_selector).val(null).trigger('change');
                }else{
                    $.ajax({
                        method: 'GET',
                        url : __url__,
                        dataType: 'json',
                        success: function(data, textStatus, jqXHR)
                        {
                            $(_selector).empty().trigger("change");

                            var option = new Option();
                            $(_selector).append(option).trigger('change');

                            $.each(data.items, function( key, value ) {
                                var option = new Option(value.name, value.id, false);
                                $(_selector).append(option).trigger('change');
                            });

                            $(_selector).select2({placeholder: "Seçiniz...", allowClear: true});
                            $(_selector).select2('open');
                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {

                        }
                    });
                }

            }

            @if (Request::segment(2) == 'add' || Request::segment(2) == 'edit')
                
                @php
                    $_is_tabbed = false;
                    if(Request::segment(2) != 'add'){
                        $tabConfig = config('tabs.'.Request::segment(1));
                        if(!empty($tabConfig) && count($tabConfig) > 1){
                            $_is_tabbed = true;
                        }
                    }
                @endphp

                @if($_is_tabbed)

                    @foreach ($tabConfig as $key_tabs => $value_tabs)

                        @if(!empty(data_get($value_tabs, 'form')))
                            
                            @php
                                $formConfig = config('forms.'.data_get($value_tabs, 'form'));
                            @endphp
                            //qweasd

                            @if((data_get($value_tabs, 'form') == 'match_player' || data_get($value_tabs, 'form') == 'match_action') && (!empty($model_data->completed) && $model_data->completed))
                            
                            @else

                                var fv_{{ data_get($value_tabs, 'form') }} = FormValidation.formValidation(
                                    document.getElementById('{{ data_get($value_tabs, 'form') }}Form'),
                                    {
                                        locale: 'tr_TR',
                                        localization: FormValidation.locales.tr_TR,
                                        plugins: {
                                            declarative: new FormValidation.plugins.Declarative(),

                                            trigger: new FormValidation.plugins.Trigger(),
                                            bootstrap: new FormValidation.plugins.Bootstrap(),
                                            submitButton: new FormValidation.plugins.SubmitButton(),
                                            defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                                        },
                                    }
                                );

                            @endif


                            @foreach ($formConfig as $key => $value)

                                @php
                                    $validationArr = array();
                                    $ignoreValList = array('nullable');

                                    $validateData = explode('|', data_get($value, 'validation'));
                                @endphp

                                @if(data_get($value, 'type') == 'text' || data_get($value, 'type') == 'number' || data_get($value, 'type') == 'email')

                                    @foreach ($validateData as $vValue)
                                        @php
                                            $valid = explode(':', $vValue);
                                            //dump($valid[0]);
                                            if(in_array($valid[0], $ignoreValList)){
                                                //echo 'ignoreValList:'.$valid[0];
                                            }else{
                                                if($valid[0] == 'required'){

                                                    $validationArr['validators']['notEmpty'] = array();

                                                }elseif($valid[0] == 'max'){
                                                    if(data_get($value, 'type') == 'text' || data_get($value, 'type') == 'email'){
                                                        
                                                        $validationArr['validators']['stringLength']['max'] = intval($valid[1]);

                                                    }elseif(data_get($value, 'type') == 'number'){
                                                        
                                                        $validationArr['validators']['lessThan']['max'] = intval($valid[1]);

                                                    }
                                                }elseif($valid[0] == 'min'){
                                                    if(data_get($value, 'type') == 'text' || data_get($value, 'type') == 'email'){
                                                        
                                                        $validationArr['validators']['stringLength']['min'] = intval($valid[1]);

                                                    }elseif(data_get($value, 'type') == 'number'){
                                                        
                                                        $validationArr['validators']['greaterThan']['min'] = intval($valid[1]);

                                                    }
                                                }elseif($valid[0] == 'numeric'){

                                                    $validationArr['validators']['numeric']['decimalSeparator'] = ',';
                                                    $validationArr['validators']['numeric']['thousandsSeparator'] = '';

                                                }elseif($valid[0] == 'email'){

                                                    $validationArr['validators']['emailAddress'] = array();

                                                }
                                            }        
                                        @endphp
                                    @endforeach

                                    @if((data_get($value_tabs, 'form') == 'match_player' || data_get($value_tabs, 'form') == 'match_action') && (!empty($model_data->completed) && $model_data->completed))
                                
                                    @else

                                        fv_{{ data_get($value_tabs, 'form') }}.addField("{{$key}}", @json($validationArr));

                                    @endif

                                @elseif(data_get($value, 'type') == 'select2')

                                    @if(!empty(data_get($value, 'search')) && data_get($value, 'search'))
                                    //////qweqwe
                                        $('#select2_{{$key}}').select2({
                                            placeholder: "Seçiniz...",
                                            allowClear: true,
                                            language: "tr",
                                            width: "100%",
                                            @if(!data_get($value, 'hoop'))
                                                minimumInputLength: 2,
                                            @endif
                                            ajax: {
                                                method: 'GET',
                                                delay: 250,
                                                dataType: 'json',
                                                url: function (params) {
                                                    var term = '';
                                                    if(params.term){
                                                        term = params.term;
                                                    }else{
                                                        term = '';
                                                    }

                                                    var seg = '';
                                                    @if(!empty(data_get($value, 'relation_col')))
                                                        if($('#select2_{{ data_get($value, 'relation_col') }}').val() != ''){
                                                            seg = $('#select2_{{ data_get($value, 'relation_col') }}').val()+'/';
                                                        }else{
                                                            fv_{{ data_get($value_tabs, 'form') }}.validateField('{{ data_get($value, 'relation_col') }}');
                                                            return false;
                                                        }
                                                    @elseif(!empty(data_get($value, 'url_input')))

                                                        seg = $('input:hidden[name={{ data_get($value, 'url_input') }}]').val()+'/';
                                                        
                                                    @endif 



                                                    return '{{ url(data_get($value, 'ajax_url')) }}/' + seg + term;
                                                },
                                                processResults: function (data) {
                                                    return {
                                                        results: data.items
                                                    };
                                                }
                                            }
                                        }).on('change', function() {
                                            // Revalidate the color field when an option is chosen
                                            @if(!empty(data_get($value, 'multiple')))
                                                fv_{{ data_get($value_tabs, 'form') }}.revalidateField('{{$key}}[]');
                                            @else
                                                fv_{{ data_get($value_tabs, 'form') }}.revalidateField('{{$key}}');
                                            @endif
                                        });
                                        
                                    @else

                                        $('#select2_{{$key}}').select2(select2Option).on('change', function() {
                                            // Revalidate the color field when an option is chosen
                                            @if(!empty(data_get($value, 'multiple')))
                                                fv_{{ data_get($value_tabs, 'form') }}.revalidateField('{{$key}}[]');
                                            @else
                                                fv_{{ data_get($value_tabs, 'form') }}.revalidateField('{{$key}}');
                                            @endif
                                        });

                                    @endif

                                    @if(!empty(data_get($value, 'trigger')))

                                        $('#select2_{{data_get($value, 'trigger')}}').on('change', function (e) {
                                            getAjaxData('#select2_{{$key}}', '#select2_{{ data_get($value, 'trigger') }}', '{{ url(data_get($value, 'ajax_url')) }}');
                                        });

                                        $('#select2_{{data_get($value, 'trigger')}}').on('select2:clear', function (e) {
                                            $('#select2_{{$key}}').empty().trigger("change");
                                        });

                                    @endif

                                    @if($key == 'players_id_mp')

                                        //asdqwe {{$key}}
                                    @endif

                                    @foreach ($validateData as $vValue)
                                        @php
                                            $valid = explode(':', $vValue);
                                            if(in_array($valid[0], $ignoreValList)){
                                            }else{
                                                if($valid[0] == 'required'){
                                                    $validationArr['validators']['notEmpty'] = array();
                                                }
                                            }        
                                        @endphp
                                    @endforeach

                                    @if((data_get($value_tabs, 'form') == 'match_player' || data_get($value_tabs, 'form') == 'match_action') && (!empty($model_data->completed) && $model_data->completed))
                                
                                    @else

                                        @if(!empty(data_get($value, 'multiple')))
                                            fv_{{ data_get($value_tabs, 'form') }}.addField("{{$key}}[]", @json($validationArr));
                                        @else
                                            fv_{{ data_get($value_tabs, 'form') }}.addField("{{$key}}", @json($validationArr));
                                        @endif

                                    @endif

                                @elseif(data_get($value, 'type') == 'datetimepicker')
                                    $('#datetimepicker_{{$key}}').datetimepicker({
                                        locale: 'tr',
                                        format: 'DD.MM.YYYY hh:mm',
                                        pick12HourFormat: false
                                    });
                                    $('#datetimepicker_{{$key}}').on('change.datetimepicker', function(e) {
                                        // Revalidate the date field
                                        fv_{{ data_get($value_tabs, 'form') }}.revalidateField('{{$key}}');
                                    });

                                    @foreach ($validateData as $vValue)
                                        @php
                                            $valid = explode(':', $vValue);
                                            if(in_array($valid[0], $ignoreValList)){
                                            }else{
                                                if($valid[0] == 'required'){
                                                    $validationArr['validators']['notEmpty'] = array();
                                                }elseif($valid[0] == 'date_format'){
                                                    if($valid[1] = 'd.m.Y H:i'){
                                                        $validationArr['validators']['date']['format'] = 'DD.MM.YYYY HH:mm';
                                                    }
                                                }
                                            }        
                                        @endphp
                                    @endforeach

                                    fv_{{ data_get($value_tabs, 'form') }}.addField("{{$key}}", @json($validationArr));

                                @elseif(data_get($value, 'type') == 'datepicker')

                                    $('#datepicker_{{$key}}').datetimepicker({
                                        locale: 'tr',
                                        format: 'DD.MM.YYYY'
                                    });

                                    $('#datepicker_{{$key}}').on('change.datetimepicker', function(e) {
                                        // Revalidate the date field
                                        fv_{{ data_get($value_tabs, 'form') }}.revalidateField('{{$key}}');
                                    });

                                    @foreach ($validateData as $vValue)
                                        @php
                                            $valid = explode(':', $vValue);
                                            if(in_array($valid[0], $ignoreValList)){
                                            }else{
                                                if($valid[0] == 'required'){
                                                    $validationArr['validators']['notEmpty'] = array();
                                                }elseif($valid[0] == 'date_format'){
                                                    if($valid[1] = 'd.m.Y'){
                                                        $validationArr['validators']['date']['format'] = 'DD.MM.YYYY';
                                                    }
                                                }
                                            }        
                                        @endphp
                                    @endforeach

                                    fv_{{ data_get($value_tabs, 'form') }}.addField("{{$key}}", @json($validationArr));

                                @elseif(data_get($value, 'type') == 'timepicker')

                                    $('#timepicker_{{$key}}').datetimepicker({
                                        format: 'HH:mm',
                                        stepping: 15,
                                    });

                                    $('#timepicker_{{$key}}').on('change.datetimepicker', function(e) {
                                        // Revalidate the date field
                                        fv_{{ data_get($value_tabs, 'form') }}.revalidateField('{{$key}}');
                                    });

                                    @foreach ($validateData as $vValue)
                                        @php
                                            $valid = explode(':', $vValue);
                                            if(in_array($valid[0], $ignoreValList)){
                                            }else{
                                                if($valid[0] == 'required'){
                                                    $validationArr['validators']['notEmpty'] = array();
                                                }
                                            }        
                                        @endphp
                                    @endforeach

                                    fv_{{ data_get($value_tabs, 'form') }}.addField("{{$key}}", @json($validationArr));

                                @endif

                            @endforeach
                        @endif

                    @endforeach

                @else
                    @php
                        $formConfig = config('forms.'.Request::segment(1));
                    @endphp

                    var fv = FormValidation.formValidation(
                        document.getElementById('{{ Request::segment(1) }}Form'),
                        {
                            locale: 'tr_TR',
                            localization: FormValidation.locales.tr_TR,
                            plugins: {
                                declarative: new FormValidation.plugins.Declarative(),

                                trigger: new FormValidation.plugins.Trigger(),
                                bootstrap: new FormValidation.plugins.Bootstrap(),
                                submitButton: new FormValidation.plugins.SubmitButton(),
                                defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                            },
                        }
                    );

                    @if(!is_null($formConfig))
                        @foreach ($formConfig as $key => $value)

                            @php
                                $validationArr = array();
                                $ignoreValList = array('nullable');

                                $validateData = explode('|', data_get($value, 'validation'));
                            @endphp

                            @if(data_get($value, 'type') == 'text' || data_get($value, 'type') == 'number' || data_get($value, 'type') == 'email')

                                @foreach ($validateData as $vValue)
                                    @php
                                        $valid = explode(':', $vValue);
                                        //dump($valid[0]);
                                        if(in_array($valid[0], $ignoreValList)){
                                            //echo 'ignoreValList:'.$valid[0];
                                        }else{
                                            if($valid[0] == 'required'){

                                                $validationArr['validators']['notEmpty'] = array();

                                            }elseif($valid[0] == 'max'){
                                                if(data_get($value, 'type') == 'text' || data_get($value, 'type') == 'email'){
                                                    
                                                    $validationArr['validators']['stringLength']['max'] = intval($valid[1]);

                                                }elseif(data_get($value, 'type') == 'number'){
                                                    
                                                    $validationArr['validators']['lessThan']['max'] = intval($valid[1]);

                                                }
                                            }elseif($valid[0] == 'min'){
                                                if(data_get($value, 'type') == 'text' || data_get($value, 'type') == 'email'){
                                                    
                                                    $validationArr['validators']['stringLength']['min'] = intval($valid[1]);

                                                }elseif(data_get($value, 'type') == 'number'){
                                                    
                                                    $validationArr['validators']['greaterThan']['min'] = intval($valid[1]);

                                                }
                                            }elseif($valid[0] == 'numeric'){

                                                $validationArr['validators']['numeric']['decimalSeparator'] = ',';
                                                $validationArr['validators']['numeric']['thousandsSeparator'] = '';

                                            }elseif($valid[0] == 'email'){

                                                $validationArr['validators']['emailAddress'] = array();

                                            }
                                        }        
                                    @endphp
                                @endforeach

                                fv.addField("{{$key}}", @json($validationArr));

                            @elseif(data_get($value, 'type') == 'select2')

                                @if(!empty(request()->query('tree_id')) && ($key == 'league_id' || $key == 'season_id'))
                                
                                @else

                                    @if(!empty(data_get($value, 'search')) && data_get($value, 'search'))
                                        
                                        $('#select2_{{$key}}').select2({
                                            placeholder: "Seçiniz...",
                                            allowClear: true,
                                            @if(!data_get($value, 'hoop'))
                                                minimumInputLength: 2,
                                            @endif
                                            ajax: {
                                                method: 'GET',
                                                delay: 250,
                                                dataType: 'json',
                                                url: function (params) {
                                                    var term = '';
                                                    if(params.term){
                                                        term = params.term;
                                                    }else{
                                                        term = '';
                                                    }

                                                    var seg = '';
                                                    
                                                    @if(!empty(data_get($value, 'relation_col')))
                                                        if($('#select2_{{ data_get($value, 'relation_col') }}').val() != ''){
                                                            seg = $('#select2_{{ data_get($value, 'relation_col') }}').val()+'/';
                                                        }else{
                                                            fv.validateField('{{ data_get($value, 'relation_col') }}');
                                                            return false;
                                                        }
                                                    @elseif(!empty(data_get($value, 'url_input')))

                                                        seg = $('input:hidden[name={{ data_get($value, 'url_input') }}]').val()+'/';
                                                        
                                                    @endif 
                                                    return '{{ url(data_get($value, 'ajax_url')) }}/' + seg + term;
                                                },
                                                processResults: function (data) {
                                                    return {
                                                        results: data.items
                                                    };
                                                }
                                            }
                                        }).on('change', function() {
                                            // Revalidate the color field when an option is chosen
                                            @if(!empty(data_get($value, 'multiple')))
                                                fv.revalidateField('{{$key}}[]');
                                            @else
                                                fv.revalidateField('{{$key}}');
                                            @endif
                                        });
                                        
                                    @else

                                        $('#select2_{{$key}}').select2(select2Option).on('change', function() {
                                            // Revalidate the color field when an option is chosen
                                            @if(!empty(data_get($value, 'multiple')))
                                                fv.revalidateField('{{$key}}[]');
                                            @else
                                                fv.revalidateField('{{$key}}');
                                            @endif
                                        });

                                    @endif

                                    @if(!empty(data_get($value, 'trigger')))
                                        
                                        $('#select2_{{data_get($value, 'trigger')}}').on('change', function (e) {
                                            getAjaxData('#select2_{{$key}}', '#select2_{{ data_get($value, 'trigger') }}', '{{ url(data_get($value, 'ajax_url')) }}');
                                        });

                                        $('#select2_{{data_get($value, 'trigger')}}').on('select2:clear', function (e) {
                                            $('#select2_{{$key}}').empty().trigger("change");
                                        });

                                    @endif

                                    @foreach ($validateData as $vValue)
                                        @php
                                            $valid = explode(':', $vValue);
                                            if(in_array($valid[0], $ignoreValList)){
                                            }else{
                                                if($valid[0] == 'required'){
                                                    $validationArr['validators']['notEmpty'] = array();
                                                }
                                            }        
                                        @endphp
                                    @endforeach

                                    @if(!empty(data_get($value, 'multiple')))
                                        fv.addField("{{$key}}[]", @json($validationArr));
                                    @else
                                        fv.addField("{{$key}}", @json($validationArr));
                                    @endif

                                @endif

                            @elseif(data_get($value, 'type') == 'datetimepicker')
                                $('#datetimepicker_{{$key}}').datetimepicker({
                                    locale: 'tr',
                                    format: 'DD.MM.YYYY HH:mm',
                                    pick12HourFormat: false
                                });


                            @elseif(data_get($value, 'type') == 'datepicker')

                                $('#datepicker_{{$key}}').datetimepicker({
                                    locale: 'tr',
                                    format: 'DD.MM.YYYY'
                                });

                                $('#datepicker_{{$key}}').on('change.datetimepicker', function(e) {
                                    // Revalidate the date field
                                    fv.revalidateField('{{$key}}');
                                });

                                @foreach ($validateData as $vValue)
                                    @php
                                        $valid = explode(':', $vValue);
                                        if(in_array($valid[0], $ignoreValList)){
                                        }else{
                                            if($valid[0] == 'required'){
                                                $validationArr['validators']['notEmpty'] = array();
                                            }elseif($valid[0] == 'date_format'){
                                                if($valid[1] = 'd.m.Y'){
                                                    $validationArr['validators']['date']['format'] = 'DD.MM.YYYY';
                                                }
                                            }
                                        }        
                                    @endphp
                                @endforeach

                                fv.addField("{{$key}}", @json($validationArr));

                            @elseif(data_get($value, 'type') == 'timepicker')

                                $('#timepicker_{{$key}}').datetimepicker({
                                    format: 'HH:mm',
                                    stepping: 15,
                                });

                                $('#timepicker_{{$key}}').on('change.datetimepicker', function(e) {
                                    // Revalidate the date field
                                    fv.revalidateField('{{$key}}');
                                });

                                @foreach ($validateData as $vValue)
                                    @php
                                        $valid = explode(':', $vValue);
                                        if(in_array($valid[0], $ignoreValList)){
                                        }else{
                                            if($valid[0] == 'required'){
                                                $validationArr['validators']['notEmpty'] = array();
                                            }
                                        }        
                                    @endphp
                                @endforeach

                                fv.addField("{{$key}}", @json($validationArr));

                            @endif

                        @endforeach
                    @endif
                @endif

            @endif

            $('#select2_district_id').select2(select2Option);


            $('#select2_staff_search').select2(select2Option);

            $('#select2_user_type_search').select2(select2Option);

            $('#select2_team_search').select2(select2Option);

            $('#select2_active_search').select2(select2Option);
            
            $('#select2_infoshare_search').select2(select2Option);

            $('#select2_province_search').select2(select2Option);

            $('#select2_province_search').on('change', function (e) {
                if ($('#select2_district_search').length) {
                    getAjaxData('#select2_district_search', '#select2_province_search', '/getDistrictsF');
                }
            });

            $('#select2_province_search').on('select2:clear', function (e) {
                if ($('#select2_district_search').length) {
                    $('#select2_district_search').empty().trigger("change");
                }
            });

            $('#select2_district_search').select2(select2Option);

            $('#select2_pdistrict_search').select2(select2Option);

            $('#select2_league_search').select2(select2Option);

            $('#select2_season_search').select2(select2Option);

            $('#select2_completed_search').select2(select2Option);
            
            $('#select2_videostatus_search').select2(select2Option);
            
            $('#select2_league_types_search').select2(select2Option);

            $('#select2_potential_search').select2(select2Option);
            
            $('#select2_satisfaction_search').select2(select2Option);

            $('#select2_satisfaction_jogo_search').select2(select2Option);

            $('#select2_captain_search').select2({
                placeholder: "Seçiniz...",
                allowClear: true,
                minimumInputLength: 2,
                ajax: {
                    method: 'GET',
                    delay: 250,
                    dataType: 'json',
                    url: function (params) {
                        return '/getUsersC/' + params.term;
                    },
                    processResults: function (data) {
                        return {
                            results: data.items
                        };
                    }
                }
            });
            
            $('#select2_provinceg_search').select2(select2Option);
            
            $('#select2_ground_search').select2({
                placeholder: "Seçiniz...",
                allowClear: true,
                ajax: {
                    method: 'GET',
                    delay: 250,
                    dataType: 'json',
                    url: function (params) {
                        params.province = $('#select2_provinceg_search').val();
                        return '/getGroundsP/' + params.term;
                    },
                    processResults: function (data) {
                        return {
                            results: data.items
                        };
                    }
                }
            });
            
            $('#select2_time').select2({
                placeholder: "Seçiniz...",
                allowClear: true,
                language: "tr",
                width: "100%",
                ajax: {
                    method: 'GET',
                    delay: 250,
                    dataType: 'json',
                    url: function (params) {
                        console.log(params);
                        params.ground_id = $('#select2_ground_id').val();
                        params.date = $('input[name=date]').val();
                        //console.log(params.term);
                        return '/getGroundHourF/';
                    },
                    processResults: function (data) {
                        return {
                            results: data.items
                        };
                    }
                }
            });

            $('#select2_time_e').select2({
                placeholder: "Seçiniz...",
                allowClear: true,
                language: "tr",
                width: "100%",
                ajax: {
                    method: 'GET',
                    delay: 250,
                    dataType: 'json',
                    url: function (params) {
                        console.log(params);
                        params.ground_id = $('#select2_ground_id').val();
                        params.date = $('input[name=date]').val();
                        //console.log(params.term);
                        return '/getGroundHourFE/';
                    },
                    processResults: function (data) {
                        return {
                            results: data.items
                        };
                    }
                }
            }).on('change', function (e) {
                //console.log($('#select2_time_e').val());
                if($('#select2_time_e').val() == 'custom'){
                    $("#select2_time_e option").remove();
                    $('#select2_time_e').select2({ajax: null, tags: true, placeholder: "Saat Ekle"});
                    $('#select2_time_e').select2('open');
                    //console.log($('#select2_time_e'));
                }
            });

            $('#select2_status_search').select2(select2Option);

            $('#select2_week_start_date').select2({ placeholder: "Seçiniz", language: "tr", width: "100%" });

            $('#daterange_start_search').datetimepicker({
                locale: 'tr',
                format: 'DD.MM.YYYY',
                toolbarPlacement: 'bottom',
                buttons: { showToday: false, showClear: true, showClose: false }
            });

            $('#daterange_end_search').datetimepicker({
                useCurrent: false,
                locale: 'tr',
                format: 'DD.MM.YYYY',
                toolbarPlacement: 'bottom',
                buttons: { showToday: false, showClear: true, showClose: false }
            });

            $('#daterange_start_search').on('change.datetimepicker', function(e) {
                $('#daterange_end_search').datetimepicker('minDate', e.date);
            });

            $('#daterange_end_search').on('change.datetimepicker', function(e) {
                $('#daterange_start_search').datetimepicker('maxDate', e.date);
            });


            function setReservationOffersAjaxData(_this, _rid, _roid, _val){

                $.ajax({
                    method: 'POST',
                    url : '/setReservationOffers',
                    dataType: 'json',
                    headers: { 'X-CSRF-TOKEN': $("input[name='_token']").val() },
                    data: {
                        rid: _rid,
                        roid: _roid,
                        val: _val,
                    },
                    success: function(data, textStatus, jqXHR)
                    {   
                        //console.log(data);
                        $(function() {
                            if(data.status == "success"){
                                toastr.options = {"closeButton": true,"positionClass": "toast-bottom-right"}
                                toastr.success(data.text)
                            }
                        });

                        if(data.val == 'wait'){
                            $(".resofferbtn").hide();
                            $(".resofferbtn_wait").show();
                            $("input[name='reservation_offer_id']").val('');
                            $("#reservation_offer_convert_matchButton").attr('disabled', true);
                        }else if(data.val == 'confirmed'){
                            $(".resofferbtn").hide();
                            $("."+data.val+"_"+data.roid).show();
                            $("input[name='reservation_offer_id']").val(data.roid);
                            $("#reservation_offer_convert_matchButton").attr('disabled', false);
                        }
                        
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                    }
                });
                
            }

        </script>

        <script type="text/javascript">
            
            function viButRole() {
                @if(!Auth::user()->hasPermissionTo('view_permission')) $('.btn_view_permission').hide(); @endif
                @if(!Auth::user()->hasPermissionTo('change_permission')) $('.btn_change_permission').hide(); @endif
                @if(!Auth::user()->hasPermissionTo('delete_permission')) $('.btn_delete_permission').hide(); @endif
            }
            function viButUser() {
                @if(!Auth::user()->hasPermissionTo('view_user')) $('.btn_view_user').hide(); @endif
                @if(!Auth::user()->hasPermissionTo('change_user')) $('.btn_change_user').hide(); @endif
                @if(!Auth::user()->hasPermissionTo('delete_user')) $('.btn_delete_user').hide(); @endif
            }

            function viButPosition() {
                @if(!Auth::user()->hasPermissionTo('view_position')) $('.btn_view_position').hide(); @endif
                @if(!Auth::user()->hasPermissionTo('change_position')) $('.btn_change_position').hide(); @endif
                @if(!Auth::user()->hasPermissionTo('delete_position')) $('.btn_delete_position').hide(); @endif
            }
            function viButPointType() {
                @if(!Auth::user()->hasPermissionTo('view_pointtype')) $('.btn_view_pointtype').hide(); @endif
                @if(!Auth::user()->hasPermissionTo('change_pointtype')) $('.btn_change_pointtype').hide(); @endif
                @if(!Auth::user()->hasPermissionTo('delete_pointtype')) $('.btn_delete_pointtype').hide(); @endif
            }
            function viButTactic() {
                @if(!Auth::user()->hasPermissionTo('view_tactic')) $('.btn_view_tactic').hide(); @endif
                @if(!Auth::user()->hasPermissionTo('change_tactic')) $('.btn_change_tactic').hide(); @endif
                @if(!Auth::user()->hasPermissionTo('delete_tactic')) $('.btn_delete_tactic').hide(); @endif
            }
            function viButPotential() {
                @if(!Auth::user()->hasPermissionTo('view_potential')) $('.btn_view_potential').hide(); @endif
                @if(!Auth::user()->hasPermissionTo('change_potential')) $('.btn_change_potential').hide(); @endif
                @if(!Auth::user()->hasPermissionTo('delete_potential')) $('.btn_delete_potential').hide(); @endif
            }
            function viButSatisfaction() {
                @if(!Auth::user()->hasPermissionTo('view_satisfaction')) $('.btn_view_satisfaction').hide(); @endif
                @if(!Auth::user()->hasPermissionTo('change_satisfaction')) $('.btn_change_satisfaction').hide(); @endif
                @if(!Auth::user()->hasPermissionTo('delete_satisfaction')) $('.btn_delete_satisfaction').hide(); @endif
            }
            function viButSatisfactionJogo() {
                @if(!Auth::user()->hasPermissionTo('view_satisfaction_jogo')) $('.btn_view_satisfaction_jogo').hide(); @endif
                @if(!Auth::user()->hasPermissionTo('change_satisfaction_jogo')) $('.btn_change_satisfaction_jogo').hide(); @endif
                @if(!Auth::user()->hasPermissionTo('delete_satisfaction_jogo')) $('.btn_delete_satisfaction_jogo').hide(); @endif
            }
            function viButConference() {
                @if(!Auth::user()->hasPermissionTo('view_conference')) $('.btn_view_conference').hide(); @endif
                @if(!Auth::user()->hasPermissionTo('change_conference')) $('.btn_change_conference').hide(); @endif
                @if(!Auth::user()->hasPermissionTo('delete_conference')) $('.btn_delete_conference').hide(); @endif
            }
            function viButProvince() {
                @if(!Auth::user()->hasPermissionTo('view_province')) $('.btn_view_province').hide(); @endif
                @if(!Auth::user()->hasPermissionTo('change_province')) $('.btn_change_province').hide(); @endif
                @if(!Auth::user()->hasPermissionTo('delete_province')) $('.btn_delete_province').hide(); @endif
            }
            function viButDistrict() {
                @if(!Auth::user()->hasPermissionTo('view_district')) $('.btn_view_district').hide(); @endif
                @if(!Auth::user()->hasPermissionTo('change_district')) $('.btn_change_district').hide(); @endif
                @if(!Auth::user()->hasPermissionTo('delete_district')) $('.btn_delete_district').hide(); @endif
            }
            function viButGround() {
                @if(!Auth::user()->hasPermissionTo('view_ground')) $('.btn_view_ground').hide(); @endif
                @if(!Auth::user()->hasPermissionTo('change_ground')) $('.btn_change_ground').hide(); @endif
                @if(!Auth::user()->hasPermissionTo('delete_ground')) $('.btn_delete_ground').hide(); @endif
                @if(!Auth::user()->hasPermissionTo('view_groundhour')) $('.btn_view_groundhour').hide(); @endif
            }
            function viButGroundhour() {
                @if(!Auth::user()->hasPermissionTo('change_groundhour')) $('.btn_change_groundhour').attr("disabled", true); @endif
            }
            function viButGroundproperty() {
                @if(!Auth::user()->hasPermissionTo('view_groundproperty')) $('.btn_view_groundproperty').hide(); @endif
                @if(!Auth::user()->hasPermissionTo('change_groundproperty')) $('.btn_change_groundproperty').hide(); @endif
                @if(!Auth::user()->hasPermissionTo('delete_groundproperty')) $('.btn_delete_groundproperty').hide(); @endif
            }
            function viButLeague() {
                @if(!Auth::user()->hasPermissionTo('view_league')) $('.btn_view_league').hide(); @endif
                @if(!Auth::user()->hasPermissionTo('change_league')) $('.btn_change_league').hide(); @endif
                @if(!Auth::user()->hasPermissionTo('delete_league')) $('.btn_delete_league').hide(); @endif
            }
            function viButSeason() {
                @if(!Auth::user()->hasPermissionTo('view_season')) $('.btn_view_season').hide(); @endif
                @if(!Auth::user()->hasPermissionTo('change_season')) $('.btn_change_season').hide(); @endif
                @if(!Auth::user()->hasPermissionTo('delete_season')) $('.btn_delete_season').hide(); @endif

                @if(!Auth::user()->hasPermissionTo('change_season')) $('.btn_view_seasonsettings').hide(); @endif

                @if(!Auth::user()->hasPermissionTo('view_pointseason')) $('.btn_view_season_p').hide(); @endif
                @if(!Auth::user()->hasPermissionTo('change_pointseason')) $('.btn_change_season_p').hide(); @endif
                @if(!Auth::user()->hasPermissionTo('delete_pointseason')) $('.btn_delete_season_p').hide(); @endif

                @if(!Auth::user()->hasPermissionTo('view_fixtureseason')) $('.btn_view_season_f').hide(); @endif
                @if(!Auth::user()->hasPermissionTo('change_fixtureseason')) $('.btn_change_season_f').hide(); @endif
                @if(!Auth::user()->hasPermissionTo('delete_fixtureseason')) $('.btn_delete_season_f').hide(); @endif

                @if(!Auth::user()->hasPermissionTo('view_eliminationseason')) $('.btn_view_season_e').hide(); @endif
                @if(!Auth::user()->hasPermissionTo('change_eliminationseason')) $('.btn_change_season_e').hide(); @endif
                @if(!Auth::user()->hasPermissionTo('delete_eliminationseason')) $('.btn_delete_season_e').hide(); @endif

                @if(!Auth::user()->hasPermissionTo('view_eliminationtree')) $('.btn_view_eliminationtree').hide(); @endif
            }
            function viButEliminationtree() {
                @if(!Auth::user()->hasPermissionTo('view_eliminationtree')) $('.btn_view_eliminationtree').hide(); @endif
                @if(!Auth::user()->hasPermissionTo('change_eliminationtree')) $('.btn_change_eliminationtree').hide(); @endif
                @if(!Auth::user()->hasPermissionTo('delete_eliminationtree')) $('.btn_delete_eliminationtree').hide(); @endif
            }
            function viButTeam() {
                @if(!Auth::user()->hasPermissionTo('view_team')) $('.btn_view_team').hide(); @endif
                @if(!Auth::user()->hasPermissionTo('change_team')) $('.btn_change_team').hide(); @endif
                @if(!Auth::user()->hasPermissionTo('delete_team')) $('.btn_delete_team').hide(); @endif
            }
            function viButMatch() {
                @if(!Auth::user()->hasPermissionTo('view_match')) $('.btn_view_match').hide(); @endif
                @if(!Auth::user()->hasPermissionTo('change_match')) $('.btn_change_match').hide(); @endif
                @if(!Auth::user()->hasPermissionTo('delete_match')) $('.btn_delete_match').hide(); @endif
            }
            function viButReservation() {
                @if(!Auth::user()->hasPermissionTo('view_reservation')) $('.btn_view_reservation').hide(); @endif
                @if(!Auth::user()->hasPermissionTo('change_reservation')) $('.btn_change_reservation').hide(); @endif
                @if(!Auth::user()->hasPermissionTo('delete_reservation')) $('.btn_delete_reservation').hide(); @endif
            }
                
        </script>

    </body>

    <!--end::Body-->
</html>