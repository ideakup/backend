@extends('layouts.app')

@section('content')

<!--begin::Main-->

<!--[html-partial:include:{"file":"partials/_header-mobile.html"}]/-->
@include('partials._header_mobile')
<div class="d-flex flex-column flex-root">

	<!--begin::Page-->
	<div class="d-flex flex-row flex-column-fluid page">

		<!--[html-partial:include:{"file":"partials/_aside.html"}]/-->
		@include('partials._aside')

		<!--begin::Wrapper-->
		<div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">

			<!--[html-partial:include:{"file":"partials/_header.html"}]/-->
			@include('partials._header')

			<!--begin::Content-->
			<div class="content d-flex flex-column flex-column-fluid" id="kt_content">

				<!--[html-partial:include:{"file":"partials/_subheader/subheader-v1.html"}]/-->
				@include('partials._subheader')

				<!--[html-partial:include:{"file":"partials/_content.html"}]/-->

				@if(Request::segment(1) == 'dashboard')
					@include('partials._content')
				@elseif (Request::segment(1) == 'users')
					@include('contents.users')
				@elseif (Request::segment(1) == 'roles')
					@include('contents.roles')


				@elseif (Request::segment(1) == 'conferences')
					@include('contents.conferences')
				@elseif (Request::segment(1) == 'provinces')
					@include('contents.provinces')
				@elseif (Request::segment(1) == 'districts')
					@include('contents.districts')
				

				@elseif (Request::segment(1) == 'grounds')
					@include('contents.grounds')
				@elseif (Request::segment(1) == 'ground_properties')
					@include('contents.ground_properties')


				@elseif (Request::segment(1) == 'leagues')
					@include('contents.leagues')
				@elseif (Request::segment(1) == 'league_gallery')
					@include('contents.league_gallery')
				@elseif (Request::segment(1) == 'champions')
					@include('contents.champions')
				@elseif (Request::segment(1) == 'seasons')
					@include('contents.seasons')
				@elseif (Request::segment(1) == 'seasonsettings')
					@include('contents.seasonsettings')
				@elseif (Request::segment(1) == 'seasonsp')
					@include('contents.seasonsp')
				@elseif (Request::segment(1) == 'seasonsf')
					@include('contents.seasonsf')
				@elseif (Request::segment(1) == 'seasonse')
					@include('contents.seasonse')


				@elseif (Request::segment(1) == 'teams')
					@include('contents.teams')
				@elseif (Request::segment(1) == 'matches')
					@include('contents.matches')
				@elseif (Request::segment(1) == 'reservations')
					@include('contents.reservations')

				@elseif (Request::segment(1) == 'addoutsideplayer')
					@include('contents.addoutsideplayer')

				@elseif (Request::segment(1) == 'elimination_tree')
					@include('contents.elimination_tree')
				@elseif (Request::segment(1) == 'fixture_group')
					@include('contents.fixture_group')

				@elseif (Request::segment(1) == 'player_positions')
					@include('contents.player_positions')
				@elseif (Request::segment(1) == 'point_types')
					@include('contents.point_types')
				@elseif (Request::segment(1) == 'tactics')
					@include('contents.tactics')
				@elseif (Request::segment(1) == 'team_potentials')
					@include('contents.team_potentials')
				@elseif (Request::segment(1) == 'team_satisfactions')
					@include('contents.team_satisfactions')
				@elseif (Request::segment(1) == 'team_satisfactions_jogo')
					@include('contents.team_satisfactions_jogo')
				@elseif (Request::segment(1) == 'banners')
					@include('contents.banners')

				@elseif (Request::segment(1) == 'penalties')
					@include('contents.penalties')				
				@elseif (Request::segment(1) == 'player_penalties')
					@include('contents.penalties')				
				@elseif (Request::segment(1) == 'team_penalties')
					@include('contents.penalties')
				@elseif (Request::segment(1) == 'penalty_types')
					@include('contents.penalty_types')

				@elseif (Request::segment(1) == 'agenda')
					@include('contents.agenda')

				@elseif (Request::segment(1) == 'news')
					@include('contents.news')
				@elseif (Request::segment(1) == 'news_types')
					@include('contents.news_types')
				@elseif (Request::segment(1) == 'tags')
					@include('contents.tags')
				@elseif (Request::segment(1) == 'rules')
					@include('contents.rules')
					
				@elseif (Request::segment(1) == 'monthly_panorama')
					@include('contents.monthly_panorama')
				@elseif (Request::segment(1) == 'monthly_panorama_player')
					@include('contents.monthly_panorama')
				@elseif (Request::segment(1) == 'monthly_panorama_team')
					@include('contents.monthly_panorama')
				@elseif (Request::segment(1) == 'panorama_types')
					@include('contents.panorama_types')
				@endif

			</div>

			<!--end::Content-->

			<!--[html-partial:include:{"file":"partials/_footer.html"}]/-->
			@include('partials._footer')
		</div>

		<!--end::Wrapper-->
	</div>

	<!--end::Page-->
</div>

<!--end::Main-->

@endsection