<!--begin: Datatable-->
<table class="table table-separate table-head-custom table-hover tab-table" id="kt_datatable_reservation_offer">
    <thead>
        <tr>
            <th>ID</th>
            <th>Takım</th>
            <th>Durum</th>
            <th>Teklif Tarihi</th>
            <th>İşlemler</th>
        </tr>
    </thead>
    <tbody>

        @foreach ($model_data->reservation_offer as $key_rel => $value_rel)

            <tr role="row">
                <td>{{ $value_rel->id }}</td>
                <td>{{ $value_rel->team->name }}</td>
                <td>{{ $value_rel->status }}</td>
                <td>{{ Carbon\Carbon::parse($value_rel->created_at)->format('d.m.Y H:i') }}</td>
                <td style="width: 110px;">
                    @if(Request::segment(2) != 'delete')
                        <a href="javascript:void(0);" onclick="setReservationOffersAjaxData(this, '{{ $model_data->id }}', '{{ $value_rel->id }}', 'confirmed')" class="btn btn-sm btn-success btn-icon resofferbtn resofferbtn_wait wait_{{ $value_rel->id }}" title="Onayla" @if($model_data->status != 'open') style="display:none;" @endif role="button" data-toggle="tooltip">
                            <i class="fas fa-check"></i>
                        </a>
                    
                        <a href="javascript:void(0);" onclick="setReservationOffersAjaxData(this, '{{ $model_data->id }}', '{{ $value_rel->id }}', 'wait')" class="btn btn-sm btn-danger btn-icon resofferbtn resofferbtn_confirmed confirmed_{{ $value_rel->id }}" title="Onayı Geri Al" @if($value_rel->status != 'confirmed') style="display:none;" @endif role="button" data-toggle="tooltip">
                            <i class="fas fa-times"></i>
                        </a>
                    @endif
                </td>
            </tr>

        @endforeach

    </tbody>
</table>
<!--end: Datatable-->