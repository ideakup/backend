@php
	//dump($model_data->season_image_after_slide);
@endphp
<div class="form-group row">
	<label class="col-form-label col-lg-12 col-sm-12">
		<h3 class="font-size-lg text-dark font-weight-bold mb-6 mt-10">Slide Fotoğrafı</h3>
	</label>
	<div class="col-lg-9 col-sm-12">
		<div class="card card-custom overlay">
		    <div class="card-body p-0">
		        <div class="overlay-wrapper" id="imgSeasonInfoSlidePhoto">
		          	<img src="@if(!empty($model_data->season_info_slide_photo)) {{ env('MEDIA_END').$model_data->season_info_slide_photo->data }} @endif" alt="" class="rounded" style="width: 100%;" />
		        </div>
		        <div class="overlay-layer align-items-start justify-content-end pt-5 pr-5">
		            <a href="@if(!empty($model_data->season_info_slide_photo)) {{ url('season_image/delete/'.$model_data->id.'/'.$model_data->season_info_slide_photo->id) }} @endif" class="btn btn-clean btn-icon" title="Fotoğrafı Sil" ><i class="fas fa-trash-alt"></i></a>
		        </div>
		    </div>
		</div>
	</div>
	<div class="col-lg-3 col-sm-12">
		<div class="dropzone dropzone-default dropzone-primary" id="kt_dropzone_info_slide_photo">
			<div class="dropzone-msg dz-message needsclick">
				<h3 class="dropzone-msg-title">Slide fotoğrafını sürükleyip buraya bırakın veya yüklemek için tıklayın.</h3>
				<span class="dropzone-msg-desc">Sadece 1 dosya yükleyebilirsiniz.</span>
			</div>
		</div>
	</div>
</div>

<div class="form-group row">
	<label class="col-form-label col-lg-12 col-sm-12">
		<h3 class="font-size-lg text-dark font-weight-bold mb-6 mt-10">Fotoğraf 1</h3>
	</label>
	<div class="col-lg-9 col-sm-12">
		<div class="card card-custom overlay">
		    <div class="card-body p-0">
		        <div class="overlay-wrapper" id="imgSeasonInfoImg1">
		          	<img src="@if(!empty($model_data->season_info_img1)) {{ env('MEDIA_END').$model_data->season_info_img1->data }} @endif" alt="" class="rounded" style="width: 100%;" />
		        </div>
		        <div class="overlay-layer align-items-start justify-content-end pt-5 pr-5">
		            <a href="@if(!empty($model_data->season_info_img1)) {{ url('season_image/delete/'.$model_data->id.'/'.$model_data->season_info_img1->id) }} @endif" class="btn btn-clean btn-icon" title="Fotoğrafı Sil" ><i class="fas fa-trash-alt"></i></a>
		        </div>
		    </div>
		</div>
	</div>
	<div class="col-lg-3 col-sm-12">
		<div class="dropzone dropzone-default dropzone-primary" id="kt_dropzone_info_img1">
			<div class="dropzone-msg dz-message needsclick">
				<h3 class="dropzone-msg-title">Fotoğraf 1'i sürükleyip buraya bırakın veya yüklemek için tıklayın.</h3>
				<span class="dropzone-msg-desc">Sadece 1 dosya yükleyebilirsiniz.</span>
			</div>
		</div>
	</div>
</div>

<div class="form-group row">
	<label class="col-form-label col-lg-12 col-sm-12">
		<h3 class="font-size-lg text-dark font-weight-bold mb-6 mt-10">Fotoğraf 2</h3>
	</label>
	<div class="col-lg-9 col-sm-12">
		<div class="card card-custom overlay">
		    <div class="card-body p-0">
		        <div class="overlay-wrapper" id="imgSeasonInfoImg2">
		          	<img src="@if(!empty($model_data->season_info_img2)) {{ env('MEDIA_END').$model_data->season_info_img2->data }} @endif" alt="" class="rounded" style="width: 100%;" />
		        </div>
		        <div class="overlay-layer align-items-start justify-content-end pt-5 pr-5">
		            <a href="@if(!empty($model_data->season_info_img2)) {{ url('season_image/delete/'.$model_data->id.'/'.$model_data->season_info_img2->id) }} @endif" class="btn btn-clean btn-icon" title="Fotoğrafı Sil" ><i class="fas fa-trash-alt"></i></a>
		        </div>
		    </div>
		</div>
	</div>
	<div class="col-lg-3 col-sm-12">
		<div class="dropzone dropzone-default dropzone-primary" id="kt_dropzone_info_img2">
			<div class="dropzone-msg dz-message needsclick">
				<h3 class="dropzone-msg-title">Fotoğraf 2'yi sürükleyip buraya bırakın veya yüklemek için tıklayın.</h3>
				<span class="dropzone-msg-desc">Sadece 1 dosya yükleyebilirsiniz.</span>
			</div>
		</div>
	</div>
</div>

