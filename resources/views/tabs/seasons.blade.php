@php
    $activeSeasonId = array();
@endphp

@foreach ($model_data->team_teamseason as $valid)
    @if($valid->season->active == true && $valid->season->start_date <= Carbon\Carbon::now()->format('Y-m-d') && $valid->season->end_date >= Carbon\Carbon::now()->format('Y-m-d') && $valid->season->polymorphic_ctype_id != 32)
        @php
            $activeSeasonId[] = $valid->season->id;
        @endphp
    @endif
@endforeach

@php
    $activeSeason = null;
    $activeSeasons = array();
    $existSeasons = array();

    if(!empty($model_data->league)){

        $t_seasons = $model_data->league->seasonApi;

        foreach ($t_seasons as $t_season) {
            if($t_season->active == true){
                if($t_season->polymorphic_ctype_id != 32){
                    if($t_season->start_date <= Carbon\Carbon::now()->format('Y-m-d') && $t_season->end_date >= Carbon\Carbon::now()->format('Y-m-d')){
                        $activeSeasons[] = $t_season;
                    }
                    $existSeasons[] = $t_season;
                }
            }
        }

        if(count($activeSeasons) > 1){
            foreach ($activeSeasons as $season) {
                if($season->polymorphic_ctype_id == 33){

                    $fs_group_teams_id = array();
                    foreach ($season->league_fixtureseason->groups as $group) {
                        foreach ($group->group_teams as $group_team) {
                            $fs_group_teams_id[] = $group_team->team_id;
                        }
                    }
                    if(array_search($model_data->id, $fs_group_teams_id)){
                        $activeSeason = $season;
                    }

                }
            }
        }else{
            if(count($existSeasons)){
                $activeSeason = $existSeasons[0];
            }
        }

        if(empty($activeSeason)){
            $activeSeason = $activeSeasons[0];
        }elseif(!empty($activeSeason)){

            if(!empty($model_data->team_teamseason->where('season_id', $activeSeason->id)->first())){
                $remaining_transfer_count = $model_data->team_teamseason->where('season_id', $activeSeason->id)->first()->remaining_transfer_count;
                $squad_locked = $model_data->team_teamseason->where('season_id', $activeSeason->id)->first()->squad_locked;
            }else{
                $remaining_transfer_count = $activeSeason->allowed_transfer_count;
                $squad_locked = false;
            }
        }

        //dd($activeSeason);

    }

@endphp

<!--begin: Datatable-->
<table class="table table-separate table-head-custom table-hover tab-table" id="kt_datatable_teams_seasons">
    <thead>
        <tr>
            <th>ID</th>
            <th>Lig</th>
            <th>Yıl</th>
            <th>Sezon</th>
            <th>Kadro Kilit Tar.</th>
            <th>Kalan Transfer Hakkı</th>
            <th>Toplam Maç S.</th>
            <th>Puan</th>
            <th>İşlemler</th>
        </tr>
    </thead>
    <tbody>

        @if(!empty(data_get($value_tabs, 'relationship')))

            @foreach ($model_data[data_get($value_tabs, 'relationship')] as $key_rel => $value_rel)

                <tr role="row">
                    <td>{{ $value_rel->season->id }}</td>
                    <td>{{ $value_rel->season->league->name }}</td>
                    <td>{{ $value_rel->season->year }}</td>
                    <td>{{ $value_rel->season->name }}</td>
                    <td>{{ (!empty($value_rel->squad_locked_date)) ? Carbon\Carbon::parse($value_rel->squad_locked_date)->format('d.m.Y') : '-' }}</td>
                    <td>{{ $value_rel->remaining_transfer_count }}</td>
                    <td>{{ $value_rel->match_total }}</td>
                    <td>{{ $value_rel->point }}</td>
                    <td style="width: 170px;">
                        @if($value_rel->season->id == $activeSeason->id && Auth::user()->hasPermissionTo('change_teamseason'))

                            <a href="javascript:void(0);" 
                                @if($value_rel->playoff_member)
                                    class="btn btn-sm btn-success btn-icon" title="Takım Playoff Üyesidir"
                                @else
                                    class="btn btn-sm btn-clean btn-icon" title="Takım Playoff Üyesi Değildir"
                                @endif

                                role="button" data-toggle="popvstt" data-html="true" 
                                data-content="
                                    <p>
                                        Bu sezon için Playoff Üyeliğini değiştirmek istiyor musunuz?
                                    </p>
                                    <a href='{{ url('teams/setPlayOff/'.$model_data->id.'/'.$value_rel->id) }}' class='btn btn-sm btn-light-success font-weight-bold mr-2' title='Onayla'>Onayla</a>
                                    <a href='javascript:void(0);' class='btn btn-sm btn-light-danger font-weight-bold mr-2' title='İptal'>İptal</a>
                                "
                            >
                                <i class="fas fa-trophy"></i>
                            </a>

                            <a href="javascript:void(0);" 
                                @if(!$value_rel->squad_locked)
                                    class="btn btn-sm btn-success btn-icon" title="Kadro Kilit Durumu: Kilitli Değil"
                                @else
                                    class="btn btn-sm btn-clean btn-icon" title="Kadro Kilit Durumu: Kilitli"
                                @endif

                                role="button" data-toggle="popvstt" data-html="true" 
                                data-content="
                                    <p>
                                        Bu sezon için Kadro Kilit Durumunu değiştirmek istiyor musunuz?
                                    </p>
                                    <a href='{{ url('teams/setSquadLocked/'.$model_data->id.'/'.$value_rel->id) }}' class='btn btn-sm btn-light-success font-weight-bold mr-2' title='Onayla'>Onayla</a>
                                    <a href='javascript:void(0);' class='btn btn-sm btn-light-danger font-weight-bold mr-2' title='İptal'>İptal</a>
                                "
                            >   
                                @if(!$value_rel->squad_locked)
                                    <i class="fas fa-unlock"></i>
                                @else
                                    <i class="fas fa-lock"></i>
                                @endif
                                
                            </a>

                            @if(Auth::user()->hasPermissionTo('add_seasonbonuspoint'))
                                <button type="button" class="btn btn-sm btn-primary btn-icon" title="Puan Ekle" data-toggle="modal" data-target="#addpointModel{{ $value_rel->id }}">
                                    <i class="fas fa-arrow-up"></i>
                                </button>
                                
                                <div class="modal fade" id="addpointModel{{ $value_rel->id }}" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">Puan Ekle</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <i aria-hidden="true" class="ki ki-close"></i>
                                                </button>
                                            </div>
                                            <form class='form' method='POST' action='{{ url('teams/addpoint') }}'>
                                                {{ csrf_field() }}
                                                <input type='hidden' name='team_id' value='{{ $model_data->id }}'>
                                                <input type='hidden' name='tseason_id' value='{{ $value_rel->id }}'>

                                                <div class="modal-body">
                                                    <div class="form-group row">
                                                        <label class="col-2 col-form-label">Puan</label>
                                                        <div class="col-10">
                                                            <input class="form-control" name="point" type="number" step="1" value="0">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="submit" class="btn btn-success mr-2"> Kaydet </button>
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">İptal</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            @endif


                            @if(Auth::user()->hasPermissionTo('add_seasontransfercount'))
                                <button type="button" class="btn btn-sm btn-primary btn-icon" title="Transfer Hakkı Ekle" data-toggle="modal" data-target="#addtransferModel{{ $value_rel->id }}">
                                    <i class="fas fa-user-plus"></i>
                                </button>

                                <div class="modal fade" id="addtransferModel{{ $value_rel->id }}" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">Transfer Hakkı Ekle</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <i aria-hidden="true" class="ki ki-close"></i>
                                                </button>
                                            </div>
                                            <form class='form' method='POST' action='{{ url('teams/addtransfer') }}'>
                                                {{ csrf_field() }}
                                                <input type='hidden' name='team_id' value='{{ $model_data->id }}'>
                                                <input type='hidden' name='tseason_id' value='{{ $value_rel->id }}'>

                                                <div class="modal-body">
                                                    <div class="form-group row">
                                                        <label class="col-2 col-form-label">Transfer Hakkı</label>
                                                        <div class="col-10">
                                                            <input class="form-control" name="transfer" type="number" step="1" value="0">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="submit" class="btn btn-success mr-2"> Kaydet </button>
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">İptal</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            @endif





                        @endif

                    </td>
                </tr>

            @endforeach

        @endif
        
    </tbody>
</table>
<!--end: Datatable-->
