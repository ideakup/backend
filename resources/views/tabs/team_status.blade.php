<div class="form-group mb-8">
    <div class="alert alert-custom 
        @if($model_data->status == 'active')
            alert-outline-success
        @elseif($model_data->status == 'passive')
            alert-outline-danger
        @elseif($model_data->status == 'termination')
            alert-outline-warning
        @endif
        " role="alert">
        
        <div class="alert-icon">
            <i class="flaticon-warning"></i>
        </div>
        <div class="alert-text">
            <div class="row">
                <div class="col-form-label col-lg-12 col-sm-12 font-weight-bold">Fesih Durumu : 
                    @if($model_data->status == 'active')
                        Aktif
                    @elseif($model_data->status == 'passive')
                        Feshedildi
                    @elseif($model_data->status == 'termination')
                        Fesih İsteği
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@if($model_data->status != 'passive')
    <div class="alert mb-5 p-5" role="alert">
        <h4 class="alert-heading">Fesih Süreci</h4>
        <ul>
            <li>Kaptan takım feshini talep eder ve yönetici tarafından fesih işleminin onaylanmasını bekler.</li>
            <li>Fesih sürecine giren takım, işlem onaylanana kadar normal bir takım gibi bütün işlemlerine devam eder.</li>
            <li>Fesih süreci onaylandıktan sonra kaptan dahil bütün oyuncular takımdan düşer.</li>
            <li>Hiç maç yapmamış takım sistemden tamamen kaldırılır.</li>
            <li>Daha önce oynanmış maçı olan takım pasif hale gelir ve istatistik bilgileri saklanır.</li>
            <li>Oynayacağı bütün maçlar iptal edilir.</li>
            <li>Bütün transfer teklifleri iptal edilir.</li>
            <li>Serbest kalan oyuncular ve kaptan yeni takım kurabilir.</li>
        </ul>
    </div>

    <form class="form" method="POST" action="{{ url('teams/request_termination') }}" id="terminationForm">
        
        {{ csrf_field() }}
        <input type="hidden" name="operation" id="operation">
        <input type="hidden" name="team_id" id="team_id" value="{{ Request::segment(3) }}">

        @if($model_data->status == 'active')
            <button type="button" class="btn btn-danger mr-2" id="terminationBtnPassive" onclick="terminationPassive()">
                Takımı Feshet
            </button>
        @elseif($model_data->status == 'termination')
            <button type="button" class="btn btn-secondary mr-2" id="terminationBtnCancel" onclick="terminationCancel()">
                İptal Et
            </button>
            <button type="button" class="btn btn-danger mr-2" id="terminationBtnAccept" onclick="terminationAccept()">
                Fesih İşlemini Onayla
            </button>
        @endif

        </form>
@endif


