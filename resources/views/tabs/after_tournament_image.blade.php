@php
	//dump($model_data->season_image_after_slide);
@endphp
<div class="form-group row">
	<label class="col-form-label col-lg-12 col-sm-12">
		<h3 class="font-size-lg text-dark font-weight-bold mb-6 mt-10">Slide Fotoğrafı</h3>
	</label>
	<div class="col-lg-9 col-sm-12">
		<div class="card card-custom overlay">
		    <div class="card-body p-0">
		        <div class="overlay-wrapper" id="imgSeasonAfterSlideImage">
		          	<img src="@if(!empty($model_data->season_image_after_slide)) {{ env('MEDIA_END').$model_data->season_image_after_slide->data }} @endif" alt="" class="rounded" style="width: 100%;" />
		        </div>
		        <div class="overlay-layer align-items-start justify-content-end pt-5 pr-5">
		            <a href="@if(!empty($model_data->season_image_after_slide)) {{ url('season_image/delete/'.$model_data->id.'/'.$model_data->season_image_after_slide->id) }} @endif" class="btn btn-clean btn-icon" title="Fotoğrafı Sil" ><i class="fas fa-trash-alt"></i></a>
		        </div>
		    </div>
		</div>
	</div>
	<div class="col-lg-3 col-sm-12">
		<div class="dropzone dropzone-default dropzone-primary" id="kt_dropzone_season_after_slider">
			<div class="dropzone-msg dz-message needsclick">
				<h3 class="dropzone-msg-title">Slide fotoğrafını sürükleyip buraya bırakın veya yüklemek için tıklayın.</h3>
				<span class="dropzone-msg-desc">Sadece 1 dosya yükleyebilirsiniz.</span>
			</div>
		</div>
	</div>
</div>


<div class="form-group row">
	<label class="col-form-label col-lg-12 col-sm-12">
		<h3 class="font-size-lg text-dark font-weight-bold mb-6 mt-10">Teaser Kapak Fotoğrafı</h3>
	</label>
	<div class="col-lg-9 col-sm-12">
		<div class="card card-custom overlay">
		    <div class="card-body p-0">
		        <div class="overlay-wrapper" id="imgSeasonAfterTeaserImage">
		          	<img src="@if(!empty($model_data->season_after_teaser_img)) {{ env('MEDIA_END').$model_data->season_after_teaser_img->data }} @endif" alt="" class="rounded" style="width: 100%;" />
		        </div>
		        <div class="overlay-layer align-items-start justify-content-end pt-5 pr-5">
		            <a href="@if(!empty($model_data->season_after_teaser_img)) {{ url('season_image/delete/'.$model_data->id.'/'.$model_data->season_after_teaser_img->id) }} @endif" class="btn btn-clean btn-icon" title="Fotoğrafı Sil" ><i class="fas fa-trash-alt"></i></a>
		        </div>
		    </div>
		</div>
	</div>
	<div class="col-lg-3 col-sm-12">
		<div class="dropzone dropzone-default dropzone-primary" id="kt_dropzone_season_after_teaser_img">
			<div class="dropzone-msg dz-message needsclick">
				<h3 class="dropzone-msg-title">Slide fotoğrafını sürükleyip buraya bırakın veya yüklemek için tıklayın.</h3>
				<span class="dropzone-msg-desc">Sadece 1 dosya yükleyebilirsiniz.</span>
			</div>
		</div>
	</div>
</div>


<div class="form-group row">
	<label class="col-form-label col-lg-12 col-sm-12">
		<h3 class="font-size-lg text-dark font-weight-bold mb-6 mt-10">Fotoğraf Galerisi</h3>
	</label>
	<div class="col-lg-12 col-sm-12">
		<div class="dropzone dropzone-default dropzone-primary" id="kt_dropzone_season_after">
			<div class="dropzone-msg dz-message needsclick">
				<h3 class="dropzone-msg-title">Dosyaları sürükleyip buraya bırakın veya yüklemek için tıklayın.</h3>
				<span class="dropzone-msg-desc">Aynı anda en fazla 10 dosya yükleyebilirsiniz.</span>
			</div>
		</div>
	</div>
</div>
<form class='form' method='POST' action='{{ url('season_image/desc_save') }}'>
    {{ csrf_field() }}
    <input type='hidden' name='season_id' value='{{ $model_data->id }}'>
    <input type='hidden' name='season_status' value='after'>
	<div class="row" id="imgSeasonAfterImages">
		@foreach ($model_data->season_image_after as $image)
			<div class="col-lg-4 col-sm-12">
				<div class="card card-custom overlay">
				    <div class="card-body p-0">
				        <div class="overlay-wrapper">
				          <img src="{{ env('MEDIA_END').$image->data }}" alt="" class="w-100 rounded"/>
				        </div>
				        <div class="overlay-layer align-items-start justify-content-end pt-5 pr-5">
				            <a href="{{ url('season_image/delete/'.$model_data->id.'/'.$image->id) }}" class="btn btn-clean btn-icon" title="Fotoğrafı Sil" ><i class="fas fa-trash-alt"></i></a>
				        </div>
				    </div>
				</div>
				<div class="form-group row" style="margin-top: 0.75rem;">
				    <div class="col-lg-12">
				        <input class="form-control" type="text" name="season_after_image[{{ $image->id }}]" placeholder="Fotoğraf Açıklaması" max="160" value="{{ $image->description }}">
				    </div>
				</div>
			</div>
		@endforeach
	</div>


	<div class="row" style="padding: 2rem 0 0 0; border-top: 1px solid #EBEDF3;">
		<div class="col-lg-3"></div>
		<div class="col-lg-6">
			<button type="submit" class="btn btn-success mr-2">Kaydet</button>
			<button type="button" class="btn btn-secondary" onclick="history.back()">İptal</button>
		</div>
		<div class="col-lg-3 text-right"></div>
	</div>
</form>

