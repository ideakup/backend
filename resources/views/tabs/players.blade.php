@php
    $activeSeason = null;
    $activeSeasons = array();
    $existSeasons = array();
    $remaining_transfer_count = 0;
    $squad_locked = false;

    if(!empty($model_data->league)){

        $t_seasons = $model_data->league->seasonApi;

        foreach ($t_seasons as $t_season) {
            if($t_season->active == true){
                if($t_season->polymorphic_ctype_id != 32){
                    if($t_season->start_date <= Carbon\Carbon::now()->format('Y-m-d') && $t_season->end_date >= Carbon\Carbon::now()->format('Y-m-d')){
                        $activeSeasons[] = $t_season;
                    }
                    $existSeasons[] = $t_season;
                }
            }
        }

        if(count($activeSeasons) > 1){
            foreach ($activeSeasons as $season) {
                if($season->polymorphic_ctype_id == 33){

                    $fs_group_teams_id = array();
                    foreach ($season->league_fixtureseason->groups as $group) {
                        foreach ($group->group_teams as $group_team) {
                            $fs_group_teams_id[] = $group_team->team_id;
                        }
                    }
                    if(array_search($model_data->id, $fs_group_teams_id)){
                        $activeSeason = $season;
                    }

                }
            }
        }else{
            if(count($existSeasons)){
                $activeSeason = $existSeasons[0];
            }
        }

        if(!empty($activeSeason)){

            if(!empty($model_data->team_teamseason->where('season_id', $activeSeason->id)->first())){
                $remaining_transfer_count = $model_data->team_teamseason->where('season_id', $activeSeason->id)->first()->remaining_transfer_count;
                $squad_locked = $model_data->team_teamseason->where('season_id', $activeSeason->id)->first()->squad_locked;
            }else{
                $remaining_transfer_count = $activeSeason->allowed_transfer_count;
                $squad_locked = false;
            }
        }

    }

@endphp

<div class="form-group mb-8">
    <div class="alert alert-custom alert-default" role="alert">
        <div class="alert-icon">
            <i class="flaticon-warning icon-5x"></i>
        </div>
        <div class="alert-text">
            <div class="row">
                <div class="col-form-label text-right col-lg-5 col-sm-12 font-weight-bold">Aktif Sezon :</div>
                <div class="col-form-label col-lg-7 col-sm-12"> {{ (!empty($activeSeason)) ? $activeSeason->year.' '.$activeSeason->league->name.' '.$activeSeason->name : 'Aktif Sezon Bulunmuyor...' }} </div>
            </div>
            <div class="row">
                <div class="col-form-label text-right col-lg-5 col-sm-12 font-weight-bold">Transfer Başlangıç Tarihi :</div>
                <div class="col-form-label col-lg-7 col-sm-12"> {{ (!empty($activeSeason)) ? Carbon\Carbon::parse($activeSeason->transfer_start_date)->format('d.m.Y') : '0' }} </div>
            </div>
            <div class="row">
                <div class="col-form-label text-right col-lg-5 col-sm-12 font-weight-bold">Transfer Bitiş Tarihi :</div>
                <div class="col-form-label col-lg-7 col-sm-12"> {{ (!empty($activeSeason)) ? Carbon\Carbon::parse($activeSeason->transfer_end_date)->format('d.m.Y') : '0' }} </div>
            </div>
            <div class="row">
                <div class="col-form-label text-right col-lg-5 col-sm-12 font-weight-bold">Kalan Transfer Sayısı :</div>
                <div class="col-form-label col-lg-7 col-sm-12"> @if(!empty($remaining_transfer_count)) {{ $remaining_transfer_count }} @else {{ '0' }} @endif</div>
            </div>
        </div>
    </div>
</div>

@if(!($remaining_transfer_count == 0 && $squad_locked) && Auth::user()->hasPermissionTo('add_teamplayer'))
    <a href="{{ url('teams/addinsideplayer/'.Request::segment(3)) }}" class="btn btn-danger mr-2">
        Site İçinden Ekle
    </a>
@endif

@if(!($remaining_transfer_count == 0 && $squad_locked) && Auth::user()->hasPermissionTo('add_teamplayer_fromoutside'))
    <a href="{{ url('addoutsideplayer/add/'.Request::segment(3)) }}" class="btn btn-warning mr-2">
        Site Dışından Ekle
    </a>
@endif

<!--begin: Datatable-->
<table class="table table-separate table-head-custom table-hover tab-table" id="kt_datatable_teams_players">
    <thead>
        <tr>
            <th>ID</th>
            <th>Ad</th>
            <th>Soyad</th>
            <th>Katılma Tarihi</th>
            <th>Pozisyonlar</th>
            <th>İşlemler</th>
        </tr>
    </thead>
    <tbody>
        
        @if(!empty(data_get($value_tabs, 'relationship')))

            @foreach ($model_data[data_get($value_tabs, 'relationship')] as $key_rel => $value_rel)

                <tr role="row">
                    <td>{{ $value_rel->id }}</td>
                    <td>{{ $value_rel->user->first_name }}</td>
                    <td>{{ $value_rel->user->last_name }}</td>
                    <td>@if($value_rel->team_teamplayer->count()) {{ Carbon\Carbon::parse($value_rel->team_teamplayer->first()->joined_date)->format('d.m.Y') }} @endif</td>
                    <td>{{ $value_rel->players_position->name }}</td>
                    <td style="width: 110px;">

                        <a href="{{ url("users/edit/".$value_rel->user_id) }}" class="btn btn-sm btn-clean btn-icon" title="Oyuncuyu Düzenle" role="button">
                            <i class="fas fa-user"></i>
                        </a>

                        @if(Auth::user()->hasPermissionTo('change_teamplayer'))
                            <a href="javascript:void(0);" 
                                @if($model_data->captain_id == $value_rel->id)
                                    class="btn btn-sm btn-success btn-icon" title="Takım Kaptanıdır"
                                @else
                                    class="btn btn-sm btn-clean btn-icon" title="Kaptan Olarak Ata"
                                    role="button" data-toggle="popvstt" data-html="true" 
                                    data-content="
                                        <p>
                                            Bu oyuncuyu kaptan yapmak istiyor musunuz?
                                        </p>
                                        <a href='{{ url('teams/setCaptain/'.$model_data->id.'/'.$value_rel->id) }}' class='btn btn-sm btn-light-success font-weight-bold mr-2' title='Onayla'>Onayla</a>
                                        <a href='javascript:void(0);' class='btn btn-sm btn-light-danger font-weight-bold mr-2' title='İptal'>İptal</a>
                                    "
                                @endif
                            >
                                <i class="fas fa-user-shield"></i>
                            </a>
                        @endif

                        @if(Auth::user()->hasPermissionTo('delete_teamplayer'))
                            <a href="javascript:void(0);" class="btn btn-sm btn-danger btn-icon" title="Oyuncuyu Serbest Bırak"
                                role="button" data-toggle="popvstt" data-html="true"
                                data-content="
                                    <p>
                                        Bu oyuncuyu serbest bırakmak istiyor musunuz?
                                    </p>
                                    <a href='{{ url('teams/releasePlayer/'.$model_data->id.'/'.$value_rel->id) }}' class='btn btn-sm btn-light-success font-weight-bold mr-2' title='Onayla'>Onayla</a>
                                    <a href='javascript:void(0);' class='btn btn-sm btn-light-danger font-weight-bold mr-2' title='İptal'>İptal</a>
                                "
                            >
                                <i class="fas fa-times"></i>
                            </a>
                        @endif
                    </td>
                </tr>
            @endforeach

        @endif
        
    </tbody>
</table>
<!--end: Datatable-->