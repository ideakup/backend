@php
	//dump($model_data[data_get($value_tabs, 'relationship')]);
@endphp
<!--begin: Datatable-->
<table class="table table-separate table-head-custom table-hover tab-table" id="kt_datatable_match_panoramas">
    <thead>
        <tr>
            <th>ID</th>
            <th>Panorama</th>
            <th>Oyuncu</th>
            <th>Video Kaynağı</th>
            <th>Video Embed Code</th>
            <th>İşlemler</th>
        </tr>
    </thead>
    <tbody>
        
   		@if(!empty(data_get($value_tabs, 'relationship')))

   			@foreach ($model_data[data_get($value_tabs, 'relationship')] as $key_rel => $value_rel)
                <tr role="row">
                    <td>{{ $value_rel->id }}</td>  <!-- ID -->
                    <td>{{ $value_rel->panorama_type->name }}</td>  
                    <td>
                    	@if(!empty($value_rel->match_player_id))
                    		({{ $value_rel->match_player_panorama->team->name }})
                    		@if(empty($value_rel->match_player_panorama->player_id))
                    			{{ $value_rel->match_player_panorama->guest_name }}
                    		@else
                    			{{ $value_rel->match_player_panorama->player->user->first_name.' '.$value_rel->match_player_panorama->player->user->last_name }}
                    		@endif
                    	@endif
                    </td>  
                    <td>{{ $value_rel->embed_source }}</td>
                    <td>{{ $value_rel->embed_code }}</td>
                    <td style="width: 110px;">
                        @if(Request::segment(2) != 'delete')
                            <a href="#" id="{{$loop->index}}" class="btn btn-sm btn-clean btn-icon btn-hover-success match_panorama_edit_button" title="Panorama Düzenle" role="button" data-toggle="tooltip" data-html="true" data-content="">
                                <i class="fas fa-edit"></i>
                            </a>
                            <a href="#" class="btn btn-sm btn-clean btn-icon btn-hover-danger" role="button" data-toggle="popvstt" data-html="true" 

                            title="Panorama Sil" 

                            data-content="
                                <p>
                                    ({{ $value_rel->match_player_panorama->team->name }})
                                    @if(!empty($value_rel->match_player_id))
                                        @if(empty($value_rel->match_player_panorama->player_id))
                                            {{ $value_rel->match_player_panorama->guest_name }}
                                        @else
                                            {{ $value_rel->match_player_panorama->player->user->first_name.' '.$value_rel->match_player_panorama->player->user->last_name }}
                                        @endif
                                    @endif
                                </p>
                                <p>
                                    Bu aksiyonu kaldırmak istiyor musunuz?
                                </p>
                                <a href='{{ url('match_panorama/delete/'.$model_data->id.'/'.$value_rel->id) }}' class='btn btn-sm btn-light-success font-weight-bold mr-2' title='Onayla'>Onayla</a>
                                <a href='#' class='btn btn-sm btn-light-danger font-weight-bold mr-2' title='İptal'>İptal</a>
                            ">
                                <i class="fas fa-times"></i>
                            </a>
                        @endif
                    </td>
                </tr>
            @endforeach

        @endif
        
    </tbody>
</table>
<!--end: Datatable-->
