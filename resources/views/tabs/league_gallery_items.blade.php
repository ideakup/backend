@php
    //dump($model_data);
@endphp
<!--begin: Datatable-->
<table class="table table-separate table-head-custom table-hover tab-table" id="kt_datatable_league_gallery_items">
    <thead>
        <tr>
            <th>ID</th>
            <th>Video Kaynağı</th>
            <th>Video Embed Code</th>
            <th>Açıklama</th>
            <th>İşlemler</th>
        </tr>
    </thead>
    <tbody>
        
   		@if(!empty(data_get($value_tabs, 'relationship')))

            @foreach ($model_data[data_get($value_tabs, 'relationship')] as $key_rel => $value_rel)
                <tr role="row">
                    <td>{{ $value_rel->id }}</td>  <!-- ID -->
                    <td>{{ $value_rel->embed_source }}</td>  <!-- Video Kaynağı -->
                    <td>{{ $value_rel->embed_code }}</td>  <!-- Video Embed Code -->
                    <td>{{ $value_rel->description }}</td>  <!-- Team -->
                    <td style="width: 110px;">
                        
                        <a href="#" id="{{$loop->index}}" class="btn btn-sm btn-clean btn-icon btn-hover-success league_gallery_item_edit_button" title="Galeri Elemanını Düzenle" role="button" data-toggle="tooltip" data-html="true" data-content="">
                            <i class="fas fa-edit"></i>
                        </a>
                        <a href="#" class="btn btn-sm btn-clean btn-icon btn-hover-danger" role="button" data-toggle="popvstt" data-html="true" 

                        title="Galeri Elemanını Sil" 

                        data-content="
                            <p>
                                Bu galeri elemanını kaldırmak istiyor musunuz?
                            </p>
                            <a href='{{ url('league_gallery_items/delete/'.$model_data->id.'/'.$value_rel->id) }}' class='btn btn-sm btn-light-success font-weight-bold mr-2' title='Onayla'>Onayla</a>
                            <a href='#' class='btn btn-sm btn-light-danger font-weight-bold mr-2' title='İptal'>İptal</a>
                        ">
                            <i class="fas fa-times"></i>
                        </a>
                        
                    </td>
                </tr>
            @endforeach

        @endif
        
    </tbody>
</table>
<!--end: Datatable-->
