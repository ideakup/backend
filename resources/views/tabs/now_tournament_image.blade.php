<div class="form-group row">
	<label class="col-form-label col-lg-3 col-sm-12 text-lg-right"> </label>
	<div class="col-lg-12 col-sm-12">
		<div class="dropzone dropzone-default dropzone-primary" id="kt_dropzone_season_now">
			<div class="dropzone-msg dz-message needsclick">
				<h3 class="dropzone-msg-title">Dosyaları sürükleyip buraya bırakın veya yüklemek için tıklayın.</h3>
				<span class="dropzone-msg-desc">Aynı anda en fazla 10 dosya yükleyebilirsiniz.</span>
			</div>
		</div>
	</div>
</div>
<form class='form' method='POST' action='{{ url('season_image/desc_save') }}'>
    {{ csrf_field() }}
    <input type='hidden' name='season_id' value='{{ $model_data->id }}'>
    <input type='hidden' name='season_status' value='now'>
	<div class="row" id="imgSeasonNowImages">
		@foreach ($model_data->season_image_now as $image)
			<div class="col-lg-4 col-sm-12">
				<div class="card card-custom overlay">
				    <div class="card-body p-0">
				        <div class="overlay-wrapper">
				          <img src="{{ env('MEDIA_END').$image->data }}" alt="" class="w-100 rounded"/>
				        </div>
				        <div class="overlay-layer align-items-start justify-content-end pt-5 pr-5">
				            <a href="{{ url('season_image/delete/'.$model_data->id.'/'.$image->id) }}" class="btn btn-clean btn-icon" title="Fotoğrafı Sil" ><i class="fas fa-trash-alt"></i></a>
				        </div>
				    </div>
				</div>
				<div class="form-group row" style="margin-top: 0.75rem;">
				    <div class="col-lg-12">
				        <input class="form-control" type="text" name="season_now_image[{{ $image->id }}]" placeholder="Fotoğraf Açıklaması" max="160" value="{{ $image->description }}">
				    </div>
				</div>
			</div>
		@endforeach
	</div>


	<div class="row" style="padding: 2rem 0 0 0; border-top: 1px solid #EBEDF3;">
		<div class="col-lg-3"></div>
		<div class="col-lg-6">
			<button type="submit" class="btn btn-success mr-2">Kaydet</button>
			<button type="button" class="btn btn-secondary" onclick="history.back()">İptal</button>
		</div>
		<div class="col-lg-3 text-right"></div>
	</div>
</form>

