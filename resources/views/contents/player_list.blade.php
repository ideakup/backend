<!--begin::Card-->
<div class="card card-custom">
	<div class="card-header">
		<div class="card-title">
			<span class="card-icon">
				<i class="fas fa-users text-primary"></i>
			</span>
			<h3 class="card-label">Takım : {{ App\Team::find(Request::segment(3))->name }} (Site İçinden Oyuncu Ekle)</h3>
		</div>
		<div class="card-toolbar">
			<!--begin::Button
			@if (Request::segment(2) == '' && Auth::user()->hasPermissionTo('add_user'))
				<a href="{{ url()->current() }}/add" class="btn btn-primary font-weight-bolder"><i class="fas fa-plus"></i> Kullanıcı Ekle</a>
			@endif
			end::Button-->
		</div>
	</div>
	<div class="card-body">
		<!--begin: Search Form-->
		<form class="mb-5" id='searchFormm'>
			<div class="row mb-6">

				<div class="col-lg-3 mb-5">
					<label> TC </label>
					<input type="text" class="form-control datatable-input" data-col-index="1" />
				</div>

				<div class="col-lg-3 mb-5">
					<label> Ad Soyad </label>
					<input type="text" class="form-control datatable-input" data-col-index="2" />
				</div>

				<div class="col-lg-3 mb-5">
					<label> E-posta </label>
					<input type="text" class="form-control datatable-input" data-col-index="3" />
				</div>

				<div class="col-lg-3 mb-5">
					<label> Telefon </label>
					<input type="text" class="form-control datatable-input" data-col-index="6" />
				</div>
				<!--
				<div class="col-lg-3 mb-5">
					<label> Şehir </label>
					<select class="form-control datatable-input" id="select2_province_search" data-col-index="4">
						<option></option>
						@foreach (App\Province::all() as $province)
							<option value="{{ $province->id }}"> {{ $province->name }} </option>
						@endforeach
					</select>
				</div>
				-->
				<div class="col-lg-3 mb-5">
					<label> İlçe </label>
					<select class="form-control datatable-input" id="select2_pdistrict_search" multiple="multiple" data-col-index="4">
						@foreach (App\Province::find(App\Team::find(Request::segment(3))->province_id)->district as $district)
							<option value="{{ $district->id }}"> {{ $district->name }} </option>
						@endforeach
					</select>
				</div>
				<!--
				<div class="col-lg-3 mb-5">
					<label> Yetkililer </label>
					<select class="form-control datatable-input" id="select2_staff_search">
						<option></option>
						@foreach (Spatie\Permission\Models\Role::all() as $role)
							<option value="{{ $role->name }}"> {{ $role->name }} </option>
						@endforeach
					</select>
				</div>

				<div class="col-lg-3 mb-5">
					<label> Kullanıcı Tipi </label>
					<select class="form-control datatable-input" id="select2_user_type_search">
						<option></option>
						<option value="superuser"> Süper User </option>
						<option value="coordinator"> Koordinatör </option>
						<option value="referee"> Hakem </option>
						<option value="player"> Oyuncu </option>
						<option value="only_user"> Kullanıcı </option>
					</select>
				</div>
				-->
				<div class="col-lg-3 mb-5">
					<label> Takım </label>
					<select class="form-control datatable-input" id="select2_team_search">
						<option></option>
						<option value="yes"> Var </option>
						<option value="no"> Yok </option>
					</select>
				</div>

				<div class="col-lg-3 mb-5">
					<label> Aktif </label>
					<select class="form-control datatable-input" id="select2_active_search">
						<option></option>
						<option value="yes"> Evet </option>
						<option value="no"> Hayır </option>
					</select>
				</div>

				<div class="col-lg-3 text-right mb-5">
					<label>&#160;</label>
					<span style="display: block;">
						<button class="btn btn-primary btn-primary--icon btn-sm" id="kt_search">
							<span>
								<i class="fas fa-search"></i>
								<span>Search</span>
							</span>
						</button>&#160;&#160;
						<button class="btn btn-secondary btn-secondary--icon btn-sm" id="kt_reset">
							<span>
								<i class="far fa-times-circle"></i>
								<span>Reset</span>
							</span>
						</button>
					</span>
				</div>
			</div>
		</form>
		<!--begin: Datatable-->
		<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
		<input type="hidden" name="team_id" id="team_id" value="{{ Request::segment(3) }}">

		<table class="table table-bordered table-hover table-checkable tdt" id="kt_datatable_player_list" style="margin-top: 13px !important">
		</table>
		<!--end: Datatable-->
	</div>
</div>
<!--end::Card-->