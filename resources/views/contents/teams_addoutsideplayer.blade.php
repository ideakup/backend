<!--begin::Entry-->
<div class="d-flex flex-column-fluid">

	<!--begin::Container-->
	<div class="container-fluid">

		<style type="text/css">
			.tdt td {
				white-space: nowrap;
			}

			.dataTables_wrapper table.dataTable.collapsed > tbody > tr[role="row"] > td:first-child {
			    padding-left: 20px !important;
			}

			.dataTables_wrapper table.dataTable.dtr-inline.collapsed > tbody > tr[role="row"] > td:first-child:before {
				margin-top: -5px;
			}
			
			.dataTables_wrapper .dataTable td{
				padding: 3px 5px !important;
			}

			.form-control[readonly] {
			    background-color: #F3F6F9;
			}
		</style>
		<!--begin::Dashboard-->
		
		@include('form_element._addForm')

		<!--end::Dashboard-->

	</div>

	<!--end::Container-->
</div>

<!--end::Entry-->