<div class="d-flex flex-column-fluid">
	<div class="container-fluid">
		<style type="text/css">
			.tdt td {
				white-space: nowrap;
			}

			.dataTables_wrapper table.dataTable.collapsed > tbody > tr[role="row"] > td:first-child {
			    padding-left: 20px !important;
			}

			.dataTables_wrapper table.dataTable.dtr-inline.collapsed > tbody > tr[role="row"] > td:first-child:before {
				margin-top: -5px;
			}
			
			.dataTables_wrapper .dataTable td{
				padding: 3px 5px !important;
			}

			.form-control[readonly] {
			    background-color: #F3F6F9;
			}
		</style>
		<!--begin::Dashboard-->
		@if (Request::segment(2) == '')

			<!--begin::Card-->
			<div class="card card-custom">
				<div class="card-header">
					<div class="card-title">
						<span class="card-icon">
							<i class="fas fa-users text-primary"></i>
						</span>
						<h3 class="card-label">Maçlar</h3>
					</div>
					<div class="card-toolbar">
						<!--begin::Button-->
						@if (Request::segment(2) == '' && Auth::user()->hasPermissionTo('add_match'))
							<a href="{{ url()->current() }}/add" class="btn btn-primary font-weight-bolder"><i class="fas fa-futbol"></i> Maç Ekle</a>
						@endif
						<!--end::Button-->
					</div>
				</div>
				<div class="card-body">
					<!--begin: Search Form-->
					<form class="mb-5" id='searchFormm'>

						<div class="row mb-6">

							<div class="col-lg-3 mb-5">
								<label> Lig </label>
								<select class="form-control datatable-input" id="select2_league_search">
									<option></option>
									@php
										//RoleProvince
										$roleProvince = collect(Auth::user()->roleprovince)->pluck('province_id');
								        
								        if($roleProvince->count() > 0){

								            $leagues = App\League::
									        select( 'leagues.id',
									                'leagues.name',
									                'leagues.active',
									                DB::raw("count(leagues_provinces.province_id) as count")
									        )->
									        leftJoin('leagues_provinces', 'leagues.id', '=', 'leagues_provinces.league_id')->
									        whereIn('leagues_provinces.province_id', $roleProvince)->groupBy('leagues.id')->get();

								        }else{
								            $leagues = App\League::all();
								        }
									@endphp
									@foreach ($leagues as $league)
										<option value="{{ $league->id }}"> {{ $league->name }} </option>
									@endforeach
								</select>
							</div>

							<div class="col-lg-3 mb-5">
								<label> Sezon </label>
								<select class="form-control datatable-input" id="select2_season_search" data-col-index="1">
									<option></option>

									@php
										//RoleProvince
										$roleProvince = collect(Auth::user()->roleprovince)->pluck('province_id');
								        if($roleProvince->count() > 0){

									        $seasons = App\Season::
									        select( 'seasons.id',
									                'seasons.league_id',
									                'seasons.name',
									                'seasons.polymorphic_ctype_id',
									                'seasons.year',
									                'seasons.start_date',
									                'seasons.end_date',
									                'seasons.active',
									                DB::raw("count(leagues_provinces.province_id) as count")
									        )->
									        leftJoin('leagues', 'leagues.id', '=', 'seasons.league_id')->
									        leftJoin('leagues_provinces', 'leagues.id', '=', 'leagues_provinces.league_id')->
											whereIn('leagues_provinces.province_id', $roleProvince)->groupBy('seasons.id')->get();

								        }else{
								            $seasons = App\Season::where('active', true)->orderBy('year', 'desc')->get();
								        }
									@endphp

									@foreach ($seasons as $season)
										<option value="{{ $season->id }}"> {{ $season->year }} {{ $season->league->name }} {{ $season->name }} </option>
									@endforeach
								</select>
							</div>

							<div class="col-lg-6 mb-5">
								<label>Tarih Aralığı</label>
								<div class="row">
									<div class="col">
										<div class="input-group date" id="daterange_start_search" data-target-input="nearest">
											<input type="text" class="form-control datetimepicker-input" placeholder="Başlangıç Tarihi" data-target="#daterange_start_search" />
											<div class="input-group-append" data-target="#daterange_start_search" data-toggle="datetimepicker">
												<span class="input-group-text">
													<i class="ki ki-calendar"></i>
												</span>
											</div>
										</div>
									</div>
									<div class="col">
										<div class="input-group date" id="daterange_end_search" data-target-input="nearest">
											<input type="text" class="form-control datetimepicker-input" placeholder="Bitiş Tarihi" data-target="#daterange_end_search" />
											<div class="input-group-append" data-target="#daterange_end_search" data-toggle="datetimepicker">
												<span class="input-group-text">
													<i class="ki ki-calendar"></i>
												</span>
											</div>
										</div>
									</div>
								</div>
								<input type="hidden" class="datatable-input" name="daterange_dates_search" id="daterange_dates_search" data-col-index="4">
							</div>

							<div class="col-lg-3 mb-5">
								<label> Takım Adı </label>
								<input type="text" class="form-control datatable-input" data-col-index="2" />
							</div>

							<div class="col-lg-3 mb-5">
								<label> Video Durumu </label>
								<select class="form-control datatable-input" id="select2_videostatus_search" data-col-index="6">
									<option></option>
									<option value="recordable"> Kayıt Edilebilir </option>
									<option value="recording"> Kayıt Ediliyor </option>
									<option value="recorded"> Kayıt Edildi </option>
									<option value="finished"> Tamamlandı </option>
								</select>
							</div>

							<div class="col-lg-3 mb-5">
								<label> Maç Durumu </label>
								<select class="form-control datatable-input" id="select2_completed_search" data-col-index="7">
									<option></option>
									<option value="t"> Tamamlandı </option>
									<option value="f"> Devam Ediyor </option>
								</select>
							</div>

							<div class="col-lg-3 text-right mb-5">
								<label>&#160;</label>
								<span style="display: block;">
									<button class="btn btn-primary btn-primary--icon btn-sm" id="kt_search">
										<span>
											<i class="fas fa-search"></i>
											<span>Search</span>
										</span>
									</button>&#160;&#160;
									<button class="btn btn-secondary btn-secondary--icon btn-sm" id="kt_reset">
										<span>
											<i class="far fa-times-circle"></i>
											<span>Reset</span>
										</span>
									</button>
								</span>
							</div>
						</div>

					</form>
					<!--begin: Datatable-->
					<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
					<table class="table table-bordered table-hover table-checkable tdt" id="kt_datatable_matches" style="margin-top: 13px !important">
					</table>
					<!--end: Datatable-->
				</div>
			</div>
			<!--end::Card-->
		@else
			@include('form_element._addForm')
		@endif
		<!--end::Dashboard-->

		<!--[html-partial:end:{"id":"demo1/dist/inc/view/partials/content/dashboards/demo1","page":"index"}]/-->

		<!--[html-partial:end:{"id":"demo1/dist/inc/view/demos/pages/index","page":"index"}]/-->
	</div>
	<!--end::Container-->
</div>

<!--end::Entry-->