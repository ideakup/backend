<!--begin::Entry-->
<div class="d-flex flex-column-fluid">

	<!--begin::Container-->
	<div class="container-fluid">

		<style type="text/css">
			.tdt td {
				white-space: nowrap;
			}

			.dataTables_wrapper table.dataTable.collapsed > tbody > tr[role="row"] > td:first-child {
			    padding-left: 20px !important;
			}

			.dataTables_wrapper table.dataTable.dtr-inline.collapsed > tbody > tr[role="row"] > td:first-child:before {
				margin-top: -5px;
			}
			
			.dataTables_wrapper .dataTable td{
				padding: 3px 5px !important;
			}

			.form-control[readonly] {
			    background-color: #F3F6F9;
			}
		</style>
		<!--begin::Dashboard-->
		@if (Request::segment(2) == '')

			<!--begin::Card-->
			<div class="card card-custom">
				<div class="card-header">
					<div class="card-title">
						<span class="card-icon">
							<i class="fas fa-box"></i>
						</span>
						<h3 class="card-label">Sezonlar</h3>
					</div>
					<div class="card-toolbar">
						<!--begin::Dropdown-->
						@if (Request::segment(2) == '')
							<div class="dropdown dropdown-inline mr-2">
								
								@if(Auth::user()->hasPermissionTo('add_seasonbonuspoint'))
									<a href="{{ url('/seasons/addbonus') }}" class="btn btn-success font-weight-bolder">
										<i class="fas fa-angle-double-up"></i> Bonus Puanları Ekle
									</a>
								@endif
								@if(Auth::user()->hasPermissionTo('add_season'))
									<button type="button" class="btn btn-light-primary font-weight-bolder dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<i class="fas fa-plus"></i> Sezon Ekle
									</button>
									<!--begin::Dropdown Menu-->
									<div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
										<!--begin::Navigation-->
										<ul class="navi flex-column navi-hover py-2">
											<li class="navi-header font-weight-bolder text-uppercase font-size-sm text-primary pb-2">Sezon Tipini Seçin: </li>
											<li class="navi-item">
												@if(Auth::user()->hasPermissionTo('add_pointseason'))
													<a href="{{ url('/seasonsp/add') }}" class="navi-link">
														<span class="navi-icon">
															<i class="fas fa-plus-square"></i>
														</span>
														<span class="navi-text">Puanlı S. Ekle</span>
													</a>
												@endif
											</li>
											<li class="navi-item">
												@if(Auth::user()->hasPermissionTo('add_fixtureseason'))
													<a href="{{ url('/seasonsf/add') }}" class="navi-link">
														<span class="navi-icon">
															<i class="fas fa-plus-square"></i>
														</span>
														<span class="navi-text">Fikstürlü S. Ekle</span>
													</a>
												@endif
											</li>
											<li class="navi-item">
												@if(Auth::user()->hasPermissionTo('add_eliminationseason'))
													<a href="{{ url('/seasonse/add') }}" class="navi-link">
														<span class="navi-icon">
															<i class="fas fa-plus-square"></i>
														</span>
														<span class="navi-text">Elemeli S. Ekle</span>
													</a>
												@endif
											</li>
										</ul>
										<!--end::Navigation-->
									</div>
									<!--end::Dropdown Menu-->
								@endif

							</div>
						@endif
						<!--end::Dropdown-->
					</div>
				</div>
				<div class="card-body">
					<!--begin: Search Form-->
					<form class="mb-5" id='searchFormm'>
						<div class="row mb-6">

							<div class="col-lg-3 mb-5">
								<label> Lig </label>
								<select class="form-control datatable-input" id="select2_league_search" data-col-index="1">
									<option></option>
									@php
										//RoleProvince
										$roleProvince = collect(Auth::user()->roleprovince)->pluck('province_id');
								        
								        if($roleProvince->count() > 0){

								            $leagues = App\League::
									        select( 'leagues.id',
									                'leagues.name',
									                'leagues.active',
									                DB::raw("count(leagues_provinces.province_id) as count")
									        )->
									        leftJoin('leagues_provinces', 'leagues.id', '=', 'leagues_provinces.league_id')->
									        whereIn('leagues_provinces.province_id', $roleProvince)->groupBy('leagues.id')->get();

								        }else{
								            $leagues = App\League::all();
								        }
									@endphp
									@foreach ($leagues as $league)
										<option value="{{ $league->id }}"> {{ $league->name }} </option>
									@endforeach
								</select>
							</div>

							<div class="col-lg-3 mb-5">
								<label> Yıl </label>
								<input type="number" class="form-control datatable-input" step="1" min="2005" max="2100" data-col-index="2" />
							</div>

							<div class="col-lg-3 mb-5">
								<label> Ad </label>
								<input type="text" class="form-control datatable-input" data-col-index="3" />
							</div>

							<div class="col-lg-3 mb-5">
								<label> Tip </label>
								<select class="form-control datatable-input" id="select2_league_types_search" data-col-index="4">
									<option></option>
									@if(Auth::user()->hasPermissionTo('view_pointseason'))
										<option value="34"> Puanlı Sezon </option>
									@endif
									@if(Auth::user()->hasPermissionTo('view_fixtureseason'))
										<option value="33"> Fikstürlü Sezon </option>
									@endif
									@if(Auth::user()->hasPermissionTo('view_eliminationseason'))
										<option value="32"> Elemeli Sezon </option>
									@endif
								</select>
							</div>

							<div class="col-lg-12 text-right mb-5">
								<label>&#160;</label>
								<span style="display: block;">
									<button class="btn btn-primary btn-primary--icon btn-sm" id="kt_search">
										<span>
											<i class="fas fa-search"></i>
											<span>Search</span>
										</span>
									</button>&#160;&#160;
									<button class="btn btn-secondary btn-secondary--icon btn-sm" id="kt_reset">
										<span>
											<i class="far fa-times-circle"></i>
											<span>Reset</span>
										</span>
									</button>
								</span>
							</div>

						</div>
					</form>
					<!--begin: Datatable-->
					<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
					<table class="table table-bordered table-hover table-checkable tdt" id="kt_datatable_seasons" style="margin-top: 13px !important">
					</table>
					<!--end: Datatable-->
				</div>
			</div>
			<!--end::Card-->

		@elseif (Request::segment(2) == 'addbonus')
			<!--begin::Card-->
			<div class="card card-custom">
				<div class="card-header">
					<div class="card-title">
						<span class="card-icon">
							<i class="fas fa-box"></i>
						</span>
						<h3 class="card-label">Sezonlar - Bonus Puanları Ekle</h3>
					</div>
					<div class="card-toolbar"> </div>
				</div>
				<div class="card-body">
					<!--begin: Datatable-->
					<form class="form" method="POST" action="{{ url('seasons/addbonus/save') }}" id="addBonusForm">

						<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
						<table class="table table-bordered table-hover table-checkable tdt" id="kt_datatable_seasons_addbonus" style="margin-top: 13px !important">
						</table>

						<button type="submit" class="btn btn-light-primary font-weight-bolder">
							<i class="fas fa-plus"></i> Bonus Puanları Ekle
						</button>
					</form>
					<!--end: Datatable-->
				</div>
			</div>
			<!--end::Card-->
		@elseif (Request::segment(2) == 'seasonsettings')
			@include('form_element._addForm')
		@endif
		<!--end::Dashboard-->

	</div>

	<!--end::Container-->
</div>

<!--end::Entry-->