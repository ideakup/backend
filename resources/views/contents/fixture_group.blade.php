<!--begin::Entry-->
<div class="d-flex flex-column-fluid">

	<!--begin::Container-->
	<div class="container-fluid">

		<!--[html-partial:begin:{"id":"demo1/dist/inc/view/demos/pages/index","page":"index"}]/-->

		<!--[html-partial:begin:{"id":"demo1/dist/inc/view/partials/content/dashboards/demo1","page":"index"}]/-->
		<style type="text/css">
			.tdt td {
				white-space: nowrap;
			}
			.form-control[readonly] {
			    background-color: #F3F6F9;
			}
		</style>
		<!--begin::Dashboard-->
		@if (Request::segment(2) == '')

			<!--begin::Card-->
			<div class="card card-custom">
				<div class="card-header">
					<div class="card-title">
						<span class="card-icon">
							<i class="fas fa-crosshairs"></i>
						</span>
						<h3 class="card-label">
					
						@if(!empty(request()->query('tree_id')))
							@php 
								$sss = App\Season::find(request()->query('tree_id'));
							@endphp
							{{ $sss->year.' '.$sss->league->name.' '.$sss->name }} - 
						@endif
						Grupları</h3>
						
					</div>
					<div class="card-toolbar">
						<!--begin::Button-->
						@if (true /*Request::segment(2) == '' && Auth::user()->hasPermissionTo('add_eliminationtree')*/)
							@php
								$queryString = '';
								if(!empty(request()->query('tree_id'))){
									$queryString = '?tree_id='.request()->query('tree_id');
								}
							@endphp
							<a href="{{ url()->current() }}/add{{$queryString}}" class="btn btn-primary font-weight-bolder"><i class="fas fa-plus"></i> Grup Ekle </a>
						@endif
						<!--end::Button-->
					</div>
				</div>
				<div class="card-body">
					<!--begin: Search Form-->
					<!--begin: Datatable-->
					<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
					<input type="hidden" name="tree_id" id="tree_id" value="{{ request()->query('tree_id') }}">
					<table class="table table-bordered table-hover table-checkable tdt" id="kt_datatable_fixture_group" style="margin-top: 13px !important">
					</table>
					<!--end: Datatable-->
				</div>
			</div>
			<!--end::Card-->
		@elseif (Request::segment(2) == 'tree')
			@include('contents.fixture_group_group')
		@else
			@include('form_element._addForm')
		@endif
		<!--end::Dashboard-->

		<!--[html-partial:end:{"id":"demo1/dist/inc/view/partials/content/dashboards/demo1","page":"index"}]/-->

		<!--[html-partial:end:{"id":"demo1/dist/inc/view/demos/pages/index","page":"index"}]/-->
	</div>

	<!--end::Container-->
</div>

<!--end::Entry-->