
<div class="d-flex flex-column-fluid">

	<div class="container-fluid">

		<style type="text/css">
			.tdt td {
				white-space: nowrap;
			}

			.dataTables_wrapper table.dataTable.collapsed > tbody > tr[role="row"] > td:first-child {
			    padding-left: 20px !important;
			}

			.dataTables_wrapper table.dataTable.dtr-inline.collapsed > tbody > tr[role="row"] > td:first-child:before {
				margin-top: -5px;
			}
			
			.dataTables_wrapper .dataTable td{
				padding: 3px 5px !important;
			}

			.form-control[readonly] {
			    background-color: #F3F6F9;
			}
		</style>

		@if (Request::segment(2) == '')

			<div class="card card-custom">
				<div class="card-header">
					<div class="card-title">
						<span class="card-icon">
							<i class="fas fa-user-shield text-primary"></i>
						</span>
						<h3 class="card-label">Yetkilendirme</h3>
					</div>
					<div class="card-toolbar">
						@if (Request::segment(2) == '' && Auth::user()->hasPermissionTo('add_permission'))
							<a href="{{ url()->current() }}/add" class="btn btn-primary font-weight-bolder"><i class="fas fa-plus"></i> Yetkilendirme Ekle</a>
						@endif
					</div>
				</div>
				<div class="card-body">
					<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

					<table class="table table-bordered table-hover table-checkable tdt" id="kt_datatable_roles" style="margin-top: 13px !important">
					</table>
				</div>
			</div>
		@elseif (Request::segment(2) == 'permission')

			<form class="form" method="POST" action="{{ url('roles/permission_save') }}" id='permmissionSaveForm'>

				{{ csrf_field() }}
                <input type="hidden" name="segment" value="{{ Request::segment(1) }}">
                <input type="hidden" name="crud" value="{{ Request::segment(2) }}">
                <input type="hidden" name="id" value="{{ Request::segment(3) }}">

				<div class="card card-custom gutter-b">
					<div class="card-header">
						<div class="card-title">
							<span class="card-icon">
								<i class="fas fa-user-shield text-primary"></i>
							</span>
							<h3 class="card-label">Yetkileri Düzenle ({{ $model_data->name }})</h3>
						</div>
						<div class="card-toolbar">
							<button type="submit" class="btn btn-success mr-2">Kaydet</button>
						</div>
					</div>
				</div>

				@php
					$groups = Spatie\Permission\Models\Permission::select('group', 'gorder')->where('group', 'not like', '!!!!%')->where('group', 'not like', '????%')->distinct()->orderBy('gorder', 'desc')->get();
				@endphp

				<div class="row">
					@foreach($groups as $group)
						<div class="col-lg-3">
							<div class="card card-custom gutter-b">
								<div class="card-header">
									<div class="card-title">
										<h3 class="card-label">{{ $group->group }}</h3>
									</div>
								</div>
								<div class="card-body">
								
									@php
										$permissions = Spatie\Permission\Models\Permission::where('group', $group->group)->orderBy('order','asc')->get();
									@endphp
									<div class="form-group">
										<div class="checkbox-list">
											@foreach($permissions as $permission)
												@php
													if(starts_with($permission->name, 'add_')) {
														$cb_color = 'checkbox-primary';
													} elseif(starts_with($permission->name, 'change_')) {
														$cb_color = 'checkbox-info';
													} elseif(starts_with($permission->name, 'delete_')) {
														$cb_color = 'checkbox-danger';
													} elseif(starts_with($permission->name, 'view_')) {
														$cb_color = 'checkbox-success';
													}
												@endphp	
												<label class="checkbox {{$cb_color}}" style="margin-right: 0;">
													<input type="checkbox" name="permissions[]" value="{{$permission->name}}" 
													@if($model_data->permissions->whereIn('name', $permission->name)->count()) checked="checked" @endif >
													<span></span> {{$permission->description}}
												</label>
											@endforeach
										</div>
									</div>
								</div>
							</div>
						</div>
					@endforeach
				</div>

			</form>

		@else
			@include('form_element._addForm')
		@endif

	</div>

</div>
