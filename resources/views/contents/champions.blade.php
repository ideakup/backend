<div class="d-flex flex-column-fluid">
	<div class="container-fluid">
		<style type="text/css">
			.tdt td {
				white-space: nowrap;
			}

			.dataTables_wrapper table.dataTable.collapsed > tbody > tr[role="row"] > td:first-child {
			    padding-left: 20px !important;
			}

			.dataTables_wrapper table.dataTable.dtr-inline.collapsed > tbody > tr[role="row"] > td:first-child:before {
				margin-top: -5px;
			}
			
			.dataTables_wrapper .dataTable td{
				padding: 3px 5px !important;
			}

			.form-control[readonly] {
			    background-color: #F3F6F9;
			}
		</style>
		<!--begin::Dashboard-->
		@if (Request::segment(2) == '')

			<!--begin::Card-->
			<div class="card card-custom">
				<div class="card-header">
					<div class="card-title">
						<span class="card-icon">
							<i class="fas fa-boxes"></i>
						</span>
						<h3 class="card-label">Şampiyonlar</h3>
					</div>
					<div class="card-toolbar">
						<!--begin::Button-->
						@if (Request::segment(2) == '' && Auth::user()->hasPermissionTo('add_league'))
							<a href="{{ url()->current() }}/add" class="btn btn-primary font-weight-bolder"><i class="fas fa-plus"></i> Şampiyon Ekle</a>
						@endif
						<!--end::Button-->
					</div>
				</div>
				<div class="card-body">
					<!--begin: Search Form-->
					<form class="mb-5" id='searchFormm'>
						<div class="row mb-6">

							<div class="col-lg-3 mb-5">
								<label> Lig </label>
								<select class="form-control datatable-input" id="select2_league_search">
									<option></option>
									@php
										//RoleProvince
										$roleProvince = collect(Auth::user()->roleprovince)->pluck('province_id');
								        
								        if($roleProvince->count() > 0){

								            $leagues = App\League::
									        select( 'leagues.id',
									                'leagues.name',
									                'leagues.active',
									                DB::raw("count(leagues_provinces.province_id) as count")
									        )->
									        leftJoin('leagues_provinces', 'leagues.id', '=', 'leagues_provinces.league_id')->
									        whereIn('leagues_provinces.province_id', $roleProvince)->groupBy('leagues.id')->get();

								        }else{
								            $leagues = App\League::all();
								        }
									@endphp
									@foreach ($leagues as $league)
										<option value="{{ $league->id }}"> {{ $league->name }} </option>
									@endforeach
								</select>
							</div>

							<div class="col-lg-3 mb-5">
								<label> Yıl </label>
								<input type="number" class="form-control datatable-input" step="1" min="2005" max="2100" data-col-index="3" />
							</div>

							<div class="col-lg-6 text-right mb-5">
								<label>&#160;</label>
								<span style="display: block;">
									<button class="btn btn-primary btn-primary--icon btn-sm" id="kt_search">
										<span>
											<i class="fas fa-search"></i>
											<span>Search</span>
										</span>
									</button>&#160;&#160;
									<button class="btn btn-secondary btn-secondary--icon btn-sm" id="kt_reset">
										<span>
											<i class="far fa-times-circle"></i>
											<span>Reset</span>
										</span>
									</button>
								</span>
							</div>
						</div>
					</form>
					<!--begin: Datatable-->
					<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
					<table class="table table-bordered table-hover table-checkable tdt" id="kt_datatable_champions" style="margin-top: 13px !important">
					</table>
					<!--end: Datatable-->
				</div>
			</div>
			<!--end::Card-->
		@else
			@include('form_element._addForm')
		@endif
		<!--end::Dashboard-->

		<!--[html-partial:end:{"id":"demo1/dist/inc/view/partials/content/dashboards/demo1","page":"index"}]/-->

		<!--[html-partial:end:{"id":"demo1/dist/inc/view/demos/pages/index","page":"index"}]/-->
	</div>

	<!--end::Container-->
</div>

<!--end::Entry-->