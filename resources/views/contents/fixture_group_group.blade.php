@php

	//dump($model_data);
	//dump($model_data->tree_build);
	//dump($model_data->items->where('level', 3)->where('side', 'left')->first());

@endphp

<!--begin::Card-->
<div class="card card-custom">
	<div class="card-header">
		<div class="card-title">
			<span class="card-icon">
				<i class="fas fa-sitemap"></i>
			</span>
			<h3 class="card-label">{{ $model_data->name }} Eleme Ağacı</h3>
		</div>
		<div class="card-toolbar">
		</div>
	</div>
	<div class="card-body">

		<!--begin: Search Form-->
		<form class="form" method="POST" action="{{ url('elimination_tree/tree_save') }}" id="eliminationTreeItemForm">
        
            {{ csrf_field() }}
            <input type="hidden" name="segment" value="{{ Request::segment(1) }}">
            <input type="hidden" name="crud" value="{{ Request::segment(2) }}">
            <input type="hidden" name="id" value="{{ Request::segment(3) }}">
            @if(!empty(request()->query('tree_id')))
                <input type="hidden" name="tree_id" value="{{ request()->query('tree_id') }}">
            @endif
		
			<div class="accordion accordion-solid accordion-toggle">

				@if($model_data->tree_build >= 2)
					<div class="form-group row">
						<div class="col-sm-6">

							<div class="card">
								<div class="card-header">
									<div class="card-title" data-toggle="collapse">
									<i class="fas fa-arrow-alt-circle-left"></i>SOL</div>
								</div>
								<div class="collapse show">
									<div class="card-body">

										@if($model_data->tree_build >= 7)
											<div class="row">
												@php
													$tour4_left = $model_data->items->where('level', 6)->where('side', 'left')->first();
												@endphp

												<div class="col-sm-12">
													<h5 class="col-lg-12 text-center">{{ $tour4_left->level_title }}</h5>
													<div class="separator separator-solid separator-border-3 col-lg-12"></div>
													</br>	
												</div>

												<div class="col-lg-6 col-sm-12">
													<div class="form-group row">
														<label class="col-form-label col-sm-12">Takım 1</label>
														<div class="col-sm-12">
															<select class="form-control select2_team_et" name="tour4_left_team1" 
																@if(!empty($tour4_left->match_id))
																	disabled="disabled"
																@endif
															>
																<option></option>
																@foreach($model_data->eliminaton_tree_teams as $etTeam)

																	<option value="{{ $etTeam->team_id }}"
																		@if(!empty($tour4_left->team1) && $tour4_left->team1->id == $etTeam->team_id)
																			selected="selected" 
																		@endif
																	> {{ $etTeam->team->name }} </option>

																@endforeach
															</select>
														</div>
													</div>
												</div>

												<div class="col-lg-6 col-sm-12">
													<div class="form-group row">
														<label class="col-form-label col-sm-12">Takım 2</label>
														<div class="col-sm-12">
															<select class="form-control select2_team_et" name="tour4_left_team2" 
																@if(!empty($tour4_left->match_id))
																	disabled="disabled"
																@endif
															>
																<option></option>
																@foreach($model_data->eliminaton_tree_teams as $etTeam)

																	<option value="{{ $etTeam->team_id }}"
																		@if(!empty($tour4_left->team2) && $tour4_left->team2->id == $etTeam->team_id)
																			selected="selected" 
																		@endif
																	> {{ $etTeam->team->name }} </option>

																@endforeach
															</select>
														</div>
													</div>
												</div>

												<div class="col-lg-6 col-sm-12">
													<div class="form-group row">
														<label class="col-form-label col-sm-12">Maç Tarihi</label>
														<div class="col-sm-12">
															<div class="input-group input-group-solid datepicker_et" id="tour4_left_match_date" data-target-input="nearest">
													            <input type="text" class="form-control form-control-solid datetimepicker-input " 
														            name="tour4_left_match_date"
														            placeholder="Maç Tarihi" 
														            data-target="#tour4_left_match_date" 
														            @if(!empty($tour4_left->date))
														            	value="{{ Carbon\Carbon::parse($tour4_left->date)->format('d.m.Y') }}"
														            @endif

															        @if(!empty($tour4_left->match_id))
																		disabled="disabled"
																	@endif
													            />
													            <div class="input-group-append" data-target="#tour4_left_match_date" data-toggle="datetimepicker">
													                <span class="input-group-text">
													                    <i class="fas fa-calendar-alt"></i>
													                </span>
													            </div>
													        </div>
														</div>
													</div>
												</div>

												<div class="col-lg-6 col-sm-12">
													<div class="form-group row">
														<label class="col-form-label col-sm-12">Maç Saati</label>
														<div class="col-sm-12">

															<div class="input-group input-group-solid timepicker_et" id="tour4_left_match_time" data-target-input="nearest">
													            <input type="text" class="form-control form-control-solid datetimepicker-input " 
														            name="tour4_left_match_time"
														            placeholder="Maç Saati" 
														            data-target="#tour4_left_match_time"
														            value="{{ $tour4_left->time }}"

														            @if(!empty($tour4_left->match_id))
																		disabled="disabled"
																	@endif
													            />
													            <div class="input-group-append" data-target="#tour4_left_match_time" data-toggle="datetimepicker">
													                <span class="input-group-text">
													                    <i class="fas fa-clock"></i>
													                </span>
													            </div>
													        </div>
														</div>
													</div>
												</div>

												@if(empty($tour4_left->match_id))
													<div class="col-lg-12 col-sm-12">
														<div class="form-group">
															<div class="checkbox-inline">
																<label class="checkbox checkbox-lg">
																<input type="checkbox" name="tour4_left_match_create">
																<span></span> Maç Oluştur </label>
															</div>
														</div>
													</div>
												@else
													<div class="col-lg-12 col-sm-12">
														<div class="alert alert-primary text-center" role="alert">
														    Maç detaylarını incelemek için <a href="{{ url('matches/edit/'.$tour4_left->match_id) }}" class="alert-link">buraya</a> tıklayınız.
														</div>
													</div>
												@endif

												<div class="col-sm-12">
													<div class="separator separator-solid separator-border-3 separator-primary"></div>
												</div>

											</div>
											<br />
										@endif

										@if($model_data->tree_build >= 6)
											<div class="row">
												@php
													$tour3_left = $model_data->items->where('level', 5)->where('side', 'left')->first();
												@endphp

												<div class="col-sm-12">
													<h5 class="col-lg-12 text-center">{{ $tour3_left->level_title }}</h5>
													<div class="separator separator-solid separator-border-3 col-lg-12"></div>
													</br>	
												</div>

												<div class="col-lg-6 col-sm-12">
													<div class="form-group row">
														<label class="col-form-label col-sm-12">Takım 1</label>
														<div class="col-sm-12">
															<select class="form-control select2_team_et" name="tour3_left_team1" 
																@if(!empty($tour3_left->match_id))
																	disabled="disabled"
																@endif
															>
																<option></option>
																@foreach($model_data->eliminaton_tree_teams as $etTeam)

																	<option value="{{ $etTeam->team_id }}"
																		@if(!empty($tour3_left->team1) && $tour3_left->team1->id == $etTeam->team_id)
																			selected="selected" 
																		@endif
																	> {{ $etTeam->team->name }} </option>

																@endforeach
															</select>
														</div>
													</div>
												</div>

												<div class="col-lg-6 col-sm-12">
													<div class="form-group row">
														<label class="col-form-label col-sm-12">Takım 2</label>
														<div class="col-sm-12">
															<select class="form-control select2_team_et" name="tour3_left_team2" 
																@if(!empty($tour3_left->match_id))
																	disabled="disabled"
																@endif
															>
																<option></option>
																@foreach($model_data->eliminaton_tree_teams as $etTeam)

																	<option value="{{ $etTeam->team_id }}"
																		@if(!empty($tour3_left->team2) && $tour3_left->team2->id == $etTeam->team_id)
																			selected="selected" 
																		@endif
																	> {{ $etTeam->team->name }} </option>

																@endforeach
															</select>
														</div>
													</div>
												</div>

												<div class="col-lg-6 col-sm-12">
													<div class="form-group row">
														<label class="col-form-label col-sm-12">Maç Tarihi</label>
														<div class="col-sm-12">
															<div class="input-group input-group-solid datepicker_et" id="tour3_left_match_date" data-target-input="nearest">
													            <input type="text" class="form-control form-control-solid datetimepicker-input " 
														            name="tour3_left_match_date"
														            placeholder="Maç Tarihi" 
														            data-target="#tour3_left_match_date" 
														            @if(!empty($tour3_left->date))
														            	value="{{ Carbon\Carbon::parse($tour3_left->date)->format('d.m.Y') }}"
														            @endif 
																	@if(!empty($tour3_left->match_id))
																		disabled="disabled"
																	@endif
															
													            />
													            <div class="input-group-append" data-target="#tour3_left_match_date" data-toggle="datetimepicker">
													                <span class="input-group-text">
													                    <i class="fas fa-calendar-alt"></i>
													                </span>
													            </div>
													        </div>
														</div>
													</div>
												</div>

												<div class="col-lg-6 col-sm-12">
													<div class="form-group row">
														<label class="col-form-label col-sm-12">Maç Saati</label>
														<div class="col-sm-12">

															<div class="input-group input-group-solid timepicker_et" id="tour3_left_match_time" data-target-input="nearest">
													            <input type="text" class="form-control form-control-solid datetimepicker-input " 
														            name="tour3_left_match_time"
														            placeholder="Maç Saati" 
														            data-target="#tour3_left_match_time"
														            value="{{ $tour3_left->time }}" 
																	@if(!empty($tour3_left->match_id))
																		disabled="disabled"
																	@endif
															
													            />
													            <div class="input-group-append" data-target="#tour3_left_match_time" data-toggle="datetimepicker">
													                <span class="input-group-text">
													                    <i class="fas fa-clock"></i>
													                </span>
													            </div>
													        </div>
														</div>
													</div>
												</div>

												@if(empty($tour3_left->match_id))
													<div class="col-lg-12 col-sm-12">
														<div class="form-group">
															<div class="checkbox-inline">
																<label class="checkbox checkbox-lg">
																<input type="checkbox" name="tour3_left_match_create">
																<span></span> Maç Oluştur </label>
															</div>
														</div>
													</div>
												@else
													<div class="col-lg-12 col-sm-12">
														<div class="alert alert-primary text-center" role="alert">
														    Maç detaylarını incelemek için <a href="{{ url('matches/edit/'.$tour3_left->match_id) }}" class="alert-link">buraya</a> tıklayınız.
														</div>
													</div>
												@endif

												<div class="col-sm-12">
													<div class="separator separator-solid separator-border-3 separator-primary"></div>
												</div>

											</div>
											<br />
										@endif

										@if($model_data->tree_build >= 5)
											<div class="row">
												@php
													$tour2_left = $model_data->items->where('level', 4)->where('side', 'left')->first();
												@endphp

												<div class="col-sm-12">
													<h5 class="col-lg-12 text-center">{{ $tour2_left->level_title }}</h5>
													<div class="separator separator-solid separator-border-3 col-lg-12"></div>
													</br>	
												</div>

												<div class="col-lg-6 col-sm-12">
													<div class="form-group row">
														<label class="col-form-label col-sm-12">Takım 1</label>
														<div class="col-sm-12">
															<select class="form-control select2_team_et" name="tour2_left_team1" 
																@if(!empty($tour2_left->match_id))
																	disabled="disabled"
																@endif
															>
																<option></option>
																@foreach($model_data->eliminaton_tree_teams as $etTeam)

																	<option value="{{ $etTeam->team_id }}"
																		@if(!empty($tour2_left->team1) && $tour2_left->team1->id == $etTeam->team_id)
																			selected="selected" 
																		@endif
																	> {{ $etTeam->team->name }} </option>

																@endforeach
															</select>
														</div>
													</div>
												</div>

												<div class="col-lg-6 col-sm-12">
													<div class="form-group row">
														<label class="col-form-label col-sm-12">Takım 2</label>
														<div class="col-sm-12">
															<select class="form-control select2_team_et" name="tour2_left_team2" 
																@if(!empty($tour2_left->match_id))
																	disabled="disabled"
																@endif
															>
																<option></option>
																@foreach($model_data->eliminaton_tree_teams as $etTeam)

																	<option value="{{ $etTeam->team_id }}"
																		@if(!empty($tour2_left->team2) && $tour2_left->team2->id == $etTeam->team_id)
																			selected="selected" 
																		@endif
																	> {{ $etTeam->team->name }} </option>

																@endforeach
															</select>
														</div>
													</div>
												</div>

												<div class="col-lg-6 col-sm-12">
													<div class="form-group row">
														<label class="col-form-label col-sm-12">Maç Tarihi</label>
														<div class="col-sm-12">
															<div class="input-group input-group-solid datepicker_et" id="tour2_left_match_date" data-target-input="nearest">
													            <input type="text" class="form-control form-control-solid datetimepicker-input " 
														            name="tour2_left_match_date"
														            placeholder="Maç Tarihi" 
														            data-target="#tour2_left_match_date" 
														            @if(!empty($tour2_left->date))
														            	value="{{ Carbon\Carbon::parse($tour2_left->date)->format('d.m.Y') }}"
														            @endif 
																	@if(!empty($tour2_left->match_id))
																		disabled="disabled"
																	@endif
															
													            />
													            <div class="input-group-append" data-target="#tour2_left_match_date" data-toggle="datetimepicker">
													                <span class="input-group-text">
													                    <i class="fas fa-calendar-alt"></i>
													                </span>
													            </div>
													        </div>
														</div>
													</div>
												</div>

												<div class="col-lg-6 col-sm-12">
													<div class="form-group row">
														<label class="col-form-label col-sm-12">Maç Saati</label>
														<div class="col-sm-12">

															<div class="input-group input-group-solid timepicker_et" id="tour2_left_match_time" data-target-input="nearest">
													            <input type="text" class="form-control form-control-solid datetimepicker-input " 
														            name="tour2_left_match_time"
														            placeholder="Maç Saati" 
														            data-target="#tour2_left_match_time"
														            value="{{ $tour2_left->time }}" 
																	@if(!empty($tour2_left->match_id))
																		disabled="disabled"
																	@endif
															
													            />
													            <div class="input-group-append" data-target="#tour2_left_match_time" data-toggle="datetimepicker">
													                <span class="input-group-text">
													                    <i class="fas fa-clock"></i>
													                </span>
													            </div>
													        </div>
														</div>
													</div>
												</div>

												@if(empty($tour2_left->match_id))
													<div class="col-lg-12 col-sm-12">
														<div class="form-group">
															<div class="checkbox-inline">
																<label class="checkbox checkbox-lg">
																<input type="checkbox" name="tour2_left_match_create">
																<span></span> Maç Oluştur </label>
															</div>
														</div>
													</div>
												@else
													<div class="col-lg-12 col-sm-12">
														<div class="alert alert-primary text-center" role="alert">
														    Maç detaylarını incelemek için <a href="{{ url('matches/edit/'.$tour2_left->match_id) }}" class="alert-link">buraya</a> tıklayınız.
														</div>
													</div>
												@endif
												

												<div class="col-sm-12">
													<div class="separator separator-solid separator-border-3 separator-primary"></div>
												</div>

											</div>
											<br />
										@endif
										
										@if($model_data->tree_build >= 4)
											<div class="row">
												@php
													$tour1_left = $model_data->items->where('level', 3)->where('side', 'left')->first();
												@endphp

												<div class="col-sm-12">
													<h5 class="col-lg-12 text-center">{{ $tour1_left->level_title }}</h5>
													<div class="separator separator-solid separator-border-3 col-lg-12"></div>
													</br>	
												</div>

												<div class="col-lg-6 col-sm-12">
													<div class="form-group row">
														<label class="col-form-label col-sm-12">Takım 1</label>
														<div class="col-sm-12">
															<select class="form-control select2_team_et" name="tour1_left_team1" 
																@if(!empty($tour1_left->match_id))
																	disabled="disabled"
																@endif
															>
																<option></option>
																@foreach($model_data->eliminaton_tree_teams as $etTeam)

																	<option value="{{ $etTeam->team_id }}"
																		@if(!empty($tour1_left->team1) && $tour1_left->team1->id == $etTeam->team_id)
																			selected="selected" 
																		@endif
																	> 
																		{{ $etTeam->team->name }}
																	</option>

																@endforeach
															</select>
														</div>
													</div>
												</div>

												<div class="col-lg-6 col-sm-12">
													<div class="form-group row">
														<label class="col-form-label col-sm-12">Takım 2</label>
														<div class="col-sm-12">
															<select class="form-control select2_team_et" name="tour1_left_team2" 
																@if(!empty($tour1_left->match_id))
																	disabled="disabled"
																@endif
															>
																<option></option>
																@foreach($model_data->eliminaton_tree_teams as $etTeam)

																	<option value="{{ $etTeam->team_id }}"
																		@if(!empty($tour1_left->team2) && $tour1_left->team2->id == $etTeam->team_id)
																			selected="selected" 
																		@endif
																	> {{ $etTeam->team->name }} </option>

																@endforeach
															</select>
														</div>
													</div>
												</div>

												<div class="col-lg-6 col-sm-12">
													<div class="form-group row">
														<label class="col-form-label col-sm-12">Maç Tarihi</label>
														<div class="col-sm-12">
															<div class="input-group input-group-solid datepicker_et" id="tour1_left_match_date" data-target-input="nearest">
													            <input type="text" class="form-control form-control-solid datetimepicker-input " 
														            name="tour1_left_match_date"
														            placeholder="Maç Tarihi" 
														            data-target="#tour1_left_match_date" 
														            @if(!empty($tour1_left->date))
														            	value="{{ Carbon\Carbon::parse($tour1_left->date)->format('d.m.Y') }}"
														            @endif 
																	@if(!empty($tour1_left->match_id))
																		disabled="disabled"
																	@endif
															
													            />
													            <div class="input-group-append" data-target="#tour1_left_match_date" data-toggle="datetimepicker">
													                <span class="input-group-text">
													                    <i class="fas fa-calendar-alt"></i>
													                </span>
													            </div>
													        </div>
														</div>
													</div>
												</div>

												<div class="col-lg-6 col-sm-12">
													<div class="form-group row">
														<label class="col-form-label col-sm-12">Maç Saati</label>
														<div class="col-sm-12">

															<div class="input-group input-group-solid timepicker_et" id="tour1_left_match_time" data-target-input="nearest">
													            <input type="text" class="form-control form-control-solid datetimepicker-input " 
														            name="tour1_left_match_time"
														            placeholder="Maç Saati" 
														            data-target="#tour1_left_match_time"
														            value="{{ $tour1_left->time }}" 
																	@if(!empty($tour1_left->match_id))
																		disabled="disabled"
																	@endif
															
													            />
													            <div class="input-group-append" data-target="#tour1_left_match_time" data-toggle="datetimepicker">
													                <span class="input-group-text">
													                    <i class="fas fa-clock"></i>
													                </span>
													            </div>
													        </div>
														</div>
													</div>
												</div>


												@if(empty($tour1_left->match_id))
													<div class="col-lg-12 col-sm-12">
														<div class="form-group">
															<div class="checkbox-inline">
																<label class="checkbox checkbox-lg">
																<input type="checkbox" name="tour1_left_match_create">
																<span></span> Maç Oluştur </label>
															</div>
														</div>
													</div>
												@else
													<div class="col-lg-12 col-sm-12">
														<div class="alert alert-primary text-center" role="alert">
														    Maç detaylarını incelemek için <a href="{{ url('matches/edit/'.$tour1_left->match_id) }}" class="alert-link">buraya</a> tıklayınız.
														</div>
													</div>
												@endif

												<div class="col-sm-12">
													<div class="separator separator-solid separator-border-3 separator-primary"></div>
												</div>

											</div>
											<br />
										@endif

										@if($model_data->tree_build >= 3)
											<div class="row">
												@php
													$quarterfinal_left = $model_data->items->where('level', 2)->where('side', 'left')->first();
												@endphp

												<div class="col-sm-12">
													<h5 class="col-lg-12 text-center">Çeyrek Final</h5>
													<div class="separator separator-solid separator-border-3 col-lg-12"></div>
													</br>	
												</div>

												<div class="col-lg-6 col-sm-12">
													<div class="form-group row">
														<label class="col-form-label col-sm-12">Takım 1</label>
														<div class="col-sm-12">
															<select class="form-control select2_team_et" name="quarterfinal_left_team1" 
																@if(!empty($quarterfinal_left->match_id))
																	disabled="disabled"
																@endif
															>
																<option></option>
																@foreach($model_data->eliminaton_tree_teams as $etTeam)

																	<option value="{{ $etTeam->team_id }}"
																		@if(!empty($quarterfinal_left->team1) && $quarterfinal_left->team1->id == $etTeam->team_id)
																			selected="selected" 
																		@endif
																	> {{ $etTeam->team->name }} </option>

																@endforeach
															</select>
														</div>
													</div>
												</div>

												<div class="col-lg-6 col-sm-12">
													<div class="form-group row">
														<label class="col-form-label col-sm-12">Takım 2</label>
														<div class="col-sm-12">
															<select class="form-control select2_team_et" name="quarterfinal_left_team2" 
																@if(!empty($quarterfinal_left->match_id))
																	disabled="disabled"
																@endif
															>
																<option></option>
																@foreach($model_data->eliminaton_tree_teams as $etTeam)

																	<option value="{{ $etTeam->team_id }}"
																		@if(!empty($quarterfinal_left->team2) && $quarterfinal_left->team2->id == $etTeam->team_id)
																			selected="selected" 
																		@endif
																	> {{ $etTeam->team->name }} </option>

																@endforeach
															</select>
														</div>
													</div>
												</div>

												<div class="col-lg-6 col-sm-12">
													<div class="form-group row">
														<label class="col-form-label col-sm-12">Maç Tarihi</label>
														<div class="col-sm-12">
															<div class="input-group input-group-solid datepicker_et" id="quarterfinal_left_match_date" data-target-input="nearest">
													            <input type="text" class="form-control form-control-solid datetimepicker-input " 
														            name="quarterfinal_left_match_date"
														            placeholder="Maç Tarihi" 
														            data-target="#quarterfinal_left_match_date" 
														            @if(!empty($quarterfinal_left->date))
														            	value="{{ Carbon\Carbon::parse($quarterfinal_left->date)->format('d.m.Y') }}"
														            @endif 
																	@if(!empty($quarterfinal_left->match_id))
																		disabled="disabled"
																	@endif
															
													            />
													            <div class="input-group-append" data-target="#quarterfinal_left_match_date" data-toggle="datetimepicker">
													                <span class="input-group-text">
													                    <i class="fas fa-calendar-alt"></i>
													                </span>
													            </div>
													        </div>
														</div>
													</div>
												</div>

												<div class="col-lg-6 col-sm-12">
													<div class="form-group row">
														<label class="col-form-label col-sm-12">Maç Saati</label>
														<div class="col-sm-12">

															<div class="input-group input-group-solid timepicker_et" id="quarterfinal_left_match_time" data-target-input="nearest">
													            <input type="text" class="form-control form-control-solid datetimepicker-input " 
														            name="quarterfinal_left_match_time"
														            placeholder="Maç Saati" 
														            data-target="#quarterfinal_left_match_time"
														            value="{{ $quarterfinal_left->time }}" 
																	@if(!empty($quarterfinal_left->match_id))
																		disabled="disabled"
																	@endif
															
													            />
													            <div class="input-group-append" data-target="#quarterfinal_left_match_time" data-toggle="datetimepicker">
													                <span class="input-group-text">
													                    <i class="fas fa-clock"></i>
													                </span>
													            </div>
													        </div>
														</div>
													</div>
												</div>

												@if(empty($quarterfinal_left->match_id))
													<div class="col-lg-12 col-sm-12">
														<div class="form-group">
															<div class="checkbox-inline">
																<label class="checkbox checkbox-lg">
																<input type="checkbox" name="quarterfinal_left_match_create">
																<span></span> Maç Oluştur </label>
															</div>
														</div>
													</div>
												@else
													<div class="col-lg-12 col-sm-12">
														<div class="alert alert-primary text-center" role="alert">
														    Maç detaylarını incelemek için <a href="{{ url('matches/edit/'.$quarterfinal_left->match_id) }}" class="alert-link">buraya</a> tıklayınız.
														</div>
													</div>
												@endif

												<div class="col-sm-12">
													<div class="separator separator-solid separator-border-3 separator-primary"></div>
												</div>

											</div>
											<br />
										@endif

										@if($model_data->tree_build >= 2)
											<div class="row">
												@php
													$semifinal_left = $model_data->items->where('level', 1)->where('side', 'left')->first();
												@endphp

												<div class="col-sm-12">
													<h5 class="col-lg-12 text-center">Yarı Final</h5>
													<div class="separator separator-solid separator-border-3 col-lg-12"></div>
													</br>	
												</div>

												<div class="col-lg-6 col-sm-12">
													<div class="form-group row">
														<label class="col-form-label col-sm-12">Takım 1</label>
														<div class="col-sm-12">
															<select class="form-control select2_team_et" name="semifinal_left_team1" 
																@if(!empty($semifinal_left->match_id))
																	disabled="disabled"
																@endif
															>
																<option></option>
																@foreach($model_data->eliminaton_tree_teams as $etTeam)

																	<option value="{{ $etTeam->team_id }}"
																		@if(!empty($semifinal_left->team1) && $semifinal_left->team1->id == $etTeam->team_id)
																			selected="selected" 
																		@endif
																	> {{ $etTeam->team->name }} </option>

																@endforeach
															</select>
														</div>
													</div>
												</div>

												<div class="col-lg-6 col-sm-12">
													<div class="form-group row">
														<label class="col-form-label col-sm-12">Takım 2</label>
														<div class="col-sm-12">
															<select class="form-control select2_team_et" name="semifinal_left_team2" 
																@if(!empty($semifinal_left->match_id))
																	disabled="disabled"
																@endif
															>
																<option></option>
																@foreach($model_data->eliminaton_tree_teams as $etTeam)

																	<option value="{{ $etTeam->team_id }}"
																		@if(!empty($semifinal_left->team2) && $semifinal_left->team2->id == $etTeam->team_id)
																			selected="selected" 
																		@endif
																	> {{ $etTeam->team->name }} </option>

																@endforeach
															</select>
														</div>
													</div>
												</div>

												<div class="col-lg-6 col-sm-12">
													<div class="form-group row">
														<label class="col-form-label col-sm-12">Maç Tarihi</label>
														<div class="col-sm-12">
															<div class="input-group input-group-solid datepicker_et" id="semifinal_left_match_date" data-target-input="nearest">
													            <input type="text" class="form-control form-control-solid datetimepicker-input " 
														            name="semifinal_left_match_date"
														            placeholder="Maç Tarihi" 
														            data-target="#semifinal_left_match_date" 
														            @if(!empty($semifinal_left->date))
														            	value="{{ Carbon\Carbon::parse($semifinal_left->date)->format('d.m.Y') }}"
														            @endif 
																	@if(!empty($semifinal_left->match_id))
																		disabled="disabled"
																	@endif
															
													            />
													            <div class="input-group-append" data-target="#semifinal_left_match_date" data-toggle="datetimepicker">
													                <span class="input-group-text">
													                    <i class="fas fa-calendar-alt"></i>
													                </span>
													            </div>
													        </div>
														</div>
													</div>
												</div>

												<div class="col-lg-6 col-sm-12">
													<div class="form-group row">
														<label class="col-form-label col-sm-12">Maç Saati</label>
														<div class="col-sm-12">

															<div class="input-group input-group-solid timepicker_et" id="semifinal_left_match_time" data-target-input="nearest">
													            <input type="text" class="form-control form-control-solid datetimepicker-input " 
														            name="semifinal_left_match_time"
														            placeholder="Maç Saati" 
														            data-target="#semifinal_left_match_time"
														            value="{{ $semifinal_left->time }}" 
																	@if(!empty($semifinal_left->match_id))
																		disabled="disabled"
																	@endif
															
													            />
													            <div class="input-group-append" data-target="#semifinal_left_match_time" data-toggle="datetimepicker">
													                <span class="input-group-text">
													                    <i class="fas fa-clock"></i>
													                </span>
													            </div>
													        </div>
														</div>
													</div>
												</div>

												@if(empty($semifinal_left->match_id))
													<div class="col-lg-12 col-sm-12">
														<div class="form-group">
															<div class="checkbox-inline">
																<label class="checkbox checkbox-lg">
																<input type="checkbox" name="semifinal_left_match_create">
																<span></span> Maç Oluştur </label>
															</div>
														</div>
													</div>
												@else
													<div class="col-lg-12 col-sm-12">
														<div class="alert alert-primary text-center" role="alert">
														    Maç detaylarını incelemek için <a href="{{ url('matches/edit/'.$semifinal_left->match_id) }}" class="alert-link">buraya</a> tıklayınız.
														</div>
													</div>
												@endif

												<div class="col-sm-12">
													<div class="separator separator-solid separator-border-3 separator-primary"></div>
												</div>

											</div>
										@endif

									</div>
								</div>
							</div>

						</div>

						<div class="col-sm-6">

							<div class="card">
								<div class="card-header">
									<div class="card-title" data-toggle="collapse">
									<i class="fas fa-arrow-alt-circle-right"></i>SAĞ</div>
								</div>
								<div class="collapse show">
									<div class="card-body">
										
										
										@if($model_data->tree_build >= 7)
											<div class="row">
												@php
													$tour4_right = $model_data->items->where('level', 6)->where('side', 'right')->first();
												@endphp

												<div class="col-sm-12">
													<h5 class="col-lg-12 text-center">{{ $tour4_right->level_title }}</h5>
													<div class="separator separator-solid separator-border-3 col-lg-12"></div>
													</br>	
												</div>

												<div class="col-lg-6 col-sm-12">
													<div class="form-group row">
														<label class="col-form-label col-sm-12">Takım 1</label>
														<div class="col-sm-12">
															<select class="form-control select2_team_et" name="tour4_right_team1" 
																@if(!empty($tour4_right->match_id))
																	disabled="disabled"
																@endif
															>
																<option></option>
																@foreach($model_data->eliminaton_tree_teams as $etTeam)

																	<option value="{{ $etTeam->team_id }}"
																		@if(!empty($tour4_right->team1) && $tour4_right->team1->id == $etTeam->team_id)
																			selected="selected" 
																		@endif
																	> {{ $etTeam->team->name }} </option>

																@endforeach
															</select>
														</div>
													</div>
												</div>

												<div class="col-lg-6 col-sm-12">
													<div class="form-group row">
														<label class="col-form-label col-sm-12">Takım 2</label>
														<div class="col-sm-12">
															<select class="form-control select2_team_et" name="tour4_right_team2" 
																@if(!empty($tour4_right->match_id))
																	disabled="disabled"
																@endif
															>
																<option></option>
																@foreach($model_data->eliminaton_tree_teams as $etTeam)

																	<option value="{{ $etTeam->team_id }}"
																		@if(!empty($tour4_right->team2) && $tour4_right->team2->id == $etTeam->team_id)
																			selected="selected" 
																		@endif
																	> {{ $etTeam->team->name }} </option>

																@endforeach
															</select>
														</div>
													</div>
												</div>

												<div class="col-lg-6 col-sm-12">
													<div class="form-group row">
														<label class="col-form-label col-sm-12">Maç Tarihi</label>
														<div class="col-sm-12">
															<div class="input-group input-group-solid datepicker_et" id="tour4_right_match_date" data-target-input="nearest">
													            <input type="text" class="form-control form-control-solid datetimepicker-input " 
														            name="tour4_right_match_date"
														            placeholder="Maç Tarihi" 
														            data-target="#tour4_right_match_date" 
														            @if(!empty($tour4_right->date))
														            	value="{{ Carbon\Carbon::parse($tour4_right->date)->format('d.m.Y') }}"
														            @endif 
																	@if(!empty($tour4_right->match_id))
																		disabled="disabled"
																	@endif
															
													            />
													            <div class="input-group-append" data-target="#tour4_right_match_date" data-toggle="datetimepicker">
													                <span class="input-group-text">
													                    <i class="fas fa-calendar-alt"></i>
													                </span>
													            </div>
													        </div>
														</div>
													</div>
												</div>

												<div class="col-lg-6 col-sm-12">
													<div class="form-group row">
														<label class="col-form-label col-sm-12">Maç Saati</label>
														<div class="col-sm-12">

															<div class="input-group input-group-solid timepicker_et" id="tour4_right_match_time" data-target-input="nearest">
													            <input type="text" class="form-control form-control-solid datetimepicker-input " 
														            name="tour4_right_match_time"
														            placeholder="Maç Saati" 
														            data-target="#tour4_right_match_time"
														            value="{{ $tour4_right->time }}" 
																	@if(!empty($tour4_right->match_id))
																		disabled="disabled"
																	@endif
															
													            />
													            <div class="input-group-append" data-target="#tour4_right_match_time" data-toggle="datetimepicker">
													                <span class="input-group-text">
													                    <i class="fas fa-clock"></i>
													                </span>
													            </div>
													        </div>
														</div>
													</div>
												</div>

													
												@if(empty($tour4_right->match_id))
													<div class="col-lg-12 col-sm-12">
														<div class="form-group">
															<div class="checkbox-inline">
																<label class="checkbox checkbox-lg">
																<input type="checkbox" name="tour4_right_match_create">
																<span></span> Maç Oluştur </label>
															</div>
														</div>
													</div>
												@else
													<div class="col-lg-12 col-sm-12">
														<div class="alert alert-primary text-center" role="alert">
														    Maç detaylarını incelemek için <a href="{{ url('matches/edit/'.$tour4_right->match_id) }}" class="alert-link">buraya</a> tıklayınız.
														</div>
													</div>
												@endif

												<div class="col-sm-12">
													<div class="separator separator-solid separator-border-3 separator-primary"></div>
												</div>

											</div>
											<br />
										@endif

										@if($model_data->tree_build >= 6)
											<div class="row">
												@php
													$tour3_right = $model_data->items->where('level', 5)->where('side', 'right')->first();
												@endphp

												<div class="col-sm-12">
													<h5 class="col-lg-12 text-center">{{ $tour3_right->level_title }}</h5>
													<div class="separator separator-solid separator-border-3 col-lg-12"></div>
													</br>	
												</div>

												<div class="col-lg-6 col-sm-12">
													<div class="form-group row">
														<label class="col-form-label col-sm-12">Takım 1</label>
														<div class="col-sm-12">
															<select class="form-control select2_team_et" name="tour3_right_team1" 
																@if(!empty($tour3_right->match_id))
																	disabled="disabled"
																@endif
															>
																<option></option>
																@foreach($model_data->eliminaton_tree_teams as $etTeam)

																	<option value="{{ $etTeam->team_id }}"
																		@if(!empty($tour3_right->team1) && $tour3_right->team1->id == $etTeam->team_id)
																			selected="selected" 
																		@endif
																	> {{ $etTeam->team->name }} </option>

																@endforeach
															</select>
														</div>
													</div>
												</div>

												<div class="col-lg-6 col-sm-12">
													<div class="form-group row">
														<label class="col-form-label col-sm-12">Takım 2</label>
														<div class="col-sm-12">
															<select class="form-control select2_team_et" name="tour3_right_team2" 
																@if(!empty($tour3_right->match_id))
																	disabled="disabled"
																@endif
															>
																<option></option>
																@foreach($model_data->eliminaton_tree_teams as $etTeam)

																	<option value="{{ $etTeam->team_id }}"
																		@if(!empty($tour3_right->team2) && $tour3_right->team2->id == $etTeam->team_id)
																			selected="selected" 
																		@endif
																	> {{ $etTeam->team->name }} </option>

																@endforeach
															</select>
														</div>
													</div>
												</div>

												<div class="col-lg-6 col-sm-12">
													<div class="form-group row">
														<label class="col-form-label col-sm-12">Maç Tarihi</label>
														<div class="col-sm-12">
															<div class="input-group input-group-solid datepicker_et" id="tour3_right_match_date" data-target-input="nearest">
													            <input type="text" class="form-control form-control-solid datetimepicker-input " 
														            name="tour3_right_match_date"
														            placeholder="Maç Tarihi" 
														            data-target="#tour3_right_match_date" 
														            @if(!empty($tour3_right->date))
														            	value="{{ Carbon\Carbon::parse($tour3_right->date)->format('d.m.Y') }}"
														            @endif 
																	@if(!empty($tour3_right->match_id))
																		disabled="disabled"
																	@endif
															
													            />
													            <div class="input-group-append" data-target="#tour3_right_match_date" data-toggle="datetimepicker">
													                <span class="input-group-text">
													                    <i class="fas fa-calendar-alt"></i>
													                </span>
													            </div>
													        </div>
														</div>
													</div>
												</div>

												<div class="col-lg-6 col-sm-12">
													<div class="form-group row">
														<label class="col-form-label col-sm-12">Maç Saati</label>
														<div class="col-sm-12">

															<div class="input-group input-group-solid timepicker_et" id="tour3_right_match_time" data-target-input="nearest">
													            <input type="text" class="form-control form-control-solid datetimepicker-input " 
														            name="tour3_right_match_time"
														            placeholder="Maç Saati" 
														            data-target="#tour3_right_match_time"
														            value="{{ $tour3_right->time }}" 
																	@if(!empty($tour3_right->match_id))
																		disabled="disabled"
																	@endif
															
													            />
													            <div class="input-group-append" data-target="#tour3_right_match_time" data-toggle="datetimepicker">
													                <span class="input-group-text">
													                    <i class="fas fa-clock"></i>
													                </span>
													            </div>
													        </div>
														</div>
													</div>
												</div>

													
												@if(empty($tour3_right->match_id))
													<div class="col-lg-12 col-sm-12">
														<div class="form-group">
															<div class="checkbox-inline">
																<label class="checkbox checkbox-lg">
																<input type="checkbox" name="tour3_right_match_create">
																<span></span> Maç Oluştur </label>
															</div>
														</div>
													</div>
												@else
													<div class="col-lg-12 col-sm-12">
														<div class="alert alert-primary text-center" role="alert">
														    Maç detaylarını incelemek için <a href="{{ url('matches/edit/'.$tour3_right->match_id) }}" class="alert-link">buraya</a> tıklayınız.
														</div>
													</div>
												@endif

												<div class="col-sm-12">
													<div class="separator separator-solid separator-border-3 separator-primary"></div>
												</div>

											</div>
											<br />
										@endif

										@if($model_data->tree_build >= 5)
											<div class="row">
												@php
													$tour2_right = $model_data->items->where('level', 4)->where('side', 'right')->first();
												@endphp

												<div class="col-sm-12">
													<h5 class="col-lg-12 text-center">{{ $tour2_right->level_title }}</h5>
													<div class="separator separator-solid separator-border-3 col-lg-12"></div>
													</br>	
												</div>

												<div class="col-lg-6 col-sm-12">
													<div class="form-group row">
														<label class="col-form-label col-sm-12">Takım 1</label>
														<div class="col-sm-12">
															<select class="form-control select2_team_et" name="tour2_right_team1" 
																@if(!empty($tour2_right->match_id))
																	disabled="disabled"
																@endif
															>
																<option></option>
																@foreach($model_data->eliminaton_tree_teams as $etTeam)

																	<option value="{{ $etTeam->team_id }}"
																		@if(!empty($tour2_right->team1) && $tour2_right->team1->id == $etTeam->team_id)
																			selected="selected" 
																		@endif
																	> {{ $etTeam->team->name }} </option>

																@endforeach
															</select>
														</div>
													</div>
												</div>

												<div class="col-lg-6 col-sm-12">
													<div class="form-group row">
														<label class="col-form-label col-sm-12">Takım 2</label>
														<div class="col-sm-12">
															<select class="form-control select2_team_et" name="tour2_right_team2" 
																@if(!empty($tour2_right->match_id))
																	disabled="disabled"
																@endif
															>
																<option></option>
																@foreach($model_data->eliminaton_tree_teams as $etTeam)

																	<option value="{{ $etTeam->team_id }}"
																		@if(!empty($tour2_right->team2) && $tour2_right->team2->id == $etTeam->team_id)
																			selected="selected" 
																		@endif
																	> {{ $etTeam->team->name }} </option>

																@endforeach
															</select>
														</div>
													</div>
												</div>

												<div class="col-lg-6 col-sm-12">
													<div class="form-group row">
														<label class="col-form-label col-sm-12">Maç Tarihi</label>
														<div class="col-sm-12">
															<div class="input-group input-group-solid datepicker_et" id="tour2_right_match_date" data-target-input="nearest">
													            <input type="text" class="form-control form-control-solid datetimepicker-input " 
														            name="tour2_right_match_date"
														            placeholder="Maç Tarihi" 
														            data-target="#tour2_right_match_date" 
														            @if(!empty($tour2_right->date))
														            	value="{{ Carbon\Carbon::parse($tour2_right->date)->format('d.m.Y') }}"
														            @endif 
																	@if(!empty($tour2_right->match_id))
																		disabled="disabled"
																	@endif
															
													            />
													            <div class="input-group-append" data-target="#tour2_right_match_date" data-toggle="datetimepicker">
													                <span class="input-group-text">
													                    <i class="fas fa-calendar-alt"></i>
													                </span>
													            </div>
													        </div>
														</div>
													</div>
												</div>

												<div class="col-lg-6 col-sm-12">
													<div class="form-group row">
														<label class="col-form-label col-sm-12">Maç Saati</label>
														<div class="col-sm-12">

															<div class="input-group input-group-solid timepicker_et" id="tour2_right_match_time" data-target-input="nearest">
													            <input type="text" class="form-control form-control-solid datetimepicker-input " 
														            name="tour2_right_match_time"
														            placeholder="Maç Saati" 
														            data-target="#tour2_right_match_time"
														            value="{{ $tour2_right->time }}" 
																	@if(!empty($tour2_right->match_id))
																		disabled="disabled"
																	@endif
															
													            />
													            <div class="input-group-append" data-target="#tour2_right_match_time" data-toggle="datetimepicker">
													                <span class="input-group-text">
													                    <i class="fas fa-clock"></i>
													                </span>
													            </div>
													        </div>
														</div>
													</div>
												</div>

												@if(empty($tour2_right->match_id))
													<div class="col-lg-12 col-sm-12">
														<div class="form-group">
															<div class="checkbox-inline">
																<label class="checkbox checkbox-lg">
																<input type="checkbox" name="tour2_right_match_create">
																<span></span> Maç Oluştur </label>
															</div>
														</div>
													</div>
												@else
													<div class="col-lg-12 col-sm-12">
														<div class="alert alert-primary text-center" role="alert">
														    Maç detaylarını incelemek için <a href="{{ url('matches/edit/'.$tour2_right->match_id) }}" class="alert-link">buraya</a> tıklayınız.
														</div>
													</div>
												@endif

												<div class="col-sm-12">
													<div class="separator separator-solid separator-border-3 separator-primary"></div>
												</div>

											</div>
											<br />
										@endif

										@if($model_data->tree_build >= 4)
											<div class="row">
												@php
													$tour1_right = $model_data->items->where('level', 3)->where('side', 'right')->first();
												@endphp

												<div class="col-sm-12">
													<h5 class="col-lg-12 text-center">{{ $tour1_right->level_title }}</h5>
													<div class="separator separator-solid separator-border-3 col-lg-12"></div>
													</br>	
												</div>

												<div class="col-lg-6 col-sm-12">
													<div class="form-group row">
														<label class="col-form-label col-sm-12">Takım 1</label>
														<div class="col-sm-12">
															<select class="form-control select2_team_et" name="tour1_right_team1" 
																@if(!empty($tour1_right->match_id))
																	disabled="disabled"
																@endif
															>
																<option></option>
																@foreach($model_data->eliminaton_tree_teams as $etTeam)

																	<option value="{{ $etTeam->team_id }}"
																		@if(!empty($tour1_right->team1) && $tour1_right->team1->id == $etTeam->team_id)
																			selected="selected" 
																		@endif
																	> {{ $etTeam->team->name }} </option>

																@endforeach
															</select>
														</div>
													</div>
												</div>

												<div class="col-lg-6 col-sm-12">
													<div class="form-group row">
														<label class="col-form-label col-sm-12">Takım 2</label>
														<div class="col-sm-12">
															<select class="form-control select2_team_et" name="tour1_right_team2" 
																@if(!empty($tour1_right->match_id))
																	disabled="disabled"
																@endif
															>
																<option></option>
																@foreach($model_data->eliminaton_tree_teams as $etTeam)

																	<option value="{{ $etTeam->team_id }}"
																		@if(!empty($tour1_right->team2) && $tour1_right->team2->id == $etTeam->team_id)
																			selected="selected" 
																		@endif
																	> {{ $etTeam->team->name }} </option>

																@endforeach
															</select>
														</div>
													</div>
												</div>

												<div class="col-lg-6 col-sm-12">
													<div class="form-group row">
														<label class="col-form-label col-sm-12">Maç Tarihi</label>
														<div class="col-sm-12">
															<div class="input-group input-group-solid datepicker_et" id="tour1_right_match_date" data-target-input="nearest">
													            <input type="text" class="form-control form-control-solid datetimepicker-input " 
														            name="tour1_right_match_date"
														            placeholder="Maç Tarihi" 
														            data-target="#tour1_right_match_date" 
														            @if(!empty($tour1_right->date))
														            	value="{{ Carbon\Carbon::parse($tour1_right->date)->format('d.m.Y') }}"
														            @endif 
																	@if(!empty($tour1_right->match_id))
																		disabled="disabled"
																	@endif
															
													            />
													            <div class="input-group-append" data-target="#tour1_right_match_date" data-toggle="datetimepicker">
													                <span class="input-group-text">
													                    <i class="fas fa-calendar-alt"></i>
													                </span>
													            </div>
													        </div>
														</div>
													</div>
												</div>

												<div class="col-lg-6 col-sm-12">
													<div class="form-group row">
														<label class="col-form-label col-sm-12">Maç Saati</label>
														<div class="col-sm-12">

															<div class="input-group input-group-solid timepicker_et" id="tour1_right_match_time" data-target-input="nearest">
													            <input type="text" class="form-control form-control-solid datetimepicker-input " 
														            name="tour1_right_match_time"
														            placeholder="Maç Saati" 
														            data-target="#tour1_right_match_time"
														            value="{{ $tour1_right->time }}" 
																	@if(!empty($tour1_right->match_id))
																		disabled="disabled"
																	@endif
															
													            />
													            <div class="input-group-append" data-target="#tour1_right_match_time" data-toggle="datetimepicker">
													                <span class="input-group-text">
													                    <i class="fas fa-clock"></i>
													                </span>
													            </div>
													        </div>
														</div>
													</div>
												</div>

												@if(empty($tour1_right->match_id))
													<div class="col-lg-12 col-sm-12">
														<div class="form-group">
															<div class="checkbox-inline">
																<label class="checkbox checkbox-lg">
																<input type="checkbox" name="tour1_right_match_create">
																<span></span> Maç Oluştur </label>
															</div>
														</div>
													</div>
												@else
													<div class="col-lg-12 col-sm-12">
														<div class="alert alert-primary text-center" role="alert">
														    Maç detaylarını incelemek için <a href="{{ url('matches/edit/'.$tour1_right->match_id) }}" class="alert-link">buraya</a> tıklayınız.
														</div>
													</div>
												@endif

												<div class="col-sm-12">
													<div class="separator separator-solid separator-border-3 separator-primary"></div>
												</div>

											</div>
											<br />
										@endif

										@if($model_data->tree_build >= 3)
											<div class="row">
												@php
													$quarterfinal_right = $model_data->items->where('level', 2)->where('side', 'right')->first();
												@endphp

												<div class="col-sm-12">
													<h5 class="col-lg-12 text-center">Çeyrek Final</h5>
													<div class="separator separator-solid separator-border-3 col-lg-12"></div>
													</br>	
												</div>

												<div class="col-lg-6 col-sm-12">
													<div class="form-group row">
														<label class="col-form-label col-sm-12">Takım 1</label>
														<div class="col-sm-12">
															<select class="form-control select2_team_et" name="quarterfinal_right_team1" 
																@if(!empty($quarterfinal_right->match_id))
																	disabled="disabled"
																@endif
															>
																<option></option>
																@foreach($model_data->eliminaton_tree_teams as $etTeam)

																	<option value="{{ $etTeam->team_id }}"
																		@if(!empty($quarterfinal_right->team1) && $quarterfinal_right->team1->id == $etTeam->team_id)
																			selected="selected" 
																		@endif
																	> {{ $etTeam->team->name }} </option>

																@endforeach
															</select>
														</div>
													</div>
												</div>

												<div class="col-lg-6 col-sm-12">
													<div class="form-group row">
														<label class="col-form-label col-sm-12">Takım 2</label>
														<div class="col-sm-12">
															<select class="form-control select2_team_et" name="quarterfinal_right_team2" 
																@if(!empty($quarterfinal_right->match_id))
																	disabled="disabled"
																@endif
															>
																<option></option>
																@foreach($model_data->eliminaton_tree_teams as $etTeam)

																	<option value="{{ $etTeam->team_id }}"
																		@if(!empty($quarterfinal_right->team2) && $quarterfinal_right->team2->id == $etTeam->team_id)
																			selected="selected" 
																		@endif
																	> {{ $etTeam->team->name }} </option>

																@endforeach
															</select>
														</div>
													</div>
												</div>

												<div class="col-lg-6 col-sm-12">
													<div class="form-group row">
														<label class="col-form-label col-sm-12">Maç Tarihi</label>
														<div class="col-sm-12">
															<div class="input-group input-group-solid datepicker_et" id="quarterfinal_right_match_date" data-target-input="nearest">
													            <input type="text" class="form-control form-control-solid datetimepicker-input " 
														            name="quarterfinal_right_match_date"
														            placeholder="Maç Tarihi" 
														            data-target="#quarterfinal_right_match_date" 
														            @if(!empty($quarterfinal_right->date))
														            	value="{{ Carbon\Carbon::parse($quarterfinal_right->date)->format('d.m.Y') }}"
														            @endif 
																	@if(!empty($quarterfinal_right->match_id))
																		disabled="disabled"
																	@endif
															
													            />
													            <div class="input-group-append" data-target="#quarterfinal_right_match_date" data-toggle="datetimepicker">
													                <span class="input-group-text">
													                    <i class="fas fa-calendar-alt"></i>
													                </span>
													            </div>
													        </div>
														</div>
													</div>
												</div>

												<div class="col-lg-6 col-sm-12">
													<div class="form-group row">
														<label class="col-form-label col-sm-12">Maç Saati</label>
														<div class="col-sm-12">

															<div class="input-group input-group-solid timepicker_et" id="quarterfinal_right_match_time" data-target-input="nearest">
													            <input type="text" class="form-control form-control-solid datetimepicker-input " 
														            name="quarterfinal_right_match_time"
														            placeholder="Maç Saati" 
														            data-target="#quarterfinal_right_match_time"
														            value="{{ $quarterfinal_right->time }}" 
																	@if(!empty($quarterfinal_right->match_id))
																		disabled="disabled"
																	@endif
															
													            />
													            <div class="input-group-append" data-target="#quarterfinal_right_match_time" data-toggle="datetimepicker">
													                <span class="input-group-text">
													                    <i class="fas fa-clock"></i>
													                </span>
													            </div>
													        </div>
														</div>
													</div>
												</div>

												@if(empty($quarterfinal_right->match_id))
													<div class="col-lg-12 col-sm-12">
														<div class="form-group">
															<div class="checkbox-inline">
																<label class="checkbox checkbox-lg">
																<input type="checkbox" name="quarterfinal_right_match_create">
																<span></span> Maç Oluştur </label>
															</div>
														</div>
													</div>
												@else
													<div class="col-lg-12 col-sm-12">
														<div class="alert alert-primary text-center" role="alert">
														    Maç detaylarını incelemek için <a href="{{ url('matches/edit/'.$quarterfinal_right->match_id) }}" class="alert-link">buraya</a> tıklayınız.
														</div>
													</div>
												@endif

												<div class="col-sm-12">
													<div class="separator separator-solid separator-border-3 separator-primary"></div>
												</div>

											</div>
											<br />
										@endif

										@if($model_data->tree_build >= 2)
											<div class="row">
												@php
													$semifinal_right = $model_data->items->where('level', 1)->where('side', 'right')->first();
												@endphp

												<div class="col-sm-12">
													<h5 class="col-lg-12 text-center">Yarı Final</h5>
													<div class="separator separator-solid separator-border-3 col-lg-12"></div>
													</br>	
												</div>

												<div class="col-lg-6 col-sm-12">
													<div class="form-group row">
														<label class="col-form-label col-sm-12">Takım 1</label>
														<div class="col-sm-12">
															<select class="form-control select2_team_et" name="semifinal_right_team1" 
																@if(!empty($semifinal_right->match_id))
																	disabled="disabled"
																@endif
															>
																<option></option>
																@foreach($model_data->eliminaton_tree_teams as $etTeam)

																	<option value="{{ $etTeam->team_id }}"
																		@if(!empty($semifinal_right->team1) && $semifinal_right->team1->id == $etTeam->team_id)
																			selected="selected" 
																		@endif
																	> {{ $etTeam->team->name }} </option>

																@endforeach
															</select>
														</div>
													</div>
												</div>

												<div class="col-lg-6 col-sm-12">
													<div class="form-group row">
														<label class="col-form-label col-sm-12">Takım 2</label>
														<div class="col-sm-12">
															<select class="form-control select2_team_et" name="semifinal_right_team2" 
																@if(!empty($semifinal_right->match_id))
																	disabled="disabled"
																@endif
															>
																<option></option>
																@foreach($model_data->eliminaton_tree_teams as $etTeam)

																	<option value="{{ $etTeam->team_id }}"
																		@if(!empty($semifinal_right->team2) && $semifinal_right->team2->id == $etTeam->team_id)
																			selected="selected" 
																		@endif
																	> {{ $etTeam->team->name }} </option>

																@endforeach
															</select>
														</div>
													</div>
												</div>

												<div class="col-lg-6 col-sm-12">
													<div class="form-group row">
														<label class="col-form-label col-sm-12">Maç Tarihi</label>
														<div class="col-sm-12">
															<div class="input-group input-group-solid datepicker_et" id="semifinal_right_match_date" data-target-input="nearest">
													            <input type="text" class="form-control form-control-solid datetimepicker-input " 
														            name="semifinal_right_match_date"
														            placeholder="Maç Tarihi" 
														            data-target="#semifinal_right_match_date" 
														            @if(!empty($semifinal_right->date))
														            	value="{{ Carbon\Carbon::parse($semifinal_right->date)->format('d.m.Y') }}"
														            @endif 
																	@if(!empty($semifinal_right->match_id))
																		disabled="disabled"
																	@endif
															
													            />
													            <div class="input-group-append" data-target="#semifinal_right_match_date" data-toggle="datetimepicker">
													                <span class="input-group-text">
													                    <i class="fas fa-calendar-alt"></i>
													                </span>
													            </div>
													        </div>
														</div>
													</div>
												</div>

												<div class="col-lg-6 col-sm-12">
													<div class="form-group row">
														<label class="col-form-label col-sm-12">Maç Saati</label>
														<div class="col-sm-12">

															<div class="input-group input-group-solid timepicker_et" id="semifinal_right_match_time" data-target-input="nearest">
													            <input type="text" class="form-control form-control-solid datetimepicker-input " 
														            name="semifinal_right_match_time"
														            placeholder="Maç Saati" 
														            data-target="#semifinal_right_match_time"
														            value="{{ $semifinal_right->time }}" 
																	@if(!empty($semifinal_right->match_id))
																		disabled="disabled"
																	@endif
															
													            />
													            <div class="input-group-append" data-target="#semifinal_right_match_time" data-toggle="datetimepicker">
													                <span class="input-group-text">
													                    <i class="fas fa-clock"></i>
													                </span>
													            </div>
													        </div>
														</div>
													</div>
												</div>

												@if(empty($semifinal_right->match_id))
													<div class="col-lg-12 col-sm-12">
														<div class="form-group">
															<div class="checkbox-inline">
																<label class="checkbox checkbox-lg">
																<input type="checkbox" name="semifinal_right_match_create">
																<span></span> Maç Oluştur </label>
															</div>
														</div>
													</div>
												@else
													<div class="col-lg-12 col-sm-12">
														<div class="alert alert-primary text-center" role="alert">
														    Maç detaylarını incelemek için <a href="{{ url('matches/edit/'.$semifinal_right->match_id) }}" class="alert-link">buraya</a> tıklayınız.
														</div>
													</div>
												@endif

												<div class="col-sm-12">
													<div class="separator separator-solid separator-border-3 separator-primary"></div>
												</div>

											</div>
										@endif


									</div>
								</div>
							</div>

						</div>
						
					</div>
				@endif
				
				<div class="row">
					<div class="col-sm-12">

						<div class="card">
							<div class="card-header">
								<div class="card-title" data-toggle="collapse">
								<i class="fas fa-trophy"></i>FİNAL</div>
							</div>
							<div class="collapse show">
								<div class="card-body">

									<div class="row">
										@php
											$final_left = $model_data->items->where('level', 0)->first();
										@endphp

										<div class="col-sm-12">
											<h5 class="col-lg-12 text-center">Final</h5>
											<div class="separator separator-solid separator-border-3 col-lg-12"></div>
											</br>	
										</div>

										<div class="col-lg-5 col-sm-12">
											<div class="form-group row">
												<label class="col-form-label text-right col-lg-3 col-sm-12">Takım 1</label>
												<div class="col-lg-8 col-sm-12">
													<select class="form-control select2_team_et" name="final_team1" 
														@if(!empty($final_left->match_id))
															disabled="disabled"
														@endif
													>
														<option></option>
														@foreach($model_data->eliminaton_tree_teams as $etTeam)

															<option value="{{ $etTeam->team_id }}"
																@if(!empty($final_left->team1) && $final_left->team1->id == $etTeam->team_id)
																	selected="selected" 
																@endif
															> {{ $etTeam->team->name }} </option>

														@endforeach
													</select>
												</div>
											</div>
										</div>

										<div class="col-lg-5 col-sm-12">
											<div class="form-group row">
												<label class="col-form-label text-right col-lg-3 col-sm-12">Takım 2</label>
												<div class="col-lg-8 col-sm-12">
													<select class="form-control select2_team_et" name="final_team2" 
														@if(!empty($final_left->match_id))
															disabled="disabled"
														@endif
													>
														<option></option>
														@foreach($model_data->eliminaton_tree_teams as $etTeam)

															<option value="{{ $etTeam->team_id }}"
																@if(!empty($final_left->team2) && $final_left->team2->id == $etTeam->team_id)
																	selected="selected" 
																@endif
															> {{ $etTeam->team->name }} </option>

														@endforeach
													</select>
												</div>
											</div>
										</div>

										<div class="col-lg-5 col-sm-12">
											<div class="form-group row">
												<label class="col-form-label text-right col-lg-3 col-sm-12">Maç Tarihi</label>
												<div class="col-lg-8 col-sm-12">
													<div class="input-group input-group-solid datepicker_et" id="final_match_date" data-target-input="nearest">
											            <input type="text" class="form-control form-control-solid datetimepicker-input " 
												            name="final_match_date"
												            placeholder="Maç Tarihi" 
												            data-target="#final_match_date" 
												            @if(!empty($final_left->date))
												            	value="{{ Carbon\Carbon::parse($final_left->date)->format('d.m.Y') }}"
												            @endif 
															@if(!empty($final_left->match_id))
																disabled="disabled"
															@endif
													
											            />
											            <div class="input-group-append" data-target="#final_match_date" data-toggle="datetimepicker">
											                <span class="input-group-text">
											                    <i class="fas fa-calendar-alt"></i>
											                </span>
											            </div>
											        </div>
												</div>
											</div>
										</div>

										<div class="col-lg-5 col-sm-12">
											<div class="form-group row">
												<label class="col-form-label text-right col-lg-3 col-sm-12">Maç Saati</label>
												<div class="col-lg-8 col-sm-12">

													<div class="input-group input-group-solid timepicker_et" id="final_match_time" data-target-input="nearest">
											            <input type="text" class="form-control form-control-solid datetimepicker-input " 
												            name="final_match_time"
												            placeholder="Maç Saati" 
												            data-target="#final_match_time"
												            value="{{ $final_left->time }}" 
															@if(!empty($final_left->match_id))
																disabled="disabled"
															@endif
													
											            />
											            <div class="input-group-append" data-target="#final_match_time" data-toggle="datetimepicker">
											                <span class="input-group-text">
											                    <i class="fas fa-clock"></i>
											                </span>
											            </div>
											        </div>
												</div>
											</div>
										</div>

										@if(empty($final_left->match_id))
											<div class="col-lg-2 col-sm-12">
												<div class="form-group">
													<div class="col-sm-12">
														<div class="checkbox-inline">
															<label class="checkbox checkbox-lg">
															<input type="checkbox" name="final_match_create">
															<span></span> Maç Oluştur </label>
														</div>
													</div>
												</div>
											</div>
										@else
											<div class="col-lg-12 col-sm-12">
												<div class="alert alert-primary text-center" role="alert">
												    Maç detaylarını incelemek için <a href="{{ url('matches/edit/'.$final_left->match_id) }}" class="alert-link">buraya</a> tıklayınız.
												</div>
											</div>
										@endif

									</div>

								</div>
							</div>
						</div>

					</div>
				</div>

				
			</div>

			<div class="row" style="padding: 2rem 0 0 0; border-top: 1px solid #EBEDF3;">
	            <div class="col-lg-3"></div>
	            <div class="col-lg-6">
	                <button type="submit" class="btn btn-success mr-2">Kaydet</button>
	                <button type="button" class="btn btn-secondary" onclick="history.back()">İptal</button>
	            </div>
	        </div>

		</form>
		<!--end: Datatable-->
	</div>
</div>
<!--end::Card-->