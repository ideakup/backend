<!--begin::Entry-->
<div class="d-flex flex-column-fluid">

	<!--begin::Container-->
	<div class="container-fluid">

		<!--[html-partial:begin:{"id":"demo1/dist/inc/view/demos/pages/index","page":"index"}]/-->

		<!--[html-partial:begin:{"id":"demo1/dist/inc/view/partials/content/dashboards/demo1","page":"index"}]/-->
		<style type="text/css">
			.tdt td {
				white-space: nowrap;
			}
			.form-control[readonly] {
			    background-color: #F3F6F9;
			}
		</style>
		<!--begin::Dashboard-->
		@if (Request::segment(2) == '')

			<!--begin::Card-->
			<div class="card card-custom">
				<div class="card-header">
					<div class="card-title">
						<span class="card-icon">
							<i class="fas fa-city"></i>
						</span>
						<h3 class="card-label">Şehirler</h3>
					</div>
					<div class="card-toolbar">
						<!--begin::Button-->
						@if (Request::segment(2) == '' && Auth::user()->hasPermissionTo('add_province'))
							<a href="{{ url()->current() }}/add" class="btn btn-primary font-weight-bolder"><i class="fas fa-plus"></i> Şehir Ekle</a>
						@endif
						<!--end::Button-->
					</div>
				</div>
				<div class="card-body">
					<!--begin: Datatable-->
					<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
					<table class="table table-bordered table-hover table-checkable tdt" id="kt_datatable_provinces" style="margin-top: 13px !important">
					</table>
					<!--end: Datatable-->
				</div>
			</div>
			<!--end::Card-->
		@else
			@include('form_element._addForm')
		@endif
		<!--end::Dashboard-->

		<!--[html-partial:end:{"id":"demo1/dist/inc/view/partials/content/dashboards/demo1","page":"index"}]/-->

		<!--[html-partial:end:{"id":"demo1/dist/inc/view/demos/pages/index","page":"index"}]/-->
	</div>

	<!--end::Container-->
</div>

<!--end::Entry-->