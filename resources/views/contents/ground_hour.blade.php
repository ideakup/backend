<!--begin::Card-->
<div class="card card-custom">
	<div class="card-header">
		<div class="card-title">
			<span class="card-icon">
				<i class="fas fa-clock"></i>
			</span>
			<h3 class="card-label">{{ $model_data->name }} Saatleri</h3>
		</div>
		<div class="card-toolbar">
		</div>
	</div>
	<div class="card-body">

		<!--begin: Search Form-->
		<form class="mb-5" id='searchFormm'>
			<div class="row mb-6">
				<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
				<input type="hidden" name="ground_id" id="ground_id" value="{{ Request::segment(3) }}">
				@php
					//$en = Carbon\CarbonImmutable::now()->locale('en_US');
					$carbonSelect = Carbon\Carbon::now();
				@endphp

				<div class="col-lg-4 mb-5">
					<label> Hafta Seçiniz... </label>
					<select class="form-control" name="select2_week_start_date" id="select2_week_start_date">
						@for ($i = 0; $i < 20; $i++)
						    <option value="{{ $carbonSelect->startOfWeek()->format('d.m.Y') }}">
						    	{{ $carbonSelect->startOfWeek()->format('d.m.Y').' - '.$carbonSelect->endOfWeek()->format('d.m.Y') }}
						    </option>
						    @php
						    	$carbonSelect->addWeek();
						    @endphp
						@endfor
					</select>
				</div>

				<div class="col-lg-8 text-right mb-5">
					<label>&#160;</label>
					<span style="display: block;">
						<button class="btn btn-primary btn-primary--icon btn-sm" id="kt_search">
							<span>
								<i class="fas fa-search"></i>
								<span>Search</span>
							</span>
						</button>&#160;&#160;
						<button class="btn btn-secondary btn-secondary--icon btn-sm" id="kt_reset">
							<span>
								<i class="far fa-times-circle"></i>
								<span>Reset</span>
							</span>
						</button>
					</span>
				</div>

				<!--begin: Datatable-->
				

			</div>
		</form>
		@php
			//$en = Carbon\CarbonImmutable::now()->locale('en_US');
			$carbon = Carbon\Carbon::now()->startOfWeek();
			//dump($carbon);
			//
			//dump($carbon->startOfWeek()->format('d.m.Y').' - '.$carbon->endOfWeek()->format('d.m.Y'));
		@endphp
		<table class="table table-bordered table-hover table-checkable tdt" id="kt_datatable_ground_hour" style="margin-top: 13px !important">
			<thead>
				<tr>
					<th> </th>
					<th> </th>
					<th> </th>
					<th> </th>
					<th> </th>
					<th> </th>
					<th> </th>
					<th> </th>
					<th> </th>
				</tr>
			</thead>
		</table>
		<!--end: Datatable-->
	</div>
</div>
<!--end::Card-->