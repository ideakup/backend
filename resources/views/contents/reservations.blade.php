<div class="d-flex flex-column-fluid">
	<div class="container-fluid">
		<style type="text/css">
			.tdt td {
				white-space: nowrap;
			}

			.dataTables_wrapper table.dataTable.collapsed > tbody > tr[role="row"] > td:first-child {
			    padding-left: 20px !important;
			}

			.dataTables_wrapper table.dataTable.dtr-inline.collapsed > tbody > tr[role="row"] > td:first-child:before {
				margin-top: -5px;
			}
			
			.dataTables_wrapper .dataTable td{
				padding: 3px 5px !important;
			}

			.form-control[readonly] {
			    background-color: #F3F6F9;
			}
		</style>
		<!--begin::Dashboard-->
		@if (Request::segment(2) == '')

			<!--begin::Card-->
			<div class="card card-custom">
				<div class="card-header">
					<div class="card-title">
						<span class="card-icon">
							<i class="fas fa-users text-primary"></i>
						</span>
						<h3 class="card-label">Rezervasyonlar</h3>
					</div>
					<div class="card-toolbar">
						<!--begin::Button-->
						@if (Request::segment(2) == '' && Auth::user()->hasPermissionTo('add_reservation'))
							<a href="{{ url()->current() }}/add" class="btn btn-primary font-weight-bolder"><i class="fas fa-calendar-check"></i> Rezervasyonlar Ekle</a>
						@endif
						<!--end::Button-->
					</div>
				</div>
				<div class="card-body">
					<!--begin: Search Form-->
					<form class="mb-5" id='searchFormm'>

						<div class="row mb-6">
						
							<div class="col-lg-3 mb-5">
								<label> Şehir </label>
								<select class="form-control datatable-input" id="select2_provinceg_search" data-col-index="1">
									<option></option>
									@php
										//RoleProvince
										$roleProvince = collect(Auth::user()->roleprovince)->pluck('province_id');
								        if($roleProvince->count() > 0){
								            $provinces = App\Province::whereIn('id', $roleProvince)->get();
								        }else{
								            $provinces = App\Province::all();
								        }
									@endphp
									@foreach ($provinces as $province)
										<option value="{{ $province->id }}"> {{ $province->name }} </option>
									@endforeach
								</select>
							</div>

							<div class="col-lg-3 mb-5">
								<label> Halı Saha </label>
								<select class="form-control datatable-input" id="select2_ground_search" data-col-index="2">
									<option></option>
								</select>
							</div>

							<div class="col-lg-3 mb-5">
								<label> Durum </label>
								<select class="form-control datatable-input" id="select2_status_search" data-col-index="7">
									<option></option>
									<option value="open"> Açık </option>
									<option value="confirmed"> Onaylandı </option>
									<option value="suspended"> Askıya Alındı </option>
								</select>
							</div>

							<div class="col-lg-3 text-right mb-5">
								<label>&#160;</label>
								<span style="display: block;">
									<button class="btn btn-primary btn-primary--icon btn-sm" id="kt_search">
										<span>
											<i class="fas fa-search"></i>
											<span>Search</span>
										</span>
									</button>&#160;&#160;
									<button class="btn btn-secondary btn-secondary--icon btn-sm" id="kt_reset">
										<span>
											<i class="far fa-times-circle"></i>
											<span>Reset</span>
										</span>
									</button>
								</span>
							</div>
						</div>
						
					</form>
					<!--begin: Datatable-->
					<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
					<table class="table table-bordered table-hover table-checkable tdt" id="kt_datatable_reservations" style="margin-top: 13px !important">
					</table>
					<!--end: Datatable-->
				</div>
			</div>
			<!--end::Card-->
		@else
			@include('form_element._addForm')
		@endif
		<!--end::Dashboard-->

		<!--[html-partial:end:{"id":"demo1/dist/inc/view/partials/content/dashboards/demo1","page":"index"}]/-->

		<!--[html-partial:end:{"id":"demo1/dist/inc/view/demos/pages/index","page":"index"}]/-->
	</div>
	<!--end::Container-->
</div>

<!--end::Entry-->