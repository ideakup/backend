<!--begin::Entry-->
<div class="d-flex flex-column-fluid">

	<!--begin::Container-->
	<div class="container-fluid">

		<!--[html-partial:begin:{"id":"demo1/dist/inc/view/demos/pages/index","page":"index"}]/-->

		<!--[html-partial:begin:{"id":"demo1/dist/inc/view/partials/content/dashboards/demo1","page":"index"}]/-->
		<style type="text/css">
			.tdt td {
				white-space: nowrap;
			}
			.form-control[readonly] {
			    background-color: #F3F6F9;
			}
		</style>
		<!--begin::Dashboard-->
		@if (Request::segment(2) == '')

			<!--begin::Card-->
			<div class="card card-custom">
				<div class="card-header">
					<div class="card-title">
						<span class="card-icon">
							<i class="fas fa-balance-scale"></i>
						</span>
						<h3 class="card-label">Cezalar</h3>
					</div>
					<div class="card-toolbar">
						<!--begin::Button-->
						@if (Request::segment(2) == '' && Auth::user()->hasPermissionTo('add_position'))
							<a href="{{ url('/player_penalties/add') }}" class="btn btn-primary font-weight-bolder"><i class="fas fa-plus"></i> Oyuncu Cezası Ekle </a>
						@endif
						<!--end::Button-->
						&nbsp;&nbsp;
						<!--begin::Button-->
						@if (Request::segment(2) == '' && Auth::user()->hasPermissionTo('add_position'))
							<a href="{{ url('/team_penalties/add') }}" class="btn btn-primary font-weight-bolder"><i class="fas fa-plus"></i> Takım Cezası Ekle </a>
						@endif
						<!--end::Button-->
					</div>
				</div>
				<div class="card-body">
					<!--begin: Search Form-->
					<form class="mb-5" id='searchFormm'>
						<div class="row mb-6">
							<div class="col-lg-3 mb-5">
								<label> Sezon </label>
								<select class="form-control datatable-input" id="select2_season_search" data-col-index="2">
									<option></option>

									@php
										//RoleProvince
										$roleProvince = collect(Auth::user()->roleprovince)->pluck('province_id');
								        if($roleProvince->count() > 0){

									        $seasons = App\Season::
									        select( 'seasons.id',
									                'seasons.league_id',
									                'seasons.name',
									                'seasons.polymorphic_ctype_id',
									                'seasons.year',
									                'seasons.start_date',
									                'seasons.end_date',
									                'seasons.active',
									                DB::raw("count(leagues_provinces.province_id) as count")
									        )->
									        leftJoin('leagues', 'leagues.id', '=', 'seasons.league_id')->
									        leftJoin('leagues_provinces', 'leagues.id', '=', 'leagues_provinces.league_id')->
											whereIn('leagues_provinces.province_id', $roleProvince)->groupBy('seasons.id')->get();

								        }else{
								            $seasons = App\Season::where('active', true)->orderBy('year', 'desc')->get();
								        }
									@endphp

									@foreach ($seasons as $season)
										<option value="{{ $season->id }}"> {{ $season->year }} {{ $season->league->name }} {{ $season->name }} </option>
									@endforeach
								</select>
							</div>
							<div class="col-lg-9 text-right mb-5">
								<label>&#160;</label>
								<span style="display: block;">
									<button class="btn btn-primary btn-primary--icon btn-sm" id="kt_search">
										<span>
											<i class="fas fa-search"></i>
											<span>Search</span>
										</span>
									</button>&#160;&#160;
									<button class="btn btn-secondary btn-secondary--icon btn-sm" id="kt_reset">
										<span>
											<i class="far fa-times-circle"></i>
											<span>Reset</span>
										</span>
									</button>
								</span>
							</div>
						</div>
					</form>
					<!--begin: Datatable-->
					<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
					<table class="table table-bordered table-hover table-checkable tdt" id="kt_datatable_penalties" style="margin-top: 13px !important">
					</table>
					<!--end: Datatable-->
				</div>
			</div>
			<!--end::Card-->
		@else
			@include('form_element._addForm')
		@endif
		<!--end::Dashboard-->

		<!--[html-partial:end:{"id":"demo1/dist/inc/view/partials/content/dashboards/demo1","page":"index"}]/-->

		<!--[html-partial:end:{"id":"demo1/dist/inc/view/demos/pages/index","page":"index"}]/-->
	</div>

	<!--end::Container-->
</div>

<!--end::Entry-->