<!--begin::Entry-->
<div class="d-flex flex-column-fluid">

	<!--begin::Container-->
	<div class="container-fluid">

		<!--[html-partial:begin:{"id":"demo1/dist/inc/view/demos/pages/index","page":"index"}]/-->

		<!--[html-partial:begin:{"id":"demo1/dist/inc/view/partials/content/dashboards/demo1","page":"index"}]/-->
		<style type="text/css">
			.tdt td {
				white-space: nowrap;
			}

			#kt_datatable_teams .dataTables_wrapper table.dataTable.collapsed > tbody > tr[role="row"] > td:first-child {
			    padding-left: 20px !important;
			}

			#kt_datatable_teams .dataTables_wrapper table.dataTable.dtr-inline.collapsed > tbody > tr[role="row"] > td:first-child:before {
				margin-top: -5px;
			}
			
			#kt_datatable_teams .dataTables_wrapper .dataTable td{
				padding: 3px 5px !important;
			}

			.form-control[readonly] {
			    background-color: #F3F6F9;
			}
			
			.dataTable td .select2-container--default .select2-selection--single .select2-selection__rendered{
			    padding: .3rem;
    			line-height: 0.5;
    		}
		</style>
		<!--begin::Dashboard-->
		@if (Request::segment(2) == '')

			<!--begin::Card-->
			<div class="card card-custom">
				<div class="card-header">
					<div class="card-title">
						<span class="card-icon">
							<i class="fas fa-users text-primary"></i>
						</span>
						<h3 class="card-label">Takımlar</h3>
					</div>
					<div class="card-toolbar">
						<!--begin::Button-->
						@if (Request::segment(2) == '' && Auth::user()->hasPermissionTo('add_team'))
							<a href="{{ url()->current() }}/add" class="btn btn-primary font-weight-bolder"><i class="fas fa-plus"></i> Takım Ekle</a>
						@endif
						<!--end::Button-->
					</div>
				</div>
				<div class="card-body">
					<!--begin: Search Form-->
					<form class="mb-5" id='searchFormm'>
						<div class="row mb-6">

							<div class="col-lg-3 mb-5">
								<label> Ad </label>
								<input type="text" class="form-control datatable-input" data-col-index="1" />
							</div>

							<div class="col-lg-3 mb-5">
								<label> Şehir </label>
								<select class="form-control datatable-input" id="select2_province_search" data-col-index="2">
									<option></option>
									@php
										//RoleProvince
										$roleProvince = collect(Auth::user()->roleprovince)->pluck('province_id');
								        if($roleProvince->count() > 0){
								            $provinces = App\Province::whereIn('id', $roleProvince)->get();
								        }else{
								            $provinces = App\Province::all();
								        }
									@endphp
									@foreach ($provinces as $province)
										<option value="{{ $province->id }}"> {{ $province->name }} </option>
									@endforeach
								</select>
							</div>

							<div class="col-lg-3 mb-5">
								<label> İlçe </label>
								<select class="form-control datatable-input" id="select2_district_search" multiple="multiple" data-col-index="3">
								</select>
							</div>


							<div class="col-lg-3 mb-5">
								<label> Kaptan </label>
								<select class="form-control datatable-input" id="select2_captain_search" data-col-index="4">
									<option></option>
								</select>
							</div>

							<div class="col-lg-3 mb-5">
								<label> Telefon </label>
								<input type="number" class="form-control datatable-input" min="0" data-col-index="5" />
							</div>

							<div class="col-lg-3 mb-5">
								<label> Potansiyel </label>
								<select class="form-control" id="select2_potential_search">
									<option></option>
									@foreach (App\TeamPotential::all() as $potential)
										<option value="{{ $potential->id }}"> {{ $potential->name }} </option>
									@endforeach
								</select>
							</div>

							<div class="col-lg-3 mb-5">
								<label> Memnuniyet </label>
								<select class="form-control" id="select2_satisfaction_search">
									<option></option>
									@foreach (App\TeamSatisfaction::all() as $satisfaction)
										<option value="{{ $satisfaction->id }}"> {{ $satisfaction->name }} </option>
									@endforeach
								</select>
							</div>

							<div class="col-lg-3 mb-5">
								<label> Jogo Memnuniyet </label>
								<select class="form-control" id="select2_satisfaction_jogo_search">
									<option></option>
									@foreach (App\TeamSatisfactionJogo::all() as $satisfaction_jogo)
										<option value="{{ $satisfaction_jogo->id }}"> {{ $satisfaction_jogo->name }} </option>
									@endforeach
								</select>
							</div>

							<input type="hidden" class="datatable-input" id="potential" value="" data-col-index="7">

							<div class="col-lg-3 mb-5">
								<label> Fesih Durumu </label>
								<select class="form-control datatable-input" id="select2_active_search" data-col-index="8">
									<option></option>
									<option value="active"> Aktif </option>
									<option value="termination"> Fesih İsteği </option>
									<option value="passive"> Feshedildi </option>
								</select>
							</div>

							<div class="col-lg-3 mb-5">
								<label> Kadro Sayısı </label>
								<div class="row align-items-center">
									<div class="col-6">
										<input type="number" id="ksmin" class="form-control datatable-input" placeholder="Min" min="0" />
									</div>
									<div class="col-6">
										<input type="number" id="ksmax" class="form-control datatable-input" placeholder="Max" min="0" />
									</div>
								</div>
							</div>

							<input type="hidden" class="datatable-input" id="ks" value="" data-col-index="10">

							<div class="col-lg-6 mb-5">
							</div>


							<div class="col-lg-9">
								<div class="row mb-6">
									<div class="col-lg-4 mb-5">
										<label> Maç Sayısı </label>
										<div class="row align-items-center">
											<div class="col-6">
												<input type="number" id="msmin" class="form-control datatable-input" placeholder="Min" min="0" />
											</div>
											<div class="col-6">
												<input type="number" id="msmax" class="form-control datatable-input" placeholder="Max" min="0" />
											</div>
										</div>
									</div>

									<div class="col-lg-4 mb-5">
										<label>Tarih Aralığı</label>
										<div class="row">
											<div class="col">
												<div class="input-group date" id="daterange_start_search" data-target-input="nearest">
													<input type="text" class="form-control datetimepicker-input" placeholder="Başlangıç Tarihi" data-target="#daterange_start_search" />
													<div class="input-group-append" data-target="#daterange_start_search" data-toggle="datetimepicker">
														<span class="input-group-text">
															<i class="ki ki-calendar"></i>
														</span>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="col-lg-4 mb-5">
										<label>&#160;</label>
										<div class="row">
											<div class="col">
												<div class="input-group date" id="daterange_end_search" data-target-input="nearest">
													<input type="text" class="form-control datetimepicker-input" placeholder="Bitiş Tarihi" data-target="#daterange_end_search" />
													<div class="input-group-append" data-target="#daterange_end_search" data-toggle="datetimepicker">
														<span class="input-group-text">
															<i class="ki ki-calendar"></i>
														</span>
													</div>
												</div>
											</div>
										</div>
										<!--<input type="hidden" class="datatable-input" name="daterange_dates_search" id="daterange_dates_search" data-col-index="4">-->
										<input type="hidden" class="datatable-input" id="ms" value="" data-col-index="9">
									</div>
								</div>
							</div>





							<div class="col-lg-3 text-right mb-5">
								<label>&#160;</label>
								<span style="display: block;">
									<button class="btn btn-primary btn-primary--icon btn-sm" id="kt_search">
										<span>
											<i class="fas fa-search"></i>
											<span>Search</span>
										</span>
									</button>&#160;&#160;
									<button class="btn btn-secondary btn-secondary--icon btn-sm" id="kt_reset">
										<span>
											<i class="far fa-times-circle"></i>
											<span>Reset</span>
										</span>
									</button>
								</span>
							</div>
						</div>
					</form>
					<!--begin: Datatable-->
					<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
					<table class="table table-bordered table-hover table-checkable tdt" id="kt_datatable_teams" style="margin-top: 13px !important">
					</table>
					<!--end: Datatable-->
				</div>
			</div>

			<script type="text/javascript">
		
				var _potential = new Array();
				@foreach (App\TeamPotential::all() as $potential)
					_potential[{{ $potential->id }}] = '{{ $potential->name }}';
				@endforeach;

				var _satisfaction = new Array();
				@foreach (App\TeamSatisfaction::all() as $satisfaction)
					_satisfaction[{{ $satisfaction->id }}] = '{{ $satisfaction->name }}';
				@endforeach;

				var _satisfaction_jogo = new Array();
				@foreach (App\TeamSatisfactionJogo::all() as $satisfaction_jogo)
					_satisfaction_jogo[{{ $satisfaction_jogo->id }}] = '{{ $satisfaction_jogo->name }}';
				@endforeach;

			</script>
			<!--end::Card-->
		
		@elseif (Request::segment(2) == 'addinsideplayer')
			@include('contents.player_list')
		@else
			@include('form_element._addForm')
		@endif
		<!--end::Dashboard-->

		<!--[html-partial:end:{"id":"demo1/dist/inc/view/partials/content/dashboards/demo1","page":"index"}]/-->

		<!--[html-partial:end:{"id":"demo1/dist/inc/view/demos/pages/index","page":"index"}]/-->
	</div>
	<!--end::Container-->
</div>

<!--end::Entry-->