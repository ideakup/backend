<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=display-width, initial-scale=1.0, maximum-scale=1.0,">
    <meta name="description" content="Şifre Yenileme Başarılı">
    <title>Şifre Yenileme Başarılı</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Signika:400,300,600,700" rel="stylesheet" type="text/css">
    <style type="text/css">
	    /* Reset */
	    body {
	        margin: 0px;
	        padding: 0px;
	    }

	    span.preheader {
	        display: none !important;
	    }

	    /* Responsive */
	    @media only screen and (max-width:640px) {

	        /* Tables
	        parameters: width, alignment, padding */
	        table[class=scale] {
	            width: 100% !important;
	        }

	        table[class=scale2] {
	            width: 100% !important;
	            text-align: center;
	        }

	        table[class=scale-center-both] {
	            width: 100% !important;
	            text-align: center !important;
	            padding-left: 20px !important;
	            padding-right: 20px !important;
	        }

	        table[class=scale-center-bottom] {
	            width: 100% !important;
	            text-align: center !important;
	            padding-bottom: 12px !important;
	        }

	        table[class=margin-center] {
	            margin: auto !important;
	        }

	        table[class=hide] {
	            display: none !important;
	        }

	        /* Td */
	        td[class=scale-center-both] {
	            width: 100% !important;
	            text-align: center !important;
	            padding-left: 20px !important;
	            padding-right: 20px !important;
	        }

	        td[class=scale-center-both-image] {
	            width: 100% !important;
	            text-align: center !important;
	        }

	        td[class=scale-center-left] {
	            width: 100% !important;
	            text-align: right !important;
	        }

	        td[class=scale-center-bottom] {
	            width: 100% !important;
	            text-align: center !important;
	            padding-bottom: 24px !important;
	        }

	        td[class=scale-center-bottom-both] {
	            width: 100% !important;
	            text-align: center !important;
	            padding-bottom: 12px !important;
	            padding-left: 20px !important;
	            padding-right: 20px !important;
	        }

	        td[class=bottom12] {
	            padding-bottom: 12px !important;
	        }

	        td[class=bottom19] {
	            padding-bottom: 19px !important;
	        }

	        td[class=bottom21] {
	            padding-bottom: 21px !important;
	        }

	        td[class=height4] {
	            height: 4px !important;
	            font-size: 1px !important;
	        }

	        /* Img */
	        img[class="scale"] {
	            width: 90% !important;
	        }

	        img[class="reset"] {
	            width: 100% !important;
	        }
	    }
    </style>
</head>

<body bgcolor="#ffffff" style="background-color:#ffffff">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" data-id="40">
        <tbody>
            <tr>
                <td class="arkaplanrenk">
                    <table width="620" border="0" cellspacing="0" cellpadding="0" align="center">
                        <tbody>
                            <tr>
                                <td height="20">&nbsp;</td>
                            </tr>
                            <tr>
                                <td height="42" style="font-family:'source_sans_probold', Helvetica, Arial, sans-serif;font-size:12px;text-align:right;color:#444444">
                                    Eğer bu e-postayı düzgün göremiyorsanız, lütfen <a href="#">buraya tıklayın</a>.
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" data-id="3" style="margin-top: 0px;">
        <tbody>
            <tr>
                <td class="arkaplanrenk" bgcolor="#ffffff" style="background-color: rgb(255, 255, 255);">
                    <table width="620" border="0" cellspacing="0" cellpadding="0" align="center" class="scale">
                        <tbody>
                            <tr>
                                <td class="ustbilgi" bgcolor="#FFFFFF" style="background-color:#ffffff">
                                    <table width="560" border="0" cellspacing="0" cellpadding="0" align="center" class="scale">
                                        <tbody>
                                            <tr>
                                                <td height="15" style="font-size: 1px;">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table width="530" border="0" cellspacing="0" cellpadding="0" align="center" class="scale">
                                                        <tbody>
                                                            <tr>
                                                                <td style="font-family:'source_sans_probold', Helvetica, Arial, sans-serif; font-size: 24px; line-height: 30px; color: #444444;text-align:center" class="scale-center-bottom-both">
                                                                    <img src="http://useinbox-files.s3-eu-west-1.amazonaws.com/m_5ed697aa36419f0001727a63/images/bilyonerrakipbulligi182x84png7cbd54.png" width="200" style="width:200px;" id="resim0" class="" alt="Featured - Educate">
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="15" style="font-size: 1px;">&nbsp;</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" data-id="13">
        <tbody>
            <tr>
                <td class="arkaplanrenk" bgcolor="#ffffff" style="background-color: rgb(255, 255, 255);">
                    <table width="620" border="0" cellspacing="0" cellpadding="0" align="center" class="scale">
                        <tbody>
                            <tr>
                                <td class="acikarkaplanrenk" bgcolor="#FFFFFF" style="background-color:#ffffff">
                                    <table width="560" border="0" cellspacing="0" cellpadding="0" align="center" class="scale-center-both">
                                        <tbody>
                                            <tr>
                                                <td height="21">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="summernote-air" style="font-family: source_sans_probold, Helvetica, Arial, sans-serif; font-size: 32px; font-weight: 600; line-height: 35px; color: rgb(68, 68, 68);">
                                                    <div style="text-align: left;"><span style="font-size: 24px;">Merhaba {{ $user->first_name }} {{ $user->last_name }},</span></div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="10" style="font-size: 1px;">&nbsp;</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" data-id="16">
        <tbody>
            <tr>
                <td class="arkaplanrenk" bgcolor="#ffffff" style="background-color: rgb(255, 255, 255);">
                    <table width="620" border="0" cellspacing="0" cellpadding="0" align="center" class="scale">
                        <tbody>
                            <tr>
                                <td class="acikarkaplanrenk" bgcolor="#FFFFFF" style="background-color:#ffffff">
                                    <table width="560" border="0" cellspacing="0" cellpadding="0" align="center" class="scale-center-both">
                                        <tbody>
                                            <tr>
                                                <td height="10">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="summernote-air" style="font-family: source_sans_proregular, Helvetica, Arial, sans-serif; font-size: 16px; line-height: 27px; color: rgb(68, 68, 68); text-align: left;">
                                                    <div><br></div>
                                                    <div>Şifre Yenileme Başarılı. Yeni şifrenizle giriş yapabilirsiniz...&nbsp;</div>
                                                    <div><br></div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="10">&nbsp;</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" data-id="26" style="opacity: 1; margin-top: 0px;">
        <tbody>
            <tr>
                <td class="arkaplanrenk" bgcolor="#ffffff" style="background-color: rgb(255, 255, 255);">
                    <table width="620" border="0" cellspacing="0" cellpadding="0" align="center" class="scale">
                        <tbody>
                            <tr>
                                <td class="acikarkaplanrenk" bgcolor="#FFFFFF" style="background-color:#ffffff">
                                    <table width="560" border="0" cellspacing="0" cellpadding="0" align="center" class="scale-center-both">
                                        <tbody>
                                            <tr>
                                                <td height="5">&nbsp;</td>
                                            </tr>
                                            <tr data-delete="d1_115">
                                                <td align="center">
                                                    <table align="center" class="buton" style="background-color: rgb(56, 110, 64); border-radius: 5px;" border="0" cellpadding="0" cellspacing="0" bgcolor="#386e40">
                                                        <tbody>
                                                            <tr>
                                                                <td align="center" height="45" style="font-family:'source_sans_proregular', Helvetica, Arial, sans-serif; font-size: 16px; color: #FFFFFF; line-height: 45px!important; padding-left: 25px; padding-right: 25px;">
                                                                	
                                                                    <a href="{{ $url }}" class="buton_yazi" style="text-decoration: none; color: #FFFFFF" data-delete="d1_115">Rakipbul.com</a>
                                                                	
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="5">&nbsp;</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" data-id="16" style="opacity: 1; margin-top: 0px;">
        <tbody>
            <tr>
                <td class="arkaplanrenk" bgcolor="#ffffff" style="background-color: rgb(255, 255, 255);">
                    <table width="620" border="0" cellspacing="0" cellpadding="0" align="center" class="scale">
                        <tbody>
                            <tr>
                                <td class="acikarkaplanrenk" bgcolor="#FFFFFF" style="background-color:#ffffff">
                                    <table width="560" border="0" cellspacing="0" cellpadding="0" align="center" class="scale-center-both">
                                        <tbody>
                                            <tr>
                                                <td class="summernote-air" style="font-family: source_sans_proregular, Helvetica, Arial, sans-serif; font-size: 16px; line-height: 27px; color: rgb(68, 68, 68); text-align: left;">
                                                    <p>Teşekkür ederiz.</p>
                                                    <p><b>Bilyoner Rakipbul Ligi Ekibi</b></p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="10">&nbsp;</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" data-id="30">
        <tbody>
            <tr>
                <td class="arkaplanrenk" bgcolor="#ffffff" style="background-color: rgb(255, 255, 255);">
                    <table width="620" border="0" cellspacing="0" cellpadding="0" align="center" class="scale">
                        <tbody>
                            <tr>
                                <td class="arkaplanrenk" height="42" bgcolor="#ffffff" style="background-color: rgb(255, 255, 255);">
                                    &nbsp;
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" data-id="8" style="margin-top: 0px;">
        <tbody>
            <tr>
                <td class="arkaplanrenk" bgcolor="#ffffff" style="background-color: rgb(255, 255, 255);">
                    <table width="620" border="0" cellspacing="0" cellpadding="0" align="center" class="scale">
                        <tbody>
                            <tr>
                                <td class="altbilgi" bgcolor="#386e40" style="background-color: rgb(56, 110, 64);">
                                    <table width="560" border="0" cellspacing="0" cellpadding="0" align="center" class="scale-center-both">
                                        <tbody>
                                            <tr>
                                                <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="font-family: source_sans_proregular, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 27px; color: rgb(255, 255, 255);" class="summernote-air altbilgi_yazi">
                                                    <p>RB Organizasyon ve Paz. Tic. LTD. Şti.</p>
                                                    <p>212 Ağaoğlu My Office <a href="D:188">D:188</a>&nbsp;34218&nbsp;Mahmutbey İstanbul Fax: 0212 237 81 82</p>
                                                    <p><span style="font-size: 10px;">Rakipbul.com elektronik posta veri tabanında kayıtlı olduğunuz için bu iletiyi alıyorsunuz.</span></p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="15" style="font-size: 1px;">&nbsp;</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</body>

</html>