
<!--begin::Aside-->
<div class="aside aside-left aside-fixed d-flex flex-column flex-row-auto" id="kt_aside">
	<!--begin::Brand-->
	<div class="brand flex-column-auto" id="kt_brand">

		<!--begin::Logo-->
		<a href="{{ url('/') }}" class="brand-logo" style="font-size: 24px; font-weight: 900;">
			<!-- <img alt="Logo" src="assets/media/logos/logo-light.png" /> -->
			{{ config('app.name', 'Laravel') }}
		</a>
		<!--end::Logo-->

		<!--begin::Toggle-->
		<button class="brand-toggle btn btn-sm px-0" id="kt_aside_toggle">
			<span class="svg-icon svg-icon svg-icon-xl">

				<!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Angle-double-left.svg-->
				<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
					<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
						<polygon points="0 0 24 0 24 24 0 24" />
						<path d="M5.29288961,6.70710318 C4.90236532,6.31657888 4.90236532,5.68341391 5.29288961,5.29288961 C5.68341391,4.90236532 6.31657888,4.90236532 6.70710318,5.29288961 L12.7071032,11.2928896 C13.0856821,11.6714686 13.0989277,12.281055 12.7371505,12.675721 L7.23715054,18.675721 C6.86395813,19.08284 6.23139076,19.1103429 5.82427177,18.7371505 C5.41715278,18.3639581 5.38964985,17.7313908 5.76284226,17.3242718 L10.6158586,12.0300721 L5.29288961,6.70710318 Z" fill="#000000" fill-rule="nonzero" transform="translate(8.999997, 11.999999) scale(-1, 1) translate(-8.999997, -11.999999)" />
						<path d="M10.7071009,15.7071068 C10.3165766,16.0976311 9.68341162,16.0976311 9.29288733,15.7071068 C8.90236304,15.3165825 8.90236304,14.6834175 9.29288733,14.2928932 L15.2928873,8.29289322 C15.6714663,7.91431428 16.2810527,7.90106866 16.6757187,8.26284586 L22.6757187,13.7628459 C23.0828377,14.1360383 23.1103407,14.7686056 22.7371482,15.1757246 C22.3639558,15.5828436 21.7313885,15.6103465 21.3242695,15.2371541 L16.0300699,10.3841378 L10.7071009,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(15.999997, 11.999999) scale(-1, 1) rotate(-270.000000) translate(-15.999997, -11.999999)" />
					</g>
				</svg>

				<!--end::Svg Icon-->
			</span>
		</button>

		<!--end::Toolbar-->
	</div>

	<!--end::Brand-->

	<!--begin::Aside Menu-->
	<div class="aside-menu-wrapper flex-column-fluid" id="kt_aside_menu_wrapper">

		<!--begin::Menu Container-->
		<div id="kt_aside_menu" class="aside-menu my-4" data-menu-vertical="1" data-menu-scroll="1" data-menu-dropdown-timeout="500">

			<!--begin::Menu Nav-->
			<ul class="menu-nav">

				<li class="menu-item {{ (Request::segment(1) === 'dashboard') ? 'menu-item-active' : '' }}" aria-haspopup="true">
					<a href="{{ url('/dashboard') }}" class="menu-link">
						<span class="menu-icon"> <i class="fas fa-layer-group {{ (Request::segment(1) === 'dashboard') ? 'text-primary' : '' }}"></i> </span>
						<span class="menu-text">Dashboard</span>
					</a>
				</li>

				@if(Auth::user()->hasPermissionTo('view_user'))
					<li class="menu-item {{ (Request::segment(1) === 'users') ? 'menu-item-active' : '' }}" aria-haspopup="true">
						<a href="{{ url('/users') }}" class="menu-link">
							<span class="menu-icon"> <i class="fas fa-users {{ (Request::segment(1) === 'users') ? 'text-primary' : '' }}"></i> </span>
							<span class="menu-text"> Kullanıcılar </span>
						</a>
					</li>
				@endif

				@if(Auth::user()->hasPermissionTo('view_permission'))
					<li class="menu-item {{ (Request::segment(1) === 'roles') ? 'menu-item-active' : '' }}" aria-haspopup="true">
						<a href="{{ url('/roles') }}" class="menu-link">
							<span class="menu-icon"> <i class="fas fa-user-shield {{ (Request::segment(1) === 'roles') ? 'text-primary' : '' }}"></i> </span>
							<span class="menu-text"> Yetkilendirme </span>
						</a>
					</li>
				@endif


				<li class="menu-section">
					<h4 class="menu-text">Konum</h4>
					<i class="menu-icon ki ki-bold-more-hor icon-md"></i>
				</li>

				@if(Auth::user()->hasPermissionTo('view_conference'))
					<li class="menu-item {{ (Request::segment(1) === 'conferences') ? 'menu-item-active' : '' }}" aria-haspopup="true">
						<a href="{{ url('/conferences') }}" class="menu-link">
							<span class="menu-icon"> <i class="fas fa-globe {{ (Request::segment(1) === 'conferences') ? 'text-primary' : '' }}"></i> </span>
							<span class="menu-text"> Konferanslar </span>
						</a>
					</li>
				@endif

				@php
					//RoleProvince
			        $roleProvince = collect(Auth::user()->roleprovince)->pluck('province_id');
        		@endphp
				@if(Auth::user()->hasPermissionTo('view_province') && $roleProvince->count() == 0)
					<li class="menu-item {{ (Request::segment(1) === 'provinces') ? 'menu-item-active' : '' }}" aria-haspopup="true">
						<a href="{{ url('/provinces') }}" class="menu-link">
							<span class="menu-icon"> <i class="fas fa-city {{ (Request::segment(1) === 'provinces') ? 'text-primary' : '' }}"></i> </span>
							<span class="menu-text"> Şehirler </span>
						</a>
					</li>
				@endif

				@if(Auth::user()->hasPermissionTo('view_district'))
					<li class="menu-item {{ (Request::segment(1) === 'districts') ? 'menu-item-active' : '' }}" aria-haspopup="true">
						<a href="{{ url('/districts') }}" class="menu-link">
							<span class="menu-icon"> <i class="fas fa-building {{ (Request::segment(1) === 'districts') ? 'text-primary' : '' }}"></i> </span>
							<span class="menu-text"> İlçeler </span>
						</a>
					</li>
				@endif


				<li class="menu-section">
					<h4 class="menu-text">Ligler</h4>
					<i class="menu-icon ki ki-bold-more-hor icon-md"></i>
				</li>
				
				@if(Auth::user()->hasPermissionTo('view_league'))
					<li class="menu-item {{ (Request::segment(1) === 'leagues') ? 'menu-item-active' : '' }}" aria-haspopup="true">
						<a href="{{ url('/leagues') }}" class="menu-link">
							<span class="menu-icon"> <i class="fas fa-boxes {{ (Request::segment(1) === 'leagues') ? 'text-primary' : '' }}"></i> </span>
							<span class="menu-text"> Ligler </span>
						</a>
					</li>
				@endif

				@if(Auth::user()->hasPermissionTo('view_league'))
					<li class="menu-item {{ (Request::segment(1) === 'champions') ? 'menu-item-active' : '' }}" aria-haspopup="true">
						<a href="{{ url('/champions') }}" class="menu-link">
							<span class="menu-icon"> <i class="fas fa-trophy {{ (Request::segment(1) === 'champions') ? 'text-primary' : '' }}"></i> </span>
							<span class="menu-text"> Şampiyonluklar </span>
						</a>
					</li>
				@endif

				@if(Auth::user()->hasPermissionTo('view_season'))
					<li class="menu-item {{ (Request::segment(1) === 'seasons') ? 'menu-item-active' : '' }}" aria-haspopup="true">
						<a href="{{ url('/seasons') }}" class="menu-link">
							<span class="menu-icon"> <i class="fas fa-box {{ (Request::segment(1) === 'seasons') ? 'text-primary' : '' }}"></i> </span>
							<span class="menu-text"> Sezonlar </span>
						</a>
					</li>
				@endif

				@if(Auth::user()->hasPermissionTo('view_team'))
					<li class="menu-item {{ (Request::segment(1) === 'teams') ? 'menu-item-active' : '' }}" aria-haspopup="true">
						<a href="{{ url('/teams') }}" class="menu-link">
							<span class="menu-icon"> <i class="fas fa-users {{ (Request::segment(1) === 'teams') ? 'text-primary' : '' }}"></i> </span>
							<span class="menu-text"> Takımlar </span>
						</a>
					</li>
				@endif

				@if(Auth::user()->hasPermissionTo('view_match'))
					<li class="menu-item {{ (Request::segment(1) === 'matches') ? 'menu-item-active' : '' }}" aria-haspopup="true">
						<a href="{{ url('/matches') }}" class="menu-link">
							<span class="menu-icon"> <i class="fas fa-futbol {{ (Request::segment(1) === 'matches') ? 'text-primary' : '' }}"></i> </span>
							<span class="menu-text"> Maçlar </span>
						</a>
					</li>
				@endif

				@if(Auth::user()->hasPermissionTo('view_reservation'))
					<li class="menu-item {{ (Request::segment(1) === 'reservations') ? 'menu-item-active' : '' }}" aria-haspopup="true">
						<a href="{{ url('/reservations') }}" class="menu-link">
							<span class="menu-icon"> <i class="fas fa-calendar-check {{ (Request::segment(1) === 'reservations') ? 'text-primary' : '' }}"></i> </span>
							<span class="menu-text"> Rezervasyon </span>
						</a>
					</li>
				@endif


				<li class="menu-section">
					<h4 class="menu-text">Halı Saha</h4>
					<i class="menu-icon ki ki-bold-more-hor icon-md"></i>
				</li>

				@if(Auth::user()->hasPermissionTo('view_ground'))
					<li class="menu-item {{ (Request::segment(1) === 'grounds') ? 'menu-item-active' : '' }}" aria-haspopup="true">
						<a href="{{ url('/grounds') }}" class="menu-link">
							<span class="menu-icon"> <i class="fas fa-border-all {{ (Request::segment(1) === 'grounds') ? 'text-primary' : '' }}"></i> </span>
							<span class="menu-text"> Halı Sahalar </span>
						</a>
					</li>
				@endif

				@if(Auth::user()->hasPermissionTo('view_groundproperty'))
					<li class="menu-item {{ (Request::segment(1) === 'ground_properties') ? 'menu-item-active' : '' }}" aria-haspopup="true">
						<a href="{{ url('/ground_properties') }}" class="menu-link">
							<span class="menu-icon"> <i class="fas fa-border-none {{ (Request::segment(1) === 'ground_properties') ? 'text-primary' : '' }}"></i> </span>
							<span class="menu-text"> Halı Saha Özellikleri </span>
						</a>
					</li>
				@endif

				<li class="menu-section">
					<h4 class="menu-text">Banner</h4>
					<i class="menu-icon ki ki-bold-more-hor icon-md"></i>
				</li>

				@if(Auth::user()->hasPermissionTo('add_banner'))
					<li class="menu-item {{ (Request::segment(1) === 'banners') ? 'menu-item-active' : '' }}" aria-haspopup="true">
						<a href="{{ url('/banners') }}" class="menu-link">
							<span class="menu-icon"> <i class="fas fa-flag {{ (Request::segment(1) === 'banners') ? 'text-primary' : '' }}"></i> </span>
							<span class="menu-text"> Banner Yönetimi </span>
						</a>
					</li>
				@endif

				@if(Auth::user()->hasPermissionTo('add_banner'))
					<li class="menu-section">
						<h4 class="menu-text">Panorama İşlemleri</h4>
						<i class="menu-icon ki ki-bold-more-hor icon-md"></i>
					</li>

					<li class="menu-item {{ (Request::segment(1) === 'monthly_panorama') ? 'menu-item-active' : '' }}" aria-haspopup="true">
						<a href="{{ url('/monthly_panorama') }}" class="menu-link">
							<span class="menu-icon"> <i class="fas fa-crosshairs {{ (Request::segment(1) === 'monthly_panorama') ? 'text-primary' : '' }}"></i> </span>
							<span class="menu-text"> Aylık Panoramalar </span>
						</a>
					</li>

					<li class="menu-item {{ (Request::segment(1) === 'panorama_types') ? 'menu-item-active' : '' }}" aria-haspopup="true">
						<a href="{{ url('/panorama_types') }}" class="menu-link">
							<span class="menu-icon"> <i class="fas fa-crosshairs {{ (Request::segment(1) === 'panorama_types') ? 'text-primary' : '' }}"></i> </span>
							<span class="menu-text"> Panorama Türleri </span>
						</a>
					</li>
				@endif

				<li class="menu-section">
					<h4 class="menu-text">Ceza İşlemleri</h4>
					<i class="menu-icon ki ki-bold-more-hor icon-md"></i>
				</li>
				<li class="menu-item {{ (Request::segment(1) === 'penalties') ? 'menu-item-active' : '' }}" aria-haspopup="true">
					<a href="{{ url('/penalties') }}" class="menu-link">
						<span class="menu-icon"> <i class="fas fa-balance-scale {{ (Request::segment(1) === 'penalties') ? 'text-primary' : '' }}"></i> </span>
						<span class="menu-text"> Cezalar </span>
					</a>
				</li>
				<li class="menu-item {{ (Request::segment(1) === 'penalty_types') ? 'menu-item-active' : '' }}" aria-haspopup="true">
					<a href="{{ url('/penalty_types') }}" class="menu-link">
						<span class="menu-icon"> <i class="fas fa-gavel {{ (Request::segment(1) === 'penalty_types') ? 'text-primary' : '' }}"></i> </span>
						<span class="menu-text"> Ceza Türleri </span>
					</a>
				</li>



				<li class="menu-section">
					<h4 class="menu-text">Haberler</h4>
					<i class="menu-icon ki ki-bold-more-hor icon-md"></i>
				</li>
				<li class="menu-item {{ (Request::segment(1) === 'news') ? 'menu-item-active' : '' }}" aria-haspopup="true">
					<a href="{{ url('/news') }}" class="menu-link">
						<span class="menu-icon"> <i class="fas fa-newspaper {{ (Request::segment(1) === 'news') ? 'text-primary' : '' }}"></i> </span>
						<span class="menu-text"> Haberler </span>
					</a>
				</li>
				<li class="menu-item {{ (Request::segment(1) === 'news_types') ? 'menu-item-active' : '' }}" aria-haspopup="true">
					<a href="{{ url('/news_types') }}" class="menu-link">
						<span class="menu-icon"> <i class="fas fa-tasks {{ (Request::segment(1) === 'news_types') ? 'text-primary' : '' }}"></i> </span>
						<span class="menu-text"> Haber Türleri </span>
					</a>
				</li>
				<li class="menu-item {{ (Request::segment(1) === 'tags') ? 'menu-item-active' : '' }}" aria-haspopup="true">
					<a href="{{ url('/tags') }}" class="menu-link">
						<span class="menu-icon"> <i class="fas fa-tags {{ (Request::segment(1) === 'tags') ? 'text-primary' : '' }}"></i> </span>
						<span class="menu-text"> Etiketler </span>
					</a>
				</li>
				<li class="menu-item {{ (Request::segment(1) === 'agenda') ? 'menu-item-active' : '' }}" aria-haspopup="true">
					<a href="{{ url('/agenda') }}" class="menu-link">
						<span class="menu-icon"> <i class="fas fa-photo-video {{ (Request::segment(1) === 'agenda') ? 'text-primary' : '' }}"></i> </span>
						<span class="menu-text"> Gündem </span>
					</a>
				</li>





				<li class="menu-section">
					<h4 class="menu-text">Tanımlamalar</h4>
					<i class="menu-icon ki ki-bold-more-hor icon-md"></i>
				</li>
				
				@if(Auth::user()->hasPermissionTo('view_position'))
					<li class="menu-item {{ (Request::segment(1) === 'player_positions') ? 'menu-item-active' : '' }}" aria-haspopup="true">
						<a href="{{ url('/player_positions') }}" class="menu-link">
							<span class="menu-icon"> <i class="fas fa-crosshairs {{ (Request::segment(1) === 'player_positions') ? 'text-primary' : '' }}"></i> </span>
							<span class="menu-text"> Oyuncu Pozisyonları </span>
						</a>
					</li>
				@endif
				
				@if(Auth::user()->hasPermissionTo('view_pointtype'))
					<li class="menu-item {{ (Request::segment(1) === 'point_types') ? 'menu-item-active' : '' }}" aria-haspopup="true">
						<a href="{{ url('/point_types') }}" class="menu-link">
							<span class="menu-icon"> <i class="fas fa-crosshairs {{ (Request::segment(1) === 'point_types') ? 'text-primary' : '' }}"></i> </span>
							<span class="menu-text"> Puan Türleri </span>
						</a>
					</li>
				@endif

				@if(Auth::user()->hasPermissionTo('view_tactic'))
					<li class="menu-item {{ (Request::segment(1) === 'tactics') ? 'menu-item-active' : '' }}" aria-haspopup="true">
						<a href="{{ url('/tactics') }}" class="menu-link">
							<span class="menu-icon"> <i class="fas fa-crosshairs {{ (Request::segment(1) === 'tactics') ? 'text-primary' : '' }}"></i> </span>
							<span class="menu-text"> Taktikler </span>
						</a>
					</li>
				@endif

				@if(Auth::user()->hasPermissionTo('view_potential'))
					<li class="menu-item {{ (Request::segment(1) === 'team_potentials') ? 'menu-item-active' : '' }}" aria-haspopup="true">
						<a href="{{ url('/team_potentials') }}" class="menu-link">
							<span class="menu-icon"> <i class="fas fa-crosshairs {{ (Request::segment(1) === 'team_potentials') ? 'text-primary' : '' }}"></i> </span>
							<span class="menu-text"> Takım Potansiyelleri </span>
						</a>
					</li>
				@endif

				@if(Auth::user()->hasPermissionTo('view_satisfaction'))
					<li class="menu-item {{ (Request::segment(1) === 'team_satisfactions') ? 'menu-item-active' : '' }}" aria-haspopup="true">
						<a href="{{ url('/team_satisfactions') }}" class="menu-link">
							<span class="menu-icon"> <i class="fas fa-crosshairs {{ (Request::segment(1) === 'team_satisfactions') ? 'text-primary' : '' }}"></i> </span>
							<span class="menu-text"> Takım Memnuniyetleri </span>
						</a>
					</li>
				@endif

				@if(Auth::user()->hasPermissionTo('view_satisfaction'))
					<li class="menu-item {{ (Request::segment(1) === 'team_satisfactions_jogo') ? 'menu-item-active' : '' }}" aria-haspopup="true">
						<a href="{{ url('/team_satisfactions_jogo') }}" class="menu-link">
							<span class="menu-icon"> <i class="fas fa-crosshairs {{ (Request::segment(1) === 'team_satisfactions_jogo') ? 'text-primary' : '' }}"></i> </span>
							<span class="menu-text"> Takım Jogo Memnuniyetleri </span>
						</a>
					</li>
				@endif

				@if(Auth::user()->hasPermissionTo('view_rules'))
					<li class="menu-item {{ (Request::segment(1) === 'rules') ? 'menu-item-active' : '' }}" aria-haspopup="true">
						<a href="{{ url('/rules') }}" class="menu-link">
							<span class="menu-icon"> <i class="fas fa-crosshairs {{ (Request::segment(1) === 'rules') ? 'text-primary' : '' }}"></i> </span>
							<span class="menu-text"> Kural Kitapçığı </span>
						</a>
					</li>
				@endif


			</ul>

			<!--end::Menu Nav-->
		</div>

		<!--end::Menu Container-->
	</div>

	<!--end::Aside Menu-->
</div>

<!--end::Aside-->