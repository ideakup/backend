<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">{{ data_get($value, 'title') }} : </label>
    <div class="col-lg-6 col-sm-12">
        <div class="input-group input-group-solid date" id="datetimepicker_{{$key}}" data-target-input="nearest">
            <input type="text" class="form-control form-control-solid datetimepicker-input" 

            name="{{ $key }}"
            placeholder="{{ data_get($value, 'placeholder') }}" 
            data-target="#datetimepicker_{{$key}}" 
            
            value="{{ (!is_null($model_data)) ? Carbon\Carbon::parse($model_data->$key)->format('d.m.Y H:m') : Carbon\Carbon::now()->format('d.m.Y H:m') }}"
            
            @if(Request::segment(2) == 'view' || Request::segment(2) == 'delete') disabled="disabled" @endif

            />
            <div class="input-group-append" data-target="#datetimepicker_{{$key}}" data-toggle="datetimepicker">
                <span class="input-group-text">
                    <i class="fas fa-calendar-alt"></i>
                </span>
            </div>
        </div>
        @if(!empty(data_get($value, 'description')))
            <span class="form-text text-muted">{{ data_get($value, 'description') }}</span>
        @endif
    </div>
</div>