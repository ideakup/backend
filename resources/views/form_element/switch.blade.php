@if( !empty(data_get($value, 'addvisible')) && data_get($value, 'addvisible') == 'no' && Request::segment(2) == 'add' )

@else

	<div class="form-group row">
		<label class="col-form-label text-right col-lg-3 col-sm-12">{{ data_get($value, 'title') }} :</label>
		<div class="col-lg-9 col-md-9 col-sm-12">
			<input name="completed" data-switch="true" type="checkbox" data-handle-width="100"

				@if(!empty($model_data->$key) && $model_data->$key == true)
					checked="checked"
				@endif

				data-on-text="{{ data_get($value, 'data')['on']['name'] }}" data-on-color="primary" 
				data-off-text="{{ data_get($value, 'data')['off']['name'] }}" data-off-color="warning"

				 @if(Request::segment(2) == 'view' || Request::segment(2) == 'delete' || (Request::segment(2) == 'edit' && (!$team1_last_match_control || !$team2_last_match_control))) disabled="disabled" @endif

			/>
			@if(!empty(data_get($value, 'description')))
		    	<span class="form-text text-muted">{{ data_get($value, 'description') }}</span>
		    @endif
		</div>
	</div>

	@if(Request::segment(2) == 'edit' && (!$team1_last_match_control || !$team2_last_match_control))
		<div class="form-group row">
			<label class="col-form-label text-right col-lg-3 col-sm-12"> </label>
			<div class="col-lg-9 col-md-9 col-sm-12">
				<div class="alert alert-custom alert-light-warning fade show mb-5" role="alert">
					<div class="alert-text">
						<strong>Yalnızca takımların son maçını "Devam Ediyor" durumuna alabilirsiniz.</strong><br>
						{!! (!$team1_last_match_control) ? '<strong>'.$model_data->team1->name.'</strong> takımının tamamlanmış son maçı değildir.<br>' : '' !!}
						{!! (!$team2_last_match_control) ? '<strong>'.$model_data->team2->name.'</strong> takımının tamamlanmış son maçı değildir.' : '' !!}
					</div>
				</div>
			</div>
		</div>
	@endif

@endif

