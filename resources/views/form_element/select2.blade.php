@php
	//dump($model_data->team1->team_teamplayer);
	//dump($model_data->team2->team_teamplayer);
	//dump($model_data->match_press->press_conferences_players);
	//dump($model_data->match_press);
	//dump($key);

	//dump($value);
	//dump(App\Season::find(request()->query('tree_id'))->league_id);
	//dump('league_id: '.$model_data->team_teamseason->season->league->name);
    //dump('season_id: '.$model_data->team_teamseason->season->id);
    //dump('season_name: '.$model_data->team_teamseason->season->name);
	//dump($model_data->eliminaton_tree_teams);
	
	//dump($team1_last_match_control);
	//dump($team2_last_match_control);

	//dump($model_data);


	//dump(data_get($value, 'dataSource')::find($model_data[$key]));
	//dump($model_data->league_eliminationseason->base_season);
	/*
	dump($key);
	dump($value);
	dump($model_data[$key]);
	dump($model_data->panorama_types_prize);
	*/
	//dump(json_decode($model_data->season_katilimci_teams->data));

	//dump(Request::segment(1));



@endphp

@if( !empty(data_get($value, 'addvisible')) && data_get($value, 'addvisible') == 'no' && Request::segment(2) == 'add' )

@else

	<div class="form-group row">
		<label class="col-form-label text-right col-lg-3 col-sm-12">{{ data_get($value, 'title') }} : </label>
		<div class="col-lg-6 col-sm-12">


			@if(!empty(request()->query('tree_id')) && $key == 'league_id')
				@php 
					$ss = App\Season::find(request()->query('tree_id'));
				@endphp
				<input type="hidden" name="league_id" id="select2_league_id" value="{{ $ss->league->id }}">
				<div class="alert alert-secondary" role="alert">{{ $ss->league->name }}</div>
				
			@elseif(!empty(request()->query('tree_id')) && $key == 'season_id')
				@php 
					$ss = App\Season::find(request()->query('tree_id'));
				@endphp
				<input type="hidden" name="season_id" id="select2_season_id" value="{{ $ss->id }}">
				<div class="alert alert-secondary" role="alert">{{ $ss->year }} {{ $ss->league->name }} {{ $ss->name }}</div>
				
			@else
			
				<select class="form-control select2" id="select2_{{$key}}" 
					
					@if(!empty(data_get($value, 'multiple')))
						name="{{ $key }}[]"
						multiple="multiple"
					@else
						name="{{ $key }}"
					@endif 

					@if(Request::segment(2) == 'view' || Request::segment(2) == 'delete') disabled="disabled" @endif



					@if(Request::segment(2) == 'edit' && $key == 'completed_status' && ($model_data->completed || (!$team1_last_match_control || !$team2_last_match_control))) disabled="disabled" @endif


				>
					@if(empty(data_get($value, 'trigger')))
						<option></option>

						@if(empty(data_get($value, 'search')))

							@if(empty(data_get($value, 'dataSource')))

								@if(data_get($value, 'seperator') == 'match_team_player')
									
									@php
										$__team1 = $model_data->team1;
							        @endphp
							        <option value="guest1">---{{ '('.$__team1->name.') Misafir Oyuncu' }}---</option>
							        @foreach($__team1->player as $__team1_player)
										<option value="{{ $__team1_player->id }}">
											{{ '('.$__team1->name.') '.$__team1_player->user->first_name.' '.$__team1_player->user->last_name }}
										</option>
							        @endforeach
							        @php
										$__team2 = $model_data->team2;
							        @endphp
							        @if(!empty($__team2))
								        <option value="guest2">---{{ '('.$__team2->name.') Misafir Oyuncu' }}---</option>
								        @foreach($__team2->player as $__team2_player)
											<option value="{{ $__team2_player->id }}">
												{{ '('.$__team2->name.') '.$__team2_player->user->first_name.' '.$__team2_player->user->last_name}}
											</option>
								        @endforeach
							   		@endif
							   	@elseif(data_get($value, 'seperator') == 'match_team_player_all')
									
									@php
										$match_team_player_all = $model_data->match_player;
							        @endphp
							        @foreach($match_team_player_all as $match_team_player)
										<option value="{{ $match_team_player->id }}">

											@if(empty($match_team_player->player_id))
												({{ $match_team_player->team->name }}) {{ $match_team_player->guest_name }}
											@else
												({{ $match_team_player->team->name }}) {{ $match_team_player->player->user->first_name }} {{ $match_team_player->player->user->last_name }}
											@endif

										</option>
							        @endforeach
							   	
							   	@elseif(data_get($value, 'seperator') == 'match_team_player_press')
							   		@php
						        		//print_r($model_data->match_press->press_conferences_players);
						        		//die;
						        	@endphp
							        @foreach($model_data->match_player as $m_player)

										<option value="{{ $m_player->id }}"
												@if(!empty($model_data->match_press))
													@foreach($model_data->match_press->press_conferences_players as $m_pcp)

														@if($m_player->id == $m_pcp->matchplayer_id)
															selected="selected"
														@endif 

													@endforeach
												@endif 
											>
											@if(empty($m_player->player_id))
												{{ '('.$m_player->team->name.') '.$m_player->guest_name }}
											@else
												{{ '('.$m_player->team->name.') '.$m_player->player->user->first_name.' '.$m_player->player->user->last_name }}
											@endif
										</option>
							        @endforeach

							    @elseif(data_get($value, 'seperator') == 'match_teams')

							    	@if(!empty($model_data->team1))
										<option value="{{ $model_data->team1->id }}"
											@if(!empty($model_data->match_press))
												@if($model_data->team1->id == $model_data->match_press->team_id)
													selected="selected"
												@endif 
											@endif 
											>
											{{ $model_data->team1->name }}
										</option>
									@endif
									@if(!empty($model_data->team2))
										<option value="{{ $model_data->team2->id }}"
											@if(!empty($model_data->match_press))
												@if($model_data->team2->id == $model_data->match_press->team_id)
													selected="selected"
												@endif 
											@endif 
											>
											{{ $model_data->team2->name }}
										</option>
									@endif
							    @endif

							@else

								@php
									if( data_get($value, 'dataSource') == 'json_data'){
										$dataSource = data_get($value, 'json_data');
									}else{

										if($key == 'province_id'){
											//RoleProvince
											$roleProvince = collect(Auth::user()->roleprovince)->pluck('province_id');
									        if($roleProvince->count() > 0){
									            $dataSource = data_get($value, 'dataSource')::whereIn('id', $roleProvince)->get();
									        }else{
									            $dataSource = data_get($value, 'dataSource')::all();
									        }
										}else if($key == 'league_id'){
											//RoleProvince
											$roleProvince = collect(Auth::user()->roleprovince)->pluck('province_id');
									        if($roleProvince->count() > 0){
									            $dataSource = App\League::
										        select( 'leagues.id',
										                'leagues.name',
										                'leagues.active',
										                DB::raw("count(leagues_provinces.province_id) as count")
										        )->
										        leftJoin('leagues_provinces', 'leagues.id', '=', 'leagues_provinces.league_id')->
										        whereIn('leagues_provinces.province_id', $roleProvince)->groupBy('leagues.id')->get();
									        }else{
									            $dataSource = App\League::all();
									        }

									    }else if($key == 'panorama_id' && data_get($value, 'seperator') == 'monthly_panorama_player_types'){ //'seperator' => 'monthly_panorama_types',
									    	$dataSource = data_get($value, 'dataSource')::
									    	leftJoin('panorama_types_prize', 'panorama_types.id', '=', 'panorama_types_prize.panoramatype_ptr_id')->
									    	where('panorama_types_prize.period', 'month')->where('panorama_types.to_who','player')->get();

									    }else if($key == 'panorama_id' && data_get($value, 'seperator') == 'monthly_panorama_team_types'){ //'seperator' => 'monthly_panorama_types',
									    	$dataSource = data_get($value, 'dataSource')::
									    	leftJoin('panorama_types_prize', 'panorama_types.id', '=', 'panorama_types_prize.panoramatype_ptr_id')->
									    	where('panorama_types_prize.period', 'month')->where('panorama_types.to_who','team')->get();

										}else if($key == 'panorama_id' && data_get($value, 'seperator') == 'match_panorama_type'){ //'seperator' => 'monthly_panorama_types',
									    	$dataSource = data_get($value, 'dataSource')::
									    	leftJoin('panorama_types_prize', 'panorama_types.id', '=', 'panorama_types_prize.panoramatype_ptr_id')->
									    	where('panorama_types_prize.period', 'match')->get();

										}else{
											$dataSource = data_get($value, 'dataSource')::all();
										}

									}
								@endphp

								@foreach ($dataSource as $valuee)
									
									<option value="{{ $valuee['id'] }}"

										@if(empty(data_get($value, 'add_only')))
											
											@if(!empty($model_data))

												@if(!empty($value_tabs) && !empty(data_get($value_tabs, 'relationship')))
													
													@php
														if(!empty(data_get($value, 'renamecol'))){
															$key_r = data_get($value, 'renamecol');
														}else{
															$key_r = $key;
														}
													@endphp

													@if( !empty($model_data[data_get($value_tabs, 'relationship')][$key_r]) )
				                						@if( $model_data[data_get($value_tabs, 'relationship')][$key_r] ==  $valuee['id'] )
															selected="selected"
														@endif
													@endif

												@elseif(!empty(data_get($value, 'relationship')))
												
													@if(empty(data_get($value, 'multiple')))

														@if( $model_data[data_get($value, 'relationship')][data_get($value, 'relation_col')] ==  $valuee['id'] )
															selected="selected"
														@endif

													@else

														@foreach ($model_data[data_get($value, 'relationship')] as $selectedVal)
															@if( $selectedVal[data_get($value, 'relation_col')] ==  $valuee['id'] )
																selected="selected"
															@endif
														@endforeach

													@endif

												@else

													@if(data_get($value, 'seperator') == 'user_role_id')
														@if(!empty($model_data->getRoleNames()))
															@if(in_array($valuee['name'], $model_data->getRoleNames()->toArray()))
																selected="selected"
															@endif
														@endif
													@elseif(data_get($value, 'seperator') == 'reservation_league_id')
												        @if($model_data->team_teamseason->season->league_id == $valuee['id'])
															selected="selected"
														@endif
													@elseif(data_get($value, 'seperator') == 'elimination_tree_league_id')
												        @if($model_data->league_eliminationseason->season_ptr->league_id == $valuee['id'])
															selected="selected"
														@endif
													@elseif(data_get($value, 'seperator') == 'fixture_group_league_id')
												        @if($model_data->league_fixtureseason->season_ptr->league_id == $valuee['id'])
															selected="selected"
														@endif
													@elseif($model_data[$key] == $valuee['id'])
														selected="selected"
													@elseif(Request::segment(1) == 'panorama_types' && $key == 'period' && $model_data->panorama_types_prize->period == $valuee['id'])
														selected="selected"
													@elseif(Request::segment(1) == 'panorama_types' && $key == 'position_id' && $model_data->panorama_types_prize->position_id == $valuee['id'])
														selected="selected"
													@endif



												@endif
											
											@endif
										@endif
									>
										{{ $valuee['name'] }}
									</option>

								@endforeach

							@endif

						@else

							@if(data_get($value, 'relationship') == 'user')

								@php
									$__user = data_get($value, 'dataSource')::find($model_data[$key])[data_get($value, 'relationship')];
								@endphp
								@if(!is_null($__user))
									<option value="{{ $model_data[$key] }}" selected="selected">
										{{ '(TC:'.$__user->profile->identity_number.') '.$__user->first_name.' '.$__user->last_name }}
									</option>
								@endif
							@elseif(data_get($value, 'seperator') == 'active_season')

								@php
									$__season = data_get($value, 'dataSource')::find($model_data[$key]);
						        @endphp

						        @if(!is_null($__season))
									<option value="{{ $model_data[$key] }}" selected="selected">
										{{ $__season->id.' '.$__season->league->name.' '.$__season->year.' '.$__season->name }}
									</option>
								@endif
							@elseif(data_get($value, 'seperator') == 'base_season')

								@php
									// gogo
									$__season = $model_data;
						        @endphp

						        @if(!is_null($__season))
						        	@php
										$__baseseason = $model_data->league_eliminationseason->base_season;
							        @endphp
							        @if(!is_null($__baseseason))
										<option value="{{$__baseseason->id }}" selected="selected">
											{{ $__baseseason->league->name.' '.$__baseseason->year.' '.$__baseseason->name }}
										</option>
									@endif
								@endif
							@elseif(data_get($value, 'seperator') == 'match_team')

								@php
									$__team = App\Team::find($model_data[$key]);
						        @endphp
						        
						        @if(!is_null($__team))
									<option value="{{ $model_data[$key] }}" selected="selected">
										{{ $__team->name }}
									</option>
								@endif
							@elseif(data_get($value, 'seperator') == 'match_ground')

								@php
									$__ground = App\Ground::find($model_data[$key]);
						        @endphp
						        
						        @if(!is_null($__ground))
									<option value="{{ $model_data[$key] }}" selected="selected">
										{{ $__ground->name }}
									</option>
								@endif
							@elseif(data_get($value, 'seperator') == 'match_referee')

								@php
									$__user = App\Referee::find($model_data[$key]);
						        @endphp
						        
						        @if(!is_null($__user))
									<option value="{{ $model_data[$key] }}" selected="selected">
										{{ $__user->user->first_name.' '.$__user->user->last_name }}
									</option>
								@endif
							@elseif(data_get($value, 'seperator') == 'match_coordinator')

								@php
									$__user = App\Coordinator::find($model_data[$key]);
						        @endphp
						        
						        @if(!is_null($__user))
									<option value="{{ $model_data[$key] }}" selected="selected">
										{{ $__user->user->first_name.' '.$__user->user->last_name }}
									</option>
								@endif
							@elseif(data_get($value, 'seperator') == 'reservation_season_id' && !empty($model_data))

								@php
									$__season = App\Season::find($model_data->team_teamseason->season->id);
						        @endphp
						        
						        @if(!is_null($__season))
									<option value="{{ $__season->id }}" selected="selected">
										{{ $__season->league->name.' '.$__season->year.' '.$__season->name }}
									</option>
								@endif
							@elseif(data_get($value, 'seperator') == 'reservation_ground_id' && !empty($model_data))

								@php
									$__grounds = App\Ground::find($model_data->reservation_groundtable->ground->id);
						        @endphp
						        
						        @if(!is_null($__grounds))
									<option value="{{ $__grounds->id }}" selected="selected">
										{{ $__grounds->name }}
									</option>
								@endif
							@elseif(data_get($value, 'seperator') == 'reservation_team_id' && !empty($model_data))

								@php
									$__team = App\Team::find($model_data->team_teamseason->team->id);
						        @endphp
						        
						        @if(!is_null($__team))
									<option value="{{ $__team->id }}" selected="selected">
										{{ $__team->name }}
									</option>
								@endif
							@elseif(data_get($value, 'seperator') == 'match_time' && !empty($model_data))
						        
						        @if(!is_null($model_data->time))
									<option value="{{ $model_data->time }}" selected="selected">
										{{ Carbon\Carbon::parse($model_data->time)->format('H:i') }}
									</option>
								@endif
							@elseif(data_get($value, 'seperator') == 'reservation_time' && !empty($model_data))
						        
						        @if(!is_null($model_data->reservation_groundtable))
									<option value="{{ $model_data->reservation_groundtable->id }}" selected="selected">
										{{ Carbon\Carbon::parse($model_data->reservation_groundtable->time)->format('H:i') }}
									</option>
								@endif
							@elseif(data_get($value, 'seperator') == 'elimination_tree_season_id' && !empty($model_data))
						        
						        @if(!is_null($model_data->league_eliminationseason->season_ptr))
									<option value="{{ $model_data->league_eliminationseason->season_ptr->id }}" selected="selected">
										{{ $model_data->league_eliminationseason->season_ptr->year.' '.$model_data->league_eliminationseason->season_ptr->league->name.' '.$model_data->league_eliminationseason->season_ptr->name }}
									</option>
								@endif
							@elseif(data_get($value, 'seperator') == 'fixture_group_season_id' && !empty($model_data))
						        
						        @if(!is_null($model_data->league_fixtureseason->season_ptr))
									<option value="{{ $model_data->league_fixtureseason->season_ptr->id }}" selected="selected">
										{{ $model_data->league_fixtureseason->season_ptr->year.' '.$model_data->league_fixtureseason->season_ptr->league->name.' '.$model_data->league_fixtureseason->season_ptr->name }}
									</option>
								@endif
							@elseif(data_get($value, 'seperator') == 'elimination_tree_teams')

								@if(!empty($model_data->eliminaton_tree_teams))
							    	@foreach($model_data->eliminaton_tree_teams as $etTeam)
										<option value="{{ $etTeam->team_id }}" selected="selected">
											{{ $etTeam->team->name }}
										</option>
									@endforeach
								@endif
							@elseif(data_get($value, 'seperator') == 'fixture_group_teams')

								@if(!empty($model_data->group_teams))
							    	@foreach($model_data->group_teams as $etTeam)
										<option value="{{ $etTeam->team_id }}" selected="selected">
											{{ $etTeam->team->name }}
										</option>
									@endforeach
								@endif
							@elseif(data_get($value, 'seperator') == 'seasonsettings')
							
								@if(!empty($model_data->championships))
									<option value="{{ $model_data->championships->team->id }}" selected="selected">
										{{ $model_data->championships->team->name }}
									</option>
								@endif
							@elseif(data_get($value, 'seperator') == 'season_team2')
							
								@if(!empty($model_data->season_team2->data))
									@php
										$season_team2_id = json_decode($model_data->season_team2->data);
										$season_team2 = App\Team::find($season_team2_id);
									@endphp

									<option value="{{ $season_team2->id }}" selected="selected">
										{{ $season_team2->name }}
									</option>
								@endif
							@elseif(data_get($value, 'seperator') == 'season_team3')
							
								@if(!empty($model_data->season_team3->data))
									@php
										$season_team3_id = json_decode($model_data->season_team3->data);
										$season_team3 = App\Team::find($season_team3_id);
									@endphp

									<option value="{{ $season_team3->id }}" selected="selected">
										{{ $season_team3->name }}
									</option>
								@endif
							@elseif(data_get($value, 'seperator') == 'season_team4')
							
								@if(!empty($model_data->season_team4->data))
									@php
										$season_team4_id = json_decode($model_data->season_team4->data);
										$season_team4 = App\Team::find($season_team4_id);
									@endphp

									<option value="{{ $season_team4->id }}" selected="selected">
										{{ $season_team4->name }}
									</option>
								@endif
							@elseif(data_get($value, 'seperator') == 'temsilciteam')

								@if(!empty($model_data->season_temsilci_team))

									@php
										$_temsilci_team = App\Team::where('id', $model_data->season_temsilci_team->data)->orderBy('name', 'asc')->first();
									@endphp

									<option value="{{ $_temsilci_team->id }}" selected="selected">
										{{ $_temsilci_team->name }}
									</option>

								@endif
							@elseif(data_get($value, 'seperator') == 'katilimciteams')

								@if(!empty($model_data->season_katilimci_teams))

									@php
										$teams_ids = json_decode($model_data->season_katilimci_teams->data);
										$_katilimci_teams = array();
										if(!empty($teams_ids)){
											$_katilimci_teams = App\Team::whereIn('id', $teams_ids)->orderBy('name', 'asc')->get();
										}
									@endphp

									@foreach ($_katilimci_teams as $kteam)
										<option value="{{ $kteam->id }}" selected="selected">
											{{ $kteam->name }}
										</option>
									@endforeach

								@endif
							@elseif(data_get($value, 'seperator') == 'player_mvp')

								@if(!empty($model_data->season_player_mvp->data))

									@php
										$player_mvp_id = json_decode($model_data->season_player_mvp->data);
										$player_mvp = App\Player::find($player_mvp_id);
									@endphp

									<option value="{{ $player_mvp->id }}" selected="selected">
										{{ '(TC:'.$player_mvp->user->profile->identity_number.') '.$player_mvp->user->first_name.' '.$player_mvp->user->last_name }}
									</option>

								@endif
							@elseif(data_get($value, 'seperator') == 'player_kog')

								@if(!empty($model_data->season_player_kog->data))

									@php
										$player_kog_id = json_decode($model_data->season_player_kog->data);
										$player_kog = App\Player::find($player_kog_id);
									@endphp

									<option value="{{ $player_kog->id }}" selected="selected">
										{{ '(TC:'.$player_kog->user->profile->identity_number.') '.$player_kog->user->first_name.' '.$player_kog->user->last_name }}
									</option>

								@endif
							@elseif(data_get($value, 'seperator') == 'player_beststriker')

								@if(!empty($model_data->season_player_beststriker->data))

									@php
										$player_beststriker_id = json_decode($model_data->season_player_beststriker->data);
										$player_beststriker = App\Player::find($player_beststriker_id);
									@endphp

									<option value="{{ $player_beststriker->id }}" selected="selected">
										{{ '(TC:'.$player_beststriker->user->profile->identity_number.') '.$player_beststriker->user->first_name.' '.$player_beststriker->user->last_name }}
									</option>

								@endif
							@elseif(data_get($value, 'seperator') == 'player_bestmiddle')

								@if(!empty($model_data->season_player_bestmiddle->data))

									@php
										$player_bestmiddle_id = json_decode($model_data->season_player_bestmiddle->data);
										$player_bestmiddle = App\Player::find($player_bestmiddle_id);
									@endphp

									<option value="{{ $player_bestmiddle->id }}" selected="selected">
										{{ '(TC:'.$player_bestmiddle->user->profile->identity_number.') '.$player_bestmiddle->user->first_name.' '.$player_bestmiddle->user->last_name }}
									</option>

								@endif
							@elseif(data_get($value, 'seperator') == 'player_bestdefance')

								@if(!empty($model_data->season_player_bestdefance->data))

									@php
										$player_bestdefance_id = json_decode($model_data->season_player_bestdefance->data);
										$player_bestdefance = App\Player::find($player_bestdefance_id);
									@endphp

									<option value="{{ $player_bestdefance->id }}" selected="selected">
										{{ '(TC:'.$player_bestdefance->user->profile->identity_number.') '.$player_bestdefance->user->first_name.' '.$player_bestdefance->user->last_name }}
									</option>

								@endif
							@elseif(data_get($value, 'seperator') == 'player_bestgoalkeeper')

								@if(!empty($model_data->season_player_bestgoalkeeper->data))

									@php
										$player_bestgoalkeeper_id = json_decode($model_data->season_player_bestgoalkeeper->data);
										$player_bestgoalkeeper = App\Player::find($player_bestgoalkeeper_id);
									@endphp

									<option value="{{ $player_bestgoalkeeper->id }}" selected="selected">
										{{ '(TC:'.$player_bestgoalkeeper->user->profile->identity_number.') '.$player_bestgoalkeeper->user->first_name.' '.$player_bestgoalkeeper->user->last_name }}
									</option>

								@endif
							@elseif(data_get($value, 'seperator') == 'player_penalties_season_id')
								
								@if(!empty($model_data))
									@php
										$__season = App\Season::find($model_data->season_id);
							        @endphp
							        
							        @if(!is_null($__season))
										<option value="{{ $__season->id }}" selected="selected">
											{{ $__season->league->name.' '.$__season->year.' '.$__season->name }}
										</option>
									@endif
								@endif
							@elseif(data_get($value, 'seperator') == 'player_penalties_match_id')
								
								@if(!empty($model_data))
									@php
										$__match = App\Match::find($model_data->match_id);
							        @endphp
							        
							        @if(!is_null($__match))
										<option value="{{ $__match->id }}" selected="selected">
											{{ $__match->team1->name.' - '.$__match->team2->name }}
										</option>
									@endif
								@endif
							@elseif(data_get($value, 'seperator') == 'ppenalties_type_id')
								
								@if(!empty($model_data))
									@php
										$__playerPenaltyType = App\PlayerPenalty::find($model_data->id)->penalty_type;
							        @endphp
							        
							        @if(!is_null($__playerPenaltyType))
										<option value="{{ $__playerPenaltyType->id }}" selected="selected">
											{{ $__playerPenaltyType->name }}
										</option>
									@endif
								@endif
							@elseif(data_get($value, 'seperator') == 'player_penalties_match_player_id')
								
								@if(!empty($model_data))
									@php
										$__playerPenaltyMP = App\PlayerPenalty::find($model_data->id);
										$__penaltyMP = App\Penalty::find($model_data->id);
										$__mp = App\MatchPlayer::where('match_id', $__penaltyMP->match_id)->where('player_id', $__playerPenaltyMP->player_id)->first();

							        @endphp
							        
							        @if(!is_null($__mp))
										<option value="{{ $__mp->id }}" selected="selected">
											@if(!empty($__mp->team))
												{{ '('.$__mp->team->name.') ' }}
											@endif
											@if(!empty($__mp->player->user))
												{{ $__mp->player->user->first_name .' '. $__mp->player->user->last_name }}
											@elseif(!empty($__playerPenaltyMP->guest_name))
												{{ $__playerPenaltyMP->guest_name.' (Misafir)' }}
											@endif
										</option>
									@endif
								@endif
							@elseif(data_get($value, 'seperator') == 'team_penalties_season_id')
								
								@if(!empty($model_data))
									@php
										$__season = App\Season::find($model_data->season_id);
							        @endphp
							        
							        @if(!is_null($__season))
										<option value="{{ $__season->id }}" selected="selected">
											{{ $__season->league->name.' '.$__season->year.' '.$__season->name }}
										</option>
									@endif
								@endif
							@elseif(data_get($value, 'seperator') == 'team_penalties_match_id')
								
								@if(!empty($model_data))
									@php
										$__match = App\Match::find($model_data->match_id);
							        @endphp
							        
							        @if(!is_null($__match))
										<option value="{{ $__match->id }}" selected="selected">
											{{ $__match->team1->name.' - '.$__match->team2->name }}
										</option>
									@endif
								@endif
							@elseif(data_get($value, 'seperator') == 'tpenalties_type_id')
								
								@if(!empty($model_data))
									@php
										$__teamPenaltyType = App\TeamPenalty::find($model_data->id)->penalty_type;
							        @endphp
							        
							        @if(!is_null($__teamPenaltyType))
										<option value="{{ $__teamPenaltyType->id }}" selected="selected">
											{{ $__teamPenaltyType->name }}
										</option>
									@endif
								@endif
							@elseif(data_get($value, 'seperator') == 'team_penalties_match_team_id')
								
								@if(!empty($model_data))
									@php
										$__teamPenaltyMP = App\TeamPenalty::find($model_data->id)->team;
							        @endphp
							        
							        @if(!is_null($__teamPenaltyMP))
										<option value="{{ $__teamPenaltyMP->id }}" selected="selected">
											{{ $__teamPenaltyMP->name }}
										</option>
									@endif
								@endif
							@elseif(data_get($value, 'seperator') == 'panorama_types_season')

								@php
									$__season = data_get($value, 'dataSource')::find($model_data[$key]);
						        @endphp

						        @if(!is_null($__season))
									<option value="{{ $model_data[$key] }}" selected="selected">
										{{ $__season->id.' '.$__season->league->name.' '.$__season->year.' '.$__season->name }}
									</option>
								@endif
							@elseif(data_get($value, 'seperator') == 'monthly_panorama_player_season')

								@php
									$__season = data_get($value, 'dataSource')::find($model_data[$key]);
						        @endphp

						        @if(!is_null($__season))
									<option value="{{ $model_data[$key] }}" selected="selected">
										{{ $__season->id.' '.$__season->league->name.' '.$__season->year.' '.$__season->name }}
									</option>
								@endif
							@elseif(data_get($value, 'seperator') == 'monthly_panorama_player_team')

								@php
									$__team = data_get($value, 'dataSource')::find($model_data[$key]);
						        @endphp

						        @if(!is_null($__team))
									<option value="{{ $model_data[$key] }}" selected="selected">
										{{ $__team->name }} ({{$__team->province->name }})
									</option>
								@endif
							@elseif(data_get($value, 'seperator') == 'monthly_panorama_player_player')

								@php
									$__player = data_get($value, 'dataSource')::find($model_data[$key]);
						        @endphp

						        @if(!is_null($__player))
									<option value="{{ $model_data[$key] }}" selected="selected">
										{{ '('.$__player->team->name.') '.$__player->user->first_name.' '.$__player->user->last_name }}
									</option>
								@endif
							@endif

						@endif

					@else

						@php
							if(empty($formConfig[data_get($value, 'trigger')]['relation_col'])){
								$dataSource = data_get($value, 'dataSource')::where(data_get($value, 'trigger'), $model_data[data_get($value, 'trigger')])->get();
							}else{
								$dataSource = data_get($value, 'dataSource')::where($formConfig[data_get($value, 'trigger')]['relation_col'], $model_data[$formConfig[data_get($value, 'trigger')]['relationship']][$formConfig[data_get($value, 'trigger')]['relation_col']])->get();
							}
						@endphp

						@foreach ($dataSource as $valuee)

							<option value="{{ $valuee->id }}"
								
								@if(empty(data_get($value, 'multiple')))
								
									@if(!empty($model_data))
										@if(!empty(data_get($value, 'relationship')))
											@if( $model_data[data_get($value, 'relationship')][data_get($value, 'relation_col')] ==  $valuee->id )
												selected="selected"
											@endif
										@else
											@if($model_data[$key] == $valuee->id)
												selected="selected"
											@endif
										@endif
									@endif
										
								@else
									
									@foreach ($model_data[data_get($value, 'relationship')] as $selectedVal)
										@if( $selectedVal->district_id == $valuee->id )
											selected="selected"
										@endif
									@endforeach

								@endif

							>

							{{ $valuee->name }}
							
							</option>
						@endforeach
						

					@endif

				</select>

			@endif


		</div>
	</div>

@endif