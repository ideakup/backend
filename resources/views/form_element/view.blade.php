<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">{{ data_get($value, 'title') }} : </label>
    <div class="col-lg-6 col-sm-12">
    	<div class="alert alert-secondary" role="alert">{{ (!is_null($model_data)) ? $model_data->$key : '' }}</div>
    </div>
</div>