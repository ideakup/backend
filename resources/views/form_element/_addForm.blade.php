@php
    $_is_tabbed = false;
    if(Request::segment(2) != 'add'){
        $tabConfig = config('tabs.'.Request::segment(1));
        if(!empty($tabConfig) && count($tabConfig) > 1){
            $_is_tabbed = true;
        }
    }
@endphp
@php
    $team1_last_match_control = true;
    $team2_last_match_control = true;

    if(Request::segment(1) == 'matches' && Request::segment(2) == 'edit' && $model_data->completed){
        $season_id = $model_data->season->id;

        $team1_matches1 = collect($model_data->team1->match1)->where('completed', true);
        $team1_matches2 = collect($model_data->team1->match2)->where('completed', true);
        $team1_last_match = $team1_matches1->merge($team1_matches2)->sortByDesc('time')->sortByDesc('date')->first();
        if(!empty($team1_last_match) && $model_data->id == $team1_last_match->id) {
            
        } else {
            $team1_last_match_control = false;
        }

        $team2_matches1 = collect($model_data->team2->match1)->where('completed', true);
        $team2_matches2 = collect($model_data->team2->match2)->where('completed', true);
        $team2_last_match = $team2_matches1->merge($team2_matches2)->sortByDesc('time')->sortByDesc('date')->first();

        if(!empty($team2_last_match) && $model_data->id == $team2_last_match->id) {
            
        } else {
            $team2_last_match_control = false;
        }
    }
@endphp
<div class="card card-custom">
    <div class="card-header {{ ($_is_tabbed) ? 'card-header-tabs-line' : '' }}">
        <h3 class="card-title">
            {{ __('title.'.Request::segment(1)) }} {{ __('title.'.Request::segment(2)) }}
            @if(Request::segment(1) == 'seasonsettings')
                ( {{ $model_data->year.' '.$model_data->league->name.' '.$model_data->name }} )
            @endif
        </h3>
        <div class="card-toolbar">
            @if($_is_tabbed)
                <ul class="nav nav-tabs nav-boldest nav-tabs-line">
                    @foreach ($tabConfig as $key_tabs => $value_tabs)

                        @php
                            $__show = '';
                            if(empty(request()->get('t')) && $loop->index == 0){
                                $__show = 'active';
                            }elseif(request()->get('t') == $key_tabs){
                                $__show = 'active';
                            }

                            $viewControl = false;
                            if(Request::segment(1) == 'teams' && data_get($value_tabs, 'content') == 'seasons'){
                                if(Auth::user()->hasPermissionTo('view_teamseason')){
                                    $viewControl = true;
                                }
                            }elseif(Request::segment(1) == 'teams' && data_get($value_tabs, 'content') == 'players'){
                                if(Auth::user()->hasPermissionTo('view_teamplayer')){
                                    $viewControl = true;
                                }
                            }elseif(Request::segment(1) == 'teams' && data_get($value_tabs, 'content') == 'team_status'){
                                if(Auth::user()->hasPermissionTo('termination_team')){
                                    $viewControl = true;
                                }
                            }elseif(Request::segment(1) == 'matches' && data_get($value_tabs, 'form') == 'match_video'){
                                if(Auth::user()->hasPermissionTo('view_matchvideo')){
                                    $viewControl = true;
                                }
                            }elseif(Request::segment(1) == 'matches' && data_get($value_tabs, 'content') == 'match_player'){
                                if(Auth::user()->hasPermissionTo('view_matchplayer')){
                                    $viewControl = true;
                                }
                            }elseif(Request::segment(1) == 'matches' && data_get($value_tabs, 'content') == 'match_action'){
                                if(Auth::user()->hasPermissionTo('view_matchaction')){
                                    $viewControl = true;
                                }
                            }elseif(Request::segment(1) == 'matches' && data_get($value_tabs, 'content') == 'match_panorama'){
                                if(Auth::user()->hasPermissionTo('view_matchpanorama')){
                                    $viewControl = true;
                                }
                            }elseif(Request::segment(1) == 'matches' && data_get($value_tabs, 'form') == 'match_press'){
                                if(Auth::user()->hasPermissionTo('view_pressconference')){
                                    $viewControl = true;
                                }
                            }elseif(Request::segment(1) == 'matches' && data_get($value_tabs, 'content') == 'match_image'){
                                if(Auth::user()->hasPermissionTo('view_matchimage')){
                                    $viewControl = true;
                                }
                            }else{
                                $viewControl = true;
                            }
                        @endphp

                        @if($viewControl)
                            <li class="nav-item">
                                <a class="nav-link {{ $__show }}" data-toggle="tab" href="#{{$key_tabs}}">
                                    <span class="nav-icon">
                                        <i class="{{ data_get($value_tabs, 'icon') }}"></i>
                                    </span>
                                    <span class="nav-text">{{ data_get($value_tabs, 'title') }}</span>
                                </a>
                            </li>
                        @endif

                    @endforeach
                </ul>
            @endif
        </div>
    </div>

    <div class="card-body">
        <div class="mb-3">
            
        	@if($_is_tabbed)
                <div class="tab-content">
                    @foreach ($tabConfig as $key_tabs => $value_tabs)

                        @php
                            $__show = '';
                            if(empty(request()->get('t')) && $loop->index == 0){
                                $__show = 'show active';
                            }elseif(request()->get('t') == $key_tabs){
                                $__show = 'show active';
                            }
                        @endphp
                        
                        <div class="tab-pane fade {{ $__show }}" id="{{ $key_tabs }}" role="tabpanel" aria-labelledby="{{ $key_tabs }}">

                            @if(!empty(data_get($value_tabs, 'content')))

                                @if(data_get($value_tabs, 'content') == 'seasons' && Auth::user()->hasPermissionTo('view_teamseason'))
                                    @include('tabs.seasons')
                                @elseif(data_get($value_tabs, 'content') == 'players')
                                    @include('tabs.players')
                                @elseif(data_get($value_tabs, 'content') == 'team_status')
                                    @include('tabs.team_status')
                                @elseif(data_get($value_tabs, 'content') == 'team_photo')
                                    @include('tabs.team_photo')
                                @elseif(data_get($value_tabs, 'content') == 'user_photo')
                                    @include('tabs.user_photo')
                                @elseif(data_get($value_tabs, 'content') == 'match_player')
                                    @include('tabs.match_player')
                                @elseif(data_get($value_tabs, 'content') == 'match_action')
                                    @include('tabs.match_action')
                                @elseif(data_get($value_tabs, 'content') == 'match_panorama')
                                    @include('tabs.match_panorama')
                                @elseif(data_get($value_tabs, 'content') == 'match_image')
                                    @include('tabs.match_image')
                                @elseif(data_get($value_tabs, 'content') == 'new_photogallery')
                                    @include('tabs.new_photogallery')
                                @elseif(data_get($value_tabs, 'content') == 'agenda_photo')
                                    @include('tabs.agenda_photo')
                                @elseif(data_get($value_tabs, 'content') == 'ground_photogallery')
                                    @include('tabs.ground_photogallery')
                                @elseif(data_get($value_tabs, 'content') == 'reservation_offer')
                                    @include('tabs.reservation_offer')
                                @elseif(data_get($value_tabs, 'content') == 'banner_photo')
                                    @include('tabs.banner_photo')
                                @elseif(data_get($value_tabs, 'content') == 'before_tournament_image')
                                    @include('tabs.before_tournament_image')
                                @elseif(data_get($value_tabs, 'content') == 'now_tournament_image')
                                    @include('tabs.now_tournament_image')
                                @elseif(data_get($value_tabs, 'content') == 'after_tournament_image')
                                    @include('tabs.after_tournament_image')
                                @elseif(data_get($value_tabs, 'content') == 'info_tournament_image')
                                    @include('tabs.info_tournament_image')
                                @elseif(data_get($value_tabs, 'content') == 'panorama_types_photo')
                                    @include('tabs.panorama_types_photo')
                                @elseif(data_get($value_tabs, 'content') == 'league_gallery_items')
                                    @include('tabs.league_gallery_items')
                                @endif

                            @endif

                            @if(!empty(data_get($value_tabs, 'form')))
                                @php
                                    $formConfig = config('forms.'.data_get($value_tabs, 'form'));
                                @endphp

                                @if(!is_null($formConfig))


                                    @if((data_get($value_tabs, 'form') == 'match_player' || data_get($value_tabs, 'form') == 'match_action') && (!empty($model_data->completed) && $model_data->completed))
                                    @else
                                        
                                        <form class="form" method="POST" action="{{ url(data_get($value_tabs, 'form').'/save') }}" id="{{ data_get($value_tabs, 'form') }}Form">
        
                                            {{ csrf_field() }}
                                            <input type="hidden" name="segment" value="{{ Request::segment(1) }}">
                                            <input type="hidden" name="crud" value="{{ Request::segment(2) }}">
                                            <input type="hidden" name="id" value="{{ Request::segment(3) }}">

                                            @foreach ($formConfig as $key => $value)

                                                @php
                                                    //dump($key);
                                                    //dump($value);
                                                @endphp
                                                @if(data_get($value, 'type') == 'h3' || data_get($value, 'type') == 'p')
                                                    @include('form_element.header')
                                                @elseif(data_get($value, 'type') == 'text' || data_get($value, 'type') == 'number' || data_get($value, 'type') == 'email')
                                                    @include('form_element.input')
                                                @elseif(data_get($value, 'type') == 'hidden')
                                                    @include('form_element.hidden')
                                                @elseif(data_get($value, 'type') == 'html')
                                                    @include('form_element.html')
                                                @elseif(data_get($value, 'type') == 'view')
                                                    @include('form_element.view')
                                                @elseif(data_get($value, 'type') == 'password')
                                                    @include('form_element.password')
                                                @elseif(data_get($value, 'type') == 'textarea')
                                                    @include('form_element.textarea')
                                                @elseif(data_get($value, 'type') == 'checkbox')
                                                    @include('form_element.checkbox')
                                                @elseif(data_get($value, 'type') == 'radio')
                                                    @include('form_element.radio')
                                                @elseif(data_get($value, 'type') == 'select2')
                                                    @include('form_element.select2')
                                                @elseif(data_get($value, 'type') == 'datetimepicker')
                                                    @include('form_element.datetimepicker')
                                                @elseif(data_get($value, 'type') == 'datepicker')
                                                    @include('form_element.datepicker')
                                                @elseif(data_get($value, 'type') == 'timepicker')
                                                    @include('form_element.timepicker')
                                                @elseif(data_get($value, 'type') == 'button')
                                                    @include('form_element.button')
                                                @elseif(data_get($value, 'type') == 'switch')
                                                    @include('form_element.switch')
                                                @endif

                                            @endforeach

                                            <div class="row" style="padding: 2rem 0 0 0; border-top: 1px solid #EBEDF3;">
                                                <div class="col-lg-3"></div>
                                                <div class="col-lg-6">
                                                    
                                                    @if(Request::segment(2) == 'view')
                                                        <button type="button" class="btn btn-secondary" onclick="history.back()">Geri</button>
                                                    @elseif(Request::segment(2) == 'add' || Request::segment(2) == 'edit')

                                                        @if( data_get($value_tabs, 'form') == 'reservation_offer_convert_match' )
                                                            
                                                            @php $roid = ''; @endphp
                                                            @foreach ($model_data->reservation_offer as $key_rel => $value_rel)
                                                                @if($value_rel->status == 'confirmed')
                                                                    @php $roid = $value_rel->id; @endphp
                                                                @endif
                                                            @endforeach

                                                            <button type="submit" class="btn btn-success mr-2" id="{{ data_get($value_tabs, 'form') }}Button" @if(empty($roid)) @endif>
                                                                Maça Dönüştür
                                                            </button>

                                                        @else

                                                            <button type="submit" class="btn btn-success mr-2">
                                                                Kaydet
                                                            </button>

                                                        @endif



                                                        @if( data_get($value_tabs, 'form') == 'match_player' )
                                                            <a href="{{ url()->current() }}?t=tab_03" type="button" class="btn btn-secondary">İptal</a>
                                                        @elseif( data_get($value_tabs, 'form') == 'match_action' )
                                                            <a href="{{ url()->current() }}?t=tab_04" type="button" class="btn btn-secondary">İptal</a>
                                                        @else
                                                            <button type="button" class="btn btn-secondary" onclick="history.back()">İptal</button>
                                                        @endif

                                                    @elseif(Request::segment(2) == 'delete')
                                                        <button type="button" class="btn btn-secondary" onclick="history.back()">İptal</button>
                                                    @endif
                                                    
                                                </div>
                                                <div class="col-lg-3 text-right">
                                                    @if(Request::segment(2) == 'delete')
                                                        @if((Request::segment(1) == 'matches' && $key_tabs == 'tab_01'))
                                                            @if(Request::segment(1) == 'matches' && Request::segment(2) == 'delete' && !$model_data->completed)
                                                                <button type="submit" class="btn btn-danger mr-2">Sil</button>
                                                            @else
                                                                <div class="alert alert-custom alert-light-danger" role="alert" style="margin-bottom:0;">
                                                                    <div class="alert-text">
                                                                        Maç durumu <strong>"Tamamlandı"</strong> olan maç silinemez.
                                                                    </div>
                                                                </div>
                                                            @endif
                                                        @elseif((Request::segment(1) == 'reservations' && $key_tabs == 'tab_01'))
                                                            <button type="submit" class="btn btn-danger mr-2">Sil</button>
                                                        @elseif (Request::segment(1) != 'matches' && Request::segment(1) != 'reservations')
                                                            <button type="submit" class="btn btn-danger mr-2">Sil</button>
                                                        @endif
                                                    @endif
                                                </div>
                                            </div>
                                        </form>

                                    @endif

                                @endif

                            @endif

                        </div>

                    @endforeach
                </div>

            @else

                @php
                    $formConfig = config('forms.'.Request::segment(1));
                @endphp

                @if(!is_null($formConfig))

                    <form class="form" method="POST" action="{{ url(Request::segment(1).'/save') }}" id="{{Request::segment(1)}}Form">

                        {{ csrf_field() }}
                        <input type="hidden" name="segment" value="{{ Request::segment(1) }}">
                        <input type="hidden" name="crud" value="{{ Request::segment(2) }}">
                        <input type="hidden" name="id" value="{{ Request::segment(3) }}">

                        @if(!empty(request()->query('tree_id')))
                            <input type="hidden" name="tree_id" value="{{ request()->query('tree_id') }}">
                        @endif

                        @foreach ($formConfig as $key => $value)
                            
                            @if(empty($model_data->player) && !empty(data_get($value, 'relationship')) && data_get($value, 'relationship') == 'player' && Request::segment(1) != 'addoutsideplayer')
                                @php
                                    //dump('Taraftar tipi kullanıcının oyuncu özelliklerini gizler');
                                @endphp
                            @else
                            
                                @if(data_get($value, 'type') == 'h3' || data_get($value, 'type') == 'p')
                                    @include('form_element.header')
                                @elseif(data_get($value, 'type') == 'text' || data_get($value, 'type') == 'number' || data_get($value, 'type') == 'email')
                                    @include('form_element.input')
                                @elseif(data_get($value, 'type') == 'hidden')
                                    @include('form_element.hidden')
                                @elseif(data_get($value, 'type') == 'password')
                                    @include('form_element.password')
                                @elseif(data_get($value, 'type') == 'textarea')
                                    @include('form_element.textarea')
                                @elseif(data_get($value, 'type') == 'html')
                                    @include('form_element.html')
                                @elseif(data_get($value, 'type') == 'checkbox')
                                    @include('form_element.checkbox')
                                @elseif(data_get($value, 'type') == 'radio')
                                    @include('form_element.radio')
                                @elseif(data_get($value, 'type') == 'select2')
                                    @include('form_element.select2')
                                @elseif(data_get($value, 'type') == 'datetimepicker')
                                    @include('form_element.datetimepicker')
                                @elseif(data_get($value, 'type') == 'datepicker')
                                    @include('form_element.datepicker')
                                @elseif(data_get($value, 'type') == 'timepicker')
                                    @include('form_element.timepicker')
                                @elseif(data_get($value, 'type') == 'button')
                                    @include('form_element.button')
                                @elseif(data_get($value, 'type') == 'switch')
                                    @include('form_element.switch')
                                @endif

                            @endif

                        @endforeach

                        <div class="row" style="padding: 2rem 0 0 0; border-top: 1px solid #EBEDF3;">
                            <div class="col-lg-3"></div>
                            <div class="col-lg-6">
                                
                                @if(Request::segment(2) == 'view')
                                    <button type="button" class="btn btn-secondary" onclick="history.back()">Geri</button>
                                @elseif(Request::segment(2) == 'add' || Request::segment(2) == 'edit')
                                    <button type="submit" class="btn btn-success mr-2">Kaydet</button>
                                    <button type="button" class="btn btn-secondary" onclick="history.back()">İptal</button>
                                @elseif(Request::segment(2) == 'delete')
                                    <button type="button" class="btn btn-secondary" onclick="history.back()">İptal</button>
                                @endif
                                
                            </div>
                            <div class="col-lg-3 text-right">
                                @if(Request::segment(2) == 'delete')
                                    <button type="submit" class="btn btn-danger mr-2">Sil</button>
                                @endif
                            </div>
                        </div>

                    </form>

                @endif

            @endif

        </div>

    </div>

</div>
