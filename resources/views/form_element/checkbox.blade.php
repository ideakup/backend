<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">{{ data_get($value, 'title') }} : </label>
    <div class="col-lg-6 col-sm-12">
        <div class="checkbox-inline">

    		<label class="checkbox">
                <input 
                    
                    type="checkbox" 
                    name="{{ $key }}" 
                    value="{{ data_get($value, 'value') }}" 

                    @if(!empty(data_get($value, 'relationship')))
                        @if(!is_null($model_data[data_get($value, 'relationship')]))
                            @if($model_data[data_get($value, 'relationship')][data_get($value, 'relation_col')] == data_get($value, 'checkvalue'))
                                checked="checked"
                            @endif
                        @endif
                    @else
                        @if(!is_null($model_data) && $model_data->$key == true) checked="checked" @endif
                    @endif

                    @if(Request::segment(2) == 'view' || Request::segment(2) == 'delete') disabled="disabled" @endif

                />
                <span></span>{{ data_get($value, 'title') }}
                
            </label>

        </div>
        @if(!empty(data_get($value, 'description')))
        	<span class="form-text text-muted">{{ data_get($value, 'description') }}</span>
        @endif
    </div>
</div>

