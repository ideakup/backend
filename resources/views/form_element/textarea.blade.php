<div class="form-group row mb-1">
    <label class="col-form-label text-right col-lg-3 col-sm-12">{{ data_get($value, 'title') }} : </label>
    <div class="col-lg-6 col-sm-12">
        <textarea class="form-control" rows="4"

            name="{{ $key }}" 
            placeholder="{{ data_get($value, 'placeholder') }}"
            @if(Request::segment(2) == 'view' || Request::segment(2) == 'delete') disabled="disabled" @endif

            >{{ (!is_null($model_data)) ? $model_data->$key : '' }}</textarea>
    </div>
    @if(!empty(data_get($value, 'description')))
        <span class="form-text text-muted">{{ data_get($value, 'description') }}</span>
    @endif
</div>