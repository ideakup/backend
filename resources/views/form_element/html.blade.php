@php
    //dd($model_data);
@endphp
<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">{{ data_get($value, 'title') }} : </label>
    <div class="col-lg-6 col-sm-12">
        @if($key == 'before_teaser_text')
            <textarea id="{{ $key }}" name="{{ $key }}" class="tox-target">
                {{ (!is_null($model_data->season_before_teaser_text)) ? $model_data->season_before_teaser_text->data : '' }}
            </textarea>
        @elseif ($key == 'after_teaser_text')
            <textarea id="{{ $key }}" name="{{ $key }}" class="tox-target">
                {{ (!is_null($model_data->season_after_teaser_text)) ? $model_data->season_after_teaser_text->data : '' }}
            </textarea>
        @elseif ($key == 'info_text')
            <textarea id="{{ $key }}" name="{{ $key }}" class="tox-target">
                {{ (!is_null($model_data->season_info_text)) ? $model_data->season_info_text->data : '' }}
            </textarea>
        @elseif ($key == 'content')
            <textarea id="{{ $key }}" name="{{ $key }}" class="tox-target">
                {{ (!empty($model_data->content)) ? $model_data->content : '' }}
            </textarea>
        @elseif ($key == 'rule_text')
            <textarea id="{{ $key }}" name="{{ $key }}" class="tox-target">
                {{ (!empty($model_data->rule_text)) ? $model_data->rule_text : '' }}
            </textarea>
        @endif
    </div>
</div>
