<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">{{ data_get($value, 'title') }} : </label>
    <div class="col-lg-6 col-sm-12">
 
        <input class="form-control " 

            type="{{ data_get($value, 'type') }}" 
            name="{{ $key }}" 
            placeholder="{{ data_get($value, 'placeholder') }}" 

            @if(data_get($value, 'type') == 'number')

                @if(!empty(data_get($value, 'step')))
                    step="{{ data_get($value, 'step') }}"
                @endif

            @endif

            @foreach (explode('|', data_get($value, 'validation')) as $valid)

                @if(str_before($valid, ':') == 'min')
                    min="{{ str_after($valid, ':') }}"
                @elseif(str_before($valid, ':') == 'max')
                    max="{{ str_after($valid, ':') }}"
                @endif

            @endforeach

            @if(!empty(data_get($value, 'custom')))

                @php
                    $activeSeason = null;
                @endphp

                @foreach ($model_data->team_teamseason as $valid)
                    @if($valid->season->active == true && $valid->season->start_date <= Carbon\Carbon::now()->format('Y-m-d') && $valid->season->end_date >= Carbon\Carbon::now()->format('Y-m-d') && $valid->season->polymorphic_ctype_id != 32)
                        @php
                            $activeSeason = $valid->season;
                        @endphp
                    @endif
                @endforeach

                @php
                    //dump($key);
                @endphp

                @if($key == 'custom_transfer_start_date')
                    value="{{ (!empty($activeSeason)) ? Carbon\Carbon::parse($activeSeason->transfer_start_date)->format('d.m.Y') : '' }}" disabled="disabled"
                @elseif($key == 'custom_transfer_end_date')
                    value="{{ (!empty($activeSeason)) ? Carbon\Carbon::parse($activeSeason->transfer_end_date)->format('d.m.Y') : '' }}" disabled="disabled"
                @elseif($key == 'custom_remaining_transfer_count')
                    value="{{ $model_data->team_teamseason->first()->remaining_transfer_count }}" disabled="disabled"
                @elseif($key == 'before_teaser_text' && !empty($model_data->season_before_teaser_text->data))
                    value="{{ $model_data->season_before_teaser_text->data }}"
                @elseif($key == 'before_teaser' && !empty($model_data->season_before_teaser->data))
                    value="{{ $model_data->season_before_teaser->data }}"
                @elseif($key == 'before_playlist' && !empty($model_data->season_before_playlist->data))
                    value="{{ $model_data->season_before_playlist->data }}"
                @elseif($key == 'after_teaser_text' && !empty($model_data->season_after_teaser_text->data))
                    value="{{ $model_data->season_after_teaser_text->data }}"
                @elseif($key == 'info_text' && !empty($model_data->season_info_text->data))
                    value="{{ $model_data->season_info_text->data }}"
                @elseif($key == 'after_teaser' && !empty($model_data->season_after_teaser->data))
                    value="{{ $model_data->season_after_teaser->data }}"
                @elseif($key == 'now_teaser' && !empty($model_data->season_now_teaser->data))
                    value="{{ $model_data->season_now_teaser->data }}"
                @elseif($key == 'total_match' && !empty($model_data->season_total_match->data))
                    value="{{ $model_data->season_total_match->data }}"
                @elseif($key == 'to_tt' && !empty($model_data->season_to_tt->data))
                    value="{{ $model_data->season_to_tt->data }}"
                @elseif($key == 'to_fkbt' && !empty($model_data->season_to_fkbt->data))
                    value="{{ $model_data->season_to_fkbt->data }}"
                @elseif($key == 'to_o' && !empty($model_data->season_to_o->data))
                    value="{{ $model_data->season_to_o->data }}"
                @elseif($key == 'to_fkbo' && !empty($model_data->season_to_fkbo->data))
                    value="{{ $model_data->season_to_fkbo->data }}"
                @elseif($key == 'to_cye' && !empty($model_data->season_to_cye->data))
                    value="{{ $model_data->season_to_cye->data }}"
                @endif




            @elseif(empty(data_get($value, 'add_only')))
                @if(!empty($value_tabs) && !empty(data_get($value_tabs, 'relationship')))
                    value="{{ (!is_null($model_data[data_get($value_tabs, 'relationship')])) ? $model_data[data_get($value_tabs, 'relationship')][$key] : '' }}"
                @elseif(!empty(data_get($value, 'relationship')))
                    value="{{ (!is_null($model_data[data_get($value, 'relationship')])) ? $model_data[data_get($value, 'relationship')][data_get($value, 'relation_col')] : '' }}"
                @else
                    value="{{ (!is_null($model_data)) ? $model_data->$key : '' }}"
                @endif
            @endif
        
            @if(Request::segment(2) == 'view' || Request::segment(2) == 'delete') disabled="disabled" @endif

        />

      

        @if(!empty(data_get($value, 'description')))
        	<span class="form-text text-muted">{{ data_get($value, 'description') }}</span>
        @endif
    </div>
</div>