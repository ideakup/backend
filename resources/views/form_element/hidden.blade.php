@if(!empty(data_get($value, 'defaultValue')))

	<input type="hidden" name="{{ $key }}" value="{{ data_get($value, 'defaultValue') }}">

@elseif(!empty(data_get($value, 'model_col')))

	<input type="hidden" name="{{ $key }}" value="{{ $model_data[data_get($value, 'model_col')] }}">

@elseif(!empty(data_get($value, 'exception')) && data_get($value, 'exception') == 'reservation_offer_id')
	
	@php $roid = ''; @endphp
	
	@foreach ($model_data->reservation_offer as $key_rel => $value_rel)
		@if($value_rel->status == 'confirmed')
			@php $roid = $value_rel->id; @endphp
		@endif
    @endforeach

	<input type="hidden" name="{{ $key }}" value="{{ $roid }}">

@elseif(!empty(data_get($value, 'exception')) && data_get($value, 'exception') == 'league_gallery_league_id')
	@if(!empty(request()->query('league_id')))
		<input type="hidden" name="{{ $key }}" value="{{ request()->query('league_id') }}">
	@else
		<input type="hidden" name="{{ $key }}" value="{{ $model_data[$key] }}">
	@endif

@else

	<input type="hidden" name="{{ $key }}" value="">
	
@endif