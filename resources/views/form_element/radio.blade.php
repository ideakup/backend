<div class="form-group row align-items-center">
    <label class="col-form-label text-right col-lg-3 col-sm-12">{{ data_get($value, 'title') }} : </label>
    <div class="col-lg-6 col-sm-12">
        <div class="radio-inline">

            @foreach (data_get($value, 'values') as $val)

                <label class="radio">
                    <input type="radio" 
                    
                    name="{{ $key }}" 
                    value="{{ data_get($val, 'value') }}"

                    @if(!empty(data_get($value, 'relationship')))
                        @if(!is_null($model_data[data_get($value, 'relationship')]))
                            @if($model_data[data_get($value, 'relationship')][data_get($value, 'relation_col')] == data_get($val, 'value'))
                                checked="checked" 
                            @endif     
                        @endif
                    @else
                        @if(!is_null($model_data))
                            @if($model_data->$key == data_get($val, 'value'))
                                checked="checked" 
                            @endif     
                        @endif
                    @endif
                    
                    >
                    <span></span>{{ data_get($val, 'title') }}
                </label>
            
            @endforeach

        </div>
        @if(!empty(data_get($value, 'description')))
            <span class="form-text text-muted">{{ data_get($value, 'description') }}</span>
        @endif
    </div>
</div>