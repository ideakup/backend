<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">{{ data_get($value, 'title') }} : </label>
    <div class="col-lg-6 col-sm-12">
        <input class="form-control" 

        type="{{ data_get($value, 'type') }}" 
        name="{{ $key }}" 
        placeholder="{{ data_get($value, 'placeholder') }}" 
        value="{{ (!is_null($model_data)) ? $model_data->$key : '' }}"
        @if(Request::segment(2) == 'view' || Request::segment(2) == 'delete') disabled="disabled" @endif

        />

        @if(!empty(data_get($value, 'description')))
        	<span class="form-text text-muted">{{ data_get($value, 'description') }}</span>
        @endif
    </div>
</div>